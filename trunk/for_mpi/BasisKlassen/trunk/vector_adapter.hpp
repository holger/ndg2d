#ifndef vector_adapter_hpp
#define vector_adapter_hpp

#include <iostream>

#include "vector_class.hpp"
#include "particle_class.hpp"
#include "vector_adapter_typen.hpp"

namespace BK {

const int TEST_vector_adapter = 0;

template<class TYPE>
class VectorAdapterBase 
{
 public:
  
  // Type von dem die Adapter-elemente sind
  typedef typename SetType<TYPE>::DOUBLE DOUBLE;

  VectorAdapterBase(const VectorAdapter<TYPE> &v_a);

  // HAUPT-Konstruktor: erzeugt vector-adapter aus Vector<TYPE>
  VectorAdapterBase(Vector<TYPE>& _typeArray);
  
 protected:

  // Anzahl der Adapter-elemente pro TYPE
  int typIndexSize;

  // Pointer auf origninal-array
  Vector<TYPE>& typeArray;

  // Gr��e des adapter_arrays
  int size;

  // Gr��e des typ-arrays
  int typArraySize;
};

// ---------------------------------------------------------------------------------------

template<class TYPE>
class VectorAdapter : public SetType<TYPE>::ADAPTER_TYPE
{
 public:
  
  // Type von dem die Adapter-elemente sind
  typedef typename SetType<TYPE>::DOUBLE DOUBLE;
  
 /*  // default-Konstruktor */
/*   VectorAdapter(); */

  // HAUPT-Konstruktor: erzeugt vector-adapter aus Vector<TYPE>
  VectorAdapter(Vector<TYPE>& _typeArray);
  
  // copy-Konstruktor
  VectorAdapter(const VectorAdapter &v_a);
  
  // Destruktor
  ~VectorAdapter();
  
  // read-operator f�r TYPE-Element
  TYPE operator() (int i) const;

  // write-operator f�r TYPE-Element
  TYPE& operator() (int i);

  // Kopier-Operator= kopiert elementweise
  VectorAdapter &operator=(const VectorAdapter& v);  
  
  // Kopier-Operator= kopiert elementweise
  VectorAdapter &operator=(const Vector<DOUBLE>& v);  
  
  // gibt die Anzahl der Elemente des Adapters zur�ck
  int getSize() const;

  // gibt die Anzahl der Elemente des VectorAdapters zur�ck
  int getTypArraySize() const;
  
  // bestimmt das maximale Element
  DOUBLE getMaxValue() const;            

  // bestimmt das minimale Element
  DOUBLE getMinValue() const;   
  
 protected:

 };
 // Multiplikations-Operator mit Skalar: 3.*v
template<class TYPE>
Vector<typename SetType<TYPE>::DOUBLE> operator*(typename SetType<TYPE>::DOUBLE zahl, const VectorAdapter<TYPE>& v);

// Multiplikations-Operator mit Skalar: v*3.
template<class TYPE>
Vector<typename SetType<TYPE>::DOUBLE> operator*(const VectorAdapter<TYPE>& v, typename SetType<TYPE>::DOUBLE zahl);

// Divisions-Operator mit Skalar: v/3.
template<class TYPE>
Vector<typename SetType<TYPE>::DOUBLE> operator/(const VectorAdapter<TYPE>& v, typename SetType<TYPE>::DOUBLE zahl);

// Additions-Operator f�r zwei VectorAdapter: v+w
template<class TYPE>
Vector<typename SetType<TYPE>::DOUBLE> operator+
(const VectorAdapter<TYPE>& v, const VectorAdapter<TYPE>& w);

// Subtraktions-Operator f�r zwei VectorAdapter: v-w
template<class TYPE>
Vector<typename SetType<TYPE>::DOUBLE> operator-
(const VectorAdapter<TYPE>& v, const VectorAdapter<TYPE>& w);

// Skalarprodukt von v und w: v*w  
template<class TYPE>
typename SetType<TYPE>::DOUBLE operator*(const VectorAdapter<TYPE>& v, const VectorAdapter<TYPE>& w);

// Euklidisches Betragsquadrat
template<class TYPE>
typename SetType<TYPE>::DOUBLE sqr(const VectorAdapter<TYPE>& v);

// -----------------------------------------------------------------------------------------------------
// definitions
// -----------------------------------------------------------------------------------------------------

template<class TYPE>
VectorAdapterBase<TYPE>::VectorAdapterBase(const VectorAdapter<TYPE> &v_a) : typeArray(v_a.typeArray)
{
  typArraySize = v_a.getTypArraySize();
  size = v_a.getSize();
}

template<class TYPE>
VectorAdapterBase<TYPE>::VectorAdapterBase(Vector<TYPE>& _typeArray) : typeArray(_typeArray)
{
  //cout << "VectorAdapterBase(Vector<TYPE>)\n";
  typArraySize = typeArray.getSize();
}

// ----------------------------------------------------------------------------------------------

template<class TYPE>
VectorAdapter<TYPE>::VectorAdapter(Vector<TYPE>& _typeArray)
    : SetType<TYPE>::ADAPTER_TYPE(_typeArray)
{
  size = typeArray.getSize()*typIndexSize;
}

template<class TYPE>
VectorAdapter<TYPE>::VectorAdapter(const VectorAdapter &v_a)
  : SetType<TYPE>::ADAPTER_TYPE(v_a)
{}

template<class TYPE>
VectorAdapter<TYPE>::~VectorAdapter()
{
  if(TEST_vector_adapter) std::cerr << "VectorAdapter: Destruktor" << std::endl;
}

template<class TYPE>
int VectorAdapter<TYPE>::getSize() const
{
  return size;
}

template<class TYPE>

int VectorAdapter<TYPE>::getTypArraySize() const
{
  return typArraySize;
}

template<class TYPE>
//typename VectorAdapter<TYPE>::DOUBLE VectorAdapter<TYPE>::get_min_value() const
typename SetType<TYPE>::DOUBLE VectorAdapter<TYPE>::getMinValue() const
{
  DOUBLE min_value;
  minValue = (*this)[0];
  for(int i=1;i<size;i++){
    if(minValue > (*this)[i]){
      minValue = (*this)[i];
    }
    else;
  }
  return minValue;
}

template<class TYPE>
typename SetType<TYPE>::DOUBLE VectorAdapter<TYPE>::getMaxValue() const
{
  DOUBLE minValue;
  minValue = (*this)[0];
  for(int i=1;i<size;i++){
    if(minValue < (*this)[i]){
      minValue = (*this)[i];
    }
    else;
  }
  return minValue;
}

template<class TYPE>
TYPE VectorAdapter<TYPE>::operator() (int i) const
{
  if (i < typeArray_low || i > typeArray_high) {
    std::cerr <<  "invalid index " << i << std::endl;
    abort();
  }
  return (*typeArray)[i];
}

template<class TYPE>
TYPE& VectorAdapter<TYPE>::operator() (int i)
{
  if (i < typeArray_low || i > typeArray_high) {
    std::cerr <<  "invalid index " << i << std::endl;
    abort();
  }
  return (*typeArray)[i];
}

template<class TYPE>
VectorAdapter<TYPE>& VectorAdapter<TYPE>::operator= (const VectorAdapter<TYPE>& v)
{
  if(typeArray_low != v.get_typeArray_low() || typeArray_high != v.get_typeArray_high()){
    std::cerr << "low- oder high- indizes stimmen nicht �berein!" << std::endl;
    abort();
  }
  for(int i=typeArray_low;i<=typeArray_high;i++){
    (*this)(i) = v(i);
  }
  return *this;
}

template<class TYPE>
VectorAdapter<TYPE>& VectorAdapter<TYPE>::operator= (const Vector<DOUBLE>& v)
{
  if(typeArray_low != v.get_typeArray_low() || typeArray_high != v.get_typeArray_high()){
    std::cerr << "low- oder high- indizes stimmen nicht �berein!" << std::endl;
    abort();
  }
  for(int i=typeArray_low;i<=typeArray_high;i++){
    (*this)(i) = v(i);
  }
  return *this;
}

template<class TYPE>
std::ostream& operator<<(std::ostream &of, const VectorAdapter<TYPE> &v_a)         // Ausgabeoperator der Vektorklasse
{  
  if(TEST_vector_adapter) std::cerr << "VectorAdapter: << " << std::endl;
  of << "[";
  for (int i = 0; i < v_a.getSize()-1; i++)
    of << v_a[i] << ",";
  of << v_a[v_a.getSize()-1] << "]";

  return of;
}

template<class TYPE>
Vector<typename SetType<TYPE>::DOUBLE> operator*(typename SetType<TYPE>::DOUBLE zahl, const VectorAdapter<TYPE>& v)
{
  Vector<typename SetType<TYPE>::DOUBLE > temp(v.getSize());
  for(int i=0;i < v.getSize();i++){
    temp[i] = v[i]*zahl;
  }
  return temp;
}

template<class TYPE>
Vector<typename SetType<TYPE>::DOUBLE> operator*(const VectorAdapter<TYPE>& v, typename SetType<TYPE>::DOUBLE zahl)
{
  Vector<typename SetType<TYPE>::DOUBLE > temp(v.getSize());
  for(int i=0;i < v.getSize();i++){
    temp[i] = v[i]*zahl;
  }
  return temp;
}

template<class TYPE>
Vector<typename SetType<TYPE>::DOUBLE> operator/(const VectorAdapter<TYPE>& v, typename SetType<TYPE>::DOUBLE zahl)
{
  Vector<typename SetType<TYPE>::DOUBLE > temp(v.getSize());
  for(int i=0;i < v.getSize();i++){
    temp[i] = v[i]/zahl;
  }
  return temp;
}

template<class TYPE>
Vector<typename SetType<TYPE>::DOUBLE> operator+
(const VectorAdapter<TYPE>& v, const VectorAdapter<TYPE>& w)
{
  Vector<typename SetType<TYPE>::DOUBLE > temp(v.getSize());
  for(int i=0;i < v.getSize();i++){
    temp[i] = v[i]+w[i];
  }
  return temp;
}

template<class TYPE>
Vector<typename SetType<TYPE>::DOUBLE> operator-
(const VectorAdapter<TYPE>& v, const VectorAdapter<TYPE>& w)
{
  Vector<typename SetType<TYPE>::DOUBLE > temp(v.getSize());
  for(int i=0;i < v.getSize();i++){
    temp[i] = v[i]-w[i];
  }
  return temp;
}

template<class TYPE>
typename SetType<TYPE>::DOUBLE operator*(const VectorAdapter<TYPE>& v, const VectorAdapter<TYPE>& w)
{
  typename SetType<TYPE>::DOUBLE tmp = 0;
  for(int i = 0;i<v.getSize();i++){
    tmp += v[i]*w[i];
  }
  return tmp;
}

template<class TYPE>
typename SetType<TYPE>::DOUBLE sqr(const VectorAdapter<TYPE>& v)
{
  typename SetType<TYPE>::DOUBLE tmp = 0;
  for(int i=0;i<v.getSize();i++){
    tmp += v[i]*v[i];
  }
  return tmp;
}

} // namespace BK

#endif
