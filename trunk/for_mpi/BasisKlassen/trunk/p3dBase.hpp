#ifndef bk_p3dBase_hpp
#define bk_p3dBase_hpp

#include <cmath>

#include "singleton.hpp"
#include "utilities.hpp"
#include "fftBase.hpp"

#ifdef Darwin_x86_64
#define ADD_FORTRAN_PREFIX(name,parameters)		\
  __p3dfft_MOD_##name parameters
	  //	  p3dfft_mp_##name##_ parameters
  //      __p3dfft__##name parameters
// p3dfft_mp_##name##_ parameters
#endif

// Intel and gcc/clang compilers produce different names for fortran functions.
// See https://en.wikipedia.org/wiki/Name_mangling#Fortran
#ifdef Linux_x86_64
#if defined __INTEL_COMPILER
#define ADD_FORTRAN_PREFIX(name, parameters) \
        p3dfft_mp_##name##_ parameters
#elif defined __GNUC__  // also includes clang
#define ADD_FORTRAN_PREFIX(name, parameters) \
        __p3dfft_MOD_##name parameters
#endif
#endif

//#ifdef Linux_i386
//#define ADD_FORTRAN_PREFIX(name,parameters)		
//        p3dfft_mp_##name_ parameters
//#endif
#ifdef Linux_i386
#define ADD_FORTRAN_PREFIX(name,parameters)   \
        __p3dfft__##name parameters
#endif

#ifdef AIX_00223C3B4C00
#define ADD_FORTRAN_PREFIX(name,parameters)   \
        __p3dfft_NMOD_##name##_ parameters
#endif
#ifdef AIX_00CF30914C00
#define ADD_FORTRAN_PREFIX(name,parameters)   \
        __p3dfft_NMOD_##name##_ parameters
#endif
#ifdef AIX_00CFEBE14C00
#define ADD_FORTRAN_PREFIX(name,parameters)   \
        __p3dfft_NMOD_##name##_ parameters
#endif
#ifdef Linux_ppc64
#define ADD_FORTRAN_PREFIX(name,parameters)   \
        __p3dfft_NMOD_##name##_ parameters
#endif

#ifdef BK_P3D_DOUBLE_PRECISION
extern "C" { 
  void ADD_FORTRAN_PREFIX(p3dfft_setup,(int*,int&,int&,int&,bool&););
  void ADD_FORTRAN_PREFIX(get_dims,(int*, int*, int*,int&););
  void ADD_FORTRAN_PREFIX(p3dfft_btran_c2r,(const double*,double*,unsigned char&););
  void ADD_FORTRAN_PREFIX(p3dfft_btran_c2rx,(const double*,double*,unsigned char&););
  void ADD_FORTRAN_PREFIX(p3dfft_btran_c2ry,(const double*,double*,unsigned char&););
  void ADD_FORTRAN_PREFIX(p3dfft_ftran_r2c,(const double*,double*,unsigned char&););
  void ADD_FORTRAN_PREFIX(p3dfft_ftran_r2cx,(const double*,double*,unsigned char&););
  void ADD_FORTRAN_PREFIX(p3dfft_ftran_r2cy,(const double*,double*,unsigned char&););
}
#endif
#ifdef BK_P3D_SINGLE_PRECISION
extern "C" { 
  void ADD_FORTRAN_PREFIX(p3dfft_setup,(int*,int&,int&,int&,bool&);)
    void ADD_FORTRAN_PREFIX(get_dims,(int*, int*, int*,int&);)
    void ADD_FORTRAN_PREFIX(p3dfft_btran_c2r,(const float*,float*,unsigned char&);)
    void ADD_FORTRAN_PREFIX(p3dfft_btran_c2rx,(const float*,float*,unsigned char&);)
    void ADD_FORTRAN_PREFIX(p3dfft_btran_c2ry,(const float*,float*,unsigned char&);)
    void ADD_FORTRAN_PREFIX(p3dfft_ftran_r2c,(const float*,float*,unsigned char&);)
    void ADD_FORTRAN_PREFIX(p3dfft_ftran_r2cx,(const float*,float*,unsigned char&);)
    void ADD_FORTRAN_PREFIX(p3dfft_ftran_r2cy,(const float*,float*,unsigned char&);)
}
#endif

class FieldUtils;

namespace BK {

  void p3dfft_setup(int* processorGridDims,int nx,int ny,int nz,bool overwrite);

void p3dfft_get_dims(int* lo, int* hi, int* size, int type, int commRank = 0);

template<class T>
void p3dfft_r2c(const T* input,T* output,unsigned char inplace)
{
  ADD_FORTRAN_PREFIX(p3dfft_ftran_r2c,(input,output,inplace));
}
template<class T>
void p3dfft_r2cx(const T* input,T* output,unsigned char inplace)
{
  ADD_FORTRAN_PREFIX(p3dfft_ftran_r2cx,(input,output,inplace));
}
template<class T>
void p3dfft_r2cy(const T* input,T* output,unsigned char inplace)
{
  ADD_FORTRAN_PREFIX(p3dfft_ftran_r2cy,(input,output,inplace));
}
template<class T>
void p3dfft_c2r(const T* input,T* output,unsigned char inplace)
{
  ADD_FORTRAN_PREFIX(p3dfft_btran_c2r,(input,output,inplace));
}
template<class T>
void p3dfft_c2rx(const T* input,T* output,unsigned char inplace)
{
  ADD_FORTRAN_PREFIX(p3dfft_btran_c2rx,(input,output,inplace));
}
template<class T>
void p3dfft_c2ry(const T* input,T* output,unsigned char inplace)
{
  ADD_FORTRAN_PREFIX(p3dfft_btran_c2ry,(input,output,inplace));
}

/* void p3dfft_r2c(const float* input,float* output,unsigned char inplace); */
/* void p3dfft_c2r(const float* input,float* output,unsigned char inplace); */

// class handling fft with p3d. Use as a StackSingleton!
  
class P3dBase :  public FftBase
{
public:
  friend class BK::StackSingleton<P3dBase>;
  friend class ::FieldUtils;

  ~P3dBase();
  
  // initialisiert die FFTW (bestimmt lokale parameter, erzeugt FFTW-Pläne)
  void init(const MpiBase& mpiBase, int globalNx, int ny, int nz, 
	    double lx, double ly, double lz, int* processorGridDims);

  int get_processorGridDims(unsigned int i) {assert(i < 2); return processorGridDims_[i];}

private:

  P3dBase();

  //  int processorGridDims_[2];
  int loIndexR_[3];
  int hiIndexR_[3];
  int sizeR_[3];
  int loIndexC_[3];
  int hiIndexC_[3];
  int sizeC_[3];

};


} // namespace BK;

#endif
