#ifndef bk_mpiBase_hpp
#define bk_mpiBase_hpp

#include <cassert>
#include <list>
#include <string>
#include <algorithm>

#include <mpi.h>

#include "parameter.hpp"
#include "logger.hpp"
#include "functor.hpp"
#include "byteOrdering.hpp"
#include "singleton.hpp"

namespace BK {

class MpiBase
{
public:
  friend class BK::StackSingleton<MpiBase>;
  
  ~MpiBase();
  
  void init(int& argc, char*** argv);

  int get_commRank() const;
  int get_commSize() const;
    
  void barrier();

  template<class type0, class type1>
  void orderedCall(Functor<type0,type1> functor, std::list<int> ordering);

  void finalize() const;
  
  template<class T>
  void mpiAllReduceSum(T* localArray, T* array, unsigned int number);
  template<class T>
  void mpiAllReduceMax(T* localArray, T* array, unsigned int number);
  template<class T>
  void mpiAllReduceMin(T* localArray, T* array, unsigned int number);

  template<class Container>
  void writeContainerOfConstLengthVec(const Container& list, std::string filename) const;

  template<class Container>
  void writeContainerOfConstLengthVecMpiIo(const Container& list, std::string filename) const;

  template<class Container>
  void writeContainerOfStruct(const Container& list, std::string filename) const;

  template<class T>
  static MPI_Datatype mpiType();

private:
  MpiBase();
  MpiBase(const MpiBase&);
  MpiBase& operator=(const MpiBase&);

  int commRank_;
  int commSize_;

  CONSTPARA(bool,initFlag);
};

template<class Container>
void MpiBase::writeContainerOfConstLengthVec(const Container& list, std::string filename) const
{ 
  typedef typename Container::value_type VectorType;
  typedef typename Container::value_type::value_type ElementType;

  int sizeOfT=sizeof(ElementType);
  int sizeOfVector=0;
  int sizeOfList = list.size();
  
  if(sizeOfList != 0)
    sizeOfVector = (*list.begin()).size();

  int sizeOfVectors[commSize_];
  MPI_Gather(&sizeOfVector,1,MPI_INT,&sizeOfVectors[0],1,MPI_INT,0,MPI_COMM_WORLD);
  
  if(commRank_ == 0) {
    int maxSizeOfVector = 0;
    for(int i=0;i<commSize_;i++) 
      if(sizeOfVectors[i] > maxSizeOfVector) maxSizeOfVector = sizeOfVectors[i];
    
    sizeOfVector = maxSizeOfVector;
  }
  
  unsigned int localNumber = list.size();
  unsigned int globalNumbers[commSize_];

  MPI_Gather(&localNumber,1,MPI_UNSIGNED,&globalNumbers[0],1,MPI_UNSIGNED,0,MPI_COMM_WORLD);
  MPI_Bcast(&globalNumbers[0],commSize_,MPI_UNSIGNED,0,MPI_COMM_WORLD);

  unsigned int globalNumber = 0;
  for(int i=0; i<commSize_; i++) 
    globalNumber += globalNumbers[i];
  
  //  if(commRank_ == 0)
  /*   for(int i=0; i<commSize_; i++)  */
  /*     std::cerr << "commRank = " << commRank_ << " globalNumbers i= " << i << "  " << globalNumbers[i] << "  sizeOfVector = " << sizeOfVector << std::endl; */
  
  if(globalNumber == 0) return;

  ElementType* toWrite = 0;
  if(sizeOfList != 0) 
    toWrite = new ElementType[sizeOfList*sizeOfVector];

  unsigned int count = 0;
  for(typename Container::const_iterator listIt = list.begin(); listIt != list.end(); listIt++)
    for(typename VectorType::const_iterator vecIt = listIt->begin(); vecIt != listIt->end(); vecIt++)
      toWrite[count++] = *vecIt;

  if(system_ordering() == BK::bigendian) {
    for(int i=0;i<sizeOfList*sizeOfVector;i++)
      BK::flip(toWrite[i]);
  }

  char file[500];

  sprintf(file,"%s",filename.c_str());

  if(commRank_ == 0) {
    std::ofstream out(filename.c_str(), std::ios::binary | std::ios::out | std::ios::app);
    
    out.write(reinterpret_cast<char*>(toWrite),sizeOfT*sizeOfVector*sizeOfList);

    if(toWrite != 0) {
      delete [] toWrite;
      toWrite = 0;
    }
    
    MPI_Status mpi_status;
    for(int commRank = 1; commRank < commSize_; commRank++) {
      
      toWrite = new ElementType[globalNumbers[commRank]*sizeOfVector];
      MPI_Recv(toWrite,globalNumbers[commRank]*sizeOfVector*sizeOfT,MPI_BYTE,commRank,0,MPI_COMM_WORLD,&mpi_status);
      out.write(reinterpret_cast<char*>(toWrite),sizeOfT*sizeOfVector*globalNumbers[commRank]);
    }
  } else {

    MPI_Send(toWrite, sizeOfVector*sizeOfList*sizeOfT,MPI_BYTE,0,0,MPI_COMM_WORLD);
    
    if(toWrite != 0) {
      delete [] toWrite;
      toWrite = 0;
    }
  }
}

template<class Container>
void MpiBase::writeContainerOfConstLengthVecMpiIo(const Container& list, std::string filename) const
{ 
  typedef typename Container::value_type VectorType;
  typedef typename Container::value_type::value_type ElementType;

  int sizeOfT=sizeof(ElementType);
  int sizeOfVector=0;
  int sizeOfList = list.size();
  
  //  std::cerr << PAR(sizeOfList) << std::endl;

  if(sizeOfList != 0)
    sizeOfVector = (*list.begin()).size();

  int sizeOfVectors[commSize_];
  MPI_Gather(&sizeOfVector,1,MPI_INT,&sizeOfVectors[0],1,MPI_INT,0,MPI_COMM_WORLD);
  
  if(commRank_ == 0) {
    int maxSizeOfVector = 0;
    for(int i=0;i<commSize_;i++) 
      if(sizeOfVectors[i] > maxSizeOfVector) maxSizeOfVector = sizeOfVectors[i];
    
    sizeOfVector = maxSizeOfVector;
  }

  MPI_Bcast(&sizeOfVector,1,MPI_INT,0,MPI_COMM_WORLD);
  //  std::cerr << PAR(sizeOfVector) << std::endl;

  unsigned int localNumber = list.size();
  unsigned int globalNumbers[commSize_];

  MPI_Gather(&localNumber,1,MPI_UNSIGNED,&globalNumbers[0],1,MPI_UNSIGNED,0,MPI_COMM_WORLD);
  MPI_Bcast(&globalNumbers[0],commSize_,MPI_UNSIGNED,0,MPI_COMM_WORLD);

  unsigned int globalNumber = 0;
  for(int i=0; i<commSize_; i++)
    globalNumber += globalNumbers[i];

  
  std::cerr << "commRank = " << commRank_ << " globalNumber = " << globalNumber << std::endl;
  if(globalNumber == 0) return;
  ElementType* toWrite;
  if(sizeOfList != 0) {
    toWrite = new ElementType[sizeOfList*sizeOfVector];

    unsigned int count = 0;
    for(typename Container::const_iterator listIt = list.begin(); listIt != list.end(); listIt++)
      for(typename VectorType::const_iterator vecIt = listIt->begin(); vecIt != listIt->end(); vecIt++)
	toWrite[count++] = *vecIt;

    if(system_ordering() == BK::bigendian) {
      for(int i=0;i<sizeOfList*sizeOfVector;i++)
	BK::flip(toWrite[i]);
    }
  }

  MPI_File mpiFile;

  char file[500];

  sprintf(file,"%s",filename.c_str());
  //  std::cerr << "writing to " << filename << std::endl;
  //  MPI_Info mpiInfo;
  MPI_File_open(MPI_COMM_WORLD, file, MPI_MODE_WRONLY | MPI_MODE_CREATE | MPI_MODE_APPEND, MPI_INFO_NULL, &mpiFile );

  MPI_Offset my_offset = 0;
  for(int i=0;i<commRank_;i++)
    my_offset += globalNumbers[i];
    
  my_offset *= sizeOfT*sizeOfVector;
  
  /* MPI_Offset fileOffset; */
  /* MPI_File_get_position(mpiFile,&fileOffset); */
  MPI_File_seek(mpiFile, my_offset, MPI_SEEK_SET);
  
  MPI_Status status;
  MPI_File_write_all(mpiFile,reinterpret_cast<char*>(&toWrite[0]) , sizeOfT*sizeOfVector*sizeOfList, MPI_BYTE, &status);
  
  MPI_File_close(&mpiFile);

  if(sizeOfList != 0)
    delete [] toWrite;
}

template<class Container>
void MpiBase::writeContainerOfStruct(const Container& list, std::string filename) const
{ 
  typedef typename Container::value_type StructType;

  int sizeOfT=sizeof(StructType);
  int sizeOfList = list.size();
  
  unsigned int localNumber = list.size();
  unsigned int globalNumbers[commSize_];

  MPI_Gather(&localNumber,1,MPI_UNSIGNED,&globalNumbers[0],1,MPI_UNSIGNED,0,MPI_COMM_WORLD);
  MPI_Bcast(&globalNumbers[0],commSize_,MPI_UNSIGNED,0,MPI_COMM_WORLD);

  StructType* toWrite;
  if(sizeOfList != 0) 
    toWrite = new StructType[sizeOfList];

  unsigned int count = 0;
  for(typename Container::const_iterator listIt = list.begin(); listIt != list.end(); listIt++)
    toWrite[count++] = *listIt;

  MPI_File mpiFile;

  char file[500];

  sprintf(file,"%s",filename.c_str());
  //  std::cerr << "writing to " << filename << std::endl;
  MPI_File_open(MPI_COMM_WORLD, file, MPI_MODE_WRONLY | MPI_MODE_CREATE | MPI_MODE_APPEND, MPI_INFO_NULL, &mpiFile );

  MPI_Offset my_offset = 0;
  for(int i=0;i<commRank_;i++)
    my_offset += globalNumbers[i];
    
  my_offset *= sizeOfT;
  
  MPI_Offset fileOffset;
  MPI_File_get_position(mpiFile,&fileOffset);
  MPI_File_seek(mpiFile, my_offset, MPI_SEEK_SET);
  
  MPI_Status status;
  MPI_File_write_all(mpiFile,reinterpret_cast<char*>(&toWrite[0]) , sizeOfT*sizeOfList, MPI_BYTE, &status);
  
  MPI_File_close(&mpiFile);

  if(sizeOfList != 0)
    delete [] toWrite;
}

template <>
inline MPI_Datatype MpiBase::mpiType<int>() { return MPI_INT; }

template <>
inline MPI_Datatype MpiBase::mpiType<long long int>() { return MPI_LONG_LONG_INT; }

template <>
inline MPI_Datatype MpiBase::mpiType<size_t>() { return MPI_AINT; }

template <>
inline MPI_Datatype MpiBase::mpiType<double>() { return MPI_DOUBLE; }

template <>
inline MPI_Datatype MpiBase::mpiType<float>() { return MPI_FLOAT; }

template<class T>
void MpiBase::mpiAllReduceSum(T* localArray, T* array, unsigned int number)
{ 
  auto type = mpiType<T>();
  MPI_Reduce(localArray,array,number,type,MPI_SUM,0,MPI_COMM_WORLD);
  MPI_Bcast(array,number,type,0,MPI_COMM_WORLD);
}

template<class T>
void MpiBase::mpiAllReduceMax(T* localArray, T* array, unsigned int number)
{ 
  auto type = mpiType<T>();
  MPI_Reduce(localArray,array,number,type,MPI_MAX,0,MPI_COMM_WORLD);
  MPI_Bcast(array,number,type,0,MPI_COMM_WORLD);
}

template<class T>
void MpiBase::mpiAllReduceMin(T* localArray, T* array, unsigned int number)
{ 
  auto type = mpiType<T>();
  MPI_Reduce(localArray,array,number,type,MPI_MIN,0,MPI_COMM_WORLD);
  MPI_Bcast(array,number,type,0,MPI_COMM_WORLD);
}

inline int MpiBase::get_commRank() const
{
  assert(initFlag_);
  return commRank_;
}

inline int MpiBase::get_commSize() const
{
  assert(initFlag_);
  return commSize_;
}

template<class type0, class type1>
void MpiBase::orderedCall(Functor<type0,type1> functor, std::list<int> ordering)
{
  std::list<int>::const_iterator myPos = find(ordering.begin(),ordering.end(),commRank_);

  //  std::cerr << commRank_ << "  myPos = " << *myPos << "  " << *(--ordering.end()) << std::endl;

  int signal = commRank_;  
  if(commRank_ == *ordering.begin()) {
    int sRank = *(++ordering.begin());
    //   std::cerr << commRank_ << " ausführen\n"; 
    functor();
    MPI_Send(&signal,1,MPI_INT,sRank,sRank,MPI_COMM_WORLD);
    //   std::cerr << commRank_ << " sende an " << *(++ordering.begin()) << std::endl;
  }
  else {
    if(commRank_ == *(--ordering.end())) {
      MPI_Status status[4];
      int wRank = *(--myPos); ++myPos;
      //      std::cerr << commRank_ << " end-warte auf " << wRank << std::endl;

      MPI_Recv(&signal,1,MPI_INT,wRank,*myPos,MPI_COMM_WORLD,&status[0]);
      //std::cerr << commRank_ << " ausführen\n"; 
      functor();
    }
    else {
      MPI_Status status[4];
      int wRank = *(--myPos); ++myPos;
      int sRank = *(++myPos); --myPos;
      //      std::cerr << commRank_ << " warte auf " << wRank << std::endl;
      MPI_Recv(&signal,1,MPI_INT,wRank,*myPos,MPI_COMM_WORLD,&status[0]);
      //      std::cerr << commRank_ << " ausführen\n"; 
      functor();
      MPI_Send(&signal,1,MPI_INT,sRank,sRank,MPI_COMM_WORLD);
      //      std::cerr << commRank_ << " sende an " << sRank << std::endl;
    }
  }
}


}

#endif
