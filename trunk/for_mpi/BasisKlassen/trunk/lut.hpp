#ifndef lut_hpp
#define lut_hpp

#include <iostream>

#include "functor.hpp"

namespace BK {

class LUT {
 public:
  LUT() {data = NULL;};
    
  LUT( double (*fp)(double), double min, double max, int N = 1000 ) {
    init(fp,min,max,N);
  };
    
  void init( double (*fp)(double), double min, double max, int N = 1000 ) {
    assert(max > min);
    this->N      = N;
    this->scale  = (N-1)/(max-min);
    this->offset = -min;
    this->data   = new double[N];
      
    for(int cnt=0; cnt < N; cnt++)
      this->data[cnt] = fp( cnt/this->scale - this->offset );
  };
    
  ~LUT() {
    if( this->data != NULL)
      
      delete [] this->data;
  };
    
  inline double operator()(double pos) {
    int index = (int) ( this->scale * ( pos + this->offset ) );
    assert( index >= 0 && index < N );
    return this->data[index];
  }
    
 private:
  int N;
  double scale;
  double offset;
  double* data;
    
};

class LutFunctor {
 public:
  LutFunctor() {data = NULL;};
    
  LutFunctor( Functor<double, BK_TYPELIST_1(double)>& functor, double min, double max, int N = 1000 ) {
    init(functor,min,max,N);
  };
    
  void init( Functor<double, BK_TYPELIST_1(double)>& functor, double min, double max, int N = 1000 ) {
    assert(max > min);
    this->N      = N;
    this->scale  = (N-1)/(max-min);
    this->offset = -min;
    this->data   = new double[N];
      
    for(int cnt=0; cnt < N; cnt++)
      this->data[cnt] = functor( cnt/this->scale - this->offset );
  };
    
  ~LutFunctor() {
    
    if(this->data != NULL) {
      delete [] this->data;
    }
  };
    
  inline double operator()(double pos) {
    int index = (int) ( this->scale * ( pos + this->offset ) );
    assert( index >= 0 && index < N );
    return this->data[index];
  }
    
 private:
  int N;
  double scale;
  double offset;
  double* data;
    
};

} // namespace BK

#endif
