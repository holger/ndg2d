#ifndef inheritor_h
#define inheritor_h

namespace BK {

  template<class T>
  struct Inheritor : public T
  {};
  
} // namespace BK

#endif
