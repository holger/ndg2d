#include "utilities.hpp"

#include <cstdlib>

// #undef LOGLEVEL
// #define LOGLEVEL 4

namespace BK {

void copyAsciiFile(std::string filenameIn, std::string filenameOut, bool assertion)
{
  if(filenameIn != filenameOut) {
    
    int length;
    char * buffer;
    
    std::stringstream stream;

    {
      std::ifstream quelldatei(filenameIn.c_str(),std::ios::in);

      if(!quelldatei){
	std::cerr << "ERROR in ReadInitFile::copyAsciiFile: " << filenameIn << " kann nicht geoeffnet werden!\n";
	if(assertion)
	  exit(1);
      }
	
      ERRORLOGL(4,"get length of file ...");
      quelldatei.seekg (0, std::ios::end);
      length = quelldatei.tellg();
	
      ERRORLOGL(4,PAR(length));
      quelldatei.seekg (0, std::ios::beg);
	
      ERRORLOGL(4,"allocate memory ...");
      buffer = new char[length];
	
      ERRORLOGL(4,"read data as a block ...");
	
      quelldatei.read(buffer,length);
	
      quelldatei.close();
    }

    std::ofstream zieldatei(filenameOut.c_str(),std::ios::out);      

    if(!zieldatei){
      std::cerr << "ERROR in ReadInitFile::copyAsciiFile: " << filenameOut << " kann nicht geoeffnet werden!\n";
      if(assertion)
	exit(1);
    }

    zieldatei.write (buffer,length);
      
    zieldatei.close();
    
    delete [] buffer;
  }
}

bool replaceSubString(std::string& str, const std::string& subStr, const std::string& insertStr)
{
  bool success = false;
  
  std::string::size_type subStrPos = str.rfind(subStr);
  
  if(subStrPos != std::string::npos) {
    str.replace(subStrPos,subStr.size(),insertStr);
    success = true;
  }
  
  return success;
}
  
} // namespace BK
