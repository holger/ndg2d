#ifndef sparseMatrix_hpp
#define sparseMatrix_hpp

#include <map>
#include <cassert>

#include "parameter.hpp"
#include "matrixElement.hpp"

namespace BK {

template<class T>
class SparseMatrix
{
public:
  
  typedef T value_type;
  typedef std::map<unsigned int,MatrixElement<T> > MapType;
  
  typedef typename std::map<unsigned int,MatrixElement<T> >::iterator iterator;
  typedef typename std::map<unsigned int,MatrixElement<T> >::const_iterator const_iterator;
  
  iterator begin() {return matrix.begin();}
  const_iterator begin() const {return matrix.begin();}  

  iterator end() {return matrix.end();}
  const_iterator end() const {return matrix.end();}  

  SparseMatrix();
  SparseMatrix(unsigned int nx, unsigned int ny, unsigned int nz);
  
  void init(unsigned int nx, unsigned int ny, unsigned int nz);
  
  inline T& operator()(unsigned int nx, unsigned int ny, unsigned int nz);
  inline T operator()(unsigned int nx, unsigned int ny, unsigned int nz) const;

  inline T& value(unsigned int nx, unsigned int ny, unsigned int nz);
  inline T value(unsigned int nx, unsigned int ny, unsigned int nz) const;

  inline MatrixElement<T>& matrixElement(unsigned int nx, unsigned int ny, unsigned int nz);
  inline MatrixElement<T> matrixElement(unsigned int nx, unsigned int ny, unsigned int nz) const;
  
  inline bool exists(unsigned int nx, unsigned int ny, unsigned int nz) const;

  std::map<unsigned int,MatrixElement<T> > matrix;

  unsigned int size() const {return matrix.size();}

  CONSTPARA(unsigned int, nx);
  CONSTPARA(unsigned int, ny);
  CONSTPARA(unsigned int, nz);

  CONSTPARA(bool,initFlag);
  
  void clear() {
    matrix.clear();
  }
};

template<class T>
std::ostream& operator<<(std::ostream &of, const SparseMatrix<T> &m)
{
  for(typename SparseMatrix<T>::const_iterator it = m.begin(); it != m.end(); it++) {
    of << "(" << (it->second).x << "," << (it->second).y << "," << (it->second).z 
       << ") = " << (it->second).value << std::endl;
  }    
  return of;
}  

template<class T>
SparseMatrix<T>::SparseMatrix()
{
  initFlag_ = false;
}

template<class T>
SparseMatrix<T>::SparseMatrix(unsigned int nx, unsigned int ny, unsigned int nz)
{
  init(nx,ny,nz);
}

template<class T>
void SparseMatrix<T>::init(unsigned int nx, unsigned int ny, unsigned int nz)
{
  nx_ = nx;
  ny_ = ny;
  nz_ = nz;

  initFlag_ = true;
}

template<class T>
inline bool SparseMatrix<T>::exists(unsigned int x, unsigned int y, unsigned int z) const
{
  assert(initFlag_ == true);
  
  if(matrix.find(z + nz_*(y + ny_*x)) != matrix.end()) return true;
  else return false;
}

template<class T>
inline T& SparseMatrix<T>::operator()(unsigned int x, unsigned int y, unsigned int z)
{
  assert(initFlag_ == true);

  MatrixElement<T>& element = matrix[z + nz_*(y + ny_*x)];
  element.x = x;
  element.y = y;
  element.z = z;
  return element.value;
}

template<class T>
inline T SparseMatrix<T>::operator()(unsigned int x, unsigned int y, unsigned int z) const
{
  assert(initFlag_ == true);

  MatrixElement<T>& element = matrix[z + nz_*(y + ny_*x)];
  element.x = x;
  element.y = y;
  element.z = z;
  return element.value;
}

template<class T>
inline T& SparseMatrix<T>::value(unsigned int x, unsigned int y, unsigned int z)
{
  assert(initFlag_ == true);

  MatrixElement<T>& element = matrix[z + nz_*(y + ny_*x)];
  element.x = x;
  element.y = y;
  element.z = z;
  return element.value;
}

template<class T>
inline T SparseMatrix<T>::value(unsigned int x, unsigned int y, unsigned int z) const
{
  assert(initFlag_ == true);

  MatrixElement<T>& element = matrix[z + nz_*(y + ny_*x)];
  element.x = x;
  element.y = y;
  element.z = z;
  return element.value;
}

template<class T>
inline MatrixElement<T>& SparseMatrix<T>::matrixElement(unsigned int x, unsigned int y, unsigned int z)
{
  assert(initFlag_ == true);

  MatrixElement<T>& element = matrix[z + nz_*(y + ny_*x)];
  element.x = x;
  element.y = y;
  element.z = z;
  return element;
}

template<class T>
inline MatrixElement<T> SparseMatrix<T>::matrixElement(unsigned int x, unsigned int y, unsigned int z) const
{
  assert(initFlag_ == true);

  MatrixElement<T>& element = matrix[z + nz_*(y + ny_*x)];
  element.x = x;
  element.y = y;
  element.z = z;
  return element;
}

} // namespace BK

#endif
