#include "p3dBase.hpp"

#include "logger.hpp"

#undef LOGLEVEL
#define LOGLEVEL 0

namespace BK {

void p3dfft_setup(int* processorGridDims,int nx,int ny,int nz)
{
  bool overwrite = 1;
  ADD_FORTRAN_PREFIX(p3dfft_setup,(processorGridDims,nx,ny,nz,overwrite));
}

void p3dfft_get_dims(int* lo, int* hi, int* size, int type, int commRank)
{
  type += 1;
  ADD_FORTRAN_PREFIX(get_dims,(lo,hi,size,type));
  for(int i=0;i<3;i++){
    lo[i] -= 1;
    hi[i] -= 1;
  }

  int tempLo[3];
  int tempHi[3];
  int tempSize[3];
  
  for(int i=0;i<3;i++) {
    tempLo[i] = lo[i];
    tempHi[i] = hi[i];
    tempSize[i] = size[i];
  }

  lo[0] = tempLo[2];
  hi[0] = tempHi[2];
  lo[2] = tempLo[0];
  hi[2] = tempHi[0];
  size[2] = tempSize[0];
  size[0] = tempSize[2];

  ERRORLOGL(1,BK::appendString(commRank,"  ",type,"  ",lo[0],"  ",hi[0],"  ",lo[1],"  ",hi[1],"  ",lo[2],"  "
			       ,hi[2],"  ",size[0],"  ",size[1],"  ",size[2]));
}

// void p3dfft_r2c(const double* input,double* output,unsigned char inplace)
// {
//   ADD_FORTRAN_PREFIX(ftran_r2c(input,output,inplace));
// }

// void p3dfft_c2r(const double* input,double* output,unsigned char inplace)
// {
//   ADD_FORTRAN_PREFIX(btran_c2r(input,output,inplace));
// }

//   unsigned int P3dBase::commRank_;

P3dBase::P3dBase()
{
  ERRORLOGL(1,"P3dBase::P3dBase()");

  initFlag_ = false;
}

P3dBase::~P3dBase()
{
  ERRORLOGL(1,"P3dBase::~P3dBase()");

  ERRORLOGL(4,"P3dBase::~P3dBase()-end");
}

void P3dBase::init(const MpiBase& mpiBase,int globalNx, int ny, int nz, 
		   double lx, double ly, double lz, int* processorGridDims)
{
  ERRORLOGL(1,"P3dBase::init(int nx, int ny, int nz)");

  mpiBase_ = &mpiBase;

  commRank_ = mpiBase.get_commRank();

  processorGridDims_[0] = processorGridDims[0];
  processorGridDims_[1] = processorGridDims[1];

  fftwBlockArray_ = NULL;

  globalNxR_ = globalNx;
  globalNyR_ = ny;
  globalNzR_ = nz;
  
  globalNxC_ = globalNxR_;
  globalNyC_ = globalNyR_;
  globalNzC_ = globalNzR_/2;
  
  ERRORLOG(3,"calling p3dfft_setup ...");
  p3dfft_setup(&processorGridDims_[0],globalNzR_,globalNyR_,globalNxR_);
  
  ERRORLOG(3,"getting local real array dimensions ...");
  p3dfft_get_dims(&loIndexR_[0],&hiIndexR_[0],&sizeR_[0],0,mpiBase.get_commRank());
  ERRORLOG(3,"getting local complex array dimensions ...");
  p3dfft_get_dims(&loIndexC_[0],&hiIndexC_[0],&sizeC_[0],1,mpiBase.get_commRank());
  
  nxR_ = sizeR_[0];
  nyR_ = sizeR_[1];
  nzR_ = sizeR_[2];
  nxC_ = sizeC_[0];
  nyC_ = sizeC_[1];
  nzC_ = sizeC_[2];

  if(nxR_ == 0 || nyR_ == 0 || nzR_ == 0) {
    ERRORLOGL(0,BK::appendString("ERROR in P3dBase::init after FFTW-Plan-Creation: nx = 0 on process ",mpiBase.get_commRank()));

    exit(0);
  }
  
  for(int i=0;i<3;i++) {
    localStartR_[i] = loIndexR_[i];
    localStartC_[i] = loIndexC_[i];
  }

  // P3D normiert nicht!
  scale_ = 1./(double(globalNxR_)*double(globalNyR_)*double(globalNzR_));
  
  fftwBlockArray_ = new FftwBlock[mpiBase.get_commSize()];

  FftBase::init(mpiBase,lx,ly,lz);

  if(mpiBase_->get_commRank() == 0)
    std::cerr << "scale! = " << scale_ << std::endl;

  ERRORLOGL(4,"P3dBase::init(int nx, int ny, int nz)-end");
}

} // namespace BK
