#include "gridPoint.hpp"

namespace BK {

std::ostream& operator<<(std::ostream &of, const GridPoint& gridPoint)
{
  of << "(" << gridPoint.x << "," << gridPoint.y << "," << gridPoint.z << ")";
  return of;
}

} // namespace BK
