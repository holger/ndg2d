#include "fftwBlock.hpp"

namespace BK {

std::ostream& operator<<(std::ostream& of, const FftwBlock& fftwBlock)
{
  of << "commRank = " << fftwBlock.get_commRank() << std::endl
     << "myCoordX = " << fftwBlock.get_myCoordX() << std::endl
     << "myCoordY = " << fftwBlock.get_myCoordY() << std::endl
     << "leftCoord = " << fftwBlock.get_leftCoord() << std::endl
     << "rightCoord = " << fftwBlock.get_rightCoord() << std::endl
     << "bottomCoord = " << fftwBlock.get_bottomCoord() << std::endl
     << "topCoord = " << fftwBlock.get_topCoord() << std::endl
     << "localNyAfterTranspose = " << fftwBlock.get_localNyAfterTranspose() << std::endl
     << "localYStartAfterTranspose = " << fftwBlock.get_localYStartAfterTranspose() << std::endl
     << "totalLocalSize = " << fftwBlock.get_totalLocalSize() << std::endl
     << "localStartR(0) = " << fftwBlock.get_localStartR(0) << std::endl
     << "localStartR(1) = " << fftwBlock.get_localStartR(1) << std::endl
     << "localStartR(2) = " << fftwBlock.get_localStartR(2) << std::endl
     << "localStartC(0) = " << fftwBlock.get_localStartC(0) << std::endl
     << "localStartC(1) = " << fftwBlock.get_localStartC(1) << std::endl
     << "localStartC(2) = " << fftwBlock.get_localStartC(2) << std::endl;

  return of;
}



} // namespace BK
