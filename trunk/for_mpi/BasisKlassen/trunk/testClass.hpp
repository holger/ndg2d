#ifndef testClass_h
#define testClass_h

#include <vector>
#include <iostream>

class Test
{
public:
  Test() { std::cerr << "Test::Test()\n"; }

  ~Test() { std::cerr << "Test::~Test()\n"; }

  Test(const Test& test) { std::cerr << "Test::Test(const& Test&)\n"
				     << "&test = " << &test << std::endl; }

};

#endif
