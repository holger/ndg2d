#ifndef bk_to8dString_hpp
#define bk_to8dString_hpp

#include <sstream>
#include <iomanip>

namespace BK {
  
  template<class Int>
  std::string to8dString(const Int i) {
    static_assert(std::is_integral<Int>::value, "Integral required.");
    
    std::ostringstream fname_stream;
    fname_stream << std::setfill('0') << std::setw(8) << i; 
    return fname_stream.str();
  }
}
#endif
