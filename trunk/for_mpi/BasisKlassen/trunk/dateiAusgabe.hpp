#ifndef dateiAusgabe_hpp
#define dateiAusgabe_hpp

#include <fstream>
#include <string>
#include <sstream>
#include <vector>
#include <cstdlib>
#include <iomanip>

#include "TypeTraits.hpp"
#include "utilities.hpp"
#include "output.hpp"

// ----------------------------------------------------------------------------------------------------------------------------------

namespace BK {

template<class Type0>
void asciiColumnOut(const Type0& c0,std::string filename)
{
  std::ofstream out(filename.c_str());
  for(typename Type0::const_iterator i=c0.begin();i!=c0.end();i++) {
    out << *i << std::endl;
  }
  out.close();  
}

template<class Container>
void containerFileOut(const Container& container, std::string filename)
{
  std::ofstream out(filename.c_str());
  
  out << BK::out(container);

  out.close();
}

template<class StorageType>
void dateiAusgabeXR(const StorageType& outX, const int indexStart, const int indexEnd, const std::string fileName)
{
  std::cerr << "dateiAusgabeXR in Datei:" << fileName << std::endl;
  std::ofstream out;
  out.open(fileName.c_str());  
  for(int i=indexStart;i<=indexEnd;i++){
    out << outX(i) << std::endl;
  }
  out.close();
}

template<class StorageType>
void dateiAusgabeXE(const StorageType& outX, const int indexStart, const int indexEnd, const std::string fileName)
{
  std::cerr << "dateiAusgabeXE in Datei:" << fileName << std::endl;
  std::ofstream out;
  out.open(fileName.c_str());  
  for(int i=indexStart;i<=indexEnd;i++){
    out << outX[i] << std::endl;
  }
  out.close();
}


template<class StorageType>
void dateiAusgabeXYR(const StorageType& outX, const StorageType& outY, int indexStart, int indexEnd, std::string fileName)
{
  std::ofstream out;
  out.open(fileName.c_str());  
  for(int i=indexStart;i<=indexEnd;i++){
    out << outX(i) << "\t" << outY(i) << std::endl;
  }
  out.close();
} 

template<class StorageType>
void dateiAusgabeXYE(const StorageType& outX, const StorageType& outY, int indexStart, int indexEnd, std::string fileName)
{
  std::ofstream out;
  out.open(fileName.c_str());
  for(int i=indexStart;i<=indexEnd;i++){
    //    std::cerr << "i = " << i << outX[i] << "\t" << outY[i] << std::endl;
    out << outX[i] << "\t" << outY[i] << std::endl;
  }
  out.close();
}

template<class StorageType>
void dateiAusgabeXYS(const StorageType& outX, const StorageType& outY, int indexStart, int indexEnd,  int step, std::string fileName)
{
  std::ofstream out;
  out.open(fileName.c_str(), std::ofstream::out | std::ofstream::app);
  for(int i=indexStart;i<=indexEnd;i++){
    out  <<  step << "\t" << int(outX[i]) << "\t" 
         << std::scientific << std::setprecision(8) << outY[i] << std::endl;
  }
  out.close();
}

template<class StorageType>
  void dateiAusgabePXYZ(const StorageType& outP,const StorageType& outX, const StorageType& outY, const StorageType& outZ, int indexStart, int indexEnd, int step, std::string fileName)
{
  std::ofstream out;
  out.open(fileName.c_str(), std::ofstream::out | std::ofstream::app);
  for(int i=indexStart;i<=indexEnd;i++){
    out <<  step << "\t" << int(outP[i]) <<  "\t"
        << std::scientific << std::setprecision(8) << outX[i] <<  "\t" << outY[i] <<  "\t" << outZ[i] << std::endl;
  }
  out.close();
}


template<class T>
void asciiColumnFileOut(std::string filename, const T* out1, const T* out2 = NULL, const T* out3 = NULL, const T* out4 = NULL, 
			const T* out5 = NULL, const T* out6 = NULL, const T* out7 = NULL, const T* out8 = NULL,const T* out9 = NULL,
			const T* out10 = NULL, const T* out11 = NULL, const T* out12 = NULL, const T* out13 = NULL, const T* out14 = NULL)
{
  unsigned int columnNumber = 1;
  std::vector<typename T::const_iterator> itVec;
  
  itVec.push_back(out1->cbegin());
  if(out2 != NULL) {
    columnNumber++;
    itVec.push_back(out2->cbegin());
  }
  if(out3 != NULL) {
    columnNumber++; 
    itVec.push_back(out3->cbegin());
  }
  if(out4 != NULL) {
    columnNumber++;
    itVec.push_back(out4->cbegin());
  }  
  if(out5 != NULL) {
    columnNumber++;
    itVec.push_back(out5->cbegin());
  }
  if(out6 != NULL) {
    columnNumber++; 
    itVec.push_back(out6->cbegin());
  } 
  if(out7 != NULL) {
    columnNumber++;
    itVec.push_back(out7->cbegin());
  }
  if(out8 != NULL) {
    columnNumber++;
    itVec.push_back(out8->cbegin());
  }
  if(out9 != NULL) {
    columnNumber++;
    itVec.push_back(out9->cbegin());
  }
  if(out10 != NULL) {
    columnNumber++;
    itVec.push_back(out10->cbegin());
  }
  if(out11 != NULL) {
    columnNumber++;
    itVec.push_back(out11->cbegin());
  }
  if(out12 != NULL) {
    columnNumber++;
    itVec.push_back(out12->cbegin());
  }
  if(out13 != NULL) {
    columnNumber++;
    itVec.push_back(out13->cbegin());
  }
  if(out14 != NULL) {
    columnNumber++;
    itVec.push_back(out14->cbegin());
  }

  std::ofstream out(filename.c_str());

  for(itVec[0];itVec[0] != out1->cend();) {
    for(typename std::vector<typename T::const_iterator>::iterator i = itVec.begin();i!=itVec.end();i++) {
      out << **i << "  ";
      (*i)++;
    }
    out << std::endl;
  }
  out.close();
}

template<class T>
void asciiColumnFileOut(std::string filename, std::vector<std::vector<T>> const& vecVec, std::ios_base::openmode openMode = std::ios::out)
{
  std::ofstream out(filename.c_str(), openMode);
  
  for(size_t j=0; j<vecVec[0].size(); j++) {
    for(size_t i=0; i<vecVec.size(); i++) {
      if(vecVec[i].size() != vecVec[0].size()) {
	std::cerr << "\nERROR in asciiColumnFileOut: vecVec[" << i  << "].size() = "
		  << vecVec[i].size() << " != " << vecVec[0].size() << "!\n";
	exit(1);
      }
      out << vecVec[i][j] << "\t";
    }
    out << std::endl;
  }
}

template<class StorageType>
void dateiAusgabeXYZR(const StorageType& outX, const StorageType& outY, const StorageType& outZ, int indexStart, int indexEnd, std::string fileName)
{
  std::ofstream out;
  out.open(fileName.c_str());  
  for(int i=indexStart;i<=indexEnd;i++){
    out << outX(i) << "\t" << outY(i) << "\t" << outZ(i) << std::endl;
  }
  out.close();
} 

template<class StorageType>
void dateiAusgabeV2(const StorageType& outV2, int indexStart, int indexEnd, std::string fileName)
{
  std::ofstream out;
  out.open(fileName.c_str());  
  for(int i=indexStart;i<=indexEnd;i++){
    for(int j=indexStart;j<indexEnd;j++){
      out << outV2(i,j) << " ";
    }
    out << outV2(i,indexEnd);
    out << std::endl;
  }
  out.close();
} 

template<class StorageType>
void avsAusgabe2(const StorageType& outV2, const std::string var1Name, int indexStart, int indexEnd, std::string fieldFileName, std::string dirName)
{
  std::ofstream out;
  out.open((appendString(dirName,"/",fieldFileName,".fld")).c_str());
  out << "# AVS" << std::endl
      << "ndim=2" << std::endl
      << "dim1=" << indexEnd-indexStart+1 << std::endl
      << "dim2=" << indexEnd-indexStart+1 << std::endl
      << "nspace=2" << std::endl
      << "veclen=1" << std::endl
      << "data=real" << std::endl
      << "field=uniform" << std::endl << std::endl
      << "variable 1 file=" << appendString(var1Name,".dat") << " filetype=ascii" << std::endl;
  out.close();
  dateiAusgabeV2(outV2, indexStart,indexEnd,appendString(dirName,"/",var1Name,".dat"));
}

template<class StorageType, int vecLength>
void avsAusgabe3D(StorageType** field, int* loIndex, int* hiIndex, std::string filename, std::string dirName)
{
  std::cout << "avsAusgabe3D in Datei:" << dirName << "/" << filename << std::endl;
  std::ofstream out;

  out.open(appendString(dirName,"/",filename,".fld").c_str());
  
  out << "# AVS" << std::endl
      << "ndim=3" << std::endl
      << "dim1=" << hiIndex[0]-loIndex[0]+1 << std::endl
      << "dim2=" << hiIndex[1]-loIndex[1]+1 << std::endl
      << "dim3=" << hiIndex[2]-loIndex[2]+1 << std::endl
      << "nspace=3" << std::endl
      << "veclen=" << vecLength << std::endl
      << "data=float" << std::endl
      << "field=uniform" << std::endl << std::endl;


  for(int i=0;i<vecLength;i++){
    out << "variable " << i+1 << " file=" << appendString(filename,"_",i+1,".dat") << " filetype=binary" << std::endl;
  }

  out.close();

  float tmp;
  int floatSize = sizeof(tmp);

  for(int l=0;l<vecLength;l++){
   
    out.open(appendString(dirName,"/",filename,"_",l+1,".dat").c_str(), std::ios::binary|std::ios::out); 
    for(int k=loIndex[2];k<=hiIndex[2];k++){
      for(int j=loIndex[1];j<=hiIndex[1];j++){
	for(int i=loIndex[0];i<=hiIndex[0];i++){
	  tmp = float((*field[l])(i,j,k));
	  out.write(reinterpret_cast<const char*>(&tmp), floatSize);
	}
      }
    }
    out.close();
  }
}

template<int vecLength>
void avsAusgabe3D(double** field, int* dim, std::string filename, std::string dirName, bool ascii = true)
{
  std::cout << "avsAusgabe3D in Datei:" << dirName << "/" << filename << std::endl;
  std::ofstream out;

  out.open(appendString(dirName,"/",filename,".fld").c_str());
  
  out << "# AVS" << std::endl
      << "ndim=3" << std::endl
      << "dim1=" << dim[0] << std::endl
      << "dim2=" << dim[1] << std::endl
      << "dim3=" << dim[2] << std::endl
      << "nspace=3" << std::endl
      << "veclen=" << vecLength << std::endl
      << "data=float" << std::endl
      << "field=uniform" << std::endl << std::endl;


  if(ascii) {
    for(int i=0;i<vecLength;i++){
      out << "variable " << i+1 << " file=" << appendString(filename,"_",i+1,".dat") << " filetype=ascii" << std::endl;
    }
  }
  else {
    for(int i=0;i<vecLength;i++){
      out << "variable " << i+1 << " file=" << appendString(filename,"_",i+1,".dat") << " filetype=binary" << std::endl;
    }
  }
  out.close();

  float tmp;
  int floatSize = sizeof(tmp);

  for(int l=0;l<vecLength;l++){
    if(ascii) {
      out.open(appendString(dirName,"/",filename,"_",l+1,".dat").c_str()); 

      for(int i=0;i<dim[0];i++){       
	for(int j=0;j<dim[1];j++){
	  for(int k=0;k<dim[2];k++){
	    out << (field[l])[k+dim[2]*(j+dim[1]*i)] << "  ";
	  }
	  out << std::endl;
	}
	out << std::endl;
      }
      out.close();
    }
    else {
      out.open(appendString(dirName,"/",filename,"_",l+1,".dat").c_str(), std::ios::binary|std::ios::out); 

      for(int i=0;i<dim[0];i++){
	for(int j=0;j<dim[1];j++){
	  for(int k=0;k<dim[2];k++){
	    tmp = float((field[l])[k+dim[2]*(j+dim[1]*i)]);
	    out.write(reinterpret_cast<const char*>(&tmp), floatSize);
	  }
	}
      }
      out.close();
    }
  }
}

template<template<class Type> class VectorClass, class ParticleType>
  void avsAusgabeParticle(const VectorClass<ParticleType>& particleArray, int particleNumber, std::string filename, std::string dirName)
{
  std::cerr << "avsAusgabeParticle in Datei:" << dirName << "/" << filename << std::endl;
  std::string dir = dirName;
  std::ofstream out;
  out.open(appendString(dir,"/",filename,".fld").c_str());  
  out << "# AVS" << std::endl
      << "ndim=3" << std::endl
      << "dim1=1\n" 
      << "dim2=1\n"
      << "dim3=" << particleNumber << std::endl
      << "nspace=3" << std::endl
      << "veclen=1" << std::endl
      << "data=float" << std::endl
      << "field=irregular" << std::endl << std::endl
      << "variable 1 file=" << appendString(filename,".dat") << " filetype=ascii offset=3 stride=4" << std::endl
      << "coord 1 file=" << appendString(filename,".dat") << " filetype=ascii offset=0 stride=4" << std::endl
      << "coord 2 file=" << appendString(filename,".dat") << " filetype=ascii offset=1 stride=4" << std::endl
      << "coord 3 file=" << appendString(filename,".dat") << " filetype=ascii offset=2 stride=4" << std::endl;
  out.close();
  out.open(appendString(dir,"/",filename,".dat").c_str()); 
  for(int i=0; i<particleNumber;i++){
    out << particleArray[i].space[0] << "\t" << particleArray[i].space[1] << "\t" << particleArray[i].space[2] << "\t" << 1 << std::endl;
  }
  out.close();
}

template<template<class Type> class VectorClass, class ParticleType>
void writeParticles(VectorClass<ParticleType>& particleArray, int particleNumber, std::string filename)
{
  std::cerr << "writeParticles in Datei:" << filename << std::endl;
  
  std::ofstream out(filename.c_str(), std::ios::binary|std::ios::out);
  
  int floatSize = sizeof(double);
 
  for(int i=0; i<particleNumber;i++){
    out.write(reinterpret_cast<char*>(&(particleArray[i].space[0])),floatSize);
    out.write(reinterpret_cast<char*>(&(particleArray[i].space[1])),floatSize);
    out.write(reinterpret_cast<char*>(&(particleArray[i].space[2])),floatSize);
  }
  
  out.close();    
}

template<template<class Type> class VectorClass, class ParticleType>
void readParticles(VectorClass<ParticleType>& particleArray, int particleNumber, std::string filename)
{
  std::cerr << "readParticles aus Datei:" << filename << std::endl;
  
  std::ifstream quelldatei(filename.c_str(), std::ios::binary|std::ios::out);

  if(!quelldatei){
    std::cerr << filename << " cannot be opened for reading!\n";
    exit(1);
  }    

  int floatSize = sizeof(double); 

  for(int i=0; i<particleNumber;i++){
    quelldatei.read(reinterpret_cast<char*>(&(particleArray[i].space[0])),floatSize);
    quelldatei.read(reinterpret_cast<char*>(&(particleArray[i].space[1])),floatSize);
    quelldatei.read(reinterpret_cast<char*>(&(particleArray[i].space[2])),floatSize);
  }
}

template<template<class Type> class VectorClass, class ParticleType>
void avsAusgabeParticleVelo(const VectorClass<ParticleType>& particleArray, int particleNumber, std::string filename, std::string dirName)
{
  std::cout << "avsAusgabeParticle in Datei:" << dirName << "/" << filename << std::endl;
  std::string dir = dirName;
  std::ofstream out;
  out.open(appendString(dir,"/",filename,".fld").c_str());  
  out << "# AVS" << std::endl
      << "ndim=3" << std::endl
      << "dim1=1\n" 
      << "dim2=1\n"
      << "dim3=" << particleNumber << std::endl
      << "nspace=3" << std::endl
      << "veclen=1" << std::endl
      << "data=float" << std::endl
      << "field=irregular" << std::endl << std::endl
      << "variable 1 file=" << appendString(filename,".dat") << " filetype=ascii offset=3 stride=4" << std::endl
      << "coord 1 file=" << appendString(filename,".dat") << " filetype=ascii offset=0 stride=4" << std::endl
      << "coord 2 file=" << appendString(filename,".dat") << " filetype=ascii offset=1 stride=4" << std::endl
      << "coord 3 file=" << appendString(filename,".dat") << " filetype=ascii offset=2 stride=4" << std::endl;
  out.close();
  out.open(appendString(dir,"/",filename,".dat").c_str()); 
  for(int i=0; i<particleNumber;i++){
    out << particleArray[i].velo[0] << "\t" << particleArray[i].velo[1] << "\t" << particleArray[i].velo[2] << "\t" << 1 << std::endl;
  }
  out.close();
}

template<template<class Type> class VectorClass, class ParticleType>
void avsAusgabeParticleInitVelo(const VectorClass<ParticleType>& particleArray, int particleNumber, std::string filename, std::string dirName)
{
  std::cout << "avsAusgabeParticle in Datei:" << dirName << "/" << filename << std::endl;
  std::string dir = dirName;
  std::ofstream out;
  out.open(appendString(dir,"/",filename,".fld").c_str());  
  out << "# AVS" << std::endl
      << "ndim=3" << std::endl
      << "dim1=1\n" 
      << "dim2=1\n"
      << "dim3=" << particleNumber << std::endl
      << "nspace=3" << std::endl
      << "veclen=1" << std::endl
      << "data=float" << std::endl
      << "field=irregular" << std::endl << std::endl
      << "variable 1 file=" << appendString(filename,".dat") << " filetype=ascii offset=3 stride=4" << std::endl
      << "coord 1 file=" << appendString(filename,".dat") << " filetype=ascii offset=0 stride=4" << std::endl
      << "coord 2 file=" << appendString(filename,".dat") << " filetype=ascii offset=1 stride=4" << std::endl
      << "coord 3 file=" << appendString(filename,".dat") << " filetype=ascii offset=2 stride=4" << std::endl;
  out.close();
  out.open(appendString(dir,"/",filename,".dat").c_str()); 
  for(int i=0; i<particleNumber;i++){
    out << particleArray[i].initVelo[0] << "\t" << particleArray[i].initVelo[1] << "\t" << particleArray[i].initVelo[2] << "\t" << 1 << std::endl;
  }
  out.close();
}

template<class StorageType, int vecLength>
void avsAusgabeAscii3D(StorageType** field, int* loIndex, int* hiIndex, std::string filename, std::string dirName)
{
  std::cout << "avsAusgabe3D in Datei:" << dirName << "/" << filename << std::endl;
  std::ofstream out;

  out.open(appendString(dirName,"/",filename,".fld").c_str());
  
  out << "# AVS" << std::endl
      << "ndim=3" << std::endl
      << "dim1=" << hiIndex[0]-loIndex[0]+1 << std::endl
      << "dim2=" << hiIndex[1]-loIndex[1]+1 << std::endl
      << "dim3=" << hiIndex[2]-loIndex[2]+1 << std::endl
      << "nspace=3" << std::endl
      << "veclen=" << vecLength << std::endl
      << "data=float" << std::endl
      << "field=uniform" << std::endl << std::endl;


  for(int i=0;i<vecLength;i++){
    out << "variable " << i+1 << " file=" << appendString(filename,"_",i+1,".dat") << " filetype=ascii" << std::endl;
  }

  out.close();

  for(int l=0;l<vecLength;l++){
   
    out.open(appendString(dirName,"/",filename,"_",l+1,".dat").c_str()); 

    for(int k=loIndex[2];k<=hiIndex[2];k++){
      for(int j=loIndex[1];j<=hiIndex[1];j++){
	for(int i=loIndex[0];i<=hiIndex[0];i++){ 
	  out << (*field[l])(i,j,k) << "  ";
	}
	out << std::endl;
      }
      out << std::endl;
    }
    out.close();

  }
}

template<class StorageType, int vecLength>
void avsAusgabe3DAppend(StorageType** field, int* loIndex, int* hiIndex, int timeStep, std::string filename, std::string dirName)
{
  std::cout << "avsAusgabe3D in Datei:" << dirName << "/" << filename << std::endl;
  std::ofstream out;

  out.open(appendString(dirName,"/",filename,".fld").c_str());
  
  out << "# AVS" << std::endl
      << "ndim=4" << std::endl
      << "dim1=" << hiIndex[0]-loIndex[0]+1 << std::endl
      << "dim2=" << hiIndex[1]-loIndex[1]+1 << std::endl
      << "dim3=" << hiIndex[2]-loIndex[2]+1 << std::endl
      << "dim4=" << timeStep << std::endl
      << "nspace=4" << std::endl
      << "veclen=" << vecLength << std::endl
      << "data=float" << std::endl
      << "field=uniform" << std::endl << std::endl;


  for(int i=0;i<vecLength;i++){
    out << "variable " << i+1 << " file=" << appendString(filename,"_",i+1,".dat") << " filetype=binary" << std::endl;
  }

  out.close();

  float tmp;
  int floatSize = sizeof(tmp);

  for(int l=0;l<vecLength;l++){
   
    out.open(appendString(dirName,"/",filename,"_",l+1,".dat").c_str(), std::ios::binary|std::ios::out|std::ios::app); 
    for(int k=loIndex[2];k<=hiIndex[2];k++){
      for(int j=loIndex[1];j<=hiIndex[1];j++){
	for(int i=loIndex[0];i<=hiIndex[0];i++){
	  tmp = float((*field[l])(i,j,k));
	  out.write(reinterpret_cast<const char*>(&tmp), floatSize);
	}
      }
    }
    out.close();
  }
}

} // namespace BK;

#endif
