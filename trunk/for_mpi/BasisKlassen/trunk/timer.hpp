#ifndef TIMER_HPP
#define TIMER_HPP

#include <ctime>
#include <string>
#include <iostream>
#include <iomanip>
#include <map>

#include "singleton.hpp"
#include "utilities.hpp"
#include "logger.hpp"

namespace BK {

class TimerManagement;
class Timer;

// globale Referenz auf Logger-Singleton------------------------
#ifndef timerM

#define timerM BK::Singleton<BK::TimerManagement>::instance()

#endif

// Timer-Klasse zur Laufzeitbestimmung--------------------------
class Timer
{
 public:
  // Default Konstr. name=noname
  Timer(); 

  // Konstruktor: weist Timer Namen zu
  Timer(std::string name_); 

  // copy-Konstruktor
  Timer(const Timer& t);

  // Destruktor
  ~Timer();
  
  // startet Timer
  void start();

  // startet Timer und setzt accumulatedTime_=0
  void restart();

  // stopt Timer
  void stop();

  // Zeitdauer seit start()
  double elapsedTime();
  
  // Zeitdauer insgesamt (seit 1.Start)
  double check();

  // schreibt accumulatedTime ins default-log-file
  void logAccumulatedTime() const;

  // schreibt accumulatedTime ins log-file logFilename_
  void logAccumulatedTime(const std::string logFilename) const;

  // gibt accumulatedTime zur�ck
  double accumulatedTime() const {
    return accumulatedTime_;
  }

  // Name des Timers
  std::string name;

  // Name des log-files
  std::string logFilename;
  
 protected:
  // Timer eingeschaltet
  bool running;
  
  // Zeitdauer seit 1.Start oder restart
  double accumulatedTime_;

  // Zeitpunkt vom letzten start()
  clock_t startTime;
  
  void operator=(const Timer& t);
};


// Klasse zur Verwaltung mehrerer Timer durch strings-----------
class TimerManagement
{
 public:

  TimerManagement();

  // gibt Referenz auf Timer zu string name zur�ck
  Timer& get(const std::string name);

  // schreibt accumulatedTime aller Timer ins Logfile logFilename_
  void logEveryAccumulatedTime(const std::string logFilename_);

  // schreibt accumulatedTime aller Timer ins default-Logfile
  void logEveryAccumulatedTime();

  // schreibt gesamte accumulatedTime ins log-file logFilename_ und gibt sie zur�ck
  double logAccumulatedTimeSumme(const std::string logFilename_);

 private:

  // Map der Timer mit Key string
  std::map<std::string, Timer> timerMap;

  // Iterator zur obigen Map
  std::map<std::string, Timer>::iterator itTimerMap;

  // Name des log-files
  std::string logFilename;
};

} // namespace BK

#endif
