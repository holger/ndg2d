#ifndef vector_class_hpp
#define vector_class_hpp

#include <iostream>
#include <assert.h>
#include <cmath>
#include <vector>

#include "range.hpp"
#include "iterator.hpp"
#include "logger.hpp"

// #undef LOGLEVEL
// #define LOGLEVEL 0

namespace BK {

template<class TYPE>
class Vector {
 public:

  typedef TYPE value_type;
  typedef Iterator<TYPE> iterator;
  typedef ConstIterator<TYPE> const_iterator;

  iterator begin() const {return iterator(&data[0]);}

  iterator end() const {return iterator(&data[length]);}

  // default-Konstruktor
  Vector();
  
  // copy-Konstruktor
  Vector(const Vector<TYPE>& v);     
  
  Vector(const std::vector<TYPE>& v);

  // Konstruktor: erzeugt Vector<TYPE> mit L�nge _length und Indizes von 0 bis _length-1
  Vector(int _length);                 

  Vector(TYPE e0,TYPE e1);
  Vector(TYPE e0,TYPE e1,TYPE e2);
  Vector(TYPE e0,TYPE e1,TYPE e2,TYPE e3);
  Vector(TYPE e0,TYPE e1,TYPE e2,TYPE e3,TYPE e4);
  Vector(TYPE e0,TYPE e1,TYPE e2,TYPE e3,TYPE e4,TYPE e5);  

  // Konstruktor: erzeugt Vector<TYPE> mit Indizes von _loIndex bis _hiIndex
  Vector(Range range);
  
  // Destruktor
  ~Vector();   
 
  // l�scht alten Vector und erzeugt neuen mit L�nge _length und Indizes von 0 bis _length-1
  void resize(int _length);

  // l�scht alten Vector und erzeugt neuen mit Indizes von loIndex bis hiIndex
  void resize(Range range);

  // Index-Operator[] zum auslesen des TYPE
  const TYPE& operator[] (int i) const;           

  // Index-Operator[] zum beschreiben mit TYPE
  TYPE &operator[] (int i);  

  // Index-Operator[] zum auslesen des TYPE
  const TYPE& operator() (int i) const;           

  // Index-Operator[] zum beschreiben mit TYPE
  TYPE& operator() (int i);  

  // Kopier-Operator= kopiert elementweise
  Vector &operator=(const Vector<TYPE>& v);     

  // Zuweisungsoperator f�r zahl: v = 3 bedeutet v[i] = 3
  Vector &operator=(TYPE zahl); 

  // Additions-Zuweisungsoperator f�r Vector: w += v 
  Vector &operator+=(const Vector<TYPE>& v);

  // Subtraktions-Zuweisungsoperator f�r Vector: w -= v 
  Vector &operator-=(const Vector<TYPE>& v);

  Vector& operator++()
  {
    for (int i = loIndex; i <= hiIndex; i++)
      ++dataFast[i];

    return *this;
  }

  Vector operator++(int)
  {
    Vector temp = *this;
    ++*this;
    return temp;
  }

  // Multiplikations-Zuweisungsoperator f�r Vector: v *= 3. 
  template<class TYPE2>
    Vector &operator*=(TYPE2 zahl);

  // Divisions-Zuweisungsoperator f�r Vector: v /= 3. 
  Vector &operator/=(double v);
  Vector &operator/=(float v);
  Vector &operator/=(int v);

  // Multiplikations-Operator mit Skalar: v*3.
  template<class TYPE2>
  Vector operator*(TYPE2 zahl);

  // Division-Operator mit Skalar: v/3.
  Vector operator/(double zahl);
  Vector operator/(float zahl);
  Vector operator/(int zahl);

  // Additions-Operator f�r zwei Vectoren: v+w
  Vector operator+(const Vector& v);

  // Subtraktions-Operator f�r zwei Vectoren: v-w
  Vector operator-(const Vector& v);

  // bestimmt das maximale Element
  const TYPE getMaxValue() const;            

  // bestimmt das minimale Element
  const TYPE getMinValue() const;            

  // gibt die Anzahl der Elemente des Vectors zur�ck
  unsigned int getSize() const { return length; }
  unsigned int size() const { return length; }

  // Euklidisches Betragsquadrat
  TYPE sqr() const;
  
  // Euklidische Norm
  TYPE norm() const;

  // gibt einen Pointer auf das Element mit Index index zur�ck (z.b. vec.data_pointer[j] = &zahl)
  TYPE* dataPointer(int index); 

  // gibt einen Pointer auf data-Array zur�ck
  TYPE* dataPointer(); 
  
  // kleinster bzw. groesster Index
  int loIndex;
  int hiIndex;

 private:

  // kleinster Index, groester Index, Anzahl der Elemente
  unsigned int length;

  // Pointer auf das zu erzeugende Array
  TYPE *data;   

  TYPE* dataFast;
};

template<class TYPE>
std::ostream& operator<<(std::ostream &of, const Vector<TYPE> &v);   

// //Multiplikations-Operatoren mit Skalar: 3.*v
// template<class TYPE, class TYPE2>
// Vector<TYPE> operator*(TYPE2 zahl, const Vector<TYPE>& v);

// //Multiplikations-Operatoren mit Skalar: v*3
// template<class TYPE, class TYPE2>
// Vector<TYPE> operator*(const Vector<TYPE>& v, TYPE2 zahl);

// // Additions-Operator f�r zwei Vectoren: v+w
// template<class TYPE>
// Vector<TYPE> operator+(const Vector<TYPE>& v, const Vector<TYPE>& w);

// // Subtraktions-Operator f�r zwei Vectoren: v-w
// template<class TYPE>
// Vector<TYPE> operator-(const Vector<TYPE>& v, const Vector<TYPE>& w);

// -------------------------------------------------------------------------
// definitions
// -------------------------------------------------------------------------

template<class TYPE>
Vector<TYPE>::Vector()                    
{
  ERRORLOGL(3,"Vector<TYPE>::Vector()");

  length = 0;
  loIndex = 0;
  hiIndex = 0;
  data = NULL;
  dataFast = NULL;
}

template<class TYPE>
Vector<TYPE>::Vector(int _length)                 
{  
  length=_length;
  loIndex = 0;
  hiIndex = length-1;
  data = new TYPE[length];
  dataFast = data;
}

template<class TYPE>
Vector<TYPE>::Vector(Range range)
{
  data=NULL;
  dataFast=NULL;
  resize(range);
}

template<class TYPE>
Vector<TYPE>::Vector(TYPE e0,TYPE e1)
{
  data = NULL;
  dataFast = NULL;
  resize(2);
  dataFast[0] = e0;
  dataFast[1] = e1;

}

template<class TYPE>
Vector<TYPE>::Vector(TYPE e0,TYPE e1,TYPE e2)
{
  data = NULL;
  dataFast = NULL;
  resize(3);
  dataFast[0] = e0;
  dataFast[1] = e1;
  dataFast[2] = e2;
}

template<class TYPE>
Vector<TYPE>::Vector(TYPE e0,TYPE e1,TYPE e2,TYPE e3)
{
  data = NULL;
  dataFast = NULL;
  resize(4);
  dataFast[0] = e0;
  dataFast[1] = e1;
  dataFast[2] = e2;
  dataFast[3] = e3;
}

template<class TYPE>
Vector<TYPE>::Vector(TYPE e0,TYPE e1,TYPE e2,TYPE e3,TYPE e4)
{
  data = NULL;
  dataFast = NULL;
  resize(5);
  dataFast[0] = e0;
  dataFast[1] = e1;
  dataFast[2] = e2;
  dataFast[3] = e3;
  dataFast[4] = e4;
}

template<class TYPE>
Vector<TYPE>::Vector(TYPE e0,TYPE e1,TYPE e2,TYPE e3,TYPE e4,TYPE e5)
{
  data = NULL;
  dataFast = NULL;
  resize(6);
  dataFast[0] = e0;
  dataFast[1] = e1;
  dataFast[2] = e2;
  dataFast[3] = e3;
  dataFast[4] = e4;
  dataFast[5] = e5;
}

template<class TYPE>
void Vector<TYPE>::resize(int size)
{  
  //assert(data == NULL);
  delete [] data;
  loIndex = 0;
  hiIndex = size-1;
  length=hiIndex-loIndex+1;
  data = new TYPE[length];
  dataFast = data-loIndex;
}

template<class TYPE>
void Vector<TYPE>::resize(Range range)            
{  
  //  assert(data == NULL);
  delete [] data;
  loIndex = range.index[0];
  hiIndex = range.index[1];
  length=hiIndex-loIndex+1;
  data = new TYPE[length];
  dataFast = data-loIndex;
}

template<class TYPE>
Vector<TYPE>::~Vector()
{
  delete [] data;
}

template<class TYPE>
Vector<TYPE>::Vector(const Vector<TYPE>& v)                       
{
  length = v.length;
  loIndex = v.loIndex;
  hiIndex = v.hiIndex;
  data = new TYPE[length];
  dataFast = data-loIndex;
  for (int i = loIndex; i <= hiIndex; i++)
    dataFast[i] = v[i];
}

template<class TYPE>
Vector<TYPE>::Vector(const std::vector<TYPE>& v)                       
{
  length = v.size();
  loIndex = 0;
  hiIndex = v.size()-1;
  data = new TYPE[length];
  dataFast = data-loIndex;
  for (int i = loIndex; i <= hiIndex; i++)
    dataFast[i] = v[i];
}

template<class TYPE>
inline const TYPE& Vector<TYPE>::operator[] (int i) const        
{
  assert( i >= loIndex && i <= hiIndex);
  return dataFast[i];
}

template<class TYPE>
inline TYPE& Vector<TYPE>::operator[] (int i)            
{
  assert( i >= loIndex && i <= hiIndex);
  return dataFast[i];
}

template<class TYPE>
inline const TYPE& Vector<TYPE>::operator() (int i) const        
{
  assert( i >= loIndex && i <= hiIndex);
  return dataFast[i];
}

template<class TYPE>
inline TYPE& Vector<TYPE>::operator() (int i)            
{
  assert( i >= loIndex && i <= hiIndex);
  return dataFast[i];
}

template<class TYPE>
Vector<TYPE> &Vector<TYPE>::operator=(const Vector<TYPE>& v)
{
  assert(loIndex == v.loIndex && hiIndex == v.hiIndex);
  for (int i = loIndex; i <= hiIndex; i++)
    dataFast[i] = v[i];
  return *this;
}

template<class TYPE>
Vector<TYPE> &Vector<TYPE>::operator=(TYPE zahl)
{
  for (unsigned int i = 0; i < length; i++)
    data[i] = zahl;
  return *this;
}

template<class TYPE>
Vector<TYPE> &Vector<TYPE>::operator+=(const Vector<TYPE>& v)
{
  assert(loIndex == v.loIndex && hiIndex == v.hiIndex);
  for (int i = loIndex; i <= hiIndex; i++)
    dataFast[i] += v[i];
  return *this;
}

template<class TYPE>
Vector<TYPE>& Vector<TYPE>::operator-=(const Vector<TYPE>& v)
{
  assert(loIndex == v.loIndex && hiIndex == v.hiIndex);
  for (int i = loIndex; i <= hiIndex; i++)
    data[i-loIndex] -= v[i];
  return *this;
}

template<class TYPE>
Vector<TYPE> Vector<TYPE>::operator-(const Vector<TYPE>& v)
{
  Vector<TYPE> temp = (*this);
  temp -= v;

  return temp;
}

template<class TYPE>
Vector<TYPE> Vector<TYPE>::operator+(const Vector<TYPE>& v)
{
  Vector<TYPE> temp = (*this);
  temp += v;

  return temp;
}

template<class TYPE>
template<class TYPE2>
Vector<TYPE> &Vector<TYPE>::operator*=(TYPE2 zahl)
{
  for (unsigned int i = 0; i < length; i++)
    data[i] *= zahl;
  return *this;
}

template<class TYPE>
template<class TYPE2>
Vector<TYPE> Vector<TYPE>::operator*(TYPE2 zahl)
{
  Vector<TYPE> temp = (*this);
  temp *= zahl;

  return temp;
}

template<class TYPE>
Vector<TYPE> &Vector<TYPE>::operator/=(double zahl)
{
  for (int i = loIndex; i <= hiIndex; i++)
    dataFast[i] /= zahl;
  return *this;
}

template<class TYPE>
Vector<TYPE> &Vector<TYPE>::operator/=(float zahl)
{
  for (int i = loIndex; i <= hiIndex; i++)
    dataFast[i] /= zahl;
  return *this;
}

template<class TYPE>
Vector<TYPE> &Vector<TYPE>::operator/=(int zahl)
{
  for (int i = loIndex; i <= hiIndex; i++)
    dataFast[i] /= zahl;
  return *this;
}

template<class TYPE>
Vector<TYPE> Vector<TYPE>::operator/(double zahl)
{
  Vector<TYPE> temp = (*this);
  temp /= zahl;

  return temp;
}

template<class TYPE>
Vector<TYPE> Vector<TYPE>::operator/(float zahl)
{
  Vector<TYPE> temp = (*this);
  temp /= zahl;

  return temp;
}

template<class TYPE>
Vector<TYPE> Vector<TYPE>::operator/(int zahl)
{
  Vector<TYPE> temp = (*this);
  temp /= zahl;

  return temp;
}

template<class TYPE>
std::ostream& operator<<(std::ostream &of, const Vector<TYPE> &v)   
{       
  of << "(";
  for (int i = v.loIndex; i < v.hiIndex; i++)
    of << v[i] << ",";
  of << v[v.hiIndex] << ")";

  return of;
}

template<class TYPE>
TYPE* Vector<TYPE>::dataPointer(int index)
{
  return &data[index];
}

template<class TYPE>
TYPE* Vector<TYPE>::dataPointer()
{
  return data;
}

template<class TYPE>
const TYPE Vector<TYPE>::getMaxValue() const
{
  TYPE max_value;
  max_value = data[0];
  for(unsigned int i=1;i< length;i++){
    if(max_value < data[i]){
      max_value = data[i];
    }
    else;
  }
  return max_value;
}

template<class TYPE>
const TYPE Vector<TYPE>::getMinValue() const
{
  TYPE min_value;
  min_value = data[0];
  for(unsigned int i=1;i<length;i++){
    if(min_value > data[i]){
      min_value = data[i];
    }
    else;
  }
  return min_value;
}

template<class TYPE>
TYPE Vector<TYPE>::sqr() const
{
  TYPE tmp=0;
  for (unsigned int i = 0; i < length; i++){
    tmp += data[i]*data[i];
  }
  return tmp;
}

template<class TYPE>
TYPE Vector<TYPE>::norm() const
{
  TYPE tmp=0;
  for (unsigned int i = 0; i < length; i++){
    tmp += data[i]*data[i];
  }
  return sqrt(tmp);
}

} // namespace BK

#endif
