#ifndef integralTrapez_h
#define integralTrapez_h

namespace BK {

template<class T>
T integralTrapez(const T* fArray, const T* xArray, int n)
{
  T sum = 0;
  
  for(int i=0;i<n-1;i++) {
    sum += 0.5*(fArray[i] + fArray[i+1])*(xArray[i+1] - xArray[i]);
  }
  return sum;  
}

template<class T>
T integralEuler(const T* fArray, const T* xArray, int n)
{
  T sum = 0;
  
  for(int i=0;i<n-1;i++) {
    sum += fArray[i]*(xArray[i+1] - xArray[i]);
  }
  return sum;  
}

template<class T>
void integral(T* result, const T* fArray, const T* xArray, int n)
{
  for(int i=0;i<n;i++) {
    result[i] = integralTrapez(fArray,xArray,i+1);
  }
}

} // namespace BK

#endif
