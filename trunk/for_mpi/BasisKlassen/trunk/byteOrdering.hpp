#ifndef bk_byteOrdering_h
#define bk_byteOrdering_h

namespace BK {

void flip4(char b[]);
void flip8(char b[]);
void flip(float& u);
void flip(int& u);
void flip(double& u);

enum byteordering_type { bigendian, littleendian };

byteordering_type system_ordering();

} // namespace BK

#endif
