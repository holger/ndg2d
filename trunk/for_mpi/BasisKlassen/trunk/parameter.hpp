#ifndef bk_parameter_h
#define bk_parameter_h

#include "assert.hpp"
#include "utilities.hpp"

#define STR_EXPAND(tok) #tok
#define STR(tok) STR_EXPAND(tok)
 
#define PARAMETER(TYPE,name)                                          \
        private: TYPE name##_;                                        \
	public:  void set_##name(TYPE value) {name##_ = value;}       \
	public:  TYPE& set_##name() {return name##_;}                 \
        public:  const TYPE& get_##name() const {return name##_;}

#define LOCKPARAM(TYPE,name)                                                           \
        private: TYPE name##_;                                                         \
	public:  void set_##name(TYPE value) {assert(lock_ == 0); name##_ = value;}    \
	public:  TYPE& set_##name() {assert(lock_ == 0); return name##_;}              \
        public:  const TYPE& get_##name() const {return name##_;}

#define CONSTPARA(TYPE,name)                                               \
        private: TYPE name##_;                                             \
        public:  const TYPE& get_##name() const {return name##_;}          \

// Compile-time constant.
#define CONSTEXPRPARA(TYPE,name,value)                                     \
        private: constexpr static TYPE name##_ = value;                    \
        public:  constexpr static TYPE get_##name() { return name##_; }

#define PROCONPAR(TYPE,name)                                               \
        protected: TYPE name##_;                                           \
        public:  const TYPE& get_##name() const {return name##_;}          \

#define PROTEPARA(TYPE,name)                                               \
        protected: TYPE name##_;                                           \
	public:  void set_##name(TYPE value) {name##_ = value;}            \
	public:  TYPE& set_##name() {return name##_;}                      \
        public:  const TYPE& get_##name() const {return name##_;}          \

#define CONSTAPAR(TYPE,name)                                               \
        private: static TYPE name##_;                                      \
        public:  static const TYPE& get_##name() {return name##_;}         \

#define PROSTAPAR(TYPE,name)                                               \
        protected: static TYPE name##_;					   \
        public:  static const TYPE& get_##name() {return name##_;}         \

#define CONSTARRA(TYPE,name,length)                                        \
        private: TYPE name##_[length];				           \
	public: const TYPE& get_##name(int i) const {                      \
	  ASSERT(i >= 0 && i < length, BK::appendString("ERROR in CONSTARRA(",STR(TYPE),",",STR(name),",",length,")::get_",STR(name),"(int i) const:"," i = ",i," < 0 || > length-1 = ",length-1)); \
	  return name##_[i];                                               \
	}                                                                  \

#endif
