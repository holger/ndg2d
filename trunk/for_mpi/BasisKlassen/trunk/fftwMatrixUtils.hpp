#ifndef fftwMatrixUtils_hpp
#define fftwMatrixUtils_hpp

#include <cassert>

//#include "fftwMatrix.hpp"
#include "rand.hpp"
#include "fftw_complex.hpp"
#include "dataType.hpp"
#include "fftBase.hpp"
#include "logger.hpp"

namespace BK {

class NoFilter;
class CesaroFilter;
class LanczosFilter;
class RaisedCosineFilter;
class SharpenedRaisedCosineFilter;
  
// Rotation im Fourierraum: (jxF,jyF,jzF) = ik x (bxF,byF,bzF)
template<class T1, class T2, class Filter=NoFilter>
void rotFourier(T1& jxF, T1& jyF, T1& jzF,const T2& bxF, const T2& byF, const T2& bzF);
  
// Rotation im Fourierraum: (jxF,jyF,jzF) = ik x (jxF,jyF,jzF)
template<class T, class Filter=NoFilter>  
void rotFourier(T& jxF, T& jyF, T& jzF);

// Inverse Rotation im Fourierraum (rot rot = - laplace für div=0) aus (wxF,wyF,wzF) = rot(vxF,vyF,vzF) folgt (vxF,vyF,vzF) = 1/k^2 (ik x (wxF,wyF,wzF))
template<class T1,class T2>
void rotInvFourier(T1& vxF, T1& vyF, T1& vzF, const T2& wxF, const T2& wyF, const T2& wzF);

// Inverse Rotation im Fourierraum (rot rot = - laplace für div=0) aus (wxF,wyF,wzF) = rot(vxF,vyF,vzF) folgt (vxF,vyF,vzF) = 1/k^2 (ik x (vxF,vyF,vzF))
template<class T>
void rotInvFourier(T& vxF, T& vyF, T& vzF);

template<class T, class Filter=NoFilter>
void rot(T& jx, T& jy, T& jz, const T& bx, const T& by, const T& bz);

template<class T, class Filter=NoFilter>
void rot(T& jx, T& jy, T& jz);

template<class T>
void rotInv(T& wx, T& wy, T& wz);

template<class T>
void rotInv(T& vx, T& vy, T& vz, const T& wx, const T& wy, const T& wz);

template<class T, class Filter=NoFilter>
void divF(T& div, const T& vx, const T& vy, const T& vz);

template<class T>
double intDivF(const T& vx, const T& vy, const T& vz);

template<class T>
void gradF(T& px, T& py, T& pz, const T& p);

//Fourier space geometric transforms
template<class T>
void shiftzF(T& p1, const T& p0, double a);
template<class T> //skew in X by Y
void skewxF(T& p1, const T& p0, std::vector<double> a, double dx);
template<class T> //skew in Y by X
void skewyF(T& p1, const T& p0, std::vector<double> a, double dx);

//real space geometric transforms
template<class T>
void shift(T& p1, const T& p0,unsigned int axis,double a);
template<class T>
void rotatez(T& p1, const T& p0, std::vector<double> a, double dx, bool clip=false);
template<class T>
void rotatex(T& p1, const T& p0, std::vector<double> a, double dx, bool clip=false);
template<class T>
void rotatey(T& p1, const T& p0, std::vector<double> a, double dx, bool clip=false);
template<class T>
void rotate(T& p1, const T& p0, unsigned int axis, std::vector<double> a,double dx);

template<class T>
void clipz(T& p1, const T& p0, int a=0, double max=1., double min=0., bool clip=true);

template<class T>
void gradInvF(T& p, const T& gradX, const T& gradY, const T& gradZ);

template<class T, class Filter=NoFilter>
void laplaceF(T& result, const T& u);

template<class T, class Filter=NoFilter>
void laplaceF(T& result);

template<class T>
void projectF(T& wxF, T& wyF, T& wzF);

template<class T>
void crossProduct(T& crossX, T& crossY, T& crossZ,
		  const T& ax, const T& ay, const T& az,
		  const T& bx, const T& by, const T& bz);

template<class T>
void convolution(T& dest, const T& a, const T& b);

template<class T>
void convolutionF(T& dest, const T& a, const T& b);

template<template <class TYPE> class VECTOR, class TYPE> 
bool fftwMatrixNamesBeg2Indices(VECTOR<TYPE> fields, VECTOR<std::string> names, VECTOR<int>& indizes);

template<template <class TYPE> class VECTOR, class TYPE> 
bool fftwMatrixNamePattern2Indices(VECTOR<TYPE> fields, VECTOR<std::string> names, VECTOR<int>& indizes);

template<template <class TYPE> class VECTOR, class TYPE> 
bool fftwMatrixNames2Indices(VECTOR<TYPE> fields, VECTOR<std::string> names, VECTOR<int>& indizes);

template<template <class TYPE> class VECTOR, class TYPE> 
bool fftwMatrixNamePattern2Names(VECTOR<TYPE> fields, VECTOR<std::string> names, VECTOR<std::string>& realNames);

template<class T>
void rotateFourierModes(T& dest, double strength);

template<class T>
void randomizeFourierModes(T& dest, long seed = 0);

template<class T>
void average(T& averaged, const T& field, double norm = 1);

/// returns the local maximum value of a 3dim vector field
template<class T>
double maxAbsOf3DVectorField(const T& vx, const T& vy, const T& vz);

/// returns the global maximum value of a 3dim vector field
template<class T>
double globalMaxAbsOf3DVectorField(const T& vx, const T& vy, const T& vz);

/// returns the local maximum value of a scalar field
template<class T>
double maxAbsOf3DScalarField(const T& v);

/// returns the global maximum value of a scalar field
template<class T>
double globalMaxAbsOf3DScalarField(const T& v);

// ---------------------------------------------------------------------------------------------------------
// definitions
// ---------------------------------------------------------------------------------------------------------

template<class MATRIX>
void projectOnDivFree(MATRIX& vxF, MATRIX& vyF, MATRIX& vzF)
{
  ERRORLOGL(3,"projectOnDivFree");

  BK::Complex<double> tempX;
  BK::Complex<double> tempY;
  BK::Complex<double> tempZ;

  const typename MATRIX::base_type& base = *(vxF.get_base());

  for(int x=vxF.get_loC(0);x<=vxF.get_hiC(0);x++)
    for(int y=vxF.get_loC(1);y<=vxF.get_hiC(1);y++)
      for(int z=vxF.get_loC(2);z<=vxF.get_hiC(2);z++) {
       	BK::Complex<typename MATRIX::value_type> vxS = vxF.c(x,y,z);
	BK::Complex<typename MATRIX::value_type> vyS = vyF.c(x,y,z);
	BK::Complex<typename MATRIX::value_type> vzS = vzF.c(x,y,z);
	double kxS = base.kx(x);
	double kyS = base.ky(y);
	double kzS = base.kz(z);
	double k2S = base.k2(x,y,z);

	if(k2S > 0) {
	  tempX = (vxS*kxS*kxS/k2S+vyS*kxS*kyS/k2S+vzS*kxS*kzS/k2S);
	  tempY = (vxS*kxS*kyS/k2S+vyS*kyS*kyS/k2S+vzS*kyS*kzS/k2S);
	  tempZ = (vxS*kxS*kzS/k2S+vyS*kzS*kyS/k2S+vzS*kzS*kzS/k2S);
	  
	  vxF.c(x,y,z) -= tempX;
	  vyF.c(x,y,z) -= tempY;
	  vzF.c(x,y,z) -= tempZ;
	}
      }
  
  ERRORLOGL(4,"projectOnDivFree-end");
}

template<class T>
void rotateFourierModes(T& dest, double strength)
{
  ERRORLOGL(3,"rotateFourierModes");

  assert(dest.get_dataType() == ComplexData);
  
  const typename T::base_type& base = *(dest.get_base());
  
  for(int x=dest.get_loC(0);x<=dest.get_hiC(0);x++)
    for(int y=dest.get_loC(1);y<=dest.get_hiC(1);y++)
      for(int z=1;z<=dest.get_hiC(2);z++)
	dest.c(x,y,z) *= exp(II*strength*pow(base.k2(x,y,z),4/12.));

  for(int x=dest.get_loC(0);x<=dest.get_hiC(0);x++)
    for(int y=dest.get_loC(1);y<=dest.get_hiC(1);y++) {
      if(base.localFftw2MathGlobalYC(y) > 0)
	dest.c(x,y,0) *= exp(II*strength*pow(base.k2(x,y,0),4/12.));
      if(base.localFftw2MathGlobalYC(y) < 0)
	dest.c(x,y,0) *= exp(-II*strength*pow(base.k2(x,y,0),4/12.));
      if(base.localFftw2MathGlobalYC(y) == 0 && base.localFftw2MathGlobalXC(x) > 0)
	dest.c(x,y,0) *= exp(II*strength*pow(base.k2(x,y,0),4/12.));
      if(base.localFftw2MathGlobalYC(y) == 0 && base.localFftw2MathGlobalXC(x) < 0)
	dest.c(x,y,0) *= exp(-II*strength*pow(base.k2(x,y,0),4/12.));
    }

  ERRORLOGL(4,"rotateFourierModes-end");
}

template<class T>
void randomizeFourierModes(T& dest, long seed)
{
  ERRORLOGL(3,BK::appendString("randomizeFourierModes ",seed));

  const typename T::base_type& base = *(dest.get_base());

  srandf90(seed + base.mpiBase_->get_commRank());

  assert(dest.get_dataType() == ComplexData);
  
  for(int x=dest.get_loC(0);x<=dest.get_hiC(0);x++)
    for(int y=dest.get_loC(1);y<=dest.get_hiC(1);y++)
      for(int z=1;z<=dest.get_hiC(2);z++)
	dest.c(x,y,z) *= exp(2*II*M_PI*ranf());

  int tempX;
  int tempY;

  srandf90(seed+1);

  for(int x=-int(base.get_globalNxC()/2);x<int(base.get_globalNxC()/2);x++)
    for(int y=-int(base.get_globalNyC()/2);y<int(base.get_globalNyC()/2);y++) {
      
      double randomPhase = 2*M_PI*ranf();
      
      if(y < 0) {
	tempX = base.globalMath2FftwLocalXC(-x);
	tempY = base.globalMath2FftwLocalYC(-y);
	
	if(dest.inRangeC(tempX,tempY,0))
	  dest.c(tempX,tempY,0) *= exp(-II*randomPhase);
	
	tempX = base.globalMath2FftwLocalXC(x);
	tempY = base.globalMath2FftwLocalYC(y);

	if(dest.inRangeC(tempX,tempY,0))
	  dest.c(tempX,tempY,0) *= exp(II*randomPhase);
      }
      else {	    
	if(y == 0 && x < 0) {
	  
	  tempX = base.globalMath2FftwLocalXC(-x);
	  tempY = base.globalMath2FftwLocalYC(-y);

	  if(dest.inRangeC(tempX,tempY,0))
	    dest.c(tempX,tempY,0) *= exp(-II*randomPhase);

	  tempX = base.globalMath2FftwLocalXC(x);
	  tempY = base.globalMath2FftwLocalYC(y);

	  if(dest.inRangeC(tempX,tempY,0))
	    dest.c(tempX,tempY,0) *= exp(II*randomPhase);
	}
      }
    }

  ERRORLOGL(4,"randomizeFourierModes-end");
}

template<class T1,class T2>
void rotInvFourier(T1& vxF, T1& vyF, T1& vzF,
		   const T2& wxF, const T2& wyF, const T2& wzF)
{
  ERRORLOGL(3,"rotInvFourier const");

  assert(wxF.get_dataType() == ComplexData);
  assert(wyF.get_dataType() == ComplexData);
  assert(wzF.get_dataType() == ComplexData);

  vxF.set_dataType(ComplexData);
  vyF.set_dataType(ComplexData);
  vzF.set_dataType(ComplexData);

  vxF.c(0,0,0) = Complex<typename T1::value_type>(0,0);
  vyF.c(0,0,0) = Complex<typename T1::value_type>(0,0);
  vzF.c(0,0,0) = Complex<typename T1::value_type>(0,0);

  const typename T1::base_type& base = *(vxF.get_base());

  for(int x=vxF.get_loC(0);x<=vxF.get_hiC(0);x++)
    for(int y=vxF.get_loC(1);y<=vxF.get_hiC(1);y++)
      for(int z=vxF.get_loC(2);z<=vxF.get_hiC(2);z++)
	if(base.k2(x,y,z) != 0){        // remove if !!!
	  vxF.c(x,y,z) = II/base.k2(x,y,z)*(base.ky(y)*wzF.c(x,y,z) - base.kz(z)*wyF.c(x,y,z));
	  vyF.c(x,y,z) = II/base.k2(x,y,z)*(base.kz(z)*wxF.c(x,y,z) - base.kx(x)*wzF.c(x,y,z));
	  vzF.c(x,y,z) = II/base.k2(x,y,z)*(base.kx(x)*wyF.c(x,y,z) - base.ky(y)*wxF.c(x,y,z));
	}
  
  ERRORLOGL(4,"rotInvFourier const -end");
}

template<class T>
void rotInvFourier(T& vxF, T& vyF, T& vzF)
{
  ERRORLOGL(3,"rotInvFourier");

  assert(vxF.get_dataType() == ComplexData);
  assert(vyF.get_dataType() == ComplexData);
  assert(vzF.get_dataType() == ComplexData);

  const typename T::base_type& base = *(vxF.get_base());

  if(base.k2(0,0,0) == 0) {
    vxF.c(0,0,0) = Complex<typename T::value_type>(0,0);
    vyF.c(0,0,0) = Complex<typename T::value_type>(0,0);
    vzF.c(0,0,0) = Complex<typename T::value_type>(0,0);
  }

  Complex<typename T::value_type> tempX;
  Complex<typename T::value_type> tempY;
  Complex<typename T::value_type> tempZ;

  for(int x=vxF.get_loC(0);x<=vxF.get_hiC(0);x++)
    for(int y=vxF.get_loC(1);y<=vxF.get_hiC(1);y++)
      for(int z=vxF.get_loC(2);z<=vxF.get_hiC(2);z++)
	if(base.k2(x,y,z) != 0){        // remove if !!!
	  tempX = vxF.c(x,y,z);
	  tempY = vyF.c(x,y,z);
	  tempZ = vzF.c(x,y,z);

	  vxF.c(x,y,z) = II/base.k2(x,y,z)*(base.ky(y)*tempZ - base.kz(z)*tempY);
	  vyF.c(x,y,z) = II/base.k2(x,y,z)*(base.kz(z)*tempX - base.kx(x)*tempZ);
	  vzF.c(x,y,z) = II/base.k2(x,y,z)*(base.kx(x)*tempY - base.ky(y)*tempX);
	}
	
  ERRORLOGL(4,"rotInvFourier-end");
}
  class NoFilter
  {
  public:
    NoFilter(FftBase const&) {}

    double sigma(int x, int y, int z) {
      return 1.;
    }
  };
  
  class CesaroFilter
  {
  public:
    CesaroFilter(FftBase const& fftBase) : fftBase_(fftBase) {}
    
    double sigma(int x, int y, int z) {
      return 1.-2.*sqrt(sqr(fftBase_.kx(x)/(fftBase_.get_globalNxR()+2.))+sqr(fftBase_.ky(y)/(fftBase_.get_globalNyR()+2.))+sqr(fftBase_.kz(z)/(fftBase_.get_globalNzR()+2.)));
    }
    
  private:
    FftBase const& fftBase_;
  };

  class LanczosFilter
  {
  public:
    LanczosFilter(FftBase const& fftBase) : fftBase_(fftBase) {}
    
    double sigma(int x, int y, int z) {
      return (fftBase_.kx(x)==0.&&fftBase_.ky(y)==0.&&fftBase_.kz(z)==0.)?
	1.:
	sin(2.*M_PI*sqrt(sqr(fftBase_.kx(x)/fftBase_.get_globalNxR())+
			 sqr(fftBase_.ky(y)/fftBase_.get_globalNyR())+
			 sqr(fftBase_.kz(z)/fftBase_.get_globalNzR())))/(2.*M_PI*sqrt(sqr(fftBase_.kx(x)/fftBase_.get_globalNxR())+
										      sqr(fftBase_.ky(y)/fftBase_.get_globalNyR())+
										      sqr(fftBase_.kz(z)/fftBase_.get_globalNzR())));
    }
    
  private:
    FftBase const& fftBase_;
  };

  class RaisedCosineFilter
  {
  public:
    RaisedCosineFilter(FftBase const& fftBase) : fftBase_(fftBase) {}
    
    double sigma(int x, int y, int z) {
      return .5*(1.+cos(2.*M_PI*sqrt(sqr(fftBase_.kx(x)/fftBase_.get_globalNxR())+sqr(fftBase_.ky(y)/fftBase_.get_globalNyR())+sqr(fftBase_.kz(z)/fftBase_.get_globalNzR()))));
    }
    
  private:
    FftBase const& fftBase_;
  };

  class SharpenedRaisedCosineFilter
  {
  public:
    SharpenedRaisedCosineFilter(FftBase const& fftBase) : rCFilter_(fftBase) {}
    
    double sigma(int x, int y, int z) {
      return sqr(sqr(rCFilter_.sigma(x,y,z)))*(35.-84.*rCFilter_.sigma(x,y,z)+70.*sqr(rCFilter_.sigma(x,y,z))-20.*rCFilter_.sigma(x,y,z)*sqr(rCFilter_.sigma(x,y,z)));
    }
    
  private:
    RaisedCosineFilter rCFilter_;
  };
  

template<class T, class Filter>
void rotFourier(T& jxF, T& jyF, T& jzF)
{
  ERRORLOGL(3,"rotFourier");

  assert(jxF.get_dataType() == ComplexData);
  assert(jyF.get_dataType() == ComplexData);
  assert(jzF.get_dataType() == ComplexData);

  Complex<typename T::value_type> tempX;
  Complex<typename T::value_type> tempY;
  Complex<typename T::value_type> tempZ;

  const typename T::base_type& fftBase = *(jxF.get_base());
  Filter filter(fftBase);
  
  for(int x=jxF.get_loC(0);x<=jxF.get_hiC(0);x++)
    for(int y=jxF.get_loC(1);y<=jxF.get_hiC(1);y++)
      for(int z=jxF.get_loC(2);z<=jxF.get_hiC(2);z++){
	tempX = jxF.c(x,y,z);
	tempY = jyF.c(x,y,z);
	tempZ = jzF.c(x,y,z);
	
	jxF.c(x,y,z) = filter.sigma(x,y,z)*(II*(fftBase.ky(y)*tempZ - fftBase.kz(z)*tempY));
	jyF.c(x,y,z) = filter.sigma(x,y,z)*(II*(fftBase.kz(z)*tempX - fftBase.kx(x)*tempZ));
	jzF.c(x,y,z) = filter.sigma(x,y,z)*(II*(fftBase.kx(x)*tempY - fftBase.ky(y)*tempX));
      }
  
  ERRORLOGL(4,"rotFourier-end");
}

template<class T1, class T2, class Filter>
void rotFourier(T1& jxF, T1& jyF, T1& jzF, const T2& bxF, const T2& byF, const T2& bzF)
{
  ERRORLOGL(3,"rotFourier const");

  assert(bxF.get_dataType() == ComplexData);
  assert(byF.get_dataType() == ComplexData);
  assert(bzF.get_dataType() == ComplexData);

  jxF.set_dataType(ComplexData);
  jyF.set_dataType(ComplexData);
  jzF.set_dataType(ComplexData);

  const typename T1::base_type& fftBase = *(jxF.get_base());
  Filter filter(fftBase);
  
  for(int x=jxF.get_loC(0);x<=jxF.get_hiC(0);x++)
    for(int y=jxF.get_loC(1);y<=jxF.get_hiC(1);y++)
      for(int z=jxF.get_loC(2);z<=jxF.get_hiC(2);z++) {
	jxF.c(x,y,z) = filter.sigma(x,y,z)*(II*(fftBase.ky(y)*bzF.c(x,y,z) - fftBase.kz(z)*byF.c(x,y,z)));
	jyF.c(x,y,z) = filter.sigma(x,y,z)*(II*(fftBase.kz(z)*bxF.c(x,y,z) - fftBase.kx(x)*bzF.c(x,y,z)));
	jzF.c(x,y,z) = filter.sigma(x,y,z)*(II*(fftBase.kx(x)*byF.c(x,y,z) - fftBase.ky(y)*bxF.c(x,y,z)));
      }
  
  ERRORLOGL(4,"rotFourier const -end");
}

template<class T, class Filter>
void rot(T& jx, T& jy, T& jz)
{
  ERRORLOGL(3,"rot");

  assert(jx.get_dataType() == RealData);
  assert(jy.get_dataType() == RealData);
  assert(jz.get_dataType() == RealData);
  
  jx.fftR2C();
  jy.fftR2C();
  jz.fftR2C();

  rotFourier<T, Filter>(jx,jy,jz);

  jx.fftC2R();
  jy.fftC2R();
  jz.fftC2R();

  ERRORLOGL(4,"rot-end");
}	  

template<class T, class Filter>
void rot(T& jx, T& jy, T& jz,
	 const T& bx, const T& by, const T& bz)
{
  ERRORLOGL(3,"rot const");

  assert(bx.get_dataType() == RealData);
  assert(by.get_dataType() == RealData);
  assert(bz.get_dataType() == RealData);
  
  jx.set_dataType(RealData);
  jy.set_dataType(RealData);
  jz.set_dataType(RealData);

  jx.fftR2C(bx);
  jy.fftR2C(by);
  jz.fftR2C(bz);

  rotFourier<T, Filter>(jx,jy,jz);

  jx.fftC2R();
  jy.fftC2R();
  jz.fftC2R();

  ERRORLOGL(4,"rot const-end");
}

template<class T>
void rotInv(T& wx, T& wy, T& wz)
{
  ERRORLOGL(3,"rotInv");

  assert(wx.get_dataType() == RealData);
  assert(wy.get_dataType() == RealData);
  assert(wz.get_dataType() == RealData);
  
  wx.fftR2C();
  wy.fftR2C();
  wz.fftR2C();

  rotInvFourier(wx,wy,wz);

  wx.fftC2R();
  wy.fftC2R();
  wz.fftC2R();

  ERRORLOGL(4,"rotation-end");
}	  

template<class T>
void rotInv(T& vx, T& vy, T& vz,
	    const T& wx, const T& wy, const T& wz)
{
  ERRORLOGL(3,"rot const");
  
  assert(wx.get_dataType() == RealData);
  assert(wy.get_dataType() == RealData);
  assert(wz.get_dataType() == RealData);
  
  vx.set_dataType(RealData);
  vy.set_dataType(RealData);
  vz.set_dataType(RealData);

  vx.fftR2C(wx);
  vy.fftR2C(wy);
  vz.fftR2C(wz);

  rotInvFourier(vx,vy,vz);

  vx.fftC2R();
  vy.fftC2R();
  vz.fftC2R();

  ERRORLOGL(4,"rot const-end");
}

template<class T, class Filter>
void divF(T& div, const T& vx, const T& vy, const T& vz)
{
  ERRORLOGL(4,"divF");
 
  assert(vx.get_dataType() == ComplexData);
  assert(vy.get_dataType() == ComplexData);
  assert(vz.get_dataType() == ComplexData);

  div.set_dataType(BK::ComplexData);
  
  const typename T::base_type& base = *(vx.get_base());
  Filter filter(base);
  
  for(int x=vx.get_loC(0);x<=vx.get_hiC(0);x++)
    for(int y=vx.get_loC(1);y<=vx.get_hiC(1);y++)
      for(int z=vx.get_loC(2);z<=vx.get_hiC(2);z++) {
	div.c(x,y,z) = filter.sigma(x,y,z)*II*(base.kx(x)*vx.c(x,y,z)
					       + base.ky(y)*vy.c(x,y,z)
					       + base.kz(z)*vz.c(x,y,z));
      }
  
  ERRORLOGL(4,"divF-end");
}

template<class T>
double intDivF(const T& vx, const T& vy, const T& vz)
{
  ERRORLOGL(4,"intDivF");
 
  assert(vx.get_dataType() == ComplexData);
  assert(vy.get_dataType() == ComplexData);
  assert(vz.get_dataType() == ComplexData);

  T divTemp(vx);
  
  divF(divTemp,vx,vy,vz);
  
  double divergence = 0;

  for(int x=vx.get_loC(0);x<=vx.get_hiC(0);x++)
    for(int y=vx.get_loC(1);y<=vx.get_hiC(1);y++)
      for(int z=vx.get_loC(2);z<=vx.get_hiC(2);z++) {
	divergence += sqrNorm(divTemp.c(x,y,z)) 
	  + sqrNorm(divTemp.c(x,y,z)) 
	  + sqrNorm(divTemp.c(x,y,z));
      }

  return sqrt(divergence);
  
  ERRORLOGL(4,"intDivF-end");
}

template<class T>
void gradInvF(T& p, const T& gradX, const T& gradY, const T& gradZ)
{
  ERRORLOGL(3,"gradInvF const");

  assert(gradX.get_dataType() == ComplexData);
  assert(gradY.get_dataType() == ComplexData);
  assert(gradZ.get_dataType() == ComplexData);

  p.set_dataType(ComplexData);

  p.c(0,0,0) = Complex<typename T::value_type>(0,0);
 
  const typename T::base_type& base = *(p.get_base());

  for(int x=p.get_loC(0);x<=p.get_hiC(0);x++)
    for(int y=p.get_loC(1);y<=p.get_hiC(1);y++)
      for(int z=p.get_loC(2);z<=p.get_hiC(2);z++)
	if(base.k2(x,y,z) > 0)        // if ausbauen !!!
	  p.c(x,y,z) = -II/base.k2(x,y,z)*(base.kx(x)*gradX.c(x,y,z) + base.ky(y)*gradY.c(x,y,z) + base.kz(z)*gradZ.c(x,y,z));
  
  ERRORLOGL(4,"gradInv const-end");
}

template<class T>
void gradF(T& px, T& py, T& pz, const T& p)
{
  ERRORLOGL(3,"gradF const");

  assert(p.get_dataType() == ComplexData);

  px.set_dataType(ComplexData);
  py.set_dataType(ComplexData);
  pz.set_dataType(ComplexData);

  const typename T::base_type& base = *(p.get_base());
  
  for(int x=p.get_loC(0);x<=p.get_hiC(0);x++)
    for(int y=p.get_loC(1);y<=p.get_hiC(1);y++)
      for(int z=p.get_loC(2);z<=p.get_hiC(2);z++){
	px.c(x,y,z) =II*base.kx(x)*p.c(x,y,z);
	py.c(x,y,z) =II*base.ky(y)*p.c(x,y,z);
	pz.c(x,y,z) =II*base.kz(z)*p.c(x,y,z);
      }
  ERRORLOGL(4,"gradF const-end");
}

template<class T>
void shiftzF(T& p1, const T& p0, double a)
{
  ERRORLOGL(3,"shiftzF const");

  assert(p0.get_dataType() == ComplexData);

  p1.set_dataType(ComplexData);
 
  const typename T::base_type& base = *(p0.get_base());

//  int Nx = base.get_globalNxR();
//  int Ny = base.get_globalNyR();
  for(int x=p0.get_loC(0);x<=p0.get_hiC(0);x++)
    for(int y=p0.get_loC(1);y<=p0.get_hiC(1);y++)
      for(int z=p0.get_loC(2);z<=p0.get_hiC(2);z++){
	p1.c(x,y,z) = exp(II*a*base.kz(z))*p0.c(x,y,z);
      }
  
  ERRORLOGL(4,"shiftzF const-end");
}

template<class T>
void shift(T& p1, const T& p0, unsigned int axis, double a)
{
  ERRORLOGL(3,"shift const");

  assert(axis<=2);
  assert(p0.get_dataType() == RealData);
  p1.set_dataType(RealData);
  typename T::base_type& base = *(p0.get_base());
  for(int x=p0.get_loR(0);x<=p0.get_hiR(0);x++)
    for(int y=p0.get_loR(1);y<=p0.get_hiR(1);y++)
      for(int z=p0.get_loR(2);z<=p0.get_hiR(2);z++)
	p1.r(x,y,z) = p0.r(x,y,z);

  if(axis==0)
    p1.transpose_xz(base);
  else if(axis==1)
    p1.transpose_yz(base);
  p1.fftR2Cz();
  shiftzF(p1,p1,a);
  p1.fftC2Rz();
  if(axis==0)
    p1.transpose_xz(base);
  else if(axis==1)
    p1.transpose_yz(base);

  ERRORLOGL(4,"shift const-end");
}

template<class T>
void clipz(T& p1, const T& p0, int a, double max, double min, bool clip)
{
  ERRORLOGL(3,"clipz const");

  assert(p0.get_dataType() == RealData);

  p1.set_dataType(RealData);
 
  const typename T::base_type& base = *(p0.get_base());

  int Nx = base.get_globalNxR();
  int Ny = base.get_globalNyR();
  int R  = (Nx>Ny)?(Nx/2):(Ny/2);
  int Cx = (base.globalMath2FftwLocalXR(Nx/2));
  int Cy = (base.globalMath2FftwLocalYR(Ny/2));
  double inner = 0.;
  for(int x=p0.get_loR(0);x<=p0.get_hiR(0);x++)
    for(int y=p0.get_loR(1);y<=p0.get_hiR(1);y++){
      inner = ((x-Cx)*(x-Cx)+(y-Cy)*(y-Cy)<(R-a)*(R-a))?1.:0.;
      for(int z=p0.get_loR(2);z<=p0.get_hiR(2);z++){
	p1.r(x,y,z) = (!clip)?(p0.r(x,y,z)):(inner*((p0.r(x,y,z)>max)?max:((p0.r(x,y,z)<min)?min:p0.r(x,y,z))));
      }}
  
  ERRORLOGL(4,"clipz const-end");
}

template<class T>
void skewxF(T& p1, const T& p0, std::vector<double> a, double dx)
{
  ERRORLOGL(3,"skewxF const");

  assert(p0.get_dataType() == ComplexData);

  p1.set_dataType(ComplexData);
 
  typename T::base_type& base = *(p0.get_base());

  int Nx = base.get_globalNxR();
  int Ny = base.get_globalNyR();
  int Nz = base.get_globalNzR();
  int scale = Nz/Nx;
  if(scale>1) for(int i=0;i<Nz/scale;i++) a[i]=a[i+Nz/scale/2];
//  double Cx = (double)(base.globalMath2FftwLocalXC(Nx/2));
//  double Cy = (double)(base.globalMath2FftwLocalYC(Ny/2));
  int me = base.mpiBase_->get_commRank();
  int NprocZ = base.get_processorGridDims(1);
  int NprocY = base.get_processorGridDims(0);
  int row = me%NprocZ;
  int column = (me-row)/NprocZ;
  for(int x=p0.get_loC(0);x<=p0.get_hiC(0);x++)
    for(int y=p0.get_loC(1);y<=p0.get_hiC(1);y++)
      for(int z=p0.get_loC(2);z<=p0.get_hiC(2);z++)
	  p1.c(x,y,z) = p1.skew(x,y,p0.c(x,y,z),Nx/2,Ny/2,column*Ny/NprocY,dx*base.kz(z),a,false);
	  //p1.c(x,y,z) = exp(II*((double)x-Nx/2)*dx*a[y+column*Ny/NprocY]*base.kz(z))*p0.c(x,y,z);
	  //p1.c(x,y,z) = exp(II*((double)(y)+column*Ny/NprocY-Ny/2)*dx*a[x]*base.kz(z))*p0.c(x,y,z);
  
  ERRORLOGL(4,"skewxF const-end");
}

template<class T>
void skewyF(T& p1, const T& p0, std::vector<double> a, double dx)
{
  ERRORLOGL(3,"skewyF const");

  assert(p0.get_dataType() == ComplexData);

  p1.set_dataType(ComplexData);
 
  typename T::base_type& base = *(p0.get_base());

  int Nx = base.get_globalNxR();
  int Ny = base.get_globalNyR();
  int Nz = base.get_globalNzR();
  int scale = Nz/Nx;
  if(scale>1) for(int i=0;i<Nz/scale;i++) a[i]=a[i+Nz/scale/2];
//  double Cx = (double)(base.globalMath2FftwLocalXC(Nx/2));
//  double Cy = (double)(base.globalMath2FftwLocalYC(Ny/2));
  int me = base.mpiBase_->get_commRank();
  int NprocZ = base.get_processorGridDims(1);
  int NprocY = base.get_processorGridDims(0);
  int row = me%NprocZ;
  int column = (me-row)/NprocZ;
  for(int x=p0.get_loC(0);x<=p0.get_hiC(0);x++)
    for(int y=p0.get_loC(1);y<=p0.get_hiC(1);y++)
      for(int z=p0.get_loC(2);z<=p0.get_hiC(2);z++)
	  p1.c(x,y,z) = p1.skew(x,y,p0.c(x,y,z),Nx/2,Ny/2,column*Ny/NprocY,dx*base.kz(z),a,true);
	  //p1.c(x,y,z) = exp(II*((double)(y)+column*Ny/NprocY-Ny/2)*dx*a[x]*base.kz(z))*p0.c(x,y,z);
	  //p1.c(x,y,z) = exp(II*((double)x-Nx/2)*dx*a[y+column*Ny/NprocY]*base.kz(z))*p0.c(x,y,z);
  
  ERRORLOGL(4,"skewyF const-end");
}

template<class T>
void rotatez(T& p1, const T& p0, std::vector<double> a, double dx, bool clip)
{
  ERRORLOGL(3,"rotatez const");

  assert(p0.get_dataType() == RealData);

  p1.set_dataType(RealData);
 
  typename T::base_type& base = *(p0.get_base());

  std::vector<double> tanalpha,sinalpha,tanalpha2;
  for(unsigned int i=0;i<base.get_globalNzR();i++) {
    tanalpha.push_back(tan(a[i]/2.));
    tanalpha2.push_back(tan(a[i]/2.));
    sinalpha.push_back(-sin(a[i]));
  }

//  clipz(p1,p0,0,1.,0.,clip);
  for(int x=p0.get_loR(0);x<=p0.get_hiR(0);x++)
    for(int y=p0.get_loR(1);y<=p0.get_hiR(1);y++)
      for(int z=p0.get_loR(2);z<=p0.get_hiR(2);z++)
	p1.r(x,y,z) = p0.r(x,y,z);

  p1.transpose_xz(base);
  p1.fftR2Cz();
  skewyF(p1,p1,tanalpha,dx);
  p1.fftC2Rz();
  p1.transpose_xz(base);
  p1.transpose_yz(base);
  p1.fftR2Cz();
  skewxF(p1,p1,sinalpha,dx);
  p1.fftC2Rz();
  p1.transpose_yz(base);
  p1.transpose_xz(base);
  p1.fftR2Cz();
  skewyF(p1,p1,tanalpha2,dx);
  p1.fftC2Rz();
  p1.transpose_xz(base);
//  clipz(p1,p1,0,1000.,-1000.,clip);

  tanalpha.clear();
  tanalpha2.clear();
  sinalpha.clear();
  
  ERRORLOGL(4,"rotatez const-end");
}

template<class T>
void rotate(T& p1, const T& p0, unsigned int axis, std::vector<double> a,double dx)
{
  ERRORLOGL(3,"rotate const");

  assert(p0.get_dataType() == RealData);

  p1.set_dataType(RealData);

  typename T::base_type& base = *(p0.get_base());

  std::vector<double> tanalpha,sinalpha;
  for(unsigned int i=0;i<base.get_globalNzR();i++) {
    tanalpha.push_back(tan(a[i]/2.));
    sinalpha.push_back(-sin(a[i]));
  }

  for(int x=p0.get_loR(0);x<=p0.get_hiR(0);x++)
    for(int y=p0.get_loR(1);y<=p0.get_hiR(1);y++)
      for(int z=p0.get_loR(2);z<=p0.get_hiR(2);z++)
	p1.r(x,y,z) = p0.r(x,y,z);

  if(axis==0)
   {
    p1.transpose_yz(base);
    p1.fftR2Cz();
    skewyF(p1,p1,tanalpha,dx);
    p1.fftC2Rz();
    p1.transpose_yz(base);
    p1.fftR2Cz();
    skewyF(p1,p1,sinalpha,dx);
    p1.fftC2Rz();
    p1.transpose_yz(base);
    p1.fftR2Cz();
    skewyF(p1,p1,tanalpha,dx);
    p1.fftC2Rz();
    p1.transpose_yz(base);
   }
  else if(axis==1)
   {
    p1.fftR2Cz();
    skewxF(p1,p1,tanalpha,dx);
    p1.fftC2Rz();
    p1.transpose_xz(base);
    p1.fftR2Cz();
    skewxF(p1,p1,sinalpha,dx);
    p1.fftC2Rz();
    p1.transpose_xz(base);
    p1.fftR2Cz();
    skewxF(p1,p1,tanalpha,dx);
    p1.fftC2Rz();
   }
  else if(axis==2)
   {
    p1.transpose_xz(base);
    p1.fftR2Cz();
    skewyF(p1,p1,tanalpha,dx);
    p1.fftC2Rz();
    p1.transpose_xz(base);
    p1.transpose_yz(base);
    p1.fftR2Cz();
    skewxF(p1,p1,sinalpha,dx);
    p1.fftC2Rz();
    p1.transpose_yz(base);
    p1.transpose_xz(base);
    p1.fftR2Cz();
    skewyF(p1,p1,tanalpha,dx);
    p1.fftC2Rz();
    p1.transpose_xz(base);
   }

  tanalpha.clear();
  sinalpha.clear();

  ERRORLOGL(4,"rotate const-end");
}

template<class T>
void rotatex(T& p1, const T& p0, std::vector<double> a, double dx, bool clip)
{
  ERRORLOGL(3,"rotatex const");

  assert(p0.get_dataType() == RealData);

  p1.set_dataType(RealData);
 
  typename T::base_type& base = *(p0.get_base());

  std::vector<double> tanalpha,sinalpha;
  for(unsigned int i=0;i<base.get_globalNzR();i++) {
    tanalpha.push_back(tan(a[i]/2.));
    sinalpha.push_back(-sin(a[i]));
  }

//  clipz(p1,p0,0,1.,0.,clip);
  for(int x=p0.get_loR(0);x<=p0.get_hiR(0);x++)
    for(int y=p0.get_loR(1);y<=p0.get_hiR(1);y++)
      for(int z=p0.get_loR(2);z<=p0.get_hiR(2);z++)
	p1.r(x,y,z) = p0.r(x,y,z);
  p1.transpose_yz(base);
  p1.fftR2Cz();
  skewyF(p1,p1,tanalpha,dx);
  p1.fftC2Rz();
  p1.transpose_yz(base);
  p1.fftR2Cz();
  skewyF(p1,p1,sinalpha,dx);
  p1.fftC2Rz();
  p1.transpose_yz(base);
  p1.fftR2Cz();
  skewyF(p1,p1,tanalpha,dx);
  p1.fftC2Rz();
  p1.transpose_yz(base);
//  clipz(p1,p1,0,1000.,-1000.,clip);

  tanalpha.clear();
  sinalpha.clear();
  
  ERRORLOGL(4,"rotatex const-end");
}

template<class T>
void rotatey(T& p1, const T& p0, std::vector<double> a, double dx, bool clip)
{
  ERRORLOGL(3,"rotatey const");

  assert(p0.get_dataType() == RealData);

  p1.set_dataType(RealData);
 
  typename T::base_type& base = *(p0.get_base());

  std::vector<double> tanalpha,sinalpha;
  for(unsigned int i=0;i<base.get_globalNzR();i++) {
    tanalpha.push_back(tan(a[i]/2.));
    sinalpha.push_back(-sin(a[i]));
  }

//  clipz(p1,p0,0,1.,0.,clip);
  for(int x=p0.get_loR(0);x<=p0.get_hiR(0);x++)
    for(int y=p0.get_loR(1);y<=p0.get_hiR(1);y++)
      for(int z=p0.get_loR(2);z<=p0.get_hiR(2);z++)
	p1.r(x,y,z) = p0.r(x,y,z);
  p1.fftR2Cz();
  skewxF(p1,p1,tanalpha,dx);
  p1.fftC2Rz();
  p1.transpose_xz(base);
  p1.fftR2Cz();
  skewxF(p1,p1,sinalpha,dx);
  p1.fftC2Rz();
  p1.transpose_xz(base);
  p1.fftR2Cz();
  skewxF(p1,p1,tanalpha,dx);
  p1.fftC2Rz();
//  clipz(p1,p1,0,1000.,-1000.,clip);

  tanalpha.clear();
  sinalpha.clear();
  
  ERRORLOGL(4,"rotatey const-end");
}

template<class T, class Filter>
void laplaceF(T& result, const T& u)
{
  ERRORLOGL(3,"laplaceF const");

  assert(u.get_dataType() == ComplexData);
  assert(result.get_sizeC() == u.get_sizeC());

  result.set_dataType(ComplexData);

  const typename T::base_type& base = *(u.get_base());
  Filter filter(base);
  
  for(int x=u.get_loC(0);x<=u.get_hiC(0);x++)
    for(int y=u.get_loC(1);y<=u.get_hiC(1);y++)
      for(int z=u.get_loC(2);z<=u.get_hiC(2);z++)
	result.c(x,y,z) = -sqr(filter.sigma(x,y,z))*base.k2(x,y,z)*u.c(x,y,z);


  ERRORLOGL(4,"laplaceF const-end");
}

template<class T, class Filter>
void laplaceF(T& result)
{
  ERRORLOGL(3,"laplaceF");

  assert(result.get_dataType() == ComplexData);

  const typename T::base_type& base = *(result.get_base());
  Filter filter(base);
 
  for(int x=result.get_loC(0);x<=result.get_hiC(0);x++)
    for(int y=result.get_loC(1);y<=result.get_hiC(1);y++)
      for(int z=result.get_loC(2);z<=result.get_hiC(2);z++)
	result.c(x,y,z) *= -sqr(filter.sigma(x,y,z))*base.k2(x,y,z);


  ERRORLOGL(4,"laplaceF -end");
}

template<class T>
void calcPressure(T& p, const T& vxF, const T& vyF, const T& vzF)
{
  assert(vxF.get_dataType() == ComplexData);
  assert(vyF.get_dataType() == ComplexData);
  assert(vzF.get_dataType() == ComplexData);

  BK::Complex<typename T::value_type> tempX;
  BK::Complex<typename T::value_type> tempY;
  BK::Complex<typename T::value_type> tempZ;
  
  const typename T::base_type& base = *(vxF.get_base());
 
  for(int x=vxF.get_loC(0);x<=vxF.get_hiC(0);x++)
    for(int y=vxF.get_loC(1);y<=vxF.get_hiC(1);y++)
      for(int z=vxF.get_loC(2);z<=vxF.get_hiC(2);z++) {
       	BK::Complex<typename T::value_type> vxS = vxF.c(x,y,z);
	BK::Complex<typename T::value_type> vyS = vyF.c(x,y,z);
	BK::Complex<typename T::value_type> vzS = vzF.c(x,y,z);
	double kxS = base.kx(x);
	double kyS = base.ky(y);
	double kzS = base.kz(z);
	double k2S = base.k2(x,y,z);
	
	if(k2S > 0) {
	  tempX = (vxS*kxS*kxS/k2S+vyS*kxS*kyS/k2S+vzS*kxS*kzS/k2S);
	  tempY = (vxS*kxS*kyS/k2S+vyS*kyS*kyS/k2S+vzS*kyS*kzS/k2S);
	  tempZ = (vxS*kxS*kzS/k2S+vyS*kzS*kyS/k2S+vzS*kzS*kzS/k2S);
	  
	  vxF.c(x,y,z) -= tempX;
	  vyF.c(x,y,z) -= tempY;
	  vzF.c(x,y,z) -= tempZ;
	}
      }
}

template<class T>
void projectF(T& vxF, T& vyF, T& vzF)
{
  assert(vxF.get_dataType() == ComplexData);
  assert(vyF.get_dataType() == ComplexData);
  assert(vzF.get_dataType() == ComplexData);

  BK::Complex<typename T::value_type> tempX;
  BK::Complex<typename T::value_type> tempY;
  BK::Complex<typename T::value_type> tempZ;
  
  const typename T::base_type& base = *(vxF.get_base());
 
  for(int x=vxF.get_loC(0);x<=vxF.get_hiC(0);x++)
    for(int y=vxF.get_loC(1);y<=vxF.get_hiC(1);y++)
      for(int z=vxF.get_loC(2);z<=vxF.get_hiC(2);z++) {
       	BK::Complex<typename T::value_type> vxS = vxF.c(x,y,z);
	BK::Complex<typename T::value_type> vyS = vyF.c(x,y,z);
	BK::Complex<typename T::value_type> vzS = vzF.c(x,y,z);
	double kxS = base.kx(x);
	double kyS = base.ky(y);
	double kzS = base.kz(z);
	double k2S = base.k2(x,y,z);
	
	if(k2S > 0) {
	  tempX = (vxS*kxS*kxS/k2S+vyS*kxS*kyS/k2S+vzS*kxS*kzS/k2S);
	  tempY = (vxS*kxS*kyS/k2S+vyS*kyS*kyS/k2S+vzS*kyS*kzS/k2S);
	  tempZ = (vxS*kxS*kzS/k2S+vyS*kzS*kyS/k2S+vzS*kzS*kzS/k2S);
	  
	  vxF.c(x,y,z) -= tempX;
	  vyF.c(x,y,z) -= tempY;
	  vzF.c(x,y,z) -= tempZ;
	}
      }
}

template<class T>
void crossProduct(T& crossX, T& crossY, T& crossZ,
		  const T& ax, const T& ay, const T& az,
		  const T& bx, const T& by, const T& bz)
{
  ERRORLOGL(3,"crossProduct");

  for(unsigned int i=0;i<ax.get_sizeR();i++) {
    crossX.ri(i) = (ay.ri(i)*bz.ri(i)-az.ri(i)*by.ri(i));
    crossY.ri(i) = (az.ri(i)*bx.ri(i)-ax.ri(i)*bz.ri(i));
    crossZ.ri(i) = (ax.ri(i)*by.ri(i)-ay.ri(i)*bx.ri(i));
  }
  
  ERRORLOGL(4,"crossProduct-end");
}


template<template <class TYPE> class VECTOR, class TYPE> 
bool fftwMatrixNamesBeg2Indices(VECTOR<TYPE> fields, VECTOR<std::string> names, VECTOR<int>& indizes)
{
  VECTOR<int> nameExists(names.size());

  indizes.resize(names.size());
  
  for(unsigned int i=0;i<names.size();i++) {
    indizes[i] = -1;
    nameExists[i] = false;
  }

  for(unsigned int i=0;i<fields.size();i++) 
    for(unsigned int j=0;j<names.size();j++)
      if(compareStringBegin(fields[i]->get_name(),names[j]) && names[j] != "" && fields[i]->get_name() != "") {
	nameExists[j] = true;
	indizes[j] = i;	
      }
  
  bool success = nameExists[0];
  for(unsigned int i=1;i<names.size();i++) 
    success *= nameExists[i]; 
    
  return success;
}

template<template <class TYPE> class VECTOR, class TYPE> 
bool fftwMatrixNamePattern2Indices(VECTOR<TYPE> fields, VECTOR<std::string> names, VECTOR<int>& indizes)
{
  VECTOR<int> nameExists(names.size());

  indizes.resize(names.size());
  
  for(unsigned int j=0;j<names.size();j++)
    indizes[j] = -1;
  
  for(unsigned int i=0;i< names.size();i++)
    nameExists[i] = false;

  for(unsigned int i=0;i<fields.size();i++) 
    for(unsigned int j=0;j<names.size();j++)
      if(fields[i]->get_name().rfind(names[j]) != std::string::npos) {
	nameExists[j] = true;
	indizes[j] = i;	
      }
  
  bool success = nameExists[0];
  for(unsigned int i=1;i<names.size();i++) 
    success *= nameExists[i]; 
    
  return success;
}

template<template <class TYPE> class VECTOR, class TYPE> 
bool fftwMatrixNamePattern2Names(VECTOR<TYPE> fields, VECTOR<std::string> names, VECTOR<std::string>& realNames)
{
  VECTOR<int> nameExists(names.size());

  realNames.resize(names.size());
  
  for(unsigned int j=0;j<names.size();j++)
    realNames[j] = "";
  
  for(unsigned int i=0;i< names.size();i++)
    nameExists[i] = false;

  for(unsigned int i=0;i<fields.size();i++) 
    for(unsigned int j=0;j<names.size();j++)
      if(fields[i]->get_name().rfind(names[j]) != std::string::npos) {
	nameExists[j] = true;
	realNames[j] = names[j];	
      }
  
  bool success = nameExists[0];
  for(unsigned int i=1;i<names.size();i++) 
    success *= nameExists[i]; 
    
  return success;
}

template<template <class TYPE> class VECTOR, class TYPE> 
bool fftwMatrixNames2Indices(VECTOR<TYPE> fields, VECTOR<std::string> names, VECTOR<int>& indizes)
{
  VECTOR<int> nameExists(names.size());

  indizes.resize(names.size());
  
  for(unsigned int j=0;j<names.size();j++)
    indizes[j] = -1;
  
  for(unsigned int i=0;i< names.size();i++)
    nameExists[i] = false;

  for(unsigned int i=0;i<fields.size();i++) 
    for(unsigned int j=0;j<names.size();j++)
      if(fields[i]->get_name() == names[j]) {
	nameExists[j] = true;
	indizes[j] = i;	
      }
  
  bool success = nameExists[0];
  for(unsigned int i=1;i<names.size();i++) 
    success *= nameExists[i];
  
  return success;
}

template<class T>
void convolution(T& dest, const T& a, const T& b)
{
  dest.set_dataType(BK::RealData);

  for(unsigned int i=0;i<a.get_sizeR();i++)
    dest.ri(i) = a.ri(i)*b.ri(i);

  dest.set_name(BK::appendString(a.get_name(),"*",b.get_name()));
}

template<class T>
void convolutionF(T& dest, const T& a, const T& b)
{
  dest.set_dataType(BK::ComplexData);

  a.fftC2R();
  b.fftC2R();

  convolution(dest,a,b);

  dest.fftR2C();
}

template<class T>
void average(T& averaged, const T& field, double norm)
{
  ERRORLOGL(3,BK::appendString("average(",averaged.get_name(),", ",field.get_name(),")"));

  if(!averaged.get_existence()) {

    ERRORLOGL(3,BK::appendString("average(",averaged.get_name(),", ",field.get_name(),") for the first time"));

    averaged.resize(*field.get_base(),field.get_dataType(),BK::appendString(field.get_name(),"Mean"));

    for(unsigned int i=0;i<averaged.get_sizeR();i++) 
      averaged.ri(i) = field.ri(i)/norm;      

    averaged.set_averageNumber(1);
    
    ERRORLOGL(4,PAR(averaged.get_name()));
    return;
  }
  
  int averageNumber = averaged.get_averageNumber();

  ++(averaged.set_averageNumber());

  for(unsigned int i=0;i<averaged.get_sizeR();i++) 
    averaged.ri(i) = float(averageNumber-1)/averageNumber*averaged.ri(i) + field.ri(i)/(averageNumber*norm);
  
  ERRORLOGL(3,BK::appendString("average(",averaged.get_name(),", ",field.get_name(),")-end"));
}

template<class T>
double maxAbsOf3DVectorField(const T& vx, const T& vy, const T& vz)
{
  ERRORLOGL(3,"maxAbsOf3DVectorField");

  auto fftBase = vx.get_base();
  
  double max = 0;
  for(int x=0;x<fftBase->get_nxR();x++){
    for(int y=0;y<fftBase->get_nyR();y++){
      for(int z=0;z<fftBase->get_nzR();z++){
	double temp = BK::sqr(vx.r(x,y,z))+BK::sqr(vy.r(x,y,z))+BK::sqr(vz.r(x,y,z));
	if(temp > max)
	  max = temp;
      }
    }
  }

  double sqrtMax = sqrt(max);
  ERRORLOGL(4,"maxAbsOf3DVectorField-end");

  return sqrtMax;
}

template<class T>
double globalMaxAbsOf3DVectorField(const T& vx, const T& vy, const T& vz)
{
  ERRORLOGL(3,"globalMaxAbsOf3DVectorField");

  ERRORLOGL(4,"gathering cfl numbers ...");

  double max = maxAbsOf3DVectorField(vx,vy,vz);
  MPI_Allreduce(MPI_IN_PLACE, &max, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
  
  ERRORLOGL(3,"globalMaxAbsOf3DVectorField-end");

  return max;
}

template<class T>
double maxAbsOf3DScalarField(const T& v)
{
  ERRORLOGL(3,"maxAbsOf3DScalarField");

  auto fftBase = v.get_base();
  
  double max = 0;
  for(int x=0;x<fftBase->get_nxR();x++){
    for(int y=0;y<fftBase->get_nyR();y++){
      for(int z=0;z<fftBase->get_nzR();z++){
	double temp = fabs(v.r(x,y,z));
	if(temp > max)
	  max = temp;
      }
    }
  }

  ERRORLOGL(4,"maxAbsOf3DScalarField-end");

  return max;
}

template<class T>
double globalMaxAbsOf3DScalarField(const T& v)
{
  ERRORLOGL(3,"globalMaxAbsOf3DScalarField");

  ERRORLOGL(4,"gathering cfl numbers ...");

  double max = maxAbsOf3DScalarField(v);
  MPI_Allreduce(MPI_IN_PLACE, &max, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
  
  ERRORLOGL(3,"globalMaxAbsOf3DVectorField-end");

  return max;
}

} // namespace BK

#endif
