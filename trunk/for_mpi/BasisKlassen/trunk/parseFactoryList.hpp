#ifndef parseFactoryList_hpp
#define parseFactoryList_hpp

#include <iostream>
#include <list>

#include "Factory.hpp"

namespace BK {

template<class BASE, class CONCRETE>
BASE* createObject()
{
  return new CONCRETE;
}

// -----------------------------------------------------------------------------

class Parameter {
    public:
        /// Default constructor
        Parameter () {};
        // Destructor
        virtual ~Parameter () {};

        /** @brief This method has to rebuild 
         *  the data from the input file and return the next token of th input file
         *  in a string.
         */
        virtual std::string Rebuild (std::istream& in) = 0; // abstract class Parameter

        /// Always returns true since sorting is irrelevant
        friend bool operator< (const Parameter& left, const Parameter& right) {
            return true; 
        };
}; 

/** @brief Single value parameter of some type.
 *
 *  This reads in a single value of some type TYPE from the input file
 *  and stores it under a given address. If the parameter is not encountered
 *  in the input, a default value is set.
 */
template<class TYPE>
class ParameterValue : public Parameter {
    private:
        TYPE* pValue;   ///< Pointer to the variable to set
        TYPE Default;   ///< A default value
    public:
        /// Constructor takes a pointer to the variable and a default value
        ParameterValue (TYPE* _pValue, TYPE _Default) : pValue(_pValue) { 
            SetDefault(_Default);
        };
        /// Destructor
        virtual ~ParameterValue () {};
  
        /// Sets the default value
        void SetDefault (TYPE _Default) { 
            Default = _Default;
            *pValue = Default; 
        };
  
        /// Reads the value from the stream and returns the next token
        virtual std::string Rebuild(std::istream& in){
	  in >> *pValue;
	  std::string strToken;
	  in >> strToken;
	  return strToken;
	}
}; // ParameterValue


// --------------------------------------------------------------------------------------

/** @brief Erstellt aus initfile Liste von Objekten mit Parametern
 *
 *  registerObject und makeParamMap m�ssen gestellt werden
*/

template<class BASE, class IDENTIFIER_TYPE>
class ParseFactoryList
{
public:
  ParseFactoryList(){
  }

  virtual ~ParseFactoryList(){};

  // registriert alle m�glichen Objecte an der factory
  virtual void registerObjects() = 0;

  // Identifications-Map zwischen Identifiern und Referenzen der Parameter
  virtual void makeParamMap(BASE* baseObject, std::map<std::string,Parameter*>& parameterMap) = 0;

  // Erzeugt Object zu Identifier und f�gt es objectList zu
  BASE* addObjectToList(IDENTIFIER_TYPE name){
    BASE* baseObject = factory.CreateObject(name);
     objectList.push_back(baseObject);
     return baseObject;
  }
  
  // list initfile und erzeugt enthaltene Objecte mit zugehoerigen Parametern
  void readFile(std::string filename){

    std::ifstream in; 
    in.open(filename.c_str());
    std::string identifier;
    std::string identifier2;

    in >> identifier;

    BASE* baseObject = NULL;  

    while(in.eof() != true){
      in >> identifier2;

      if(identifier2 == "{"){
	baseObject = addObjectToList(identifier);  
	std::map<std::string,Parameter*> parameterMap;
	makeParamMap(baseObject,parameterMap);
	in >> identifier2;
	while(identifier2 != "}"){
	  identifier2 = parameterMap[identifier2]->Rebuild(in);
	}
      }
      
      in >> identifier;      
      if(in.eof() == true) break;
  
    } 
  } 
  
  // List der Objecte
  std::list<BASE*> objectList; 

  // ObjectFactory, h�lt Identifier und Object-Erzeugungs-Funktion
  BK::Factory<BASE,IDENTIFIER_TYPE> factory;
};

}


#endif
