#ifndef output_hpp
#define output_hpp

#include <iostream>
#include <map>
#include <list>
#include <vector>

//#include "vector.hpp"

namespace BK {

// -------------------------------------------------------------------------------------
// Output of Containers (for example std::list, std::vector, std::map, BK::Vector, ...)
// -------------------------------------------------------------------------------------

template<class CONTAINER>
class ContainerOut;

template<class CONTAINER>
ContainerOut<CONTAINER> out(const CONTAINER& liste);

template<class CONTAINER>
class ContainerOut
{
public:
  ContainerOut(const CONTAINER& liste) : container(liste) {};
  
  const CONTAINER& container;

  //  friend std::ostream& operator<< <>(std::ostream &of, const ContainerOut<CONTAINER,TYPE>& v);
};

// output of std::map
template<class KEYTYPE, class VALUETYPE>
std::ostream& operator<<(std::ostream &of, const ContainerOut<std::map<KEYTYPE, VALUETYPE> >& v)
{
  typename std::map<KEYTYPE, VALUETYPE>::const_iterator it;
  
  it = v.container.begin();
  of << std::endl;
  if (it!=v.container.end())
    {
      of << it->first << "\t" << out(it->second);
      ++it;
    }
  while (it!=v.container.end())
    {
      of << std::endl << it->first << "\t" << out(it->second);
      ++it;
    }
  return of;
}

  
// Output of std::multimap
template<class KEYTYPE, class VALUETYPE>
std::ostream& operator<<(std::ostream &of, const ContainerOut<std::multimap<KEYTYPE, VALUETYPE> >& v)
{
  typename std::multimap<KEYTYPE, VALUETYPE>::const_iterator it;
  
  it = v.container.begin();
  of << std::endl;
  if (it!=v.container.end())
    {
      of << it->first << "\t" << out(it->second);
      ++it;
    }
  while (it!=v.container.end())
    {
      of << std::endl << it->first << "\t" << out(it->second);
      ++it;
    }
  return of;
}

// Output of std::pair
template<class TYPE1, class TYPE2>
std::ostream& operator<<(std::ostream &of, const ContainerOut<std::pair<TYPE1,TYPE2> >& pair)
{
  of << pair.container.first << "\t" << pair.container.second;

  return of;
}

// Output of POD
std::ostream& operator<<(std::ostream &of, const ContainerOut<double>& number);
std::ostream& operator<<(std::ostream &of, const ContainerOut<float>& number);
std::ostream& operator<<(std::ostream &of, const ContainerOut<int>& number);
std::ostream& operator<<(std::ostream &of, const ContainerOut<std::string>& number);

// Output of std::list
template<class TYPE>
std::ostream& operator<<(std::ostream &of, const ContainerOut<std::list<TYPE> >& v)
{
  typename std::list<TYPE>::const_iterator it;

  it = v.container.begin();
  if (it!=v.container.end())
    {
      of << *it;
      ++it;
    }
  while (it!=v.container.end())
    {
      of << "," << *it;
      ++it;
    }

  return of;
}

// Output of std::vector
template<class TYPE>
std::ostream& operator<<(std::ostream &of, const ContainerOut<std::vector<TYPE> >& v)
{
  typename std::vector<TYPE>::const_iterator it;

  it = v.container.begin();
  if (it!=v.container.end())
    {
      of << *it;
      ++it;
    }
  while (it!=v.container.end())
    {
      of << "," << *it;
      ++it;
    }

  return of;
}

// // Output of BK::Vector
// template<class TYPE>
// std::ostream& operator<<(std::ostream &of, const ContainerOut<Vector<TYPE> >& v)
// {
//   typename Vector<TYPE>::const_iterator it;
//   of << "(";
//   it = v.container.begin();
//   if (it!=v.container.end())
//     {
//       of << *it;
//       ++it;
//     }
//   while (it!=v.container.end())
//     {
//       of << "," << *it;
//       ++it;
//     }
//   of << ")";
//   return of;
// }

template<class CONTAINER>
std::ostream& operator<<(std::ostream &of, const ContainerOut<CONTAINER>& v)
{
//   typename CONTAINER::const_iterator it;

//   it = v.container.begin();
//   if (it!=v.container.end())
//     {
//       of << *it;
//       ++it;
//     }
//   while (it!=v.container.end())
//     {
//       of << "," << *it;
//       ++it;
//     }
  of << v.container;
  return of;
}

// function to output containers (for example std::list, std::vector, ...)
template<class CONTAINER>
ContainerOut<CONTAINER> out(const CONTAINER& liste)
{
  return ContainerOut<CONTAINER>(liste) ;
}


} // namespace BK

#endif
