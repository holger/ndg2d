#ifndef SYMMETRY_BASE_HPP
#define SYMMETRY_BASE_HPP

#include "fftBase.hpp"

namespace BK
{

    class SymmetryBase
	: public FftBase
    {
    public:

	SymmetryBase();
	~SymmetryBase();

	void init(const MpiBase&, int, int, int, 
		  double, double, double, int*);

	inline int get_processorGridDims(unsigned int i) 
	{
	    assert(i < 2); 
	    return processorGridDims_[i];
	}

    private:
	int processorGridDims_[2];
//	int loIndexR_[3];
//	int hiIndexR_[3];
//	int sizeR_[3];
//	int loIndexC_[3];
//	int hiIndexC_[3];
//	int sizeC_[3];

	int me_;
	int nProc_;
    };

}

#endif

