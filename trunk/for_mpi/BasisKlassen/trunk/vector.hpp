#ifndef BK_vector_hpp
#define BK_vector_hpp

#include <iostream>
#include <vector>
#include <initializer_list>
#include <cassert>

#include "toString.hpp"
#include "assert.hpp"
#include "vecExpression.hpp"

namespace BK {

template<class T_numtype>
class Vector : public VecExpression<Vector<T_numtype>>
{
public:

  typedef T_numtype value_type;
  
  typedef typename std::vector<T_numtype>::iterator iterator;
  typedef typename std::vector<T_numtype>::const_iterator const_iterator;
  typedef typename std::vector<T_numtype>::reverse_iterator reverse_iterator;
  typedef typename std::vector<T_numtype>::const_reverse_iterator const_reverse_iterator;
  
  Vector() { }
  
  ~Vector() { }

  // A Vec can be constructed from any VecExpression, forcing its evaluation.
  template <typename E>
    Vector(VecExpression<E> const& vec)  {
    data_.resize(vec.size());
    
    for (size_t i = 0; i != vec.size(); ++i) {
      data_[i] = vec[i];
    }
  }

  Vector(size_t size) : data_(size) {};
  
  Vector(const Vector<T_numtype>& vec) : data_(vec.data_) {};
  
  Vector(std::initializer_list<T_numtype> const& initializer_list) : data_(initializer_list) {}

  Vector(std::vector<T_numtype> const& vector) : data_(vector) {};

  Vector& operator=(std::vector<T_numtype> const& vector) {
    data_ = vector.data_;

    return *this;
  }

  Vector& operator=(T_numtype const& value) {
    for(auto & val : data_)
      val = value;

    return *this;
  }

  template<typename E>
  Vector& operator=(VecExpression<E> const& vec) {
    //    std::cerr << "  Vector& operator=(VecExpression<E> const& vec)\n";
    
    for (size_t i = 0; i != vec.size(); ++i) {
      //      std::cerr << vec[i] << std::endl;
      data_[i] = vec[i];
    }
    
    return *this;
  }

  Vector(Vector<T_numtype>&& vec) noexcept = default;
  Vector& operator=(const Vector<T_numtype>& vec) = default;

  // Vector& operator-() {
  //   for(size_t i = 0; i < size(); i++)
  //     data_[i] *= (-1);

  //   return *this;
  // }

  Vector& operator=(std::initializer_list<T_numtype> const& initializer_list) {
    data_ = initializer_list;
    return *this;
  }

  Vector& operator+=(std::vector<T_numtype> const& vector) {
    for(size_t i = 0; i < size(); i++)
      data_[i] += vector[i];

    return *this;
  }

  Vector& operator-=(std::vector<T_numtype> const& vector) {
    for(size_t i = 0; i < size(); i++)
      data_[i] -= vector[i];
    
    return *this;
  }

  template <typename E>
    Vector& operator+=(VecExpression<E> const& vector) {
    for(size_t i = 0; i < size(); i++)
      data_[i] += vector[i];
    
    return *this;
  }

  template <typename E>
    Vector& operator-=(VecExpression<E> const& vector) {
    for(size_t i = 0; i < size(); i++)
      data_[i] -= vector[i];

    return *this;
  }

  Vector& operator*=(T_numtype value) {
    for(size_t i = 0; i < size(); i++)
      data_[i] *= value;

    return *this;
  }

  Vector& operator/=(T_numtype value) {
    for(size_t i = 0; i < size(); i++)
      data_[i] /= value;

    return *this;
  }

  Vector& operator+=(T_numtype value) {
    for(size_t i = 0; i < size(); i++)
      data_[i] += value;

    return *this;
  }

  Vector& operator-=(T_numtype value) {
    for(size_t i = 0; i < size(); i++)
      data_[i] -= value;

    return *this;
  }

  T_numtype operator[](size_t i) const
  {
    ASSERT((i >= 0) && (i< size()),
	   toString("ERROR in BK::vector: i = ", i, " < 0 or >= size() = ", size()));
    
    return data_[i];
  }

  T_numtype& operator[](size_t i)
  {
    ASSERT((i >= 0) && (i< size()),
	   toString("ERROR in BK::vector: i = ", i, " < 0 or >= size() = ", size()));

    return data_[i];
  }

  void push_back( const T_numtype& value ) {
    data_.push_back(value);
  }

  void pop_back() {
    data_.pop_back();
  }

  iterator erase(const_iterator pos) {
    return data_.erase(pos);
  }

  iterator erase( const_iterator first, const_iterator last ) {
    return data_.erase(first, last);
  }

  template< class... Args >
  void emplace_back( Args&&... args ) {
    data_.emplace_back(args...);
  }

  size_t size() const {
    return data_.size();
  }

  size_t capacity() const {
    return data_.capacity();
  }

  void shrink_to_fit() {
    data_.shrink_to_fit();
  }

  T_numtype* data() noexcept {
    return data_.data();
  }

  void resize(size_t count, T_numtype value = T_numtype()) {
    data_.resize(count, value);
  }

  void reserve(size_t new_cap) {
    data_.reserve(new_cap);
  }    
  
  void clear() {
    data_.clear();
  }

  T_numtype& back() {
    return data_.back();
  }

  T_numtype const& back() const {
    return data_.back();
  }

  size_t max_size() const {
    return data_.max_size();
  }
  
  iterator begin() noexcept {
    return data_.begin();
  }

  iterator end() noexcept {
    return data_.end();
  }

  const_iterator cbegin() const noexcept {
    return data_.cbegin();
  }

  const_iterator cend() const noexcept {
    return data_.cend();
  }

  reverse_iterator rbegin() noexcept {
    return data_.rbegin();
  }

  reverse_iterator rend() noexcept {
    return data_.rend();
  }

  const_reverse_iterator crbegin() const noexcept {
    return data_.crbegin();
  }
  
  const_reverse_iterator crend() const noexcept {
    return data_.crend();
  }
  
private:
  std::vector<T_numtype> data_;
};

template<class TYPE>
std::ostream& operator<<(std::ostream &of, const Vector<TYPE> &v)   
{       
  of << "(";
  for (size_t i = 0; i < v.size()-1; i++)
    of << v[i] << ",";
  of << v[v.size()-1] << ")";

  return of;
}

} // namespace BK
  
#endif
