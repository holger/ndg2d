#ifndef utilities_hpp
#define utilities_hpp

#include <iostream>
#include <sstream>
#include <string>
#include <fstream>
#include <map>
#include <vector>
#include <list> 

#include "TypeManip.hpp"
#include "logger.hpp"

namespace BK {

// -----------------------------------------------------------------------------

template<class DOUBLE>
inline int round(DOUBLE a) {
  return int(a + 0.5);
}

// -----------------------------------------------------------------------------

template<class DOUBLE, class CONTAINER>
long long copyContainerToPOD(DOUBLE*& pod, const CONTAINER& container)
{
  if(container.size() == 0)
    return 0;
  
  else {
    pod = new DOUBLE[container.size()];
    long long temp = 0;
    for(typename CONTAINER::const_iterator it = container.begin(); it != container.end(); it++) {
      pod[temp] = *it;
      ++temp;
    }
    return temp;
  }
}

// -----------------------------------------------------------------------------

template<class T>
inline T max(const T& a1, const T& a2, const T& a3)
{
  T maxTemp = std::max(a1,a2);
  return std::max(maxTemp,a3);
}

// -----------------------------------------------------------------------------

template<class T>
inline T min(const T& a1, const T& a2, const T& a3)
{
  T minTemp = std::min(a1,a2);
  return std::min(minTemp,a3);
}

// -----------------------------------------------------------------------------

template <class T>
T sign(T x)
{
  if (x < 0)
    return T(-1);
  else if (x > 0)
    return T(1);
  else
    return T(0);
} 

// -----------------------------------------------------------------------------

void copyAsciiFile(std::string filenameIn, std::string filenameOut, bool assertion = false);

// -----------------------------------------------------------------------------

template<class STORAGETYPE, class TYPE>
  TYPE maxOfArray3D(const STORAGETYPE& array, int* lowIndex, int* highIndex);

// -----------------------------------------------------------------------------

// castet char* zu vorgegebenem Type ToType
template<class TOTYPE>
TOTYPE castCharToType(const char* input);

// castet std::string zu vorgegebenem Type ToType
template<class TOTYPE>
TOTYPE castCharToType(const std::string input);

// ------------------------------------------------------------------------------

#ifndef UTIL_H
// berechnet das Quadrat von input
template<class TYPE>
inline TYPE sqr(const TYPE input)
{
  return input*input;
}
#endif

// ------------------------------------------------------------------------------

inline int factorial (int num)
{
 if (num==1)
  return 1;
 return factorial(num-1)*num; // recursive call
}

// ------------------------------------------------------------------------------

template <int n>
struct Factorial {
	enum { value = n * Factorial<n - 1>::value };
};
 
template <>
struct Factorial<0> {
	enum { value = 1 };
};

// ------------------------------------------------------------------------------

template<class T>
inline T intPow(const T x, Int2Type<0>){
    return 1;
}

template<class T, int N>
inline T intPow(const T x, BK::Int2Type<N>){
  return intPow(x, Int2Type<N-1>()) * x;
}

template<int N, class T>
inline T intPow(const T x)
{
    return intPow(x, Int2Type<N>());
}

template<int A, int B>
struct Mod
{
  enum { value = A % B };
};

// -------------------------------------------------------------------------------


/* template<class T, int N> */
/* struct PowHelper */
/* { */
/*   static T pow(const T x){ */
/*     return PowHelper<T, N-1>::pow(x) * x; */
/*   } */
/* }; */
 
/* template<class T> */
/* struct PowHelper<T, 1> */
/* { */
/*   static T pow(const T x){ */
/*     return x; */
/*   } */
/* }; */
 
/* template<class T> */
/* struct PowHelper<T, 0> */
/* { */
/*   static T pow(const T x){ */
/*     return 1; */
/*   } */
/* }; */
 
/* template<int N, class T> */
/* T pow(T const x) */
/* { */
/*   return PowHelper<T, N>::pow(x); */
/* } */

// ------------------------------------------------------------------------------

// Berechnet ganzahlige Potenzen von x
template<class TYPE>
inline double intPow(TYPE x, int potenz)
{
  if(potenz == 0)
    return 1.;
  double tmp = x;
  if(potenz < 0){
    for(int i=0;i<-potenz-1;i++){
      tmp *= x;
    }
    return 1/tmp;
  }
  else
    for(int i=0;i<potenz-1;i++){
      tmp *= x;
    }
  return tmp;
}

// -------------------------------------------------------------------------------

template<class CONTAINER, class T>
typename CONTAINER::iterator stlContainerFindFirstOf(CONTAINER& container, const T& object)
{
  typename CONTAINER::iterator iterator = container.end();
  for(typename CONTAINER::iterator it = container.begin(); it != container.end(); it++) {
    if(*it == object) {
      iterator = it;
      break;
    }
  }
  return iterator;
}

// -------------------------------------------------------------------------------

bool replaceSubString(std::string& str, const std::string& subStr, const std::string& insertStr);

// -------------------------------------------------------------------------------

inline bool compareStringBegin(std::string str1, std::string str2)
{
  unsigned int number = std::min(str1.size(),str2.size());
  
  str1.resize(number);
  str2.resize(number);

  return (str1 == str2);
}

// -------------------------------------------------------------------------------

// verlängert String mit übergebenen Dingen
template <class T0>
std::string appendString(const T0 input)
{
  std::ostringstream tmpStream;
  tmpStream << input;
  return tmpStream.str();  
}

template <class T0, class T1>
std::string appendString(const T0 input, const T1 s1)
{
  std::ostringstream tmpStream;
  tmpStream << input  << s1;
  return tmpStream.str();  
}

template <class T0, class T1, class T2>
std::string appendString(const T0 input, const T1 s1, const T2 s2)
{
  std::ostringstream tmpStream;
  tmpStream << input << s1 << s2;
  return tmpStream.str();  
}

template <class T0, class T1, class T2, class T3>
std::string appendString(const T0 input, const T1 s1, const T2 s2, const T3 s3)
{
  std::ostringstream tmpStream;
  tmpStream << input << s1 << s2 << s3;
  return tmpStream.str();  
}

template <class T0, class T1, class T2, class T3, class T4>
std::string appendString(const T0 input, const T1 s1, const T2 s2, const T3 s3, const T4 s4)
{
  std::ostringstream tmpStream;
  tmpStream << input << s1 << s2 << s3 << s4;
  return tmpStream.str();  
}

template <class T0, class T1, class T2, class T3, class T4, class T5>
std::string appendString(const T0 input, const T1 s1, const T2 s2, const T3 s3, const T4 s4, const T5 s5)
{
  std::ostringstream tmpStream;
  tmpStream << input << s1 << s2 << s3 << s4 << s5;
  return tmpStream.str();  
}

template <class T0, class T1, class T2, class T3, class T4, class T5, class T6>
std::string appendString(const T0 input, const T1 s1, const T2 s2, const T3 s3, const T4 s4, const T5 s5, 
			 const T6 s6)
{
  std::ostringstream tmpStream;
  tmpStream << input << s1 << s2 << s3 << s4 << s5 << s6;
  return tmpStream.str();  
}

template <class T0, class T1, class T2, class T3, class T4, class T5, class T6, class T7>
std::string appendString(const T0 input, const T1 s1, const T2 s2, const T3 s3, const T4 s4, const T5 s5, 
			 const T6 s6, const T7 s7)
{
  std::ostringstream tmpStream;
  tmpStream << input << s1 << s2 << s3 << s4 << s5 << s6 << s7;
  return tmpStream.str();  
}

template <class T0, class T1, class T2, class T3, class T4, class T5, class T6, class T7, class T8>
std::string appendString(const T0 input, const T1 s1, const T2 s2, const T3 s3, const T4 s4, const T5 s5, 
			 const T6 s6, const T7 s7, const T8 s8)
{
  std::ostringstream tmpStream;
  tmpStream << input << s1 << s2 << s3 << s4 << s5 << s6 << s7 << s8;
  return tmpStream.str();  
}

template <class T0, class T1, class T2, class T3, class T4, class T5, class T6, class T7, class T8, 
	  class T9>
std::string appendString(const T0 input, const T1 s1, const T2 s2, const T3 s3, const T4 s4, const T5 s5, 
			 const T6 s6, const T7 s7, const T8 s8, const T9 s9)
{
  std::ostringstream tmpStream;
  tmpStream << input << s1 << s2 << s3 << s4 << s5 << s6 << s7 << s8 << s9;
  return tmpStream.str();  
}

template <class T0, class T1, class T2, class T3, class T4, class T5, class T6, class T7, class T8, 
	  class T9, class T10>
std::string appendString(const T0 input, const T1 s1, const T2 s2, const T3 s3, const T4 s4, const T5 s5, 
			 const T6 s6, const T7 s7, const T8 s8, const T9 s9, const T10 s10)
{
  std::ostringstream tmpStream;
  tmpStream << input << s1 << s2 << s3 << s4 << s5 << s6 << s7 << s8 << s9 << s10;
  return tmpStream.str();  
}

template <class T0, class T1, class T2, class T3, class T4, class T5, class T6, class T7, class T8, 
	  class T9, class T10, class T11>
std::string appendString(const T0 input, const T1 s1, const T2 s2, const T3 s3, const T4 s4, const T5 s5, 
			 const T6 s6, const T7 s7, const T8 s8, const T9 s9, const T10 s10, const T11 s11)
{
  std::ostringstream tmpStream;
  tmpStream << input << s1 << s2 << s3 << s4 << s5 << s6 << s7 << s8 << s9 << s10 << s11;
  return tmpStream.str();  
}

template <class T0, class T1, class T2, class T3, class T4, class T5, class T6, class T7, class T8, 
	  class T9, class T10, class T11, class T12>
std::string appendString(const T0 input, const T1 s1, const T2 s2, const T3 s3, const T4 s4, const T5 s5, 
			 const T6 s6, const T7 s7, const T8 s8, const T9 s9, const T10 s10, const T11 s11, 
			 const T12 s12)
{
  std::ostringstream tmpStream;
  tmpStream << input << s1 << s2 << s3 << s4 << s5 << s6 << s7 << s8 << s9 << s10 << s11 << s12;
  return tmpStream.str();  
}

template <class T0, class T1, class T2, class T3, class T4, class T5, class T6, class T7, class T8, 
	  class T9, class T10, class T11, class T12, class T13>
std::string appendString(const T0 input, const T1 s1, const T2 s2, const T3 s3, const T4 s4, const T5 s5, 
			 const T6 s6, const T7 s7, const T8 s8, const T9 s9, const T10 s10, const T11 s11, 
			 const T12 s12, const T13 s13)
{
  std::ostringstream tmpStream;
  tmpStream << input << s1 << s2 << s3 << s4 << s5 << s6 << s7 << s8 << s9 << s10 << s11 << s12 
	    << s13;
  return tmpStream.str();  
}

template <class T0, class T1, class T2, class T3, class T4, class T5, class T6, class T7, class T8, 
	  class T9, class T10, class T11, class T12, class T13, class T14>
std::string appendString(const T0 input, const T1 s1, const T2 s2, const T3 s3, const T4 s4, const T5 s5, 
			 const T6 s6, const T7 s7, const T8 s8, const T9 s9, const T10 s10, const T11 s11, 
			 const T12 s12, const T13 s13, const T14 s14)
{
  std::ostringstream tmpStream;
  tmpStream << input << s1 << s2 << s3 << s4 << s5 << s6 << s7 << s8 << s9 << s10 << s11 << s12 
	    << s13 << s14;
  
  return tmpStream.str();  
}

template <class T0, class T1, class T2, class T3, class T4, class T5, class T6, class T7, class T8, 
	  class T9, class T10, class T11, class T12, class T13, class T14, class T15>
std::string appendString(const T0 input, const T1 s1, const T2 s2, const T3 s3, const T4 s4, const T5 s5, 
			 const T6 s6, const T7 s7, const T8 s8, const T9 s9, const T10 s10, const T11 s11, 
			 const T12 s12, const T13 s13, const T14 s14, const T15 s15)
{
  std::ostringstream tmpStream;
  tmpStream << input << s1 << s2 << s3 << s4 << s5 << s6 << s7 << s8 << s9 << s10 << s11 << s12 
	    << s13 << s14 << s15;
  
  return tmpStream.str();  
}

template <class T0, class T1, class T2, class T3, class T4, class T5, class T6, class T7, class T8, 
	  class T9, class T10, class T11, class T12, class T13, class T14, class T15, class T16>
std::string appendString(const T0 input, const T1 s1, const T2 s2, const T3 s3, const T4 s4, const T5 s5, 
			 const T6 s6, const T7 s7, const T8 s8, const T9 s9, const T10 s10, const T11 s11, 
			 const T12 s12, const T13 s13, const T14 s14, const T15 s15, const T16 s16)
{
  std::ostringstream tmpStream;
  tmpStream << input << s1 << s2 << s3 << s4 << s5 << s6 << s7 << s8 << s9 << s10 << s11 << s12 
	    << s13 << s14 << s15 << s16;
  
  return tmpStream.str();  
}

template <class T0, class T1, class T2, class T3, class T4, class T5, class T6, class T7, class T8, 
	  class T9, class T10, class T11, class T12, class T13, class T14, class T15, class T16,
	  class T17>
std::string appendString(const T0 input, const T1 s1, const T2 s2, const T3 s3, const T4 s4, const T5 s5, 
			 const T6 s6, const T7 s7, const T8 s8, const T9 s9, const T10 s10, const T11 s11, 
			 const T12 s12, const T13 s13, const T14 s14, const T15 s15, const T16 s16,
			 const T17 s17)
{
  std::ostringstream tmpStream;
  tmpStream << input << s1 << s2 << s3 << s4 << s5 << s6 << s7 << s8 << s9 << s10 << s11 << s12 
	    << s13 << s14 << s15 << s16 << s17;
  
  return tmpStream.str();  
}

template <class T0, class T1, class T2, class T3, class T4, class T5, class T6, class T7, class T8, 
	  class T9, class T10, class T11, class T12, class T13, class T14, class T15, class T16,
	  class T17, class T18>
std::string appendString(const T0 input, const T1 s1, const T2 s2, const T3 s3, const T4 s4, const T5 s5, 
			 const T6 s6, const T7 s7, const T8 s8, const T9 s9, const T10 s10, const T11 s11, 
			 const T12 s12, const T13 s13, const T14 s14, const T15 s15, const T16 s16,
			 const T17 s17, const T18 s18)
{
  std::ostringstream tmpStream;
  tmpStream << input << s1 << s2 << s3 << s4 << s5 << s6 << s7 << s8 << s9 << s10 << s11 << s12 
	    << s13 << s14 << s15 << s16 << s17 << s18;
  
  return tmpStream.str();  
}

template <class T0, class T1, class T2, class T3, class T4, class T5, class T6, class T7, class T8, 
	  class T9, class T10, class T11, class T12, class T13, class T14, class T15, class T16,
	  class T17, class T18, class T19>
std::string appendString(const T0 input, const T1 s1, const T2 s2, const T3 s3, const T4 s4, const T5 s5, 
			 const T6 s6, const T7 s7, const T8 s8, const T9 s9, const T10 s10, const T11 s11, 
			 const T12 s12, const T13 s13, const T14 s14, const T15 s15, const T16 s16,
			 const T17 s17, const T18 s18, const T19 s19)
{
  std::ostringstream tmpStream;
  tmpStream << input << s1 << s2 << s3 << s4 << s5 << s6 << s7 << s8 << s9 << s10 << s11 << s12 
	    << s13 << s14 << s15 << s16 << s17 << s18 << s19;
  
  return tmpStream.str();  
}

template <class T0, class T1, class T2, class T3, class T4, class T5, class T6, class T7, class T8, 
	  class T9, class T10, class T11, class T12, class T13, class T14, class T15, class T16,
	  class T17, class T18, class T19, class T20>
std::string appendString(const T0 input, const T1 s1, const T2 s2, const T3 s3, const T4 s4, const T5 s5, 
			 const T6 s6, const T7 s7, const T8 s8, const T9 s9, const T10 s10, const T11 s11, 
			 const T12 s12, const T13 s13, const T14 s14, const T15 s15, const T16 s16,
			 const T17 s17, const T18 s18, const T19 s19, const T20 s20)
{
  std::ostringstream tmpStream;
  tmpStream << input << s1 << s2 << s3 << s4 << s5 << s6 << s7 << s8 << s9 << s10 << s11 << s12 
	    << s13 << s14 << s15 << s16 << s17 << s18 << s19 << s20;
  
  return tmpStream.str();  
}

template <class T0, class T1, class T2, class T3, class T4, class T5, class T6, class T7, class T8, 
	  class T9, class T10, class T11, class T12, class T13, class T14, class T15, class T16,
	  class T17, class T18, class T19, class T20, class T21>
std::string appendString(const T0 input, const T1 s1, const T2 s2, const T3 s3, const T4 s4, const T5 s5, 
			 const T6 s6, const T7 s7, const T8 s8, const T9 s9, const T10 s10, const T11 s11, 
			 const T12 s12, const T13 s13, const T14 s14, const T15 s15, const T16 s16,
			 const T17 s17, const T18 s18, const T19 s19, const T20 s20, const T21 s21)
{
  std::ostringstream tmpStream;
  tmpStream << input << s1 << s2 << s3 << s4 << s5 << s6 << s7 << s8 << s9 << s10 << s11 << s12 
	    << s13 << s14 << s15 << s16 << s17 << s18 << s19 << s20 << s21;
  
  return tmpStream.str();  
}

template <class T0, class T1, class T2, class T3, class T4, class T5, class T6, class T7, class T8, 
	  class T9, class T10, class T11, class T12, class T13, class T14, class T15, class T16,
	  class T17, class T18, class T19, class T20, class T21, class T22>
std::string appendString(const T0 input, const T1 s1, const T2 s2, const T3 s3, const T4 s4, const T5 s5, 
			 const T6 s6, const T7 s7, const T8 s8, const T9 s9, const T10 s10, const T11 s11, 
			 const T12 s12, const T13 s13, const T14 s14, const T15 s15, const T16 s16,
			 const T17 s17, const T18 s18, const T19 s19, const T20 s20, const T21 s21,
			 const T22 s22)
{
  std::ostringstream tmpStream;
  tmpStream << input << s1 << s2 << s3 << s4 << s5 << s6 << s7 << s8 << s9 << s10 << s11 << s12 
	    << s13 << s14 << s15 << s16 << s17 << s18 << s19 << s20 << s21 << s22;
  
  return tmpStream.str();  
}

template <class T0, class T1, class T2, class T3, class T4, class T5, class T6, class T7, class T8, 
	  class T9, class T10, class T11, class T12, class T13, class T14, class T15, class T16,
	  class T17, class T18, class T19, class T20, class T21, class T22, class T23>
std::string appendString(const T0 input, const T1 s1, const T2 s2, const T3 s3, const T4 s4, const T5 s5, 
			 const T6 s6, const T7 s7, const T8 s8, const T9 s9, const T10 s10, const T11 s11, 
			 const T12 s12, const T13 s13, const T14 s14, const T15 s15, const T16 s16,
			 const T17 s17, const T18 s18, const T19 s19, const T20 s20, const T21 s21,
			 const T22 s22, const T23 s23)
{
  std::ostringstream tmpStream;
  tmpStream << input << s1 << s2 << s3 << s4 << s5 << s6 << s7 << s8 << s9 << s10 << s11 << s12 
	    << s13 << s14 << s15 << s16 << s17 << s18 << s19 << s20 << s21 << s22 << s23;
  
  return tmpStream.str();  
}

template <class T0, class T1, class T2, class T3, class T4, class T5, class T6, class T7, class T8, 
	  class T9, class T10, class T11, class T12, class T13, class T14, class T15, class T16,
	  class T17, class T18, class T19, class T20, class T21, class T22, class T23, class T24>
std::string appendString(const T0 input, const T1 s1, const T2 s2, const T3 s3, const T4 s4, const T5 s5, 
			 const T6 s6, const T7 s7, const T8 s8, const T9 s9, const T10 s10, const T11 s11, 
			 const T12 s12, const T13 s13, const T14 s14, const T15 s15, const T16 s16,
			 const T17 s17, const T18 s18, const T19 s19, const T20 s20, const T21 s21,
			 const T22 s22, const T23 s23, const T24 s24)
{
  std::ostringstream tmpStream;
  tmpStream << input << s1 << s2 << s3 << s4 << s5 << s6 << s7 << s8 << s9 << s10 << s11 << s12 
	    << s13 << s14 << s15 << s16 << s17 << s18 << s19 << s20 << s21 << s22 << s23 << s24;
  
  return tmpStream.str();  
}

template <class T0, class T1, class T2, class T3, class T4, class T5, class T6, class T7, class T8, 
	  class T9, class T10, class T11, class T12, class T13, class T14, class T15, class T16,
	  class T17, class T18, class T19, class T20, class T21, class T22, class T23, class T24, class T25>
std::string appendString(const T0 input, const T1 s1, const T2 s2, const T3 s3, const T4 s4, const T5 s5, 
			 const T6 s6, const T7 s7, const T8 s8, const T9 s9, const T10 s10, const T11 s11, 
			 const T12 s12, const T13 s13, const T14 s14, const T15 s15, const T16 s16,
			 const T17 s17, const T18 s18, const T19 s19, const T20 s20, const T21 s21,
			 const T22 s22, const T23 s23, const T24 s24, const T25 s25)
{
  std::ostringstream tmpStream;
  tmpStream << input << s1 << s2 << s3 << s4 << s5 << s6 << s7 << s8 << s9 << s10 << s11 << s12 
	    << s13 << s14 << s15 << s16 << s17 << s18 << s19 << s20 << s21 << s22 << s23 << s24 << s25;
  
  return tmpStream.str();  
}

template <class T0, class T1, class T2, class T3, class T4, class T5, class T6, class T7, class T8, 
	  class T9, class T10, class T11, class T12, class T13, class T14, class T15, class T16,
	  class T17, class T18, class T19, class T20, class T21, class T22, class T23, class T24, class T25,
	  class T26>
std::string appendString(const T0 input, const T1 s1, const T2 s2, const T3 s3, const T4 s4, const T5 s5, 
			 const T6 s6, const T7 s7, const T8 s8, const T9 s9, const T10 s10, const T11 s11, 
			 const T12 s12, const T13 s13, const T14 s14, const T15 s15, const T16 s16,
			 const T17 s17, const T18 s18, const T19 s19, const T20 s20, const T21 s21,
			 const T22 s22, const T23 s23, const T24 s24, const T25 s25, const T26 s26)
{
  std::ostringstream tmpStream;
  tmpStream << input << s1 << s2 << s3 << s4 << s5 << s6 << s7 << s8 << s9 << s10 << s11 << s12 
	    << s13 << s14 << s15 << s16 << s17 << s18 << s19 << s20 << s21 << s22 << s23 << s24 << s25
	    << s26;
  
  return tmpStream.str();  
}

template <class T0, class T1, class T2, class T3, class T4, class T5, class T6, class T7, class T8, 
	  class T9, class T10, class T11, class T12, class T13, class T14, class T15, class T16,
	  class T17, class T18, class T19, class T20, class T21, class T22, class T23, class T24, class T25,
	  class T26, class T27>
std::string appendString(const T0 input, const T1 s1, const T2 s2, const T3 s3, const T4 s4, const T5 s5, 
			 const T6 s6, const T7 s7, const T8 s8, const T9 s9, const T10 s10, const T11 s11, 
			 const T12 s12, const T13 s13, const T14 s14, const T15 s15, const T16 s16,
			 const T17 s17, const T18 s18, const T19 s19, const T20 s20, const T21 s21,
			 const T22 s22, const T23 s23, const T24 s24, const T25 s25, const T26 s26,
			 const T27 s27)
{
  std::ostringstream tmpStream;
  tmpStream << input << s1 << s2 << s3 << s4 << s5 << s6 << s7 << s8 << s9 << s10 << s11 << s12 
	    << s13 << s14 << s15 << s16 << s17 << s18 << s19 << s20 << s21 << s22 << s23 << s24 << s25
	    << s26 << s27;
  
  return tmpStream.str();  
}

template <class T0, class T1, class T2, class T3, class T4, class T5, class T6, class T7, class T8, 
	  class T9, class T10, class T11, class T12, class T13, class T14, class T15, class T16,
	  class T17, class T18, class T19, class T20, class T21, class T22, class T23, class T24, class T25,
	  class T26, class T27, class T28>
std::string appendString(const T0 input, const T1 s1, const T2 s2, const T3 s3, const T4 s4, const T5 s5, 
			 const T6 s6, const T7 s7, const T8 s8, const T9 s9, const T10 s10, const T11 s11, 
			 const T12 s12, const T13 s13, const T14 s14, const T15 s15, const T16 s16,
			 const T17 s17, const T18 s18, const T19 s19, const T20 s20, const T21 s21,
			 const T22 s22, const T23 s23, const T24 s24, const T25 s25, const T26 s26,
			 const T27 s27, const T28 s28)
{
  std::ostringstream tmpStream;
  tmpStream << input << s1 << s2 << s3 << s4 << s5 << s6 << s7 << s8 << s9 << s10 << s11 << s12 
	    << s13 << s14 << s15 << s16 << s17 << s18 << s19 << s20 << s21 << s22 << s23 << s24 << s25
	    << s26 << s27 << s28;
  
  return tmpStream.str();  
}

template <class T0, class T1, class T2, class T3, class T4, class T5, class T6, class T7, class T8, 
	  class T9, class T10, class T11, class T12, class T13, class T14, class T15, class T16,
	  class T17, class T18, class T19, class T20, class T21, class T22, class T23, class T24, class T25,
	  class T26, class T27, class T28, class T29>
std::string appendString(const T0 input, const T1 s1, const T2 s2, const T3 s3, const T4 s4, const T5 s5, 
			 const T6 s6, const T7 s7, const T8 s8, const T9 s9, const T10 s10, const T11 s11, 
			 const T12 s12, const T13 s13, const T14 s14, const T15 s15, const T16 s16,
			 const T17 s17, const T18 s18, const T19 s19, const T20 s20, const T21 s21,
			 const T22 s22, const T23 s23, const T24 s24, const T25 s25, const T26 s26,
			 const T27 s27, const T28 s28, const T29 s29)
{
  std::ostringstream tmpStream;
  tmpStream << input << s1 << s2 << s3 << s4 << s5 << s6 << s7 << s8 << s9 << s10 << s11 << s12 
	    << s13 << s14 << s15 << s16 << s17 << s18 << s19 << s20 << s21 << s22 << s23 << s24 << s25
	    << s26 << s27 << s28 << s29;
  
  return tmpStream.str();  
}

template <class T0, class T1, class T2, class T3, class T4, class T5, class T6, class T7, class T8, 
	  class T9, class T10, class T11, class T12, class T13, class T14, class T15, class T16,
	  class T17, class T18, class T19, class T20, class T21, class T22, class T23, class T24, class T25,
	  class T26, class T27, class T28, class T29, class T30>
std::string appendString(const T0 input, const T1 s1, const T2 s2, const T3 s3, const T4 s4, const T5 s5, 
			 const T6 s6, const T7 s7, const T8 s8, const T9 s9, const T10 s10, const T11 s11, 
			 const T12 s12, const T13 s13, const T14 s14, const T15 s15, const T16 s16,
			 const T17 s17, const T18 s18, const T19 s19, const T20 s20, const T21 s21,
			 const T22 s22, const T23 s23, const T24 s24, const T25 s25, const T26 s26,
			 const T27 s27, const T28 s28, const T29 s29, const T30 s30)
{
  std::ostringstream tmpStream;
  tmpStream << input << s1 << s2 << s3 << s4 << s5 << s6 << s7 << s8 << s9 << s10 << s11 << s12 
	    << s13 << s14 << s15 << s16 << s17 << s18 << s19 << s20 << s21 << s22 << s23 << s24 << s25
	    << s26 << s27 << s28 << s29 << s30;
  
  return tmpStream.str();  
}

template <class T0, class T1, class T2, class T3, class T4, class T5, class T6, class T7, class T8, 
	  class T9, class T10, class T11, class T12, class T13, class T14, class T15, class T16,
	  class T17, class T18, class T19, class T20, class T21, class T22, class T23, class T24, class T25,
	  class T26, class T27, class T28, class T29, class T30, class T31>
std::string appendString(const T0 input, const T1 s1, const T2 s2, const T3 s3, const T4 s4, const T5 s5, 
			 const T6 s6, const T7 s7, const T8 s8, const T9 s9, const T10 s10, const T11 s11, 
			 const T12 s12, const T13 s13, const T14 s14, const T15 s15, const T16 s16,
			 const T17 s17, const T18 s18, const T19 s19, const T20 s20, const T21 s21,
			 const T22 s22, const T23 s23, const T24 s24, const T25 s25, const T26 s26,
			 const T27 s27, const T28 s28, const T29 s29, const T30 s30, const T31 s31)
{
  std::ostringstream tmpStream;
  tmpStream << input << s1 << s2 << s3 << s4 << s5 << s6 << s7 << s8 << s9 << s10 << s11 << s12 
	    << s13 << s14 << s15 << s16 << s17 << s18 << s19 << s20 << s21 << s22 << s23 << s24 << s25
	    << s26 << s27 << s28 << s29 << s30 << s31;
  
  return tmpStream.str();  
}

template <class T0, class T1, class T2, class T3, class T4, class T5, class T6, class T7, class T8, 
	  class T9, class T10, class T11, class T12, class T13, class T14, class T15, class T16,
	  class T17, class T18, class T19, class T20, class T21, class T22, class T23, class T24, class T25,
	  class T26, class T27, class T28, class T29, class T30, class T31, class T32>
std::string appendString(const T0 input, const T1 s1, const T2 s2, const T3 s3, const T4 s4, const T5 s5, 
			 const T6 s6, const T7 s7, const T8 s8, const T9 s9, const T10 s10, const T11 s11, 
			 const T12 s12, const T13 s13, const T14 s14, const T15 s15, const T16 s16,
			 const T17 s17, const T18 s18, const T19 s19, const T20 s20, const T21 s21,
			 const T22 s22, const T23 s23, const T24 s24, const T25 s25, const T26 s26,
			 const T27 s27, const T28 s28, const T29 s29, const T30 s30, const T31 s31,
			 const T32 s32)
{
  std::ostringstream tmpStream;
  tmpStream << input << s1 << s2 << s3 << s4 << s5 << s6 << s7 << s8 << s9 << s10 << s11 << s12 
	    << s13 << s14 << s15 << s16 << s17 << s18 << s19 << s20 << s21 << s22 << s23 << s24 << s25
	    << s26 << s27 << s28 << s29 << s30 << s31 << s32;
  
  return tmpStream.str();  
}

template <class T0, class T1, class T2, class T3, class T4, class T5, class T6, class T7, class T8, 
	  class T9, class T10, class T11, class T12, class T13, class T14, class T15, class T16,
	  class T17, class T18, class T19, class T20, class T21, class T22, class T23, class T24, class T25,
	  class T26, class T27, class T28, class T29, class T30, class T31, class T32, class T33>
std::string appendString(const T0 input, const T1 s1, const T2 s2, const T3 s3, const T4 s4, const T5 s5, 
			 const T6 s6, const T7 s7, const T8 s8, const T9 s9, const T10 s10, const T11 s11, 
			 const T12 s12, const T13 s13, const T14 s14, const T15 s15, const T16 s16,
			 const T17 s17, const T18 s18, const T19 s19, const T20 s20, const T21 s21,
			 const T22 s22, const T23 s23, const T24 s24, const T25 s25, const T26 s26,
			 const T27 s27, const T28 s28, const T29 s29, const T30 s30, const T31 s31,
			 const T32 s32, const T33 s33)
{
  std::ostringstream tmpStream;
  tmpStream << input << s1 << s2 << s3 << s4 << s5 << s6 << s7 << s8 << s9 << s10 << s11 << s12 
	    << s13 << s14 << s15 << s16 << s17 << s18 << s19 << s20 << s21 << s22 << s23 << s24 << s25
	    << s26 << s27 << s28 << s29 << s30 << s31 << s32 << s33;
  
  return tmpStream.str();  
}

template <class T0, class T1, class T2, class T3, class T4, class T5, class T6, class T7, class T8, 
	  class T9, class T10, class T11, class T12, class T13, class T14, class T15, class T16,
	  class T17, class T18, class T19, class T20, class T21, class T22, class T23, class T24, class T25,
	  class T26, class T27, class T28, class T29, class T30, class T31, class T32, class T33,
	  class T34>
std::string appendString(const T0 input, const T1 s1, const T2 s2, const T3 s3, const T4 s4, const T5 s5, 
			 const T6 s6, const T7 s7, const T8 s8, const T9 s9, const T10 s10, const T11 s11, 
			 const T12 s12, const T13 s13, const T14 s14, const T15 s15, const T16 s16,
			 const T17 s17, const T18 s18, const T19 s19, const T20 s20, const T21 s21,
			 const T22 s22, const T23 s23, const T24 s24, const T25 s25, const T26 s26,
			 const T27 s27, const T28 s28, const T29 s29, const T30 s30, const T31 s31,
			 const T32 s32, const T33 s33, const T34 s34)
{
  std::ostringstream tmpStream;
  tmpStream << input << s1 << s2 << s3 << s4 << s5 << s6 << s7 << s8 << s9 << s10 << s11 << s12 
	    << s13 << s14 << s15 << s16 << s17 << s18 << s19 << s20 << s21 << s22 << s23 << s24 << s25
	    << s26 << s27 << s28 << s29 << s30 << s31 << s32 << s33 << s34;
  
  return tmpStream.str();  
}

template <class T0, class T1, class T2, class T3, class T4, class T5, class T6, class T7, class T8, 
	  class T9, class T10, class T11, class T12, class T13, class T14, class T15, class T16,
	  class T17, class T18, class T19, class T20, class T21, class T22, class T23, class T24, class T25,
	  class T26, class T27, class T28, class T29, class T30, class T31, class T32, class T33,
	  class T34, class T35>
std::string appendString(const T0 input, const T1 s1, const T2 s2, const T3 s3, const T4 s4, const T5 s5, 
			 const T6 s6, const T7 s7, const T8 s8, const T9 s9, const T10 s10, const T11 s11, 
			 const T12 s12, const T13 s13, const T14 s14, const T15 s15, const T16 s16,
			 const T17 s17, const T18 s18, const T19 s19, const T20 s20, const T21 s21,
			 const T22 s22, const T23 s23, const T24 s24, const T25 s25, const T26 s26,
			 const T27 s27, const T28 s28, const T29 s29, const T30 s30, const T31 s31,
			 const T32 s32, const T33 s33, const T34 s34, const T35 s35)
{
  std::ostringstream tmpStream;
  tmpStream << input << s1 << s2 << s3 << s4 << s5 << s6 << s7 << s8 << s9 << s10 << s11 << s12 
	    << s13 << s14 << s15 << s16 << s17 << s18 << s19 << s20 << s21 << s22 << s23 << s24 << s25
	    << s26 << s27 << s28 << s29 << s30 << s31 << s32 << s33 << s34 << s35;
  
  return tmpStream.str();  
}

template <class T0, class T1, class T2, class T3, class T4, class T5, class T6, class T7, class T8, 
	  class T9, class T10, class T11, class T12, class T13, class T14, class T15, class T16,
	  class T17, class T18, class T19, class T20, class T21, class T22, class T23, class T24, class T25,
	  class T26, class T27, class T28, class T29, class T30, class T31, class T32, class T33,
	  class T34, class T35, class T36>
std::string appendString(const T0 input, const T1 s1, const T2 s2, const T3 s3, const T4 s4, const T5 s5, 
			 const T6 s6, const T7 s7, const T8 s8, const T9 s9, const T10 s10, const T11 s11, 
			 const T12 s12, const T13 s13, const T14 s14, const T15 s15, const T16 s16,
			 const T17 s17, const T18 s18, const T19 s19, const T20 s20, const T21 s21,
			 const T22 s22, const T23 s23, const T24 s24, const T25 s25, const T26 s26,
			 const T27 s27, const T28 s28, const T29 s29, const T30 s30, const T31 s31,
			 const T32 s32, const T33 s33, const T34 s34, const T35 s35, const T36 s36)
{
  std::ostringstream tmpStream;
  tmpStream << input << s1 << s2 << s3 << s4 << s5 << s6 << s7 << s8 << s9 << s10 << s11 << s12 
	    << s13 << s14 << s15 << s16 << s17 << s18 << s19 << s20 << s21 << s22 << s23 << s24 << s25
	    << s26 << s27 << s28 << s29 << s30 << s31 << s32 << s33 << s34 << s35 << s36;
  
  return tmpStream.str();  
}

template <class T0, class T1, class T2, class T3, class T4, class T5, class T6, class T7, class T8, 
	  class T9, class T10, class T11, class T12, class T13, class T14, class T15, class T16,
	  class T17, class T18, class T19, class T20, class T21, class T22, class T23, class T24, class T25,
	  class T26, class T27, class T28, class T29, class T30, class T31, class T32, class T33,
	  class T34, class T35, class T36, class T37>
std::string appendString(const T0 input, const T1 s1, const T2 s2, const T3 s3, const T4 s4, const T5 s5, 
			 const T6 s6, const T7 s7, const T8 s8, const T9 s9, const T10 s10, const T11 s11, 
			 const T12 s12, const T13 s13, const T14 s14, const T15 s15, const T16 s16,
			 const T17 s17, const T18 s18, const T19 s19, const T20 s20, const T21 s21,
			 const T22 s22, const T23 s23, const T24 s24, const T25 s25, const T26 s26,
			 const T27 s27, const T28 s28, const T29 s29, const T30 s30, const T31 s31,
			 const T32 s32, const T33 s33, const T34 s34, const T35 s35, const T36 s36,
			 const T37 s37)
{
  std::ostringstream tmpStream;
  tmpStream << input << s1 << s2 << s3 << s4 << s5 << s6 << s7 << s8 << s9 << s10 << s11 << s12 
	    << s13 << s14 << s15 << s16 << s17 << s18 << s19 << s20 << s21 << s22 << s23 << s24 << s25
	    << s26 << s27 << s28 << s29 << s30 << s31 << s32 << s33 << s34 << s35 << s36 << s37;
  
  return tmpStream.str();  
}

template <class T0, class T1, class T2, class T3, class T4, class T5, class T6, class T7, class T8, 
	  class T9, class T10, class T11, class T12, class T13, class T14, class T15, class T16,
	  class T17, class T18, class T19, class T20, class T21, class T22, class T23, class T24, class T25,
	  class T26, class T27, class T28, class T29, class T30, class T31, class T32, class T33,
	  class T34, class T35, class T36, class T37, class T38>
std::string appendString(const T0 input, const T1 s1, const T2 s2, const T3 s3, const T4 s4, const T5 s5, 
			 const T6 s6, const T7 s7, const T8 s8, const T9 s9, const T10 s10, const T11 s11, 
			 const T12 s12, const T13 s13, const T14 s14, const T15 s15, const T16 s16,
			 const T17 s17, const T18 s18, const T19 s19, const T20 s20, const T21 s21,
			 const T22 s22, const T23 s23, const T24 s24, const T25 s25, const T26 s26,
			 const T27 s27, const T28 s28, const T29 s29, const T30 s30, const T31 s31,
			 const T32 s32, const T33 s33, const T34 s34, const T35 s35, const T36 s36,
			 const T37 s37, const T38 s38)
{
  std::ostringstream tmpStream;
  tmpStream << input << s1 << s2 << s3 << s4 << s5 << s6 << s7 << s8 << s9 << s10 << s11 << s12 
	    << s13 << s14 << s15 << s16 << s17 << s18 << s19 << s20 << s21 << s22 << s23 << s24 << s25
	    << s26 << s27 << s28 << s29 << s30 << s31 << s32 << s33 << s34 << s35 << s36 << s37 << s38;
  
  return tmpStream.str();  
}

template <class T0, class T1, class T2, class T3, class T4, class T5, class T6, class T7, class T8, 
	  class T9, class T10, class T11, class T12, class T13, class T14, class T15, class T16,
	  class T17, class T18, class T19, class T20, class T21, class T22, class T23, class T24, class T25,
	  class T26, class T27, class T28, class T29, class T30, class T31, class T32, class T33,
	  class T34, class T35, class T36, class T37, class T38, class T39>
std::string appendString(const T0 input, const T1 s1, const T2 s2, const T3 s3, const T4 s4, const T5 s5, 
			 const T6 s6, const T7 s7, const T8 s8, const T9 s9, const T10 s10, const T11 s11, 
			 const T12 s12, const T13 s13, const T14 s14, const T15 s15, const T16 s16,
			 const T17 s17, const T18 s18, const T19 s19, const T20 s20, const T21 s21,
			 const T22 s22, const T23 s23, const T24 s24, const T25 s25, const T26 s26,
			 const T27 s27, const T28 s28, const T29 s29, const T30 s30, const T31 s31,
			 const T32 s32, const T33 s33, const T34 s34, const T35 s35, const T36 s36,
			 const T37 s37, const T38 s38, const T39 s39)
{
  std::ostringstream tmpStream;
  tmpStream << input << s1 << s2 << s3 << s4 << s5 << s6 << s7 << s8 << s9 << s10 << s11 << s12 
	    << s13 << s14 << s15 << s16 << s17 << s18 << s19 << s20 << s21 << s22 << s23 << s24 << s25
	    << s26 << s27 << s28 << s29 << s30 << s31 << s32 << s33 << s34 << s35 << s36 << s37 << s38
	    << s39;
  
  return tmpStream.str();  
}



// ------------------------------------------------------------------------
// definitions
// ------------------------------------------------------------------------

template<class STORAGETYPE, class TYPE>
TYPE maxOfArray3D(const STORAGETYPE& array, int* lowIndex, int* highIndex)
{
  TYPE maximum = array(lowIndex[0],lowIndex[1],lowIndex[2]);
  for(int i=lowIndex[0];i<=highIndex[0];i++){
    for(int j=lowIndex[1];j<=highIndex[1];j++){
      for(int k=lowIndex[2];k<=highIndex[2];k++){
	if(array(i,j,k) > maximum)
	  maximum = array(i,j,k);
      }
    }
  }
  return maximum;
}

template<class TOTYPE>
TOTYPE castCharToType(const char* input)
{
  std::stringstream temp;
  TOTYPE toType;  
  temp << input;
  temp >> toType;
  return toType;  
}

template<class TOTYPE>
TOTYPE castCharToType(const std::string input)
{
  std::stringstream temp;
  TOTYPE toType;  
  temp << input;
  temp >> toType;
  return toType;  
}

template<class TOTYPE>
void castStringToList(const std::string& listString, TOTYPE& list)
{
  unsigned int kommaPos1 = 0;
  unsigned int kommaPos2 = listString.find_first_of(",",0);
  unsigned int subLength = kommaPos2 - kommaPos1;
  
//   std::cerr << "kommaPos1 = " << kommaPos1 << std::endl;
//   std::cerr << "kommaPos2 = " << kommaPos2 << std::endl;
//   std::cerr << "subLength = " << subLength << std::endl;

//   std::cerr << "string::npos = " << std::string::npos << std::endl;


  while(1) {
  
    std::string subListString(listString,kommaPos1,subLength);  
    
    std::istringstream parameterStream(subListString);
//     std::cerr << "subListString = " << subListString << std::endl;
    typename TOTYPE::value_type parameter;
    parameterStream >> parameter;
    
    list.push_back(parameter);

    if(kommaPos2 > listString.size()) break;

    kommaPos1 = kommaPos2+1;
    kommaPos2 = listString.find(",",kommaPos1);
    if(int(kommaPos2) == int(std::string::npos)) 
      kommaPos2 = listString.size()+1;
    
    subLength = kommaPos2 - kommaPos1;

//     std::cerr << "kommaPos1 = " << kommaPos1 << std::endl;
//     std::cerr << "kommaPos2 = " << kommaPos2 << std::endl;
//     std::cerr << "subLength = " << subLength << std::endl;
  }

}

} // namespace BK

#endif
