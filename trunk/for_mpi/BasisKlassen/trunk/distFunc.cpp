#include "distFunc.hpp"

namespace BK {

double logFac(int x)
{
  double temp = 0;
  for(int i=1;i<=x;i++)
    temp += log(double(i));
  
  return temp;
}

} // namespace BK
