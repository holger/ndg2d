#ifndef BK_array_hpp
#define BK_array_hpp

#include <iostream>
#include <array>
#include <initializer_list>
#include <cassert>

#include "toString.hpp"
#include "assert.hpp"
#include "vecExpression.hpp"

namespace BK {

template<class T_numtype, int N_length>
class Array : public VecExpression<Array<T_numtype, N_length>>
{
public:

  typedef typename std::array<T_numtype,N_length>::iterator iterator;
  typedef typename std::array<T_numtype,N_length>::const_iterator const_iterator;
  typedef typename std::array<T_numtype,N_length>::reverse_iterator reverse_iterator;
  typedef typename std::array<T_numtype,N_length>::const_reverse_iterator const_reverse_iterator;
  
  Array() { }
  
  ~Array() { }

  // A Vec can be constructed from any VecExpression, forcing its evaluation.
  template <typename E>
    Array(VecExpression<E> const& vec)  {
    for (size_t i = 0; i != vec.size(); ++i) {
      data_[i] = vec[i];
    }
  }
  
  Array(const Array<T_numtype,N_length>& vec) : data_(vec.data_) {};
  
  Array(std::initializer_list<T_numtype> const& vec) {
    std::copy(vec.begin(), vec.end(), data_.begin());
  }

  Array(T_numtype value) {
    fill(value);
  };

  Array(std::array<T_numtype,N_length> const&  array) {
    for(size_t i = 0; i < size(); i++)
      data_[i] = array[i];
  };

  Array& operator=(std::array<T_numtype,N_length> const&  array) {
    for(size_t i = 0; i < size(); i++)
      data_[i] = array[i];

    return *this;
  }

  Array& operator=(std::initializer_list<T_numtype> const& initializer_list) {
    std::copy(initializer_list.begin(), initializer_list.end(), data_.begin());
    return *this;
  }

  Array& operator-() {
    for(size_t i = 0; i < size(); i++)
      data_[i] *= (-1);

    return *this;
  }

  Array& operator+=(std::array<T_numtype,N_length> const&  array) {
    for(size_t i = 0; i < size(); i++)
      data_[i] += array[i];

    return *this;
  }

  Array& operator-=(std::array<T_numtype,N_length> const&  array) {
    for(size_t i = 0; i < size(); i++)
      data_[i] -= array[i];
    
    return *this;
  }

  template <typename E>
    Array& operator+=(VecExpression<E> const&  array) {
    for(size_t i = 0; i < size(); i++)
      data_[i] += array[i];
    
    return *this;
  }

  template <typename E>
    Array& operator-=(VecExpression<E> const&  array) {
    for(size_t i = 0; i < size(); i++)
      data_[i] -= array[i];

    return *this;
  }

  template <typename E>
    bool operator==(VecExpression<E> const&  array) {

    for(size_t i = 0; i < size(); i++)
      if(data_[i] != array[i]) {
	return false;
      }
    
    return true;
  }

  template <typename E>
    bool operator!=(VecExpression<E> const&  array) {

    return !operator==(array);
  }

  Array& operator=(T_numtype value) {
    for(size_t i = 0; i < size(); i++)
      data_[i] = value;

    return *this;
  }

  Array& operator*=(T_numtype value) {
    for(size_t i = 0; i < size(); i++)
      data_[i] *= value;

    return *this;
  }

  Array& operator/=(T_numtype value) {
    for(size_t i = 0; i < size(); i++)
      data_[i] /= value;

    return *this;
  }

  Array& operator+=(T_numtype value) {
    for(size_t i = 0; i < size(); i++)
      data_[i] += value;

    return *this;
  }

  Array& operator-=(T_numtype value) {
    for(size_t i = 0; i < size(); i++)
      data_[i] -= value;

    return *this;
  }
  
  Array operator-(T_numtype value) {
    Array tmp(*this);
    
    for(size_t i = 0; i < size(); i++)
      tmp[i] -= value;
    
    return tmp;
  }

  void fill (const T_numtype& val) {
    data_.fill(val);
  }

  constexpr size_t size() const
  {
    return N_length;
  }

  T_numtype* data() noexcept {
    return data_.data();
  }

  T_numtype operator[](size_t i) const
  {
    ASSERT((i >= 0) && (i<N_length),
	   toString("ERROR in BK::array: i = ", i, " < 0 or >= N_length = ", N_length));
    
    return data_[i];
  }

  T_numtype& operator[](size_t i)
  {
    ASSERT((i >= 0) && (i<N_length),
	   toString("ERROR in BK::array: i = ", i, " < 0 or >= N_length = ", N_length));

    return data_[i];
  }

  iterator begin() noexcept {
    return data_.begin();
  }

  iterator end() noexcept {
    return data_.end();
  }

  const_iterator cbegin() const noexcept {
    return data_.cbegin();
  }

  const_iterator cend() const noexcept {
    return data_.cend();
  }

  reverse_iterator rbegin() noexcept {
    return data_.rbegin();
  }

  reverse_iterator rend() noexcept {
    return data_.rend();
  }

  const_reverse_iterator crbegin() const noexcept {
    return data_.crbegin();
  }
  
  const_reverse_iterator crend() const noexcept {
    return data_.crend();
  }
  
private:
  std::array<T_numtype, N_length> data_;
};

template<class TYPE, int LENGTH>
std::ostream& operator<<(std::ostream &of, const Array<TYPE, LENGTH> &v)   
{       
  of << "(";
  for (size_t i = 0; i < LENGTH-1; i++)
    of << v[i] << ",";
  of << v[LENGTH-1] << ")";

  return of;
}
  
} // namespace BK
  
#endif
