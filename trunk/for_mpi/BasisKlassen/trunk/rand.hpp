#ifndef rand_h
#define rand_h
//-----------------------------------------------------------------------------
#include <stdlib.h>
//-----------------------------------------------------------------------------
/** @file rand.h
  * @brief random number generator
  *
  * Import an external random number generator.
  */
//-----------------------------------------------------------------------------
namespace BK {
  /// seed the random number generator
  void srandf90(long seed = 0);
  /// return random number
  double ranf();

} // namespace BK

#endif
