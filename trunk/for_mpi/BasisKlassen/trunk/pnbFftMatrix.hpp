#ifndef bk_pnbFftMatrix_hpp
#define bk_pnbFftMatrix_hpp

#include <fftw3.h>

#include "matrix.hpp"
#include "pnbFftBase.hpp"

namespace BK {

// --------------------------------------------------------------------
template<class T>
class PnbFftMatrix;

template<class T>
class Fftw3Helper
{
public:
  
};

template<>
class Fftw3Helper<double>
{
public:
  typedef double fftw_complex[2];
  
  Fftw3Helper() {
    p1 = new fftw_plan;
    p2 = new fftw_plan;
  }
  
  ~Fftw3Helper() {
    fftw_destroy_plan(*p1);
    fftw_destroy_plan(*p2);
    delete p1;
    delete p2;
  }

  fftw_plan* p1;
  fftw_plan* p2;

  void createPlanC2R2(int rank, const int *n, int howmany,
		  fftw_complex *in, const int *inembed,
		  int istride, int idist,
		  fftw_complex *out, const int *onembed,
		  int ostride, int odist,
		  int sign, unsigned flags) {
    *p2=fftw_plan_many_dft(rank,n,howmany,in,inembed,istride,idist,out,onembed,ostride,odist,
			  FFTW_BACKWARD,FFTW_ESTIMATE);
  }

  void createPlanC2R1(int rank, const int *n, int howmany,
		      fftw_complex *in, const int *inembed,
		      int istride, int idist,
		      double *out, const int *onembed,
		      int ostride, int odist,
		      unsigned flags) {
    *p1=fftw_plan_many_dft_c2r(rank,n,howmany,in,inembed,istride,idist,out,onembed,ostride,odist,
			       FFTW_ESTIMATE);
  }

  void createPlanR2C1(int rank, const int *n, int howmany,
		      double *in, const int *inembed,
		      int istride, int idist,
		      fftw_complex *out, const int *onembed,
		      int ostride, int odist,
		      unsigned flags) {
    *p1=fftw_plan_many_dft_r2c(1,n,howmany,in,inembed,istride,idist,
			   out,onembed,ostride,odist,FFTW_ESTIMATE);
  }

  void createPlanR2C2(int rank, const int *n, int howmany,
		      fftw_complex *in, const int *inembed,
		      int istride, int idist,
		      fftw_complex *out, const int *onembed,
		      int ostride, int odist,
		      int sign, unsigned flags) {
    *p2=fftw_plan_many_dft(1,n,howmany,in,inembed,istride,idist,out,onembed,ostride,odist,FFTW_FORWARD,FFTW_ESTIMATE);
  }

  void execute1() {
    fftw_execute(*p1);
  }
  void execute2() {
    fftw_execute(*p2);
  }
};

template<>
class Fftw3Helper<float>
{
public:
  typedef float fftw_complex[2];

  Fftw3Helper() {
    p1 = new fftwf_plan;
    p2 = new fftwf_plan;
  }

  ~Fftw3Helper() {
    fftwf_destroy_plan(*p1);
    fftwf_destroy_plan(*p2);
    delete p1;
    delete p2;
  }

  fftwf_plan* p1;
  fftwf_plan* p2;

  void createPlanC2R2(int rank, const int *n, int howmany,
		  fftw_complex *in, const int *inembed,
		  int istride, int idist,
		  fftw_complex *out, const int *onembed,
		  int ostride, int odist,
		  int sign, unsigned flags) {
    *p2=fftwf_plan_many_dft(rank,n,howmany,in,inembed,istride,idist,out,onembed,ostride,odist,
			    FFTW_BACKWARD,FFTW_ESTIMATE);
  }

  void createPlanC2R1(int rank, const int *n, int howmany,
		      fftw_complex *in, const int *inembed,
		      int istride, int idist,
		      float *out, const int *onembed,
		      int ostride, int odist,
		      unsigned flags) {
    *p1=fftwf_plan_many_dft_c2r(rank,n,howmany,in,inembed,istride,idist,out,onembed,ostride,odist,
			       FFTW_ESTIMATE);
  }

  void createPlanR2C1(int rank, const int *n, int howmany,
		      float *in, const int *inembed,
		      int istride, int idist,
		      fftw_complex *out, const int *onembed,
		      int ostride, int odist,
		      unsigned flags) {
    *p1=fftwf_plan_many_dft_r2c(1,n,howmany,in,inembed,istride,idist,
			   out,onembed,ostride,odist,FFTW_ESTIMATE);
  }

  void createPlanR2C2(int rank, const int *n, int howmany,
		      fftw_complex *in, const int *inembed,
		      int istride, int idist,
		      fftw_complex *out, const int *onembed,
		      int ostride, int odist,
		      int sign, unsigned flags) {
    *p2=fftwf_plan_many_dft(1,n,howmany,in,inembed,istride,idist,out,onembed,ostride,odist,FFTW_FORWARD,FFTW_ESTIMATE);
  }
  void execute1() {
    fftwf_execute(*p1);
  }
  void execute2() {
    fftwf_execute(*p2);
  }
};

template<class T>
class PnbFftInfo : public Fftw3Helper<T>
{
public:
  int NX;
  int NY;
  int NZ;
  int Nb;

  MPI_Request* Sreq;
  MPI_Request* Rreq;
  MPI_Status* Sstat;
  MPI_Status* Rstat;

  int Nproc;
  PnbFftMatrix<T>* field;
  PnbFftMatrix<T>* tempMatrix;
};

template<class T>
class PnbFftMatrix : public Matrix<T>
{
 public:

  typedef T fftw_complex[2];

  using Matrix<T>::operator=;
  
  typedef T value_type;
  typedef PnbFftBase base_type;

  PnbFftMatrix(const std::string& name = "");
  ~PnbFftMatrix();

  PnbFftMatrix(const PnbFftMatrix& matrix);

  PnbFftMatrix(const int* l, const int* h, DataType type, const std::string& name = "");
  PnbFftMatrix(Range xRange, Range yRange, Range zRange, DataType type, const std::string& name = "");
  PnbFftMatrix(int nx, int ny, int nz, DataType type, const std::string& name = "");

  PnbFftMatrix<T>& operator=(const PnbFftMatrix<T>& matrix);

  void resize(Range xRange, Range yRange, Range zRange, DataType type,const std::string& name);
  void resize(const int* l, const int* h, DataType type, const std::string& name = "");
  void resize(int nx, int ny, int nz, DataType type, const std::string& name = "");
  
  PnbFftMatrix(PnbFftBase& base, DataType type, const std::string& name = "");

  void resize(PnbFftBase& base, DataType type, const std::string& name = "");
  
  //  void fftR(int sign, int complete); 
  void transpose_xy();
  void transpose_nb(PnbFftInfo<T>* pnbFftInfo);
  // FFT complex to real in-place 
  void fftC2R();
  // FFT real to complex in-place 
  void fftR2C();

  void fftC2R(std::list<PnbFftMatrix*> fields);
  void fftR2C(std::list<PnbFftMatrix*> fields);

  PnbFftInfo<T>* fftC2RStart(PnbFftMatrix* field);
  void fftC2RFinish(PnbFftInfo<T>* pnbFftInfo);
  PnbFftInfo<T>* fftR2CStart(PnbFftMatrix* field);
  void fftR2CFinish(PnbFftInfo<T>* pnbFftInfo);
  
  // FFT complex to real out-of-place
  template<class T2> void fftC2R(const PnbFftMatrix<T2>& from);
  // FFT real to complex out-of-place
  template<class T2> void fftR2C(const PnbFftMatrix<T2>& from);  

//   void rotateFourierModes(double strength);

  void swapXYC();
  void swapXYR();

  base_type* get_base() const {
    assert(base_ != nullptr);
    return base_;
  }

 protected:

  inline T b(int rank, int i, int j, int k) const;
  inline T& b(int rank, int i, int j, int k);

  base_type* base_;
};

// --------------------------------------------------------------------------------------------------------------------------
// definitions
// --------------------------------------------------------------------------------------------------------------------------

// #undef LOGLEVEL
// #define LOGLEVEL 4


template<class T>
PnbFftMatrix<T>::PnbFftMatrix(const std::string& name) : Matrix<T>(name)
{
  ERRORLOGL(1,"PnbFftMatrix<T>::PnbFftMatrix()");

  base_ = NULL;

  ERRORLOGL(4,"PnbFftMatrix<T>::PnbFftMatrix()-end");
}

template<class T>
PnbFftMatrix<T>::~PnbFftMatrix() 
{
  ERRORLOGL(1,"PnbFftMatrix<T>::~PnbFftMatrix()");

  ERRORLOGL(4,"PnbFftMatrix<T>::~PnbFftMatrix()-end");
}

template<class T>
PnbFftMatrix<T>::PnbFftMatrix(const PnbFftMatrix<T>& matrix) : Matrix<T>(matrix)
{
  ERRORLOGL(1,"PnbFftMatrix<T>::PnbFftMatrix(const PnbFftMatrix& matrix)");

  base_ = matrix.base_;

  ERRORLOGL(4,"PnbFftMatrix<T>::PnbFftMatrix(const PnbFftMatrix& matrix)-end");
}

template<class T>
PnbFftMatrix<T>& PnbFftMatrix<T>::operator=(const PnbFftMatrix<T>& matrix)
{
  if(matrix.existence_) { 
    BK::Matrix<T>::operator=(matrix);
    base_ = matrix.base_;
  }
  else {
    base_ = NULL;
    this->existence_ = false;
  }

  return *this;
}

template<class T>
PnbFftMatrix<T>::PnbFftMatrix(PnbFftBase& base, DataType type,const std::string& name) 
{
  ERRORLOGL(1,"PnbFftMatrix<T>::PnbFftMatrix(const PnbFftBase&, DataType,const std::string& name)");

  this->existence_ = false;
  
  this->collectedNumber_++;

  resize(base,type,name);

  ERRORLOGL(4,"PnbFftMatrix<T>::PnbFftMatrix(const PnbFftBase&, DataType)-end");
}
  
template<class T>
PnbFftMatrix<T>::PnbFftMatrix(const int* l, const int* h, DataType type, const std::string& name) 
{
  ERRORLOGL(1,"PnbFftMatrix<T>::PnbFftMatrix(const int* l, const int* h, DataType type, const std::string& name");
  
  resize(l,h,type,name);
  
  ERRORLOGL(4,"PnbFftMatrix<T>::PnbFftMatrix(const int* l, const int* h, DataType type, const std::string& name-end");
}

template<class T>
PnbFftMatrix<T>::PnbFftMatrix(Range xRange, Range yRange, Range zRange, DataType type, const std::string& name)
{
  ERRORLOGL(1,"PnbFftMatrix<T>::PnbFftMatrix(Range xRange, Range yRange, Range zRange, DataType type, const std::string& name)");
  
  resize(xRange,yRange,zRange,type,name);
  
  ERRORLOGL(4,"PnbFftMatrix<T>::PnbFftMatrix(Range xRange, Range yRange, Range zRange, DataType type, const std::string& name)-end");
}

template<class T>
PnbFftMatrix<T>::PnbFftMatrix(int nx, int ny, int nz, DataType type, const std::string& name)
{
  ERRORLOGL(1,"PnbFftMatrix<T>::PnbFftMatrix(int nx, int ny, int nz, DataType type, const std::string& name)");
  
  resize(nx,ny,nz,type,name);

  ERRORLOGL(4,"PnbFftMatrix<T>::PnbFftMatrix(int nx, int ny, int nz, DataType type, const std::string& name)-end");
}

template<class T>
void PnbFftMatrix<T>::resize(Range xRange, Range yRange, Range zRange, DataType type,const std::string& name)
{
  ERRORLOGL(1,"PnbFftMatrix<T>::resize(Range xRange, Range yRange, Range zRange, DataType type,const std::string& name)");
  
  Matrix<T>::resize(xRange,yRange,zRange,type,name);

  base_ = NULL;

  ERRORLOGL(1,"PnbFftMatrix<T>::resize(Range xRange, Range yRange, Range zRange, DataType type,const std::string& name)-end");
} 

template<class T>
void PnbFftMatrix<T>::resize(int nx, int ny, int nz, DataType type,const std::string& name)
{
  ERRORLOGL(1,"PnbFftMatrix<T>::resize(int nx, int ny, int nz,DataType type,const std::string& name)");
  
  this->existence_ = false;
  resize(Range(0,nx-1),Range(0,ny-1),Range(0,nz-1),type,name);
  
  ERRORLOGL(1,"PnbFftMatrix<T>::resize(int nx, int ny, int nz,DataType type,const std::string& name)-end");
}

template<class T>
void PnbFftMatrix<T>::resize(const int* l, const int* h, DataType type,const std::string& name)
{
  ERRORLOGL(1,"PnbFftMatrix<T>::resize(const int* l, const int* h, DataType type,const std::string& name)");

  this->existence_ = false;  

  resize(Range(l[0],h[0]),Range(l[1],h[1]),Range(l[2],h[2]),type,name);
  
  ERRORLOGL(4,"PnbFftMatrix<T>::resize(const int* l, const int* h, DataType type,const std::string& name)-end");
}

template<class T>
void PnbFftMatrix<T>::resize(PnbFftBase& base, DataType type, const std::string& name)
{
  ERRORLOGL(1,BK::appendString("PnbFftMatrix<T>::resize(PnbFftBase& base, DataType = ",type," const std::string& = ",name,")"));

  this->existence_ = false; 

  Matrix<T>::resize(base.get_nxR(),base.get_nyR(),base.get_nzR()+2,base.get_nxC(),base.get_nyC(),base.get_nzC()+1,type,name);  

  base_ = &base;
  
  ERRORLOGL(4,"PnbFftMatrix<T>::resize(PnbFftBase& base, DataType type, const std::string& name)-end");
}

template<class T>
T PnbFftMatrix<T>::b(int rank, int i, int j, int k) const
{
  
  return this->matr_fast[int(k + this->dimR[2]*(j + this->dimR[1]*i)+rank*BK::sqr(this->base_->get_nyR())*(this->base_->get_nzR()+2))]; // C-ordering
}

template<class T>
T& PnbFftMatrix<T>::b(int rank, int i, int j, int k)
{
  return this->matr_fast[int(k + this->dimR[2]*(j + this->dimR[1]*i)+rank*BK::sqr(this->base_->get_nyR())*(this->base_->get_nzR()+2))]; // C-ordering
}

template<class T>
void PnbFftMatrix<T>::transpose_xy() 
{
  ERRORLOGL(3,"PnbFftMatrix<T>::transpose_xy()");

  int NX = this->base_->get_nxR();
  int NY = this->base_->get_nyR();
  int NZ = this->base_->get_nzR();  
  

  int Nproc = this->base_->mpiBase_->get_commSize();
  
  int i, j, k, p, Nb=NX/Nproc, Nw=Nb*NY*(NZ+2);
  PnbFftMatrix<T> tempMatrix(*base_,BK::RealData);
  //  scalar c;
  MPI_Status Sstat[Nproc], Rstat[Nproc];
  MPI_Request Sreq[Nproc], Rreq[Nproc];

  ERRORLOGL(3,PAR(NX));
  ERRORLOGL(3,PAR(NY));
  ERRORLOGL(3,PAR(NZ));
  ERRORLOGL(3,PAR(Nb));
  ERRORLOGL(3,PAR(Nw));
  ERRORLOGL(3,PAR(this->dimR[2]));
  ERRORLOGL(3,PAR(tempMatrix.get_loR(0)));
  ERRORLOGL(3,PAR(tempMatrix.get_loR(1)));
  ERRORLOGL(3,PAR(tempMatrix.get_loR(2)));
  ERRORLOGL(3,PAR(tempMatrix.get_hiR(0)));
  ERRORLOGL(3,PAR(tempMatrix.get_hiR(1)));
  ERRORLOGL(3,PAR(tempMatrix.get_hiR(2)));

  /*   Block Transposition using Isend-Irecv   */

  int me = this->base_->mpiBase_->get_commRank();

  tempMatrix = 0.;

  ERRORLOGL(3,PAR(tempMatrix.dimR[0]));
  ERRORLOGL(3,PAR(tempMatrix.dimR[1]));
  ERRORLOGL(3,PAR(tempMatrix.dimR[2]));
  ERRORLOGL(3,PAR(tempMatrix.base_->get_nxR()));
  ERRORLOGL(3,PAR(tempMatrix.base_->get_nyR()));
  ERRORLOGL(3,PAR(tempMatrix.base_->get_nzR()));
  ERRORLOGL(3,PAR(tempMatrix.sizeR_));
  ERRORLOGL(3,PAR(tempMatrix.sizeC_));
  ERRORLOGL(3,PAR(Nw*sizeof(T)));

  ERRORLOGL(3,"Isend and Irecv ...");
  for(p=0; p<Nproc; p++) {   
    if(p != me)
//         { MPI_Isend(a->b[p][0][0],Nw,MPI_real,p,me,MPI_COMM_WORLD,Sreq+p);
//           MPI_Irecv(c->b[p][0][0],Nw,MPI_real,p,p,MPI_COMM_WORLD,Rreq+p);

      { 
	ERRORLOGL(3,BK::appendString(me,"  ",p,"  ",this->dimR[2]*this->base_->get_nyR()*this->base_->get_nyR()*p));
// 	MPI_Isend(&(this->matr_fast[int(this->dimR[2]*this->base_->get_nyR()*this->base_->get_nyR()*p)]),Nw*sizeof(T)/4,MPI_BYTE,p,me,MPI_COMM_WORLD,Sreq+p);
// 	MPI_Irecv(&(tempMatrix.matr_fast[int(this->dimR[2]*this->base_->get_nyR()*this->base_->get_nyR()*p)]),Nw*sizeof(T)/4,MPI_BYTE,p,p,MPI_COMM_WORLD,Rreq+p);
	MPI_Isend(&(this->matr[int(this->dimR[2]*this->base_->get_nyR()*this->base_->get_nyR()*p)]),Nw*sizeof(T),MPI_BYTE,p,me,MPI_COMM_WORLD,Sreq+p);
	MPI_Irecv(&(tempMatrix.matr[int(this->dimR[2]*this->base_->get_nyR()*this->base_->get_nyR()*p)]),Nw*sizeof(T),MPI_BYTE,p,p,MPI_COMM_WORLD,Rreq+p);
      }
  }



  ERRORLOGL(3,"transposing local blocks ...");
  for(i=0; i<Nb; i++) for(j=0; j<NY; j++) for(k=0; k<NZ; k++)
					    tempMatrix.b(me,i,j,k) = b(me,i,j,k);
  for(i=0; i<Nb; i++) for(j=0; j<NY; j++) for(k=0; k<NZ; k++)
					    b(me,i,j,k) = tempMatrix.b(me,j,i,k);


  ERRORLOGL(3,"transposing non-local blocks ...");
  for(p=0; p<Nproc; p++) {
    if(p != me) {
      MPI_Wait(Rreq+p,Rstat+p);     
      MPI_Wait(Sreq+p,Sstat+p);
      for(i=0; i<Nb; i++) 
	for(j=0; j<NY; j++) 
	  for(k=0; k<NZ; k++)
	    	    b(p,i,j,k) = tempMatrix.b(p,j,i,k);
    }
  }

}

template<class T>
void PnbFftMatrix<T>::transpose_nb(PnbFftInfo<T>* pnbFftInfo) 
{
  ERRORLOGL(3,"PnbFftMatrix<T>::transpose_nb(PnbFftInfo& pnbFftInfo)");

  int NX = this->base_->get_nxR();
  int NY = this->base_->get_nyR();
  int NZ = this->base_->get_nzR();    

  int Nproc = this->base_->mpiBase_->get_commSize();
  
  int i, j, k, p, Nb=NX/Nproc, Nw=Nb*NY*(NZ+2);

  pnbFftInfo->Nb = Nb;

  PnbFftMatrix<T>* tempMatrix = new PnbFftMatrix<T>(*base_,BK::RealData);
  
  pnbFftInfo->tempMatrix = tempMatrix;
  
  //  scalar c;
  MPI_Status* Sstat = new MPI_Status[Nproc];
  MPI_Status* Rstat = new MPI_Status[Nproc];
  MPI_Request* Sreq = new MPI_Request[Nproc];
  MPI_Request* Rreq = new MPI_Request[Nproc];

  pnbFftInfo->Sstat = Sstat;
  pnbFftInfo->Rstat = Rstat;
  pnbFftInfo->Sreq = Sreq;
  pnbFftInfo->Rreq = Rreq;

  /*   Block Transposition using Isend-Irecv   */

  int me = this->base_->mpiBase_->get_commRank();

  ERRORLOGL(3,"Isend and Irecv ...");
  for(p=0; p<Nproc; p++) {   
    if(p != me) {
      ERRORLOGL(3,BK::appendString(me,"  ",p,"  ",this->dimR[2]*this->base_->get_nyR()*this->base_->get_nyR()*p));
      MPI_Isend(&(this->matr[int(this->dimR[2]*this->base_->get_nyR()*this->base_->get_nyR()*p)]),Nw*sizeof(T),MPI_BYTE,p,me,MPI_COMM_WORLD,Sreq+p);
      MPI_Irecv(&(tempMatrix->matr[int(this->dimR[2]*this->base_->get_nyR()*this->base_->get_nyR()*p)]),Nw*sizeof(T),MPI_BYTE,p,p,MPI_COMM_WORLD,Rreq+p);
    }
  }
  
  ERRORLOGL(3,"transposing local blocks ...");
  for(i=0; i<Nb; i++) for(j=0; j<NY; j++) for(k=0; k<NZ; k++)
					    tempMatrix->b(me,i,j,k) = b(me,i,j,k);
  for(i=0; i<Nb; i++) for(j=0; j<NY; j++) for(k=0; k<NZ; k++)
					    b(me,i,j,k) = tempMatrix->b(me,j,i,k);
}

template<class T>
void PnbFftMatrix<T>::fftC2R(std::list<PnbFftMatrix<T>*> fields)
{
  if(fields.size() == 3) {
    typename std::list<PnbFftMatrix<T>*>::iterator fieldIt = fields.begin();
    PnbFftMatrix<T>* matrixP1 = *(fieldIt);
    PnbFftMatrix<T>* matrixP2 = *(++fieldIt);
    PnbFftMatrix<T>* matrixP3 = *(++fieldIt);

    BK::PnbFftInfo<T>* pnbFftInfo1;
    BK::PnbFftInfo<T>* pnbFftInfo2;
    BK::PnbFftInfo<T>* pnbFftInfo3;
    
    pnbFftInfo1 = matrixP1->fftC2RStart(matrixP1);
    pnbFftInfo2 = matrixP2->fftC2RStart(matrixP2);
    
    fftC2RFinish(pnbFftInfo1);
    
    pnbFftInfo3 = matrixP3->fftC2RStart(matrixP3);

    fftC2RFinish(pnbFftInfo2);
    
    fftC2RFinish(pnbFftInfo3);

    matrixP1->dataType_ = RealData;
    matrixP2->dataType_ = RealData;
    matrixP3->dataType_ = RealData;
  }

//   if(fields.size() >= 2) {
    
//     for(typename std::list<PnbFftMatrix<T>* >::iterator fieldIt = fields.begin(); fieldIt != fields.end(); fieldIt++) {
      
//       //       (*fieldIt)->fftC2R();
      
//       //    (*fieldIt)->fftR(1,0);
      
//       BK::PnbFftInfo<T>* pnbFftInfo;
      
//       pnbFftInfo = (*fieldIt)->fftC2RStart(*fieldIt);
      
//       fftC2RFinish(pnbFftInfo);
      
//       (*fieldIt)->dataType_ = RealData;
//     }
//   }
//   else {
//     BK::PnbFftInfo<T>* pnbFftInfo;
    
//     pnbFftInfo = (*(fields.begin()))->fftC2RStart(*(fields.begin()));
    
//     fftC2RFinish(pnbFftInfo);
    
//     (*(fields.begin()))->dataType_ = RealData;
//   }
}

template<class T>
void PnbFftMatrix<T>::fftR2C(std::list<PnbFftMatrix<T>*> fields)
{
  if(fields.size() == 3) {
    typename std::list<PnbFftMatrix<T>*>::iterator fieldIt = fields.begin();
    PnbFftMatrix<T>* matrixP1 = *(fieldIt);
    PnbFftMatrix<T>* matrixP2 = *(++fieldIt);
    PnbFftMatrix<T>* matrixP3 = *(++fieldIt);

    BK::PnbFftInfo<T>* pnbFftInfo1;
    BK::PnbFftInfo<T>* pnbFftInfo2;
    BK::PnbFftInfo<T>* pnbFftInfo3;
    
    pnbFftInfo1 = matrixP1->fftR2CStart(matrixP1);
    pnbFftInfo2 = matrixP2->fftR2CStart(matrixP2);
    
    fftR2CFinish(pnbFftInfo1);
    
    pnbFftInfo3 = matrixP3->fftR2CStart(matrixP3);

    *matrixP1 *= base_->get_scale();
    
    fftR2CFinish(pnbFftInfo2);
    
    *matrixP2 *= base_->get_scale();

    fftR2CFinish(pnbFftInfo3);

    *matrixP3 *= base_->get_scale();
    
    matrixP1->dataType_ = ComplexData;
    matrixP2->dataType_ = ComplexData;
    matrixP3->dataType_ = ComplexData;
  }
//   if(fields.size() >= 2) {
//     for(typename std::list<PnbFftMatrix<T>*>::iterator fieldIt = fields.begin(); fieldIt != fields.end(); fieldIt++) {
      
//       //    (*fieldIt)->fftR(-1,0);
//       //(*fieldIt)->fftR2C();

//       PnbFftMatrix<T>* matrixP = *fieldIt;
//       BK::PnbFftInfo<T>* pnbFftInfo;
      
//       pnbFftInfo = matrixP->fftR2CStart(matrixP);
      
//       fieldIt++;
//       PnbFftMatrix<T>* matrixP2 = *fieldIt;
//       BK::PnbFftInfo<T>* pnbFftInfo2;
//       pnbFftInfo2 = matrixP2->fftR2CStart(matrixP2);

//       fftR2CFinish(pnbFftInfo);
      
//       *matrixP *= base_->get_scale();

//       fftR2CFinish(pnbFftInfo2);

//       *matrixP2 *= base_->get_scale();
      
//       matrixP->dataType_ = ComplexData;
//       matrixP2->dataType_ = ComplexData;
//     }  
//   }
//   else {
//     BK::PnbFftInfo<T>* pnbFftInfo;
    
//     pnbFftInfo = (*(fields.begin()))->fftR2CStart(*(fields.begin()));
    
//     fftR2CFinish(pnbFftInfo);
    
//     *(*(fields.begin())) *= base_->get_scale();
    
//     (*(fields.begin()))->dataType_ = ComplexData;
//   }
}

template<class T>
void PnbFftMatrix<T>::fftC2R() 
{
  ERRORLOGL(3,"PnbFftMatrix<T>::fftC2R");

  assert(base_->get_initFlag() == true);
  assert(this->dataType_ == ComplexData);
  assert(base_ != NULL);

  BK::PnbFftInfo<T>* pnbFftInfo;

  pnbFftInfo = fftC2RStart(this);
  
  fftC2RFinish(pnbFftInfo);
//  fftR(1,0);
   
   this->dataType_ = RealData;

  ERRORLOGL(4,"PnbFftMatrix<T>::fftC2R-end");
}

template<class T>
void PnbFftMatrix<T>::fftR2C() 
{
  ERRORLOGL(3,"PnbFftMatrix<T>::fftR2C");

  assert(base_->get_initFlag() == true);
  assert(this->dataType_ == RealData);
  assert(base_ != NULL);

  BK::PnbFftInfo<T>* pnbFftInfo;

  pnbFftInfo = fftR2CStart(this);
  
  fftR2CFinish(pnbFftInfo);

//   fftR(-1,0);
   *this *= base_->get_scale();

  this->dataType_ = ComplexData;

  ERRORLOGL(4,"PnbFftMatrix<T>::fftR2C-end");
}

template<class T>
template<class T2>
void PnbFftMatrix<T>::fftR2C(const PnbFftMatrix<T2>& from) 
{
  ERRORLOGL(3,"PnbFftMatrix<T>::fftR2C(const PnbFftMatrix<T2>& from)");

  assert(base_->get_initFlag() == true);
  assert(from.get_dataType() == RealData);
  assert(base_ != NULL);
  
  this->dataType_ = RealData;

  this->operator=(from);

  BK::PnbFftInfo<T>* pnbFftInfo;

  pnbFftInfo = fftR2CStart(this);
  
  fftR2CFinish(pnbFftInfo);
  
//   fftR(-1,0);
   
   *this *= base_->get_scale();
  this->dataType_ = ComplexData;

  ERRORLOGL(4,"PnbFftMatrix<T>::fftR2C(const PnbFftMatrix<T2>& from)-end");
}

template<class T>
template<class T2>
void PnbFftMatrix<T>::fftC2R(const PnbFftMatrix<T2>& from) 
{
  ERRORLOGL(3,"PnbFftMatrix<T>::fftC2R(const PnbFftMatrix<T2>& from)");

  assert(base_->get_initFlag() == true);
  assert(from.get_dataType() == ComplexData);
  assert(base_ != NULL);

  this->dataType_ = RealData;

  this->operator=(from);

  BK::PnbFftInfo<T>* pnbFftInfo;

  pnbFftInfo = fftC2RStart(this);
  
  fftC2RFinish(pnbFftInfo);

//   fftR(1,0);

  this->dataType_ = RealData;

  ERRORLOGL(4,"PnbFftMatrix<T>::fftC2R(const PnbFftMatrix<T2>& from)-end");
}

template<class T>
PnbFftInfo<T>* PnbFftMatrix<T>::fftC2RStart(PnbFftMatrix<T>* field)
{
  ERRORLOGL(3,BK::appendString("PnbFftMatrix<T>::fftC2RStart"));

  PnbFftInfo<T>* pnbFftInfo = new PnbFftInfo<T>;

  pnbFftInfo->field = field;

  //  std::cout << field->get_name() << "  " << *field << std::endl;

  int NX = this->base_->get_nxR();
  int NY = this->base_->get_nyR();
  int NZ = this->base_->get_nzR();

  pnbFftInfo->NX = NX;
  pnbFftInfo->NY = NY;
  pnbFftInfo->NZ = NZ;

  int howmany_z=NX*NY, N_in_z[1], inembed_z[1];
  N_in_z[0]=NZ; inembed_z[0]=NZ+2;
  int istride_z=1 , idist_z=(NZ+2);
  int onembed_z[1];
  onembed_z[0]=(NZ/2+1);
  int ostride_z=1,  odist_z= (NZ/2+1);  

  int howmany_x =NY*(NZ/2+1), N_in_x[1], inembed_x[1]; 
  N_in_x[0]=NX; inembed_x[0]=NX;
  int istride_x=(NZ/2+1)*NY, idist_x=1; 
  int onembed_x[1]; onembed_x[0]=NX; 
  int ostride_x=(NZ/2+1)*NY, odist_x=1; 

//   fftw_plan* p1b = new fftw_plan;
//   fftw_plan* p2b = new fftw_plan;
  
//   pnbFftInfo->p1 = p1b;
//   pnbFftInfo->p2 = p2b;

  int Nproc = this->base_->mpiBase_->get_commSize();

  pnbFftInfo->Nproc = Nproc;

  ERRORLOGL(3,PAR(Nproc));

  this->dataType_ = RealData;
//   for(int x=get_loR(0);x<=get_hiR(0);x++) 
//     for(int y=get_loR(1);y<=get_hiR(1);y++) {
//       r(x,y,get_hiR(2)-1) = 0;
//       r(x,y,get_hiR(2)) = 0;
//     }
  
  ERRORLOGL(3,"creating p1b and p2b ...");
  pnbFftInfo->createPlanC2R2(1,N_in_x,howmany_x,reinterpret_cast<fftw_complex*>(this->dataR()),inembed_x,istride_x,idist_x,
			     reinterpret_cast<fftw_complex*>(this->dataR()),onembed_x,ostride_x,odist_x,FFTW_BACKWARD,FFTW_ESTIMATE);
  pnbFftInfo->createPlanC2R1(1,N_in_z,howmany_z,reinterpret_cast<fftw_complex*>(this->dataR()),onembed_z,ostride_z,odist_z,
			     this->dataR(),inembed_z,istride_z,idist_z,FFTW_ESTIMATE);

//   *pnbFftInfo->p2 =  fftw_plan_many_dft();
//   *pnbFftInfo->p1 = fftw_plan_many_dft_c2r();

  ERRORLOGL(3,"executing p2b ...");
  
  pnbFftInfo->execute2();
  //  fftw_execute(*pnbFftInfo->p2);
  transpose_nb(pnbFftInfo);

  return pnbFftInfo;
}

template<class T>
void PnbFftMatrix<T>::fftC2RFinish(PnbFftInfo<T>* pnbFftInfo)
{
  ERRORLOGL(3,BK::appendString("PnbFftMatrix<T>::fftC2RFinish"));

  ERRORLOGL(3,"executing p1b and p2b ...");
  
  ERRORLOGL(3,"transposing non-local blocks ...");
  int me = this->base_->mpiBase_->get_commRank();

  for(int p=0; p<pnbFftInfo->Nproc; p++) {
    if(p != me) {
      MPI_Wait(pnbFftInfo->Rreq+p,pnbFftInfo->Rstat+p);     
      MPI_Wait(pnbFftInfo->Sreq+p,pnbFftInfo->Sstat+p);
      for(int i=0; i<pnbFftInfo->Nb; i++) 
	for(int j=0; j<pnbFftInfo->NY; j++) 
	  for(int k=0; k<pnbFftInfo->NZ; k++)
	    pnbFftInfo->field->b(p,i,j,k) = pnbFftInfo->tempMatrix->b(p,j,i,k);
    }
  }

  delete pnbFftInfo->tempMatrix;
  
  pnbFftInfo->execute2();
  pnbFftInfo->execute1();
//   fftw_execute(*(pnbFftInfo->p2));
//   fftw_execute(*(pnbFftInfo->p1));
  ERRORLOGL(3,"destroying plans p1b and p2b ...");
  
  //  fftw_destroy_plan(*(pnbFftInfo->p1));fftw_destroy_plan(*(pnbFftInfo->p2));

  delete pnbFftInfo;

  //       for(int x=get_loR(0);x<=get_hiR(0);x++) 
  // 	for(int y=get_loR(1);y<=get_hiR(1);y++) {
  // 	  r(x,y,get_hiR(2)-1) = 0;
  // 	  r(x,y,get_hiR(2)) = 0;
  // 	}
  
  //zero(a);

  
  ERRORLOGL(3,BK::appendString("PnbFftMatrix<T>::fftC2RFinish-end"));
}  


template<class T>
PnbFftInfo<T>* PnbFftMatrix<T>::fftR2CStart(PnbFftMatrix<T>* field)
{
  ERRORLOGL(3,BK::appendString("PnbFftMatrix<T>::fftR2CStart"));

  PnbFftInfo<T>* pnbFftInfo = new PnbFftInfo<T>;

  pnbFftInfo->field = field;

  int NX = this->base_->get_nxR();
  int NY = this->base_->get_nyR();
  int NZ = this->base_->get_nzR();

  pnbFftInfo->NX = NX;
  pnbFftInfo->NY = NY;
  pnbFftInfo->NZ = NZ;

  int howmany_z=NX*NY, N_in_z[1], inembed_z[1];
  N_in_z[0]=NZ; inembed_z[0]=NZ+2;
  int istride_z=1 , idist_z=(NZ+2);
  int onembed_z[1];
  onembed_z[0]=(NZ/2+1);
  int ostride_z=1,  odist_z= (NZ/2+1);  

  int howmany_x =NY*(NZ/2+1), N_in_x[1], inembed_x[1]; 
  N_in_x[0]=NX; inembed_x[0]=NX;
  int istride_x=(NZ/2+1)*NY, idist_x=1; 
  int onembed_x[1]; onembed_x[0]=NX; 
  int ostride_x=(NZ/2+1)*NY, odist_x=1; 


//   fftw_plan* p1 = new fftw_plan;
//   fftw_plan* p2 = new fftw_plan;
  
//   pnbFftInfo->p1 = p1;
//   pnbFftInfo->p2 = p2;

  int Nproc = this->base_->mpiBase_->get_commSize();

  pnbFftInfo->Nproc = Nproc;

  ERRORLOGL(3,PAR(Nproc));

  ERRORLOGL(3,"creating p1 and p2 ...");
  pnbFftInfo->createPlanR2C1(1,N_in_z,howmany_z,this->dataR(),inembed_z,istride_z,idist_z,
			     reinterpret_cast<fftw_complex*>(this->dataR()),onembed_z,ostride_z,odist_z,FFTW_ESTIMATE);
  pnbFftInfo->createPlanR2C2(1,N_in_x,howmany_x,reinterpret_cast<fftw_complex*>(this->dataR()),inembed_x,istride_x,idist_x,
			     reinterpret_cast<fftw_complex*>(this->dataR()),onembed_x,ostride_x,odist_x,FFTW_FORWARD,FFTW_ESTIMATE);
  //  *pnbFftInfo->p1 = fftw_plan_many_dft_r2c();
  //  *pnbFftInfo->p2 = fftw_plan_many_dft();
  
  ERRORLOGL(3,"executing p1 and p2 ...");

  pnbFftInfo->execute1();
  pnbFftInfo->execute2();
//   fftw_execute(*p1);
//   fftw_execute(*p2);
  transpose_nb(pnbFftInfo);

  ERRORLOGL(4,BK::appendString("PnbFftMatrix<T>::fftR2CStart-end"));

  return pnbFftInfo;
}

template<class T>
void PnbFftMatrix<T>::fftR2CFinish(PnbFftInfo<T>* pnbFftInfo)
{
  ERRORLOGL(3,BK::appendString("PnbFftMatrix<T>::fftR2CFinish"));

  ERRORLOGL(3,"transposing non-local blocks ...");
  int me = this->base_->mpiBase_->get_commRank();

  for(int p=0; p<pnbFftInfo->Nproc; p++) {
    if(p != me) {
      MPI_Wait(pnbFftInfo->Rreq+p,pnbFftInfo->Rstat+p);     
      MPI_Wait(pnbFftInfo->Sreq+p,pnbFftInfo->Sstat+p);
      for(int i=0; i<pnbFftInfo->Nb; i++) 
	for(int j=0; j<pnbFftInfo->NY; j++) 
	  for(int k=0; k<pnbFftInfo->NZ; k++)
	    pnbFftInfo->field->b(p,i,j,k) = pnbFftInfo->tempMatrix->b(p,j,i,k);
    }
  }
  
  ERRORLOGL(3,"executing again p2 ...");
  pnbFftInfo->execute2();
  //  fftw_execute(*(pnbFftInfo->p2));

  ERRORLOGL(3,"destroying plans p1 and p2 ...");
  delete pnbFftInfo;
//   fftw_destroy_plan(*(pnbFftInfo->p1));
//   fftw_destroy_plan(*(pnbFftInfo->p2));
}


} // namespace BK

#endif
