#include "symmetryMatrix.hpp"

namespace BK
{

    template <>
    SymmetryInfo< double >* SymmetryMatrix< double >::fftC2RStart(SymmetryMatrix< double >* rhs)
    {
	ERRORLOGL(3,"SymmetryInfo< double >* SymmetryMatrix< double >::fftC2RStart(SymmetryMatrix< double >* rhs)");

	SymmetryInfo< double >* info = new SymmetryInfo< double >;

	int nx = this->base_->get_nxR();
	int ny = this->base_->get_nyR();
	int nz = this->base_->get_nzR();

	info->nx_ = nx;
	info->ny_ = ny;
	info->nz_ = nz;

	info->nProc_ = this->base_->mpiBase_->get_commSize();

	info->field_ = rhs;

	// select the z-component of the Symmetry type
	if(this->symmetry_ & 1)
	    info->planZ_ = this->get_plan_z_odd(SYMMETRY_BACKWARD);
	else
	    info->planZ_ = this->get_plan_z_even(SYMMETRY_BACKWARD);

	// select the y-component of the Symmetry type
	if(this->symmetry_ & 2)
	    info->planY_ = this->get_plan_x_odd(SYMMETRY_BACKWARD);
	else
	    info->planY_ = this->get_plan_x_even(SYMMETRY_BACKWARD);    

	// select the x-component of the Symmetry type
	if(this->symmetry_ & 4)
	    info->planX_ = this->get_plan_x_odd(SYMMETRY_BACKWARD);
	else
	    info->planX_ = this->get_plan_x_even(SYMMETRY_BACKWARD); 

	fftw_execute(info->planX_);
	transpose_nb(info);


	ERRORLOGL(3,"SymmetryInfo< double >* SymmetryMatrix< double >::fftC2RStart(SymmetryMatrix< double >* rhs) --> Done");
	return info;
    }

    template<>
    void SymmetryMatrix< double >::fftC2RFinish(SymmetryInfo< double >* info)
    {
	ERRORLOGL(3,"SymmetryInfo< double >* SymmetryMatrix< double >::fftC2RFinish(SymmetryInfo<double>* info)");
	int myRank = this->base_->mpiBase_->get_commRank();
	std::cerr << "Process ( " << myRank << " / " << info->nProc_ << " )" << std::endl;

	for(int p=0; p < info->nProc_; p++)
	{
	    if(p != myRank) 
	    {
		ERRORLOGL(5, "Entering MPI Wait Block");
		MPI_Wait(info->Sreq_ + p, info->Sstat_ + p);
		ERRORLOGL(5, "Leaving MPI Wait Block");
		ERRORLOGL(5, "Entering MPI Wait Block");
		MPI_Wait(info->Rreq_ + p, info->Rstat_ + p);     
		ERRORLOGL(5, "Leaving MPI Wait Block");
		for(int i = 0; i< info->nb_; i++) 
		    for(int j = 0; j<info->ny_; j++) 
			for(int k = 0; k < info->nz_; k++)
			    info->field_->b(p,i,j,k) = info->tempMatrix_->b(p,j,i,k);
	    }
	}

	delete info->tempMatrix_;

	ERRORLOGL(4, "Executing plan Y");
	fftw_execute(info->planY_);
	ERRORLOGL(4, "Executing plan Y --> Done");
	ERRORLOGL(4, "Executing plan Z");
	fftw_execute(info->planZ_);
	ERRORLOGL(4, "Executing plan Z --> Done");    

	fftw_destroy_plan(info->planX_);
	fftw_destroy_plan(info->planY_);
	fftw_destroy_plan(info->planZ_);

	ERRORLOGL(3,"SymmetryInfo< double >* SymmetryMatrix< double >::fftC2RFinish(SymmetryInfo<double>* info) --> Done");

	delete info; 
    }

    template<>
    SymmetryInfo< double >* SymmetryMatrix< double >::fftR2CStart(SymmetryMatrix< double >* rhs)
    {
	SymmetryInfo< double >* info = new SymmetryInfo< double >;

	int nx = this->base_->get_nxR();
	int ny = this->base_->get_nyR();
	int nz = this->base_->get_nzR();

	info->nx_ = nx;
	info->ny_ = ny;
	info->nz_ = nz;
    
	info->nProc_ = this->base_->mpiBase_->get_commSize();

	info->field_ = rhs;

	// select the z-component of the Symmetry type
	if(this->symmetry_ & 1)
	    info->planZ_ = this->get_plan_z_odd(SYMMETRY_FORWARD);
	else
	    info->planZ_ = this->get_plan_z_even(SYMMETRY_FORWARD);

	// select the y-component of the Symmetry type
	if(this->symmetry_ & 2)
	    info->planY_ = this->get_plan_x_odd(SYMMETRY_FORWARD);
	else
	    info->planY_ = this->get_plan_x_even(SYMMETRY_FORWARD);    

	// select the x-component of the Symmetry type
	if(this->symmetry_ & 4)
	    info->planX_ = this->get_plan_x_odd(SYMMETRY_FORWARD);
	else
	    info->planX_ = this->get_plan_x_even(SYMMETRY_FORWARD); 

	fftw_execute(info->planX_);
	fftw_execute(info->planZ_);
  
	transpose_nb(info);

	return info;
    }

    template<>
    void SymmetryMatrix< double >::fftR2CFinish(SymmetryInfo< double >* info)
    {
	int myRank = this->base_->mpiBase_->get_commRank();
	std::cerr << "Process ( " << myRank << " / " << info->nProc_ << " )" << std::endl;
	for(int p=0; p<info->nProc_; p++)
	{
	    if(p != myRank) 
	    {
		MPI_Wait(info->Rreq_+p, info->Rstat_ + p);     
		MPI_Wait(info->Sreq_+p, info->Sstat_ + p);
		for(int i = 0; i< info->nb_; i++) 
		    for(int j = 0; j<info->ny_; j++) 
			for(int k = 0; k < info->nz_; k++)
			    info->field_->b(p,i,j,k) = info->tempMatrix_->b(p,j,i,k);
	    }
	}
    
	delete info->tempMatrix_;
    
	fftw_execute(info->planY_);
    
	fftw_destroy_plan(info->planX_);
	fftw_destroy_plan(info->planY_);
	fftw_destroy_plan(info->planZ_);
    
	delete info; 
    }
}
