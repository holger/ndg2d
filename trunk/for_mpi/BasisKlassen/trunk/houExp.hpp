#ifndef houExp_hpp
#define houExp_hpp

#include <cmath>

#include "utilities.hpp"
#include "parameter.hpp"

namespace BK {
/// parameter for Hou-dealiasing
const int hou_M = 36;
const int hou_alpha = 36;

class HouExp
{
public:
  HouExp(int kMax, int alpha, int m) {kMax_ = kMax; alpha_ = alpha; m_ = m;}
  CONSTPARA(int,kMax);
  CONSTPARA(int,alpha);
  CONSTPARA(int,m);
  
  inline double exp(double i)
  {
    return std::exp(-alpha_*BK::intPow(i/kMax_,m_));
  }
};

} // namespace BK

#endif
