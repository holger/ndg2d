#ifndef notificationHandler_hpp
#define notificationHandler_hpp

#ifdef CPP11

#include <map>
#include <functional>
#include <string>
#include <list>

#include "singleton.hpp"

// notification design pattern. Use as a StackSingleton!

namespace BK {

template<typename TResult, template<class>  class CALLBACK, typename ...TArgs>
class NotificationHandler
{
public:

  friend class BK::StackSingleton<NotificationHandler<TResult,CALLBACK,TArgs...>>;

  // adds a callback (with name functionName) for a key. functionName allows to know which callbacks are registered.
  bool add(std::string key, CALLBACK<TResult(TArgs...)> callback, std::string functionName = "") {
    notifyMap_.insert(std::pair<std::string, CALLBACK<TResult(TArgs...)>>(key, callback));
    if(functionName != "")
      functionNameMap_.insert(std::pair<std::string, std::string>(key, functionName));

    return true;
  }

  // removes callback from notifyMap_
  template<typename CALLBACK2>
  bool remove(CALLBACK2 callback) {

    bool removed = false;
    
    for(auto it = notifyMap_.begin(); it != notifyMap_.end(); it++) 
      if(it->second == callback) {
	notifyMap_.erase(it);
	removed = true;
  	break;
      }

    return removed;
  }

  // executes all callback registered for a key
  void exec(std::string key, TArgs... parameter) {
    auto ret = notifyMap_.equal_range(key);
    for(auto it = ret.first; it != ret.second; it++)
      (it->second)(parameter...);

    if(ret.first != ret.second) {
      executedKeyList_.push_back(key);
      executedKeyList_.sort();
      executedKeyList_.unique();
    }
  }

  std::multimap<std::string, std::string> get_functionNameMap() const
  {
    return functionNameMap_;
  }

  std::list<std::string> get_executedKeyList() const {
    return executedKeyList_;
  }
  
  size_t get_addedFunctionNumber() const {
    return notifyMap_.size();
  }

  size_t get_numberOfFunctionNames() const {
    return functionNameMap_.size();
  }

  std::multimap<std::string, CALLBACK<TResult(TArgs...)>> get_map() const {
    return notifyMap_;
  }
    
protected:

  /// default construktor, disabled
  NotificationHandler() {};
  /// copy construktor, disabled
  NotificationHandler(const NotificationHandler&);
  /// assignement operator, disabled
  NotificationHandler& operator=(const NotificationHandler&);

  std::multimap<std::string, CALLBACK<TResult(TArgs...)>> notifyMap_;

  std::multimap<std::string, std::string> functionNameMap_;

  std::list<std::string> executedKeyList_;
  
};

// template<typename TResult, template<typename> class CALLBACK, typename ...TArgs>
// class NotificationHandler
// {
// public:

//   friend class BK::StackSingleton<NotificationHandler<TResult,CALLBACK,TArgs...>>;

//   // adds a callback (with name functionName) for a key. functionName allows to know which callbacks are registered.
//   void add(std::string key, CALLBACK<TResult(TArgs...)> callback, std::string functionName = "") {
//     notifyMap_.insert(std::pair<std::string, CALLBACK<TResult(TArgs...)>>(key, callback));
//     if(functionName != "")
//       functionNameMap_.insert(std::pair<std::string, std::string>(key, functionName));
//   }

//   // void remove(CALLBACK2<TResult(TArgs...)> callback) {
//   //   static_assert(std::is_same<CALLBACK,CALLBACK2>::value, "CALLBACK != CALLBACK2");
    
//   //   for(auto entry : notifyMap_) 
//   //     if(entry.second == callback) {
//   // 	erase(entry);
//   // 	break;
//   //     }
//   // }

//   // executes all callback registered for a key
//   void exec(std::string key, TArgs... parameter) {
//     auto ret = notifyMap_.equal_range(key);
//     for(auto it = ret.first; it != ret.second; it++)
//       (it->second)(parameter...);

//     if(ret.first != ret.second) {
//       executedKeyList_.push_back(key);
//       executedKeyList_.sort();
//       executedKeyList_.unique();
//     }
//   }

//   std::multimap<std::string, std::string> get_functionNameMap() const
//   {
//     return functionNameMap_;
//   }

//   std::list<std::string> get_executedKeyList() const {
//     return executedKeyList_;
//   }
  
//   size_t get_addedFunctionNumber() const {
//     return notifyMap_.size();
//   }

//   size_t get_numberOfFunctionNames() const {
//     return functionNameMap_.size();
//   }

// private:

//   /// default construktor, disabled
//   NotificationHandler() {};
//   /// copy construktor, disabled
//   NotificationHandler(const NotificationHandler&);
//   /// assignement operator, disabled
//   NotificationHandler& operator=(const NotificationHandler&);

//   std::multimap<std::string, CALLBACK<TResult(TArgs...)>> notifyMap_;

//   std::multimap<std::string, std::string> functionNameMap_;

//   std::list<std::string> executedKeyList_;
  
// };

} // namespace BK

#endif
#endif
