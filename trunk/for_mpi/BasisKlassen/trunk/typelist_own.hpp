#ifndef typelist_own_h
#define typelist_own_h

template<class TList, class T> struct IndexOfList;

template<class T>
struct IndexOfList<BK::NullType,T>
{
  enum {indexCounter = 0};
};

template<class Head, class Tail, class T>
struct IndexOfList<BK::Typelist<Head,Tail>,T>
{
  private:
  enum { indexCounterOfTail = IndexOfList<Tail,T>::indexCounter, 
	 listFoundFlag = indexCounterOfTail > 0 ? 1 : 0,
	 foundInHeadFlag = BK::TL::IndexOf<Head,T>::value >= 0 ? 1 : 0};
 public:
  enum {indexCounter = foundInHeadFlag > 0 ? listFoundFlag+foundInHeadFlag : indexCounterOfTail+listFoundFlag, 
	value = indexCounter-1};
};

#endif
