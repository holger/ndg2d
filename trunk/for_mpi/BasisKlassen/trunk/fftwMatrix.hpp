#ifndef fftwMatrix_hpp
#define fftwMatrix_hpp

#include "matrix.hpp"
#include "fftwBase.hpp"

namespace BK {

// --------------------------------------------------------------------

template<class T>
class FftwMatrix : public Matrix<T>
{
 public:

  using Matrix<T>::operator=;
  
  typedef T value_type;
  typedef FftwBase base_type;
  
  FftwMatrix(const std::string& name = "");
  ~FftwMatrix();

  FftwMatrix(const FftwMatrix& matrix);

  FftwMatrix(const int* l, const int* h, DataType type, const std::string& name = "");
  FftwMatrix(Range xRange, Range yRange, Range zRange, DataType type, const std::string& name = "");
  FftwMatrix(int nx, int ny, int nz, DataType type, const std::string& name = "");

  FftwMatrix<T>& operator=(const FftwMatrix<T>& matrix);
  
  void resize(Range xRange, Range yRange, Range zRange, DataType type,const std::string& name);
  void resize(const int* l, const int* h, DataType type, const std::string& name = "");
  void resize(int nx, int ny, int nz, DataType type, const std::string& name = "");
  
  FftwMatrix(FftwBase& base, DataType type, const std::string& name = "");

  void resize(FftwBase& base, DataType type, const std::string& name = "");

  void transpose_xz(FftwBase& base);
  void transpose_yz(FftwBase& base);

  inline Complex<T> skew(int x,int y,const BK::Complex<T>& c,int cx,int cy,int offset,double kz,std::vector<double> a,bool mode) const;

  // FFT complex to real in-place 
  void fftC2R();
  void fftC2Rz();
  // FFT real to complex in-place 
  void fftR2C();
  void fftR2Cz();
  // FFT complex to real out-of-place
  template<class T2> void fftC2R(const FftwMatrix<T2>& from);
  // FFT real to complex out-of-place
  template<class T2> void fftR2C(const FftwMatrix<T2>& from);  

//   void rotateFourierModes(double strength);

  void swapXYC();
  void swapXYR();

  FftwBase* get_base() const {
    assert(fftwBase_ != nullptr);
    return fftwBase_;
  }

 protected:

  CONSTPARA(fftwnd_mpi_output_order,fftwOrder);

  FftwBase* fftwBase_;
};

// --------------------------------------------------------------------------------------------------------------------------
// definitions
// --------------------------------------------------------------------------------------------------------------------------

#undef LOGLEVEL
#define LOGLEVEL 0

template<class T>
FftwMatrix<T>::FftwMatrix(const std::string& name) : Matrix<T>(name)
{
  ERRORLOGL(1,"FftwMatrix<T>::FftwMatrix()");

  fftwBase_ = NULL;

  ERRORLOGL(4,"FftwMatrix<T>::FftwMatrix()-end");
}

template<class T>
FftwMatrix<T>::~FftwMatrix() 
{
  ERRORLOGL(1,"FftwMatrix<T>::~FftwMatrix()");

  ERRORLOGL(4,"FftwMatrix<T>::~FftwMatrix()-end");
}

template<class T>
FftwMatrix<T>::FftwMatrix(const FftwMatrix<T>& matrix)
{
  ERRORLOGL(1,"FftwMatrix<T>::FftwMatrix(const FftwMatrix& matrix)");

  if(matrix.existence_) {    

    resize(*matrix.fftwBase_, matrix.dataType_, matrix.name_);

    operator=(matrix);
  }
  else {
    fftwBase_ = NULL;
    this->existence_ = false;
  }
    
  ERRORLOGL(4,"FftwMatrix<T>::FftwMatrix(const FftwMatrix& matrix)-end");
}

template<class T>
FftwMatrix<T>& FftwMatrix<T>::operator=(const FftwMatrix<T>& matrix)
{
  if(matrix.existence_) { 
    BK::Matrix<T>::operator=(matrix);
    fftwBase_ = matrix.fftwBase_;
  }
  else {
    fftwBase_ = NULL;
    this->existence_ = false;
  }

  return *this;
}

template<class T>
FftwMatrix<T>::FftwMatrix(FftwBase& base, DataType type,const std::string& name) 
{
  ERRORLOGL(1,"FftwMatrix<T>::FftwMatrix(const FftwBase&, DataType,const std::string& name)");

  this->existence_ = false;
  
  this->collectedNumber_++;

  resize(base,type,name);

  ERRORLOGL(4,"FftwMatrix<T>::FftwMatrix(const FftwBase&, DataType)-end");
}
  
template<class T>
FftwMatrix<T>::FftwMatrix(const int* l, const int* h, DataType type, const std::string& name) 
{
  ERRORLOGL(1,"FftwMatrix<T>::FftwMatrix(const int* l, const int* h, DataType type, const std::string& name");
  
  resize(l,h,type,name);
  
  ERRORLOGL(4,"FftwMatrix<T>::FftwMatrix(const int* l, const int* h, DataType type, const std::string& name-end");
}

template<class T>
FftwMatrix<T>::FftwMatrix(Range xRange, Range yRange, Range zRange, DataType type, const std::string& name)
{
  ERRORLOGL(1,"FftwMatrix<T>::FftwMatrix(Range xRange, Range yRange, Range zRange, DataType type, const std::string& name)");
  
  resize(xRange,yRange,zRange,type,name);
  
  ERRORLOGL(4,"FftwMatrix<T>::FftwMatrix(Range xRange, Range yRange, Range zRange, DataType type, const std::string& name)-end");
}

template<class T>
FftwMatrix<T>::FftwMatrix(int nx, int ny, int nz, DataType type, const std::string& name)
{
  ERRORLOGL(1,"FftwMatrix<T>::FftwMatrix(int nx, int ny, int nz, DataType type, const std::string& name)");
  
  resize(nx,ny,nz,type,name);

  ERRORLOGL(4,"FftwMatrix<T>::FftwMatrix(int nx, int ny, int nz, DataType type, const std::string& name)-end");
}

template<class T>
void FftwMatrix<T>::resize(FftwBase& base, DataType type,const std::string& name)
{
  ERRORLOGL(1,"FftwMatrix<T>::resize(const FftwBase&, DataType,const std::string& name)");

  ERRORLOGL(4,PAR(type));
  
  assert(type == RealData || type == ComplexData);

  fftwBase_ = &base;

  fftwOrder_ = fftwBase_->get_fftwOrder();

  ERRORLOGL(3,PAR(fftwOrder_));

  this->name_ = name;
  this->existence_ = false;

  if(fftwOrder_ == FFTW_NORMAL_ORDER)
    Matrix<T>::resize(fftwBase_->get_nxR(),fftwBase_->get_nyR(),fftwBase_->get_nzR()/2+1,ComplexData,name);
  
  if(fftwOrder_ == FFTW_TRANSPOSED_ORDER) {
    this->loC[0] = 0; this->loC[1] = 0; this->loC[2] = 0;
    this->hiC[0] = fftwBase_->get_globalNxR()-1; this->hiC[1] = fftwBase_->get_localNyAfterTranspose()-1; 
    this->hiC[2] = fftwBase_->get_nzR()/2;
    
    this->loR[0] = 0; this->loR[1] = 0; this->loR[2] = 0;
    this->hiR[0] = fftwBase_->get_nxR()-1; this->hiR[1] = fftwBase_->get_nyR()-1; this->hiR[2] = fftwBase_->get_nzR()+1;
  
    for(int i=0;i<3;i++) {
      this->dimR[i] = this->hiR[i]-this->loR[i]+1;
      this->dimC[i] = this->hiC[i]-this->loC[i]+1;
    }
    
    this->sizeR_ = this->dimR[0]*this->dimR[1]*this->dimR[2];
    this->sizeC_ = this->dimC[0]*this->dimC[1]*this->dimC[2];
    
    this->dataType_ = type;
    
    this->matr = new T[this->sizeR_];

    this->collectedSize_ += this->sizeR_*sizeof(T);

    if(this->matr == NULL) {
      std::cerr << "ERROR: failed to allocate mem for FftwMatrix<T>";
      abort();
    }
    
    int p = -this->loR[0];
    p = (p*this->dimR[1]-this->loR[1])*this->dimR[2] - this->loR[2];
    
    this->matr_fast = this->matr + p;
    
    this->existence_ = true;
    
    ERRORLOGL(4,PAR6(this->dimR[0],this->dimR[1],this->dimR[2],this->dimC[0],this->dimC[1],this->dimC[2]));
    ERRORLOGL(4,PAR2(this->sizeR_,this->sizeC_));
    ERRORLOGL(4,PAR6(this->loR[0],this->hiR[0],this->loR[1],this->hiR[1],this->loR[2],this->hiR[2]));
    ERRORLOGL(4,PAR6(this->loC[0],this->hiC[0],this->loC[1],this->hiC[1],this->loC[2],this->hiC[2]));
    ERRORLOGL(4,PAR(p));
    ERRORLOGL(4,PAR(this->dataType_));
  }

  if(type == RealData)
    this->dataType_ = RealData;
  
  ERRORLOGL(4,"FftwMatrix<T>::resize(const FftwBase&, DataType)-end");
}

template<class T>
void FftwMatrix<T>::resize(Range xRange, Range yRange, Range zRange, DataType type,const std::string& name)
{
  ERRORLOGL(1,"FftwMatrix<T>::resize(Range xRange, Range yRange, Range zRange, DataType type,const std::string& name)");
  
  Matrix<T>::resize(xRange,yRange,zRange,type,name);

  fftwBase_ = NULL;
  fftwOrder_ = FFTW_NORMAL_ORDER;
  
  ERRORLOGL(1,"FftwMatrix<T>::resize(Range xRange, Range yRange, Range zRange, DataType type,const std::string& name)-end");
} 

template<class T>
void FftwMatrix<T>::resize(int nx, int ny, int nz, DataType type,const std::string& name)
{
  ERRORLOGL(1,"FftwMatrix<T>::resize(int nx, int ny, int nz,DataType type,const std::string& name)");
  
  this->existence_ = false;
  resize(Range(0,nx-1),Range(0,ny-1),Range(0,nz-1),type,name);
  
  ERRORLOGL(1,"FftwMatrix<T>::resize(int nx, int ny, int nz,DataType type,const std::string& name)-end");
}

template<class T>
void FftwMatrix<T>::resize(const int* l, const int* h, DataType type,const std::string& name)
{
  ERRORLOGL(1,"FftwMatrix<T>::resize(const int* l, const int* h, DataType type,const std::string& name)");

  this->existence_ = false;  

  resize(Range(l[0],h[0]),Range(l[1],h[1]),Range(l[2],h[2]),type,name);
  
  ERRORLOGL(4,"FftwMatrix<T>::resize(const int* l, const int* h, DataType type,const std::string& name)-end");
}

// template<class T>
// void FftwMatrix<T>::rotateFourierModes(double strength)
// {
//   ERRORLOGL(3,"FftwMatrix<T>::rotateFourierModes");

//   assert(this->dataType_ == ComplexData);

//   for(int x=this->loC[0];x<=this->hiC[0];x++)
//     for(int y=this->loC[1];y<=this->hiC[1];y++)
//       for(int z=this->loC[2];z<=this->hiC[2];z++)
// 	this->c(x,y,z) *= exp(II*strength*pow(fftwBase_->k2(x,y,z),4/12.));

//   ERRORLOGL(4,"FftwMatrix<T>::rotateFourierModes-end");
// }

template<class T>
void FftwMatrix<T>::swapXYC()
{
  ERRORLOGL(3,"FftwMatrix<T>::swapXYC");
  
  BK::FftwMatrix<T> temp(*this);

  for(int x=this->loC[0];x<=this->hiC[0];x++) {
    for(int y=this->loC[1];y<=this->hiC[1];y++) {
      for(int z=this->loC[2];z<=this->hiC[2];z++) {
	reinterpret_cast<Complex<T>*>(this->matr_fast)[int(z + this->dimC[2]*(x + this->dimC[0]*y))] = temp.c(x,y,z);
      }
    }
  }
  
  ERRORLOGL(4,"FftwMatrix<T>::swapXYC-end");
}

template<class T>
void FftwMatrix<T>::swapXYR()
{
  ERRORLOGL(3,"FftwMatrix<T>::swapXYR");

  BK::FftwMatrix<T> temp(*this);

  for(int x=this->loC[0];x<=this->hiC[0];x++) {
    for(int y=this->loC[1];y<=this->hiC[1];y++) {
      for(int z=this->loC[2];z<=this->hiC[2];z++) {
	this->c(x,y,z) = reinterpret_cast<Complex<T>*>(temp.matr_fast)[int(z + this->dimC[2]*(x + this->dimC[0]*y))];
      }
    }
  }

  ERRORLOGL(4,"FftwMatrix<T>::swapXYR-end");
}

template<class T>
void FftwMatrix<T>::fftC2R()
{
  ERRORLOGL(3,"FftwMatrix<T>::fftwC2R");

  assert(fftwBase_->get_initFlag() == true);
  assert(this->dataType_ == ComplexData);
  assert(fftwBase_ != NULL);

  if(fftwOrder_ == FFTW_TRANSPOSED_ORDER)
    swapXYC();
  
  ERRORLOG(4,"allocating work-array ...");
  FftwMatrix<T> work(*fftwBase_,BK::RealData);

  ERRORLOG(4,"performing fftC2R ...");
  rfftwnd_mpi(fftwBase_->get_planC2R(), 1, this->matr, work.dataR(), fftwOrder_);

  this->dataType_ = RealData;

  ERRORLOGL(4,"FftwMatrix<T>::fftwC2R-end");
}

template<class T>
void FftwMatrix<T>::fftR2C()
{
  ERRORLOGL(3,"FftwMatrix<T>::fftR2C");

  assert(fftwBase_->get_initFlag() == true);
  assert(this->dataType_ == RealData);
  assert(fftwBase_ != NULL);

  ERRORLOG(4,"allocating work-array ...");
  FftwMatrix<T> work(*fftwBase_,BK::RealData);

  ERRORLOG(4,"performing fftR2C ...");
  rfftwnd_mpi(fftwBase_->get_planR2C(), 1, this->matr, work.dataR(), fftwOrder_);

  *this *= fftwBase_->get_scale();

  this->dataType_ = ComplexData;

  if(fftwOrder_ == FFTW_TRANSPOSED_ORDER)
    swapXYR();

  ERRORLOGL(4,"FftwMatrix<T>::fftR2C-end");
}

template<class T> inline Complex<T> FftwMatrix<T>::skew(int x,int y,const BK::Complex<T>& c,int cx,int cy,int offset,double kz,std::vector<double> a,bool mode) const
{
  return exp(II*  ((mode)?(((double)(x)-cx)*a[y+offset]):(((double)(y)+offset-cy)*a[x]))  *kz)*c;
}

template<class T>
void FftwMatrix<T>::transpose_yz(FftwBase& base)
{
  ERRORLOGL(3,"FftwMatrix<T>::transpose");

  int NX = base.get_nxR();
  int NY = base.get_nyR();
  int NZ = base.get_nzR();

//  int Nproc = base.mpiBase_->get_commSize();
//  int NprocX = base.get_processorGridDims(0);
  int NprocY = base.get_processorGridDims(1);

  
  int Nw=NX*NY*(NZ+2)/NprocY;
  FftwMatrix<T> tempMatrix(base,BK::RealData);

  ERRORLOGL(3,PAR(NX));
  ERRORLOGL(3,PAR(NY));
  ERRORLOGL(3,PAR(NZ));
  ERRORLOGL(3,PAR(Nw));
  ERRORLOGL(3,PAR(this->dimR[2]));
  ERRORLOGL(3,PAR(tempMatrix.get_loR(0)));
  ERRORLOGL(3,PAR(tempMatrix.get_loR(1)));
  ERRORLOGL(3,PAR(tempMatrix.get_loR(2)));
  ERRORLOGL(3,PAR(tempMatrix.get_hiR(0)));
  ERRORLOGL(3,PAR(tempMatrix.get_hiR(1)));
  ERRORLOGL(3,PAR(tempMatrix.get_hiR(2)));

  // Only local transpose

  tempMatrix = 0.;

  ERRORLOGL(3,PAR(tempMatrix.dimR[0]));
  ERRORLOGL(3,PAR(tempMatrix.dimR[1]));
  ERRORLOGL(3,PAR(tempMatrix.dimR[2]));
  ERRORLOGL(3,PAR(tempMatrix.base->get_nxR()));
  ERRORLOGL(3,PAR(tempMatrix.base->get_nyR()));
  ERRORLOGL(3,PAR(tempMatrix.base->get_nzR()));
  ERRORLOGL(3,PAR(tempMatrix.sizeR_));
  ERRORLOGL(3,PAR(tempMatrix.sizeC_));
  ERRORLOGL(3,PAR(Nw*sizeof(T)));


  ERRORLOGL(3,"Local transpose ...");

  for(unsigned int i=0; i<this->dimR[0]; i++)
    for(unsigned int j=0; j<this->dimR[1]; j++)
      for(unsigned int k=0; k<this->dimR[2]-2; k+=1) {
	// Z muss am langsamsten variieren, um xy-Ebenen (Bloecke) verschicken zu koennen, Y muss am schnellsten variieren
	tempMatrix.matr[int(j + this->dimR[2]*(k + this->dimR[1]*i))] = this->matr_fast[int(k + this->dimR[2]*(j + this->dimR[1]*i))];
      }
  
  for(int i=0;i<Nw*NprocY;i++) {
    this->matr[i] = tempMatrix.matr[i];
  }
  
//debug output
//int me = base.mpiBase_->get_commRank();
//if(me==1) {for(i=0;i<Nw*NprocY;i++) std::cerr<<this->matr[i]<<" "; std::cerr<<std::endl; exit(0);}

  ERRORLOGL(4,"FftwMatrix<T>::transpose-end");
}








template<class T>
void FftwMatrix<T>::transpose_xz(FftwBase& base)
{
  ERRORLOGL(3,"FftwMatrix<T>::transpose");

  int NX = base.get_nxR();
  int NY = base.get_nyR();
  int NZ = base.get_nzR();

  int Nproc = base.mpiBase_->get_commSize();
  int NprocX = base.get_processorGridDims(0);
  int NprocY = base.get_processorGridDims(1);

  
  int p;
//NbX=NX/NprocX, NbY=NY/NprocY
  int Nw=NX*NY*NZ/NprocX, Nw2=NX*NY*(NZ+2)/NprocX;
  FftwMatrix<T> tempMatrix(base,BK::RealData);
  MPI_Status Sstat[Nproc], Rstat[Nproc];
  MPI_Request Sreq[Nproc], Rreq[Nproc];

  ERRORLOGL(3,PAR(NX));
  ERRORLOGL(3,PAR(NY));
  ERRORLOGL(3,PAR(NZ));
  ERRORLOGL(3,PAR(NbX));
  ERRORLOGL(3,PAR(NbY));
  ERRORLOGL(3,PAR(Nw));
  ERRORLOGL(3,PAR(this->dimR[2]));
  ERRORLOGL(3,PAR(tempMatrix.get_loR(0)));
  ERRORLOGL(3,PAR(tempMatrix.get_loR(1)));
  ERRORLOGL(3,PAR(tempMatrix.get_loR(2)));
  ERRORLOGL(3,PAR(tempMatrix.get_hiR(0)));
  ERRORLOGL(3,PAR(tempMatrix.get_hiR(1)));
  ERRORLOGL(3,PAR(tempMatrix.get_hiR(2)));

  /*   Block Transposition using Isend-Irecv   */

  int me = base.mpiBase_->get_commRank();

  tempMatrix = 0.;

  ERRORLOGL(3,PAR(tempMatrix.dimR[0]));
  ERRORLOGL(3,PAR(tempMatrix.dimR[1]));
  ERRORLOGL(3,PAR(tempMatrix.dimR[2]));
  ERRORLOGL(3,PAR(tempMatrix.base->get_nxR()));
  ERRORLOGL(3,PAR(tempMatrix.base->get_nyR()));
  ERRORLOGL(3,PAR(tempMatrix.base->get_nzR()));
  ERRORLOGL(3,PAR(tempMatrix.sizeR_));
  ERRORLOGL(3,PAR(tempMatrix.sizeC_));
  ERRORLOGL(3,PAR(Nw*sizeof(T)));

  ERRORLOGL(3,"Local transpose ...");
  for(unsigned int i=0; i<this->dimR[0]; i++)
    for(unsigned int j=0; j<this->dimR[1]; j++)
      for(unsigned int k=0; k<this->dimR[2]-2; k++)
   {
    tempMatrix.matr[int(i + this->dimR[0]*(j + this->dimR[1]*k))] = this->matr[int(k + this->dimR[2]*(j + this->dimR[1]*i))];
    this->matr[int(k + this->dimR[2]*(j + this->dimR[1]*i))] = 0.;
   }

  ERRORLOGL(3,"Isend and Irecv ...");
  for(p=me%NprocY; p<Nproc; p+=NprocY)
   {
    if(p != me)
      { 
	MPI_Isend(&(tempMatrix.matr[int(Nw*((p-p%NprocY)/NprocY))]),Nw*sizeof(T),MPI_BYTE,p,me,MPI_COMM_WORLD,Sreq+p);
	MPI_Irecv(&(this->matr[int(Nw*((p-p%NprocY)/NprocY))]),Nw*sizeof(T),MPI_BYTE,p,p,MPI_COMM_WORLD,Rreq+p);
      }
  }

 for(int i=int(Nw*((me-me%NprocY)/NprocY));i<int(Nw*(me-me%NprocY)/NprocY+Nw);i++)
   this->matr[i] = tempMatrix.matr[i];


  for(p=me%NprocY; p<Nproc; p+=NprocY)
   {
    if(p != me)
     {
      MPI_Wait(Rreq+p,Rstat+p);     
      MPI_Wait(Sreq+p,Sstat+p);
     }
   }

  for(int i=0,j=0;i<Nw2*NprocX;i++,j++)
    tempMatrix.matr[j] = this->matr[i];

  int pos = 0;
  for(int i=0;i<Nw2*NprocX;i++)
   {
    this->matr[i] = tempMatrix.matr[pos];
    pos++;
    if(pos%NX==0)
      pos = pos-NX+Nw;
    if(pos>=Nw*NprocX)
      pos = pos-Nw*NprocX+NX;
   }

  pos = Nw*NprocX-1;
  for(int i=Nw2*NprocX-1;i>=0;i--)
   {
    if(i%(NZ+2)==NZ+1||i%(NZ+2)==NZ)
     {
      this->matr[i] = 0.;
     }
    else
     {
      this->matr[i] = this->matr[pos];
      pos--;
     }
   }

//debug output
//if(me==1) {for(i=0;i<Nw2*NprocX;i++) std::cerr<<this->matr[i]<<" "; std::cerr<<std::endl; exit(0);}

  ERRORLOGL(4,"FftwMatrix<T>::transpose-end");
}

template<class T>
void FftwMatrix<T>::fftC2Rz()
{
  ERRORLOGL(3,"FftwMatrix<T>::fftwC2Rz");

  assert(fftwBase_->get_initFlag() == true);
  assert(this->dataType_ == ComplexData);
  assert(fftwBase_ != NULL);

  if(fftwOrder_ == FFTW_TRANSPOSED_ORDER)
    swapXYC();
  
  ERRORLOG(4,"performing fftC2Rz ...");
  int n = fftwBase_->get_globalNzR();
  int howmany = fftwBase_->get_globalNxR()*fftwBase_->get_globalNyR()/fftwBase_->mpiBase_->get_commSize();
  int rank = 1;
  int ghost = 2;

  //resort
  T* temp = new T[(n+ghost)*sizeof(T)];
  for(int xy=0;xy<howmany;xy++)
   {
    for(int z=0;z<(n+ghost)/2;z++)
     {
      temp[z] = this->matr[2*z+(n+ghost)*xy];
      temp[n-z] = this->matr[2*z+1+(n+ghost)*xy];
     }
    for(int z=0;z<n+ghost-1;z++)
      this->matr[z+(n+ghost)*xy]=temp[z];
   }
  delete[] temp;

  rfftw(fftwBase_->get_planC2Rz(),howmany,this->matr,rank,n+ghost,this->matr,rank,n+ghost);

  this->dataType_ = RealData;

  ERRORLOGL(4,"FftwMatrix<T>::fftwC2Rz-end");
}

template<class T>
void FftwMatrix<T>::fftR2Cz()
{
  ERRORLOGL(3,"FftwMatrix<T>::fftR2Cz");

  assert(fftwBase_->get_initFlag() == true);
  assert(this->dataType_ == RealData);
  assert(fftwBase_ != NULL);

  ERRORLOG(4,"performing fftR2Cz ...");
  int n = fftwBase_->get_globalNzR();
  int howmany = fftwBase_->get_globalNxR()*fftwBase_->get_globalNyR()/fftwBase_->mpiBase_->get_commSize();
  int rank = 1;
  int ghost = 2;

  rfftw(fftwBase_->get_planR2Cz(),howmany,this->matr,rank,n+ghost,this->matr,rank,n+ghost);

  //resort
  T* temp = new T[(n+ghost)*sizeof(T)];
  for(int xy=0;xy<howmany;xy++)
   {
    for(int z=0;z<(n+ghost)/2;z++)
     {
      temp[2*z]=this->matr[z+(n+ghost)*xy];
      temp[2*z+1]=this->matr[n-z+(n+ghost)*xy];
     }
    for(int z=0;z<n+ghost-1;z++)
     {
      this->matr[z+(n+ghost)*xy]=temp[z];
     }
   }
  delete[] temp;

  int scaling = fftwBase_->get_globalNxR()*fftwBase_->get_globalNyR();
  *this *= fftwBase_->get_scale()*static_cast<float>(scaling);

  this->dataType_ = ComplexData;

  if(fftwOrder_ == FFTW_TRANSPOSED_ORDER)
    swapXYR();

//debug output
//if(fftwBase_->mpiBase_->get_commRank()==0) {for(int i=0;i<fftwBase_->get_nxR()*fftwBase_->get_nyR()*(fftwBase_->get_nzR()+2);i++) std::cerr<<this->matr[i]<<" "; std::cerr<<std::endl; exit(0);}

  ERRORLOGL(4,"FftwMatrix<T>::fftR2Cz-end");
}

template<class T>
template<class T2>
void FftwMatrix<T>::fftR2C(const FftwMatrix<T2>& from)
{
  ERRORLOGL(3,"FftwMatrix<T>::fftR2C(const FftwMatrix<T2>& from)");

  assert(fftwBase_->get_initFlag() == true);
  assert(from.get_dataType() == RealData);
  assert(fftwBase_ != NULL);

  this->set_dataType(BK::RealData);

  this->operator=(from);

  FftwMatrix<T> work(*fftwBase_,BK::RealData);
  rfftwnd_mpi(fftwBase_->get_planR2C(), 1, this->matr, work.dataR(), fftwOrder_);

  *this *= fftwBase_->get_scale();

  this->dataType_ = ComplexData;

  if(fftwOrder_ == FFTW_TRANSPOSED_ORDER)
    swapXYR();

  ERRORLOGL(4,"FftwMatrix<T>::fftR2C(const FftwMatrix<T>& from)-end");
}

template<class T>
template<class T2>
void FftwMatrix<T>::fftC2R(const FftwMatrix<T2>& from)
{
  ERRORLOGL(3,"FftwMatrix<T>::fftC2R(const FftwMatrix<T>& from)");

  assert(fftwBase_->get_initFlag() == true);
  assert(from.get_dataType() == ComplexData);
  assert(fftwBase_ != NULL);

  this->dataType_ = ComplexData;

  this->operator=(from);

  if(fftwOrder_ == FFTW_TRANSPOSED_ORDER)
    swapXYC();

  FftwMatrix<T> work(*fftwBase_,BK::RealData);
  rfftwnd_mpi(fftwBase_->get_planC2R(), 1, this->matr, work.dataR(), fftwOrder_);

  this->dataType_ = RealData;

  ERRORLOGL(4,"FftwMatrix<T>::fftC2R(const FftwMatrix<T>& from)-end");
}

} // namespace BK

#endif

