#include "symmetryBase.hpp"

#include "logger.hpp"

//#undef  LOGLEVEL
//#define LOGLEVEL 5

namespace BK
{

    SymmetryBase::SymmetryBase()
    {
	ERRORLOGL(4, "SymmetryBase::SymmetryBase()");
	// FIXME normierung 
	initFlag_ = false;
	ERRORLOGL(5, "SymmetryBase::SymmetryBase() --> Done");
    }

    SymmetryBase::~SymmetryBase()
    {
	ERRORLOGL(4, "SymmetryBase::~SymmetryBase()");
	ERRORLOGL(5, "SymmetryBase::~SymmetryBase() --> Done");
    }

    void SymmetryBase::init(const MpiBase& mpiBase, 
			    int nx, int ny, int nz, 
			    double lx, double ly, double lz, 
			    int* procGrid)
    {
	// FIXME 1d parallelization only
	assert( 1 == procGrid[1]);

	mpiBase_ = &mpiBase;
	commRank_= mpiBase_->get_commRank();

	processorGridDims_[0] = procGrid[0];
	processorGridDims_[1] = procGrid[1];

	fftwBlockArray_ = NULL;

	globalNxR_ = nx;
	globalNyR_ = ny;
	globalNzR_ = nz;

	globalNxC_ = nx;
	globalNyC_ = ny;
	globalNzC_ = nz;

	nxR_ = nx;
	nyR_ = ny/mpiBase_->get_commSize();
	nzR_ = nz;

	nxC_ = nx;
	nyC_ = ny/mpiBase_->get_commSize();
	nzC_ = nz;

	
	if(nxR_ == 0 || nyR_ == 0 || nzR_ == 0) 
	{
	    ERRORLOGL(0,BK::appendString("ERROR in SymmetryBase::init after FFTW-Plan-Creation: invalid size in process: ", mpiBase_->get_commRank()));
	    exit(0);
	}

	localStartR_[0] = 0;
	localStartR_[1] = mpiBase_->get_commRank()*nyR_;
	localStartR_[2] = 0;


	localStartC_[0] = 0;
	localStartC_[1] = mpiBase_->get_commRank()*nyC_;
	localStartC_[2] = 0;	

	fftwBlockArray_ = new FftwBlock[mpiBase_->get_commSize()];
	FftBase::init(mpiBase,lx,ly,lz);
	
	nProc_ = mpiBase_->get_commSize();
	me_ = mpiBase_->get_commRank();
	
	ERRORLOGL(4,PAR(nProc_));
	ERRORLOGL(4,PAR(me_));
	
	ERRORLOGL(4,"SymmetryBase::init(int nx, int ny, int nz)-end");
	
	// FIXME: set scale correctly
	scale_ = 1.0;
    }
}

