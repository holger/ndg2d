#ifndef distFunc
#define distFunc

#include <cmath>
#include <iostream>

namespace BK {

double logFac(int x);

template<class T>
T poisson(T mean, int x)
{
  double temp = -logFac(x);
  temp -= mean;
  temp += x*log(mean);
  //  std::cout << "temp = " << temp << std::endl;
  return std::exp(temp);
}

} // namespace BK

#endif
