#ifndef bk_fftwBase_hpp
#define bk_fftwBase_hpp

#include <cmath>

#ifdef Linux_ppc64
#include <rfftw_mpi.h>
#else
#include <drfftw_mpi.h>
#endif

#include "singleton.hpp"
#include "utilities.hpp"
#include "fftBase.hpp"

// class handling fft with fftw. Use as a StackSingleton!

class FieldUtils;

namespace BK {

enum FftwPlanCreationMethod {FftwEstimate,FftwMeasure};

class FftwBase : public FftBase
{
public:
  friend class BK::StackSingleton<FftwBase>;
  friend class ::FieldUtils;
  
  ~FftwBase();
  
  // initialisiert die FFTW (bestimmt lokale parameter, erzeugt FFTW-Pläne)
  void init(const MpiBase& mpiBase, int globalNx, int ny, int nz, 
	    double lx, double ly, double lz, FftwPlanCreationMethod method, 
	    fftwnd_mpi_output_order order = FFTW_NORMAL_ORDER);

  int get_processorGridDims(unsigned int i) {assert(i<2); return (1-i)*mpiBase_->get_commSize()+i;}
  

  const rfftwnd_mpi_plan& get_planR2C() { return planR2C_; }
  const rfftwnd_mpi_plan& get_planC2R() { return planC2R_; }

  const rfftw_plan& get_planR2Cz() { return planR2Cz_; }
  const rfftw_plan& get_planC2Rz() { return planC2Rz_; }

  // reelle z-Dimension fuer FFTW
  CONSTPARA(int,nzpad);


  CONSTPARA(int,localNyAfterTranspose);
  CONSTPARA(int,localYStartAfterTranspose);

  CONSTPARA(fftwnd_mpi_output_order,fftwOrder);

private:
  FftwBase();
  
  // FFTW-Plaene
  rfftwnd_mpi_plan planR2C_;
  rfftwnd_mpi_plan planC2R_;

  rfftw_plan planR2Cz_;
  rfftw_plan planC2Rz_;

};

} // namespace BK;

#endif
