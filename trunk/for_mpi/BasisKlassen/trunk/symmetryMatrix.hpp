#ifndef SYMMETRY_MATRIX_HPP
#define SYMMETRY_MATRIX_HPP

#include <fftw3.h>

#include "matrix.hpp"
#include "range.hpp"
#include "symmetryBase.hpp"
#include "logger.hpp"

#undef LOGLEVEL
#define LOGLEVEL 5

namespace BK
{

    template<typename T> class SymmetryMatrix;

    template< typename T >
    struct SymmetryInfo
    {
	int nx_;
	int nb_;
	int ny_;
	int nz_;
  
	fftw_plan planX_;
	fftw_plan planY_;
	fftw_plan planZ_;

	MPI_Request* Sreq_;
	MPI_Request* Rreq_;
	MPI_Status* Sstat_;
	MPI_Status* Rstat_;

	int nProc_;
	SymmetryMatrix<T>* field_;
	SymmetryMatrix<T>* tempMatrix_;
    };

    // define symmetry types for Field/Component/Direction {VW}{XYZ}
    // types bits represent the parity 
    // 0 ==> even
    // 1 ==> odd
    enum SymmetryType{ VX = 4, VY = 2, VZ = 1, 
		       WX = 3, WY = 5, WZ = 6 };

    // DFT direction flag
    enum Direction{ SYMMETRY_FORWARD, SYMMETRY_BACKWARD };

    template< typename T >
    class SymmetryMatrix
	: public Matrix< T >
    {
    public:

	typedef T value_type;
	typedef SymmetryBase base_type;
	// fftw_complex is now an array of 2 Ts
	typedef T fftw_complex[2];

	using Matrix< T >::operator=;

	SymmetryMatrix(const std::string& = "");
	SymmetryMatrix(const SymmetryMatrix< T >&);
	SymmetryMatrix(const int*, const int*, DataType, const std::string& = "");       
	SymmetryMatrix(Range, Range, Range, DataType type, const std::string& = "");     
	SymmetryMatrix(int, int, int, DataType, const std::string& = "");	         
	SymmetryMatrix(SymmetryBase& base, DataType type, const std::string& name = ""); 

	~SymmetryMatrix(){}

	SymmetryMatrix<T>& operator=(const SymmetryMatrix<T>&);
	
	// reallocate matrix in this object shell
	void resize(Range, Range, Range, DataType, const std::string&);                       
	void resize(const int* l, const int* h, DataType type, const std::string& name = ""); 
	void resize(int nx, int ny, int nz, DataType type, const std::string& name = "");     
	void resize(SymmetryBase& base, DataType type, const std::string& name = "");         

	// does the same as _nb?!
	void transpose_xy();
	// nonblocking transpose
	void transpose_nb(SymmetryInfo<T>*);

	// do DFT
	SymmetryInfo<T>* fftC2RStart(SymmetryMatrix*);
	void fftC2RFinish(SymmetryInfo<T>*);
	SymmetryInfo<T>* fftR2CStart(SymmetryMatrix*);
	void fftR2CFinish(SymmetryInfo<T>*);
	void fftR2C();
	void fftC2R();
	void fftC2R(const SymmetryMatrix<T>& from); ///< NIY
	void fftR2C(const SymmetryMatrix<T>& from); ///< NIY


	// Symmetry controls fft types
	inline void set_symmetry(const SymmetryType symmetry)
	{
	    symmetry_ = symmetry;
	}

	inline SymmetryType const get_symmetry()
	{
	    return symmetry_;
	}
	inline const base_type* get_base() const {
          assert(base_ != nullptr);
          return base_;
        }

    protected:

	base_type* base_;

	// transformation helpers
	fftw_plan get_plan_x_odd(Direction);
	fftw_plan get_plan_z_odd(Direction);
	fftw_plan get_plan_x_even(Direction);
	fftw_plan get_plan_z_even(Direction);

	SymmetryType symmetry_;

	inline T  b(int rank, int i, int j, int k) const
	{
	    // C-ordering
	    return this->matr_fast[int(k + this->dimR[2]*(j + this->dimR[1]*i)+rank*BK::sqr(this->base_->get_nyR())*(this->base_->get_nzR()))];
	}
	inline T& b(int rank, int i, int j, int k)
	{
	    // C-ordering
	    return this->matr_fast[int(k + this->dimR[2]*(j + this->dimR[1]*i)+rank*BK::sqr(this->base_->get_nyR())*(this->base_->get_nzR()))];
	}
    };

    // /////////////////////////////////////////////////////////////////////////
    // ///////////////////////// DEFINITIONS ///////////////////////////////////
    // /////////////////////////////////////////////////////////////////////////

    // ///////////////////////// CONSTRUCTORS //////////////////////////////////

    template< typename T >
    SymmetryMatrix< T >::SymmetryMatrix(const std::string& name)
	: Matrix< T >(name), base_(NULL)
    {
    }

    template< typename T >
    SymmetryMatrix< T >::SymmetryMatrix(const SymmetryMatrix< T >& rhs)
	: Matrix< T >(rhs), base_(rhs.base_)
    {
    }

    template<class T>
    SymmetryMatrix<T>::SymmetryMatrix(SymmetryBase& base, DataType type,const std::string& name) 
    {
	ERRORLOGL(1,"SymmetryMatrix<T>::SymmetryMatrix(const SymmetryBase&, DataType,const std::string& name)");

	this->existence_ = false;
  
	this->collectedNumber_++;

	resize(base,type,name);

	ERRORLOGL(4,"SymmetryMatrix<T>::SymmetryMatrix(const SymmetryBase&, DataType)-end");
    }
  
    template<class T>
    SymmetryMatrix<T>::SymmetryMatrix(const int* l, const int* h, DataType type, const std::string& name) 
    {
	ERRORLOGL(1,"SymmetryMatrix<T>::SymmetryMatrix(const int* l, const int* h, DataType type, const std::string& name");
  
	resize(l,h,type,name);
  
	ERRORLOGL(4,"SymmetryMatrix<T>::SymmetryMatrix(const int* l, const int* h, DataType type, const std::string& name-end");
    }

    template<class T>
    SymmetryMatrix<T>::SymmetryMatrix(Range xRange, Range yRange, Range zRange, DataType type, const std::string& name)
    {
	ERRORLOGL(1,"SymmetryMatrix<T>::SymmetryMatrix(Range xRange, Range yRange, Range zRange, DataType type, const std::string& name)");
  
	resize(xRange,yRange,zRange,type,name);
  
	ERRORLOGL(4,"SymmetryMatrix<T>::SymmetryMatrix(Range xRange, Range yRange, Range zRange, DataType type, const std::string& name)-end");
    }

    template<class T>
    SymmetryMatrix<T>::SymmetryMatrix(int nx, int ny, int nz, DataType type, const std::string& name)
    {
	ERRORLOGL(1,"SymmetryMatrix<T>::SymmetryMatrix(int nx, int ny, int nz, DataType type, const std::string& name)");
  
	resize(nx,ny,nz,type,name);

	ERRORLOGL(4,"SymmetryMatrix<T>::SymmetryMatrix(int nx, int ny, int nz, DataType type, const std::string& name)-end");
    }


    template<class T>
    SymmetryMatrix<T>& SymmetryMatrix<T>::operator=(const SymmetryMatrix<T>& rhs)
    {
	if(rhs.existence_)
	{
	    BK::Matrix< T >::operator=(rhs);
	    base_ = rhs.base_;
	}
	else // catch uninitialized matrices
	{
	    base_ = NULL;
	    this->existence_ = false;
	}
	return *this;
    }

    // /////////////////////////// TRANSPOSE ///////////////////////////////////

    /** @brief non-blocking transpose of x-y
      *
      * Transpose local data then use 
      * I{send,recv} to transpose non-local blocks.
      * Code comes from pnbFftMatrix.
      */
    template<class T>
    void SymmetryMatrix<T>::transpose_xy() 
    {
	ERRORLOGL(3,"SymmetryMatrix<T>::transpose_xy()");

	int NX = this->base_->get_nxR();
	int NY = this->base_->get_nyR();
	int NZ = this->base_->get_nzR();  
  
	int Nproc = this->base_->mpiBase_->get_commSize();
  
	int i, j, k, p;

        //
	int Nb = NX/Nproc;
	//
	int Nw = Nb*NY*NZ;
	SymmetryMatrix<T> tempMatrix(*base_,BK::RealData);
	//  scalar c;
	MPI_Status Sstat[Nproc], Rstat[Nproc];
	MPI_Request Sreq[Nproc], Rreq[Nproc];

	ERRORLOGL(3,PAR(NX));
	ERRORLOGL(3,PAR(NY));
	ERRORLOGL(3,PAR(NZ));
	ERRORLOGL(3,PAR(Nb));
	ERRORLOGL(3,PAR(Nw));
	ERRORLOGL(3,PAR(this->dimR[2]));
	ERRORLOGL(3,PAR(tempMatrix.get_loR(0)));
	ERRORLOGL(3,PAR(tempMatrix.get_loR(1)));
	ERRORLOGL(3,PAR(tempMatrix.get_loR(2)));
	ERRORLOGL(3,PAR(tempMatrix.get_hiR(0)));
	ERRORLOGL(3,PAR(tempMatrix.get_hiR(1)));
	ERRORLOGL(3,PAR(tempMatrix.get_hiR(2)));

	/*   Block Transposition using Isend-Irecv   */

	int myRank = this->base_->mpiBase_->get_commRank();

	tempMatrix = 0.;

	ERRORLOGL(3,PAR(tempMatrix.dimR[0]));
	ERRORLOGL(3,PAR(tempMatrix.dimR[1]));
	ERRORLOGL(3,PAR(tempMatrix.dimR[2]));
	ERRORLOGL(3,PAR(tempMatrix.base_->get_nxR()));
	ERRORLOGL(3,PAR(tempMatrix.base_->get_nyR()));
	ERRORLOGL(3,PAR(tempMatrix.base_->get_nzR()));
	ERRORLOGL(3,PAR(tempMatrix.sizeR_));
	ERRORLOGL(3,PAR(tempMatrix.sizeC_));
	ERRORLOGL(3,PAR(Nw*sizeof(T)));

	ERRORLOGL(3,"Isend and Irecv ...");
	for(p=0; p<Nproc; p++)   
	    if(p != myRank)
	    { 
		ERRORLOGL(3,BK::appendString(myRank," ",p," ",this->dimR[2]*this->base_->get_nyR()*this->base_->get_nyR()*p));
		// dont touch this :(
		MPI_Isend(&(this->matr[int(this->dimR[2]*this->base_->get_nyR()*this->base_->get_nyR()*p)]),Nw*sizeof(T),MPI_BYTE,p,myRank,MPI_COMM_WORLD,Sreq+p);
		MPI_Irecv(&(tempMatrix.matr[int(this->dimR[2]*this->base_->get_nyR()*this->base_->get_nyR()*p)]),Nw*sizeof(T),MPI_BYTE,p,p,MPI_COMM_WORLD,Rreq+p);
	    }

	ERRORLOGL(3,"transposing local blocks 1/2...");
	std::cerr << "Nb " << Nb << std::endl
		  << "NY " << Nb << std::endl
		  << "NZ " << Nb << std::endl;
	for(i=0; i<Nb; i++) 
	    for(j=0; j<NY; j++) 
		for(k=0; k<NZ; k++)
		    tempMatrix.b(myRank,i,j,k) = b(myRank,i,j,k);
	ERRORLOGL(3,"transposing local blocks 2/2 ...");
	for(i=0; i<Nb; i++) 
	    for(j=0; j<NY; j++) 
		for(k=0; k<NZ; k++)
		    b(myRank,i,j,k) = tempMatrix.b(myRank,j,i,k);


	ERRORLOGL(3,"transposing non-local blocks ...");
	for(p=0; p<Nproc; p++)
	    if(p != myRank) {
		MPI_Wait(Rreq+p,Rstat+p);     
		MPI_Wait(Sreq+p,Sstat+p);
		for(i=0; i<Nb; i++) 
		    for(j=0; j<NY; j++) 
			for(k=0; k<NZ; k++)
			    b(p,i,j,k) = tempMatrix.b(p,j,i,k);
	    }

    }

   template<class T>
    void SymmetryMatrix<T>::transpose_nb(SymmetryInfo<T>* info) 
    {
	ERRORLOGL(3,"SymmetryMatrix<T>::transpose_nb(SymmetryInfo& pnbFftInfo)");

	int NX = this->base_->get_nxR();
	int NY = this->base_->get_nyR();
	int NZ = this->base_->get_nzR();    

	int Nproc = this->base_->mpiBase_->get_commSize();
  
	int i, j, k, p;
	int Nb=NX/Nproc;
	int Nw=Nb*NY*NZ;

	info->nb_ = Nb;

	SymmetryMatrix<T>* tempMatrix = new SymmetryMatrix<T>(*base_,BK::RealData);
  
	info->tempMatrix_ = tempMatrix;
  
	//  scalar c;
	MPI_Status* Sstat = new MPI_Status[Nproc];
	MPI_Status* Rstat = new MPI_Status[Nproc];
	MPI_Request* Sreq = new MPI_Request[Nproc];
	MPI_Request* Rreq = new MPI_Request[Nproc];

	info->Sstat_ = Sstat;
	info->Rstat_ = Rstat;
	info->Sreq_ = Sreq;
	info->Rreq_ = Rreq;

	/*   Block Transposition using Isend-Irecv   */

	int myRank = this->base_->mpiBase_->get_commRank();

	ERRORLOGL(3,"Isend and Irecv ...");
	for(p=0; p<Nproc; p++) {   
	    if(p != myRank) {
		ERRORLOGL(3,BK::appendString(myRank,"  ",p,"  ",this->dimR[2]*this->base_->get_nyR()*this->base_->get_nyR()*p));
		MPI_Isend(&(this->matr[int(this->dimR[2]*this->base_->get_nyR()*this->base_->get_nyR()*p)]),Nw*sizeof(T),MPI_BYTE,p,myRank,MPI_COMM_WORLD,Sreq+p);
		MPI_Irecv(&(tempMatrix->matr[int(this->dimR[2]*this->base_->get_nyR()*this->base_->get_nyR()*p)]),Nw*sizeof(T),MPI_BYTE,p,p,MPI_COMM_WORLD,Rreq+p);
	    }
	}
  
	ERRORLOGL(3,"transposing local blocks 1/2...");
	std::cerr << "Nb " << Nb << std::endl
		  << "NY " << Nb << std::endl
		  << "NZ " << Nb << std::endl;
	for(i=0; i<Nb; i++) 
	    for(j=0; j<NY; j++) 
		for(k=0; k<NZ; k++)
		    tempMatrix->b(myRank,i,j,k) = b(myRank,i,j,k);
	ERRORLOGL(3,"transposing local blocks 2/2...");
	for(i=0; i<Nb; i++) 
	    for(j=0; j<NY; j++) 
		for(k=0; k<NZ; k++)
		    b(myRank,i,j,k) = tempMatrix->b(myRank,j,i,k);
	ERRORLOGL(3,"SymmetryMatrix<T>::transpose_nb(SymmetryInfo& pnbFftInfo) --> Done");
    }


    // ///////////////////////// TRANSFORMS ///////////////////////////////////


    // Helpers
    template< typename T >
    fftw_plan SymmetryMatrix< T >::get_plan_x_odd(Direction dir)
    {
	fftw_r2r_kind kind = (dir == SYMMETRY_FORWARD) ? FFTW_REDFT10 : FFTW_REDFT01;
	int nx = this->base_->get_nxR();
	int ny = this->base_->get_nyR();
	int nz = this->base_->get_nzR();
	return  fftw_plan_many_r2r(1,&nx, nz*ny,
				   this->dataR(), NULL,
				   ny*nz, 1,
				   this->dataR(), NULL,
				   ny*nz, 1,
				   &kind, FFTW_ESTIMATE
	                           );
    }

    template< typename T >
    fftw_plan SymmetryMatrix< T >::get_plan_x_even(Direction dir)
    {
	fftw_r2r_kind kind = (dir == SYMMETRY_FORWARD) ? FFTW_RODFT10 : FFTW_RODFT01;
	int nx = this->base_->get_nxR();
	int ny = this->base_->get_nyR();
	int nz = this->base_->get_nzR();
	return  fftw_plan_many_r2r(1,&nz, nx*ny,
				   this->dataR(), NULL,
				   1, nz,
				   this->dataR(), NULL,
				   1, nz,
				   &kind,FFTW_ESTIMATE
	                           );	
    }    

    template< typename T >
    fftw_plan SymmetryMatrix< T >::get_plan_z_odd(Direction dir)
    {
	fftw_r2r_kind kind = (dir == SYMMETRY_FORWARD) ? FFTW_RODFT10 : FFTW_RODFT01;
	int nx = this->base_->get_nxR();
	int ny = this->base_->get_nyR();
	int nz = this->base_->get_nzR();
	return  fftw_plan_many_r2r(1,&nz, nx*ny,
				   this->dataR(), NULL,
				   1, nz,
				   this->dataR(), NULL,
				   1, nz,
				   &kind,FFTW_ESTIMATE
	                           );	
    }	

    template< typename T >
    fftw_plan SymmetryMatrix< T >::get_plan_z_even(Direction dir)
    {
	fftw_r2r_kind kind = (dir == SYMMETRY_FORWARD) ? FFTW_REDFT10 : FFTW_REDFT01;
	int nx = this->base_->get_nxR();
	int ny = this->base_->get_nyR();
	int nz = this->base_->get_nzR();
	return  fftw_plan_many_r2r(1,&nz, nx*ny,
				   this->dataR(), NULL,
				   1, nz,
				   this->dataR(), NULL,
				   1, nz,
				   &kind,FFTW_ESTIMATE
	                           );		
    }

    
    template<class T>
    void SymmetryMatrix<T>::fftC2R() 
    {
	ERRORLOGL(3,"SymmetryMatrix<T>::fftC2R");

	assert(base_->get_initFlag() == true);
	//assert(this->dataType_ == ComplexData);
	assert(base_ != NULL);

	BK::SymmetryInfo<T>* info;

	info = fftC2RStart(this);
  
	fftC2RFinish(info);
   
	//this->dataType_ = RealData;

	ERRORLOGL(4,"SymmetryMatrix<T>::fftC2R-end");
    }

    template<class T>
    void SymmetryMatrix<T>::fftR2C() 
    {
	ERRORLOGL(3,"SymmetryMatrix<T>::fftR2C");

	assert(base_->get_initFlag() == true);
	assert(this->dataType_ == RealData);
	assert(base_ != NULL);

	BK::SymmetryInfo<T>* info;

	info = fftR2CStart(this);
  
	fftR2CFinish(info);

	*this *= base_->get_scale();

	//this->dataType_ = ComplexData;

	ERRORLOGL(4,"SymmetryMatrix<T>::fftR2C-end");
    }

    template<class T>
    void SymmetryMatrix<T>::fftR2C(const SymmetryMatrix<T>& from) 
    {
	ERRORLOGL(3,"SymmetryMatrix<T>::fftR2C(const SymmetryMatrix<T2>& from)");

	assert(base_->get_initFlag() == true);
	assert(from.get_dataType() == RealData);
	assert(base_ != NULL);
  
	//this->dataType_ = RealData;

	this->operator=(from);

	BK::SymmetryInfo<T>* symmetryInfo;

	symmetryInfo = fftR2CStart(this);
	fftR2CFinish(symmetryInfo);
  
	*this *= base_->get_scale();
	this->dataType_ = ComplexData;

	ERRORLOGL(4,"SymmetryMatrix<T>::fftR2C(const SymmetryMatrix<T2>& from)-end");
    }

    template<class T>
    void SymmetryMatrix<T>::fftC2R(const SymmetryMatrix<T>& from) 
    {
	ERRORLOGL(3,"SymmetryMatrix<T>::fftC2R(const SymmetryMatrix<T>& from)");

	assert(base_->get_initFlag() == true);
	assert(from.get_dataType() == ComplexData);
	assert(base_ != NULL);

	this->dataType_ = RealData;

	this->operator=(from);

	BK::SymmetryInfo<T>* info;

	info = fftC2RStart(this);
  
	fftC2RFinish(info);

	this->dataType_ = RealData;

	ERRORLOGL(4,"SymmetryMatrix<T>::fftC2R(const SymmetryMatrix<T>& from)-end");
    }

    // ////////////////////////// RESIZE HELPERS ///////////////////////////////
    template<class T>
    void SymmetryMatrix<T>::resize(Range xRange, Range yRange, Range zRange, DataType type,const std::string& name)
    {
	ERRORLOGL(1,"SymmetryMatrix<T>::resize(Range xRange, Range yRange, Range zRange, DataType type,const std::string& name)");
	
	Matrix<T>::resize(xRange,yRange,zRange,type,name);
	
	base_ = NULL;
	
	ERRORLOGL(1,"SymmetryMatrix<T>::resize(Range xRange, Range yRange, Range zRange, DataType type,const std::string& name)-end");
    } 

    template<class T>
    void SymmetryMatrix<T>::resize(int nx, int ny, int nz, DataType type,const std::string& name)
    {
	ERRORLOGL(1,"SymmetryMatrix<T>::resize(int nx, int ny, int nz,DataType type,const std::string& name)");
	
	this->existence_ = false;
	resize(Range(0,nx-1),Range(0,ny-1),Range(0,nz-1),type,name);
	
	ERRORLOGL(1,"SymmetryMatrix<T>::resize(int nx, int ny, int nz,DataType type,const std::string& name)-end");
    }

    template<class T>
    void SymmetryMatrix<T>::resize(const int* l, const int* h, DataType type,const std::string& name)
    {
	ERRORLOGL(1,"SymmetryMatrix<T>::resize(const int* l, const int* h, DataType type,const std::string& name)");
	
	this->existence_ = false;  
	
	resize(Range(l[0],h[0]),Range(l[1],h[1]),Range(l[2],h[2]),type,name);
	
	ERRORLOGL(4,"SymmetryMatrix<T>::resize(const int* l, const int* h, DataType type,const std::string& name)-end");
    }

    template<class T>
    void SymmetryMatrix<T>::resize(SymmetryBase& base, DataType type, const std::string& name)
    {
	ERRORLOGL(1,BK::appendString("SymmetryMatrix<T>::resize(SymmetryBase& base, DataType = ",type," const std::string& = ",name,")"));

	this->existence_ = false; 

	Matrix<T>::resize(base.get_nxR(),base.get_nyR(),base.get_nzR(),base.get_nxC(),base.get_nyC(),base.get_nzC(),type,name);  

	base_ = &base;
  
	ERRORLOGL(4,"SymmetryMatrix<T>::resize(SymmetryBase& base, DataType type, const std::string& name)-end");
    }

    // /////////////////////////////////////////////////////////////////////////
    // ///////////////////////// SNOITINIFED ///////////////////////////////////
    // /////////////////////////////////////////////////////////////////////////

} // ECAPSEMAN

#endif
