#include "parseFromFile.hpp"
#include "output.hpp"

namespace BK {

ReadInitFile::ReadInitFile()
{
  existence_ = false;
}
  
ReadInitFile::~ReadInitFile()
{
  ERRORLOGL(1,"ReadInitFile::~ReadInitFile()");

  ERRORLOGL(4,"ReadInitFile::~ReadInitFile()-end");
}

ReadInitFile::ReadInitFile(std::string filename, bool assertion)
{
  assertion_ = assertion;
  readFileIntern(filename,assertion);
}

bool ReadInitFile::readFile(std::string filename, bool assertion)
{
  assertion_ = assertion;
  return readFileIntern(filename,assertion);
}

bool ReadInitFile::readFileIntern(std::string filename, bool assertion)
{
  assertion_ = assertion;
  filename_ = filename;

  std::ifstream quelldatei;
  quelldatei.open(filename.c_str());
  if(!quelldatei){
    std::cerr << "ReadInitFile::readFileIntern: unable to open " << filename << "!\n";
    existence_ = false;
    if(assertion)
      abort();
  }
  std::string parameter;
  std::string key;
  if(quelldatei) {
    existence_ = true;
    while(! quelldatei.eof()){
      quelldatei >> key;
      if(key[0] == '#'){
	std::string kommentarLine;
	getline(quelldatei,kommentarLine);
      }
      else{
	quelldatei >> parameter;
	
	if(parameter == "=")
	  quelldatei >> parameter;

	if(parameter == "[") {
	  if(mapMap_.find(key) != mapMap_.end()) {
	    std::cerr << "ERROR in readFileIntern: key " << key << " already existent" << std::endl;
	    exit(1);
	  }
	  mapMap_.insert(std::pair<std::string, ParameterMap>(key,ParameterMap()));
	  
	  std::string subKey;
	  quelldatei >> subKey;

	  while(subKey != "]") {
	    
	  quelldatei >> parameter;
	  
	  if(parameter == "=")
	    quelldatei >> parameter;
	  
	  mapMap_[key].insert(std::pair<std::string, std::string>(subKey,parameter));
	  quelldatei >> subKey;
	  }
	  continue;
	}
	  
	// check for usage of ReadInitFile and ParseFactoryList
	// with one init-File
	if(parameter == "{"){  
	  break;
	}
	
	parameterMap_.insert(std::pair<std::string, std::string>(key,parameter));
	
      }
    }
  }
  
//   std::cout << "parameterMap = \n" << BK::out(parameterMap_) << std::endl;
//   std::cout << "mapMap = \n" << BK::out(mapMap_) << std::endl;
  return existence_;
}

bool ReadInitFile::parameterExists(std::string parameter)
{
  ERRORLOGL(3,"void ReadInitFile::ParameterExists(std::string parameter)");
  
  bool exists = BK::parameterExists(parameterMap_, parameter);

  return exists;

  ERRORLOGL(4,"void ReadInitFile::ParameterExists(std::string parameter)-end");
}

bool parameterExists(const std::map<std::string, std::string>& map, std::string parameter)
{
  ERRORLOGL(3,"void parameterExists(std::string parameter)");
  
  bool exists = true;

  std::map<std::string, std::string>::const_iterator it;
  it = map.find(parameter);
  
  if(it == map.end())
    exists = false;

  return exists;

  ERRORLOGL(4,"void parameterExists(std::string parameter)-end");
}

void ReadInitFile::writeMap(std::string filename)
{ 
  std::ofstream file;
  file.open(filename.c_str());
  if(!file){
    std::cerr << "ReadInitFile::writeMap: unable to open " << filename << "!\n";
    abort();
  }
  for(ParameterMap::iterator it = parameterMap_.begin(); it != parameterMap_.end(); it++){
    file << it->first << " " << it->second << std::endl;
  }
}

bool ReadInitFile::compare(ReadInitFile refMap)
{ 
  ERRORLOGL(3,"void ReadInitFile::compare(ReadInitFile refMap)");
  
  for(ParameterMap::iterator itRefMap = refMap.parameterMap_.begin(); itRefMap != refMap.parameterMap_.end(); itRefMap++){
    itParameterMap = parameterMap_.find(itRefMap->first);
    if(itParameterMap == parameterMap_.end()){
      std::cerr << "ReadInitFile(" << filename_ << ")::compare: key [ " << itRefMap->first << " ] does not exist\n";
      abort();
    }
    if(itParameterMap->second != itRefMap->second){
      std::cerr << "ReadInitFile::compare: Parameter [ " << itRefMap->first << " = " << itRefMap->second
		<< " ] in reference map does not coincide with [ " << itParameterMap->first << " = "
		<< itParameterMap->second << " ] in received map.\n";
      return false;
    }
  }

  ERRORLOGL(3,"void ReadInitFile::compare(ReadInitFile refMap)-end");

  return true;
}

bool ReadInitFile::compare(ReadInitFile refMap,std::string key, bool assertion)
{
  ERRORLOGL(3,"void ReadInitFile::compare(ReadInitFile refMap,std::string key)");

  ParameterMap::iterator itRefMap = refMap.parameterMap_.find(key);
  ParameterMap::iterator itThisMap = parameterMap_.find(key);
  
  bool temp = false;

  if(itRefMap != refMap.parameterMap_.end() && itThisMap != parameterMap_.end()) {
    temp = itRefMap->second == itThisMap->second;
  }

  if(assertion) 
    if(!temp) {
      ERRORLOGL(0,BK::appendString("ERROR in ReadInitFile::compare(ReadInitFile refMap,std::string key, bool assertion): refMap(",key,") = "
				   ,itRefMap->second," != this->map(",key,") = ",itThisMap->second));
      abort();
    }
  
  return temp;

  ERRORLOGL(4,"void ReadInitFile::compare(ReadInitFile refMap,std::string key)-end");
}

template<>
void assertType<int>(std::string value)
{
  ERRORLOGL(4,"void ReadInitFile::assertType<int>(std::string)");
  
  if(value.find(".") != std::string::npos) {
    ERRORLOGL(0,BK::appendString("ERROR in assertType<int>(",value,") : value is not int."));
    abort();
  }
}

template<>
void assertType<bool>(std::string value)
{
  ERRORLOGL(4,"void ReadInitFile::assertType<bool>(std::string)");
  
  if(value != "0" && value != "1") {
    ERRORLOGL(0,BK::appendString("ERROR in assertType<bool>(",value,") : value is not bool."));
    abort();
  }
}

} // namespace BK
