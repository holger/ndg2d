#include "range.hpp"

namespace BK {

Range::Range()
{
  ERRORLOGL(1,"Range::Range()");

  index[0] = 0; 
  index[1] = 0;
  range = 0;

  ERRORLOGL(4,"Range::Range()-end");
}

Range::Range(int low, int high)
{
  ERRORLOGL(1,"Range::Range(int low, int high)");

  ERRORLOG(4,PAR(low));
  ERRORLOG(4,PAR(high));
  
  assert(low < high+1);
  
  index[0] = low; 
  index[1] = high;
  range = high - low +1;

  ERRORLOGL(4,"Range::Range(int low, int high)-end");
}

std::ostream& operator<<(std::ostream &of, const Range& range)
{
  of << "(" << range.index[0] << "," << range.index[1] << ")";

  return of;
}

} // namespace BK
