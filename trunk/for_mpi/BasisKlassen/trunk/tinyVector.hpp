#ifndef tinyVector
#define tinyVector

namespace BK {

template<class T_numtype, int N_length>
class TinyVector
{
public:

  TinyVector()
  { }
  
  ~TinyVector() 
  { }

  inline TinyVector(const TinyVector<T_numtype,N_length>& x);
  
  template <typename T_numtype2>
  inline TinyVector(const TinyVector<T_numtype2,N_length>& x);

  inline TinyVector(T_numtype initValue);

  TinyVector(T_numtype x0, T_numtype x1)
  {
    data_[0] = x0;
    data_[1] = x1;
  }
  
  TinyVector(T_numtype x0, T_numtype x1, T_numtype x2)
  {
    data_[0] = x0;
    data_[1] = x1;
    data_[2] = x2;
  }
  
  TinyVector(T_numtype x0, T_numtype x1, T_numtype x2,
	     T_numtype x3)
  {
    data_[0] = x0;
    data_[1] = x1;
    data_[2] = x2;
    data_[3] = x3;
  }
  
  TinyVector(T_numtype x0, T_numtype x1, T_numtype x2,
	     T_numtype x3, T_numtype x4)
  {
    data_[0] = x0;
    data_[1] = x1;
    data_[2] = x2;
    data_[3] = x3;
    data_[4] = x4;
  }

  TinyVector(T_numtype x0, T_numtype x1, T_numtype x2,
	     T_numtype x3, T_numtype x4, T_numtype x5)
  {
    data_[0] = x0;
    data_[1] = x1;
    data_[2] = x2;
    data_[3] = x3;
    data_[4] = x4;
    data_[5] = x5;
  }

  TinyVector(T_numtype x0, T_numtype x1, T_numtype x2,
	     T_numtype x3, T_numtype x4, T_numtype x5, T_numtype x6)
  {
    data_[0] = x0;
    data_[1] = x1;
    data_[2] = x2;
    data_[3] = x3;
    data_[4] = x4;
    data_[5] = x5;
    data_[6] = x6;
  }

  TinyVector(T_numtype x0, T_numtype x1, T_numtype x2,
	     T_numtype x3, T_numtype x4, T_numtype x5, T_numtype x6,
	     T_numtype x7)
  {
    data_[0] = x0;
    data_[1] = x1;
    data_[2] = x2;
    data_[3] = x3;
    data_[4] = x4;
    data_[5] = x5;
    data_[6] = x6;
    data_[7] = x7;
  }

  TinyVector(T_numtype x0, T_numtype x1, T_numtype x2,
	     T_numtype x3, T_numtype x4, T_numtype x5, T_numtype x6,
	     T_numtype x7, T_numtype x8)
  {
    data_[0] = x0;
    data_[1] = x1;
    data_[2] = x2;
    data_[3] = x3;
    data_[4] = x4;
    data_[5] = x5;
    data_[6] = x6;
    data_[7] = x7;
    data_[8] = x8;
  }

  TinyVector(T_numtype x0, T_numtype x1, T_numtype x2,
	     T_numtype x3, T_numtype x4, T_numtype x5, T_numtype x6,
	     T_numtype x7, T_numtype x8, T_numtype x9)
  {
    data_[0] = x0;
    data_[1] = x1;
    data_[2] = x2;
    data_[3] = x3;
    data_[4] = x4;
    data_[5] = x5;
    data_[6] = x6;
    data_[7] = x7;
    data_[8] = x8;
    data_[9] = x9;
  }

  TinyVector(T_numtype x0, T_numtype x1, T_numtype x2,
	     T_numtype x3, T_numtype x4, T_numtype x5, T_numtype x6,
	     T_numtype x7, T_numtype x8, T_numtype x9, T_numtype x10)
  {
    data_[0] = x0;
    data_[1] = x1;
    data_[2] = x2;
    data_[3] = x3;
    data_[4] = x4;
    data_[5] = x5;
    data_[6] = x6;
    data_[7] = x7;
    data_[8] = x8;
    data_[9] = x9;
    data_[10] = x10;
  }
  
  unsigned length() const
  { return N_length; }

  T_numtype operator()(unsigned i) const
  {
    return data_[i];
  }

  T_numtype& operator()(unsigned i)  { 
    return data_[i];
  }
  

  T_numtype operator[](unsigned i) const
  {
    return data_[i];
  }

  T_numtype& operator[](unsigned i)
  {
    return data_[i];
  }


private:
  T_numtype data_[N_length];
};

// -------------------------------------------------------------------------
// definitions
// -------------------------------------------------------------------------

template<typename T>
class TinyVector<T,0> {
 public:
  inline TinyVector(T initValue) {}
};

// template<typename T>
// class TinyVector<T,0> {
// };

template<typename T_numtype, int N_length>
inline TinyVector<T_numtype, N_length>::TinyVector(T_numtype initValue)
{
    for (int i=0; i < N_length; ++i)
        data_[i] = initValue;
}

template<typename T_numtype, int N_length>
inline TinyVector<T_numtype, N_length>::TinyVector(const 
    TinyVector<T_numtype, N_length>& x)
{
    for (int i=0; i < N_length; ++i)
        data_[i] = x.data_[i];
}

template<typename T_numtype, int N_length> template<typename T_numtype2>
inline TinyVector<T_numtype, N_length>::TinyVector(const 
    TinyVector<T_numtype2, N_length>& x)
{
    for (int i=0; i < N_length; ++i)
        data_[i] = static_cast<T_numtype>(x[i]);
}

} // namespace BK

#endif

