#ifndef singleton_h
#define singleton_h

namespace BK {

template<class BASE>
class Singleton : public BASE
{
 public:
  static Singleton& instance();

 private:
  // Konstruktors
  Singleton(){};
   
  // copy Konstruktor
  Singleton(const Singleton&);

  // Zuweisungs-Operator
  Singleton& operator=(const Singleton&);

  // Destruktor
  ~Singleton(){};

  // Zeiger auf einzige Instanz
  static Singleton* pInstance_;
};

// --------------------------------------------------------
// definitions
// --------------------------------------------------------

template<class BASE>
Singleton<BASE>* Singleton<BASE>::pInstance_ = 0;

template<class BASE>
Singleton<BASE>& Singleton<BASE>::instance()
{
  if(!pInstance_){
    static Singleton theInstance;
    pInstance_ = &theInstance;

  }
  return *pInstance_;
}

template<class BASE>
class StackSingleton
{
public:
  static BASE& instance() {
    static BASE instance_;
    return instance_;
  }

  ~StackSingleton() {};
  
private:

  StackSingleton() {};
  StackSingleton(const StackSingleton&);
  StackSingleton& operator=(const StackSingleton&);  
};

} // namespace BK 

#endif
