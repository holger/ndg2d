#include "filesystem.hpp"

#include <cstdlib>

// #undef LOGLEVEL
// #define LOGLEVEL 4

namespace BK {

bool setupDirectory(const std::string& dir)
{
  ERRORLOGL(3,BK::appendString("BK::setupDirectory(const std::string& ",dir,")"));

  // Erzeugung des Verzeichnisses zum aktuellen Step
  path directory = dir;
  if(!exists( directory ) && dir != "") {
    create_directory( directory );
//     try {
//       create_directory( directory );
//     }
//     catch (filesystem_error& e) {
//       std::cout << e.what() << std::endl;
//       std::cerr << "ERROR in setupDirectory: Unable to create directory " << dir << std::endl;
//       exit(0); 
//     }
  }

  ERRORLOGL(4,BK::appendString("BK::setupDirectory(const std::string& ",dir,")-end"));

  return true;
}

bool exists(const path& ph)
{
    struct stat path_stat;
    return ::stat( ph.string().c_str(), &path_stat ) == 0;  
}

bool exists(const std::string& dir) {
  return exists(path(dir));
}

void create_directory(const path& dir_path)
{
  if ( ::mkdir( dir_path.string().c_str(),
	S_IRWXU|S_IRWXG|S_IRWXO ) != 0 )
    {
      path dirPath(dir_path);
      PathFracList pathFracList = dirPath.get_fracList();
      std::string dirFrac = "";
      for(PathFracList::iterator it = pathFracList.begin();it != pathFracList.end();it++) {
	dirFrac += *it;
	if(!exists(dirFrac))
	  if ( ::mkdir( dirFrac.c_str(),
			S_IRWXU|S_IRWXG|S_IRWXO ) != 0 )
	    {
	      std::cerr << "ERROR in create_directory: cannot create: " << dirFrac << std::endl;
	      abort();
	    }
	//	throw filesystem_error(errno);
      }
    }
}

bool is_directory(const path& ph)
{
  struct stat path_stat;
  if ( ::stat(ph.string().c_str(), &path_stat ) != 0 ) {
    //	throw filesystem_error(errno);
    ERRORLOG(1,"ERROR in is_directory");
    exit(1);
  }

  return S_ISDIR( path_stat.st_mode );
}

bool isDirectoryPath(const std::string& dir)
{
  if(path(dir).leaf().string() == "")
    return true;
  else
    return false;
}

}
