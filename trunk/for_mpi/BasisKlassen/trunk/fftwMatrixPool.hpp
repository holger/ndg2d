#ifndef fftwMatrixPool_hpp
#define fftwMatrixPool_hpp

#include "range.hpp"
#include "fftwMatrix.hpp"

namespace BK {

class FftwBase;

template<class T>
class FftwMatrixPool
{
public:
  FftwMatrixPool();
  FftwMatrixPool(Range xRange, Range yRange, Range zRange);
  FftwMatrixPool(FftwBase& base);

  void setMatrixSize(Range xRange, Range yRange, Range zRange);
  void setMatrixSize(FftwBase& base);

  void add(unsigned int number);

  FftwMatrix<T>& get(unsigned int number);

  const FftwMatrix<T>& constGet(unsigned int number);

  CONSTPARA(int,size);

private:

  std::vector<FftwMatrix<T>*> pool_;

  Range xRange_;
  Range yRange_;
  Range zRange_;

  FftwBase* fftwBase_;

  bool sized_;
};

// --------------------------------------------------------------------------
// definitions
// --------------------------------------------------------------------------

#undef LOGLEVEL
#define LOGLEVEL 0

template<class T>
FftwMatrixPool<T>::FftwMatrixPool()
{
  ERRORLOGL(1,"FftwMatrixPool::FftwMatrixPool()");
  
  sized_ = false;
  fftwBase_ = NULL;
  
  ERRORLOGL(4,"FftwMatrixPool::FftwMatrixPool()-end");
}

template<class T>
FftwMatrixPool<T>::FftwMatrixPool(Range xRange, Range yRange, Range zRange)
{
  ERRORLOGL(1,"FftwMatrixPool<T>::FftwMatrixPool(Range xRange, Range yRange, Range zRange)");
  
  setMatrixSize(xRange,yRange,zRange);
  
  ERRORLOGL(4,"FftwMatrixPool<T>::FftwMatrixPool(Range xRange, Range yRange, Range zRange)-end");
}

template<class T>
FftwMatrixPool<T>::FftwMatrixPool(FftwBase& base)
{
  ERRORLOGL(1,"FftwMatrixPool<T>::FftwMatrixPool()");
  
  setMatrixSize(base);
  
  ERRORLOGL(4,"FftwMatrixPool<T>::FftwMatrixPool()-end");
}

template<class T>
void FftwMatrixPool<T>::setMatrixSize(Range xRange, Range yRange, Range zRange)
{
  ERRORLOGL(1,"FftwMatrixPool<T>::setMatrixSize(Range xRange, Range yRange, Range zRange)");

  xRange_ = xRange;
  yRange_ = yRange;
  zRange_ = zRange;

  sized_ = true;
  size_ = 0;
  fftwBase_ = NULL; //false;

  ERRORLOGL(1,PAR(xRange_));
  ERRORLOGL(1,PAR(yRange_));
  ERRORLOGL(1,PAR(zRange_));

  ERRORLOGL(4,"FftwMatrixPool<T>::setMatrixSize(Range xRange, Range yRange, Range zRange)-end");
}

template<class T>
void FftwMatrixPool<T>::setMatrixSize(FftwBase& base)
{
  ERRORLOGL(1,"FftwMatrixPool<T>::setMatrixSize(FftwBase& base)");
  
  fftwBase_ = &base;

  xRange_ = Range(0,base.get_nxR()-1);
  yRange_ = Range(0,base.get_nyR()-1);
  zRange_ = Range(0,base.get_nzR()-1);

  sized_ = true;
  size_ = 0;

  ERRORLOGL(1,PAR(xRange_));
  ERRORLOGL(1,PAR(yRange_));
  ERRORLOGL(1,PAR(zRange_));
  
  ERRORLOGL(4,"FftwMatrixPool<T>::setMatrixSize(FftwBase& base)-end");
}

template<class T>
FftwMatrix<T>& FftwMatrixPool<T>::get(unsigned int number)
{
  ERRORLOGL(3,"FftwMatrixPool<T>::get");

  if(pool_.size() < number + 1) {
    add(number - pool_.size() + 1);
  }

  ERRORLOGL(4,"FftwMatrixPool<T>::get-end");
  
  return *pool_[number];
}

template<class T>
void FftwMatrixPool<T>::add(unsigned int number)
{
  ERRORLOGL(3,"FftwMatrixPool<T>::add");

  ERRORLOGL(4,PAR(pool_.size()));

  if(!sized_) {
    ERRORLOGL(0,"ERROR in FftwMatrixPool<T>::add: setMatrixSize noch nicht ausgefuehrt");
    abort();
  }
  
  for(unsigned int i=0;i<number;i++) {
    ERRORLOGL(4,PAR(i));
    
    if(fftwBase_)
      pool_.push_back(new FftwMatrix<T>(*fftwBase_,RealData));
    else
      pool_.push_back(new FftwMatrix<T>(xRange_,yRange_,zRange_,RealData));

    pool_[size_]->set_name(BK::appendString("pool",i));
    
    ERRORLOGL(4,PAR(pool_.size()));
  
    size_++;    
    ERRORLOGL(4,PAR(size_));
  }

  ERRORLOGL(4,"FftwMatrixPool<T>::add-end");
}

} // namespace BK

#endif
