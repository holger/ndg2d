#ifndef point_class_hpp
#define point_class_hpp

#include <iostream>
#include <sstream>
#include <string>

#include "vector_class.hpp"

namespace BK {

template<class DOUBLE, int dim>
class Point
{
public:

  // default-Konstruktor
  Point();

  // copy-Konstruktor
  Point(const Point<DOUBLE, dim>& v);                             

  // copy-Operator= kopiert elementweise
  Point &operator=(const Point<DOUBLE, dim>& v);                  
  
  // copy-Operator*= multipliziert Point-koord mit Skalar (z.B. p*=2)
  Point &operator*=(const DOUBLE skalar);
  
  //  Konstruktor: erzeugt Point mit koord[0] = _x und Koord[1] = _y
  Point(const DOUBLE& _x, const DOUBLE& _y = 0, const DOUBLE& _z = 0);

  // Konstruktor: erzeugt Point mit Koord=[_x,_y,_z] und farbe = _farbe und groesse = groesse
  Point(const DOUBLE& _x, const DOUBLE& _y, const DOUBLE& _z, const Vector<int>& _farbe , const int& _groesse);

  // Konstruktor: erzeugt Point mit Koord=[_x,_y,_z] und farbe = _farbe und groesse = groesse
  Point(const DOUBLE& _x, const DOUBLE& _y, const Vector<int>& _farbe , const int& _groesse);
  
  // Koordinaten des Point [x,y,z]
  Vector<DOUBLE> koord;             
  
  // Farbe des Point als RGB-Vector [int R, int G, int B]
  Vector<int> farbe;  
  
  // Groesse des Point
  int groesse; 

  // Dimension des Point
  int dimension;
  
 private:
  
  // Kopierfunktion
  void copy(const Point<DOUBLE, dim>& v);
};

// -----------------------------------------------------------------------------------------------
// definitions
// -----------------------------------------------------------------------------------------------

template<class DOUBLE, int dim>
Point<DOUBLE, dim>::Point() 
{
  dimension = dim;
  koord.resize(dimension);
  farbe.resize(3);
  for(int i=0;i<dimension;i++){
    koord[i] = 0;
  }
  farbe[0] = 0; farbe[1] = 0; farbe[2] = 0;
  groesse=1;
}

template<class DOUBLE, int dim>
Point<DOUBLE,dim>::Point(const DOUBLE& _x, const DOUBLE& _y, const DOUBLE& _z, const Vector<int>& _farbe, const int& _groesse) 
{
  dimension = dim;
  koord.resize(dimension);
  farbe.resize(3);
  Vector<DOUBLE> koord_tmp(3);
  koord_tmp[0] = _x; koord_tmp[1] = _y; koord_tmp[2] = _z;
  for(int i=0;i<dimension;i++)
    koord[i] = koord_tmp[i];
  farbe = _farbe;
  groesse = _groesse;
}

template<class DOUBLE, int dim>
Point<DOUBLE,dim>::Point(const DOUBLE& _x, const DOUBLE& _y, const Vector<int>& _farbe, const int& _groesse) 
{
  dimension = dim;
  koord.resize(dimension);
  farbe.resize(3);
  Vector<DOUBLE> koord_tmp(2);
  koord_tmp[0] = _x; koord_tmp[1] = _y;
  for(int i=0;i<dimension;i++)
    koord[i] = koord_tmp[i];
  farbe = _farbe;
  groesse = _groesse;
}

template<class DOUBLE, int dim>
Point<DOUBLE,dim>::Point(const DOUBLE& _x, const DOUBLE& _y, const DOUBLE& _z)
{
  dimension = dim;
  koord.resize(dimension);
  farbe.resize(3);
  Vector<DOUBLE> koord_tmp(3);
  koord_tmp[0] = _x; koord_tmp[1] = _y; koord_tmp[2] = _z;
  for(int i=0;i<dimension;i++)
    koord[i] = koord_tmp[i];
  farbe[0] = 0; farbe[1] = 0; farbe[2] = 0;
  groesse = 1;
}

template<class DOUBLE, int dim>
Point<DOUBLE, dim>::Point(const Point<DOUBLE, dim>& v)
{
  dimension = dim;
  koord.resize(dimension);
  farbe.resize(3);
  copy(v);
}

template<class DOUBLE, int dim>
std::ostream& operator<<(std::ostream &of, const Point<DOUBLE, dim> &v)      
{ 
  of << "[";
  for(int i=0;i<v.dimension-1;i++)
    of << v.koord[i] << ",";
  
  of << v.koord[v.dimension-1] << "]";
      
  return of;
}

template<class DOUBLE, int dim>
void Point<DOUBLE, dim>::copy(const Point<DOUBLE, dim>& v)
{
  for(int i=0;i<3;i++){
    farbe[i] = v.farbe[i];
  }
  groesse = v.groesse;
  for(int i=0;i<v.dimension;i++){
    koord[i] = v.koord[i];
  }
}

template<class DOUBLE, int dim>
Point<DOUBLE, dim>& Point<DOUBLE, dim>::operator=(const Point<DOUBLE, dim>& v)             // Kopykonstruktor der Vektor-Klasse
{
  copy(v);
  return *this;
}

template<class DOUBLE, int dim>
Point<DOUBLE, dim> &Point<DOUBLE, dim>::operator*=(const DOUBLE skalar)
{
  koord*=skalar;
  return *this;
}

template<class DOUBLE, int dim>
Point<DOUBLE,dim> operator+(const Point<DOUBLE, dim>& v, const Point<DOUBLE, dim>& w)
{
  Point<DOUBLE,dim> temp;
  for (int i = 0; i < v.dimension; i++)
    temp.koord[i] = v.koord[i]+w.koord[i];

  return temp;
}

template<class DOUBLE, int dim>
Point<DOUBLE,dim> operator-(const Point<DOUBLE, dim>& v, const Point<DOUBLE, dim>& w)
{
  Point<DOUBLE,dim> temp;
  for (int i = 0; i < v.dimension; i++)
    temp.koord[i] = v.koord[i]-w.koord[i];

  return temp;
}

template<class DOUBLE, int dim>
Point<DOUBLE, dim> operator*(DOUBLE zahl, const Point<DOUBLE, dim>& v)
{
  Point<DOUBLE, dim> temp(v);
  temp.koord = temp.koord*zahl;
  return temp;
}

template<class DOUBLE, int dim>
Point<DOUBLE ,dim> operator*(const Point<DOUBLE, dim>& v, DOUBLE zahl)
{
  Point<DOUBLE, dim> temp(v);
  cout << temp.koord << endl;
  temp.koord = temp.koord*zahl;
  return temp;
}

} // namespace BK

#endif

