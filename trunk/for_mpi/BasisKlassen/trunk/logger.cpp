#include "logger.hpp"

#include <cstdlib>

namespace BK {

Logger::Logger()
{}

Logger::~Logger()
{ 
  for(itLogFileMap = logFileMap.begin(); itLogFileMap != logFileMap.end();itLogFileMap++){
    itLogFileMap->second->close();
    delete itLogFileMap->second;;
  }
}

void Logger::fileCheck(std::string filename)
{
  itLogFileMap = logFileMap.find(filename);
  if(itLogFileMap == logFileMap.end()){
    std::ofstream* logFile = new std::ofstream;
    logFile->unsetf(std::ios::unitbuf);
    std::pair<std::map<std::string, std::ofstream*>::iterator,bool> checkPair;
    checkPair = logFileMap.insert(std::pair<std::string, std::ofstream*>(filename,logFile));
    if(checkPair.second != true){
      std::cerr << "Einfuegen von ofstream in logFileMap fehlgeschlagen\n";
      exit(1);
    }
    logFile->open(filename.c_str(), std::ios::out | std::ios::app);
    itLogFileMap = checkPair.first;
  }
}

void Logger::line(std::string filename, std::string line)
{
  fileCheck(filename);
  *(itLogFileMap->second) << line << std::endl;
  (itLogFileMap->second)->flush();
}

void Logger::out(std::string filename, std::string comment)
{
  fileCheck(filename);
  *(itLogFileMap->second) << comment;
  (itLogFileMap->second)->flush();
}

void Logger::blankLine(std::string filename)
{
  fileCheck(filename);
  *(itLogFileMap->second) << std::endl;
}

} // namespace BK
