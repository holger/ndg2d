#ifndef gridPoint_h
#define gridPoint_h

#include <fstream>

namespace BK {

struct GridPoint
{
  GridPoint(int xIndex, int yIndex, int zIndex) { x = xIndex; y = yIndex; z=zIndex;}
  unsigned int x;
  unsigned int y;
  unsigned int z;
}; 

std::ostream& operator<<(std::ostream &of, const GridPoint& gridPoint);

} // namespace BK

#endif
