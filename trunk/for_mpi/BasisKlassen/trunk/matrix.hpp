#ifndef bk_matrix_hpp
#define bk_matrix_hpp

#include <iostream>
#include <assert.h>
#include <cstdlib>
#include <cstring>

//#include <malloc.h>
//#include <fftw3.h>

#include "assert.hpp"
#include "range.hpp"
#include "gridPoint.hpp"
#include "dataType.hpp"
#include "parameter.hpp"
#include "fftw_complex.hpp"
#include "output.hpp"

namespace BK {

extern const Complex<double> II;

template<class T>
class Matrix
{
 public:
   
  typedef T value_type;
  
  PROSTAPAR(unsigned int, collectedNumber);
  PROSTAPAR(unsigned int, collectedSize);

  Matrix(const std::string& name = "");
  ~Matrix();

  Matrix(const Matrix& matrix);

  Matrix(const int* l, const int* h, DataType type, const std::string& name = "");
  Matrix(Range xRange, Range yRange, Range zRange, DataType type, const std::string& name = "");
  Matrix(int nx, int ny, int nz, DataType type, const std::string& name = "");

  void resize(const int* l, const int* h, DataType type, const std::string& name = "");
  void resize(Range xRange, Range yRange, Range zRange, DataType type, const std::string& name = "");
  void resize(int nx, int ny, int nz, DataType type, const std::string& name = "");
  void resize(int nxR, int nyR, int nzR, int nxC, int nyC, int nzC, DataType type,const std::string& name);

  inline T r(int i, int j, int k) const;
  inline const Complex<T>& c(int i, int j, int k) const;

  inline T& r(int i, int j, int k);
  inline Complex<T>& c(int i, int j, int k);

  inline T ri(unsigned int i) const;
  inline T& ri(unsigned int i);

  inline const Complex<T>& ci(unsigned int i) const;
  inline Complex<T>& ci(unsigned int i);

  inline T operator()(int i, int j, int k) const;
  inline T& operator()(int i, int j, int k);

  int get_loR(int i) const { assert(i < 3); return loR[i];} 
  int get_hiR(int i) const { assert(i < 3); return hiR[i];}

  const int* get_loR() const { return &loR[0]; }
  const int* get_hiR() const { return &hiR[0]; }

  int get_loC(int i) const { assert(i < 3); return loC[i];}
  int get_hiC(int i) const { assert(i < 3); return hiC[i];}

  unsigned int get_dimR(int i) const { assert(i < 3); return dimR[i];}
  unsigned int get_dimC(int i) const { assert(i < 3); return dimC[i];}

  bool inRangeC(int x, int y, int z) const;
  bool inRangeR(int x, int y, int z) const;

  void deleteData();

  bool checkProperties(const Matrix& matrix);

  void swapMatrix(Matrix& matrix);

  T meanR();
  Complex<T> meanC();
  T varianceR();
  
  T* dataR();
  Complex<T>* dataC();
  
  const T* constDataR() const;
  const Complex<T>* constDataC() const;

  Matrix& operator=(const Matrix& matrix);

  template<class T2>
  Matrix& operator=(const Matrix<T2>& matrix);

  Matrix& operator=(T value);

  Matrix& operator=(Complex<T> value);

  Matrix& operator*=(T value);

  Matrix& operator*=(Complex<T> value);

  Matrix& operator/=(T value);

  Matrix& operator+=(const Matrix& matrix);

  Matrix& operator-=(const Matrix& matrix);

  T maxR() const;
  T minR() const;
  
  PROTEPARA(DataType,dataType);
  PROTEPARA(std::string,name);

  PROTEPARA(unsigned int,sizeR);
  PROTEPARA(unsigned int,sizeC);
  PROTEPARA(bool,existence);
  PROTEPARA(T,max);
  PROTEPARA(T,min);
  PROTEPARA(int,averageNumber);

  T* matr;
  T* matr_fast;

 protected:

  int loR[3];
  int hiR[3];
  unsigned int dimR[3];

  int loC[3];
  int hiC[3];
  unsigned int dimC[3];

  PROTEPARA(T,varianceR);
  PROTEPARA(T,meanR);
  PROTEPARA(T,meanC);
};

// --------------------------------------------------------------------------------------------------------------------------
// definitions
// --------------------------------------------------------------------------------------------------------------------------

#undef LOGLEVEL
#define LOGLEVEL 0

template<class T> unsigned int Matrix<T>::collectedNumber_ = 0;
template<class T> unsigned int Matrix<T>::collectedSize_ = 0;

template<class T>
std::ostream& operator<<(std::ostream &of, const Matrix<T>& matrix);

template<class T>
void maxOfMatrix(const Matrix<T>& vx, unsigned int number, std::vector<T>& maxVec, std::vector<GridPoint>& gridPointVec);

template<class T>
void normOfVectorField(Matrix<T>& result, const Matrix<T>& vx, const Matrix<T>& vy, const Matrix<T>& vz);

template<class T>
void normSqrOfVectorField(Matrix<T>& result, const Matrix<T>& vx, const Matrix<T>& vy, const Matrix<T>& vz);

template<class T>
Matrix<T>::Matrix(const std::string& name)
{
  ERRORLOGL(1,"Matrix<T>::Matrix()");

  name_ = name;
  matr = 0;
  sizeR_ = 0;
  sizeC_ = 0;

  collectedNumber_++;

  existence_ = false;

  averageNumber_ = 0;

  ERRORLOGL(4,PAR(existence_));

  ERRORLOGL(4,"Matrix<T>::Matrix()-end");
}

template<class T>
Matrix<T>::Matrix(const Matrix<T>& matrix)
{
  ERRORLOGL(1,"Matrix<T>::Matrix(const Matrix<T>& matrix)");

  name_ = matrix.name_;

  if(matrix.existence_) {

    averageNumber_ = matrix.averageNumber_;

    for(int i=0;i<3;i++) {
      loR[i] = matrix.loR[i];
      hiR[i] = matrix.hiR[i];

      loC[i] = matrix.loC[i];
      hiC[i] = matrix.hiC[i];

      dimR[i] = matrix.dimR[i];
      dimC[i] = matrix.dimC[i];
    }

    sizeR_ = matrix.sizeR_;
    sizeC_ = matrix.sizeC_;

    max_ = matrix.max_;
    min_ = matrix.min_;

    varianceR_ = matrix.varianceR_;
    meanR_ = matrix.meanR_;
    meanC_ = matrix.meanC_;
  
    dataType_ = matrix.dataType_;

    // matr = (T*) fftw_malloc(sizeR_ * sizeof(T));
    //matr = (T*) memalign(16,sizeR_ * sizeof(T));
    //    matr = new T[sizeR_];
    matr = new T[sizeC_*2];

    collectedSize_ += 2*sizeC_*sizeof(T);

    if(matr == NULL) {
      std::cerr << "ERROR: failed to allocate mem for Matrix<T>\n";
      exit(1);
    }

    int p = -loR[0];
    p = (p*dimR[1]-loR[1])*dimR[2] - loR[2];

    matr_fast = matr + p;

    existence_ = true;
 
    //   if(matrix.dataType_ == RealData)
    //     resize(matrix.loR, matrix.hiR,matrix.dataType_,matrix.name_);
    //   else
    //     resize(matrix.loC, matrix.hiC,matrix.dataType_,matrix.name_);
  
    this->operator=(matrix);
  }
  else {
    existence_ = false;
    averageNumber_ = 0;
  }
  collectedNumber_++;
    ERRORLOGL(4,"Matrix<T>::Matrix(const Matrix<T>& matrix)-end");
}

template<class T>
Matrix<T>& Matrix<T>::operator=(const Matrix<T>& matrix)
{
  ERRORLOGL(3,"Matrix<T>::operator=(const Matrix<T>& matrix)");

  ERRORLOGL(4,PAR(sizeR_));
  ERRORLOGL(4,PAR(matrix.sizeR_));
  
  for (int d = 0; d < 3; d++) {

    ASSERT(matrix.get_loR(d) == loR[d],BK::appendString("T Matrix<T>::operator=(const Matrix<T>& matrix) : ",name_,": matrix.get_loR(",d,") = ",matrix.get_loR(d)," != loR[d] = ",loR[d]));
    ASSERT(matrix.get_hiR(d) == hiR[d],BK::appendString("T Matrix<T>::operator=(const Matrix<T>& matrix) : ",name_,": matrix.get_hiR(",d,") = ",matrix.get_hiR(d)," != hiR[d] = ",hiR[d]));
    ASSERT(matrix.get_loC(d) == loC[d],BK::appendString("T Matrix<T>::operator=(const Matrix<T>& matrix) : ",name_,": matrix.get_loC(",d,") = ",matrix.get_loC(d)," != loC[d] = ",loC[d]));
    ASSERT(matrix.get_hiC(d) == hiC[d],BK::appendString("T Matrix<T>::operator=(const Matrix<T>& matrix) : ",name_,": matrix.get_hiC(",d,") = ",matrix.get_hiC(d)," != hiC[d] = ",hiC[d]));

    // assert(matrix.get_loR(d) == loR[d]);
    // assert(matrix.get_hiR(d) == hiR[d]);
    // assert(matrix.get_loC(d) == loC[d]);
    // assert(matrix.get_hiC(d) == hiC[d]);
    
    // if (matrix.get_loR(d) != loR[d] ||
    // 	matrix.get_hiR(d) != hiR[d]) {
    //   ERRORLOGL(0,"matrix.loR[d] != loR[d] || matrix.hiR[d] != hiR[d]");
    //   exit(0);
    //   deleteData();
    //   if(matrix.dataType_ == RealData)
    // 	resize(matrix.loR, matrix.hiR,matrix.dataType_,matrix.name_);
    //   else
    // 	resize(matrix.loC, matrix.hiC,matrix.dataType_,matrix.name_);
    //   break;
    // }
  }
  
  ERRORLOG(4,"copying data ...");
  //  memcpy(matr,matrix.matr,sizeof(T)*sizeR_);
  memcpy(matr,matrix.matr,sizeof(T)*2*sizeC_);

  dataType_ = matrix.dataType_;
  varianceR_ = matrix.varianceR_;
  meanR_ = matrix.meanR_;
  meanC_ = matrix.meanC_;
  max_ = matrix.max_;
  min_ = matrix.min_;

  ERRORLOGL(4,"Matrix<T>::operator=(const Matrix<T>& matrix)-end");

  return *this;
}

template<class T>
Matrix<T>::Matrix(Range xRange, Range yRange, Range zRange, DataType type,const std::string& name)
{
  ERRORLOGL(1,"Matrix<T>::Matrix(Range xRange, Range yRange, Range zRange,DataType type,const std::string& name)");

  name_ = name;
  
  existence_ = false;

  collectedNumber_++;

  resize(xRange, yRange, zRange, type, name);
}

template<class T>
Matrix<T>::Matrix(const int* l, const int* h, DataType type,const std::string& name)
{
  ERRORLOGL(1,"Matrix<T>::Matrix(const int* l, const int* h,DataType type,const std::string& name)");

  name_ = name;
  
  existence_ = false;

  collectedNumber_++;
  
  resize(Range(l[0],h[0]),Range(l[1],h[1]),Range(l[2],h[2]),type,name);
}

template<class T>
Matrix<T>::Matrix(int nx, int ny, int nz, DataType type,const std::string& name)
{
  ERRORLOGL(1,"Matrix<T>::Matrix(int nx, int ny, int nz,DataType type,const std::string& name)");

  name_ = name;

  existence_ = false;

  collectedNumber_++;

  resize(Range(0,nx-1), Range(0,ny-1), Range(0,nz-1), type, name);
}

template<class T>
Matrix<T>::~Matrix()
{
  ERRORLOGL(1,"Matrix<T>::~FftwMatrix()");

  deleteData();

  collectedNumber_--;

  ERRORLOGL(4,"Matrix<T>::~FftwMatrix()-end");
}

template<class T>
void Matrix<T>::resize(int nx, int ny, int nz, DataType type,const std::string& name)
{
  ERRORLOGL(1,"Matrix<T>::resize(int nx, int ny, int nz,DataType type,const std::string& name)");

  ERRORLOGL(4,PAR(nx));
  ERRORLOGL(4,PAR(ny));
  ERRORLOGL(4,PAR(nz));

  existence_ = false;  

  resize(Range(0,nx-1),Range(0,ny-1),Range(0,nz-1),type,name);

  ERRORLOGL(4,"Matrix<T>::resize(int nx, int ny, int nz)");
}

template<class T>
void Matrix<T>::resize(const int* l, const int* h, DataType type,const std::string& name)
{
  ERRORLOGL(1,"Matrix<T>::resize(const int* l, const int* h, DataType type,const std::string& name)");

  existence_ = false;  

  resize(Range(l[0],h[0]),Range(l[1],h[1]),Range(l[2],h[2]),type,name);
}

template<class T>
void Matrix<T>::resize(Range xRange, Range yRange, Range zRange, DataType type,const std::string& name)
{
  ERRORLOGL(1,"Matrix<T>::resize(Range xRange, Range yRange, Range zRange, DataType type,const std::string& name)");

  assert(existence_ == false);
  assert(xRange.range != 0);
  assert(yRange.range != 0);
  assert(zRange.range != 0);

  name_ = name;

  if(type == RealData) {
    loR[0] = xRange.index[0]; loR[1] = yRange.index[0]; loR[2] = zRange.index[0];
    hiR[0] = xRange.index[1]; hiR[1] = yRange.index[1]; hiR[2] = zRange.index[1];

    ERRORLOGL(3,PAR((hiR[2]-loR[2]+1) % 2));
    ERRORLOGL(3,PAR(loR[2]));
    ERRORLOGL(3,PAR(hiR[2]));
    assert((hiR[2]-loR[2]+1) % 2 == 0);

    loC[0] = xRange.index[0]; loC[1] = yRange.index[0]; loC[2] = zRange.index[0];
    hiC[0] = xRange.index[1]; hiC[1] = yRange.index[1]; hiC[2] = (zRange.index[1]-1)/2;
    //    hiC[0] = xRange.index[1]; hiC[1] = yRange.index[1]; hiC[2] = zRange.index[1]/2;
  }  

  if(type == ComplexData) {
    loC[0] = xRange.index[0]; loC[1] = yRange.index[0]; loC[2] = zRange.index[0];
    hiC[0] = xRange.index[1]; hiC[1] = yRange.index[1]; hiC[2] = zRange.index[1];
    
    loR[0] = xRange.index[0]; loR[1] = yRange.index[0]; loR[2] = zRange.index[0];
    //    hiR[0] = xRange.index[1]; hiR[1] = yRange.index[1]; hiR[2] = zRange.index[1]*2+1;
    hiR[0] = xRange.index[1]; hiR[1] = yRange.index[1]; hiR[2] = zRange.index[1]*2+1;

  }  
  
  for(int i=0;i<3;i++) {
    dimR[i] = hiR[i]-loR[i]+1;
    dimC[i] = hiC[i]-loC[i]+1;
  }
  
  sizeR_ = dimR[0]*dimR[1]*dimR[2];
  sizeC_ = dimC[0]*dimC[1]*dimC[2];

  dataType_ = type;

  ERRORLOG(4,BK::appendString("allocation mem of ",2*sizeC_*8/1024/1024," MB"));

  //  matr = (T*) fftw_malloc(sizeR_ * sizeof(T));
  //matr = (T*) memalign(16,sizeR_ * sizeof(T));
  //  matr = new T[sizeR_];
  matr = new T[sizeC_*2];

  collectedSize_ += 2*sizeC_*sizeof(T);

  if(matr == NULL) {
    std::cerr << "ERROR: failed to allocate mem for Matrix<T>(Range(" << xRange.index[0] << "," << xRange.index[1] 
	      << "),Range(" << yRange.index[0] << "," << yRange.index[1] << "),Range(" 
	      << zRange.index[0] << "," << zRange.index[1] << "))\n";
    abort();
  }

  int p = -loR[0];
  p = (p*dimR[1]-loR[1])*dimR[2] - loR[2];

  matr_fast = matr + p;

  existence_ = true;

  ERRORLOGL(4,PAR6(dimR[0],dimR[1],dimR[2],dimC[0],dimC[1],dimC[2]));
  ERRORLOGL(4,PAR2(sizeR_,sizeC_));
  ERRORLOGL(4,PAR6(loR[0],hiR[0],loR[1],hiR[1],loR[2],hiR[2]));
  ERRORLOGL(4,PAR6(loC[0],hiC[0],loC[1],hiC[1],loC[2],hiC[2]));
  ERRORLOGL(4,PAR(p));
  ERRORLOGL(4,PAR(dataType_));

  ERRORLOGL(4,"Matrix<T>::resize(Range xRange, Range yRange, Range zRange, DataType type)-end");
}

template<class T>
void Matrix<T>::resize(int nxR, int nyR, int nzR, int nxC, int nyC, int nzC, DataType type,const std::string& name)
{
  ERRORLOGL(1,"Matrix<T>::resize(int nxR, int nyR, int nzR, int nxC, int nyC, int nzC, DataType type,const std::string& name)");
  
  assert(nxR != 0 && nyR != 0 && nzR != 0 && nxC != 0 && nyC != 0 && nzC != 0);

  if(nxR*nyR*nzR != nxC*nyC*nzC*2) {
    ERRORLOGL(3,"ERROR in  Matrix<T>::resize(int nxR, int nyR, int nzR, int nxC, int nyC, int nzC, DataType type,const std::string& name):");
    ERRORLOGL(3,"nxR*nyR*nzR == nxC*nyC*nzC*2");
    ERRORLOGL(3,PAR(nxR));
    ERRORLOGL(3,PAR(nyR));
    ERRORLOGL(3,PAR(nzR));
    ERRORLOGL(3,PAR(nxC));
    ERRORLOGL(3,PAR(nyC));
    ERRORLOGL(3,PAR(nzC));
    
    //    exit(1);
  }

  name_ = name;
  dataType_ = type;

  loR[0] = 0; loR[1] = 0; loR[2] = 0;
  hiR[0] = nxR-1; hiR[1] = nyR-1; hiR[2] = nzR-1;
  
  loC[0] = 0; loC[1] = 0; loC[2] = 0;
  hiC[0] = nxC-1; hiC[1] = nyC-1; hiC[2] = nzC-1;

  for(int i=0;i<3;i++) {
    dimR[i] = hiR[i]-loR[i]+1;
    dimC[i] = hiC[i]-loC[i]+1;
  }
  
  sizeR_ = dimR[0]*dimR[1]*dimR[2];
  sizeC_ = dimC[0]*dimC[1]*dimC[2];

  //matr = (T*) memalign(16,sizeR_ * sizeof(T));
  //  matr = (T*) fftw_malloc(sizeR_ * sizeof(T));
  //  matr = new T[sizeR_];
  matr = new T[sizeC_*2];

  collectedSize_ += 2*sizeC_*sizeof(T);

  if(matr == NULL) {
    std::cerr << "ERROR: failed to allocate mem for Matrix<T>" << std::endl;
    abort();
  }

  int p = -loR[0];
  p = (p*dimR[1]-loR[1])*dimR[2] - loR[2];

  matr_fast = matr + p;

  existence_ = true;

  ERRORLOGL(4,PAR6(dimR[0],dimR[1],dimR[2],dimC[0],dimC[1],dimC[2]));
  ERRORLOGL(4,PAR2(sizeR_,sizeC_));
  ERRORLOGL(4,PAR6(loR[0],hiR[0],loR[1],hiR[1],loR[2],hiR[2]));
  ERRORLOGL(4,PAR6(loC[0],hiC[0],loC[1],hiC[1],loC[2],hiC[2]));
  ERRORLOGL(4,PAR(p));
  ERRORLOGL(4,PAR(dataType_));

  ERRORLOGL(4,"Matrix<T>::resize(int nxR, int nyR, int nzR, int nxC, int nyC, int nzC, DataType type,const std::string& name)-end");
}


template<class T>
void Matrix<T>::deleteData()
{
  ERRORLOGL(1,"Matrix<T>::deleteData()");

  if (matr) {
    //    free(matr);
    delete [] matr;

    collectedSize_ -= 2*sizeC_*sizeof(T);
  
  }

  matr = 0;
  sizeR_ = 0;
  sizeC_ = 0;

  ERRORLOGL(4,"Matrix<T>::deleteData()-end");
}


template<class T>
inline T Matrix<T>::r(int i, int j, int k) const
{
  ERRORLOGL(5,BK::appendString("Matrix<T>::r(int ",i,",int ",j,", int ",k,") const"));
  
  ASSERT(dataType_ == RealData,BK::appendString("T Matrix<T>::r(int i, int j, int k) const : ",name_,": DataType = ",dataType_," != RealData"));

//   ASSERT((i >= loR[0]) && (i <= hiR[0]),"T Matrix<T>::r(int i, int j, int k) const : " << name_ << ": i = "
// 	 << i << " < loR[0] = " << loR[0] << " || i = " << i << " > hiR[0] = " << hiR[0]);
  ASSERT((i >= loR[0]) && (i <= hiR[0]),BK::appendString("T Matrix<T>::r(int i, int j, int k) const : ",name_,": i = "
							 ,i," < loR[0] = ",loR[0]," || i = ",i," > hiR[0] = ",hiR[0]));
  ASSERT((j >= loR[1]) && (j <= hiR[1]),BK::appendString("T Matrix<T>::r(int i, int j, int k) const : ",name_,": j = "
							 ,j," < loR[1] = ",loR[1]," || j = ",j," > hiR[1] = ",hiR[1]));
  ASSERT((k >= loR[2]) && (k <= hiR[2]),BK::appendString("T Matrix<T>::r(int i, int j, int k) const : ",name_,": k = "
							 ,k," < loR[2] = ",loR[2]," || i = ",k," > hiR[2] = ",hiR[2]));

  return matr_fast[int(k + dimR[2]*(j + dimR[1]*i))]; // C-ordering
  //return matr_fast[int(i + dimR[0]*(j + dimR[1]*k))];
}

template<class T>
inline T& Matrix<T>::r(int i, int j, int k)
{
  ERRORLOGL(5,BK::appendString("Matrix<T>::r(int ",i,",int ",j,", int ",k,")"));

  ASSERT(dataType_ == RealData,BK::appendString("T Matrix<T>::r(int i, int j, int k) const : ",name_,": DataType = ",dataType_," != RealData"));
//   ASSERT((i >= loR[0]) && (i <= hiR[0]),"T Matrix<T>::r(int i, int j, int k) const : " << name_ << ": i = "
// 	 << i << " < loR[0] = " << loR[0] << " || i = " << i << " > hiR[0] = " << hiR[0]);

  ASSERT((i >= loR[0]) && (i <= hiR[0]),BK::appendString("T Matrix<T>::r(int i, int j, int k) const : ",name_,": i = "
							 ,i," < loR[0] = ",loR[0]," || i = ",i," > hiR[0] = ",hiR[0]));
  ASSERT((j >= loR[1]) && (j <= hiR[1]),BK::appendString("T Matrix<T>::r(int i, int j, int k) const : ",name_,": j = "
							 ,j," < loR[1] = ",loR[1]," || j = ",j," > hiR[1] = ",hiR[1]));
  ASSERT((k >= loR[2]) && (k <= hiR[2]),BK::appendString("T Matrix<T>::r(int i, int j, int k) const : ",name_,": k = "
							 ,k," < loR[2] = ",loR[2]," || i = ",k," > hiR[2] = ",hiR[2]));

  return matr_fast[int(k + dimR[2]*(j + dimR[1]*i))]; // C-ordering
  //return matr_fast[int(i + dimR[0]*(j + dimR[1]*k))];
}

template<class T>
inline T Matrix<T>::ri(unsigned int i) const
{
  ERRORLOGL(5,BK::appendString("Matrix<T>::ri(int ",i,") const"));

  assert(dataType_ == RealData);
  assert((i >= 0 && i < sizeR_));

  return matr[i];
}

template<class T>
inline T& Matrix<T>::ri(unsigned int i)
{
  ERRORLOGL(5,BK::appendString("Matrix<T>::ri(int ",i,")"));

  assert(dataType_ == RealData);
  assert((i >= 0 && i < sizeR_));

  return matr[i];
}

template<class T>
inline const Complex<T>& Matrix<T>::ci(unsigned int i) const
{
  ERRORLOGL(5,BK::appendString("Matrix<T>::ci(int ",i,") const"));

  ASSERT(dataType_ == ComplexData,BK::appendString("T Matrix<T>::ci(int i, int j, int k) const : ",name_,": DataType = ",dataType_," != ComplexData"));
  assert((i >= 0 && i < sizeC_));

  return reinterpret_cast<Complex<T>*>(matr)[i];
}

template<class T>
inline Complex<T>& Matrix<T>::ci(unsigned int i)
{
  ERRORLOGL(5,BK::appendString("Matrix<T>::ci(int ",i,")"));

  ASSERT(dataType_ == ComplexData,BK::appendString("T Matrix<T>::ci(int i, int j, int k) const : ",name_,": DataType = ",dataType_," != ComplexData"));
  assert((i >= 0 && i < sizeC_));

  return reinterpret_cast<Complex<T>*>(matr)[i];
}

template<class T>
inline const Complex<T>& Matrix<T>::c(int i, int j, int k) const
{
  ERRORLOGL(5,BK::appendString("Matrix<T>::c(int ",i,",int ",j,", int ",k,") const"));

  ASSERT(dataType_ == ComplexData,BK::appendString("T Matrix<T>::c(int i, int j, int k) const : ",name_,": DataType = ",dataType_," != ComplexData"));
  //  assert(dataType_ == ComplexData);
  assert((i >= loC[0]) && (i <= hiC[0]));
  assert((j >= loC[1]) && (j <= hiC[1]));
  assert((k >= loC[2]) && (k <= hiC[2]));

  return reinterpret_cast<Complex<T>*>(matr_fast)[int(k + dimC[2]*(j + dimC[1]*i))]; // C-ordering
  //return reinterpret_cast<Complex<T>*>(matr_fast)[int(i + dimC[0]*(j + dimC[1]*k))];
}

template<class T>
inline Complex<T>& Matrix<T>::c(int i, int j, int k)
{
  ERRORLOGL(5,BK::appendString("Matrix<T>::c(int ",i,",int ",j,", int ",k,")"));

  ASSERT(dataType_ == ComplexData,BK::appendString("T Matrix<T>::c(int i, int j, int k) const : ",name_,": DataType = ",dataType_," != ComplexData"));
  //  assert(dataType_ == ComplexData);
  assert((i >= loC[0]) && (i <= hiC[0]));
  assert((j >= loC[1]) && (j <= hiC[1]));
  assert((k >= loC[2]) && (k <= hiC[2]));

  return reinterpret_cast<Complex<T>*>(matr_fast)[int(k + dimC[2]*(j + dimC[1]*i))]; // C-ordering
  //return reinterpret_cast<Complex<T>*>(matr_fast)[int(i + dimC[0]*(j + dimC[1]*k))];
}

template<class T>
inline T* Matrix<T>::dataR()
{
  assert(dataType_ == RealData);
  return matr;
}

template<class T>
inline Complex<T>* Matrix<T>::dataC()
{
  assert(dataType_ == ComplexData);
  return reinterpret_cast<Complex<T>*>(matr);
  
}

template<class T>
inline const T* Matrix<T>::constDataR() const
{
  return matr;
}

template<class T>
inline const Complex<T>* Matrix<T>::constDataC() const
{
  return reinterpret_cast<const Complex<T>*>(matr);
}

template<class T>
inline bool Matrix<T>::inRangeC(int x,int y, int z) const
{
  ERRORLOGL(3,"Matrix<T>::inRangeC");

  bool xInRangeC = x >= loC[0] && x <= hiC[0];
  bool yInRangeC = y >= loC[1] && y <= hiC[1];
  bool zInRangeC = z >= loC[2] && z <= hiC[2];

  ERRORLOGL(4,"Matrix<T>::inRangeC-end");

  return xInRangeC && yInRangeC && zInRangeC;
}

template<class T>
inline bool Matrix<T>::inRangeR(int x,int y, int z) const
{
  ERRORLOGL(3,"Matrix<T>::inRangeC");

  bool xInRangeR = x >= loR[0] && x <= hiR[0];
  bool yInRangeR = y >= loR[1] && y <= hiR[1];
  bool zInRangeR = z >= loR[2] && z <= hiR[2];

  ERRORLOGL(4,"Matrix<T>::inRangeC-end");

  return xInRangeR && yInRangeR && zInRangeR;
}

template<class T>
inline T Matrix<T>::operator()(int i, int j, int k) const
{
  return r(i,j,k);
}

template<class T>
inline T& Matrix<T>::operator()(int i, int j, int k)
{
  return r(i,j,k);
}

template<class T>
inline bool Matrix<T>::checkProperties(const Matrix<T>& matrix)
{
  ERRORLOGL(3,"Matrix<T>::checkProperties");

  for(int i=0;i<3;i++) {
    assert(loR[i] == matrix.loR[i]);
    assert(hiR[i] == matrix.hiR[i]);
    assert(loC[i] == matrix.loC[i]);
    assert(hiC[i] == matrix.hiC[i]);
    assert(dimR[i] == matrix.dimR[i]);
    assert(dimC[i] == matrix.dimC[i]);
  }
  assert(existence_ == matrix.existence_);    

  ERRORLOGL(4,"Matrix<T>::checkProperties-end");  

  return true;
}

template<class T>
inline void Matrix<T>::swapMatrix(Matrix<T>& matrix)
{
  ERRORLOGL(3,"Matrix<T>::swapMatrix");
  
  assert(checkProperties(matrix) == true);

  T* matrTemp;
  
  matrTemp = matrix.matr;
  
  matrix.matr = this->matr;

  this->matr = matrTemp;

  ERRORLOGL(4,"Matrix<T>::swapMatrix-end");
}


template<class T>
template<class T2>
Matrix<T>& Matrix<T>::operator=(const Matrix<T2>& matrix)
{
  ERRORLOGL(3,"Matrix<T>::operator=(const Matrix<T2>& matrix)");

  ERRORLOGL(4,PAR(sizeR_));
  ERRORLOGL(4,PAR(matrix.sizeR_));

  assert(sizeR_ == matrix.get_sizeR());
  assert(dataType_ == matrix.get_dataType());

  for (int d = 0; d < 3; d++) {
      
      assert(matrix.get_loR(d) == loR[d]);
      assert(matrix.get_hiR(d) == hiR[d]);
      assert(matrix.get_loC(d) == loC[d]);
      assert(matrix.get_hiC(d) == hiC[d]);

    //   if (matrix.get_loR(d) != loR[d] ||
    // 	  matrix.get_hiR(d) != hiR[d]) {
	
    //   ERRORLOGL(0,"matrix.loR[d] != loR[d] || matrix.hiR[d] != hiR[d]");
    //   exit(0);
    //   deleteData();
    //   resize(matrix.get_loR(), matrix.get_hiR(),matrix.get_dataType(),matrix.get_name());
    //   break;
    // }
  }
 
  if(dataType_ == RealData) {
    for(unsigned int i=0;i<sizeR_;i++) {
      this->ri(i) = matrix.ri(i);
    }
  }
  else {
    for(unsigned int i=0;i<sizeC_;i++) {
      this->ci(i) = matrix.ci(i);
    }
  }

  ERRORLOGL(4,"Matrix<T>::operator=(const Matrix<T2>& matrix)-end");

  return *this;
}

template<class T>
Matrix<T>& Matrix<T>::operator=(T value)
{
  ERRORLOGL(3,"Matrix<T>::operator=(T value)");

  assert(dataType_ == RealData);

  for (int i = loR[0]; i <= hiR[0]; i++) {
    for (int j = loR[1]; j <= hiR[1]; j++) {
      for (int k = loR[2]; k <= hiR[2]; k++) {
	r(i,j,k) = value;
      }
    }
  }

  ERRORLOGL(4,"Matrix<T>::operator=(T value)-end");

  return *this;
}

template<class T>
Matrix<T>& Matrix<T>::operator=(Complex<T> value)
{
  ERRORLOGL(3,"Matrix<T>::operator=(Complex<T> value)");

  //assert(existence_ == true);
  ASSERT(existence_ == true ,BK::appendString("Matrix<T>& Matrix<T>::operator=(Complex<T> value) : ",name_,": DataType = ",dataType_," != ComplexData"));
  
  
  ASSERT(dataType_ == ComplexData,BK::appendString("Matrix<T>& Matrix<T>::operator=(Complex<T> value) : ",name_,": DataType = ",dataType_," != ComplexData"));

  ERRORLOGL(4,PAR(loC[0]));
  ERRORLOGL(4,PAR(hiC[0]));
  ERRORLOGL(4,PAR(loC[1]));
  ERRORLOGL(4,PAR(hiC[1]));
  ERRORLOGL(4,PAR(loC[2]));
  ERRORLOGL(4,PAR(hiC[2]));

  for (int i = loC[0]; i <= hiC[0]; i++) {
    for (int j = loC[1]; j <= hiC[1]; j++) {
      for (int k = loC[2]; k <= hiC[2]; k++) {
	c(i,j,k) = value;
      }
    }
  }

  ERRORLOGL(4,"Matrix<T>::operator=(Complex<T> value)-end");

  return *this;
}

template<class T>
Matrix<T>& Matrix<T>::operator*=(T value)
{
  ERRORLOGL(3,"Matrix<T>::operator*=(T value)");

  //  for(unsigned int i=0;i<sizeR_;i++) {
  for(unsigned int i=0;i<2*sizeC_;i++) {
    matr[i] *= value;
  }

  ERRORLOGL(4,"Matrix<T>::operator*=(T value)-end");

  return *this;
}

template<class T>
Matrix<T>& Matrix<T>::operator/=(T value)
{
  ERRORLOGL(3,"Matrix<T>::operator/=(T value)");

  //  for(unsigned int i=0;i<sizeR_;i++) {
  for(unsigned int i=0;i<2*sizeC_;i++) {
    matr[i] /= value;
  }

  ERRORLOGL(4,"Matrix<T>::operator/=(T value)-end");

  return *this;
}

template<class T>
Matrix<T>& Matrix<T>::operator*=(Complex<T> value)
{
  ERRORLOGL(3,"Matrix<T>::operator*=(Complex<T> value)");

  assert(dataType_ == ComplexData);
 
  for (int i = loC[0]; i <= hiC[0]; i++) {
    for (int j = loC[1]; j <= hiC[1]; j++) {
      for (int k = loC[2]; k <= hiC[2]; k++) {
	c(i,j,k) *= value;
      }
    }
  }

  ERRORLOGL(4,"Matrix<T>::operator*=(Complex<T> value)-end");

  return *this;
}

template<class T>
Matrix<T>& Matrix<T>::operator+=(const Matrix<T>& matrix)
{
  ERRORLOGL(3,"Matrix<T>::operator+=(const Matrix<T>& matrix)");

  ERRORLOGL(4,PAR(sizeR_));
  ERRORLOGL(4,PAR(matrix.sizeR_));

  assert(sizeR_ == matrix.sizeR_);
  assert(dataType_ == matrix.dataType_);
  
  //  for(unsigned int i=0;i<sizeR_;i++) {
  for(unsigned int i=0;i<2*sizeC_;i++) {
    matr[i] += matrix.matr[i];
  }

  ERRORLOGL(4,"Matrix<T>::operator+=(const Matrix<T>& matrix)-end");

  return *this;
}

template<class T>
Matrix<T>& Matrix<T>::operator-=(const Matrix<T>& matrix)
{
  ERRORLOGL(3,"Matrix<T>::operator-=(const Matrix<T>& matrix)");

  ERRORLOGL(4,PAR(sizeR_));
  ERRORLOGL(4,PAR(matrix.sizeR_));

  assert(sizeR_ == matrix.sizeR_);
  assert(dataType_ == matrix.dataType_);
  
  //  for(unsigned int i=0;i<sizeR_;i++) {
  for(unsigned int i=0;i<2*sizeC_;i++) {
    matr[i] -= matrix.matr[i];
  }

  ERRORLOGL(4,"Matrix<T>::operator-=(const Matrix<T>& matrix)-end");

  return *this;
}

template<class T>
T Matrix<T>::varianceR()
{
  ERRORLOGL(3,"Matrix<T>::varianceR");

  assert(dataType_ == RealData);

  T variance = 0;

  meanR();

  for(int x=get_loR(0);x<=get_hiR(0);x++) {
    for(int y=get_loR(1);y<=get_hiR(1);y++) {
      for(int z=get_loR(2);z<=get_hiR(2);z++) {
	variance += sqr(r(x,y,z)-meanR_);
      }
    }
  }

  variance /= sizeR_;

  varianceR_ = variance;

  return variance;
  
  ERRORLOGL(4,"Matrix<T>::varianceR-end");
}

template<class T>
T Matrix<T>::meanR()
{
  ERRORLOGL(3,"Matrix<T>::meanR");

  assert(dataType_ == RealData);

  T mean = 0;

  for(int x=get_loR(0);x<=get_hiR(0);x++) {
    for(int y=get_loR(1);y<=get_hiR(1);y++) {
      for(int z=get_loR(2);z<=get_hiR(2);z++) {
	mean += r(x,y,z);
      }
    }
  }

  mean /= sizeR_;

  meanR_ = mean;
  
  return mean;

  ERRORLOGL(4,"Matrix<T>::meanR-end");
}

template<class T>
T Matrix<T>::minR() const
{
  ERRORLOGL(3,"Matrix<T>::minR");

  assert(dataType_ == RealData);

  T min = r(0,0,0);

  for(int x=get_loR(0);x<=get_hiR(0);x++) {
    for(int y=get_loR(1);y<=get_hiR(1);y++) {
      for(int z=get_loR(2);z<=get_hiR(2);z++) {
	if(r(x,y,z) > min)
	  min = r(x,y,z);
      }
    }
  }

  return min;

  ERRORLOGL(4,"Matrix<T>::minR-end");
}

template<class T>
T Matrix<T>::maxR() const
{
  ERRORLOGL(3,"Matrix<T>::minR");

  assert(dataType_ == RealData);

  T max = r(0,0,0);

  for(int x=get_loR(0);x<=get_hiR(0);x++) {
    for(int y=get_loR(1);y<=get_hiR(1);y++) {
      for(int z=get_loR(2);z<=get_hiR(2);z++) {
	if(r(x,y,z) > max)
	  max = r(x,y,z);
      }
    }
  }

  return max;

  ERRORLOGL(4,"Matrix<T>::minR-end");
}

template<class T>
Complex<T> Matrix<T>::meanC()
{
  ERRORLOGL(3,"Matrix<T>::meanC");

  assert(dataType_ == ComplexData);

  Complex<T> mean = Complex<T>(0.,0.);

  for(int x=get_loC(0);x<=get_hiC(0);x++) {
    for(int y=get_loC(1);y<=get_hiC(1);y++) {
      for(int z=get_loC(2);z<=get_hiC(2);z++) {
	mean += c(x,y,z);
      }
    }
  }

  mean /= sizeC_;
  
  return mean;

  ERRORLOGL(4,"Matrix<T>::meanC-end");
}


template<class T>
std::ostream& operator<<(std::ostream &of, const Matrix<T>& matrix)
{
  switch(matrix.get_dataType()) {
  case RealData: 
    
    of << "DataType = RealData , Dimensionen: (" << matrix.get_loR(0) << "," << matrix.get_hiR(0) << ")x("
       << matrix.get_loR(1) << "," << matrix.get_hiR(1) << ")x(" << matrix.get_loR(2) << "," 
       << matrix.get_hiR(2) << ")" << std::endl;

    for(int x=matrix.get_loR(0);x<=matrix.get_hiR(0);x++) {
      for(int y=matrix.get_loR(1);y<=matrix.get_hiR(1);y++) {
	for(int z=matrix.get_loR(2);z<=matrix.get_hiR(2);z++) {
	  of << "(" << x << "," << y << "," << z << ") = " << matrix.r(x,y,z) << "  ";
	}
	of << std::endl;
      }
      of << std::endl;
    }
    
    break;
    
  case ComplexData:

    of << "DataType = ComplexData , Dimensionen: (" << matrix.get_loC(0) << "," << matrix.get_hiC(0) << ")x("
       << matrix.get_loC(1) << "," << matrix.get_hiC(1) << ")x(" << matrix.get_loC(2) << "," 
       << matrix.get_hiC(2) << ")" << std::endl;

    for(int x=matrix.get_loC(0);x<=matrix.get_hiC(0);x++) {    
      for(int y=matrix.get_loC(1);y<=matrix.get_hiC(1);y++) {
	for(int z=matrix.get_loC(2);z<=matrix.get_hiC(2);z++) {    	
	  of << "(" << x << "," << y << "," << z << ") = " << matrix.c(x,y,z) << "  ";
	}
	of << std::endl;
      }
      of << std::endl;
    }
    
    break;
  }
  return of;
}

template<class T>
void normSqrOfVectorField(Matrix<T>& result, const Matrix<T>& vx, const Matrix<T>& vy, const Matrix<T>& vz)
{
  ERRORLOGL(3,"normSqrOfVectorField");

  DataType dataType= vx.get_dataType();

  assert(vy.get_dataType() == dataType);
  assert(vz.get_dataType() == dataType);

  assert(vx.get_sizeR() == result.get_sizeR());
  assert(vy.get_sizeR() == result.get_sizeR());
  assert(vz.get_sizeR() == result.get_sizeR());

  result.set_dataType(dataType);

  if(dataType == RealData) {
    
    for(int x=vx.get_loR(0);x<=vx.get_hiR(0);x++) {
      for(int y=vx.get_loR(1);y<=vx.get_hiR(1);y++) {
	for(int z=vx.get_loR(2);z<=vx.get_hiR(2);z++) {
	  result.r(x,y,z) = sqr(vx.r(x,y,z))+sqr(vy.r(x,y,z))+sqr(vz.r(x,y,z));
	}
      }
    }
  }
  else {
    for(int x=vx.get_loC(0);x<=vx.get_hiC(0);x++) {
      for(int y=vx.get_loC(1);y<=vx.get_hiC(1);y++) {
	for(int z=vx.get_loC(2);z<=vx.get_hiC(2);z++) {
	  result.c(x,y,z) = sqrNorm(vx.c(x,y,z))+sqrNorm(vy.c(x,y,z))+sqrNorm(vz.c(x,y,z));
	}
      }
    }
  }

  ERRORLOGL(4,"normSqrOfVectorField-end");
}

template<class T>
void normOfVectorField(Matrix<T>& result, const Matrix<T>& vx, const Matrix<T>& vy, const Matrix<T>& vz)
{
  ERRORLOGL(3,"normSqrOfVectorField");

  DataType dataType= vx.get_dataType();

  assert(vy.get_dataType() == dataType);
  assert(vz.get_dataType() == dataType);

  ASSERT(vx.get_sizeR() == result.get_sizeR(),BK::appendString(PAR(vx.get_sizeR()),",",PAR(result.get_sizeR())));
  ASSERT(vy.get_sizeR() == result.get_sizeR(),BK::appendString(PAR(vy.get_sizeR()),",",PAR(result.get_sizeR())));
  ASSERT(vz.get_sizeR() == result.get_sizeR(),BK::appendString(PAR(vz.get_sizeR()),",",PAR(result.get_sizeR())));

  result.set_dataType(dataType);

  if(dataType == RealData) {
    
    for(int x=vx.get_loR(0);x<=vx.get_hiR(0);x++) {
      for(int y=vx.get_loR(1);y<=vx.get_hiR(1);y++) {
	for(int z=vx.get_loR(2);z<=vx.get_hiR(2);z++) {
	  result.r(x,y,z) = sqrt(sqr(vx.r(x,y,z))+sqr(vy.r(x,y,z))+sqr(vz.r(x,y,z)));
	}
      }
    }
  }
  else {
    for(int x=vx.get_loC(0);x<=vx.get_hiC(0);x++) {
      for(int y=vx.get_loC(1);y<=vx.get_hiC(1);y++) {
	for(int z=vx.get_loC(2);z<=vx.get_hiC(2);z++) {
	  result.c(x,y,z) = sqrt(sqrNorm(vx.c(x,y,z))+sqrNorm(vy.c(x,y,z))+sqrNorm(vz.c(x,y,z)));
	}
      }
    }
  }

  ERRORLOGL(4,"normSqrOfVectorField-end");
}

template<class T>
T normOfVectorField(const Matrix<T>& vx, const Matrix<T>& vy, const Matrix<T>& vz)
{
  ERRORLOGL(3,"normOfVectorField");
  
  assert(vx.get_dataType() == BK::RealData);
  assert(vy.get_dataType() == BK::RealData);
  assert(vz.get_dataType() == BK::RealData);
  
  T norm = 0;
  
  for(unsigned int i = 0; i<vx.get_sizeR(); i++)
    norm += sqr(vx.ri(i))+sqr(vy.ri(i))+sqr(vz.ri(i));    

  return norm;

  ERRORLOGL(4,"normOfVectorField-end");
}

template<class T>
void maxOfMatrix(const Matrix<T>& vx, unsigned int number, std::vector<T>& maxVec, std::vector<GridPoint>& gridPointVec)
{
  ERRORLOGL(3,"maxOfMatrix");
  
  assert(vx.get_dataType() == RealData);
  assert(number <= vx.get_sizeR());
  
  std::map<T,GridPoint> maxMap;
  
  bool breakIt = false;
  for(int x=vx.get_loR(0);x<=vx.get_hiR(0);x++) {
    for(int y=vx.get_loR(1);y<=vx.get_hiR(1);y++) {
      for(int z=vx.get_loR(2);z<=vx.get_hiR(2);z++) {
	maxMap.insert(std::pair<T,GridPoint>(vx.r(x,y,z),GridPoint(x,y,z)));
	ERRORLOGL(5,PAR(maxMap.size()));
	if(maxMap.size() == number) {
	  breakIt = true; break;
	}
      }
      if(breakIt == true) break;
    }
    if(breakIt == true) break;
  }
  
  ERRORLOGL(4,BK::appendString("anfangssize = ",maxMap.size()));
  
  ERRORLOGL(4,BK::appendString("Anfangs-MaxMap = \n",BK::out(maxMap)));
  
  for(int x=vx.get_loR(0);x<=vx.get_hiR(0);x++) {
    for(int y=vx.get_loR(1);y<=vx.get_hiR(1);y++) {
      for(int z=vx.get_loR(2);z<=vx.get_hiR(2);z++) {
	if(vx.r(x,y,z) > (maxMap.begin())->first ) {
	  ERRORLOG(4,PAR(maxMap.begin()->first));
	  ERRORLOG(4,PAR(vx.r(x,y,z)));
	  maxMap.insert(std::pair<T,GridPoint>(vx.r(x,y,z),GridPoint(x,y,z)));
	  if(maxMap.size() > number)
	    maxMap.erase(maxMap.begin());

	  ERRORLOG(4,PAR(maxMap.size()));
	}
      }
    }
  }
  
  ERRORLOGL(4,BK::appendString("endsize = ",maxMap.size()));

  for(typename std::map<T,GridPoint>::const_iterator it=maxMap.begin();it!=maxMap.end();it++) {
    std::cout << it->first << "  " << it->second << std::endl;
  }

  for(typename std::map<T,GridPoint>::const_iterator it=maxMap.begin();it!=maxMap.end();it++) {
    maxVec.push_back(it->first);
    gridPointVec.push_back(it->second);
  }

  ERRORLOGL(4,"maxOfMatrix-end");
}


} // namespace BK

#endif
