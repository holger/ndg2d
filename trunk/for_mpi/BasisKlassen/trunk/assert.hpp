#ifndef assert_h
#define assert_h

#include <string>

#include <iostream>
#include <cstdlib>

#ifndef NDEBUG
# define ASSERT(expr,msg)						\
  {if(!(expr)) {							\
      std::cerr << "assertion failed in file " <<  __FILE__ << " line " << __LINE__ << " for " << __STRING(expr) << " with message : \n" << msg << std::endl; abort();}}
//__assert_fail (__STRING(expr), __FILE__, __LINE__, __ASSERT_FUNCTION))
#else
# define ASSERT(expr,msg) (void)(0)
#endif

#endif
