#ifndef fftw_complex_h
#define fftw_complex_h

#include <iostream>
#include <sstream>
#include <cmath>

#ifdef MYALPHA
#include <g++-v3/cmath>
#endif

namespace BK {


  //typedef double fftw_real;
 
// Forward declarations
  
// class of complex numbers, useable with FFTW
template<class T>
class Complex;


// euklidic norm
template<class T>
T norm(const Complex<T>& z);
 
/* // Transcendentals: */
/* fftw_complex Cexp(const fftw_complex& z); */

template<class T>
std::ostream& operator<< (std::ostream& of, const Complex<T>& z);

template<class T>
class Complex 
{
 public:
  Complex() {};
  
  // copy constructor
  template<class TYPE>
  Complex(const Complex<TYPE>& z);
  
  // init constructor
  Complex(T x, T y);
  
  // real and imaginary part
  T re;
  T im;
  
  T real() const;
  T imag() const;
  
  // assignment operators for real numbers
  template<class TYPE> Complex& operator=(TYPE x);
  template<class TYPE> Complex& operator+=(TYPE x);
  template<class TYPE> Complex& operator-=(TYPE x);
  template<class TYPE> Complex& operator*=(TYPE x);
  template<class TYPE> Complex& operator/=(TYPE x);
  
  // assignment operators for Complex numbers
  template<class TYPE> Complex<T>& operator=(const Complex<TYPE>& z);
  template<class TYPE> Complex<T>& operator+=(const Complex<TYPE>& z);
  template<class TYPE> Complex<T>& operator-=(const Complex<TYPE>& z);
  template<class TYPE> Complex<T>& operator*=(const Complex<TYPE>& z);
  template<class TYPE> Complex<T>& operator/=(const Complex<TYPE>& z);
  
  double norm() const;
};

  extern const Complex<double> II;

template<class T>
inline Complex<T>::Complex(T x, T y) 
  : re(x), im(y) {}
 
template<class T>
inline T Complex<T>::real() const 
{
  return re;
}

template<class T>
inline T Complex<T>::imag() const 
{
  return im;
}

template<class T>
inline double Complex<T>::norm() const
{
  return this->re*this->re+this->im*this->im;
}

template<class T>
std::ostream& operator<< (std::ostream& of, const Complex<T>& z);

template<class T1,class T2>
inline Complex<T1> operator+(const Complex<T1>& z1, const Complex<T2>& z2)
{
  return Complex<T1>(z1)+=z2;
}

template<class T1,class T2>
inline Complex<T1> operator+(const Complex<T1>& z, const T2& x)
{
  return Complex<T1>(z)+=x;
}

template<class T1,class T2>
inline Complex<T1> operator+(const T2& x, const Complex<T1>& z)
{
  return Complex<T1>(z)+=x;
}

template<class T1,class T2>
inline Complex<T1> operator-(const Complex<T1>& z1, const Complex<T2>& z2)
{
  return Complex<T1>(z1)-=z2;
}

template<class T1,class T2>
inline Complex<T1> operator-(const Complex<T1>& z, const T2& x)
{
  return Complex<T1>(z)-=x;
}

template<class T1,class T2>
inline Complex<T1> operator-(const T2& x, const Complex<T1>& z)
{
  return Complex<T1>(z)-=x;
}

template<class T1,class T2>
inline Complex<T1> operator*(const Complex<T1>& z1, const Complex<T2>& z2)
{
  return (Complex<T1>(z1)*=z2);
}

template<class T1,class T2>
inline Complex<T1> operator*(const Complex<T1>& z, const T2& x)
{
  return Complex<T1>(z)*=x;
}

template<class T1,class T2>
inline Complex<T1> operator*(const T2& x, const Complex<T1>& z)
{
  return Complex<T1>(z)*=x;
}

  template<class T1,class T2>
inline Complex<T1> operator/(const Complex<T1>& z1, const Complex<T2>& z2)
{
  return Complex<T1>(z1)/=z2;
}

template<class T1,class T2>
inline Complex<T1> operator/(const Complex<T1>& z, const T2& x)
{
  return Complex<T1>(z)/=x;
}

template<class T1,class T2>
inline Complex<T1> operator/(const T2& x, const Complex<T1>& z)
{
  return Complex<T1>(z)/=x;
}

template<class T>
inline Complex<T> operator+(const Complex<T>& z)
{
  return z;
}

template<class T>
inline Complex<T> operator-(const Complex<T>& z)
{
  return Complex<T>(-z.re,-z.im);
}

template<class T>
inline bool operator==(const Complex<T>& z1, const Complex<T>& z2)
{
  return z1.re == z2.re && z1.im == z2.im;
}

template<class T>
inline bool operator==(const Complex<T>& z, const T& x)
{
  return z.re == x && z.im == 0.;
}

template<class T>
inline bool operator==(const T& x, const Complex<T>& z)
{
  return z.re == x && z.im == 0.;
}

template<class T>
inline bool operator!=(const Complex<T>& z1, const Complex<T>& z2)
{
  return z1.re != z2.re || z1.im != z2.im;
}

template<class T>
inline bool operator!=(const Complex<T>& z, const T& x)
{
  return z.re != x || z.im != 0.;
}

template<class T>
inline bool operator!=(const T& x, const Complex<T>& z)
{
  return z.re != x || z.im != 0.;
}

template<class T>
inline T real(const Complex<T>& z)
{
  return z.re;
}

template<class T>
inline T imag(const Complex<T>& z)
{
  return z.im;
}

template<class T>
inline T norm(const Complex<T>& z)
{
  return sqrt(z.re * z.re + z.im * z.im);
}

template<class T>
inline T sqrNorm(const Complex<T>& z)
{
  return z.re * z.re + z.im * z.im;
}

template<class T>
inline Complex<T> polar(T rho, T theta)
{
  T a = rho*cos(theta);
  T b = rho*sin(theta);
  return Complex<T>(a,b);
}

template<class T>
inline Complex<T> exp(const Complex<T>& z)
{
  return polar(::exp(z.re),z.im);
}

template<class T>
inline Complex<T> conj(const Complex<T>& z)
{
  return Complex<T>(z.re,-z.im);
}

template<class T,typename _CharT, class _Traits>
std::basic_istream<_CharT, _Traits>&
			 operator>>(std::basic_istream<_CharT, _Traits>& __is, Complex<T>& __x);


template<class T>
template<class TYPE>
inline Complex<T>::Complex(const Complex<TYPE>& z) 
  : re(z.re), im(z.im) {}

template<class T>
template<class TYPE>
Complex<T>& Complex<T>::operator=(TYPE x)
{
  re = x;
  im = T();
  return *this;
}

template<class T>
template<class TYPE>
Complex<T>& Complex<T>::operator+=(TYPE x)
{
  re += x;
  return *this;
}

template<class T>
template<class TYPE>
Complex<T>& Complex<T>::operator-=(TYPE x)
{
  re -= x;
  return *this;
}

template<class T>
template<class TYPE>
Complex<T>& Complex<T>::operator*=(TYPE x)
{
  re *= x;
  im *= x;
  return *this;
}

template<class T>
template<class TYPE>
Complex<T>& Complex<T>::operator/=(TYPE x)
{
  re /= x;
  im /= x;
  return *this;
}

template<class T>
template<class TYPE>
Complex<T>& Complex<T>::operator=(const Complex<TYPE>& z)
{
  re = z.re;
  im = z.im;
  return *this;
}

template<class T>
template<class TYPE>
Complex<T>& Complex<T>::operator+=(const Complex<TYPE>& z)
{
  re += z.re;
  im += z.im;
  return *this;
}

template<class T>
template<class TYPE>
Complex<T>& Complex<T>::operator-=(const Complex<TYPE>& z)
{
  re -= z.re;
  im -= z.im;
  return *this;
}

template<class T>
template<class TYPE>
Complex<T>& Complex<T>::operator*=(const Complex<TYPE>& z)
{
  double re_tmp = re * z.re - im * z.im;
  im = re * z.im + im * z.re;
  re = re_tmp;
  return *this;
}

template<class T>
template<class TYPE>
Complex<T>& Complex<T>::operator/=(const Complex<TYPE>& z)
{
  re = (re * z.re - im * z.im) / BK::norm(z);
  im = (re * z.im + im * z.re) / BK::norm(z);
  return *this;
}

template<class T>
std::ostream& operator<< (std::ostream& of, const Complex<T>& z)
{
  of << "(" << z.re << "," << z.im << ")";
  return of;
}

template<class T,typename _CharT, class _Traits>
std::basic_istream<_CharT, _Traits>&
operator>>(std::basic_istream<_CharT, _Traits>& __is, Complex<T>& __x)
{
  T __re_x, __im_x;
  _CharT __ch;
  __is >> __ch;
  if (__ch == '(') 
    {
      __is >> __re_x >> __ch;
      if (__ch == ',') 
	{
	  __is >> __im_x >> __ch;
	  if (__ch == ')') 
	    __x = Complex<T>(__re_x, __im_x);
	  else
	    __is.setstate(std::ios_base::failbit);
	}
      else if (__ch == ')') 
	__x = Complex<T>(__re_x, T(0));
      else
	__is.setstate(std::ios_base::failbit);
	}
  else 
    {
      __is.putback(__ch);
      __is >> __re_x;
      __x = Complex<T>(__re_x, T(0));
    }
  return __is;
}


}  // namespace BK

  /* #define c_re(c)  ((c).re) */
  /* #define c_im(c)  ((c).im) */

#endif
 
 
  
