#include <iostream>
#include <complex>
#include <fstream>
#include <algorithm>

#include "dateiAusgabe.hpp"

#undef LOGLEVEL
#define LOGLEVEL 4

int main()
{
  std::cerr << "starting main ...\n";

  std::vector<double> vec1 = {11,12,13,14};
  std::vector<double> vec2 = {21,22,23,24};
  std::vector<double> vec3 = {31,32,33,34};
  
  std::vector<std::vector<double>> vecVec = {vec1, vec2, vec3};

  BK::asciiColumnFileOut("vectors.data",vecVec, std::ios::out);

  std::cerr << "main beendet\n";
}

