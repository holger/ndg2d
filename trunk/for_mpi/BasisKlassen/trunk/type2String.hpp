#ifndef type2String_h
#define type2String_h

template<typename T>
struct Type2String {};

template<>
struct Type2String<int>
{
  static std::string get() { return "i"; }
};

template<>
struct Type2String<float>
{
  static std::string get() { return "f"; }
};

template<>
struct Type2String<double>
{
  static std::string get() { return "d"; }
};

template<>
struct Type2String<bool>
{
  static std::string get() { return "b"; }
};

#endif
