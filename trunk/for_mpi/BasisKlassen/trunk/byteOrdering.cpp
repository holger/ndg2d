#include "byteOrdering.hpp"

#include <algorithm>

namespace BK {

void flip4(char b[])
{
  std::swap(b[0], b[3]);
  std::swap(b[1], b[2]);
}

void flip8(char b[])
{
  std::swap(b[0],b[7]);
  std::swap(b[1],b[6]);
  std::swap(b[2],b[5]);
  std::swap(b[3],b[4]);
}

void flip(float& u)
{
  flip4(reinterpret_cast<char*>(&u));
}

void flip(int& u)
{
  flip4(reinterpret_cast<char*>(&u));
}

void flip(double& u)
{
  flip8(reinterpret_cast<char*>(&u));
}

byteordering_type system_ordering() {
  long x = 1;
  if ( *( (char*)&x ) == 1 ) {
    return littleendian;
  }
  else {
    return bigendian;
  }
}

} // namespace BK
