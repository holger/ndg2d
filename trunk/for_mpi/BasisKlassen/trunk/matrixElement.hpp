#ifndef matrixElement_h
#define matrixElement_h

#include <iostream>

namespace BK {

template<class T>
class MatrixElement
{
public:
  
  MatrixElement() {}
  MatrixElement(int x_, int y_, int z_, T value_) : x(x_), y(y_), z(z_), value(value_) {}

  int x;
  int y;
  int z;

  T value;
};

template<class T>
inline std::ostream& operator<<(std::ostream &of, const MatrixElement<T>& matrixElement)
{
  return of << matrixElement.x << " " << matrixElement.y << " " << matrixElement.z << " " << matrixElement.value;
}

} // namespace BK

#endif
