#include "timer.hpp"

#include <cstdlib>

namespace BK {

// TimerManagement -----------------------------------------------------------------------------

TimerManagement::TimerManagement()
{
  logFilename = "log/timerManagement.log";
}

Timer& TimerManagement::get(const std::string name)
{
  // check, ob Timer bereits existiert
  itTimerMap = timerMap.find(name);
  
  // erzeugen und einf�gen eines neues Timers mir key: name
  if(itTimerMap == timerMap.end()){
    std::pair<std::map<std::string, Timer>::iterator,bool> checkPair;
    checkPair = timerMap.insert(std::pair<std::string, Timer>(name,Timer(name)));

    itTimerMap = timerMap.find(name);

    // Namen des Timers belegen
    itTimerMap->second.name = name;
    
    if(checkPair.second != true){
      std::cerr << "TimerManagement::get: Einfuegen von Timer(" << name << ") in timerMap fehlgeschlagen\n";
      exit(1);
    }
    FILELOGL(2,logFilename,
	     appendString("TimerManagement::get: Einf�gen von Timer [", name,"] in map erfolgreich"));
    
  }
  return itTimerMap->second;
}

void TimerManagement::logEveryAccumulatedTime()
{
  for(itTimerMap = timerMap.begin(); itTimerMap != timerMap.end(); itTimerMap++){
    itTimerMap->second.logAccumulatedTime(logFilename);
  }
}

void TimerManagement::logEveryAccumulatedTime(const std::string logFilename_)
{
  for(itTimerMap = timerMap.begin(); itTimerMap != timerMap.end(); itTimerMap++){
    itTimerMap->second.logAccumulatedTime(logFilename_);
  }
}

double TimerManagement::logAccumulatedTimeSumme(const std::string logFilename_)
{
  double gesamtAccumulatedTime = 0;
  for(itTimerMap = timerMap.begin(); itTimerMap != timerMap.end(); itTimerMap++){
    gesamtAccumulatedTime += itTimerMap->second.accumulatedTime(); 
  }
  FILELOGL(0,logFilename_,
	   appendString("AccumulatedTime  [SUMME] = ", gesamtAccumulatedTime));
  return gesamtAccumulatedTime;
}

// Timer -----------------------------------------------------------------------------

Timer::Timer() : name("noname"), running(false), accumulatedTime_(0) 
{   
  logFilename = appendString("log/",name,".log");
}

Timer::Timer(std::string name_) : name(name_), running(false), accumulatedTime_(0) 
{
}

Timer::Timer(const Timer& t)
{
  running = t.running;
  accumulatedTime_ = t.accumulatedTime_;
  startTime = t.startTime;
  name = t.name;
}

Timer::~Timer() 
{
}

void Timer::operator=(const Timer& t)
{
  running = t.running;
  accumulatedTime_ = t.accumulatedTime_;
  startTime = t.startTime;
  name = t.name;
}

void Timer::start()
{

  // Return immediately if the timer is already running
  if (running)
    return;
  
  // Change timer status to running
  running = 1;
  
  // Determine the current time;
  startTime = clock();

  FILELOGL(1,logFilename,
	   appendString("void Timer::start()","  [",name,"] at T=",clock()/CLOCKS_PER_SEC));
}

void Timer::stop()
{
  // Recalculate the total accumulated time the timer has run
  if (running){
    accumulatedTime_ += elapsedTime();
    running = 0;
  }
  else{
    std::cerr << "Timer [" << name << "] not running!\n";
    abort();
  }
} 

void Timer::restart()
{
  // angeh�ufte Zeit = 0
  accumulatedTime_ = 0;

  // Change timer status to running
  running = 1;

  // Determine the start time;
  startTime = clock();
} 

double Timer::elapsedTime()
{
  clock_t currentTime = clock();
  return double(currentTime - startTime) / CLOCKS_PER_SEC;
}

double Timer::check()
{
  if (running)
    return accumulatedTime_ + elapsedTime();

  return accumulatedTime_;
}

void Timer::logAccumulatedTime() const
{
  FILELOGL(0,logFilename,
	   appendString("AccumulatedTime","  [",name,"] = ", accumulatedTime_));
}

void Timer::logAccumulatedTime(const std::string logFilename_) const
{
  FILELOGL(0,logFilename_,
	   appendString("AccumulatedTime","  [",name,"] = ", accumulatedTime_));
}

} // namespace BK
