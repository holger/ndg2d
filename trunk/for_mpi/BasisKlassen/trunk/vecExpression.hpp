#ifndef vecExpression_hpp
#define vecExpression_hpp

namespace BK {

template <typename E>
class VecExpression 
{
public:
  
  auto operator[](size_t i) const { return static_cast<E const&>(*this)[i];     }
  size_t size()               const { return static_cast<E const&>(*this).size(); }
};

template <typename E1, typename E2>
class VecSum : public VecExpression<VecSum<E1, E2> > {
  E1 const& _u;
  E2 const& _v;

public:
  VecSum(E1 const& u, E2 const& v) : _u(u), _v(v) {
    assert(u.size() == v.size());
  }

  auto operator[](size_t i) const -> decltype(_u[i]) { return _u[i] + _v[i]; } 
  
  size_t size()               const { return _v.size(); }
};

template <typename E1, typename E2>
VecSum<VecExpression<E1>,VecExpression<E2>> operator+(VecExpression<E1> const& u, VecExpression<E2> const& v) {
  return VecSum<VecExpression<E1>, VecExpression<E2>>(u, v);
}

template <typename E1, typename E2>
class VecDiff : public VecExpression<VecDiff<E1, E2> > {
  E1 const& _u;
  E2 const& _v;

public:
  VecDiff(E1 const& u, E2 const& v) : _u(u), _v(v) {
    assert(u.size() == v.size());
  }

  auto operator[](size_t i) const -> decltype(_u[i]) { return _u[i] - _v[i]; }


  size_t size()               const { return _v.size(); }
};

template <typename E1, typename E2>
VecDiff<VecExpression<E1>,VecExpression<E2>> operator-(VecExpression<E1> const& u, VecExpression<E2> const& v) {
  return VecDiff<VecExpression<E1>, VecExpression<E2>>(u, v);
}

// // ---------------------------------------------------------------------

template <typename E1>
class MinusVec : public VecExpression<MinusVec<E1> > {
  E1 const& _v;

public:
  MinusVec(E1 const& v) : _v(v) {}

  auto operator[](size_t i) const -> decltype(_v[i]) { return -_v[i]; }

  size_t size()               const { return _v.size(); }
};

template <typename E>
MinusVec<VecExpression<E>> operator-(VecExpression<E> const& v) {
  return MinusVec<VecExpression<E>>(v);
}

// // ---------------------------------------------------------------------

template <typename E2>
class ScalarMultVec : public VecExpression<ScalarMultVec<E2> > {
  double _u;
  E2 const& _v;

public:
  ScalarMultVec(double const& u, E2 const& v) : _u(u), _v(v) {}

  auto operator[](size_t i) const -> decltype(_v[i]) { return _u*_v[i]; }

  size_t size()               const { return _v.size(); }
};

template <typename E>
ScalarMultVec<VecExpression<E>> operator*(double u, VecExpression<E> const& v) {
  return ScalarMultVec<VecExpression<E>>(u, v);
}

template <typename E1>
class VecMultScalar : public VecExpression<VecMultScalar<E1> > {
  E1 const& _u;
  double _v;

public:
  VecMultScalar(E1 const& u, double const& v) : _u(u), _v(v) {}

  auto operator[](size_t i) const -> decltype(_u[i]) { return _u[i]*_v; }

  size_t size()               const { return _u.size(); }
};

template <typename E>
VecMultScalar<VecExpression<E>> operator*(VecExpression<E> const& u, double v) {
  return VecMultScalar<VecExpression<E>>(u, v);
}

// --------------------------------------------------------------------

template <typename E1>
class VecDivScalar : public VecExpression<VecDivScalar<E1> > {
  E1 const& _u;
  double _v;

public:
  VecDivScalar(E1 const& u, double const& v) : _u(u), _v(v) {}

  auto operator[](size_t i) const -> decltype(_u[i]) { return _u[i]/_v; }

  size_t size()               const { return _u.size(); }
};

template <typename E1>
VecDivScalar<VecExpression<E1>> operator/(VecExpression<E1> const& u, double v) {
  return VecDivScalar<VecExpression<E1>>(u, v);
}

template<class E>
std::ostream& operator<<(std::ostream &of, const VecExpression<E> &v)   
{       
  of << "(";
  for (size_t i = 0; i < v.size()-1; i++)
    of << v[i] << ",";
  of << v[v.size()-1] << ")";

  return of;
}

} // namespace BK

#endif
