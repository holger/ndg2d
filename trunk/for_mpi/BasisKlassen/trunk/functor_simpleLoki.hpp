#ifndef functor_simpleLoki_hpp
#define functor_simpleLoki_hpp

#include <typeinfo>
#include <memory>
#include <cassert>

#include "Typelist.hpp"
#include "EmptyType.hpp"
#include "TypeTraits.hpp"

namespace LOKI
{
  
  template <typename R, typename Par>
  class FunctorImpl
  {
  public:
    typedef R ResultType;
    virtual R operator()(Par) = 0;
  };

  template <class ParentFunctor, typename Fun>
  class FunctorHandler
    : public ParentFunctor::Impl
  {

  public:
    typedef typename ParentFunctor::Impl::ResultType ResultType;
    typedef typename ParentFunctor::Parm Parm;
    
    FunctorHandler(const Fun& fun) : f_(fun) {}
    
    ResultType operator()(typename ParentFunctor::Parm p1)
    { return f_(p1); }
  private:
    Fun f_;
  };
  
  template <class ParentFunctor, typename PointerToObj,
	    typename PointerToMemFn>
  class MemFunHandler : public ParentFunctor::Impl
  {

  public:
    typedef typename ParentFunctor::Impl::ResultType ResultType;

    MemFunHandler(const PointerToObj& pObj, PointerToMemFn pMemFn) 
      : pObj_(pObj), pMemFn_(pMemFn)
    {}

    ResultType operator()(typename ParentFunctor::Parm p1)
    { return ((*pObj_).*pMemFn_)(p1); }
    
  private:
    PointerToObj pObj_;
    PointerToMemFn pMemFn_;
  };

  template <typename R, class Par>
  class Functor
  {
  public:
    // Handy type definitions for the body type
    typedef FunctorImpl<R, Par> Impl;
    typedef R ResultType;
    typedef Par Parm;
    
    template <typename Fun>
    Functor(Fun fun)
      : impl_(new FunctorHandler<Functor, Fun>(fun))
    {std::cout << "Functor(Fun fun)\n";}

    template <class PtrObj, typename MemFn>
    Functor(const PtrObj& p, MemFn memFn)
      : impl_(new MemFunHandler<Functor, PtrObj, MemFn>(p, memFn))
    {std::cout << "Functor(const PtrObj& p, MemFn memFn)\n";}

    ResultType operator()(Par p1)
    { return (*impl_)(p1); }
    
  private:
    Impl* impl_;
  };

} // namespace BK

#endif
