#ifndef bk_fftBase_hpp
#define bk_fftBase_hpp

//smoothing (0=none, 1=cesaro, 2=lanczos, 3=raised cosine, 4=sharpened raised cosine, 5=tadmor exponential)
#ifndef SIGMA
#define SIGMA 4
#endif

#include "lut.hpp"
#include "houExp.hpp"
#include "fftwBlock.hpp"
#include "mpiBase.hpp"
#include "dataType.hpp"
#include "fftw_complex.hpp"

namespace BK {

class FftBase
{
public:
  FftBase();
  ~FftBase();

  void init(const MpiBase& mpiBase, double lx, double ly, double lz);
  
  // Gibt commRank des reellen Array zurück, das x enthaelt 
  unsigned int getCommRankOfGlobalXR(unsigned int x) const;
  // Gibt commRank des komplexen Array zurück, das x enthaelt 
  unsigned int getCommRankOfGlobalYC(int y) const;

  // Gibt commRank des reellen Array zurück, das x,y enthaelt 
  unsigned int getCommRankOfGlobalXYR(unsigned int x, unsigned y) const;
  // Gibt commRank des complexen Array zurück, das x,y enthaelt 
  unsigned int getCommRankOfGlobalXYC(unsigned int x, unsigned y) const;
  
  unsigned int getCommRankOfMathGlobalXR(int x) const;
  unsigned int getCommRankOfMathGlobalYC(int y) const;
  
  // Berechnet aus lokalem FFTW-index (zu commRank) den globalen index
  int localFftw2FftwGlobalXC(unsigned int x, unsigned int commRank = commRank_) const;
  int localFftw2FftwGlobalYC(unsigned int y, unsigned int commRank = commRank_) const;
  int localFftw2FftwGlobalZC(unsigned int z, unsigned int commRank = commRank_) const;
  //  int localFftw2FftwGlobalXR(int x, unsigned int commRank = commRank_) const;

  // Berechnet aus lokalem FFTW-Index (zu commRank) den globalen mathematischen Index
  int localFftw2MathGlobalXC(unsigned int x, unsigned int commRank = commRank_) const;
  int localFftw2MathGlobalYC(unsigned int y, unsigned int commRank = commRank_) const;
  int localFftw2MathGlobalZC(unsigned int z, unsigned int commRank = commRank_) const;
  int localFftw2MathGlobalXR(unsigned int x, unsigned int commRank = commRank_) const;
  int localFftw2MathGlobalYR(unsigned int y, unsigned int commRank = commRank_) const;
  int localFftw2MathGlobalZR(unsigned int z, unsigned int commRank = commRank_) const;

  // Berechnet aus globalen FFTW-Index den globalen mathematischen Index
  int globalFftw2MathGlobalXC(unsigned int x) const;
  int globalFftw2MathGlobalYC(unsigned int y) const;
  int globalFftw2MathGlobalZC(unsigned int z) const;

  // Berechnet aus globalem mathematischen Index den global FFTW-Index
  int globalMath2FftwGlobalXC(int x) const;
  int globalMath2FftwGlobalYC(int y) const;
  int globalMath2FftwGlobalZC(int z) const;

  // Berechnet aus globalem mathematischen Index (zu commRank) den lobalen FFTW-Index
  unsigned int globalMath2FftwLocalXC(int x, unsigned int commRank = commRank_) const;
  unsigned int globalMath2FftwLocalYC(int y, unsigned int commRank = commRank_) const;
  unsigned int globalMath2FftwLocalZC(int z, unsigned int commRank = commRank_) const;
  template<class T>
  T globalMath2FftwLocalXR(T x, unsigned int commRank = commRank_) const;
  template<class T>
  T globalMath2FftwLocalYR(T y, unsigned int commRank = commRank_) const;
  template<class T>
  T globalMath2FftwLocalZR(T z, unsigned int commRank = commRank_) const;
  
  template<class T> T globalMath2FftwLocalR(int dir, T x, unsigned int commRank = commRank_) const
  {
    assert(initFlag_ == true);
    
    T temp;
    
    switch(dir) {
    case 0: temp = globalMath2FftwLocalXR(x,commRank); break;
    case 1: temp = globalMath2FftwLocalYR(x,commRank); break;
    case 2: temp = globalMath2FftwLocalZR(x,commRank); break;
    default: std::cerr << "ERROR in globalMath2FftwLocalR: unknown direction\n"; exit(1);
    }
     
    return temp;
  }
  

//   double globalMath2FftwLocalXR(double x, unsigned int commRank = commRank_) const;
//   double globalMath2FftwLocalYR(double y, unsigned int commRank = commRank_) const;
//   double globalMath2FftwLocalZR(double z, unsigned int commRank = commRank_) const;
  // anti-gibbs filter
//   double sigma(int x, int y, int z, bool flag=false) const {
// #if SIGMA==0
// 	return 1.; // no filter
// #elif SIGMA==1
// 	return 1.-2.*sqrt(sqr(kx(x)/(globalNxR_+2.))+sqr(ky(y)/(globalNyR_+2.))+sqr(kz(z)/(globalNzR_+2.))); // cesaro
// #elif SIGMA==2
// 	return (kx(x)==0.&&ky(y)==0.&&kz(z)==0.)?1.:sin(2.*M_PI*sqrt(sqr(kx(x)/globalNxR_)+sqr(ky(y)/globalNyR_)+sqr(kz(z)/globalNzR_)))/(2.*M_PI*sqrt(sqr(kx(x)/globalNxR_)+sqr(ky(y)/globalNyR_)+sqr(kz(z)/globalNzR_))); // lanczos
// #elif SIGMA==3
// 	return .5*(1.+cos(2.*M_PI*sqrt(sqr(kx(x)/globalNxR_)+sqr(ky(y)/globalNyR_)+sqr(kz(z)/globalNzR_)))); // raised cosine
// #elif SIGMA==4
// 	return flag?.5*(1.+cos(2.*M_PI*sqrt(sqr(kx(x)/globalNxR_)+sqr(ky(y)/globalNyR_)+sqr(kz(z)/globalNzR_)))):sqr(sqr(sigma(x,y,z,true)))*(35.-84.*sigma(x,y,z,true)+70.*sqr(sigma(x,y,z,true))-20.*sigma(x,y,z,true)*sqr(sigma(x,y,z,true))); // sharpened raised cosine, best for heaviside-like jumps
// #endif
// }
  
  // Berechnet aus globalem (FFTW) Index (zu commRank) den lobalen FFTW-Index
  unsigned int globalFftw2FftwLocalXC(int x, unsigned int commRank = commRank_) const;
  unsigned int globalFftw2FftwLocalYC(int y, unsigned int commRank = commRank_) const;
  unsigned int globalFftw2FftwLocalZC(int z, unsigned int commRank = commRank_) const;
  unsigned int globalFftw2FftwLocalXR(int x, unsigned int commRank = commRank_) const;
  unsigned int globalFftw2FftwLocalYR(int y, unsigned int commRank = commRank_) const;
  unsigned int globalFftw2FftwLocalZR(int z, unsigned int commRank = commRank_) const;

  unsigned int get_localStartC(unsigned int i) const { assert(i < 3); return localStartC_[i];}
  unsigned int get_localStartR(unsigned int i) const { assert(i < 3); return localStartR_[i];}

  inline bool inRangeC(int x, int y, int z);
  inline bool inRangeR(int x, int y, int z);
  inline bool inRangeGlobalR(int x, int y, int z);
  
  // Flag, ob init(...) schon aufgerufen wurde
  PROCONPAR(bool,initFlag);

  CONSTPARA(double,lx);
  CONSTPARA(double,ly);
  CONSTPARA(double,lz);

  CONSTPARA(double,cx);
  CONSTPARA(double,cy);
  CONSTPARA(double,cz);

  PROCONPAR(unsigned int,nxR);
  PROCONPAR(unsigned int,nyR);
  PROCONPAR(unsigned int,nzR);

  inline unsigned int nR(unsigned int direction)
  {
    unsigned int temp = 0;
    
    switch(direction) {
    case 0: return temp = nxR_; break;
    case 1: return temp = nyR_; break;
    case 2: return temp = nzR_; break;
    default: std::cerr << "Error in fftBase::nR: wrong direction\n";
    }

    return temp;
  }

  PROCONPAR(unsigned int,nxC);
  PROCONPAR(unsigned int,nyC);
  PROCONPAR(unsigned int,nzC);

  PROCONPAR(unsigned int,globalNxR);
  PROCONPAR(unsigned int,globalNyR);
  PROCONPAR(unsigned int,globalNzR);

  inline unsigned int globalNR(unsigned int direction)
  {
    unsigned int temp = 0;
    
    switch(direction) {
    case 0: temp = globalNxR_; break;
    case 1: temp = globalNyR_; break;
    case 2: temp = globalNzR_; break;
    default: std::cerr << "Error in fftBase::globalNR: wrong direction\n";
    }

    return temp;
  }

  PROCONPAR(unsigned int,globalNxC);
  PROCONPAR(unsigned int,globalNyC);
  PROCONPAR(unsigned int,globalNzC);

  // Hin und Ruecktrafo mit FFTW unterscheiden sich von Original um Faktor scale (FFTW normiert nicht!)
  PROCONPAR(double,scale);
  
  PROCONPAR(int,totalLocalSize);

  /// The ranks of the left, right, top, and bottom neighbour processes
  PROTEPARA(unsigned int,leftCoord);
  PROTEPARA(unsigned int,rightCoord);
  PROTEPARA(unsigned int,topCoord);
  PROTEPARA(unsigned int,bottomCoord);

  // The coordinates of this process
  PROTEPARA(unsigned int,myCoordX);
  PROTEPARA(unsigned int,myCoordY);

  // // local wave numbers
  // double localKx(int x) const {return cx_*x;}
  // double localKy(int y) const {return cy_*y;}
  // double localKz(int z) const {return cz_*z;}
  
  // wave numbers
  double kx(int x) const {return cx_*localFftw2MathGlobalXC(x);}
  double ky(int y) const {return cy_*localFftw2MathGlobalYC(y);}
  double kz(int z) const {return cz_*localFftw2MathGlobalZC(z);}

  // Quadrat der Wellenzahlen
  double k2(int x,int y,int z) const {return sqr(kx(x))+sqr(ky(y))+sqr(kz(z));}

  // ganzzahliger Radius von (x,y,z)
  int roundedRadius(int x, int y, int z);
  // ganzzahliger Radius von (x,y,z)
  int integerRadius(int x, int y, int z);

  /// Lookup table for the exponential function
  LUT hou_exp;
  LutFunctor lutExp;

  int processorGridDims_[2];
  //  int** processorGrid;
  
  /// dealiasing mode: 1: Hou smoothing, 2: spherical truncation, 3: full 2/3
  template<class MatrixType>
  void dealias(MatrixType& conv, int mode);

  const FftwBlock& get_fftwBlockArray(int i) const {assert(fftwBlockArray_ != 0); return fftwBlockArray_[i];}
  FftwBlock& set_fftwBlockArray(int i) {assert(fftwBlockArray_ != 0); return fftwBlockArray_[i];}
  FftwBlock*& set_fftwBlockArray() {return fftwBlockArray_;}
  const FftwBlock* get_fftwBlockArray() const {return fftwBlockArray_;}

  const FftwBlock& get_fftwBlock() const {return fftwBlock_;}
  
  float* kxField;
  float* kyField;
  float* kzField;

  //  float* sigmaField;

  void createKFields();

protected:

  static unsigned int commRank_;

  // Array of fftw-blocks (index = commRank)
  FftwBlock* fftwBlockArray_;

  // lokaler FftwBlock
  FftwBlock fftwBlock_;

  FftBase(const FftBase&);
  FftBase& operator=(const FftBase&);

  unsigned int localStartC_[3];
  unsigned int localStartR_[3];

  PROCONPAR(unsigned int,matrixNumber);

  const MpiBase* mpiBase_;
};

inline int FftBase::roundedRadius(int x, int y, int z)
{
  return int(BK::round(sqrt(sqr(cx_*localFftw2MathGlobalXC(x))+
			    sqr(cy_*localFftw2MathGlobalYC(y))+
			    sqr(cz_*localFftw2MathGlobalZC(z)))));
}

inline int FftBase::integerRadius(int x, int y, int z)
{
  return int(sqrt(sqr(cx_*localFftw2MathGlobalXC(x))+
		  sqr(cy_*localFftw2MathGlobalYC(y))+
		  sqr(cz_*localFftw2MathGlobalZC(z))));
}

inline bool FftBase::inRangeC(int x,int y, int z)
{
  ERRORLOGL(3,"FftBase::inRangeC");

  bool xInRangeC = (x>=0) && (x<int(nxC_));
  bool yInRangeC = (y>=0) && (y<int(nyC_));
  bool zInRangeC = (z>=0) && (z<int(nzC_));

  ERRORLOGL(4,"FftBase::inRangeC-end");

  return xInRangeC && yInRangeC && zInRangeC;
}

inline bool FftBase::inRangeR(int x, int y, int z)
{
  ERRORLOGL(3,"FftBase::inRangeR");

  bool xInRangeR = (x>=0) && (x<int(nxR_));
  bool yInRangeR = (y>=0) && (y<int(nyR_));
  bool zInRangeR = (z>=0) && (z<int(nzR_));

  ERRORLOGL(4,"FftBase::inRangeR-end");

  return xInRangeR && yInRangeR && zInRangeR;
}

inline bool FftBase::inRangeGlobalR(int x, int y, int z)
{
  ERRORLOGL(3,"FftBase::inRangeR");
  
  int localX = globalMath2FftwLocalXR(x);
  int localY = globalMath2FftwLocalYR(y);
  int localZ = globalMath2FftwLocalZR(z);

  bool xInRangeR = localX >= 0 && (unsigned int)(localX) < nxR_;
  bool yInRangeR = localY >= 0 && (unsigned int)(localY) < nyR_;
  bool zInRangeR = localZ >= 0 && (unsigned int)(localZ) < nzR_;

  ERRORLOGL(4,"FftBase::inRangeR-end");

  return xInRangeR && yInRangeR && zInRangeR;
}

inline unsigned int FftBase::getCommRankOfGlobalXR(unsigned int x) const
{
  assert(initFlag_ == true);

  int requestedCommRank = -1;
  for(int i=0;i<mpiBase_->get_commSize();i++){
    if(x >= fftwBlockArray_[i].get_localStartR(0) && x < fftwBlockArray_[i].get_localStartR(0)+fftwBlockArray_[i].get_nxR())
      requestedCommRank = fftwBlockArray_[i].get_commRank();
  }
  if(requestedCommRank == -1) {
    std::cerr << "ERROR in FftBase.getCommRankOfGlobalXR: x = " << x << " belongs to no process\n";
    exit(1);
  }
  return requestedCommRank;
}

inline unsigned int FftBase::getCommRankOfGlobalXYR(unsigned int x, unsigned int y) const
{
  assert(initFlag_ == true);

  int requestedCommRank = -1;
  for(int i=0;i<mpiBase_->get_commSize();i++){
    if(x >= fftwBlockArray_[i].get_localStartR(0) && x < fftwBlockArray_[i].get_localStartR(0)+fftwBlockArray_[i].get_nxR()
       && y >= fftwBlockArray_[i].get_localStartR(1) && y < fftwBlockArray_[i].get_localStartR(1)+fftwBlockArray_[i].get_nyR())
      requestedCommRank = fftwBlockArray_[i].get_commRank();
  }
  if(requestedCommRank == -1) {
    std::cerr << "ERROR in FftBase.getCommRankOfGlobalXYR: x = " << x << ", y = " << y << " belong to no process\n";
    exit(1);
  }
  return requestedCommRank;
}

inline unsigned int FftBase::getCommRankOfGlobalXYC(unsigned int x, unsigned int y) const
{
  assert(initFlag_ == true);

  int requestedCommRank = -1;
  for(int i=0;i<mpiBase_->get_commSize();i++){
    if(x >= fftwBlockArray_[i].get_localStartC(0) && x < fftwBlockArray_[i].get_localStartC(0)+fftwBlockArray_[i].get_nxC()
       && y >= fftwBlockArray_[i].get_localStartC(1) && y < fftwBlockArray_[i].get_localStartC(1)+fftwBlockArray_[i].get_nyC())
      requestedCommRank = fftwBlockArray_[i].get_commRank();
  }
  if(requestedCommRank == -1) {
    std::cerr << "ERROR in FftBase.getCommRankOfGlobalXYC: x = " << x << ", y = " << y << " belong to no process\n";
    exit(1);
  }
  return requestedCommRank;
}
  
inline unsigned int FftBase::getCommRankOfGlobalYC(int y) const
{
  assert(initFlag_ == true);
 
  int requestedCommRank = -1;
  for(int i=0;i<mpiBase_->get_commSize();i++){
    if(y >= int(fftwBlockArray_[i].get_localYStartAfterTranspose()) && y < int(fftwBlockArray_[i].get_localYStartAfterTranspose()+fftwBlockArray_[i].get_localNyAfterTranspose()))
      requestedCommRank = fftwBlockArray_[i].get_commRank();
  }
  if(requestedCommRank == -1) {
    std::cerr << "ERROR in FftBase.getCommRankOfGlobalYC: y = " << y << " existiert in keinem Prozess\n";
    
    for(int i=0;i<mpiBase_->get_commSize();i++){
      std::cerr << mpiBase_->get_commRank() << "  " << fftwBlockArray_[i].get_localYStartAfterTranspose() << "  " 
		<< fftwBlockArray_[i].get_localNyAfterTranspose() << std::endl;
    }
    
    exit(1);
  }
  return requestedCommRank;
}

inline unsigned int FftBase::getCommRankOfMathGlobalYC(int y) const
{
  assert(initFlag_ == true);
 
  int requestedCommRank = -1;
  for(int i=0;i<mpiBase_->get_commSize();i++){
    if(globalMath2FftwGlobalYC(y) >= int(fftwBlockArray_[i].get_localYStartAfterTranspose()) && globalMath2FftwGlobalYC(y) < 
       int(fftwBlockArray_[i].get_localYStartAfterTranspose()+fftwBlockArray_[i].get_localNyAfterTranspose()))
      requestedCommRank = fftwBlockArray_[i].get_commRank();
  }
  if(requestedCommRank == -1) {
    std::cerr << "ERROR in FftBase.getCommRankOfMathGlobalYC: y = " << y << " existiert in keinem Prozess\n";
    exit(1);
  }
  return requestedCommRank;
}

inline int FftBase::localFftw2FftwGlobalXC(unsigned int x, unsigned int commRank) const
{
  assert(initFlag_ == true);

  return x + fftwBlockArray_[commRank].get_localStartC(0);
}

inline int FftBase::localFftw2FftwGlobalYC(unsigned int y, unsigned int commRank) const
{
  assert(initFlag_ == true);

  return y + fftwBlockArray_[commRank].get_localStartC(1);
}

inline int FftBase::localFftw2FftwGlobalZC(unsigned int z, unsigned int commRank) const
{
  assert(initFlag_ == true);

  return z + fftwBlockArray_[commRank].get_localStartC(2);
}

inline int FftBase::localFftw2MathGlobalXC(unsigned int x, unsigned int commRank) const
{
  assert(initFlag_ == true);

  if(fftwBlockArray_[commRank].get_localStartC(0) + x >= globalNxR_/2) {
    return fftwBlockArray_[commRank].get_localStartC(0) + x - globalNxR_;}
  else {return fftwBlockArray_[commRank].get_localStartC(0) + x;}
}

// inline int FftBase::localFftw2MathGlobalYC(int y, unsigned int commRank) const
// {
//   assert(initFlag_ == true);

//   if(fftwBlockArray_[commRank].get_localStartC(1) + y >= ny_/2) {return fftwBlockArray_[commRank].get_localStartC(1) + y - ny_;}
//   else {return fftwBlockArray_[commRank].get_localStartC(1) + y;}
// }
inline int FftBase::localFftw2MathGlobalYC(unsigned int y, unsigned int commRank) const
{
  assert(initFlag_ == true);

  if(fftwBlockArray_[commRank].get_localStartC(1) + y >= globalNyR_/2) {
    return fftwBlockArray_[commRank].get_localStartC(1) + y - globalNyR_;}
  else {return fftwBlockArray_[commRank].get_localStartC(1) + y;}
}

inline int FftBase::localFftw2MathGlobalZC(unsigned int z, unsigned int commRank) const
{
  assert(initFlag_ == true);

  return fftwBlockArray_[commRank].get_localStartC(2) + z;

//   if(fftwBlockArray_[commRank].get_localStartC(2) + z >= globalNzR_/2) {
//     return fftwBlockArray_[commRank].get_localStartC(2) + z - globalNzR_;}
//   else {return fftwBlockArray_[commRank].get_localStartC(2) + z;}
}

// inline int FftBase::localFftw2MathGlobalZC(int z, unsigned int commRank) const
// {
//   assert(initFlag_ == true);

//   if (z >= nz_/2) {return z - nz_;}
//   else {return z;}
// }

// inline int FftBase::localFftw2FftwGlobalXR(int x, unsigned int commRank) const
// {
//   assert(initFlag_ == true);

//   return x + fftwBlockArray_[commRank].get_localStartR(0);
// }


inline int FftBase::localFftw2MathGlobalXR(unsigned int x, unsigned int commRank) const
{
  assert(initFlag_ == true);
  
  return fftwBlockArray_[commRank].get_localStartR(0) + x;
}

inline int FftBase::localFftw2MathGlobalYR(unsigned int y, unsigned int commRank) const
{
  assert(initFlag_ == true);

  return fftwBlockArray_[commRank].get_localStartR(1) + y;
}

inline int FftBase::localFftw2MathGlobalZR(unsigned int z, unsigned int commRank) const
{
  assert(initFlag_ == true);

  return fftwBlockArray_[commRank].get_localStartR(2) + z;
}

inline int FftBase::globalFftw2MathGlobalXC(unsigned int x) const
{
  assert(initFlag_ == true);

  if(x >= globalNxR_/2) {return x - globalNxR_;}
  else {return x;}
}

inline int FftBase::globalFftw2MathGlobalYC(unsigned int y) const
{
  assert(initFlag_ == true);

  if (y >= nyR_/2) {return y - nyR_;}
  else {return y;}
}

inline int FftBase::globalFftw2MathGlobalZC(unsigned int z) const
{
  assert(initFlag_ == true);

  if (z >= nzR_/2) {return z - nzR_;}
  else {return z;}
}

inline int FftBase::globalMath2FftwGlobalXC(int x) const
{
  assert(initFlag_ == true);

  return ((globalNxR_+x) % globalNxR_);
}

inline int FftBase::globalMath2FftwGlobalYC(int y) const
{
  assert(initFlag_ == true);

  return ((nyR_+y) % nyR_);
}

inline int FftBase::globalMath2FftwGlobalZC(int z) const
{
  assert(initFlag_ == true);

  return ((nzR_+z) % nzR_);
}

inline unsigned int FftBase::globalMath2FftwLocalXC(int x, unsigned int commRank) const
{
  assert(initFlag_ == true);
  
  return ((globalNxR_+x) % globalNxR_) - fftwBlockArray_[commRank].get_localStartC(0);
}

inline unsigned int FftBase::globalMath2FftwLocalYC(int y, unsigned int commRank) const
{
  assert(initFlag_ == true);

  return ((globalNyR_+y) % globalNyR_) - fftwBlockArray_[commRank].get_localStartC(1);
}

inline unsigned int FftBase::globalMath2FftwLocalZC(int z, unsigned int commRank) const
{
  assert(initFlag_ == true);

  return ((globalNzR_+z) % globalNzR_) - fftwBlockArray_[commRank].get_localStartC(2);
}

template<class T>
inline T FftBase::globalMath2FftwLocalXR(T x, unsigned int commRank) const
{
  assert(initFlag_ == true);

  return x - fftwBlockArray_[commRank].get_localStartR(0);
}

template<class T>
inline T FftBase::globalMath2FftwLocalYR(T y, unsigned int commRank) const
{
  assert(initFlag_ == true);

  return y - fftwBlockArray_[commRank].get_localStartR(1);
}

template<class T>
inline T FftBase::globalMath2FftwLocalZR(T z, unsigned int commRank) const
{
  assert(initFlag_ == true);

  return z;
}

// inline double FftBase::globalMath2FftwLocalXR(double x, unsigned int commRank) const
// {
//   assert(initFlag_ == true);

//   return x - fftwBlockArray_[commRank].get_localStartR(0);
// }

// inline double FftBase::globalMath2FftwLocalYR(double y, unsigned int commRank) const
// {
//   assert(initFlag_ == true);

//   return y - fftwBlockArray_[commRank].get_localStartR(1);
// }

// inline double FftBase::globalMath2FftwLocalZR(double z, unsigned int commRank) const
// {
//   assert(initFlag_ == true);

//   return z;
// }

inline unsigned int FftBase::globalFftw2FftwLocalXC(int x, unsigned int commRank) const
{
  assert(initFlag_ == true);
  
  return x - fftwBlockArray_[commRank].get_localStartC(0);
}

inline unsigned int FftBase::globalFftw2FftwLocalYC(int y, unsigned int commRank) const
{
  assert(initFlag_ == true);
  
  return y - fftwBlockArray_[commRank].get_localStartC(1);
}

inline unsigned int FftBase::globalFftw2FftwLocalZC(int z, unsigned int commRank) const
{
  assert(initFlag_ == true);
  
  return z - fftwBlockArray_[commRank].get_localStartC(2);
}

inline unsigned int FftBase::globalFftw2FftwLocalXR(int x, unsigned int commRank) const
{
  assert(initFlag_ == true);
  
  return x - fftwBlockArray_[commRank].get_localStartR(0);
}

inline unsigned int FftBase::globalFftw2FftwLocalYR(int y, unsigned int commRank) const
{
  assert(initFlag_ == true);
  
  return y - fftwBlockArray_[commRank].get_localStartR(1);
}

inline unsigned int FftBase::globalFftw2FftwLocalZR(int z, unsigned int commRank) const
{
  assert(initFlag_ == true);
  
  return z - fftwBlockArray_[commRank].get_localStartR(2);
}

template<class MatrixType>
void FftBase::dealias(MatrixType& conv, int mode)
{
  ERRORLOGL(3,"FftBase::dealias");
  
  assert(conv.get_dataType() == ComplexData);

  switch(mode) {

  case 0: {}
    break;
    
  case 3: {
    ERRORLOGL(4,"full 2/3 dealiasing");
    double limitX = globalNxR_/3.;
    double limitY = globalNyR_/3.;
    double limitZ = globalNzR_/3.;

    for(int x=conv.get_loC(0);x<=conv.get_hiC(0);x++)
      for(int y=conv.get_loC(1);y<=conv.get_hiC(1);y++)
	for(int z=conv.get_loC(2);z<=conv.get_hiC(2);z++)
	  if(sqr(localFftw2MathGlobalXC(x)/limitX) +
	     sqr(localFftw2MathGlobalYC(y)/limitY) +
	     sqr(localFftw2MathGlobalZC(z)/limitZ) >= 1)
	    conv.c(x,y,z) = Complex<typename MatrixType::value_type>(0,0);
    
/* 	  if(roundedRadius(x,y,z) >= int(globalNxR_/mode)) */
/* 	    conv.c(x,y,z) = Complex<typename MatrixType::value_type>(0,0); */
  }
    break;
    
  case 2: {
    ERRORLOGL(4,"spherical truncation");
    double limitX = globalNxR_/2.;
    double limitY = globalNyR_/2.;
    double limitZ = globalNzR_/2.;

    for(int x=conv.get_loC(0);x<=conv.get_hiC(0);x++)
      for(int y=conv.get_loC(1);y<=conv.get_hiC(1);y++)
	for(int z=conv.get_loC(2);z<=conv.get_hiC(2);z++)
	  if(sqr(localFftw2MathGlobalXC(x)/limitX) +
	     sqr(localFftw2MathGlobalYC(y)/limitY) +
	     sqr(localFftw2MathGlobalZC(z)/limitZ) >= 1)
	    conv.c(x,y,z) = Complex<typename MatrixType::value_type>(0,0);
	  
/* 	  if(roundedRadius(x,y,z) >= int(globalNxR_/mode)) */
/* 	    conv.c(x,y,z) = Complex<typename MatrixType::value_type>(0,0); */
  
  }
    break;
    
  case 1: {
    // Hou smoothing
    double limitX = globalNxR_/2.;
    double limitY = globalNyR_/2.;
    double limitZ = globalNzR_/2.;

    //    const double hou_N =  globalNxR_/2;
    for(int x=conv.get_loC(0);x<=conv.get_hiC(0);x++)
      for(int y=conv.get_loC(1);y<=conv.get_hiC(1);y++)
	for(int z=conv.get_loC(2);z<=conv.get_hiC(2);z++){
	  //	  conv.c(x,y,z) *= std::exp(-hou_alpha*intPow(sqrt(k2(x,y,z))/hou_N,hou_M));
	  // conv.c(x,y,z) *= lutExp(sqrt(k2(x,y,z)/BK::sqr(globalNxC_)));
	  // ERRORLOGL(0,BK::appendString(x,"  ",y,"  ",z,"  ",sqrt(k2(x,y,z)),"  ",globalNxC_,"  ",sqrt(k2(x,y,z)/BK::sqr(globalNxC_)),"  ",
	  // 			       lutExp(sqrt(k2(x,y,z)/BK::sqr(globalNxC_)))));
	  conv.c(x,y,z) *= lutExp(sqrt(sqr(localFftw2MathGlobalXC(x)/limitX)+
				       sqr(localFftw2MathGlobalYC(y)/limitY)+
				       sqr(localFftw2MathGlobalZC(z)/limitZ)));
	}

  }
    break;
  default:
    ERRORLOGL(0,"ERROR in dealias(MatrixType& conv, int mode):");
    ERRORLOGL(0,appendString("illegal value: mode = ",mode));
    exit(0);
    break;
  }    

  ERRORLOGL(4,"FftBase::dealias-end");
}


} // namespace BK;

#endif
