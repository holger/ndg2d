#ifndef parseFromFile_hpp
#define parseFromFile_hpp

#include <map>
#include <list>
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <cstdlib>

#include "logger.hpp"
#include "parameter.hpp"

namespace BK {

// returns parameter from map
template<class TYPE>
TYPE getParameter(const std::map<std::string, std::string>& map, std::string parameter, bool parsingAssertion = true);  

// checks if value has TYPE
template<class TYPE>
void assertType(std::string value);

// checks existence of parameter in map
bool parameterExists(const std::map<std::string, std::string>& map, std::string parameter);

//  fetches list for key in parameterMap
template<class CONT_TYPE>
void getList(std::map<std::string, std::string> const& parameterMap, std::string key, CONT_TYPE& list);

// Simple parser for konfiguration files
// Comments begin with #+whitespace
class ReadInitFile
{

 public:
  typedef std::map<std::string, std::string> ParameterMap;
  typedef std::map<std::string, ParameterMap > MapMap;

  ReadInitFile();
  
  ~ReadInitFile();

  // Constructor reading parameter from file <filename>
  ReadInitFile(std::string filename, bool assertion = true);

  // reads parameter from filename
  bool readFile(std::string filename, bool assertion = true);
  
  // writes parameter map to filename
  void writeMap(std::string filename);

  // adds (name,parameter) to parameterMap
  template<class TYPE>
    void checkInParameter(std::string name, TYPE parameter);
  
  // returns parameter as TYPE
  template<class TYPE>
  TYPE getParameter(std::string parameter, bool parsingAssertion = true);

  // checks of parameter exists
  bool parameterExists(std::string parameter);

  //  fetches list for key
  template<class CONT_TYPE>
  void getList(std::string key, CONT_TYPE& list);
  
  // compares refMaps to internal map read from filename
  bool compare(ReadInitFile refMap);

  // compares parameter 
  bool compare(ReadInitFile refMap,std::string key, bool assertion = false);

 protected:
  
  // iterator
  ParameterMap::iterator itParameterMap;

  // reads ascii parameter file
  bool readFileIntern(std::string filename, bool assertion);

  CONSTPARA(ParameterMap,parameterMap);
  CONSTPARA(MapMap, mapMap);
  CONSTPARA(std::string,filename);
  CONSTPARA(bool,assertion);
  CONSTPARA(bool,existence);
};

template<class CONT_TYPE>
void getList(std::map<std::string, std::string> const& parameterMap, std::string key, CONT_TYPE& list)
{
  typedef std::map<std::string, std::string> ParameterMap;
  
  ParameterMap::const_iterator it;
  it = parameterMap.find(key);
  if(it == parameterMap.end()){
    std::cerr << "ReadInitFile::getList: Key " << key << " does not exist in parameterMap!\n";
    exit(1);
  }
  
  std::string listString(it->second);

  unsigned int kommaPos1 = 0;
  unsigned int kommaPos2 = listString.find_first_of(",",0);
  unsigned int subLength = kommaPos2 - kommaPos1;

  while(1) {
  
    std::string subListString(listString,kommaPos1,subLength);  
    
    std::istringstream parameterStream(subListString);
    typename CONT_TYPE::value_type parameter;
    parameterStream >> parameter;
    
    list.push_back(parameter);

    if(kommaPos2 > listString.size()) break;

    kommaPos1 = kommaPos2+1;
    kommaPos2 = listString.find(",",kommaPos1);
    if(int(kommaPos2) == int(std::string::npos)) 
      kommaPos2 = listString.size()+1;
    
    subLength = kommaPos2 - kommaPos1;
  }
}

template<class TYPE>
TYPE getParameter(const std::map<std::string, std::string>& map, std::string key, bool parsingAssertion)
{
  std::map<std::string, std::string>::const_iterator it;
  it = map.find(key);
  if(it == map.end()){
    std::cerr << "ReadInitFile::getParameter: Key " << key << " does not exist in map!\n";
    abort();
  }
  
  std::string value = it->second;
  std::istringstream parameterStream(value);

  switch(int(parsingAssertion)) {
  case true: assertType<TYPE>(value); ;break;
  default: break;
  }  

  TYPE parameter; 
  parameterStream >> parameter;

  return parameter;  
}

template<class CONT_TYPE>
void ReadInitFile::getList(std::string key, CONT_TYPE& list)
{
  bool exists = parameterExists(key);
  if(!exists) {
    std::cerr << "ERROR in ReadInitFile::getList: " << key << " does not exists in file " << filename_ << "!\n";
    exit(1);
  }
  
  return BK::getList(parameterMap_,key,list);
}

template<class TYPE>
TYPE ReadInitFile::getParameter(std::string key, bool parsingAssertion)
{
  bool exists = parameterExists(key);
  if(!exists) {
    std::cerr << "ERROR in ReadInitFile::getParameter: " << key << " does not exists in file " << filename_ << "!\n";
    exit(1);
  }

  return BK::getParameter<TYPE>(parameterMap_,key,parsingAssertion);
}

template<class TYPE>
void assertType(std::string value)
{
  ERRORLOGL(4,"void ReadInitFile::assertType(std::string key)");
}

template<>
void assertType<int>(std::string value);

template<class TYPE>
void ReadInitFile::checkInParameter(std::string name, TYPE parameter)
{
  std::ostringstream parameterStream;
  parameterStream << parameter;
  
  std::pair<ParameterMap::iterator,bool> checkPair;
  checkPair = parameterMap_.insert(std::pair<std::string, std::string>(name,parameterStream.str()));
  
  if(checkPair.second != true){
    std::cerr << "ReadInitFile::checkInParameter: checkInParameter of (" << name << ") in parameterMap_ failed\n";
    abort();
  }
}

} // namespace BK

#endif
