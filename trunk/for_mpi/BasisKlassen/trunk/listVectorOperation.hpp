#ifndef listVectorOperation_hpp
#define listVectorOperation_hpp

#include <iostream>

#include "listVector.hpp"

namespace BK {

/* template<class T> */
/* ListVector<T> ListVector<T>::operator+(const ListVector<T>& listVector) */
/* { */
/*   assert(listVector.number_ == number_); */
  
/*   ListVector<T> temp = *this; */
/*   temp += listVector; */
  
/*   return temp; */
/*   //std::move(temp); */
/* } */

template<class T>
ListVector<T>& ListVector<T>::operator+=(const ListVector<T>& listVector)
{
  assert(listVector.number_ == number_);
  for(int i = 0; i < number_; i++)
    data_[i] += listVector.data_[i];
  
  return *this;
}

template<class T>
ListVector<T>& ListVector<T>::operator-=(const ListVector<T>& listVector)
{
  assert(listVector.number_ == number_);
  for(int i = 0; i < number_; i++)
    data_[i] -= listVector.data_[i];
  
  return *this;
}

template<class T>
T ListVector<T>::operator*=(const ListVector<T>& listVector)
{
  assert(listVector.number_ == number_);
  T product = 0;
  for(int i = 0; i < number_; i++)
    product += data_[i]*listVector.data_[i];
  
  return product;
}

template<class T>
ListVector<T>& ListVector<T>::operator*=(T value)
{
  for(int i = 0; i < number_; i++)
    data_[i] *= value;
  
  return *this;
}

} // namespace BK
#endif
