#include "fftwBase.hpp"

#include "logger.hpp"

#undef LOGLEVEL
#define LOGLEVEL 0

namespace BK {

  //unsigned int FftwBase::commRank_;

FftwBase::FftwBase()
{
  ERRORLOGL(1,"FftwBase::FftwBase()");

  initFlag_ = false;
}

FftwBase::~FftwBase()
{
  ERRORLOGL(1,"FftwBase::~FftwBase()");

  ERRORLOGL(4,"FftwBase::~FftwBase()-end");
}

void FftwBase::init(const MpiBase& mpiBase,int globalNx, int ny, int nz, 
		    double lx, double ly, double lz,
		    FftwPlanCreationMethod method, fftwnd_mpi_output_order order)
{
  ERRORLOGL(1,"FftwBase::init(int nx, int ny, int nz)");

  mpiBase_ = &mpiBase;

  commRank_ = mpiBase.get_commRank();

  fftwBlockArray_ = NULL;

  globalNxR_ = globalNx;
  globalNyR_ = ny;
  globalNzR_ = nz;
  
  nzpad_ = 2*(nz/2+1);
  
  globalNxC_ = globalNxR_;
  globalNyC_ = globalNyR_;
  globalNzC_ = globalNzR_/2;

  fftwOrder_ = order;
  
  ERRORLOGL(3,"Creating planR2C & planC2R ...");
  planR2C_ = rfftw3d_mpi_create_plan(MPI_COMM_WORLD,globalNxR_,globalNyR_,globalNzR_,FFTW_REAL_TO_COMPLEX,method);
  planC2R_ = rfftw3d_mpi_create_plan(MPI_COMM_WORLD,globalNxR_,globalNyR_,globalNzR_,FFTW_COMPLEX_TO_REAL,method);
  planR2Cz_ = rfftw_create_plan(globalNzR_,FFTW_FORWARD,FFTW_IN_PLACE|method);
  planC2Rz_ = rfftw_create_plan(globalNzR_,FFTW_BACKWARD,FFTW_IN_PLACE|method);
  ERRORLOGL(3,"setting local parameters...");
  
  int nxRTemp;
  int localXStart;
  rfftwnd_mpi_local_sizes(planC2R_, &(nxRTemp), &(localXStart), &(localNyAfterTranspose_),
			  &(localYStartAfterTranspose_),&(totalLocalSize_));
 
  nxR_ = nxRTemp;

  if(nxR_ == 0) {
    ERRORLOGL(0,BK::appendString("ERROR in FftwBase::init after FFTW-Plan-Creation: nx = 0 on process ",mpiBase.get_commRank()));

    exit(0);
  }

  if(order == FFTW_NORMAL_ORDER) {
    localStartC_[0] = localXStart;
    localStartC_[1] = 0;
    localStartC_[2] = 0;
    localStartR_[0] = localXStart;
    localStartR_[1] = 0;
    localStartR_[2] = 0;
    
    nyR_ = globalNyR_;
    nzR_ = globalNzR_;
    nxC_ = nxR_;
    nyC_ = globalNyR_;
    nzC_ = globalNzR_/2+1;
  }

  if(order == FFTW_TRANSPOSED_ORDER) {
    localStartC_[0] = 0;
    localStartC_[1] = localYStartAfterTranspose_;
    localStartC_[2] = 0;
    localStartR_[0] = localXStart;
    localStartR_[1] = 0;
    localStartR_[2] = 0;

    nyR_ = globalNyR_;
    nzR_ = globalNzR_;
    nxC_ = globalNxR_;
    nyC_ = localNyAfterTranspose_;
    nzC_ = globalNzR_/2+1;
  }
  

  ERRORLOGL(2,PAR(fftwOrder_));
  ERRORLOGL(2,PAR(nzpad_));
  ERRORLOGL(2,PAR(localNyAfterTranspose_));
  ERRORLOGL(2,PAR(localYStartAfterTranspose_));

  // FFTW does not normalize!
  scale_ = 1./(double(globalNxR_)*double(globalNyR_)*double(globalNzR_));

  initFlag_ = true;

  int sizeOfFftwBlock = sizeof(FftwBlock);

  processorGridDims_[0] = mpiBase.get_commSize();
  processorGridDims_[1] = 1;

  fftwBlockArray_ = new FftwBlock[mpiBase.get_commSize()];

  FftBase::init(mpiBase,lx,ly,lz);

  FftwBlock* fftwBlockArrayTemp = new FftwBlock[mpiBase.get_commSize()];

  ERRORLOGL(2,"Erzeuge FftwBlockArray");
  FftwBlock fftwBlock;
  fftwBlock.set_localNyAfterTranspose(localNyAfterTranspose_);
  fftwBlock.set_localYStartAfterTranspose(localYStartAfterTranspose_);

  // Trage myCoord zusammen
  MPI_Gather(&fftwBlock,sizeOfFftwBlock,MPI_BYTE,&fftwBlockArrayTemp[0],sizeOfFftwBlock,MPI_BYTE,0,MPI_COMM_WORLD);
  MPI_Bcast(&fftwBlockArrayTemp[0],sizeOfFftwBlock*mpiBase.get_commSize(),MPI_BYTE,0,MPI_COMM_WORLD);

  for(int i=0;i<mpiBase.get_commSize();i++) {
    fftwBlockArray_[i].set_localNyAfterTranspose(fftwBlockArrayTemp[i].get_localNyAfterTranspose());
    fftwBlockArray_[i].set_localYStartAfterTranspose(fftwBlockArrayTemp[i].get_localYStartAfterTranspose());
  }

  delete [] fftwBlockArrayTemp;

  ERRORLOGL(2,BK::appendString(mpiBase.get_commRank()," FFTW-Bloecke:\n"));
  for(int i=0;i<mpiBase.get_commSize();i++){
    ERRORLOGL(2,BK::appendString(mpiBase.get_commRank(),"  fftwBlockArray_[",i,"] = ",fftwBlockArray_[i]));
  }
  
  ERRORLOGL(4,"FftwBase::init(int nx, int ny, int nz)-end");
}

// const rfftwnd_mpi_plan& FftwBase::get_planR2C() 
// { 
//   return planR2C_; 
// }

// const rfftwnd_mpi_plan& FftwBase::get_planC2R() 
// { 
//   return planC2R_; 
// }

} // namespace BK
