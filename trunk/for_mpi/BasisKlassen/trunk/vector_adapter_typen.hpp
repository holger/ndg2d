#ifndef vector_adapter_typen_h
#define vector_adapter_typen_h

namespace BK {

template<class _TYPE>
class SetType; 

template<class TYPE>
class VectorAdapterBase;

template<class TYPE>
class VectorAdapter;

// ------------------------------------------------------------------------------

template<class FLOAT> 
class BasicTypeAdapter : public VectorAdapterBase<FLOAT>
{
 public:

  // Type für den der Adapter erstellt wird
  typedef typename SetType<FLOAT>::TYPE TYPE;
  
  // Type von dem die Adapter-elemente sind
  typedef typename SetType<FLOAT>::DOUBLE DOUBLE;
  
  BasicTypeAdapter(const VectorAdapter<TYPE> &v_a) : VectorAdapterBase<TYPE>(v_a)
    {typIndexSize = 1;}

  BasicTypeAdapter(Vector<TYPE>& _typeArray) : VectorAdapterBase<TYPE>(_typeArray)
    {typIndexSize = 1;};

  // read-operator fuer Adapter-Element
  // [x1,x2,...,xn,v1,...,vn]
  DOUBLE operator[] (int i) const;
  
  // write-operator fuer Adapter-Element
  DOUBLE& operator[] (int i);  

 protected:

};

template<class FLOAT>
class SetType
{
   public:
  // Typ von dem der Adapter ist
  typedef BasicTypeAdapter<FLOAT> ADAPTER_TYPE;
  // Type von dem die Koordinaten sind  
  typedef FLOAT DOUBLE;
  //  Type von dem der adaptierte Vector ist
  typedef FLOAT TYPE;
};

// --------------------------------------------------------------------

template<class FLOAT, int dim> 
class ParticleAdapter : public VectorAdapterBase<Particle<FLOAT,dim> >
{
 public:

  // Type für den der Adapter erstellt wird
  typedef typename SetType<Particle<FLOAT,dim> >::TYPE TYPE;
  
  // Type von dem die Adapter-elemente sind
  typedef typename SetType<Particle<FLOAT,dim> >::DOUBLE DOUBLE;
  
  ParticleAdapter(const VectorAdapter<TYPE> &v_a) : VectorAdapterBase<TYPE>(v_a)
    {typIndexSize = 2*dim;}

  ParticleAdapter(Vector<TYPE>& _typeArray) : VectorAdapterBase<TYPE>(_typeArray)
    {typIndexSize = 2*dim;};

  // read-operator fuer Adapter-Element
  // [x1,x2,...,xn,v1,...,vn]
  DOUBLE operator[] (int i) const;
  
  // write-operator fuer Adapter-Element
  DOUBLE& operator[] (int i);  

  // Gibt die Dimension des Particles zurueck
  int getDimension() const;

 protected:

  // Dimension
  const int dimension;
};

template<class FLOAT, int dim>
class SetType<Particle<FLOAT,dim> >
{
 public:
  // Typ von dem der Adapter ist
  typedef ParticleAdapter<FLOAT, dim> ADAPTER_TYPE;
  // Type von dem die Koordinaten sind  
  typedef FLOAT DOUBLE;
  // Type von dem der adaptierte Vector ist
  typedef Particle<FLOAT,dim> TYPE;
};

// ---------------------------------------------------------------------------
// definitions
// ---------------------------------------------------------------------------

template<class FLOAT>
typename SetType<FLOAT>::DOUBLE BasicTypeAdapter<FLOAT>::operator[] (int i) const
{
  if ( i < 0 || i > size-1) {
    std::cerr << "BasicTypeAdapter: invalid index in operator<FLOAT>[]: " << i << std::endl;
    abort();
  }
  return typeArray[i];
}

template<class FLOAT>
typename SetType<FLOAT>::DOUBLE& BasicTypeAdapter<FLOAT>::operator[] (int i)
{
  if ( i < 0 || i > size-1) {
    std::cerr << "BasicTypeAdapter: invalid index in operator<FLOAT>[]: " << i << std::endl;
    abort();
  }
  return typeArray[i];
}

// --------------------------------------------------------------------------------------------------

template<class FLOAT,int dim> 
typename SetType<Particle<FLOAT,dim> >::DOUBLE ParticleAdapter<FLOAT, dim>::operator[] (int i) const
{
  if ( i < 0 || i > size-1) {
    std::cerr << "ParticleAdapter: invalid index in operator<FLOAT, Particle<FLOAT,dim> >[]: " << i << std::endl;
    abort();
  }
  return typeArray[(i/dim)%typArraySize].koord[(i/(dim*typArraySize))*2+(i%dim)];
}
 
template<class FLOAT,int dim> 
typename SetType<Particle<FLOAT,dim> >::DOUBLE& ParticleAdapter<FLOAT, dim >::operator[] (int i) 
{
  if ( i < 0 || i > size-1) {
    std::cerr << "ParticleAdapter: invalid index in operator<FLOAT, Particle<FLOAT,dim> >[]: " << i << std::endl;
    abort();
  }
  return typeArray[(i/dim)%typArraySize].koord[(i/(dim*typArraySize))*2+(i%dim)];
}

} // namespace BK

#endif
