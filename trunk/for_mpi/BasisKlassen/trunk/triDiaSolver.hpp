#ifndef triDiaSolver_h
#define triDiaSolver_h

// Löst lineares Gleichungssystem Ax=rhs bei dem A eine (size X size) Tri-Diagonal-Matrix ist mit upper-Diagonale u, mid-Diagonale m 
// und lower-Diagonale l. Die Lösung x befindet sich nachher in rhs. m wird überschrieben
// u(0,n-2), m(0,n-1), l(1,n-1)

void invTriDiaStd(double* l, double* m, double* u, double* rhs, const int size)
{
  //  std::cout << "invTriDiaStd\n";
  for(int i=1;i<size;i++){
    rhs[i]-=l[i]/m[i-1]*rhs[i-1];
    m[i]-=l[i]*u[i-1]/m[i-1];
  }
  for(int i=size-2;i>-1;i--){
    rhs[i]-=u[i]/m[i+1]*rhs[i+1];
  }
  for(int i=0;i<size;i++){
    rhs[i]/=m[i]; 
  }
}

// Löst ein zyklisches Tri-Diagonal-System mit linker-unterer Ecke an_10 und rechter oberer Ecke a0n_1
// Die Lösung befindet sich nachher in rhs
 
void invTriDiaCyclic(double* l, double* m, double* u, const double a0n_1, const double an_10, double* rhs, const int size)
{
  //  std::cout << "invTriDiaCyclic\n";
  double gamma = -m[0];
  double* mOld = new double[size];
  double* v = new double[size];
  m[0] -= gamma;
  m[size-1] -= an_10*a0n_1/gamma;
  for(int i=0;i<size;i++){
    mOld[i] = m[i];
  }
  invTriDiaStd(l,m,u,rhs,size); 
  v[0] = gamma; v[size-1] = an_10;
  for(int i=1;i<size-1;i++){
    v[i] = 0.;
  }
  invTriDiaStd(l,mOld,u,v,size);
  double fact = (rhs[0]+a0n_1*rhs[size-1]/gamma)/(1.+v[0]+a0n_1*v[size-1]/gamma);
  for(int i=0;i<size;i++){
    rhs[i] -= fact*v[i];
  }
}

// --------------------------------------------------------------------------------------------

// Solver für beliebigen StorageType der Indexoperator() besitzt

// Löst lineares Gleichungssystem Ax=rhs bei dem A eine (size X size) Tri-Diagonal-Matrix ist mit upper-Diagonale u, mid-Diagonale m 
// und lower-Diagonale l. Die Lösung x befindet sich nachher in rhs. m wird überschrieben
// u(0,n-2), m(0,n-1), l(1,n-1)

template<class StorageType>
void invTriDiaStd(const StorageType l, StorageType& m, const StorageType u, StorageType& rhs, const int size)
{
  //  std::cout << "invTriDiaStd<StorageType>\n";
  for(int i=1;i<size;i++){
    rhs(i)-=l(i)/m(i-1)*rhs(i-1);
    m(i)-=l(i)*u(i-1)/m(i-1);
  }
  for(int i=size-2;i>-1;i--){
    rhs(i)-=u(i)/m(i+1)*rhs(i+1);
  }
  for(int i=0;i<size;i++){
    rhs(i)/=m(i); 
  }
}

// Löst ein zyklisches Tri-Diagonal-System mit linker-unterer Ecke an_10 und rechter oberer Ecke a0n_1
// Die Lösung befindet sich nachher in rhs
 
template<class StorageType>
void invTriDiaCyclic(const StorageType l, StorageType& m, const StorageType u, const double a0n_1, const double an_10, StorageType& rhs, const int size)
{
  //  std::cout << "invTriDiaCyclic<StorageType>\n";
  double gamma = -m(0);
  StorageType mOld(m);
  StorageType v(m);
  m(0) -= gamma;
  m(size-1) -= an_10*a0n_1/gamma;
  for(int i=0;i<size;i++){
    mOld(i) = m(i);
  }
  invTriDiaStd(l,m,u,rhs,size); 
  v(0) = gamma; v(size-1) = an_10;
  for(int i=1;i<size-1;i++){
    v(i) = 0.;
  }
  invTriDiaStd(l,mOld,u,v,size);
  double fact = (rhs(0)+a0n_1*rhs(size-1)/gamma)/(1.+v(0)+a0n_1*v(size-1)/gamma);
  for(int i=0;i<size;i++){
    rhs(i) -= fact*v(i);
  }
}

// -------------------------------------------------------------------------------------------------------------------------------

template<class StorageType>
void relaxTriDiaJakobiStd(const StorageType l, StorageType& m, const StorageType u, StorageType f, StorageType& rhs, const int indexStart, const int indexEnd)
{
  
}
#endif
