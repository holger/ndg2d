#include <iostream>
#include <complex>
#include <fstream>
#include <algorithm>

//#include <matrix.H>

#include "assert.hpp"
//#include "vector_class.hpp"
#include "triDiaSolver.hpp"
//#include "sparseMatrix.cc"
//#include "particle_class.hpp"
#include "utilities.hpp"
#include "distributionFunction.hpp"
#include "parseFromFile.hpp"
#include "logger.hpp"
#include "particleClass.hpp"
#include "fftw_complex.hpp"
#include "timer.hpp"
#include "tinyVector.hpp"
#include "functor.hpp"
#include "functor_simpleLoki.hpp"
#include "fftwMatrix.hpp"
#include "parameter.hpp"
#include "filesystem.hpp"
#include "output.hpp"
#include "fftwMatrixPool.hpp"
#include "fftwMatrixUtils.hpp"
#include "testClass.hpp"
#include "TypelistMacros.hpp"
#include "distFunc.hpp"
#include "lut.hpp"
#include "p3dMatrix.hpp"
#include "sparseMatrix.hpp"
#include "integralTrapez.hpp"
#include "singleton.hpp"
#include "vector.hpp"

#undef LOGLEVEL
#define LOGLEVEL 4

// Test1-Class
class Test1
{
  PARAMETER(double,zahl1);
  PARAMETER(double,zahl2);

public:
  void test(int a) {std::cout << "test1(" << a << ")\n";}
};

class Test2
{
public:
  void test(int a) {std::cout << "test2(" << a << ")\n";}
};

void testFunction(int a)
{
  std::cout << "standard function(" << a << ")\n";
}

// template class BK::Vector<double>;

typedef BK::Particle<float,3,3> ParticleBasis;
typedef BK::MagneticField<float,3> MagneticField;
typedef BK::HistoryVelo<float,3,100> HistoryVelo;
typedef BK_TYPELIST_7(BK::Identity,BK::Mass<float>,BK::Charge<float>,ParticleBasis,BK::TrajectoryFlag,MagneticField,HistoryVelo) ATTRIBUTE;

typedef BK::ConcreteParticle<ATTRIBUTE> PARTICLE;

class SingletonTest
{
  friend class BK::StackSingleton<SingletonTest>;
public:
  void write() {
    std::cout << "SingletonTest\n";
  }
private:
  SingletonTest() {};
  SingletonTest(const SingletonTest&);
};

int main()
{
  std::cerr << "starting main ...\n";

  BK::Vector<double> vec; 
  
  SingletonTest& singleton = BK::StackSingleton<SingletonTest>::instance();
  singleton.write();
  
  int n = 10;
  float* floatArr = new float[n];
  std::cerr.precision(16);
  for(int i=0;i<n;i++) {
    floatArr[i] = 800000000+i*i;
    std::cerr << floatArr[i]  << std::endl;
  }
  for(int i=1;i<n;i++) {
    std::cerr << floatArr[i] - floatArr[i-1] << std::endl;
  }

  //  exit(0);

  std::string message = "test";
  //  ASSERT(1==0,message);

  std::cerr << "sizeof(int) = " << sizeof(int) << std::endl;
  std::cerr << "sizeof(ofstream) = " << sizeof(std::ofstream) << std::endl;
  
  PARTICLE p1, p2;
  p1.number = 11;
  p1.trajectoryFlag = true;
  p1.b[0] = 1.3;

  std::cout << "p1.number = " << p1.number << std::endl;
  std::cout << "p1.trajectoryFlag = " << p1.trajectoryFlag << std::endl;
  std::cout << "p1.b[0] = " << p1.b[0] << std::endl;

  p2 = p1;

  std::cout << "p2.number = " << p2.number << std::endl;
  std::cout << "p2.trajectoryFlag = " << p2.trajectoryFlag << std::endl;
  std::cout << "p2.b[0] = " << p2.b[0] << std::endl;

  std::cout << "sizeof(p1) = " << sizeof(p1) << std::endl;

  for(int coord = 0;coord < 3;coord++) 
    for(int size = 0;size < 100; size++)
      p1.veloHist[coord][size] = size;

  std::cout << "size of p1 = " << PARTICLE::size << std::endl;
  
  BK::Complex<double> z1(1,2);
  BK::Complex<double> z2(0.5,-0.2);
 
  std::cout << "z1 = " << z1 << std::endl;
  std::cout << "z2 = " << z2 << std::endl;
  std::cout << "z1*z2 = " << z1*z2 << std::endl;

  float factor = 2;
  std::cout << "z1*factor = " << z1*factor << std::endl;

  std::cout << BK::appendString("z1*","z2 = ") << z1*z2 << std::endl;

  std::cout << "sqrNorm(z1) = " << real(z1) << std::endl;

//   int nnz = 4;
//   int dim = 2;
//   Matrix<double,1> elements(Index::set(nnz));
//   Matrix<double,1> vec(Index::set(dim));
//   Matrix<double,1> result(Index::set(dim));

//   Matrix<int,1> rowPointer(Index::set(dim));
//   Matrix<int,1> colIndex(Index::set(nnz));
//   elements(0) = 1;
//   elements(1) = 2;
//   elements(2) = 3;
//   elements(3) = 4;
//   rowPointer(0) = 2;
//   rowPointer(1) = 4;
//   colIndex(0) = 0;
//   colIndex(1) = 1;
//   colIndex(2) = 0;
//   colIndex(3) = 1;
//   vec(0) = 1;
//   vec(1) = 2;


//   SparseMatrix mat(elements,rowPointer,colIndex,nnz,dim);
//   mat.multRight(vec,result);
//   for(int i=0;i<dim;i++){
//     cout << result(i) << endl;
//   }

//   Matrix<double,2> matrix(Index::set(dim,dim));
//   matrix(0,0) = -1;
//   matrix(0,1) = 0;
//   matrix(1,0) = 0;
//   matrix(1,1) = 0;
//   SparseMatrix::constructSparse(matrix,dim,elements,rowPointer,colIndex,nnz);
//   SparseMatrix::multRight(elements, rowPointer, colIndex, dim, vec, result);
//   for(int i=0;i<dim;i++){
//     cout << result(i) << endl;
//   }

  // particle_class-Test:
  

  std::cerr << " RadInitFile- TEST ---------------------------------------------------\n";   
  BK::ReadInitFile initFile;

  initFile.readFile("test.init");

  std::cerr << BK::out(initFile.get_parameterMap()) << std::endl;
  std::cerr << BK::out(initFile.get_mapMap()) << std::endl;
  std::cerr << BK::getParameter<double>((initFile.get_mapMap()).begin()->second,"stokes") << std::endl;

  int a = initFile.getParameter<int>("intZahl"); 
  double b = initFile.getParameter<double>("doubleZahl"); 
  //  bool testBool = initFile.getParameter<bool>("boolZahl");
  std::string comment = initFile.getParameter<std::string>("string");
  std::cout << "a = " << a << "  b = " << b << " string = " << comment << std::endl;
   
  std::map<std::string,int> map;
  map["holger"] = 1;
  map["tom"] = 2;
  map["paul"] = 5;
  std::vector<int> liste;
  initFile.getList("liste",liste);

  for(std::vector<int>::iterator i=liste.begin();i!=liste.end();i++)
    std::cout << *i << ",";
  std::cout << std::endl;

  std::list<int> liste2;
  liste2.push_back(1);
  liste2.push_back(2);
  std::cout << "list2 = " << BK::out(liste2) << std::endl;
  std::cout << "vector = " << BK::out(liste) << std::endl;
  std::cout << "map = " << BK::out(map) << std::endl;
  
  std::cout << "map.begin() = " << BK::out(*map.begin()) << std::endl;
  BK::containerFileOut(map,"map.out");

// --------------------------------------------------------------------------

//   int number = 10000;
//   int slots = 10;
//   double zahl;
//   double minX = 0;
//   double maxX = 1;
  
//   BK::DistributionFunction<double> df();
  
//   for(int i=0;i<number;i++){
//     zahl = double(rand())/RAND_MAX*(maxX-minX);
//     df.receiveValue(zahl);
//   }

//   for(int i=0;i<slots;i++){
//     cout << df.function(i) << std::endl;
//   }
//   cout << "number = " << df.valueNumber() << endl;
//   cout << "weight = " << df.valueWeight() << endl;

//   df.normalise(1); 

//   cout << endl; 
  
//   for(int i=0;i<slots;i++){
//     cout << df.function(i) << std::endl;    
//   }  

 //  typedef Particle<3,3> ParticleBasis;
//   typedef InitialCoord<1,3> INITIALCOORD;
//   typedef TYPELIST_3(Mass,INITIALCOORD,ParticleBasis) ATTRIBUTE;
//   typedef BK::GenScatterHierarchy<ATTRIBUTE,Inheritor> PARTICLE;
 
 
//   PARTICLE particle; 
 
//   cout  << "sizeof(ParticleS) = " << sizeof(particle) << endl;
//   cout  << "sizeof(InitialCoord<1,3>) = " << sizeof(InitialCoord<1,3>) << endl;

//   particle.space[0] = 1;
//   particle.space[1] = 2;
//   particle.space[2] = 3;

//   particle.initSpace[0] = 1.5;
//   particle.initSpace[1] = 2.5;
//   particle.initSpace[2] = 3.5;


//   PARTICLE particle2(particle);
  
//   cout << particle.initSpace[0] << "  " << particle2.initSpace[0] << endl;
  // particle.charge = 2;  
 
 //  ParticleL particleL; 
//   cout  << "sizeof(ParticleL) = " << sizeof(particleL) << endl;
//   //  particleS.charge = 2;
  
  // FILELOG-TEST : -------------------------------------------------

//   FILELOGL(0,"test1.log","test1.log");
//   FILELOGL(0,"test2.log","test2.log");
//   FILELOGL(0,"test1.log","test1.log-");
//   FILELOGL(0,"test2.log","test2.log-");
//   FILELOGL(0,"test1.log","test1.log--");
//   FILELOGL(0,"test2.log","test2.log--");

  // fftw_complex-TEST ----------------------------------------------

//   FILELOGL(2,timerLogFileName,
// 	   appendString("main startet"));

//   BK::Complex<double> II(0,1);
  
//   timerM.get("test1").start();
//   BK::Complex<double> z1(1.,2.);
//   BK::Complex<double> z2(-2.,-1.);
//   BK::Complex<double> z3 = z1+z2;

//   std::cout << "z1+z2 = " << z3 << std::endl;
//   std::cout << "exp(II*pi) = " << exp(II*3.1415) << std::endl;
  
//   std::stringstream in;
//   in << "(2,3)";
//   complex<double> zz;
//   in >> z1;
//   std::cout << "zz = " << zz << std::endl;
//   std::cout << "z1 = " << z1 << std::endl;
 
//   std::cout << "hier\n";
//   complex<double> z5(-1,-2);
//   norm(z5);
//   for(int i=0;i<100;i++){
//     int k = 1;
//     k++;
//   }
//   timerM.get("test1").stop();  
  
//   timerM.get("get-test").start();
//   timerM.get("get-test").stop();
//   std::cout << "hier\n";
//   timerM.logEveryAccumulatedTime();

  // PARSER --------------------------------------------

  //std::list<char*> charList;

  // TinyVector+Matrix
  
  BK::TinyVector<double,3> tinyVec(3,4,5);

  for(int i=0;i<3;i++) {
    std::cout << tinyVec(i) << std::endl;
  }

  std::cerr << "FUNCTOR --------------------------------------------\n";

  Test1 test1;
  Test2 test2;
  BK::Functor<void, BK_TYPELIST_1(int)> functor1(&test1,&Test1::test);
  BK::Functor<void, BK_TYPELIST_1(int)> functor2(&test2,&Test2::test);

  functor1(11);
  functor2(22);

  test1.set_zahl1(2);
  test1.set_zahl2() = 5.;

  std::cout << "zahl1 = " << test1.get_zahl1() << "  zahl2 = " << test1.get_zahl2() << std::endl;

  BK::Functor<void, BK_TYPELIST_1(int)> functor3(testFunction);
  
  functor3(33);

  LOKI::Functor<void, int> functor4(&test1,&Test1::test);
  LOKI::Functor<void, int> functor5(&test2,&Test2::test);
  LOKI::Functor<void, int> functor6(testFunction);

  functor4(111);
  functor5(222);
  functor6(333);
  
//  std::cout << "functor 4 is function pointer" << BK::TypeTraits::isFunctionPointer(functor4) << std::endl; 
  
  exit(0);
  
  std::cerr << "FFTWMATRIX ------------------------------------------\n";

  int lo0 = -1;
  int hi0 = 3;
  int lo1 = 0;
  int hi1 = 1;
  int lo2 = 0;
  int hi2 = 3;

  ERRORLOGL(0,PAR(BK::FftwMatrix<double>::get_collectedNumber()));

  BK::FftwMatrix<double> mat0(BK::Range(lo0,hi0),BK::Range(lo1,hi1),BK::Range(lo2,hi2),BK::RealData);
  mat0.set_name("mat0x");
  BK::FftwMatrix<double> mat1(BK::Range(lo0,hi0),BK::Range(lo1,hi1),BK::Range(lo2,hi2),BK::RealData);
  mat1.set_name("mat1x");
  BK::FftwMatrix<double> mat2(BK::Range(lo0,hi0),BK::Range(lo1,hi1),BK::Range(lo2,hi2),BK::RealData);
  mat2.set_name("mat2x");
  BK::FftwMatrix<double> mat3(BK::Range(lo0,hi0),BK::Range(lo1,hi1),BK::Range(lo2,hi2),BK::RealData);
  mat3.set_name("mat3x");
  BK::Matrix<double> mat4(BK::Range(lo0,hi0),BK::Range(lo1,hi1),BK::Range(lo2,hi2),BK::RealData);
  mat4.set_name("mat4x");
  
  std::list<BK::FftwMatrix<double>*> matrixList;
  matrixList.push_back(&mat0);
  matrixList.push_back(&mat1);
  matrixList.push_back(&mat2);

  for(std::list<BK::FftwMatrix<double>*>::const_iterator it = matrixList.begin(); it != matrixList.end(); it++) {
    ERRORLOGL(0,PAR((**it).get_name()));
  }
  
  mat4 *= 0.;
  
  BK::Vector<int> indizes; 
  bool success = fftwMatrixNamesBeg2Indices(BK::Vector<BK::FftwMatrix<double>*>{&mat0,&mat1,&mat2,&mat3},BK::Vector<std::string>{"mat0x","mat2","mat3",""},indizes);

  ERRORLOGL(0,PAR(success));
  ERRORLOGL(0,PAR(indizes));

  BK::Vector<int> indizes2; 
  BK::Vector<std::string> name(1);
  name[0] = "mat0gf";
  success = fftwMatrixNamesBeg2Indices(BK::Vector<BK::FftwMatrix<double>*>{&mat0,&mat1,&mat2,&mat3},name,indizes2);

  ERRORLOGL(0,PAR(success));
  ERRORLOGL(0,PAR(indizes2));


  ERRORLOGL(0,PAR(BK::FftwMatrix<double>::get_collectedNumber()));

  BK::FftwMatrixPool<double> fftwMatrixPool(BK::Range(lo0,hi0),BK::Range(lo1,hi1),BK::Range(lo2,hi2));
  ERRORLOGL(0,PAR(BK::FftwMatrix<double>::get_collectedNumber()));
 
 

  for(int i=mat0.get_loR(0);i<=mat0.get_hiR(0);i++) {
    for(int j=mat0.get_loR(1);j<=mat0.get_hiR(1);j++) {
      for(int k=mat0.get_loR(2);k<=mat0.get_hiR(2);k++) {
	mat0.r(i,j,k) = i*100+j*10+k+111;
	mat4.r(i,j,k) = i*100+j*10+k+111;
      }
    }
  }
  ERRORLOGL(0,PAR(BK::FftwMatrix<double>::get_collectedNumber()));

//   for(int i=mat0.get_loR(0);i<=mat0.get_hiR(0);i++) {
//     for(int j=mat0.get_loR(1);j<=mat0.get_hiR(1);j++) {
//       for(int k=mat0.get_loR(2);k<=mat0.get_hiR(2);k++) {
// 	std::cout << "(" << i << "," << j << "," << k << ") = " << mat0.r(i,j,k) << std::endl;
//       }
//     }
//   }

//   mat0.setDataType(BK::ComplexData);
 
//   for(int i=mat0.loC[0];i<=mat0.hiC[0];i++) {
//     for(int j=mat0.loC[1];j<=mat0.hiC[1];j++) {
//       for(int k=mat0.loC[2];k<=mat0.hiC[2];k++) {
// 	std::cout << "(" << i << "," << j << "," << k << ") = " << mat0.c(i,j,k) << std::endl;
//       }
//     }
//   }

  // Vector-Class
  int loIndex = -2;
  int hiIndex = 3;

  // BK::Vector<double> vec0{BK::Range(loIndex,hiIndex)};
  // BK::Vector<double> vec1{BK::Range(loIndex,hiIndex)};
  // BK::Vector<double> vec2{BK::Range(loIndex,hiIndex)};
  // for(int i=loIndex;i<=hiIndex;i++) {
  //   vec0[i] = i;
  //   vec1[i] = i*10;
  // }

  // std::cout << "vec0.length = " << vec0.getSize() << std::endl;

  // std::cout << vec0 << std::endl;
  // std::cout << vec1 << std::endl;

  // vec2 = vec0*2.3;
  
  // vec0 -= vec1 + vec2*2.3;

  // std::cout << vec0 << std::endl; 
  // std::cout << vec2 << std::endl; 

  // Filesystem:
  std::vector<std::string> dirVec;
  BK::dirToList(dirVec,".");
  std::cout << BK::out(dirVec) << std::endl;
//   BK::setupDirectory("/home/hhomann/src/BasisKlassen/testDir1");
//   BK::setupDirectory("/home/hhomann/src/BasisKlassen/testDir2/test");
//   BK::setupDirectory("/home/hhomann/temp/temp/temp/test/");
  //  BK::setupDirectory("/home/hhomannh/src/BasisKlassen/testDir1");

//   BK::setupDirectory("/home/homann/src/BasisKlassen/testDir1");

  std::string dirName = "/home/holger/src/basisKlassen";
  BK::path dirPath(dirName);
  std::cerr << BK::out(dirPath.get_fracList()) << std::endl;
  std::cerr << dirName << " exists ? " << BK::exists(dirName) << std::endl;

//  BK::Vector<double>::iterator it = vec0.begin();
 
//  std::cout << BK::out(vec0) << std::endl;

//MacOS Workaround
// #ifndef Darwin_x86_64
//   std::cout << "min_element von vec0 = " << *std::min_element(vec0.begin(),vec0.end()) << std::endl;
//   std::cout << "max_element von vec0 = " << *std::max_element(vec0.begin(),vec0.end()) << std::endl;
// #endif
//   std::cout << BK::out(2.) << std::endl;
//   BK::containerFileOut(vec0,"vec0.out");
//   BK::containerFileOut(vec1,"vec1.out");

//   BK::asciiColumnFileOut("vecs.out",&vec0,&vec1);

  // copy-constructor-Test --------------------------------

  Test test;

  std::cerr << "std::vector<Test> ...\n";

  ERRORLOGL(1,PAR(&test));

  std::vector<Test> testVec;

  testVec.push_back(test);

  //  fftwMatrixPool.add(6);

  ERRORLOGL(1,PAR(BK::FftwMatrix<double>::get_collectedNumber()));
  ERRORLOGL(1,PAR(BK::FftwMatrix<double>::get_collectedSize()));

  BK::FftwMatrix<double>& pool0 = fftwMatrixPool.get(0);
  
  pool0 = double(0.);
  ERRORLOGL(1,PAR(BK::FftwMatrix<double>::get_collectedNumber()));
  ERRORLOGL(1,PAR(BK::FftwMatrix<double>::get_collectedSize()));

  BK::FftwMatrix<double>& pool3 = fftwMatrixPool.get(3);
  
  pool3 = 0.;
  ERRORLOGL(1,PAR(BK::FftwMatrix<double>::get_collectedNumber()));
  ERRORLOGL(1,PAR(BK::FftwMatrix<double>::get_collectedSize()));
  
  // replaceSubStr-Test

  std::string str1 = "vx";
  std::string subStr1 = "vx";
  std::string insertStr1 = "wxx";

  BK::replaceSubString(str1,subStr1,insertStr1);

  ERRORLOGL(1,PAR(str1));

  // distFunc-Test ------------------->-----------------------------------

  int max = 40;
  double mean = 10;
  double* poissonDist = new double[max];

  std::cout << BK::logFac(5) << std::endl;
  std::cout << exp(BK::logFac(5)) << std::endl;
  for(int i=0;i<max;i++)
    poissonDist[i] = BK::poisson(mean,i);

  BK::dateiAusgabeXE(poissonDist,0,max-1,"poisson.txt");
  std::cout << BK::poisson(5.,6) << std::endl;
  
  {
  // LUT-Test
  int kMax = 100;
  BK::HouExp houExp(kMax,36,36);
  BK::Functor<double, BK_TYPELIST_1(double)> hou_functor(&houExp,&BK::HouExp::exp);
  
  //  BK::LUT lutExp(&intExp,-36*BK::intPow(1,36),0,100);
  //  BK::LUT lutExp(&intExp,0,kMax,3*kMax);
  BK::LutFunctor lutExp(hou_functor,0,kMax,10*kMax);
  
  std::vector<double> lutExpArray(kMax);
  std::vector<double> expArray(kMax);
  
  for(int i=0;i<kMax;i++) {
    expArray[i] = std::exp(-36*BK::intPow(double(i)/kMax,36));
    //  lutExpArray[i] = lutExp(-36*BK::intPow(double(i)/kMax,36));
    lutExpArray[i] = lutExp(i);
  }
  
  BK::asciiColumnFileOut("exp_comp.txt",&expArray,&lutExpArray);
  }
  
  std::cerr << "SparseMatrix--------------------------------------------\n";
  
  
  BK::SparseMatrix<double> sparseMatrix;
  sparseMatrix.init(32,32,32);
  
  sparseMatrix.value(0,0,0) = 2.;

  sparseMatrix.value(1,2,5) = 5.;
  
  std::cerr << sparseMatrix(0,0,0) << "  " << sparseMatrix(1,2,5) << std::endl;
  std::cerr << sparseMatrix.value(0,0,0) << "  " << sparseMatrix.value(1,2,5) << std::endl;
  std::cerr << sparseMatrix.matrixElement(1,2,5).x << "  " << sparseMatrix.matrixElement(1,2,5).y << "  "
  	    << sparseMatrix.matrixElement(1,2,5).z << "  " << sparseMatrix.matrixElement(1,2,5).value << std::endl;

  for(BK::SparseMatrix<double>::iterator it = sparseMatrix.begin(); it != sparseMatrix.end(); it++)
    std::cerr << it->second << std::endl;

  std::cerr << "output of matrix of sparse matrix: \n";
  std::cerr << BK::out(sparseMatrix.matrix) << std::endl;

  std::cerr << "output of sparse matrix: \n";
  
  std::cerr << sparseMatrix << std::endl;
  exit(0);
  // integralTrapez - TEST

  int arraySize = 100;
  double arrayFunc[arraySize];
  double xArray[arraySize];
  for(int i=0;i<arraySize;i++) {
    xArray[i] = double(i)/arraySize;
    arrayFunc[i] = xArray[i]*xArray[i];
  }
  
  double result = BK::integralTrapez(&arrayFunc[0],&xArray[0],arraySize);
  
  std::cout << "result = " << result << " error = " << (result-1/3.)*3 << std::endl;
  
  std::cerr << "main beendet\n";
}

