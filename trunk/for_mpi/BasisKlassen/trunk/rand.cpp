#include "rand.hpp"

namespace BK {

void srandf90(long seed)
{
  //  long seed = 0;
  srand48(seed);
}

double ranf()
{
  return drand48();
}

} // namespace BK
