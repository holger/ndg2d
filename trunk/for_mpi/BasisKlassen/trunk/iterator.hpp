#ifndef iterator_h
#define iterator_h

namespace BK {

template<class TYPE>  
class ConstIterator
{
public:
    
  //  friend class Vector<TYPE>;

  ConstIterator(TYPE* init = NULL) : aktuell(init) {}

  //ConstIterator(const Vector& vec) {*this = vec.begin();}
    
  const TYPE& operator*() const {return *aktuell;}
  
  ConstIterator& operator++()
  {
    assert(aktuell != NULL);
    ++aktuell;
    return *this;
  }

  ConstIterator operator++(int)
  {
    ConstIterator temp = *this;
    ++*this;
    return temp;
  }

  ConstIterator& operator--()
  {
    assert(aktuell != NULL);
    --aktuell;
    return *this;
  }

  ConstIterator operator--(int)
  {
    ConstIterator temp = *this;
    --*this;
    return temp;
  }
    
  bool operator==(const ConstIterator& it) const
  {
    return aktuell == it.aktuell;
  }

  bool operator!=(const ConstIterator& it) const
  {
    return aktuell != it.aktuell;
  }

    
protected:
  TYPE* aktuell;
};

template<class TYPE>
class Iterator : public ConstIterator<TYPE>
{
public:
        
  Iterator(TYPE* init = NULL) : ConstIterator<TYPE>(init) { }
    
  //  Iterator(const Vector& vec) {*this = vec.begin();}
    
  TYPE& operator*() {return *this->aktuell;}

  Iterator& operator++()
  {
    assert(this->aktuell != NULL);
    ++this->aktuell;
    return *this;
  }

  Iterator operator++(int)
  {
    Iterator temp = *this;
    ++*this;
    return temp;
  }

  Iterator& operator--()
  {
    assert(this->aktuell != NULL);
    --this->aktuell;
    return *this;
  }

  Iterator operator--(int)
  {
    Iterator temp = *this;
    --*this;
    return temp;
  }
    
  bool operator==(const Iterator& it) const
  {
    return this->aktuell == it.aktuell;
  }

  bool operator!=(const Iterator& it) const
  {
    return this->aktuell != it.aktuell;
  }
    
};

} // namespace BK

#endif
