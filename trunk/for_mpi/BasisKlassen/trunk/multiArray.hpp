// Multi dimensional array class template
// The core is taken from Alejandro M. Aragón's array class (aesa)
// Expression template compliant with VecExpressions

#ifndef multiArray_hpp
#define multiArray_hpp

#include <iostream>
#include <cassert>
#include <iomanip>
#include <initializer_list>
#include <cmath>
#include <algorithm>

#include "assert.hpp"
#include "array.hpp"
#include "vecExpression.hpp"

namespace BK {

using std::cout;
using std::endl;

////////////////////////////////////////////////////////////////////////////////
// forward declarations

template <int d> struct Print;

////////////////////////////////////////////////////////////////////////////////

// iterator classes

template <typename T, typename P, int d = -1> class MultiArray_iterator;

//! MultiArray iterator class template
/*! This class is used to iterate over any dimension of the array.
 * The class contains as data members a pointer to memory, and the
 * stride that will be used to advance the iterator.
 */
template <typename T, typename P, int d>
class MultiArray_iterator : public std::iterator<std::random_access_iterator_tag, T,
                                            std::ptrdiff_t, P> {

public:
  typedef P pointer;

  //! Iterator dimension
  static int dim() { return d; }

  //! Pointer and stride parameter constructor
  MultiArray_iterator(T *x, size_t str) : p_(x), str_(str) {}

  //! Copy constructor
  MultiArray_iterator(const MultiArray_iterator &mit) : p_(mit.p_), str_(mit.str_) {}

  // Allow iterator to const_iterator conversion
  template <typename iterator>
  MultiArray_iterator(const MultiArray_iterator<
      T, typename std::enable_if<
             (std::is_same<P, typename iterator::pointer>::result), P>::type> &i)
      : p_(i.p_), str_(i.str_) {}

  //! Pre-increment iterator
  MultiArray_iterator &operator++() {
    p_ += str_;
    return *this;
  }

  //! Post-increment iterator
  MultiArray_iterator operator++(int) {
    MultiArray_iterator it(p_);
    p_ += str_;
    return it;
  }

  //! Pre-decrement iterator
  MultiArray_iterator &operator--() {
    p_ -= str_;
    return *this;
  }

  //! Post-decrement iterator
  MultiArray_iterator operator--(int) {
    MultiArray_iterator it(p_);
    p_ -= str_;
    return it;
  }

  //! Equal-to operator
  bool operator==(const MultiArray_iterator &rhs) { return p_ == rhs.p_; }

  //! Not-equal-to operator
  bool operator!=(const MultiArray_iterator &rhs) { return p_ != rhs.p_; }


  //! Dereference operator
  T &operator*() { return *p_; }

private:
  pointer p_;  //!< Pointer to memory
  size_t str_; //!< Stride
};

//! MultiArray iterator class template
/*! This partial template specialization is used to iterate over all
 * the elements of the array.
 */
template <typename T, typename P>
class MultiArray_iterator<T, P,
                     -1> : public std::iterator<std::random_access_iterator_tag,
                                                T, std::ptrdiff_t, P> {

public:
  typedef P pointer;

  //! Pointer parameter constructor
  MultiArray_iterator(T *x) : p_(x) {}

  //! Copy constructor
  MultiArray_iterator(const MultiArray_iterator &mit) : p_(mit.p_) {}

  // Allow iterator to const_iterator conversion
  template <typename iterator>
  MultiArray_iterator(const MultiArray_iterator<
      T, typename std::enable_if<
             (std::is_same<P, typename iterator::pointer>::result), P>::type> &i)
      : p_(i.p_) {}

  //! Pre-increment iterator
  MultiArray_iterator &operator++() {
    ++p_;
    return *this;
  }

  //! Post-increment iterator
  MultiArray_iterator operator++(int) { return MultiArray_iterator(p_++); }

  //! Pre-decrement iterator
  MultiArray_iterator &operator--() {
    --p_;
    return *this;
  }

  //! Post-decrement iterator
  MultiArray_iterator operator--(int) { return MultiArray_iterator(p_--); }

  //! Equal-to operator
  bool operator==(const MultiArray_iterator &rhs) { return p_ == rhs.p_; }

  //! Not-equal-to operator
  bool operator!=(const MultiArray_iterator &rhs) { return p_ != rhs.p_; }

  //! Dereference operator
  T &operator*() { return *p_; }

private:
  pointer p_; //!< Pointer to memory
};

////////////////////////////////////////////////////////////////////////////////
// array class template

//! MultiArray class template
/*! This class template is used to define tensors of any rank.
 * \tparam k - Tensor rank
 * \tparam T - Type stored in the tensor
 *
 * The class template inherits from MultiArray_traits passing itself as
 * a template parameter to use the Curiously Recurring Template
 * Pattern (CRTP).
 */
template <int k, typename T>
class MultiArray : public VecExpression<MultiArray<k, T>>{

public:
  typedef T value_type;
  typedef T *pointer;
  typedef const T *const_pointer;
  typedef T &reference;
  typedef const T &const_reference;

  typedef MultiArray_iterator<value_type, pointer> iterator;
  typedef MultiArray_iterator<const value_type, const_pointer> const_iterator;
  typedef std::reverse_iterator<iterator> reverse_iterator;
  typedef std::reverse_iterator<const_iterator> const_reverse_iterator;

  //! Helper function used to compute the index on the one-dimensional array
  //that stores the tensor elements
  template <typename pack_type> size_t index(pack_type indices) const;

private:
  //  Vector<size_t> n_ = { 0 }; //!< Tensor dimensions
  size_t n_[k] = { 0 }; //!< Tensor dimensions
  pointer data_;        //!< Pointer to memory

  ////////////////////////////////////////////////////////////////////////////////
  // indexed access operators


  //! Helper structure used by operator()
  template <typename first_type, typename... Rest> struct Check_integral {
    typedef first_type pack_type;
    enum {
      tmp = std::is_integral<first_type>::value
    };
    enum {
      value = tmp && Check_integral<Rest...>::value
    };
    static_assert(value, "*** ERROR *** Non-integral type parameter found.");
  };

  //! Partial template specialization that finishes the recursion, or
  //specialization used by vectors.
  template <typename last_type> struct Check_integral<last_type> {
    typedef last_type pack_type;
    enum {
      value = std::is_integral<last_type>::value
    };
  };

public:

  //! Helper structure used to process initializer lists
  template <int d, typename U> struct Initializer_list {
    
    typedef std::initializer_list<
        typename Initializer_list<d - 1, U>::list_type> list_type;

    static void process(list_type l, MultiArray &a, size_t s, size_t idx) {

      a.n_[k - d] = l.size(); // set dimension
      size_t j = 0;
      for (const auto &r : l)
        Initializer_list<d - 1, U>::process(r, a, s * l.size(), idx + s * j++);
    }
  };

  //! Helper structure used to process initializer lists, partial template
  //specialization to finish recursion
  template <typename U> struct Initializer_list<1, U> {

    typedef std::initializer_list<U> list_type;

    static void process(list_type l, MultiArray &a, size_t s, size_t idx) {

      a.n_[k - 1] = l.size(); // set dimension
      if (!a.data_)
        a.data_ = new value_type[s * l.size()];

      size_t j = 0;
      for (const auto &r : l)
        a.data_[idx + s * j++] = r;
    }
  };

  typedef typename Initializer_list<k, T>::list_type initializer_type;

  class IndexPack
  {
  public:
    typedef BK::Array<size_t, k> IndexArray;
    
    IndexPack(BK::Array<size_t, k> lo, BK::Array<size_t, k> hi) : lo_(lo), hi_(hi), indices_(lo) {};

    void operator++() {
      for(int i=k-1; i>=0; i--) {
	if(indices_[i] < hi_[i]) {
	  indices_[i]++;
	  return;
	}
	else {
	  indices_[i] = lo_[i];
	}
      }
    }
    
    BK::Array<size_t, k> operator()() const {
      return indices_;
    }
    
    void reset() {
      indices_ = lo_;
    }
    
    size_t size() const {
      size_t sizeTemp = hi_[0]-lo_[0]+1;
      
      for(size_t i = 1; i<k;i++)
	sizeTemp *= hi_[i]-lo_[i]+1;

      return sizeTemp;
    }

    BK::Array<size_t, k> lo() const {
      return lo_;
    }

    BK::Array<size_t, k> hi() const {
      return hi_;
    }
      
  private:

    BK::Array<size_t, k> lo_;
    BK::Array<size_t, k> hi_;
    BK::Array<size_t, k> indices_;
  };

  //////////////////////////////////////////////////////////////////////////
  // Constructors ----------------------------------------------------------
  
  //! Initializer list constructor
  MultiArray(initializer_type l);

  //! Default constructor
  MultiArray() : data_(nullptr) {}

  //! Copy constructor
  MultiArray(const MultiArray &a);

  //! Move constructor
  MultiArray(MultiArray &&src);

  //! Parameter constructor, uses the init helper function
  template <typename... Args>
  MultiArray(const Args &... args);

  //! Parameter constructor, uses the init helper function
  template <typename... Args>
  void resize(const Args &... args);

  //! Dimensional constructor
  MultiArray(Array<size_t, k> const& dim);

  //! Destructor
  ~MultiArray();

  //////////////////////////////////////////////////////////////////////////
  // Operators -------------------------------------------------------------

  //! Assignment operator
  MultiArray &operator=(const MultiArray &src);

  //! Assignment operator
  MultiArray &operator=(const T& value);

  //! Move assignment operator
  MultiArray &operator=(MultiArray &&src);

  // Assignment operator from expression template
  template<typename E>
  MultiArray &operator=(VecExpression<E> const& vecExp);

  //! Multiplication compound assignment operator
  MultiArray &operator*=(value_type s);

  //! Division compound assignment operator
  MultiArray &operator/=(value_type s);

  //! Summation compound assignment operator
  MultiArray &operator+=(const MultiArray &b);

  //! Summation compound assignment operator for VecExpressions
  template<typename E>
  MultiArray &operator+=(VecExpression<E> const& vecExp);

  //! Subtraction compound assignment operator
  MultiArray &operator-=(const MultiArray &b);

  //! Substraction compound assignment operator for VecExpressions
  template<typename E>
  MultiArray &operator-=(VecExpression<E> const& vecExp);

  //! Indexed access through operator()
  template <typename... Args> T& operator()(Args... params);

  //! Indexed access through operator() for constant tensors
  template <typename... Args> T operator()(Args... params) const;

  //! Indexed access through operator()
  T& operator()(BK::Array<size_t, k> indexPack) {
    return data_[index(indexPack)];
  }

  //! Indexed access through operator() for constant tensors
  T operator()(BK::Array<size_t, k> indexPack) const {
    return data_[index(indexPack)];
  }
  
  //! Indexed access through operator[]
  T& operator[](size_t i);
  
  //! Indexed access through operator[] for constant tensors
  T operator[](size_t i) const;

  //////////////////////////////////////////////////////////////////////////
  // Member functions ------------------------------------------------------

  // fills elements from functor
  template <class functor>
    void fill(functor fn);

  // computes the L2 norm
  T norm() const;
  
  //! Rank of the tensor
  static int rank();

  //! Dimensions of the tensor
  BK::Array<size_t, k> dim() const;

  //! Pointer to memory for raw access
  pointer data();

  //! Size of the tensor
  size_t size() const;

  //! Size along the ith direction
  size_t size(size_t i) const;

  //! Helper function used to compute the stride of iterators
  size_t stride(size_t dim) const;

  ////////////////////////////////////////////////////////////////////////////////
  // iterator functions

  //! Iterator begin
  iterator begin() { return iterator(data_); }

  //! Iterator begin for constant tensors
  const_iterator begin() const { return const_iterator(data_); }

  //! Iterator end
  iterator end() { return iterator(data_ + size()); }

  //! Iterator end for constant tensors
  const_iterator end() const { return const_iterator(data_ + size()); }

  //! Reverse iterator begin
  reverse_iterator rbegin() { return reverse_iterator(end()); }

  //! Reverse iterator begin for constante tensors
  const_reverse_iterator rbegin() const {
    return const_reverse_iterator(end());
  }

  //! Reverse iterator end
  reverse_iterator rend() { return reverse_iterator(begin()); }

  //! Reverse iterator end for constante tensors
  const_reverse_iterator rend() const {
    return const_reverse_iterator(begin());
  }

  ////////////////////////////////////////////////////////////////////////////////
  // dimensional iterator functions

  template <int d> using diterator = MultiArray_iterator<value_type, pointer, d>;

  //! Dimensional iterator begin
  template <int d> diterator<d> dbegin() {
    return diterator<d>(data_, stride(d));
  }

  //! Dimensional iterator end
  template <int d> diterator<d> dend() {
    size_t s = stride(d);
    return diterator<d>(data_ + stride(d + 1), s);
  }

  //! Dimensional iterator begin
  template <int d, typename iterator> diterator<d> dbegin(iterator it) {
    return diterator<d>(&*it, stride(d));
  }

  //! Dimensional iterator end
  template <int d, typename iterator> diterator<d> dend(iterator it) {
    size_t s = stride(d);
    return diterator<d>(&*it + stride(d + 1), s);
  }

  //! Dimensional iterator begin for constant tensors
  template <int d> diterator<d> dbegin() const {
    return diterator<d>(data_, stride(d));
  }

  //! Dimensional iterator end for constant tensors
  template <int d> diterator<d> dend() const {
    size_t s = stride(d);
    return diterator<d>(data_ + stride(d + 1), s);
  }

  //! Dimensional iterator begin for constant tensors
  template <int d, typename iterator> diterator<d> dbegin(iterator it) const {
    return diterator<d>(&*it, stride(d));
  }

  //! Dimensional iterator end for constant tensors
  template <int d, typename iterator> diterator<d> dend(iterator it) const {
    size_t s = stride(d);
    return diterator<d>(&*it + stride(d + 1), s);
  }

  // //! constructor taking an arbitrary expression
  // template <class A> MultiArray(const Expr<A> &expr) : data_(nullptr) {

  //   static_assert(std::is_same<MultiArray, typename A::result_type>::result,
  //                 "*** ERROR *** Resulting expression is not of type array.");
  //   MultiArray &a = *this;
  //   a = expr();
  // }

private:
  //! Helper function used by constructors
  size_t init_dim();

  //! init helper function that takes an integer parameter
  template <int d, typename U, typename... Args>
      typename std::enable_if <
      std::is_integral<U>::value and !std::is_pointer<U>::value and
    d<k, void>::type init(U i, Args && ... args);

  //! init helper function that takes a value to initialize all elements
  template <int d, typename U> void init(U v);

  //! init helper function that takes a pointer to already existing data
  template <int d, typename P, typename... Args>
  typename std::enable_if<std::is_pointer<P>::value, void>::type
    init(P p, Args &&... args);

  //! init helper function that takes a functor, lambda expression, etc.
  template <int d, class functor>
  typename std::enable_if<
      !std::is_integral<functor>::value and !std::is_pointer<functor>::value,
      void>::type
    init(functor fn);
  
  ////////////////////////////////////////////////////////////////////////////////
  // friend classes and functions

  //! Standard output
  template <int kk, typename TT>
    friend std::ostream &operator<<(std::ostream &os, const MultiArray<kk ,TT> &a);
};

////////////////////////////////////////////////////////////////////////////////
// implementation of array functions

// copy constructor
template <int k, typename T>
MultiArray<k, T>::MultiArray(const MultiArray<k, T> &a)
    : data_(nullptr) {

  std::copy_n(a.n_, k, n_);

  size_t s = size();
  assert((a.data_ && s > 0) || (!a.data_ && s == 0));
  
  if (a.data_) {
    data_ = new value_type[s];
    std::copy_n(a.data_, s, data_);
  } else
    data_ = nullptr;
}

// move constructor
template <int k, typename T>
MultiArray<k, T>::MultiArray(MultiArray<k, T> &&src)
    : data_(nullptr) {

  std::copy_n(src.n_, k, n_);
  data_ = src.data_;

  std::fill_n(src.n_, k, 0);
  src.data_ = nullptr;
}

template <int k, typename T>
template <typename... Args>
MultiArray<k, T>::MultiArray(const Args &... args) : data_(nullptr) {
  
  static_assert(sizeof...(Args) == k,
		"*** ERROR *** Wrong number of arguments for array");
  init<0>(args...);
}

template <int k, typename T>
template <typename... Args>
void MultiArray<k, T>::resize(const Args &... args) {

  static_assert(sizeof...(Args) == k,
		"*** ERROR *** Wrong number of arguments for array");

  if(data_ != nullptr)
    delete [] data_;
  
  init<0>(args...);
}

template <int k, typename T>
MultiArray<k, T>::MultiArray(initializer_type l) : data_(nullptr) {
  Initializer_list<k, T>::process(l, *this, 1, 0);
}

template <int k, typename T>
MultiArray<k, T>::MultiArray(Array<size_t, k> const& dim)
{
  for(int i=0; i<k; i++)
    n_[i] = dim[i];
  
  size_t s = init_dim();
  data_ = new value_type[s];
}
			       
template <int k, typename T>
MultiArray<k, T>::~MultiArray() {
  delete[] data_;
}

// assignment operator
template <int k, typename T>
MultiArray<k, T> &MultiArray<k, T>::operator=(const MultiArray<k, T> &src) {

  if (this != &src) {
    
    delete [] data_;

    std::copy_n(src.n_, k, n_);

    size_t s = size();
    assert((src.data_ && s > 0) || (!src.data_ && s == 0));

    if (src.size() > 0) {
      data_ = new value_type[s];
      std::copy_n(src.data_, s, data_);
    } else
      data_ = nullptr;
  }
  
  return *this;
}

template <int k, typename T>
MultiArray<k, T> &MultiArray<k, T>::operator=(T const& value)
{
  size_t s = size();
  for(size_t i = 0; i<s; i++)
    data_[i] = value;
  
  return *this;
}

template<int k, typename T>
template <typename E>
MultiArray<k, T> &MultiArray<k, T>::operator=(VecExpression<E> const& vecExp) {
  
  assert(size() == vecExp.size());

  for(size_t i=0;i<size();i++)
    data_[i] = vecExp[i];

  return *this;
}

// move assignment operator
template <int k, typename T>
MultiArray<k, T> &MultiArray<k, T>::operator=(MultiArray<k, T> &&src) {

  if (this != &src) {

    if (this != &src) {

      delete[] data_;
      
      std::copy_n(src.n_, k, n_);
      data_ = src.data_;
      
      // set src to default
      src.data_ = nullptr;
      std::fill_n(src.n_, k, 0);
    }
  }
  return *this;
}

template <int k, typename T>
MultiArray<k, T>& MultiArray<k, T>::operator*=(value_type s)
{
  for(size_t i=0; i<size();i++)
    data_[i] *= s;
  
  return *this;
}

template <int k, typename T>
MultiArray<k, T>& MultiArray<k, T>::operator/=(value_type s)
{
  for(size_t i=0; i<size();i++)
    data_[i] /= s;
  
  return *this;
}

template <int k, typename T>
MultiArray<k, T>& MultiArray<k, T>::operator+=(const MultiArray &b)
{
  // check dimensions
  for (int i = 0; i < k; ++i)
    assert(n_[i] == b.n_[i]);

  for(size_t i=0; i<size();i++)
    data_[i] += b.data_[i];
  
  return *this;
}

template <int k, typename T>
template<typename E>
MultiArray<k, T>& MultiArray<k, T>::operator+=(VecExpression<E> const& vecExp)
{
  assert(size() == vecExp.size());
  
  for(size_t i=0;i<size();i++)
    data_[i] += vecExp[i];
  
  return *this;
}

template <int k, typename T>
template<typename E>
MultiArray<k, T>& MultiArray<k, T>::operator-=(VecExpression<E> const& vecExp)
{
  assert(size() == vecExp.size());
  
  for(size_t i=0;i<size();i++)
    data_[i] -= vecExp[i];
  
  return *this;
}

template <int k, typename T>
MultiArray<k, T>& MultiArray<k, T>::operator-=(const MultiArray &b)
{
  // check dimensions
  for (int i = 0; i < k; ++i)
    assert(n_[i] == b.n_[i]);

  for(size_t i=0; i<size();i++)
    data_[i] -= b.data_[i];
  
  return *this;
}

template <int k, typename T>
template <typename... Args>
T& MultiArray<k, T>::operator()(Args... params)
{
  // check that the number of parameters corresponds to the size of the array
  static_assert(
		sizeof...(Args) == k,
		"*** ERROR *** Number of parameters does not match array rank.");
  
  typedef typename Check_integral<Args...>::pack_type pack_type;
  
  // unpack parameters
  pack_type indices[] = { params... };

  auto dimension = dim();
  for(int i=0; i<k; i++) {
    assert(indices[i] >= 0);
    ASSERT(size_t(indices[i]) < dimension[i], toString("indices[",i,"] = ",indices[i]," and dimension[",i,"] = ",dimension[i]));
  }
  
  //return reference
  return data_[index(indices)];
}

template <int k, typename T>
template <typename... Args>
T MultiArray<k, T>::operator()(Args... params) const
{
  // check that the number of parameters corresponds to the size of the array
  static_assert(
		sizeof...(Args) == k,
		"*** ERROR *** Number of parameters does not match array rank.");
  
  typedef typename Check_integral<Args...>::pack_type pack_type;
  
  // unpack parameters
  pack_type indices[] = { params... };

  auto dimension = dim();
  for(int i=0; i<k; i++) {
    assert(indices[i] >= 0);
    ASSERT(size_t(indices[i]) < dimension[i], toString("indices[",i,"] = ",indices[i]," and dimension[",i,"] = ",dimension[i]));
  }
  
  // return reference
  return data_[index(indices)];
}

template <int k, typename T>
T& MultiArray<k, T>::operator[](size_t i)
{
  assert((i >= 0) && (i < size()));
  
  return data_[i];
}

template <int k, typename T>
T MultiArray<k, T>::operator[](size_t i) const
{
  ASSERT((i >= 0) && (i < size()), toString("i = ",i,", size = ", size()));
  
  return data_[i];
}

template <int k, typename T>
template <class functor>
void MultiArray<k, T>::fill(functor fn) {
  std::cerr << "void fill(functor fn)" << std::endl;
  MultiArray &a = static_cast<MultiArray &>(*this);
  for (size_t i = 0; i < a.n_[0]; ++i)
    a.data_[i] = fn[i];
}

template <int k, typename T>
T MultiArray<k, T>::norm() const {
  T norm = T();
  MultiArray &a = static_cast<MultiArray &>(*this);
  assert(a.n_[0] > 0);
  for (size_t i = 0; i < a.n_[0]; ++i)
    norm += std::pow(a.data_[i], 2);
  norm = std::sqrt(norm);
  return norm;
}

template <int k, typename T>
int MultiArray<k, T>::rank()
{
  return k;
}

template <int k, typename T>
BK::Array<size_t, k> MultiArray<k, T>::dim() const {
  BK::Array<size_t, k> n;
  for(int i=0; i<k;i++)
    n[i] = n_[i];
  
  return n;
}

template <int k, typename T>
T* MultiArray<k, T>::data()
{
  return data_;
}
  
template <int k, typename T>
size_t MultiArray<k, T>::size() const {
  size_t n = 1;
  for (size_t i = 0; i < k; ++i)
    n *= n_[i];
  return n;
}

template <int k, typename T>
size_t MultiArray<k, T>::size(size_t i) const
{
  return n_[i];
}

template <int k, typename T>
size_t MultiArray<k, T>::stride(size_t dim) const {
  
  size_t s = 1;
  for (int j = 0; j < dim; ++j)
    s *= n_[j];
  return s;
}
  
////////////////////////////////////////////////////////////////////////////////////
// private member functions --------------------------------------------------------

template <int k, typename T>
size_t MultiArray<k, T>::init_dim()
{
  size_t s = n_[0];
  for (size_t i = 1; i < k; ++i) {
    if (n_[i] == 0)
      n_[i] = n_[i - 1];
    s *= n_[i];
  }
  return s;
}

template <int k, typename T>  
template <int d, typename U, typename... Args>
typename std::enable_if <
  std::is_integral<U>::value and !std::is_pointer<U>::value and
d<k, void>::type MultiArray<k, T>::init(U i, Args && ... args)
{
  assert(i != 0); // MultiArray dimension cannot be zero
  n_[d] = i;

  if(k > 1)
    init<d + 1>(args...);
}

// close recursion without this function  
template <int k, typename T>  
template <int d, typename U>
void MultiArray<k, T>::init(U i)
{
  n_[d] = i;
  size_t s = init_dim();
  data_ = new value_type[s];
  //  std::fill_n(data_, s, v);
}

// // close recursion without this function  
// template <int k, typename T>  
// template <int d>
// void MultiArray<k, T>::init(value_type v)
// {
//   size_t s = init_dim();
//   data_ = new value_type[s];
//   std::fill_n(data_, s, v);
// }


template <int k, typename T>
template <int d, typename P, typename... Args>
typename std::enable_if<std::is_pointer<P>::value, void>::type
MultiArray<k, T>::init(P p, Args &&... args)
{
  init_dim();
  data_ = p;
}

template <int k, typename T>
template <int d, class functor>
typename std::enable_if<
  !std::is_integral<functor>::value and !std::is_pointer<functor>::value,
  void>::type
MultiArray<k, T>::init(functor fn)
{
  size_t s = init_dim();
  data_ = new value_type[s];
  this->fill(fn);
}
  
//fortran ordering:
// template <int k, typename T>
// template <typename pack_type> size_t MultiArray<k, T>::index(pack_type indices) const
// {
//   size_t i = indices[0];
//   size_t s = 1;
//   for (int j = 1; j < k; ++j) {
//     assert(indices[j] >= 0);
//     assert(static_cast<size_t>(indices[j]) < n_[j]);
//     // static cast to avoid compiler warning about comparison between signed
//     // and unsigned integers
//     s *= n_[j - 1];
//     i += s * indices[j];
//   }

//   return i;
// }

//c ordering:
template <int k, typename T>
template <typename pack_type> size_t MultiArray<k, T>::index(pack_type indices) const
{
  size_t i = indices[k-1];
  size_t s = 1;

  for (int j = k - 2; j >= 0; --j) {
    ASSERT(indices[j] >= 0, toString("indices[j] = ", indices[j]));
    ASSERT(static_cast<size_t>(indices[j]) < n_[j],
	   toString("indices[j] = ", indices[j], ", n_[j] = ", n_[j], ", j = ", j));;

    // static cast to avoid compiler warning about comparison between signed
    // and unsigned integers

    s *= n_[j+1];
    i += s * indices[j];
  }
  
  return i;
}
  
////////////////////////////////////////////////////////////////////////////////////
// friend functions ---------------------------------------------------------------  
  
template <int k, typename T>
std::ostream & operator<<(std::ostream &os, const MultiArray<k, T> &a)
{
  if (a.size() == 0) {
    os << "Empty array" << endl;
    return os;
  }
  Print<k>::print(os, a.n_, a.data_);
  return os;
}
  
//! Print template partial specialization for vectors
template <> struct Print<1> {

  template <typename value_type>
  static std::ostream &print(std::ostream &os, const size_t size[],
                             const value_type *data) {

    const size_t m = size[0];
    os << "MultiArray<1> (" << m << ")" << endl;
    for (size_t i = 0; i < m; ++i)
      os << ' ' << data[i] << '\n';
    return os;
  }
};

// Fortran ordering
//! Print template partial specialization for matrices
// template <> struct Print<2> {

//   template <typename value_type>
//   static std::ostream &print(std::ostream &os, const size_t size[],
//                              const value_type *data) {

//     const size_t m = size[0];
//     const size_t n = size[1];

//     os << "MultiArray<2> (" << m << "x" << n << ")" << endl;
//     for (size_t i = 0; i < m; ++i) {
//       for (size_t j = 0; j < n; ++j)
//         os << " " << data[i + j * m];
//       cout << endl;
//     }
//     return os;
//   }
// };


// c-ordering
template <> struct Print<2> {

  template <typename value_type>
  static std::ostream &print(std::ostream &os, const size_t size[],
                             const value_type *data) {

    const size_t m = size[0];
    const size_t n = size[1];

    // size_t m = 3;
    // size_t n = 4;
    
    os << "MultiArray<2> (" << m << "x" << n << ")" << endl;
    for (size_t i = 0; i < m; ++i) {
      for (size_t j = 0; j < n; ++j)
        os << " " << data[j + i * n];
      cout << endl;
    }
    return os;
  }
};

// fortran ordering
//! Print class template that is used for tensors of order greater than 2
// template <int d> struct Print {

//   template <typename value_type>
//   static std::ostream &print(std::ostream &os, const size_t size[],
//                              const value_type *data) {

//     size_t s = 1;
//     for (int i = 0; i < d - 1; ++i)
//       s *= size[i];

//     for (size_t i = 0; i < size[d - 1]; ++i) {
//       os << "Dim " << d << ": " << i << ", ";
//       Print<d - 1>::print(os, size, data + i * s);
//     }
//     return os;
//   }
// };

// to be corrected!!!
template <int d> struct Print {

  template <typename value_type>
  static std::ostream &print(std::ostream &os, const size_t size[],
                             const value_type *data) {

    size_t s = 1;
    for (int i = d-1; i > 0; --i)
      s *= size[i];

    for (size_t i = 0; i < size[d-4]; ++i) {
      os << "Dim " << d << ": " << i << ", ";
      Print<d - 1>::print(os, size, data + i * s);
    }
    return os;
  }
};

} // namespace BK

#endif

