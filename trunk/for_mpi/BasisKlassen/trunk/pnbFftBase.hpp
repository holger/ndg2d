#ifndef bk_pnbFftBase_hpp
#define bk_pnbFftBase_hpp

#include <cmath>

#include "utilities.hpp"
#include "fftBase.hpp"

namespace BK {

class PnbFftBase :  public FftBase
{
public:

  PnbFftBase();
  ~PnbFftBase();
  
  // initialisiert die FFTW (bestimmt lokale parameter, erzeugt FFTW-Pläne)
  void init(const MpiBase& mpiBase, int globalNx, int ny, int nz, 
	    double lx, double ly, double lz, int* processorGridDims);

  int get_processorGridDims(unsigned int i) {assert(i < 2); return processorGridDims_[i];}

private:

  //  int processorGridDims_[2];
//  int loIndexR_[3];
//  int hiIndexR_[3];
//  int sizeR_[3];
//  int loIndexC_[3];
//  int hiIndexC_[3];
//  int sizeC_[3];
  
  int Nproc;
  int me;

};


} // namespace BK;

#endif
