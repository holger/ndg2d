#include "output.hpp"

namespace BK {

std::ostream& operator<<(std::ostream &of, const ContainerOut<double>& number)
{
  of << number.container;

  return of;
}

std::ostream& operator<<(std::ostream &of, const ContainerOut<float>& number)
{
  of << number.container;

  return of;
}

std::ostream& operator<<(std::ostream &of, const ContainerOut<int>& number)
{
  of << number.container;

  return of;
}

std::ostream& operator<<(std::ostream &of, const ContainerOut<std::string>& name)
{
  of << name.container;

  return of;
}

} // namespace BK
