#include "fftBase.hpp"

#include "output.hpp"

#undef LOGLEVEL
#define LOGLEVEL 0

namespace BK {

unsigned int FftBase::commRank_;

FftBase::FftBase()
{
  ERRORLOGL(1,"FftBase::FftBase()");

  initFlag_ = false;

  kxField = NULL;
  kyField = NULL;
  kzField = NULL;

  //sigmaField = NULL;
}

FftBase::~FftBase()
{
  ERRORLOGL(1,"FftBase::~FftBase()");

  if(kxField != NULL) {
    delete [] kxField;
    delete [] kyField;
    delete [] kzField;

    //delete [] sigmaField;
  }

  ERRORLOGL(4,"FftBase::~FftBase()-end");
}

void FftBase::init(const MpiBase& mpiBase, double lx, double ly, double lz)
{
  ERRORLOGL(1,"FftBase::init()");
  
  lx_ = lx;
  ly_ = ly;
  lz_ = lz;

  cx_ = 2*M_PI/lx_;
  cy_ = 2*M_PI/ly_;
  cz_ = 2*M_PI/lz_;

  ERRORLOGL(2,PAR(nxR_));
  ERRORLOGL(2,PAR(nyR_));
  ERRORLOGL(2,PAR(nzR_));
  ERRORLOGL(2,PAR(nxC_));
  ERRORLOGL(2,PAR(nyC_));
  ERRORLOGL(2,PAR(nzC_));
  ERRORLOGL(2,PAR(totalLocalSize_));
  ERRORLOGL(2,PAR(localStartC_[0]));
  ERRORLOGL(2,PAR(localStartC_[1]));
  ERRORLOGL(2,PAR(localStartC_[2]));
  ERRORLOGL(2,PAR(localStartR_[0]));
  ERRORLOGL(2,PAR(localStartR_[1]));
  ERRORLOGL(2,PAR(localStartR_[2]));

  ERRORLOGL(2,"Erzeuge FftwBlockArray");
  FftwBlock fftwBlock;
    
  fftwBlock.set_nxR(nxR_);
  fftwBlock.set_nyR(nyR_);
  fftwBlock.set_nzR(nzR_);
  fftwBlock.set_nxC(nxC_);
  fftwBlock.set_nyC(nyC_);
  fftwBlock.set_nzC(nzC_);
  fftwBlock.set_localStartR(0) = localStartR_[0];
  fftwBlock.set_localStartR(1) = localStartR_[1];
  fftwBlock.set_localStartR(2) = localStartR_[2];
  fftwBlock.set_localStartC(0) = localStartC_[0];
  fftwBlock.set_localStartC(1) = localStartC_[1];
  fftwBlock.set_localStartC(2) = localStartC_[2];
  fftwBlock.set_commRank(mpiBase.get_commRank());
  fftwBlock.set_totalLocalSize(totalLocalSize_);

  ERRORLOGL(2,BK::appendString(mpiBase.get_commRank(),"fftwBlockArray_[0] = ",fftwBlock));

  int sizeOfFftwBlock = sizeof(FftwBlock);

  ERRORLOGL(2,BK::appendString(mpiBase.get_commRank(),"  sizeof(FftwBlock)= ",sizeof(FftwBlock)));

  ERRORLOG(2,"Trage alle lokalen Fftw-Bloecke in fftwBlockArray zusammen");
  // MPI_Allgather(&fftwBlockArray_[0],sizeOfFftwBlock,MPI_BYTE,&fftwBlockArray_[0],sizeOfFftwBlock,MPI_BYTE,MPI_COMM_WORLD);
  MPI_Gather(&fftwBlock,sizeOfFftwBlock,MPI_BYTE,&fftwBlockArray_[0],sizeOfFftwBlock,MPI_BYTE,0,MPI_COMM_WORLD);
  MPI_Bcast(&fftwBlockArray_[0],sizeOfFftwBlock*mpiBase.get_commSize(),MPI_BYTE,0,MPI_COMM_WORLD);

  ERRORLOGL(2,BK::appendString(mpiBase.get_commRank()," FFTW-Bloecke:\n"));
  for(int i=0;i<mpiBase.get_commSize();i++){
    ERRORLOGL(2,BK::appendString(mpiBase.get_commRank(),"  fftwBlockArray_[",i,"] = ",fftwBlockArray_[i]));
  }

  std::list<unsigned int> localXStartList;
  for(int i=0;i<mpiBase.get_commSize();i++) {
    localXStartList.push_back(fftwBlockArray_[i].get_localStartR(0));
  }
  localXStartList.sort();
  localXStartList.unique();

  unsigned int sizeX = localXStartList.size();

  std::list<unsigned int> localYStartList;
  for(int i=0;i<mpiBase.get_commSize();i++) {
    localYStartList.push_back(fftwBlockArray_[i].get_localStartR(1));
  }
  localYStartList.sort();
  localYStartList.unique();
  
  ERRORLOGL(2,BK::appendString(mpiBase.get_commRank(),"  ",BK::out(localXStartList)));
  ERRORLOGL(2,BK::appendString(mpiBase.get_commRank(),"  ",BK::out(localYStartList)));
  
  unsigned int sizeY = localYStartList.size();

  if(sizeX*sizeY != (unsigned int)(mpiBase.get_commSize())) {
    ERRORLOGL(0,"ERROR in void FftBase::init(const MpiBase& mpiBase):");
    ERRORLOGL(0,BK::appendString("sizeX*sizeY = ",sizeX*sizeY," != mpiBase.get_commSize() = ",mpiBase.get_commSize()));
  }
  
  unsigned int temp = 0;

  std::list<unsigned int>::const_iterator myCoordXIt;
  for(std::list<unsigned int>::const_iterator i=localXStartList.begin();i!=localXStartList.end();i++) {
    if(localStartR_[0] == *i) {
      myCoordX_ = temp;
      myCoordXIt = i;
    }
    temp++;
  }
  temp = 0;
  std::list<unsigned int>::const_iterator myCoordYIt;
  for(std::list<unsigned int>::const_iterator i=localYStartList.begin();i!=localYStartList.end();i++) {
    if(localStartR_[1] == *i) {
      myCoordY_ = temp;
      myCoordYIt = i;
    }
    temp++;
  }

  unsigned int leftXStart;
  unsigned int rightXStart;

  if(sizeX == 1) {
    leftXStart = localStartR_[0];
    rightXStart = localStartR_[0];
  }
  else
    if(myCoordXIt == localXStartList.begin()) {
      leftXStart = *(--localXStartList.end());
      rightXStart = *(++myCoordXIt);
      --myCoordXIt;
    }
    else
      if(myCoordXIt == --localXStartList.end()) {
	leftXStart = *(--myCoordXIt);
	++myCoordXIt;
	rightXStart = *(localXStartList.begin());
      }
      else {
	leftXStart = *(--myCoordXIt);
	++myCoordXIt;
	rightXStart = *(++myCoordXIt);
      }
  
  unsigned int bottomYStart;
  unsigned int topYStart;

  if(sizeY == 1) {
    bottomYStart = localStartR_[1];
    topYStart = localStartR_[1];
  }
  else
    if(myCoordYIt == localYStartList.begin()) {
      bottomYStart = *(--localYStartList.end());
      topYStart = *(++myCoordYIt);
      --myCoordYIt;
    }
    else
      if(myCoordYIt == --localYStartList.end()) {
	bottomYStart = *(--myCoordYIt);
	++myCoordYIt;
	topYStart = *(localYStartList.begin());
      }
      else {
	bottomYStart = *(--myCoordYIt);
	++myCoordYIt;
	topYStart = *(++myCoordYIt);
      }

  for(int i=0;i<mpiBase.get_commSize();i++) {
    if(fftwBlockArray_[i].get_localStartR(0) == leftXStart && fftwBlockArray_[i].get_localStartR(1) == localStartR_[1]) leftCoord_ = i;
    if(fftwBlockArray_[i].get_localStartR(0) == rightXStart && fftwBlockArray_[i].get_localStartR(1) == localStartR_[1]) rightCoord_ = i;
    if(fftwBlockArray_[i].get_localStartR(1) == bottomYStart && fftwBlockArray_[i].get_localStartR(0) == localStartR_[0]) bottomCoord_ = i;
    if(fftwBlockArray_[i].get_localStartR(1) == topYStart && fftwBlockArray_[i].get_localStartR(0) == localStartR_[0]) topCoord_ = i;
  }
  

  fftwBlock.set_nxR(nxR_);
  fftwBlock.set_nyR(nyR_);
  fftwBlock.set_nzR(nzR_);
  fftwBlock.set_nxC(nxC_);
  fftwBlock.set_nyC(nyC_);
  fftwBlock.set_nzC(nzC_);
  fftwBlock.set_localStartR(0) = localStartR_[0];
  fftwBlock.set_localStartR(1) = localStartR_[1];
  fftwBlock.set_localStartR(2) = localStartR_[2];
  fftwBlock.set_localStartC(0) = localStartC_[0];
  fftwBlock.set_localStartC(1) = localStartC_[1];
  fftwBlock.set_localStartC(2) = localStartC_[2];
  fftwBlock.set_commRank(mpiBase.get_commRank());
  fftwBlock.set_myCoordX(myCoordX_);
  fftwBlock.set_myCoordY(myCoordY_);
  fftwBlock.set_leftCoord(leftCoord_);
  fftwBlock.set_rightCoord(rightCoord_);
  fftwBlock.set_bottomCoord(bottomCoord_);
  fftwBlock.set_topCoord(topCoord_);
  fftwBlock.set_totalLocalSize(totalLocalSize_);

  ERRORLOGL(2,BK::appendString(mpiBase.get_commRank(),"  collecting myCoords ..."));

  MPI_Gather(&fftwBlock,sizeOfFftwBlock,MPI_BYTE,&fftwBlockArray_[0],sizeOfFftwBlock,MPI_BYTE,0,MPI_COMM_WORLD);
  MPI_Bcast(&fftwBlockArray_[0],sizeOfFftwBlock*mpiBase.get_commSize(),MPI_BYTE,0,MPI_COMM_WORLD);
  
  ERRORLOGL(2,"\nFFTW-Bloecke:\n");
  for(int i=0;i<mpiBase.get_commSize();i++){
    ERRORLOGL(2,PAR(fftwBlockArray_[i]));
  }

  //BK::HouExp houExp(globalNxR_/2,36,36);
  BK::HouExp houExp(1.,36,36);
  BK::Functor<double, BK_TYPELIST_1(double)> hou_functor(&houExp,&BK::HouExp::exp);
  
  int maxN = BK::max(globalNxR_/2,globalNyR_/2,globalNzR_/2);
  //lutExp.init(hou_functor,0,sqrt(3.)*maxN,20*maxN);
  lutExp.init(hou_functor,0,sqrt(3.),20*maxN);

  initFlag_ = true;

  ERRORLOGL(4,"FftBase::init()-end");
}

void FftBase::createKFields()
{
  if(kxField == NULL) {
    long long size = nxC_*nyC_*nzC_;
    kxField = new float[size];
    kyField = new float[size];
    kzField = new float[size];

    //    sigmaField = new float[size];

//    int dimC0 = nxC_;
    int dimC1 = nyC_;
    int dimC2 = nzC_;

    for(unsigned int x=0;x<nxC_;x++)
      for(unsigned int y=0;y<nyC_;y++)
	for(unsigned int z=0;z<nzC_;z++) {
	  kxField[z + dimC2*(y + dimC1*x)] = kx(x);
	  kyField[z + dimC2*(y + dimC1*x)] = ky(y);
	  kzField[z + dimC2*(y + dimC1*x)] = kz(z);

	  //	  sigmaField[z + dimC2*(y + dimC1*x)] = sigma(x,y,z);
	}
  }
}


} // namespace BK
