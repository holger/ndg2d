#ifndef integration_h
#define integration_h

namespace BK {

template <class StorageType>
double integrateTrapez(const StorageType& function, const int indexStart, const int indexEnd, const double h)
{
  double tmp = function(indexStart)/2.;
  for(int i=indexStart+1;i<indexEnd; i++){
    tmp += function(i);
  }
  return (tmp+function(indexEnd)/2.)*h;
}

// Simpson-Integration (indexEnd-indexStart+1 muessen ungerade sein!)
template <class StorageType>
double integrateSimpson(const StorageType& function, const int indexStart, const int indexEnd, const double h)
{
  double tmp = function(indexStart);
  for(int i=indexStart+1;i<=indexEnd;i += 2){
    tmp += 4*function(i);
    tmp += 2*function(i+1);
  }
  tmp -= function(indexEnd);
  return tmp*h/3.;
}

} // namespace BK

#endif
