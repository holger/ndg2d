#ifndef bk_p3dMatrix_hpp
#define bk_p3dMatrix_hpp

#include "matrix.hpp"
#include "p3dBase.hpp"

namespace BK {

// --------------------------------------------------------------------

template<class T>
class P3dMatrix : public Matrix<T>
{
 public:

  using Matrix<T>::operator=;
  
  typedef T value_type;
  typedef P3dBase base_type;

  P3dMatrix(const std::string& name = "");
  ~P3dMatrix();

  P3dMatrix(const P3dMatrix& matrix);

  P3dMatrix(const int* l, const int* h, DataType type, const std::string& name = "");
  P3dMatrix(Range xRange, Range yRange, Range zRange, DataType type, const std::string& name = "");
  P3dMatrix(int nx, int ny, int nz, DataType type, const std::string& name = "");

  P3dMatrix<T>& operator=(const P3dMatrix<T>& matrix);

  void resize(Range xRange, Range yRange, Range zRange, DataType type,const std::string& name);
  void resize(const int* l, const int* h, DataType type, const std::string& name = "");
  void resize(int nx, int ny, int nz, DataType type, const std::string& name = "");
  
  P3dMatrix(P3dBase& base, DataType type, const std::string& name = "");

  void resize(P3dBase& base, DataType type, const std::string& name = "");

  void transpose_xz(P3dBase& base);
  void transpose_yz(P3dBase& base);

  inline Complex<T> skew(int x,int y,const BK::Complex<T>& c,int cx,int cy,int offset,double kz,std::vector<double> a,bool mode) const;

  // FFT complex to real in-place 
  void fftC2R();
  void fftC2Rz();
  // FFT real to complex in-place 
  void fftR2C();
  void fftR2Cz();
  // FFT complex to real out-of-place
  template<class T2> void fftC2R(const P3dMatrix<T2>& from);
  // FFT real to complex out-of-place
  template<class T2> void fftR2C(const P3dMatrix<T2>& from);  

//   void rotateFourierModes(double strength);

  void swapXYC();
  void swapXYR();

  P3dBase* get_base() const {
    assert(p3dBase_ != nullptr);
    return p3dBase_;
  }

 protected:

  inline T b(P3dBase& base, int rank, int i, int j, int k) const;
  inline T& b(P3dBase& base, int rank, int i, int j, int k);

  P3dBase* p3dBase_;
};

// --------------------------------------------------------------------------------------------------------------------------
// definitions
// --------------------------------------------------------------------------------------------------------------------------

#undef LOGLEVEL
#define LOGLEVEL 0

template<class T>
P3dMatrix<T>::P3dMatrix(const std::string& name) : Matrix<T>(name)
{
  ERRORLOGL(1,"P3dMatrix<T>::P3dMatrix()");

  p3dBase_ = NULL;

  ERRORLOGL(4,"P3dMatrix<T>::P3dMatrix()-end");
}

template<class T>
P3dMatrix<T>::~P3dMatrix() 
{
  ERRORLOGL(1,"P3dMatrix<T>::~P3dMatrix()");

  ERRORLOGL(4,"P3dMatrix<T>::~P3dMatrix()-end");
}

template<class T>
P3dMatrix<T>::P3dMatrix(const P3dMatrix<T>& matrix) : Matrix<T>(matrix)
{
  ERRORLOGL(1,"P3dMatrix<T>::P3dMatrix(const P3dMatrix& matrix)");

  p3dBase_ = matrix.p3dBase_;

  ERRORLOGL(4,"P3dMatrix<T>::P3dMatrix(const P3dMatrix& matrix)-end");
}

template<class T>
P3dMatrix<T>& P3dMatrix<T>::operator=(const P3dMatrix<T>& matrix)
{
  if(matrix.existence_) { 
    BK::Matrix<T>::operator=(matrix);
    p3dBase_ = matrix.p3dBase_;
  }
  else {
    p3dBase_ = NULL;
    this->existence_ = false;
  }

  return *this;
}

template<class T>
P3dMatrix<T>::P3dMatrix(P3dBase& base, DataType type,const std::string& name) 
{
  ERRORLOGL(1,"P3dMatrix<T>::P3dMatrix(const P3dBase&, DataType,const std::string& name)");

  this->existence_ = false;
  
  this->collectedNumber_++;

  resize(base,type,name);

  ERRORLOGL(4,"P3dMatrix<T>::P3dMatrix(const P3dBase&, DataType)-end");
}
  
template<class T>
P3dMatrix<T>::P3dMatrix(const int* l, const int* h, DataType type, const std::string& name) 
{
  ERRORLOGL(1,"P3dMatrix<T>::P3dMatrix(const int* l, const int* h, DataType type, const std::string& name");
  
  resize(l,h,type,name);
  
  ERRORLOGL(4,"P3dMatrix<T>::P3dMatrix(const int* l, const int* h, DataType type, const std::string& name-end");
}

template<class T>
P3dMatrix<T>::P3dMatrix(Range xRange, Range yRange, Range zRange, DataType type, const std::string& name)
{
  ERRORLOGL(1,"P3dMatrix<T>::P3dMatrix(Range xRange, Range yRange, Range zRange, DataType type, const std::string& name)");
  
  resize(xRange,yRange,zRange,type,name);
  
  ERRORLOGL(4,"P3dMatrix<T>::P3dMatrix(Range xRange, Range yRange, Range zRange, DataType type, const std::string& name)-end");
}

template<class T>
P3dMatrix<T>::P3dMatrix(int nx, int ny, int nz, DataType type, const std::string& name)
{
  ERRORLOGL(1,"P3dMatrix<T>::P3dMatrix(int nx, int ny, int nz, DataType type, const std::string& name)");
  
  resize(nx,ny,nz,type,name);

  ERRORLOGL(4,"P3dMatrix<T>::P3dMatrix(int nx, int ny, int nz, DataType type, const std::string& name)-end");
}

template<class T>
void P3dMatrix<T>::resize(Range xRange, Range yRange, Range zRange, DataType type,const std::string& name)
{
  ERRORLOGL(1,"P3dMatrix<T>::resize(Range xRange, Range yRange, Range zRange, DataType type,const std::string& name)");
  
  Matrix<T>::resize(xRange,yRange,zRange,type,name);

  p3dBase_ = NULL;

  ERRORLOGL(1,"P3dMatrix<T>::resize(Range xRange, Range yRange, Range zRange, DataType type,const std::string& name)-end");
} 

template<class T>
void P3dMatrix<T>::resize(int nx, int ny, int nz, DataType type,const std::string& name)
{
  ERRORLOGL(1,"P3dMatrix<T>::resize(int nx, int ny, int nz,DataType type,const std::string& name)");
  
  this->existence_ = false;
  resize(Range(0,nx-1),Range(0,ny-1),Range(0,nz-1),type,name);
  
  ERRORLOGL(1,"P3dMatrix<T>::resize(int nx, int ny, int nz,DataType type,const std::string& name)-end");
}

template<class T>
void P3dMatrix<T>::resize(const int* l, const int* h, DataType type,const std::string& name)
{
  ERRORLOGL(1,"P3dMatrix<T>::resize(const int* l, const int* h, DataType type,const std::string& name)");

  this->existence_ = false;  

  resize(Range(l[0],h[0]),Range(l[1],h[1]),Range(l[2],h[2]),type,name);
  
  ERRORLOGL(4,"P3dMatrix<T>::resize(const int* l, const int* h, DataType type,const std::string& name)-end");
}

template<class T>
void P3dMatrix<T>::resize(P3dBase& base, DataType type, const std::string& name)
{
  ERRORLOGL(1,BK::appendString("P3dMatrix<T>::resize(P3dBase& base, DataType = ",type," const std::string& = ",name,")"));

  this->existence_ = false; 

  Matrix<T>::resize(base.get_nxR(),base.get_nyR(),base.get_nzR(),base.get_nxC(),base.get_nyC(),base.get_nzC(),type,name);  
  
  p3dBase_ = &base;
  
  ERRORLOGL(4,"P3dMatrix<T>::resize(P3dBase& base, DataType type, const std::string& name)-end");
}

template<class T>
T P3dMatrix<T>::b(P3dBase& base, int rank, int i, int j, int k) const
{
  
  return this->matr_fast[int(k + this->dimR[2]*(j + this->dimR[1]*i)+rank)]; // C-ordering
}

template<class T>
T& P3dMatrix<T>::b(P3dBase& base, int rank, int i, int j, int k)
{
  return this->matr_fast[int(k + this->dimR[2]*(j + this->dimR[1]*i)+rank)]; // C-ordering
}


template<class T> inline Complex<T> P3dMatrix<T>::skew(int x,int y,const BK::Complex<T>& c,int cx,int cy,int offset,double kz,std::vector<double> a,bool mode) const
{
  return exp(II*  ((mode)?(((double)(y)+offset-cy)*a[x]):(((double)(x)-cx)*a[y+offset]))  *kz)*c;
}


template<class T>
void P3dMatrix<T>::transpose_yz(P3dBase& base)
{
  ERRORLOGL(3,"P3dMatrix<T>::transpose");

  int NX = base.get_nxR();
  int NY = base.get_nyR();
  int NZ = base.get_nzR();

  int Nproc = base.mpiBase_->get_commSize();
  int NprocX = base.get_processorGridDims(0);
  int NprocY = base.get_processorGridDims(1);

  int scale = NZ/NX/NprocX;
  NZ = NZ/scale;
  
  int i, j, k, p;
//NbX=NX/NprocX, NbY=NY/NprocY
  int Nw=NX*NY*NZ/NprocY;
  P3dMatrix<T> tempMatrix(base,BK::RealData);
  MPI_Status Sstat[Nproc], Rstat[Nproc];
  MPI_Request Sreq[Nproc], Rreq[Nproc];

  ERRORLOGL(3,PAR(NX));
  ERRORLOGL(3,PAR(NY));
  ERRORLOGL(3,PAR(NZ));
  ERRORLOGL(3,PAR(NbX));
  ERRORLOGL(3,PAR(NbY));
  ERRORLOGL(3,PAR(Nw));
  ERRORLOGL(3,PAR(this->dimR[2]));
  ERRORLOGL(3,PAR(tempMatrix.get_loR(0)));
  ERRORLOGL(3,PAR(tempMatrix.get_loR(1)));
  ERRORLOGL(3,PAR(tempMatrix.get_loR(2)));
  ERRORLOGL(3,PAR(tempMatrix.get_hiR(0)));
  ERRORLOGL(3,PAR(tempMatrix.get_hiR(1)));
  ERRORLOGL(3,PAR(tempMatrix.get_hiR(2)));

  /*   Block Transposition using Isend-Irecv   */

  int me = base.mpiBase_->get_commRank();

  tempMatrix = 0.;

  ERRORLOGL(3,PAR(tempMatrix.dimR[0]));
  ERRORLOGL(3,PAR(tempMatrix.dimR[1]));
  ERRORLOGL(3,PAR(tempMatrix.dimR[2]));
  ERRORLOGL(3,PAR(tempMatrix.base->get_nxR()));
  ERRORLOGL(3,PAR(tempMatrix.base->get_nyR()));
  ERRORLOGL(3,PAR(tempMatrix.base->get_nzR()));
  ERRORLOGL(3,PAR(tempMatrix.sizeR_));
  ERRORLOGL(3,PAR(tempMatrix.sizeC_));
  ERRORLOGL(3,PAR(Nw*sizeof(T)));

//  double filler = this->matr[0];
  if(scale>1) for(i=0; i<NX; i++) for(j=0; j<NY; j++) for(k=0; k<NZ; k++)
   {
    double temp = this->matr[int(k + this->dimR[2]/scale*(j + this->dimR[1]*i))];
    this->matr[int(k + this->dimR[2]/scale*(j + this->dimR[1]*i))] = this->matr[int(this->dimR[2]/2/scale + k + this->dimR[2]*(j + this->dimR[1]*i))];
    this->matr[int(this->dimR[2]/2/scale + k + this->dimR[2]*(j + this->dimR[1]*i))] = temp;
   }

  ERRORLOGL(3,"Local transpose ...");
  for(i=0; i<NX; i++) for(j=0; j<NY; j++)
   {
    for(k=0; k<NZ; k++)
     {
      // Z muss am langsamsten variieren, um xy-Ebenen (Bloecke) verschicken zu koennen, Y muss am schnellsten variieren
      tempMatrix.matr[int(this->dimR[1]-1-j + this->dimR[1]*(this->dimR[0]-1-i + this->dimR[0]*(this->dimR[2]/scale-1-k)))] = this->matr_fast[int(k + this->dimR[2]/scale*(j + this->dimR[1]*i))];
     }
   }//dimR2 / 2

  int xi,xiperp;
  for(int i=0;i<Nw*NprocY;i++)
    this->matr[i] = tempMatrix.matr[i];

  //switch ik
  for(int p=0;p<NprocY;p++) for(int j=0;j<NY;j++) for(xi=NX*p*NZ/NprocY,xiperp=NX*p*NZ/NprocY;xi<NX*(p+1)*NZ/NprocY;xi++)
   {
    tempMatrix.matr[int(j + this->dimR[1]*xi)] = this->matr[int(j + this->dimR[1]*xiperp)];
    xiperp+=NX;
    if(xiperp>=NX*(p+1)*NZ/NprocY)
      xiperp=xiperp%NX+1+NX*p*NZ/NprocY;
   }

  ERRORLOGL(3,"Isend and Irecv ...");
  for(p=(me-me%NprocY); p<(me-me%NprocY)+NprocY; p+=1)
   {
    if(p != me)
      { 
	MPI_Isend(&(tempMatrix.matr[int(Nw*(NprocY-1-p%NprocY))]),Nw*sizeof(T),MPI_BYTE,p,me,MPI_COMM_WORLD,Sreq+p);
	MPI_Irecv(&(this->matr[int(Nw*(NprocY-1-p%NprocY))]),Nw*sizeof(T),MPI_BYTE,p,p,MPI_COMM_WORLD,Rreq+p);
      }
   }

  for(i=int(Nw*(NprocY-1-me%NprocY));i<int(Nw*(NprocY-1-me%NprocY)+Nw);i++)
    this->matr[i] = tempMatrix.matr[i];

  for(p=(me-me%NprocY); p<(me-me%NprocY)+NprocY; p+=1)
   {
    if(p != me)
     {
      MPI_Wait(Rreq+p,Rstat+p);     
      MPI_Wait(Sreq+p,Sstat+p);
     }
   }

  for(i=0;i<Nw*NprocY;i++)
    tempMatrix.matr[i] = this->matr[i];

  int pos = 0;
  for(i=0;i<Nw*NprocY;i++)
   {
    this->matr[i] = tempMatrix.matr[Nw*NprocY-1-pos];
    pos++;
    int cnt = NY;
    if(pos%cnt==0)
       pos=pos+Nw-cnt;
    if(pos>=Nw*NprocY)
      pos=pos%Nw+cnt;
   }

//debug
//if(me==1) {for(i=0;i<Nw*NprocY;i++) std::cerr<<this->matr[i]<<" "; std::cerr<<std::endl; exit(0);}

  if(scale>1) {
  for(i=NX-1; i>=0; i--) for(j=NY-1; j>=0; j--) for(k=NZ-1; k>=0; k--)
   {
    double temp = this->matr[int(this->dimR[2]/2/scale + k + this->dimR[2]*(j + this->dimR[1]*i))];
    this->matr[int(this->dimR[2]/2/scale + k + this->dimR[2]*(j + this->dimR[1]*i))] = this->matr[int(k + this->dimR[2]/scale*(j + this->dimR[1]*i))];
    this->matr[int(k + this->dimR[2]/scale*(j + this->dimR[1]*i))] = temp;
   }
/*  for(i=0; i<NX; i++) for(j=0; j<NY; j++) for(k=0; k<NZ*scale; k++)
   {
    if(k<this->dimR[2]/2/scale||k>=this->dimR[2]*3/2/scale)
    this->matr[int(k + this->dimR[2]*(j + this->dimR[1]*i))] = filler;
   }*/ }

  ERRORLOGL(4,"P3dMatrix<T>::transpose-end");
 }

template<class T>
void P3dMatrix<T>::transpose_xz(P3dBase& base)
 {
  ERRORLOGL(3,"P3dMatrix<T>::transpose");

  int NX = base.get_nxR();
  int NY = base.get_nyR();
  int NZ = base.get_nzR(); ///

  int Nproc = base.mpiBase_->get_commSize();
  int NprocX = base.get_processorGridDims(0);
  int NprocY = base.get_processorGridDims(1);
  
  int scale = NZ/NX/NprocX;
  NZ = NZ/scale;

  int i, j, k, p;
//NbX=NX/NprocX, NbY=NY/NprocY
  int Nw=NX*NY*NZ/NprocX;
  P3dMatrix<T> tempMatrix(base,BK::RealData);
  MPI_Status Sstat[Nproc], Rstat[Nproc];
  MPI_Request Sreq[Nproc], Rreq[Nproc];

  ERRORLOGL(3,PAR(NX));
  ERRORLOGL(3,PAR(NY));
  ERRORLOGL(3,PAR(NZ));
  ERRORLOGL(3,PAR(NbX));
  ERRORLOGL(3,PAR(NbY));
  ERRORLOGL(3,PAR(Nw));
  ERRORLOGL(3,PAR(this->dimR[2]));
  ERRORLOGL(3,PAR(tempMatrix.get_loR(0)));
  ERRORLOGL(3,PAR(tempMatrix.get_loR(1)));
  ERRORLOGL(3,PAR(tempMatrix.get_loR(2)));
  ERRORLOGL(3,PAR(tempMatrix.get_hiR(0)));
  ERRORLOGL(3,PAR(tempMatrix.get_hiR(1)));
  ERRORLOGL(3,PAR(tempMatrix.get_hiR(2)));

  /*   Block Transposition using Isend-Irecv   */

  int me = base.mpiBase_->get_commRank();

  tempMatrix = 0.;

  ERRORLOGL(3,PAR(tempMatrix.dimR[0]));
  ERRORLOGL(3,PAR(tempMatrix.dimR[1]));
  ERRORLOGL(3,PAR(tempMatrix.dimR[2]));
  ERRORLOGL(3,PAR(tempMatrix.base->get_nxR()));
  ERRORLOGL(3,PAR(tempMatrix.base->get_nyR()));
  ERRORLOGL(3,PAR(tempMatrix.base->get_nzR()));
  ERRORLOGL(3,PAR(tempMatrix.sizeR_));
  ERRORLOGL(3,PAR(tempMatrix.sizeC_));
  ERRORLOGL(3,PAR(Nw*sizeof(T)));

//  double filler = this->matr[0];
  if(scale>1) for(i=0; i<NX; i++) for(j=0; j<NY; j++) for(k=0; k<NZ; k++)
   {
    double temp = this->matr[int(k + this->dimR[2]/scale*(j + this->dimR[1]*i))];
    this->matr[int(k + this->dimR[2]/scale*(j + this->dimR[1]*i))] = this->matr[int(this->dimR[2]/2/scale + k + this->dimR[2]*(j + this->dimR[1]*i))];
    this->matr[int(this->dimR[2]/2/scale + k + this->dimR[2]*(j + this->dimR[1]*i))] = temp;
   }

  ERRORLOGL(3,"Local transpose ...");
  for(i=0; i<NX; i++) for(j=0; j<NY; j++) for(k=0; k<NZ; k++)
   {
    tempMatrix.matr[int(i + this->dimR[0]*(j + this->dimR[1]*k))] = this->matr[int(k + this->dimR[2]/scale*(j + this->dimR[1]*i))];
    this->matr[int(k + this->dimR[2]/scale*(j + this->dimR[1]*i))] = 0.;
   }

  ERRORLOGL(3,"Isend and Irecv ...");
  for(p=me%NprocY; p<Nproc; p+=NprocY)
   {
    if(p != me)
      { 
	MPI_Isend(&(tempMatrix.matr[int(Nw*((p-p%NprocY)/NprocY))]),Nw*sizeof(T),MPI_BYTE,p,me,MPI_COMM_WORLD,Sreq+p);
	MPI_Irecv(&(this->matr[int(Nw*((p-p%NprocY)/NprocY))]),Nw*sizeof(T),MPI_BYTE,p,p,MPI_COMM_WORLD,Rreq+p);
      }
   }

  for(i=int(Nw*((me-me%NprocY)/NprocY));i<int(Nw*(me-me%NprocY)/NprocY+Nw);i++)
    this->matr[i] = tempMatrix.matr[i];

  for(p=me%NprocY; p<Nproc; p+=NprocY)
   {
    if(p != me)
     {
      MPI_Wait(Rreq+p,Rstat+p);     
      MPI_Wait(Sreq+p,Sstat+p);
     }
   }

  for(i=0;i<Nw*NprocX;i++)
    tempMatrix.matr[i] = this->matr[i];

  int pos = 0;
  for(i=0;i<Nw*NprocX;i++)
   {
    this->matr[i] = tempMatrix.matr[pos];
    pos++;
    if(pos%NX==0)
      pos=pos+Nw-NX;
    if(pos>=Nw*NprocX)
      pos=pos%Nw+NX;
   }

//debug
//if(me==1) {for(i=0;i<Nw*NprocX;i++) std::cerr<<this->matr[i]<<" "; std::cerr<<std::endl; exit(0);}

  if(scale>1) {
  for(i=NX-1; i>=0; i--) for(j=NY-1; j>=0; j--) for(k=NZ-1; k>=0; k--)
   {
    double temp = this->matr[int(this->dimR[2]/2/scale + k + this->dimR[2]*(j + this->dimR[1]*i))];
    this->matr[int(this->dimR[2]/2/scale + k + this->dimR[2]*(j + this->dimR[1]*i))] = this->matr[int(k + this->dimR[2]/scale*(j + this->dimR[1]*i))];
    this->matr[int(k + this->dimR[2]/scale*(j + this->dimR[1]*i))] = temp;
   }
/*  for(i=0; i<NX; i++) for(j=0; j<NY; j++) for(k=0; k<NZ*scale; k++)
   {
    if(k<this->dimR[2]/2/scale||k>=this->dimR[2]*3/2/scale)
    this->matr[int(k + this->dimR[2]*(j + this->dimR[1]*i))] = filler;
   }*/ }

  ERRORLOGL(4,"P3dMatrix<T>::transpose-end");
}

template<class T>
void P3dMatrix<T>::fftC2R() 
{
  ERRORLOGL(3,"P3dMatrix<T>::fftC2R");

  assert(p3dBase_->get_initFlag() == true);
  assert(this->dataType_ == ComplexData);
  assert(p3dBase_ != NULL);

  p3dfft_c2r(this->matr,this->matr,true);

  this->dataType_ = RealData;

  ERRORLOGL(4,"P3dMatrix<T>::fftC2R-end");
}

template<class T>
void P3dMatrix<T>::fftC2Rz() 
{
  ERRORLOGL(3,"P3dMatrix<T>::fftC2Rz");

  assert(p3dBase_->get_initFlag() == true);
  assert(this->dataType_ == ComplexData);
  assert(p3dBase_ != NULL);

  p3dfft_c2rx(this->matr,this->matr,true);

  this->dataType_ = RealData;

  ERRORLOGL(4,"P3dMatrix<T>::fftC2Rz-end");
}

template<class T>
void P3dMatrix<T>::fftR2C() 
{
  ERRORLOGL(3,"P3dMatrix<T>::fftR2C");

  assert(p3dBase_->get_initFlag() == true);
  assert(this->dataType_ == RealData);
  assert(p3dBase_ != NULL);

  p3dfft_r2c(this->matr,this->matr,true);

  *this *= p3dBase_->get_scale();

  this->dataType_ = ComplexData;

  ERRORLOGL(4,"P3dMatrix<T>::fftR2C-end");
}

template<class T>
void P3dMatrix<T>::fftR2Cz() 
{
  ERRORLOGL(3,"P3dMatrix<T>::fftR2Cz");

  assert(p3dBase_->get_initFlag() == true);
  assert(this->dataType_ == RealData);
  assert(p3dBase_ != NULL);

  p3dfft_r2cx(this->matr,this->matr,true);

  *this *= (p3dBase_->get_scale()*double(p3dBase_->get_globalNxR())*double(p3dBase_->get_globalNyR()));

  this->dataType_ = ComplexData;

//debug output
//if(p3dBase_->mpiBase_->get_commRank()==0) {for(int i=0;i<p3dBase_->get_nxR()*p3dBase_->get_nyR()*(p3dBase_->get_nzR()+0);i++) std::cerr<<this->matr[i]<<" "; std::cerr<<std::endl; exit(0);}

  ERRORLOGL(4,"P3dMatrix<T>::fftR2Cz-end");
}

template<class T>
template<class T2>
void P3dMatrix<T>::fftR2C(const P3dMatrix<T2>& from) 
{
  ERRORLOGL(3,"P3dMatrix<T>::fftR2C(const P3dMatrix<T2>& from)");

  assert(p3dBase_->get_initFlag() == true);
  assert(from.get_dataType() == RealData);
  assert(p3dBase_ != NULL);
  
  this->dataType_ = RealData;

  this->operator=(from);

  p3dfft_r2c(this->matr,this->matr,0);
  // p3dfft_r2c(this->matr,from.constDataR(),false);

  *this *= p3dBase_->get_scale();
  this->dataType_ = ComplexData;

  ERRORLOGL(4,"P3dMatrix<T>::fftR2C(const P3dMatrix<T2>& from)-end");
}

template<class T>
template<class T2>
void P3dMatrix<T>::fftC2R(const P3dMatrix<T2>& from) 
{
  ERRORLOGL(3,"P3dMatrix<T>::fftC2R(const P3dMatrix<T2>& from)");

  assert(p3dBase_->get_initFlag() == true);
  assert(from.get_dataType() == ComplexData);
  assert(p3dBase_ != NULL);

  this->dataType_ = RealData;

  this->operator=(from);

  p3dfft_c2r(this->matr,this->matr,0);
  //p3dfft_c2r(this->matr,from.constDataR(),false);

  this->dataType_ = RealData;

  ERRORLOGL(4,"P3dMatrix<T>::fftC2R(const P3dMatrix<T2>& from)-end");
}


} // namespace BK

#endif
