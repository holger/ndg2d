#ifndef listVector_h
#define listVector_h

#include <cstring>

namespace BK {

template<class T>
class ListVector
{
 public:

  typedef T value_type;
  
  ListVector() {
    allocatedNumber_ = 0;
    number_ = 0;
    data_ = nullptr;
  }

  ListVector(int size) {
    resize(size);
  }

  ListVector(const ListVector& listVector) {
    allocatedNumber_ = listVector.allocatedNumber_;
    number_ = listVector.number_;
    data_ = new T[allocatedNumber_];
    this->operator=(listVector);
  }

  ListVector& operator=(const ListVector& listVector) {
    assert(allocatedNumber_ == listVector.allocatedNumber_);
    assert(number_ == listVector.number_);
    memcpy(data_,listVector.data_,sizeof(T)*number_);
    
    return *this;
  }
  
  /* ListVector operator+(const ListVector& listVector); */

  ListVector& operator+=(const ListVector& listVector);
  ListVector& operator-=(const ListVector& listVector);
  ListVector& operator*=(T value);

  T operator*=(const ListVector& listVector);
  

  ~ListVector() {
    number_ = 0;
    allocatedNumber_ = 0;
    delete [] data_;
    data_ = nullptr;
  }
  
  void resize(size_t size) {
    allocatedNumber_ = size;
    number_ = size;
    data_ = new T[size];
  }

  void reserve(size_t size) {
    if(size < number_) {
      std::cerr << "ERROR in ListVector::reserve(" << size << "):\n";
      std::cerr << "size < number_ = " << number_ << std::endl;
      exit(1);
    }
    
    allocatedNumber_ = size;
    if(data_ != nullptr) {
      T* dataTemp = new T[size];
      for(size_t i=0; i < number_;i++)
	dataTemp[i] = data_[i];

      delete [] data_;
      data_ = dataTemp;
    }
    else
      data_ = new T[size];
  }
  
  void remove(size_t i) {
    data_[i] = data_[number_-1];
    --number_;
  }

  T operator[](size_t i) const {
    assert(i < number_);
    return data_[i];}
  T& operator[](size_t i) {
    assert(i < number_);
    return data_[i];
  }

  T at(size_t i) const {
    assert(i < number_);
    return data_[i];
  }
  T& at(size_t i) {
    assert(i < number_);
    return data_[i];
  }
  
  size_t allocatedNumber() const { return allocatedNumber_; }
  size_t number() const { return number_; }
  size_t size() const { return number_; }
  
  void increaseNumber(size_t n) {
    number_ += n;
    if(number_ > allocatedNumber_) {
      T* dataTemp = new T[number_];
      for(size_t i=0; i < allocatedNumber_;i++)
	dataTemp[i] = data_[i];

      delete [] data_;
      data_ = dataTemp;
      allocatedNumber_ = number_;
    }
  }

  const T* get_data() const {return data_;}

 private:
  
  size_t allocatedNumber_;
  size_t number_;
  T* data_;
};

} // namespace BK

#endif
