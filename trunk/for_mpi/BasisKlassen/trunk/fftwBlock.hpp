#ifndef fftwBlock_hpp
#define fftwBlock_hpp

#include <fstream>

#include "parameter.hpp"

namespace BK {

class FftwBlock
{
 public:
  FftwBlock() {};

  // locale dimensions (without Boundaries)
  PARAMETER(unsigned int,nxR);
  PARAMETER(unsigned int,nyR);
  PARAMETER(unsigned int,nzR);
  PARAMETER(unsigned int,nxC);
  PARAMETER(unsigned int,nyC);
  PARAMETER(unsigned int,nzC);

  // The rank of the block owner
  PARAMETER(unsigned int,commRank);

  // Number of the Block, counted from the global left boundary
  PARAMETER(unsigned int,myCoordX);
  // Number of the Block, counted from the global bottom boundary
  PARAMETER(unsigned int,myCoordY);

  // Number of left neighbour
  PARAMETER(unsigned int,leftCoord);

  // Number of right neighbour
  PARAMETER(unsigned int,rightCoord);

  // Number of top neighbour
  PARAMETER(unsigned int,topCoord);

  // Number of bottom neighbour
  PARAMETER(unsigned int,bottomCoord);
  
  // ny-size after Transpose
  PARAMETER(unsigned int,localNyAfterTranspose);
  
  // global block position after transpose
  PARAMETER(unsigned int,localYStartAfterTranspose);

  // number of block-elements
  PARAMETER(unsigned int,totalLocalSize);

  unsigned int get_localStartR(unsigned int i) const {return localStartR_[i];}
  unsigned int get_localStartC(unsigned int i) const {return localStartC_[i];}
  
  unsigned int& set_localStartR(unsigned int i) {return localStartR_[i];}
  unsigned int& set_localStartC(unsigned int i) {return localStartC_[i];}

private:

  unsigned int localStartR_[3];
  unsigned int localStartC_[3];
};

std::ostream& operator<<(std::ostream &of, const FftwBlock& fftwBlock);

} // namespace BK

#endif
