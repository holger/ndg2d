#ifndef bk_logger_hpp
#define bk_logger_hpp

#include <iostream>
#include <fstream>
#include <string>
#include <map>

#include <boost/preprocessor/control/iif.hpp>
#include <boost/preprocessor/facilities/empty.hpp>
#include <boost/preprocessor/comparison/greater_equal.hpp>

#include "utilities.hpp"
#include "singleton.hpp"

namespace BK {

class Logger;

// globale Referenz auf Logger-Singleton
#ifndef logger

// einfaches Meyers-Singleton:
#define logger BK::Singleton<BK::Logger>::instance()

#endif

#define PAR(x) BK::appendString((#x)," = ",x)
#define PAR2(x1,x2) BK::appendString((#x1)," = ",x1,"\n",(#x2)," = ",x2)
#define PAR3(x1,x2,x3) BK::appendString((#x1)," = ",x1,"\n",(#x2)," = ",x2,"\n",(#x3)," = ",x3)
#define PAR4(x1,x2,x3,x4) BK::appendString((#x1)," = ",x1,"\n",(#x2)," = ",x2,"\n",(#x3)," = ",x3,"\n",(#x4)," = ",x4)
#define PAR5(x1,x2,x3,x4,x5) BK::appendString((#x1)," = ",x1,"\n",(#x2)," = ",x2,"\n",(#x3)," = ",x3,"\n",(#x4)," = ",x4,"\n", \
                             (#x5)," = ",x5)
#define PAR6(x1,x2,x3,x4,x5,x6) BK::appendString((#x1)," = ",x1,"\n",(#x2)," = ",x2,"\n",(#x3)," = ",x3,"\n",(#x4)," = ",x4 \
                              ,"\n",(#x5)," = ",x5,"\n",(#x6)," = ",x6)
#define PAR7(x1,x2,x3,x4,x5,x6,x7) BK::appendString((#x1)," = ",x1,"\n",(#x2)," = ",x2,"\n",(#x3)," = ",x3,"\n",(#x4)," = ",x4 \
                              ,"\n",(#x5)," = ",x5,"\n",(#x6)," = ",x6,"\n",(#x7)," = ",x7)




// LOGLEVEL: 0 bis 5 
// 0 : nur wichtigstes (wird immer ausgegeben)
// 5 : Debug

#define LOGLEVEL 0

// schreibt x in die Standartausgabe
 
#define WRITELOG(i,x)                                  \
  BOOST_PP_IIF( BOOST_PP_GREATER_EQUAL( LOGLEVEL, i ), \
    std::cout << __LINE__ << " " << __FILE__ << ": "<< x;, \
    BOOST_PP_EMPTY()                                   \
  )

// schreibt x in die Fehlerausgabe

#define ERRORLOG(i,x)                                  \
  BOOST_PP_IIF( BOOST_PP_GREATER_EQUAL( LOGLEVEL, i ), \
    std::cerr << x;, \
    BOOST_PP_EMPTY()                                   \
  )

#define ERRORLOGL(i,x)                                  \
  BOOST_PP_IIF( BOOST_PP_GREATER_EQUAL( LOGLEVEL, i ), \
    std::cerr << x << std::endl;, \
    BOOST_PP_EMPTY()                                   \
  )

#define ERRORLOGD(i,x)                                  \
  BOOST_PP_IIF( BOOST_PP_GREATER_EQUAL( LOGLEVEL, i ), \
    std::cerr << __LINE__ << " " << __FILE__ << ": " << x;, \
    BOOST_PP_EMPTY()                                   \
  )

#define ERRORLOGDL(i,x)                                  \
  BOOST_PP_IIF( BOOST_PP_GREATER_EQUAL( LOGLEVEL, i ), \
    std::cerr << __LINE__ << " " << __FILE__ << ": " << x << std::endl;, \
    BOOST_PP_EMPTY()                                   \
  )

// schreibt comment in datei filename, je nach nachdem, ob LOGLEVEL < i ist.

#define FILELOG(i,filename,comment)                                  \
  BOOST_PP_IIF( BOOST_PP_GREATER_EQUAL( LOGLEVEL, i ),               \
   logger.out(filename ,comment);,                                   \
   BOOST_PP_EMPTY()                                                  \
  )

// schreibt comment in datei filename mit Zeilenumbruch, je nach nachdem, ob LOGLEVEL < i ist.

#define FILELOGL(i,filename,comment)                                  \
  BOOST_PP_IIF( BOOST_PP_GREATER_EQUAL( LOGLEVEL, i ),                \
   logger.line(filename , comment);,                                  \
    BOOST_PP_EMPTY()                                                  \
  )

#define FILELOGD(i,filename,comment)                                  \
  BOOST_PP_IIF( BOOST_PP_GREATER_EQUAL( LOGLEVEL, i ), \
   logger.out(filename , BK::appendString(__LINE__ , " " , __FILE__ , "|   " , comment));, \
   BOOST_PP_EMPTY()                                   \
  )

// schreibt comment in datei filename mit Zeilenumbruch, je nach nachdem, ob LOGLEVEL < i ist.

#define FILELOGDL(i,filename,comment)                                  \
  BOOST_PP_IIF( BOOST_PP_GREATER_EQUAL( LOGLEVEL, i ), \
   logger.out(filename , BK::appendString(__LINE__ , " " , __FILE__ , "|   " , comment,"\n"));, \
   BOOST_PP_EMPTY()                                   \
  )

// erstellt Log-File
class Logger
{
 public:
  Logger();

  // Destruktor: schlie�t alle ofstreams
  ~Logger();

  // schreibt comment in filename
  void out(std::string filename, std::string comment);
  
  // schreibt line in filename mit Zeilenumbruch
  void line(std::string filename, std::string line);
  
  // fuegt in filename Zeilenumbruch durch
  void blankLine(std::string filename);
    
 protected:
  
  // Map haelt Datei-Namen und Pointer auf geoeffneten ofstream
  std::map<std::string, std::ofstream*> logFileMap;
  
  // Iterator zur obigen Map
  std::map<std::string, std::ofstream*>::iterator itLogFileMap;

  // prueft, ob filename in  Map logFileMap ist,
  // falls ja, steht itLogFileMap auf zugehoerigem ofstream,
  // falls nicht, wird ofstream zu filename erzeugt und der Map
  // beigefuegt
  void fileCheck(std::string filename);
};

} // namespace BK

#endif
