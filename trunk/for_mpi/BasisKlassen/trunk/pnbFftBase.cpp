#include "pnbFftBase.hpp"

#include "fftw_complex.hpp"
#include "logger.hpp"

#undef LOGLEVEL
#define LOGLEVEL 4

namespace BK {

PnbFftBase::PnbFftBase()
{
  ERRORLOGL(1,"PnbFftBase::PnbFftBase()");

  initFlag_ = false;
}

PnbFftBase::~PnbFftBase()
{
  ERRORLOGL(1,"PnbFftBase::~PnbFftBase()");

  ERRORLOGL(4,"PnbFftBase::~PnbFftBase()-end");
}

void PnbFftBase::init(const MpiBase& mpiBase,int globalNx, int ny, int nz, 
		      double lx, double ly, double lz, int* processorGridDims)
{
  ERRORLOGL(1,"PnbFftBase::init(int nx, int ny, int nz)");

  mpiBase_ = &mpiBase;

  commRank_ = mpiBase.get_commRank();

  processorGridDims_[0] = processorGridDims[0];
  processorGridDims_[1] = processorGridDims[1];

  fftwBlockArray_ = NULL;

  globalNxR_ = globalNx;
  globalNyR_ = ny;
  globalNzR_ = nz;
  
  globalNxC_ = globalNxR_;
  globalNyC_ = globalNyR_;
  globalNzC_ = globalNzR_/2;
  
    
  nxR_ = globalNxR_;
  nyR_ = globalNyR_/mpiBase.get_commSize();
  nzR_ = globalNzR_;
  nxC_ = globalNxR_;
  nyC_ = globalNyR_/mpiBase.get_commSize();
  nzC_ = globalNzR_/2;

  ERRORLOGL(4,PAR(nxR_));
  ERRORLOGL(4,PAR(nyR_));
  ERRORLOGL(4,PAR(nzR_));
  ERRORLOGL(4,PAR(nxC_));
  ERRORLOGL(4,PAR(nyC_));
  ERRORLOGL(4,PAR(nzC_));
  
  if(nxR_ == 0 || nyR_ == 0 || nzR_ == 0) {
    ERRORLOGL(0,BK::appendString("ERROR in PnbFftBase::init after FFTW-Plan-Creation: nx = 0 on process ",mpiBase.get_commRank()));

    exit(0);
  }
  
  localStartR_[0] = 0;
  localStartR_[1] = mpiBase.get_commRank()*nyR_;
  localStartR_[2] = 0;
  localStartC_[0] = 0;
  localStartC_[1] = mpiBase.get_commRank()*nyC_;
  localStartC_[2] = 0;

  ERRORLOGL(4,PAR(localStartR_[0]));
  ERRORLOGL(4,PAR(localStartR_[1]));
  ERRORLOGL(4,PAR(localStartR_[2]));
  ERRORLOGL(4,PAR(localStartC_[0]));
  ERRORLOGL(4,PAR(localStartC_[1]));
  ERRORLOGL(4,PAR(localStartC_[2]));

  // P3D normiert nicht!
  scale_ = 1./(double(globalNxR_)*double(globalNyR_)*double(globalNzR_));

  ERRORLOGL(4,PAR(scale_));

  fftwBlockArray_ = new FftwBlock[mpiBase.get_commSize()];

  FftBase::init(mpiBase,lx,ly,lz);

  Nproc = mpiBase_->get_commSize();
  me = mpiBase_->get_commRank();

  ERRORLOGL(4,PAR(Nproc));
  ERRORLOGL(4,PAR(me));

  ERRORLOGL(4,"PnbFftBase::init(int nx, int ny, int nz)-end");
}

// void PnbFftBase::fft(double* data,int sign, int complete)
// {
//   ERRORLOGL(2,"PnbFftBase::fft()");
  
// //   int  Nx=NX, Ny=NY*Nproc, Nz=NZ;
  
// //   real fnorm=1./(real)(Nx*Ny*Nz);
  
//   int NX = nxR_;
//   int NY = nyR_;
//   int NZ = nzR_;

//  int howmany_z=NX*NY, N_in_z[1], inembed_z[1];
//  N_in_z[0]=NZ; inembed_z[0]=NZ+2;
//  int istride_z=1 , idist_z=(NZ+2);
//  int onembed_z[1];
//  onembed_z[0]=(NZ/2+1);
//  int ostride_z=1,  odist_z= (NZ/2+1);  
 

//  int howmany_x =NY*(NZ/2+1), N_in_x[1], inembed_x[1]; 
//  N_in_x[0]=NX; inembed_x[0]=NX;
//  int istride_x=(NZ/2+1)*NY, idist_x=1; 
//  int onembed_x[1]; onembed_x[0]=NX; 
//  int ostride_x=(NZ/2+1)*NY, odist_x=1; 


// fftw_plan  p1, p2, p3, p1b, p2b, p3b;

//  Complex<double>* complexP = (Complex<double>*)(data);
 
// if(sign==-1)
//   { //zero(a);
//     if (Nproc> 1)
//     {  
//       p1 = fftw_plan_many_dft_r2c(1,N_in_z,howmany_z,data,inembed_z,istride_z,idist_z,
// 				  reinterpret_cast<fftw_complex*>(data),onembed_z,ostride_z,odist_z,FFTW_ESTIMATE);
//       p2 = fftw_plan_many_dft(1,N_in_x,howmany_x,reinterpret_cast<fftw_complex*>(data),inembed_x,istride_x,idist_x,
// 			      reinterpret_cast<fftw_complex*>(data),onembed_x,ostride_x,odist_x,FFTW_FORWARD,FFTW_ESTIMATE);
//      fftw_execute(p1);
//      fftw_execute(p2);
//      transpose_xy(data);
//      fftw_execute(p2);
//      if(complete)transpose_xy(a);
//      fftw_destroy_plan(p1);fftw_destroy_plan(p2);
//     }
//     else 
//      {
//       p3 = fftw_plan_dft_r2c_3d(NX, NY, NZ, &a->m[0], &a->c[0][0][0] ,FFTW_ESTIMATE);
//       fftw_execute(p3); 
//       fftw_destroy_plan(p3);
//      }
//     a*=scale_; 
//     //zero_kmax(a);
//       }



// if(sign==1)
//   { //zero_kmax(a);
//     if (Nproc > 1 )
//       {
// p2b=  fftw_plan_many_dft(1,N_in_x,howmany_x,&a->c[0][0][0],inembed_x,istride_x,idist_x,
//              &a->c[0][0][0],onembed_x,ostride_x,odist_x,FFTW_BACKWARD,FFTW_ESTIMATE);
// p1b = fftw_plan_many_dft_c2r(1,N_in_z,howmany_z,&a->c[0][0][0],onembed_z,ostride_z,odist_z,
//                                  &a->m[0],inembed_z,istride_z,idist_z,FFTW_ESTIMATE);
//        if(complete)transpose_xy(a);
//        fftw_execute(p2b);
//        transpose_xy(a);
//        fftw_execute(p2b);
//        fftw_execute(p1b);
//        fftw_destroy_plan(p1b);fftw_destroy_plan(p2b);
//       }
//     else 
//       {
//       p3b = fftw_plan_dft_c2r_3d(NX, NY, NZ, &a->c[0][0][0], &a->m[0] ,FFTW_ESTIMATE);    
//       fftw_execute(p3b);
//       fftw_destroy_plan(p3b);
//       }
//     //zero(a);
//   }

  
//   ERRORLOGL(4,"PnbFftBase::fft()");
// }

// void PnbFftBase::transpose_xy(double* data)     
// {
//   int NX = nxR_;
//   int NY = nyR_;
//   int NZ = nzR_;
  

//   //    extern int me, Nproc;
//     int i, j, k, p, Nb=NX/NPROC, Nw=Nb*NY*(NZ+2);
//     scalar c;
//     MPI_Status Sstat[NPROC], Rstat[NPROC];
//     MPI_Request Sreq[NPROC], Rreq[NPROC];


//     /*   Block Transposition using Isend-Irecv   */

//     for(p=0; p<Nproc; p++)
//     {   if(p != me)
//         { MPI_Isend(a->b[p][0][0],Nw,MPI_real,p,me,MPI_COMM_WORLD,Sreq+p);
//           MPI_Irecv(c->b[p][0][0],Nw,MPI_real,p,p,MPI_COMM_WORLD,Rreq+p);
//         }
//     }


//     for(i=0; i<Nb; i++) for(j=0; j<NY; j++) for(k=0; k<NZ; k++)
//         c->b[me][i][j][k] = a->b[me][i][j][k];
//     for(i=0; i<Nb; i++) for(j=0; j<NY; j++) for(k=0; k<NZ; k++)
//         a->b[me][i][j][k] = c->b[me][j][i][k];

//     for(p=0; p<Nproc; p++) 
//     {   if(p != me) 
//         {   MPI_Wait(Rreq+p,Rstat+p);     
//             MPI_Wait(Sreq+p,Sstat+p);
//             for(i=0; i<Nb; i++) for(j=0; j<NY; j++) for(k=0; k<NZ; k++)
//                 a->b[p][i][j][k] = c->b[p][j][i][k];
//         }
//     }
// }

} // namespace BK
