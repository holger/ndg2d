#include "pnbFftMatrix.hpp"

#include "logger.hpp"

// #undef LOGLEVEL
// #define LOGLEVEL 4

namespace BK {

// template<>
// PnbFftInfo<float>* PnbFftMatrix<float>::fftC2RStart(PnbFftMatrix<float>* field)
// {
//   ERRORLOGL(3,BK::appendString("PnbFftMatrix<T>::fftC2RStart"));

//   PnbFftInfo<float>* pnbFftInfo = new PnbFftInfo<float>;

//   pnbFftInfo->field = field;

//   //  std::cout << field->get_name() << "  " << *field << std::endl;

//   int NX = this->base_->get_nxR();
//   int NY = this->base_->get_nyR();
//   int NZ = this->base_->get_nzR();

//   pnbFftInfo->NX = NX;
//   pnbFftInfo->NY = NY;
//   pnbFftInfo->NZ = NZ;

//   int howmany_z=NX*NY, N_in_z[1], inembed_z[1];
//   N_in_z[0]=NZ; inembed_z[0]=NZ+2;
//   int istride_z=1 , idist_z=(NZ+2);
//   int onembed_z[1];
//   onembed_z[0]=(NZ/2+1);
//   int ostride_z=1,  odist_z= (NZ/2+1);  

//   int howmany_x =NY*(NZ/2+1), N_in_x[1], inembed_x[1]; 
//   N_in_x[0]=NX; inembed_x[0]=NX;
//   int istride_x=(NZ/2+1)*NY, idist_x=1; 
//   int onembed_x[1]; onembed_x[0]=NX; 
//   int ostride_x=(NZ/2+1)*NY, odist_x=1; 

//   fftwf_plan* p1b = new fftwf_plan;
//   fftwf_plan* p2b = new fftwf_plan;
  
//   pnbFftInfo->p1 = p1b;
//   pnbFftInfo->p2 = p2b;

//   int Nproc = this->base_->mpiBase_->get_commSize();

//   pnbFftInfo->Nproc = Nproc;

//   ERRORLOGL(3,PAR(Nproc));

//   dataType_ = RealData;
//   for(int x=get_loR(0);x<=get_hiR(0);x++) 
//     for(int y=get_loR(1);y<=get_hiR(1);y++) {
//       r(x,y,get_hiR(2)-1) = 0;
//       r(x,y,get_hiR(2)) = 0;
//     }
  
//   ERRORLOGL(3,"creating p1b and p2b ...");
//   *p2b =  fftwf_plan_many_dft(1,N_in_x,howmany_x,reinterpret_cast<fftw_complex*>(this->dataR()),inembed_x,istride_x,idist_x,
// 			   reinterpret_cast<fftw_complex*>(this->dataR()),onembed_x,ostride_x,odist_x,FFTW_BACKWARD,FFTW_ESTIMATE);
//   *p1b = fftwf_plan_many_dft_c2r(1,N_in_z,howmany_z,reinterpret_cast<fftw_complex*>(this->dataR()),onembed_z,ostride_z,odist_z,
// 			       this->dataR(),inembed_z,istride_z,idist_z,FFTW_ESTIMATE);

//   ERRORLOGL(3,"executing p2b ...");
  
//   fftwf_execute(*p2b);
//   transpose_nb(pnbFftInfo);

//   return pnbFftInfo;
// }

// template<>
// void PnbFftMatrix<float>::fftC2RFinish(PnbFftInfo<float>* pnbFftInfo)
// {
//   ERRORLOGL(3,BK::appendString("PnbFftMatrix<T>::fftC2RFinish"));

//   ERRORLOGL(3,"executing p1b and p2b ...");
  
//   ERRORLOGL(3,"transposing non-local blocks ...");
//   int me = this->base_->mpiBase_->get_commRank();

//   for(int p=0; p<pnbFftInfo->Nproc; p++) {
//     if(p != me) {
//       MPI_Wait(pnbFftInfo->Rreq+p,pnbFftInfo->Rstat+p);     
//       MPI_Wait(pnbFftInfo->Sreq+p,pnbFftInfo->Sstat+p);
//       for(int i=0; i<pnbFftInfo->Nb; i++) 
// 	for(int j=0; j<pnbFftInfo->NY; j++) 
// 	  for(int k=0; k<pnbFftInfo->NZ; k++)
// 	    pnbFftInfo->field->b(p,i,j,k) = pnbFftInfo->tempMatrix->b(p,j,i,k);
//     }
//   }

//   delete pnbFftInfo->tempMatrix;

//   fftwf_execute(*(pnbFftInfo->p2));
//   fftwf_execute(*(pnbFftInfo->p1));
//   ERRORLOGL(3,"destroying plans p1b and p2b ...");
  
//   fftwf_destroy_plan(*(pnbFftInfo->p1));fftwf_destroy_plan(*(pnbFftInfo->p2));

//   delete pnbFftInfo;

//   //       for(int x=get_loR(0);x<=get_hiR(0);x++) 
//   // 	for(int y=get_loR(1);y<=get_hiR(1);y++) {
//   // 	  r(x,y,get_hiR(2)-1) = 0;
//   // 	  r(x,y,get_hiR(2)) = 0;
//   // 	}
  
//   //zero(a);

  
//   ERRORLOGL(3,BK::appendString("PnbFftMatrix<T>::fftC2RFinish-end"));
// }  


// template<>
// PnbFftInfo<float>* PnbFftMatrix<float>::fftR2CStart(PnbFftMatrix<float>* field)
// {
//   ERRORLOGL(3,BK::appendString("PnbFftMatrix<T>::fftR2CStart"));

//   PnbFftInfo<float>* pnbFftInfo = new PnbFftInfo<float>;

//   pnbFftInfo->field = field;

//   int NX = this->base_->get_nxR();
//   int NY = this->base_->get_nyR();
//   int NZ = this->base_->get_nzR();

//   pnbFftInfo->NX = NX;
//   pnbFftInfo->NY = NY;
//   pnbFftInfo->NZ = NZ;

//   int howmany_z=NX*NY, N_in_z[1], inembed_z[1];
//   N_in_z[0]=NZ; inembed_z[0]=NZ+2;
//   int istride_z=1 , idist_z=(NZ+2);
//   int onembed_z[1];
//   onembed_z[0]=(NZ/2+1);
//   int ostride_z=1,  odist_z= (NZ/2+1);  

//   int howmany_x =NY*(NZ/2+1), N_in_x[1], inembed_x[1]; 
//   N_in_x[0]=NX; inembed_x[0]=NX;
//   int istride_x=(NZ/2+1)*NY, idist_x=1; 
//   int onembed_x[1]; onembed_x[0]=NX; 
//   int ostride_x=(NZ/2+1)*NY, odist_x=1; 


//   fftwf_plan* p1 = new fftwf_plan;
//   fftwf_plan* p2 = new fftwf_plan;
  
//   pnbFftInfo->p1 = p1;
//   pnbFftInfo->p2 = p2;

//   int Nproc = this->base_->mpiBase_->get_commSize();

//   pnbFftInfo->Nproc = Nproc;

//   ERRORLOGL(3,PAR(Nproc));

//   ERRORLOGL(3,"creating p1 and p2 ...");
//   *p1 = fftwf_plan_many_dft_r2c(1,N_in_z,howmany_z,this->dataR(),inembed_z,istride_z,idist_z,
// 			      reinterpret_cast<fftw_complex*>(this->dataR()),onembed_z,ostride_z,odist_z,FFTW_ESTIMATE);
//   *p2 = fftwf_plan_many_dft(1,N_in_x,howmany_x,reinterpret_cast<fftw_complex*>(this->dataR()),inembed_x,istride_x,idist_x,
// 			  reinterpret_cast<fftw_complex*>(this->dataR()),onembed_x,ostride_x,odist_x,FFTW_FORWARD,FFTW_ESTIMATE);
  
//   ERRORLOGL(3,"executing p1 and p2 ...");
//   fftwf_execute(*p1);
//   fftwf_execute(*p2);
//   transpose_nb(pnbFftInfo);

//   ERRORLOGL(4,BK::appendString("PnbFftMatrix<T>::fftR2CStart-end"));

//   return pnbFftInfo;
// }

// template<>
// void PnbFftMatrix<float>::fftR2CFinish(PnbFftInfo<float>* pnbFftInfo)
// {
//   ERRORLOGL(3,BK::appendString("PnbFftMatrix<T>::fftR2CFinish"));

//   ERRORLOGL(3,"transposing non-local blocks ...");
//   int me = this->base_->mpiBase_->get_commRank();

//   for(int p=0; p<pnbFftInfo->Nproc; p++) {
//     if(p != me) {
//       MPI_Wait(pnbFftInfo->Rreq+p,pnbFftInfo->Rstat+p);     
//       MPI_Wait(pnbFftInfo->Sreq+p,pnbFftInfo->Sstat+p);
//       for(int i=0; i<pnbFftInfo->Nb; i++) 
// 	for(int j=0; j<pnbFftInfo->NY; j++) 
// 	  for(int k=0; k<pnbFftInfo->NZ; k++)
// 	    pnbFftInfo->field->b(p,i,j,k) = pnbFftInfo->tempMatrix->b(p,j,i,k);
//     }
//   }
  
//   ERRORLOGL(3,"executing again p2 ...");
//   fftwf_execute(*(pnbFftInfo->p2));

//   ERRORLOGL(3,"destroying plans p1 and p2 ...");
//   fftwf_destroy_plan(*(pnbFftInfo->p1));
//   fftwf_destroy_plan(*(pnbFftInfo->p2));
// }


// template<>
// void PnbFftMatrix<double>::fftR(int sign, int complete) 
// {
//   ERRORLOGL(3,BK::appendString("PnbFftMatrix<T>::fft(",sign,",",complete,")"));

// //   dataType_ = RealData;

// //   for(int i=0;i<=hiR[0];i++) 
// //     for(int j=0;j<=hiR[1];j++) 
// //       for(int k=0;k<=hiR[2];k++) {
// // 	r(i,j,k) = (i+1)*100+(j+1+base_->mpiBase_->get_commRank()*base_->mpiBase_->get_commSize())*10+k+1;
// //       } 

// //   std::cout << base_->mpiBase_->get_commRank() << "  " << *this << std::endl;

// //   transpose_xy();

// //   std::cout << base_->mpiBase_->get_commRank() << " transposed: " << *this << std::endl;

// //   exit(0);

//   int NX = this->base_->get_nxR();
//   int NY = this->base_->get_nyR();
//   int NZ = this->base_->get_nzR();

//   int howmany_z=NX*NY, N_in_z[1], inembed_z[1];
//   N_in_z[0]=NZ; inembed_z[0]=NZ+2;
//   int istride_z=1 , idist_z=(NZ+2);
//   int onembed_z[1];
//   onembed_z[0]=(NZ/2+1);
//   int ostride_z=1,  odist_z= (NZ/2+1);  
 

//   int howmany_x =NY*(NZ/2+1), N_in_x[1], inembed_x[1]; 
//   N_in_x[0]=NX; inembed_x[0]=NX;
//   int istride_x=(NZ/2+1)*NY, idist_x=1; 
//   int onembed_x[1]; onembed_x[0]=NX; 
//   int ostride_x=(NZ/2+1)*NY, odist_x=1; 


//   fftw_plan  p1, p2, p3, p1b, p2b, p3b;

//   int Nproc = this->base_->mpiBase_->get_commSize();

//   ERRORLOGL(3,PAR(Nproc));

//   if(sign==-1)
//     { //zero(a);
// //       for(int x=get_loR(0);x<=get_hiR(0);x++) 
// // 	for(int y=get_loR(1);y<=get_hiR(1);y++) {
// // 	  r(x,y,get_hiR(2)-1) = 0;
// // 	  r(x,y,get_hiR(2)) = 0;
// // 	}

//       if (Nproc > 1)
// 	{  
// 	  ERRORLOGL(3,"creating p1 and p2 ...");
// 	  p1 = fftw_plan_many_dft_r2c(1,N_in_z,howmany_z,this->dataR(),inembed_z,istride_z,idist_z,
// 				      reinterpret_cast<fftw_complex*>(this->dataR()),onembed_z,ostride_z,odist_z,FFTW_ESTIMATE);
// 	  p2 = fftw_plan_many_dft(1,N_in_x,howmany_x,reinterpret_cast<fftw_complex*>(this->dataR()),inembed_x,istride_x,idist_x,
// 				  reinterpret_cast<fftw_complex*>(this->dataR()),onembed_x,ostride_x,odist_x,FFTW_FORWARD,FFTW_ESTIMATE);

// 	  ERRORLOGL(3,"executing p1 and p2 ...");
// 	  fftw_execute(p1);
// 	  fftw_execute(p2);
//  	  transpose_xy();
// 	  ERRORLOGL(3,"executing again p2 ...");
// 	  fftw_execute(p2);
// 	  if(complete)transpose_xy();
// 	  ERRORLOGL(3,"destroying plans p1 and p2 ...");
// 	  fftw_destroy_plan(p1);fftw_destroy_plan(p2);
// 	}
//       else 
// 	{
// 	  ERRORLOGL(3,"creating plan for r2c for one processor ...");
// 	  p3 = fftw_plan_dft_r2c_3d(NX, NY, NZ, this->dataR(), reinterpret_cast<fftw_complex*>(this->dataR()) ,FFTW_ESTIMATE);
	  
// 	  ERRORLOGL(3,"executing plan for r2c for one processor ...");
// 	  std::cerr << reinterpret_cast<fftw_complex*>(this->dataR())[0][0] << "  " 
// 		    << reinterpret_cast<fftw_complex*>(this->dataR())[0][1] << "  " 
// 		    << reinterpret_cast<fftw_complex*>(this->dataR())[1][0] << "  "
// 		    << reinterpret_cast<fftw_complex*>(this->dataR())[1][1] << std::endl;
// 	  fftw_execute(p3); 
// 	  fftw_destroy_plan(p3);
// 	}
//       (*this)*=this->base_->get_scale(); 
//       //zero_kmax(a);
//     }

//   if(sign==1)
//     { //zero_kmax(a);
      
//       dataType_ = RealData;
//       for(int x=get_loR(0);x<=get_hiR(0);x++) 
// 	for(int y=get_loR(1);y<=get_hiR(1);y++) {
// 	  r(x,y,get_hiR(2)-1) = 0;
// 	  r(x,y,get_hiR(2)) = 0;
// 	}

//       if (Nproc > 1 )
// 	{
// 	  ERRORLOGL(3,"creating p1b and p2b ...");
// 	  p2b=  fftw_plan_many_dft(1,N_in_x,howmany_x,reinterpret_cast<fftw_complex*>(this->dataR()),inembed_x,istride_x,idist_x,
// 				   reinterpret_cast<fftw_complex*>(this->dataR()),onembed_x,ostride_x,odist_x,FFTW_BACKWARD,FFTW_ESTIMATE);
// 	  p1b = fftw_plan_many_dft_c2r(1,N_in_z,howmany_z,reinterpret_cast<fftw_complex*>(this->dataR()),onembed_z,ostride_z,odist_z,
// 				       this->dataR(),inembed_z,istride_z,idist_z,FFTW_ESTIMATE);
// 	  if(complete)transpose_xy();
// 	  ERRORLOGL(3,"executing p2b ...");

// 	  fftw_execute(p2b);
// 	  transpose_xy();
// 	  ERRORLOGL(3,"executing p1b and p2b ...");

// 	  fftw_execute(p2b);
// 	  fftw_execute(p1b);
// 	  ERRORLOGL(3,"destroying plans p1b and p2b ...");

// 	  fftw_destroy_plan(p1b);fftw_destroy_plan(p2b);
// 	}
//       else 
// 	{
// 	  p3b = fftw_plan_dft_c2r_3d(NX, NY, NZ,reinterpret_cast<fftw_complex*>(this->dataR()) , this->dataR() ,FFTW_ESTIMATE);    
// 	  fftw_execute(p3b);
// 	  fftw_destroy_plan(p3b);
// 	}
// //       for(int x=get_loR(0);x<=get_hiR(0);x++) 
// // 	for(int y=get_loR(1);y<=get_hiR(1);y++) {
// // 	  r(x,y,get_hiR(2)-1) = 0;
// // 	  r(x,y,get_hiR(2)) = 0;
// // 	}
      
//       //zero(a);
//     }

//   ERRORLOGL(4,"PnbFftMatrix<T>::fft-end");
// }

// template<>
// void PnbFftMatrix<float>::fftR(int sign, int complete) 
// {
//   ERRORLOGL(3,"PnbFftMatrix<T>::fft");

//   int NX = this->base_->get_nxR();
//   int NY = this->base_->get_nyR();
//   int NZ = this->base_->get_nzR();

//   int howmany_z=NX*NY, N_in_z[1], inembed_z[1];
//   N_in_z[0]=NZ; inembed_z[0]=NZ+2;
//   int istride_z=1 , idist_z=(NZ+2);
//   int onembed_z[1];
//   onembed_z[0]=(NZ/2+1);
//   int ostride_z=1,  odist_z= (NZ/2+1);  
 

//   int howmany_x =NY*(NZ/2+1), N_in_x[1], inembed_x[1]; 
//   N_in_x[0]=NX; inembed_x[0]=NX;
//   int istride_x=(NZ/2+1)*NY, idist_x=1; 
//   int onembed_x[1]; onembed_x[0]=NX; 
//   int ostride_x=(NZ/2+1)*NY, odist_x=1; 


//   fftwf_plan  p1, p2, p3, p1b, p2b, p3b;

//   int Nproc = this->base_->mpiBase_->get_commSize();

//   if(sign==-1)
//     { //zero(a);
//       if (Nproc > 1)
// 	{  
// 	  p1 = fftwf_plan_many_dft_r2c(1,N_in_z,howmany_z,this->dataR(),inembed_z,istride_z,idist_z,
// 				      reinterpret_cast<fftw_complex*>(this->dataR()),onembed_z,ostride_z,odist_z,FFTW_ESTIMATE);
// 	  p2 = fftwf_plan_many_dft(1,N_in_x,howmany_x,reinterpret_cast<fftw_complex*>(this->dataR()),inembed_x,istride_x,idist_x,
// 				  reinterpret_cast<fftw_complex*>(this->dataR()),onembed_x,ostride_x,odist_x,FFTW_FORWARD,FFTW_ESTIMATE);

// 	  fftwf_execute(p1);
// 	  fftwf_execute(p2);
// 	  transpose_xy();
// 	  fftwf_execute(p2);
// 	  if(complete)transpose_xy();
// 	  fftwf_destroy_plan(p1);fftwf_destroy_plan(p2);
// 	}
//       else 
// 	{
// 	  ERRORLOGL(3,"creating planf for r2c for one processor ...");
	  
// 	  p3 = fftwf_plan_dft_r2c_3d(NX, NY, NZ, this->dataR(), reinterpret_cast<fftw_complex*>(this->dataR()) ,FFTW_ESTIMATE);

// 	  ERRORLOGL(3,"executing planf for r2c for one processor ...");

// 	  fftwf_execute(p3); 
// 	  fftwf_destroy_plan(p3);
// 	}
//       (*this)*=this->base_->get_scale(); 
//       //zero_kmax(a);
//     }

//   if(sign==1)
//     { //zero_kmax(a);
//       if (Nproc > 1 )
// 	{
// 	  p2b=  fftwf_plan_many_dft(1,N_in_x,howmany_x,reinterpret_cast<fftw_complex*>(this->dataR()),inembed_x,istride_x,idist_x,
// 				   reinterpret_cast<fftw_complex*>(this->dataR()),onembed_x,ostride_x,odist_x,FFTW_BACKWARD,FFTW_ESTIMATE);
// 	  p1b = fftwf_plan_many_dft_c2r(1,N_in_z,howmany_z,reinterpret_cast<fftw_complex*>(this->dataR()),onembed_z,ostride_z,odist_z,
// 				       this->dataR(),inembed_z,istride_z,idist_z,FFTW_ESTIMATE);
// 	  if(complete)transpose_xy();
// 	  fftwf_execute(p2b);
// 	  transpose_xy();
// 	  fftwf_execute(p2b);
// 	  fftwf_execute(p1b);
// 	  fftwf_destroy_plan(p1b);fftwf_destroy_plan(p2b);
// 	}
//       else 
// 	{
// 	  p3b = fftwf_plan_dft_c2r_3d(NX, NY, NZ,reinterpret_cast<fftw_complex*>(this->dataR()) , this->dataR() ,FFTW_ESTIMATE);    
// 	  fftwf_execute(p3b);
// 	  fftwf_destroy_plan(p3b);
// 	}
//       //zero(a);
//     }

//   ERRORLOGL(4,"PnbFftMatrix<T>::fft-end");
// }

}
