#ifndef dataType_h
#define dataType_h

namespace BK {

enum DataType {RealData, ComplexData};

// --------------------------------------------------------------------

//enum FftwOrder {FftwNormalOrder, FftwTransposedOrder};

} // namespace BK

#endif
