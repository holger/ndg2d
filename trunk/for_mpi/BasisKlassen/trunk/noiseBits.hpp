#ifndef noiseBits_h
#define noiseBits_h
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

// http://en.wikipedia.org/wiki/IEEE_754
// 1.-2. Byte: 16 bit Mantisse (LSB)
// 3. Byte: Exp im obersten bit, dann 7 bit mantisse (MSB)
// 4. Byte: sign + 7 bit exp (biased)

namespace BK{
  
template<class T>
inline void set3bit(T* f)
{
  char* m=(char*) f;
  (*m) &= 0x00; m++;
  (*m) &= 0x00; m++;
  (*m) &= 0xf0;
}

template<class T>
inline void set10bit(T* f)
{
  char* m=(char*) f;
  (*m) &= 0x00; m++;
  (*m) &= 0xE0;
}

template<class T>
inline void set12bit(T* f)
{
  char* m=(char*) f;
  (*m) &= 0x00; m++;
  (*m) &= 0xF8;
}

template<class T>
inline void set14bit(T* f)
{
  char* m=(char*) f;
  (*m) &= 0x00; m++;
  (*m) &= 0xFE;
}

template<class T>
inline void set16bit(T* f)
{
  char* m=(char*) f;
  (*m) &= 0x80;
}

template<class T>
inline void set17bit(T* f)
{
  char* m=(char*) f;
  (*m) &= 0xC0;
}

template<class T>
inline void set18bit(T* f)
{
  char* m=(char*) f;
  (*m) &= 0xE0;
}

template<class T>
inline void set20bit(T* f)
{
  char* m=(char*) f;
  (*m) &= 0xf8;
}

}
// int main()
// {

//   float a=M_PI;
//   float b=a;

//   set20bit(&a);

//   printf("Wert: %20.14f  diff: %e\n", a, a-b);

//   exit(0);
// }



#endif
