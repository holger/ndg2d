#ifndef CURLV_HPP
#define CURLV_HPP

#include "symmetryMatrix.hpp"

/** @file vel_rot.hpp
 * @brief velocity curl method
 *
 * Contains specialized implementation for
 * computing the curl of the velocity field.
 *
 * Sine the symmetries of both omega and v
 * are static, we just implement the curl
 * operation
 */

/** @brief curl of v
 *
 * Compute curl v out of place, assume
 * Fourier space v.
 * In debug mode assert:
 *
 * - all rh{xyz} args are in Fourier space
 *
 * - all boundaries match
 *
 * - all rh{xyz} have velocity type symmetry
 *
 * The result is written into lh{xyz}.
 *
 * The symmetry is as follows:
 *
 * wx (eoo) <---> vx(oee)
 *
 * wy (oeo) <---> vy(eoe)
 *
 * wx (ooe) <---> vz(eeo)
 *
 * wx = dy vz - dz vy
 */
namespace BK
{
    void symmetry_curl_v(Matrix lhx, Matrix lhy, Matrix lhz,
			 Matrix rhx, Matrix rhy, Matrix rhz)
    {
	// FIXME sanity check

	assert(rhx.get_symmetry() == VX);
	assert(rhy.get_symmetry() == VY);
	assert(rhz.get_symmetry() == VZ);

	// end of sanity check

	unsigned int UPPER_X = rhx.base_->leftCoord_;
	unsigned int LOWER_X = rhx.base_->rightCoord_;
	unsigned int UPPER_Y = rhx.base_->topCoord_;
	unsigned int LOWER_Y = rhx.base_->bottomCoord_;

	unsigned int HERE_X    = rhx.base_->myCoordX_;
	unsigned int HERE_Y    = rhx.base_->myCoordY_;

	unsigned int SIZE_X    = rhx.base_->get_processorGridDims(0);
	unsigned int SIZE_Y    = rhx.base_->get_processorGridDims(1);

	unsigned int sx = rhx.base_->get_nxR(); 
	unsigned int sy = rhx.base_->get_nyR(); 
	unsigned int sz = rhx.base_->get_nzR(); 


// //////////////////////////////////////////////////////////////
// ///////////////   MAIN CURL BODY  ////////////////////////////
// //////////////////////////////////////////////////////////////


// /////////////////   X-COMPONENT  //////////////////////////////
	// wx is odd in y-direction, vz even, therefore shift up
	// FIX: order of iterators
	for(int i = rhz.get_loC(0); i < rhz.get_hiC(0); ++i)
	    for(int j = rhz.get_loC(1) + 1; j < rhz.get_hiC(1); ++j)
		for(int j = rhz.get_loC(2); k < rhz.get_hiC(2); ++k)
		    lhx(i,j,k)  = -(ky(j)+1)*rhz(i,j+1,k);

	if( HERE_Y != 0 ) // y--direction
	{

	    int size = sx*sz;
	    T* send = new T[size];

	    // wz = dx uy - (...)
	    // shift of k is implicitly done here ... and correct
	    for(int i = rhz.get_loC(0); i < rhz.get_hiC(0); ++i)
		for(int j = rhz.get_loC(2); j < rhz.get_hiC(2); ++j)
		    send[i+rhz.sx*j] = ky(rhz.get_loC(1))*rhz(i, rhz.get_loC(1), j);

	    MPI_Send(send, size, MPI_DOUBLE, LOWER_X, MPI_ANY_TAG, MPI_COMM_WORLD);
	    delete[] send;
	}

	if(HERE_Y != SIZE_Y - 1) // y--direction
	{
	    int size = sx*sz;
	    T* recv = new T[size];
	
	    MPI_Recv(recv, size, MPI_DOUBLE, UPPER_X, MPI_ANY_TAG, MPI_COMM_WORLD);
	
	    for(int i = rhz.get_loC(0); i < rhz.get_hiC(0); ++i)
		for(int j = rhz.get_loC(2); j < rhz.get_hiC(2); ++j)
		    lhx(i, rhz.get_hiC(1), j) = -recv[i+sx*j];

	    delete[] recv;
	}
	else
	{
	    for(int i = rhz.get_loC(0); i < rhz.get_hiC(0); ++i)
		for(int j = rhz.get_loC(2); j < rhz.get_hiC(2); ++j)
		    lhx(i, rhz.get_hiC(1), j) = 0.0;
	}

	// FIX: order of iterators
	for(int i = rhz.get_loC(0); i < rhz.get_hiC(0); ++i)
	    for(int j = rhz.get_loC(1); j < rhz.get_hiC(1); ++j)
		for(int j = rhz.get_loC(2) + 1; k < rhz.get_hiC(2); ++k)
		    lhx(i,j,k) +=  (kz(k)+1)*rhy(i,j,k+1);

	// z-direction is always serial
	// and since we would add zeroes *shrug*

// /////////////////   Y-COMPONENT  //////////////////////////////

	// FIX: order of iterators
	for(int i = rhz.get_loC(0); i < rhz.get_hiC(0); ++i)
	    for(int j = rhz.get_loC(1); j < rhz.get_hiC(1); ++j)
		for(int j = rhz.get_loC(2) + 1; k < rhz.get_hiC(2); ++k)
		    lhy(i,j,k)  = -(kz(j)+1)*rhx(i,j,k+1);

	// z is never parallel
	for(int i = rhz.get_loC(0); i < rhz.get_hiC(0); ++i)
	    for(int j = rhz.get_loC(1); j < rhz.get_hiC(1); ++j)
		lhy(i,j,rhz.get_hiC(0)) = 0.0;    
    
	// FIX: order of iterators
	for(int i = rhz.get_loC(0) + 1; i < rhz.get_hiC(0); ++i)
	    for(int j = rhz.get_loC(1); j < rhz.get_hiC(1); ++j)
		for(int j = rhz.get_loC(2); k < rhz.get_hiC(2); ++k)
		    lhy(i,j,k) +=  (kx(j)+1)*rhz(i+1,j,k);

	// do MPI in  x--direction
	if(HERE_X != 0)
	{
	    int size = sy*sz;
	    T* send = new T[size]; 
 
	    // wy = (...) - dx uz
	    for(int i = rhz.get_loC(1); i < rhz.get_hiC(1); ++i)
		for(int j = rhz.get_loC(2); j < rhz.get_hiC(2); ++j)
		    send[i+rhz.sy*j] = kx(rhz.get_loC(0))*rhz(rhz.get_loC(0), i, j);

	    MPI_Send(send, size, MPI_DOUBLE, LOWER_X, MPI_ANY_TAG, MPI_COMM_WORLD);
   
	}
	// do MPI in  x--direction
	if(HERE_X != SIZE_X - 1 )
	{
	    int size = sy*sz;
	    T*  recv = new T[size]; 

	    MPI_Recv(recv, size, MPI_DOUBLE, UPPER_X, MPI_ANY_TAG, MPI_COMM_WORLD);

	    for(int i = rhz.get_loC(1); i < rhz.get_hiC(1); ++i)
		for(int j = rhz.get_loC(2); j < rhz.get_hiC(2); ++j)
		    lhy(rhz.get_hiC(0), i, j) += recv[i+rhz.sy*j];    
	}
	else
	{
	    // don't add zeroes
	}



// /////////////////   Z-COMPONENT  //////////////////////////////

	// FIX: order of iterators
	for(int i = rhz.get_loC(0) + 1; i < rhz.get_hiC(0); ++i)
	    for(int j = rhz.get_loC(1); j < rhz.get_hiC(1); ++j)
		for(int j = rhz.get_loC(2); k < rhz.get_hiC(2); ++k)
		    lhz(i,j,k)  = -(kx(j)+1)*rhy(i+1,j,k);

	if( HERE_X != 0) // x--direction
	{
	    int size = sy*sz;
	    T* send = new T[size];

	    // wz = dx uy - (...)
	    // shift of k is implicitly done here ... and correct
	    for(int i = rhz.get_loC(1); i < rhz.get_hiC(1); ++i)
		for(int j = rhz.get_loC(2); j < rhz.get_hiC(2); ++j)
		    send[i+rhz.sy*j] = kx(rhz.get_loC(0))*rhy(rhz.get_loC(0), i, j);

	    MPI_Send(send, size, MPI_DOUBLE, LOWER_X, MPI_ANY_TAG,MPI_COMM_WORLD);


	    delete[] send;
	}
	if(HERE_X != SIZE_X - 1) // x--direction
	{
	    int size = sy*sz;
	    T* recv = new T[size];

	    MPI_Recv(recv, size, MPI_DOUBLE, UPPER_X, MPI_ANY_TAG, MPI_COMM_WORLD);

	    for(int i = rhz.get_loC(1); i < rhz.get_hiC(1); ++i)
		for(int j = rhz.get_loC(2); j < rhz.get_hiC(2); ++j)
		    lhz(rhz.get_hiC(0), i, j) = -recv[i+rhz.sy*j];

	    delete[] recv;
	}
	else
	{
	    for(int i = rhz.get_loC(1); i < rhz.get_hiC(1); ++i)
		for(int j = rhz.get_loC(2); j < rhz.get_hiC(2); ++j)
		    lhz(rhz.get_hiC(0), i, j) = 0.0;
	}

	// FIX: order of iterators
	for(int i = rhz.get_loC(0); i < rhz.get_hiC(0); ++i)
	    for(int j = rhz.get_loC(1) + 1; j < rhz.get_hiC(1); ++j)
		for(int j = rhz.get_loC(2); k < rhz.get_hiC(2); ++k)
		    lhz(i,j,k) +=  (ky(j)+1)*rhx(i,j+1,k);

	if(HERE_Y != 0) // y--direction
	{
	    int size = sx*sz;
	    T* send = new T[size];

	    // wz = dx uy - (...)
	    // shift of k is implicitly done here ... and correct
	    for(int i = rhz.get_loC(0); i < rhz.get_hiC(0); ++i)
		for(int j = rhz.get_loC(2); j < rhz.get_hiC(2); ++j)
		    send[i+rhz.sx*j] = ky(rhz.get_loC(1))*rhx(i, rhz.get_loC(1), j);

	    MPI_Recv(send, size, MPI_DOUBLE, LOWER_Y, MPI_ANY_TAG, MPI_COMM_WORLD);


	    delete[] send;
	}
	if(HERE_Y != SIZE_Y - 1) // y--direction
	{
	    int size = sx*sz;
	    T* recv = new T[size];

	    MPI_Recv(recv, size, MPI_DOUBLE, UPPER_Y, MPI_ANY_TAG, MPI_COMM_WORLD);

	    for(int i = rhz.get_loC(0); i < rhz.get_hiC(0); ++i)
		for(int j = rhz.get_loC(2); j < rhz.get_hiC(2); ++j)
		    lhz(i, rhz.get_hiC(1), j) += recv[i+rhz.sx*j];

	    delete[] recv;
	}
	else
	{
	    // don't add zero
	}

	// set symmetries
	lhx.set_symmetry(VX);
	lhy.set_symmetry(VY);
	lhz.set_symmetry(VZ);
    }

// /////////////////////// END OF CURL //////////////////////////////////////////

/** @brief Compute the inverse of curl V
 *
 * Since div V = 0:
 *     curl curl V = lapl V
 * <=> curl W      = lapl V
 * <=> inv_lapl W  = V
 * 
 * Since we know the inverse Laplacian in Fourierspace, 
 * we may compute V from W.
 * 
 * This method is applied to vorticity type symmetry only, 
 * therefore only this case is implemented
 *
 * The symmetry is as follows:
 *
 * wx (eoo) <---> vx(oee)
 *
 * wy (oeo) <---> vx(eoe)
 *
 * wx (ooe) <---> vx(eeo)
 *
 * All components constitute the cos --> sin type.
 */

    void symmetry_inv_curl_w(Matrix lhx, Matrix lhy, Matrix lhz,
			     Matrix rhx, Matrix rhy, Matrix rhz)
    {
	// FIXME: sanity check

	assert(rhx.get_symmetry() == WX);
	assert(rhy.get_symmetry() == WY);
	assert(rhz.get_symmetry() == WZ);

	// end of sanity check

	unsigned int UPPER_X = rhx.base_->leftCoord_;
	unsigned int LOWER_X = rhx.base_->rightCoord_;
	unsigned int UPPER_Y = rhx.base_->topCoord_;
	unsigned int LOWER_Y = rhx.base_->bottomCoord_;

	unsigned int HERE_X    = rhx.base_->myCoordX_;
	unsigned int HERE_Y    = rhx.base_->myCoordY_;    

	unsigned int SIZE_X    = rhx.base_->get_processorGridDims(0);
	unsigned int SIZE_Y    = rhx.base_->get_processorGridDims(1);

	unsigned int sx = rhx.base_->get_nxR(); 
	unsigned int sy = rhx.base_->get_nyR(); 
	unsigned int sz = rhx.base_->get_nzR();

// //////////////////////////////////////////////////////////////
// ///////////////   MAIN CURL INV BODY  ////////////////////////
// //////////////////////////////////////////////////////////////


// /////////////////   X-COMPONENT  //////////////////////////////
	// FIX: order of iterators
	for(int i = rhz.get_loC(0); i < rhz.get_hiC(0); ++i)
	    for(int j = rhz.get_loC(1); j < rhz.get_hiC(1) - 1; ++j)
		for(int j = rhz.get_loC(2); k < rhz.get_hiC(2); ++k)
		    lhx(i,j,k)  = ky(j)*rhz(i,j+1,k)/k2(i,j,k);

	if(HERE_Y != SIZE_Y-1) // y--direction
	{
	    int size = sx*sz;
	    T* send = new T[size];

	    // wz = dx uy - (...)
	    for(int i = rhz.get_loC(0); i < rhz.get_hiC(0); ++i)
		for(int j = rhz.get_loC(2); j < rhz.get_hiC(2); ++j)
		    send[i+rhz.sx*j] = rhz(i, rhz.get_hiC(1), j);

	    MPI_Send(send, size, MPI_DOUBLE, UPPER_Y, MPI_ANY_TAG, MPI_COMM_WORLD);

	    delete[] send;
	}  
	if(HERE_Y != 0) // y--direction
	{
	    int size = sx*sz;
	    T* recv = new T[size];

	    MPI_Sendrecv(recv, size, MPI_DOUBLE, LOWER_Y, MPI_ANY_TAG, MPI_COMM_WORLD);

	    for(int i = rhz.get_loC(0); i < rhz.get_hiC(0); ++i)
		for(int j = rhz.get_loC(2); j < rhz.get_hiC(2); ++j)
		    lhx(i, rhz.get_loC(1), j) = ky(rhz.get_loC(1))*recv[i+rhz.sx*j]/k2(i,rhz.get_loC(1),k);

	    delete[] recv;
	}
	else
	{
	    for(int i = rhz.get_loC(0); i < rhz.get_hiC(0); ++i)
		for(int j = rhz.get_loC(2); j < rhz.get_hiC(2); ++j)
		    lhx(i, rhz.get_loC(1), j) = 0.0;
	}

	// FIX: order of iterators
	for(int i = rhz.get_loC(0); i < rhz.get_hiC(0); ++i)
	    for(int j = rhz.get_loC(1); j < rhz.get_hiC(1); ++j)
		for(int j = rhz.get_loC(2); k < rhz.get_hiC(2)-1; ++k)
		    lhx(i,j,k) -=  kz(k)*rhy(i,j,k-1)/k2(i,j,k);


// /////////////////   Y-COMPONENT  //////////////////////////////

	// FIX: order of iterators
	for(int i = rhz.get_loC(0); i < rhz.get_hiC(0); ++i)
	    for(int j = rhz.get_loC(1); j < rhz.get_hiC(1); ++j)
		for(int j = rhz.get_loC(2); k < rhz.get_hiC(2) - 1; ++k)
		    lhy(i,j,k)  = kz(j)*rhx(i,j,k-1)/k2(i,j,k);

	// z is never parallel
	for(int i = rhz.get_loC(1); i < rhz.get_hiC(1); ++i)
	    for(int j = rhz.get_loC(2); j < rhz.get_hiC(2); ++j)
		lhy(rhz.get_loC(0), i, j) = kz(rhz.get_loC(2))*recv[i + sx*j]/k2(i,j,rhz.get_loC(2));    
	
	
	// FIX: order of iterators
	for(int i = rhz.get_loC(0); i < rhz.get_hiC(0) - 1; ++i)
	    for(int j = rhz.get_loC(1); j < rhz.get_hiC(1); ++j)
		for(int j = rhz.get_loC(2); k < rhz.get_hiC(2); ++k)
		    lhy(i,j,k) -=  kx(j)*rhz(i-1,j,k)/k2(i,j,k);

	// do MPI in  x--direction
	if(HERE_X != SIZE_X-1)
	{
	    int size = sy*sz;
	    T* send = new T[size]; 
 
	    // wy = (...) - dx uz
	    for(int i = rhz.get_loC(1); i < rhz.get_hiC(1); ++i)
		for(int j = rhz.get_loC(2); j < rhz.get_hiC(2); ++j)
		    send[i+rhz.sy*j] = rhz(rhz.get_hiC(0), i, j);

	    MPI_Send(send, size, MPI_DOUBLE, UPPER_X, MPI_ANY_TAG,MPI_COMM_WORLD);

	    delete[] send;
   
	}
	// do MPI in  x--direction
	if(HERE_X != 0)
	{
	    int size = sy*sz;
	    T* recv = new T[size];

	    MPI_Recv(recv, size, MPI_DOUBLE, LOWER_X, MPI_ANY_TAG,MPI_COMM_WORLD);

	    for(int i = rhz.get_loC(1); i < rhz.get_hiC(1); ++i)
		for(int j = rhz.get_loC(2); j < rhz.get_hiC(2); ++j)
		    lhy(rhz.get_loC(0), i, j) -= kx(rhz.get_loC(0),i,j)*recv[i+rhz.sy*j]/k2(rhz.get_loC(0),i,j);

	    delete[] recv;
	}
	else
	{
	    // why should we add zero
	}


// /////////////////   Z-COMPONENT  //////////////////////////////

	// FIX: order of iterators
	for(int i = rhz.get_loC(0); i < rhz.get_hiC(0) - 1; ++i)
	    for(int j = rhz.get_loC(1); j < rhz.get_hiC(1); ++j)
		for(int j = rhz.get_loC(2); k < rhz.get_hiC(2); ++k)
		    lhz(i,j,k)  = (kx(j)-1)*rhy(i-1,j,k)/k2(i,j,k);

	if(HERE_X != SIZE_X-1) // x--direction
	{
	    int size = sy*sz;
	    T* recv = new T[size];
	    T* send = new T[size];

	    // wz = dx uy - (...)
	    // shift of k is implicitly done here ... and correct
	    for(int i = rhz.get_loC(1); i < rhz.get_hiC(1); ++i)
		for(int j = rhz.get_loC(2); j < rhz.get_hiC(2); ++j)
		    send[i+rhz.sy*j] = rhy(rhz.get_hiC(0), i, j);

	    MPI_Send(send, size, MPI_DOUBLE, UPPER_X, MPI_ANY_TAG,MPI_COMM_WORLD);

	    delete[] send;
	}
	if(HERE_X != 0) // x--direction
	{
	    int size = sy*sz;
	    T* recv = new T[size];

	    MPI_Recv(recv, size, MPI_DOUBLE, LOWER_X, MPI_ANY_TAG,MPI_COMM_WORLD);

	    for(int i = rhz.get_loC(1); i < rhz.get_hiC(1); ++i)
		for(int j = rhz.get_loC(2); j < rhz.get_hiC(2); ++j)
		    lhz(rhz.get_loC(0), i, j) = kx(rhz.get_loC(0),i,j)*recv[i+rhz.sy*j]/k2(rhz.get_loC(0),i,j);

	    delete[] recv;
	}
	else
	{
	    for(int i = rhz.get_loC(1); i < rhz.get_hiC(1); ++i)
		for(int j = rhz.get_loC(2); j < rhz.get_hiC(2); ++j)
		    lhz(rhz.get_loC(0), i, j) = 0.0;
	}

	// FIX: order of iterators
	for(int i = rhz.get_loC(0); i < rhz.get_hiC(0); ++i)
	    for(int j = rhz.get_loC(1); j < rhz.get_hiC(1) - 1; ++j)
		for(int j = rhz.get_loC(2); k < rhz.get_hiC(2); ++k)
		    lhz(i,j,k) -=  ky(j)*rhx(i,j-1,k)/k2(i,j,k);

	if(HERE_Y != SIZE_X-1) // y--direction
	{
	    int size = sx*sz;
	    T* recv = new T[size];
	    T* send = new T[size];

	    // wz = dx uy - (...)
	    // shift of k is implicitly done here ... and correct
	    for(int i = rhz.get_loC(0); i < rhz.get_hiC(0); ++i)
		for(int j = rhz.get_loC(2); j < rhz.get_hiC(2); ++j)
		    send[i+rhz.sx*j] = ky(rhz.get_hiC(1)-1)*rhx(i, rhz.get_hiC(1), j);

	    MPI_Send(send, size, MPI_DOUBLE, UPPER_Y, MPI_ANY_TAG,MPI_COMM_WORLD);

	    delete[] send;
	}
	if(HERE_Y != 0) // y--direction
	{
	    int size = sx*sz;
	    T* recv = new T[size];

	    MPI_Recv(recv, size, MPI_DOUBLE, LOWER_Y, MPI_ANY_TAG,MPI_COMM_WORLD);

	    for(int i = rhz.get_loC(0); i < rhz.get_hiC(0); ++i)
		for(int j = rhz.get_loC(2); j < rhz.get_hiC(2); ++j)
		    lhz(i, rhz.get_loC(1), j) -= ky(i,rhz.get_loC(1),j)*recv[i+rhz.sx*j]/k2(i,rhz.get_loC(1),j);

	    delete[] recv;
	}
	else
	{
	    //adding zeroes
	}
    }
    // set symmetries
    lhx.set_symmetry(WX);
    lhy.set_symmetry(WY);
    lhz.set_symmetry(WZ);

}

void symmetry_convolution((Matrix lhx, Matrix lhy, Matrix lhz,
			   Matrix rhx, Matrix rhy, Matrix rhz)
{
    // FIXME: sanity check

    // end

   	for(int i = rhz.get_loC(0); i < rhz.get_hiC(0); ++i)
	    for(int j = rhz.get_loC(1); j < rhz.get_hiC(1); ++j)
		for(int j = rhz.get_loC(2); k < rhz.get_hiC(2); ++k)
		{
		    lhx(i,j,k) *= rhx(i,j,k);
		    lhy(i,j,k) *= rhy(i,j,k);
		    lhz(i,j,k) *= rhz(i,j,k);
		}

	lhx.fftR2C();
	lhy.fftR2C();
	lhz.fftR2C();
}

#endif

