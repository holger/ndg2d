#ifndef particleClass_hpp
#define particleClass_hpp

#include <iostream>
#include <cmath>

#include "HierarchyGenerators.hpp"
#include "tinyVector.hpp"
#include "inheritor.hpp"

namespace BK {

class TrajectoryFlag
{
 public:
  TrajectoryFlag() {
    trajectoryFlag = false;
  }
  bool trajectoryFlag;

  TrajectoryFlag(const TrajectoryFlag& tmp) {trajectoryFlag = tmp.trajectoryFlag;}
  TrajectoryFlag operator=(const TrajectoryFlag& tmp) {trajectoryFlag = tmp.trajectoryFlag; return *this;}
};

class Identity
{
public:
  Identity() {number = 0;}
  int number;
};

class Density
{
public:
  Density() {density = 1;}
  double density;
};

// struct MajorGlobalNumber
// {
//   int majorGlobalNumber;
// };

// struct SurfaceNumber
// {
//   int surfaceNumber;
// };

class MajorIdentity
{
public:
  MajorIdentity() {majorNumber = 0;}
  int majorNumber;
};

template<class T, int SPACEDIM, int SIZE>
class HistoryVelo
{
public:
  enum { size = SIZE};

  HistoryVelo() { 
    for(int coord = 0;coord<SPACEDIM;coord++) {
      historySum[coord] = 0;
      for(size_t i=0;i<SIZE;i++)
	veloHist[coord][i] = 0;
    }
  }
  
  T historySum[SPACEDIM];  
  
  T veloHist[SPACEDIM][SIZE];
};

template<class T, int SPACEDIM>
class MajorRadiusVec
{
 public:
  MajorRadiusVec() { for(int i=0;i<SPACEDIM;i++) majorRadiusVec[i] = 0; }
  BK::TinyVector<T,SPACEDIM> majorRadiusVec;
};

template<class T>
class MajorRadius
{
 public:
  MajorRadius() {majorRadius = 0;}

  T majorRadius;
};

template<class T>
class Radius
{
 public:
  Radius() {radius = 0;}
  T radius;
};

template<class T, int SPACEDIM>
class PressureVec
{
 public:
  PressureVec() { for(int i=0;i<SPACEDIM;i++) pressureVec[i] = 0; }

  // space coordinates
  BK::TinyVector<T,SPACEDIM> pressureVec;
};

template<class T>
class Pressure
{
 public:
  Pressure() { pressure = 0; }

  T pressure;
};


template<class T>
class Mass
{
 public:
  Mass() { mass = 0; }
  T mass;
};

template<class T>
class Charge
{
 public:
  Charge() { charge = 0; }
  
  T charge;
};

template<class T>
class Color
{
 public:
  T color;
};

template<class T>
class StokesNumber
{
 public:
  StokesNumber() { stokesNumber = 0; }

  T stokesNumber;
};

class Step
{
public:
  int step;
};

template<class T, int SPACEDIM>
class Acceleration
{
 public:
  Acceleration() { for(int i=0;i<SPACEDIM;i++) a[i] = 0; }

  BK::TinyVector<T,SPACEDIM> a;
};

template<class T, int SPACEDIM>
class Velocity
{
 public:
  Velocity() { for(int i=0;i<SPACEDIM;i++) v[i] = 0; }

  BK::TinyVector<T,SPACEDIM> v;
};

template<class T, int SPACEDIM>
class FieldVelocity
{
 public:
  FieldVelocity() {

    for(int coord = 0;coord < SPACEDIM;coord++)
      u[coord] = 0;

  }
  
  BK::TinyVector<T,SPACEDIM> u;
};

template<class T, int SPACEDIM>
class FieldVelocityOld
{
 public:
  FieldVelocityOld() {

  for(int coord = 0;coord < SPACEDIM;coord++)
    uOld[coord] = 0;
  }

  BK::TinyVector<T,SPACEDIM> uOld;
};

template<class T, int SPACEDIM>
class Vorticity
{
 public:
  Vorticity() { for(int i=0;i<SPACEDIM;i++) w[i] = 0; }

  BK::TinyVector<T,SPACEDIM> w;
};

template<class T, int SPACEDIM>
class Force
{
 public:
  Force() { for(int i=0;i<SPACEDIM;i++) f[i] = 0; }

  BK::TinyVector<T,SPACEDIM> f;
};

template<class T, int SPACEDIM>
class ExternalForce
{
 public:
  ExternalForce() { for(int i=0;i<SPACEDIM;i++) {F[i] = 0; FSum[i] = 0;}}

  BK::TinyVector<T,SPACEDIM> F;
  BK::TinyVector<T,SPACEDIM> FSum;
};
 
template<class T, int SPACEDIM>
class MagneticField
{
 public:
  MagneticField() { for(int i=0;i<SPACEDIM;i++) b[i] = 0; }

  BK::TinyVector<T,SPACEDIM> b;
}; 

template<class T, int SPACEDIM>
class ElektricField
{
 public:
  ElektricField() { for(int i=0;i<SPACEDIM;i++) e[i] = 0; }

  BK::TinyVector<T,SPACEDIM> e;
};

template<class T, int SPACEDIM>
class Current
{
 public:
  Current() { for(int i=0;i<SPACEDIM;i++) j[i] = 0; }
  BK::TinyVector<T,SPACEDIM> j;
}; 

template<class T, int SPACEDIM>
class InitialMagneticField
{
 public:
  InitialMagneticField() { for(int i=0;i<SPACEDIM;i++) initB[i] = 0; }
  // space coordinates
  BK::TinyVector<T,SPACEDIM> initB;
};

template<class T, int SPACEDIM, int VELODIM>
class InitialCoord
{
 public:
  InitialCoord() { 
    for(int i=0;i<SPACEDIM;i++) initSpace[i] = 0; 
    for(int i=0;i<VELODIM;i++) initVelo[i] = 0;
  }
  // space coordinates
  BK::TinyVector<T,SPACEDIM> initSpace;

  // velocity coordinates
  BK::TinyVector<T,VELODIM> initVelo;
};

template<class T, int SPACEDIM, int VELODIM>
class ExtraCoord1
{
 public:
  ExtraCoord1() : extraSpace1(0),extraVelo1(0) {}

  // space coordinates
  BK::TinyVector<T,SPACEDIM> extraSpace1;

  // velocity coordinates
  BK::TinyVector<T,VELODIM> extraVelo1;
};

template<class T, int SPACEDIM, int VELODIM>
class ExtraCoord2
{
 public:
  ExtraCoord2() : extraSpace2(0),extraVelo2(0) {}

  // space coordinates
  BK::TinyVector<T,SPACEDIM> extraSpace2;

  // velocity coordinates
  BK::TinyVector<T,VELODIM> extraVelo2;
};

template<class T, int SPACEDIM, int VELODIM>
class ExtraCoord3
{
 public:
  ExtraCoord3() : extraSpace3(0),extraVelo3(0) {}

  // space coordinates
  BK::TinyVector<T,SPACEDIM> extraSpace3;

  // velocity coordinates
  BK::TinyVector<T,VELODIM> extraVelo3;
};

template<class T, int SPACEDIM, int VELODIM>
class Particle
{
 public:
  // default constructor
  Particle();
  
  // copy constructor
  Particle(const Particle& v);  

  // returns euclidean norm of velocity
  const T veloNorm() const;

  // returns square of euclidean norm of velocity
  const T veloSqr() const;

  int spaceDim;
  int veloDim;

  // space coordinates
  BK::TinyVector<T,SPACEDIM> space;

  // velocity coordinates
  BK::TinyVector<T,VELODIM> velo;

 protected:
  void copy(const Particle& v);  
};

template<class ATTRIBUTE>
  class ConcreteParticle : public BK::GenScatterHierarchy<ATTRIBUTE,Inheritor>
{};
 
// Erzeugung eines Concrete-Particles:------------------------------------------
/* typedef Particle<3,3> ParticleBasis; */
/* typedef TYPELIST_3(Mass,Charge,ParticleBasis) ATTRIBUTE; */
/* typedef BK::GenScatterHierarchy<ATTRIBUTE,Inheritor> PARTICLE;  oder*/
/* typedef ConcreteParticle<ATTRIBUTE> PARTICLE;  oder*/

// -----------------------------------------------------------------------------------------------
// definitions
// -----------------------------------------------------------------------------------------------

template<class T, int SPACEDIM, int VELODIM>
Particle<T,SPACEDIM,VELODIM>::Particle() : spaceDim(SPACEDIM), veloDim(VELODIM) 
{
  for(int i=0;i<SPACEDIM;i++) space[i] = 0;
  for(int i=0;i<VELODIM;i++) velo[i] = 0;
}

template<class T, int SPACEDIM, int VELODIM>
Particle<T,SPACEDIM,VELODIM>::Particle(const Particle& v) : spaceDim(SPACEDIM), veloDim(VELODIM) 
{
  copy(v);
}

template<class T, int SPACEDIM, int VELODIM>
void Particle<T,SPACEDIM,VELODIM>::copy(const Particle& v)
{
  for(int i=0;i<v.spaceDim;i++){
    space[i] = v.space[i];
  }
  for(int i=0;i<v.veloDim;i++){
    velo[i] = v.velo[i];
  }
}

template<class T, int SPACEDIM, int VELODIM>
const T Particle<T,SPACEDIM,VELODIM>::veloNorm() const
{
  T temp=0;
  for(int i=0;i<veloDim;i++){
    temp += pow(velo[i],2);
  }
  return sqrt(temp);
}

template<class T, int SPACEDIM, int VELODIM>
const T Particle<T,SPACEDIM,VELODIM>::veloSqr() const
{
  T temp=0;
  for(int i=0;i<veloDim;i++){
    temp += pow(velo[i],2);
  }
  return temp;
}

} // namespace BK

#endif
