#ifndef distributionFunction_hpp
#define distributionFunction_hpp

#include <iostream>
#include <string>
#include <cassert>

#include "parseFromFile.hpp"
#include "dateiAusgabe.hpp"

namespace BK{

template<class DOUBLE>
class DistributionFunction
{
 public:

  // Konstruktor (macht nix)
  DistributionFunction();

  // Destruktor
  ~DistributionFunction();
  
  // Initialisiert die Verteilungsfunktion
  // [maxX,minX] Definitionsbereicht der Verteilungsfunktion
  // slots = Anzahl der Unterintervalle
  void init(DOUBLE minX_, DOUBLE maxX_, int slots);

  // Initialisiert die Verteilungsfunktion
  // minX, maxX muessen durch setMinMax() gesetzt werden
  void init(int slots);

  void init();

  // nimmt Wert in Verteilungsfunktion auf
  void receiveValue(DOUBLE value);
  
  // Wert der Verteilungsfunktion in Slot i
  double function(int i);

  // Wert der Verteilungsfunktion an der Stelle x
  // noch nicht implementiert
  double function(DOUBLE x);

  // const Pointer auf Verteilungsfunktionswerte
  double* fData() const;

  // const Pointer auf Definitionsbereichs-Array der Verteilungsfunktion
  double* xData() const;
  
  // normiert die Verteilungsfunktion auf Wert norm
  void normalise(double norm);
  
  // Anzahl der Me�werte
  int valueNumber() const;

  // Gewicht der Me�werte
  double valueWeight() const;

  // Gewicht der Me�werte
  double norm() const;

  // Anzahl der Werte au�erhalb des Definitionsbereiches
  int outOfRange() const;

  // L�scht die Verteilungsfunktion
  void clear();
  
  // gibt Anzahl, wie oft die Verteilungsfunktion gel�scht und neu angelegt wurde zur�ck
  int realisationCounter() const ;

  // Ausgabe in Datei filename, zus�tzlich wird .info mit Infos erzeugt
  void fileOut(std::string filename) const; 
  
  // gibt linke und rechte Grenze des Definitionsbereiches zur�ck
  DOUBLE getMin() const {return minX;};
  DOUBLE getMax() const {return maxX;};
  
  DOUBLE& setMin() {return minX;}
  DOUBLE& setMax() {return maxX;}

  // setzt Minimum, Maximum, dx und xArray der Verteilungsfunktion
  void setMinMax(double min, double max);  

  // gibt Anzahl der slots zur�ck
  int getSlots() const {return slots_;};

  int& setSlots(){return slots_;}


 protected:

  // Anzahl der slots
  int slots_;

  // [maxX,minX] Definitionsbereicht der Verteilungsfunktion
  DOUBLE minX, maxX;
  
  // Breite der slots
  double dx;

  // array der Verteilungsfunktion
  double* fArray;

  double* xArray;

  // Anzahl der Me�werte
  int valueNumber_;

  // Gewicht der einzelnen Me�werte
  double valueWeight_;
  
  // Anfangs nicht belegt!!!
  double* norm_;

  // Anzahl der Werte au�erhalb des Definitionsbereiches
  int outOfRange_;

  // z�hlt wie oft die Verteilungsfunktion gel�scht und neu angelegt wurde
  int realisationCounter_;
};

// ---------------------------------------------------------------------------
// definitions
// ---------------------------------------------------------------------------

template<class DOUBLE>
DistributionFunction<DOUBLE>::DistributionFunction()
{
  slots_ = 0;
  maxX = minX = 0;
  fArray = NULL;
  xArray = NULL;
  norm_ = NULL;
}

template<class DOUBLE>
DistributionFunction<DOUBLE>::~DistributionFunction()
{
  if(fArray != NULL){
    delete [] fArray;
    delete [] xArray;
  }
  if(norm_ != NULL)
    delete norm_;
}

template<class DOUBLE>
void DistributionFunction<DOUBLE>::setMinMax(double min, double max)
{
  if(slots_ == 0){
    std::cerr << "ERROR in DistributionFunction<DOUBLE>::setMinMax(double min, double max): slots = 0\n";
    abort();
  }

  minX = min;
  maxX = max;
  dx = (max-min)/slots_;
  
  for(int i=0;i<slots_;i++){
    xArray[i] = minX+i*dx;
  }
}

template<class DOUBLE>
void DistributionFunction<DOUBLE>::init()
{
  if(fArray == NULL){
    std::cerr << "ERROR in DistributionFunction<DOUBLE>::init(): fArray existiert nicht!\n";
    abort();
  } 
  if(minX == maxX){
    std::cerr << "ERROR in DistributionFunction<DOUBLE>::init(): minX = maxX!\n";
    abort();
  } 
  if(slots_ == 0){
    std::cerr << "ERROR in DistributionFunction<DOUBLE>::init(): slots = 0!\n";
    abort();
  } 

  xArray = new double[slots_];
  dx = (maxX-minX)/slots_;
  for(int i=0;i<slots_;i++){
    fArray[i] = 0;
    xArray[i] = minX+i*dx;
  }
  outOfRange_ = 0;
  valueWeight_ = 1;
  norm_ = NULL;
  valueNumber_ = 0;
  realisationCounter_ = 0;
}

template<class DOUBLE>
void DistributionFunction<DOUBLE>::init(DOUBLE min_, DOUBLE max_, int slots)
{
  minX = min_;
  maxX = max_;
  slots_ = slots;
  dx = (maxX-minX)/slots_;
  xArray = new double[slots_];
  fArray = new double[slots_];
  for(int i=0;i<slots_;i++){
    fArray[i] = 0;
    xArray[i] = minX+i*dx;
  }
  outOfRange_ = 0;
  valueWeight_ = 1;
  norm_ = NULL;
  valueNumber_ = 0;
  realisationCounter_ = 0;
}

template<class DOUBLE>
void DistributionFunction<DOUBLE>::init(int slots)
{
  if(fArray != NULL){
    std::cerr << "ERROR in DistributionFunction<DOUBLE>::init(int slots): fArray existiert bereits!\n";
    abort();
  } 
  else{
    slots_ = slots;
    fArray = new double[slots_];
    xArray = new double[slots_];
  }

  for(int i=0;i<slots;i++){
    fArray[i] = 0;
  }
  
  outOfRange_ = 0;
  valueWeight_ = 1;
  norm_ = NULL;
  valueNumber_ = 0;
  realisationCounter_ = 0;
}

template<class DOUBLE>
void DistributionFunction<DOUBLE>::clear() 
{
  for(int i=0;i<slots_;i++){
    fArray[i] = 0;
  }
  outOfRange_ = 0;
  valueWeight_ = 1;
  norm_ = NULL;
  valueNumber_ = 0;
  realisationCounter_++;
}

template<class DOUBLE>
void DistributionFunction<DOUBLE>::receiveValue(DOUBLE value)
{
  if(value > maxX || value < minX)
    outOfRange_++;
  else{
  int slot = int ((value-minX)/dx);
  // std::cerr << "dx = " << dx << "  minX = " << minX << "  maxX = " << maxX << "  value = " << value << "  slot = " << slot << std::endl;
  fArray[slot] += valueWeight_;
  valueNumber_++;
  }
}

template<class DOUBLE>
double DistributionFunction<DOUBLE>::function(int i)
{
  return fArray[i];
}

template<class DOUBLE>
double* DistributionFunction<DOUBLE>::fData() const
{
  return fArray;
}

template<class DOUBLE>
double* DistributionFunction<DOUBLE>::xData() const
{
  return xArray;
}

template<class DOUBLE>
void DistributionFunction<DOUBLE>::normalise(double norm)
{
  if(norm_ == 0)
    norm_ = new double;
  
  *norm_ = norm;
  double arrayNorm = 0;
  for(int i=0;i<slots_;i++){
    arrayNorm += fArray[i];
  }
  double frac = norm/arrayNorm;
  for(int i=0;i<slots_;i++){
    fArray[i] *= frac;
  }
  valueWeight_ = *norm_/slots_;
}

template<class DOUBLE>
int DistributionFunction<DOUBLE>::valueNumber() const
{
  return valueNumber_;
}

template<class DOUBLE>
double DistributionFunction<DOUBLE>::valueWeight() const
{
  return valueWeight_;
}

template<class DOUBLE>
double DistributionFunction<DOUBLE>::norm() const
{
  if(norm_ == NULL){
    std::cerr << "Fehler: Verteilungsfunktion noch nicht normiert!\n";
    abort();
  }
  return *norm_;
}

template<class DOUBLE>
int DistributionFunction<DOUBLE>::outOfRange() const
{
  return outOfRange_;
}

template<class DOUBLE>
int DistributionFunction<DOUBLE>::realisationCounter() const
{
  return realisationCounter_;
}

template<class DOUBLE>
void DistributionFunction<DOUBLE>::fileOut(std::string filename) const
{
  if(slots_ != 0){
    dateiAusgabeXYE(xArray,fArray,0,slots_-1,filename);
    
    ReadInitFile valueMap;
    
    valueMap.checkInParameter("slots_", slots_);
    valueMap.checkInParameter("valueNumber_", valueNumber_);
    valueMap.checkInParameter("minX", minX);
    valueMap.checkInParameter("maxX", maxX);
    valueMap.checkInParameter("dx", dx);
    valueMap.checkInParameter("realisationCounter_", realisationCounter_);
    valueMap.checkInParameter("outOfRange_", outOfRange_);
    valueMap.checkInParameter("valueWeight_", valueWeight_);
    if(norm_ == NULL)
      valueMap.checkInParameter("norm_", "NO");
    else
      valueMap.checkInParameter("norm_", *norm_);
    
    valueMap.writeMap(appendString(filename,".info"));
  }
  else{
    std::cerr << "Error in `void DistributionFunction<DOUBLE>::fileOut(std::string filename) const': slots = 0\n";
    abort();
  }
}

} // namespace BK

#endif
