#include "mpiBase.hpp"

// #undef LOGLEVEL
// #define LOGLEVEL 4

namespace BK {

MpiBase::MpiBase()
{
  ERRORLOGL(1,"MpiBase::MpiBase()");
  
  initFlag_ = false;

  ERRORLOGL(4,"MpiBase::MpiBase()-end");
}

MpiBase::~MpiBase()
{
  ERRORLOGL(1,BK::appendString(commRank_,"  MpiBase::~MpiBase()"));

  MPI_Finalize();  

  ERRORLOGL(4,"MpiBase::~MpiBase()-end");
}

void MpiBase::finalize() const
{
  ERRORLOGL(1,BK::appendString(commRank_,"  MpiBase::finalize()"));

  MPI_Finalize();  

  ERRORLOGL(4,"MpiBase::finalize()-end");
}

void MpiBase::init(int& argc,char*** argv)
{
  ERRORLOGL(1,"MpiBase::init(int argc,char** argv)");

  ERRORLOGL(3,"MPI_Init ...");
  MPI_Init(&argc,argv);

  MPI_Comm_rank(MPI_COMM_WORLD,&commRank_);
  MPI_Comm_size(MPI_COMM_WORLD,&commSize_);

  initFlag_ = true;

  ERRORLOGL(4,"MpiBase::init(int argc,char** argv)-end");
}

void MpiBase::barrier()
{
  MPI_Barrier(MPI_COMM_WORLD);
}

}
