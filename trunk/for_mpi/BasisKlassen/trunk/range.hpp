#ifndef range_hpp
#define range_hpp

#include <cassert>

#include "logger.hpp"

namespace BK {

class Range
{
public:
  Range();

  Range(int low, int high);

  int index[2];
  int range;
};

std::ostream& operator<<(std::ostream &of, const Range& range);

}  // namespace BK

#endif
