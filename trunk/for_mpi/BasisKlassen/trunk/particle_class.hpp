#ifndef particle_class_hpp
#define particle_class_hpp

#include <iostream>

#include "tinyVector.hpp"

namespace BK {

template<class DOUBLE, int dim>
class Particle
{
 public:
  // default-Konstruktor
  Particle();
  
  // copy-Konstruktor
  Particle(const Particle<DOUBLE,dim>& v);  
  
/*   // Konstruktor: erzeugt Particle mit Koordinaten _space und velo = 0, Charge=1, Mass=1 */
/*   Particle(const Vector<DOUBLE>& _space);                          */
  
/*   // Konstruktor: erzeugt Particle mit Koordinaten _space und Charge _charge und Mass _mass */
/*   Particle(const Vector<DOUBLE>& _space, const DOUBLE& _charge, const DOUBLE& _mass); */

/*   // Konstruktor: erzeugt Particle mit Koordinaten _space, Geschwindigkeit _velo und Charge _charge und Mass _mass */
/*   Particle(const Vector<DOUBLE>& _space, const Vector<DOUBLE>& _velo, const DOUBLE& _charge, const DOUBLE& _mass); */
  
  // copy-Operator= kopiert elemente von v
  Particle &operator=(const Particle<DOUBLE,dim>& v);      

  // += Operator
  Particle &operator+=(const Particle<DOUBLE,dim>& v);

  // -= Operator
  Particle &operator-=(const Particle<DOUBLE,dim>& v);

  // *= Operator
  Particle &operator*=(DOUBLE zahl);

  // /= Operator
  Particle &operator/=(DOUBLE zahl);
  
  template<class TYPE>
    void setSpace(TYPE _space);
  
  template<class TYPE>
    void setVelo(TYPE _space);
  
  const BK::TinyVector<DOUBLE,dim>& space() const;
  BK::TinyVector<DOUBLE,dim>& space();
  const BK::TinyVector<DOUBLE,dim>& velo() const;
  BK::TinyVector<DOUBLE,dim>& velo();

  DOUBLE space(int i) const;
  DOUBLE& space(int i);
  
  DOUBLE velo(int i) const;
  DOUBLE& velo(int i);

  const DOUBLE& getCharge() const;
  void setCharge(DOUBLE _charge);
  
  const DOUBLE& getMass() const;
  void setMass(DOUBLE _charge); 

  // Dimension des Raumes und der Geschwindigkeit
  const int dimension;
  
  // gibt euklidische Norm der Geschwindigkeit zur�ck
  const DOUBLE getVeloNorm() const;

  // gibt euklidisches Betragsquadrat der Geschwindigkeit zur�ck
  const DOUBLE getVeloSqr() const;

 private:
  // Raum-Koordinaten
  BK::TinyVector<DOUBLE,dim> spaceKoord;

  // Geschwindigkeit-Koordinaten
  BK::TinyVector<DOUBLE,dim> veloKoord;

  // Charge
  DOUBLE charge;
  
  // Mass  
  DOUBLE mass;

  //friend ostream& operator<< <>(ostream&, const Particle<DOUBLE,dim>&);
  void copy(const Particle<DOUBLE,dim>& v);  
};

// Additions-Operator f�r zwei Teilchen: v+w
template<class DOUBLE, int dim>
Particle<DOUBLE,dim> operator+(const Particle<DOUBLE,dim>& v, const Particle<DOUBLE,dim>& w);

// Subtraktions-Operator f�r zwei Teilchen: v-w
template<class DOUBLE, int dim>
Particle<DOUBLE,dim> operator-(const Particle<DOUBLE,dim>& v, const Particle<DOUBLE,dim>& w);

// Multiplikations-Operator f�r Zahl und Teilchen: zahl*v
template<class DOUBLE, int dim>
Particle<DOUBLE,dim> operator*(const DOUBLE& zahl, const Particle<DOUBLE,dim>& w);

// Multiplikations-Operator f�r Teilchen und Zahl: w*Zahl
template<class DOUBLE, int dim>
Particle<DOUBLE,dim> operator*(const Particle<DOUBLE,dim>& v, const DOUBLE& zahl);

// gibt euklidische Raumdistanz zur�ck
template<class DOUBLE, int dim>
const DOUBLE spaceDiffNorm(const Particle<DOUBLE, dim>& p1, const Particle<DOUBLE, dim>& p2);

// gibt euklidische Geschwindigkeitsdistanz zur�ck
template<class DOUBLE, int dim>
const DOUBLE veloDiffNorm(const Particle<DOUBLE, dim>& p1, const Particle<DOUBLE, dim>& p2);

// Ausgabeoperator
template<class DOUBLE,int dim>
std::ostream& operator<<(std::ostream &of, const Particle<DOUBLE, dim> &v);

// -----------------------------------------------------------------------------------------
// definitions
// -----------------------------------------------------------------------------------------

template<class DOUBLE, int dim>
Particle<DOUBLE,dim>::Particle() : dimension(dim)
{
  for(int i=0;i<(dimension);i++){
    spaceKoord[i] = 0;
    veloKoord[i] = 0;
  }
  charge = 1;
  mass = 1;
}

template<class DOUBLE, int dim>
Particle<DOUBLE,dim>::Particle(const Particle<DOUBLE,dim>& v) : dimension(dim)
{
  copy(v);
}

template<class DOUBLE,int dim>
void Particle<DOUBLE,dim>::copy(const Particle<DOUBLE,dim>& v)
{
  for(int i=0;i<(dimension);i++){
    spaceKoord[i] = v.space(i);
    veloKoord[i] = v.velo(i);
  }
  charge = v.getCharge();
  mass = v.getMass();
}

// template<class DOUBLE, int dim>
// Particle<DOUBLE,dim>::Particle(const Vector<DOUBLE>& _space) : dimension(dim)
// {
//   if( _space.getSize() != dimension){
//     std::cerr << "Particle-Konstruktor: Dimensionen stimmen nicht �berein!" << std::endl;
//     abort();
//   }
  
//   for(int i=0;i<(dimension);i++){
//     spaceKoord[i] = _space[i];
//     veloKoord[i] = 0;
//   }
//   charge = 1;
//   mass = 1;
// }

// template<class DOUBLE, int dim>
// Particle<DOUBLE,dim>::Particle(const Vector<DOUBLE>& _space, const DOUBLE& _charge, const DOUBLE& _mass) : dimension(dim)
// {
//   if( _space.getSize() != dimension){
//     std::cerr << "Particle-Konstruktor: space-Dimensionen stimmen nicht �berein!" << std::endl;
//     abort();
//   }
//   for(int i=0;i<(dimension);i++){
//     spaceKoord[i] = _space[i];
//     veloKoord[i] = 0;
//   }
//   charge = _charge;
//   mass = _mass;
// }

// template<class DOUBLE, int dim>
// Particle<DOUBLE,dim>::Particle(const Vector<DOUBLE>& _space, const Vector<DOUBLE>& _velo, const DOUBLE& _charge, const DOUBLE& _mass) : dimension(dim)
// {
//   if( _space.getSize() != dimension){
//     std::cerr << "Particle-Konstruktor: space-Dimensionen stimmen nicht �berein!" << std::endl;
//     abort();
//   }
//   if( _velo.getSize() != dimension){
//     std::cerr << "Particle-Konstruktor: velo-Dimensionen stimmen nicht �berein!" << std::endl;
//     abort();
//   }
//   for(int i=0;i<(dimension);i++){
//     spaceKoord[i] = _space[i];
//     veloKoord[i] = _velo[i];
//   }
//   charge = _charge;
//   mass = _mass;
// }

template<class DOUBLE, int dim>
Particle<DOUBLE,dim>& Particle<DOUBLE,dim>::operator=(const Particle<DOUBLE,dim>& v)    
{
  copy(v);
  return *this;
}

template<class DOUBLE, int dim>
Particle<DOUBLE,dim>& Particle<DOUBLE,dim>::operator+=(const Particle<DOUBLE,dim>& v)    
{
  for(int i=0;i<dim;i++){
    space(i) += v.space(i);
    velo(i) += v.velo(i);
  }
  return *this;
}

template<class DOUBLE, int dim>
Particle<DOUBLE,dim>& Particle<DOUBLE,dim>::operator-=(const Particle<DOUBLE,dim>& v)    
{
  for(int i=0;i<dim;i++){
    space(i) -= v.space(i);
    velo(i) -= v.velo(i);
  }
  return *this;
}

template<class DOUBLE, int dim>
Particle<DOUBLE,dim>& Particle<DOUBLE,dim>::operator*=(DOUBLE zahl)    
{
  space() *= zahl;
  velo() *= zahl;
}

template<class DOUBLE, int dim>
Particle<DOUBLE,dim>& Particle<DOUBLE,dim>::operator/=(DOUBLE zahl)    
{
  space() /= zahl;
  velo() /= zahl;
}

template<class DOUBLE, int dim>
template<class TYPE>
void Particle<DOUBLE,dim>::setSpace(TYPE _space)
{
  if( _space.getSize() != dimension){
    std::cerr << "Particle-Konstruktor: space-Dimensionen stimmen nicht �berein!" << std::endl;
    abort();
  }
  for(int i=0;i<dimension;i++){
    spaceKoord[i] = _space[i];
  }
}

template<class DOUBLE, int dim>
template<class TYPE>
void Particle<DOUBLE,dim>::setVelo(TYPE _velo)
{
  if( _velo.getSize() != dimension){
    std::cerr << "Particle-Konstruktor: space-Dimensionen stimmen nicht �berein!" << std::endl;
    abort();
  }
  for(int i=0;i<dimension;i++){
    veloKoord[i] = _velo[i];
  }
}

template<class DOUBLE, int dim>
DOUBLE& Particle<DOUBLE,dim>::velo(int i)
{
  if (i < 0 || i > dimension-1) {
    std::cerr << "Particle::setVelo: invalid index " << i << std::endl;
    abort();
  }
  return veloKoord[i];
}

template<class DOUBLE, int dim>
DOUBLE& Particle<DOUBLE,dim>::space(int i)
{
  if (i < 0 || i > dimension-1) {
    std::cerr << "Particle::setSpace: invalid index " << i << std::endl;
    abort();
  }
  return spaceKoord[i];
}

template<class DOUBLE, int dim>
DOUBLE Particle<DOUBLE,dim>::velo(int i) const
{
  if (i < 0 || i > dimension-1) {
    std::cerr << "Particle::setVelo: invalid index " << i << std::endl;
    abort();
  }
  return veloKoord[i];
}

template<class DOUBLE, int dim>
DOUBLE Particle<DOUBLE,dim>::space(int i) const
{
  if (i < 0 || i > dimension-1) {
    std::cerr << "Particle::setSpace: invalid index " << i << std::endl;
    abort();
  }
  return spaceKoord[i];
}

template<class DOUBLE, int dim>
BK::TinyVector<DOUBLE,dim>& Particle<DOUBLE,dim>::velo() 
{
  return veloKoord;
}

template<class DOUBLE, int dim>
BK::TinyVector<DOUBLE,dim>& Particle<DOUBLE,dim>::space() 
{
  return spaceKoord;
}

template<class DOUBLE, int dim>
const BK::TinyVector<DOUBLE,dim>& Particle<DOUBLE,dim>::velo() const
{
  return veloKoord;
}

template<class DOUBLE, int dim>
const BK::TinyVector<DOUBLE,dim>& Particle<DOUBLE,dim>::space() const
{
  return spaceKoord;
}

template<class DOUBLE, int dim>
inline const DOUBLE& Particle<DOUBLE,dim>::getMass() const
{
  return mass;
}

template<class DOUBLE, int dim>
inline const DOUBLE& Particle<DOUBLE,dim>::getCharge() const
{
  return charge;
}

template<class DOUBLE, int dim>
void Particle<DOUBLE,dim>::setMass(DOUBLE _mass) 
{
  mass = _mass;
}

template<class DOUBLE, int dim>
void Particle<DOUBLE,dim>::setCharge(DOUBLE _charge) 
{
  charge = _charge;
}

template<class DOUBLE, int dim>
const DOUBLE Particle<DOUBLE,dim>::getVeloNorm() const
{
  DOUBLE temp=0;
  for(int i=0;i<dimension;i++){
    temp += pow(veloKoord[i],2);
  }
  return sqrt(temp);
}

template<class DOUBLE, int dim>
const DOUBLE Particle<DOUBLE,dim>::getVeloSqr() const
{
  DOUBLE temp=0;
  for(int i=0;i<dimension;i++){
    temp += pow(veloKoord[i],2);
  }
  return temp;
}

template<class DOUBLE, int dim>
const DOUBLE spaceDiffNorm(const Particle<DOUBLE, dim>& p1, const Particle<DOUBLE, dim>& p2)
{
  DOUBLE diff=0;
  for(int i=0;i<p1.dimension;i++){
    diff += (p1.space(i)-p2.space(i))*(p1.space(i)-p2.space(i));
  }
  return sqrt(diff);
}

template<class DOUBLE, int dim>
const DOUBLE veloDiffNorm(const Particle<DOUBLE, dim>& p1, const Particle<DOUBLE, dim>& p2)
{
  DOUBLE diff=0;
  for(int i=0;i<p1.dimension;i++){
    diff += (p1.velo(i)-p2.velo(i))*(p1.velo(i)-p2.velo(i));
  }
  return sqrt(diff);
}

template<class DOUBLE, int dim>
Particle<DOUBLE,dim> operator+(const Particle<DOUBLE,dim>& v, const Particle<DOUBLE,dim>& w)
{
  Particle<DOUBLE,dim> temp;
  temp.space() = v.space()+w.space();
  temp.velo() = v.velo()+w.velo();
  return temp;
}

template<class DOUBLE, int dim>
Particle<DOUBLE,dim> operator-(const Particle<DOUBLE,dim>& v, const Particle<DOUBLE,dim>& w)
{
  Particle<DOUBLE,dim> temp;
  temp.space() = v.space()-w.space();
  temp.velo() = v.velo()-w.velo();
  return temp;
}

template<class DOUBLE, int dim>
Particle<DOUBLE,dim> operator*(const DOUBLE& zahl, const Particle<DOUBLE,dim>& w)
{
  Particle<DOUBLE,dim> temp;
  temp.space() = zahl*w.space();
  temp.velo() = zahl*w.velo();
  return temp;
}

template<class DOUBLE, int dim>
Particle<DOUBLE,dim> operator*(const Particle<DOUBLE,dim>& w, const DOUBLE& zahl)
{
  Particle<DOUBLE,dim> temp;
  temp.space() = zahl*w.space();
  temp.velo() = zahl*w.velo();
  return temp;
}


template<class DOUBLE, int dim>
std::ostream& operator<<(std::ostream &of, const Particle<DOUBLE, dim> &v)      // Ausgabeoperator der Vektorklasse
{                                                           // ostream &of : Speicherbereich in den die Dinge nach << abgelegt werden
  of << "[";
  for(int j=0;j<dim-1;j++){
    of << v.space(j) << ","; 
  }
  of << v.space(dim-1) << "|";
  for(int j=0;j<dim-1;j++){
    of << v.velo(j) << ","; 
  }
  of << v.velo(dim-1) << "]";
  return of;
}

} // namespace BK

#endif
