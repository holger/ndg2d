#ifndef filesystem_hpp
#define filesystem_hpp

#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <stdio.h>
//#include <exception>
#include <sys/stat.h>
#include <errno.h> // POSIX error codes
#include <vector>

#include <dirent.h>

#include "logger.hpp"

namespace BK
{

using std::ifstream;
using std::ofstream;

typedef std::vector<std::string> PathFracList; 

class path
{
public:
  
  path() {}
  
  path(const char* p) :
    path_(p) {}
  
  path(const std::string& p) :
    path_(p) {}
  
  path branch_path() const {
    std::string::size_type p = path_.rfind('/');
    if (p == std::string::npos)
      return path();
    else
      return path(std::string(&path_[0], &path_[p]));
  }
  
  path leaf() const {
    std::string::size_type p = path_.rfind('/');
    if (p == std::string::npos)
      return path(path_);
    else
      return path(std::string(&path_[p + 1], &path_[path_.length()]));
  }

  std::string string() const { return path_; }
  
  path& operator/=(const path& rhs) {
    path_ += '/' + rhs.string(); 
    return *this;
  }
  
  path operator/(const path& rhs) const { return path(*this) /= rhs; }
  
  const PathFracList& get_fracList() {
    updateFracList_(path_);
    return pathFracList_;
  }

  const std::string& get_fracList(int i) {
    updateFracList_(path_);
    return pathFracList_[i];
  }

private:
  std::string path_;
  
  PathFracList pathFracList_;

  void updateFracList_(std::string path) {
    pathFracList_.clear();
    std::string::size_type slashPos1 = path.find_first_of("/");
    if(slashPos1 != 0) pathFracList_.push_back(path_.substr(0,slashPos1));
    pathFracList_.push_back("/");
    std::string::size_type slashPos2 = path.find("/",slashPos1+1);
    pathFracList_.push_back(path_.substr(slashPos1+1,slashPos2-slashPos1-1));
    
    //std::cerr << slashPos1 << "  " << slashPos2 << std::endl;
  
    while(slashPos2 != std::string::npos) {
      //      std::cerr << slashPos1 << "  " << slashPos2 << std::endl;
      slashPos1 = slashPos2;
      slashPos2 = path.find("/",slashPos1+2);      
      pathFracList_.push_back("/");
      pathFracList_.push_back(path_.substr(slashPos1+1,slashPos2-slashPos1-1));
    }
  }
};


/* class filesystem_error : public std::exception */
/* { */
/* public: */
/*     filesystem_error(int sys_err_code) : */
/* 	err_code_(sys_err_code) */
/*     { */
/*     } */

/*     const char* what() const throw() */
/*     {  */
/* 	return system_message(err_code_).c_str();  */
/*     } */

/*     static std::string system_message( int ) */
/*     { */
/* 	std::string str; */
/* 	str += std::strerror( errno ); */
/* 	return str; */
/*     } */

/* private: */
/*     int err_code_; */
/* }; */

bool exists(const path&);
bool exists(const std::string& dir);
void create_directory(const path&);
bool is_directory(const path&);
bool setupDirectory(const std::string& dir);
bool isDirectoryPath(const std::string& dir);

template<class CONTAINER>
void dirToList(CONTAINER& vec,const std::string& dir)
{
  DIR *pdir;
  struct dirent *pent;
  
  pdir=opendir(dir.c_str());
  
  if(!exists(dir)) return;
  
  while ((pent=readdir(pdir))){
    if(pent == NULL) break;
    vec.push_back(pent->d_name);
  }
  
  closedir(pdir);
}

} // namespace BK

#endif
