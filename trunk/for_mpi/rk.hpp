#ifndef rk_hpp
#define rk_hpp

#include <functional>

#include <BasisKlassen/vector.hpp>

#include "order.hpp"

class Rk
{
public:

  Rk(double dt) {
    ready_ = false;
  }
  
  virtual ~Rk() {
    for(size_t i=0; i<kVec.size(); i++)
      delete kVec[i];
  }

  virtual void advance(Problem::Array& cTemp, Problem::Array const& c, Problem::Array const& rhs) = 0;
  
protected:

  BK::Vector<Problem::Array*> kVec;
  PROTEPARA(int, stage);
  PROTEPARA(double, dt);
  PROTEPARA(bool, ready);
  PROTEPARA(int, stageNumber);
};

class Rk3 : public Rk
{
public:

  Rk3(double dt, Problem::Array const& arrayTemplate) : Rk(dt) {
    stage_ = 0;
    stageNumber_ = 3;
    kVec.resize(4);
    for(size_t i=0; i<kVec.size(); i++)
      kVec[i] = new Problem::Array(arrayTemplate);
  }

  void advance(Problem::Array& cTemp, Problem::Array const& c, Problem::Array const& rhs) {
    Problem::Array& k = *kVec[0];
    Problem::Array& k1 = *kVec[1];
    Problem::Array& k2 = *kVec[2];
    Problem::Array& k3 = *kVec[3];
      
    switch(stage_) {
    case 0:
      k1=rhs;
      k=k1/3.;
      problem.lhs(cTemp, c, k);
      ready_ = false;
      break;
      
    case 1:
      k2 = rhs;
      k=2*k2/3.;
      problem.lhs(cTemp, c, k);
      break;
      
    case 2:
      k3 = rhs;
      k=(k1 + 3*k3)/4.;
      problem.lhs(cTemp, c, k);
      ready_ = true;
      stage_ = -1;
      break;
    }
    ++stage_;
  }
};

class Rk4 : public Rk
{
public:

  Rk4(double dt, Problem::Array const& arrayTemplate) : Rk(dt) {
    stage_ = 0;
    stageNumber_ = 4;
    kVec.resize(5);
    for(size_t i=0; i<kVec.size(); i++)
      kVec[i] = new Problem::Array(arrayTemplate);
  }

  void advance(Problem::Array& cTemp, Problem::Array const& c, Problem::Array const& rhs) {
    Problem::Array& k = *kVec[0];
    Problem::Array& k1 = *kVec[1];
    Problem::Array& k2 = *kVec[2];
    Problem::Array& k3 = *kVec[3];
    Problem::Array& k4 = *kVec[4];
      
    switch(stage_) {
    case 0:
      k1=rhs;
      k=0.5*k1;
      problem.lhs(cTemp, c, k);
      ready_ = false;
      break;
      
    case 1:
      k2 = rhs;
      k=0.5*k2;
      problem.lhs(cTemp, c, k);
      break;
      
    case 2:
      k3 = rhs;
      problem.lhs(cTemp, c, k3);
      break;
      
    case 3:
      k4 = rhs;
      k=(k1 + 2*(k2 + k3) + k4)/6.;
      problem.lhs(cTemp, c, k);
      ready_ = true;
      stage_ = -1;

      break;
    }
    ++stage_;
  }
};

class Rk5 : public Rk
{
public:

  Rk5(double dt, Problem::Array const& arrayTemplate) : Rk(dt) {
    stage_ = 0;
    stageNumber_ = 6;
    kVec.resize(7);
    for(size_t i=0; i<kVec.size(); i++)
      kVec[i] = new Problem::Array(arrayTemplate);
  }

  void advance(Problem::Array& cTemp, Problem::Array const& c, Problem::Array const& rhs) {
    Problem::Array& k = *kVec[0];
    Problem::Array& k0 = *kVec[1];
    Problem::Array& k1 = *kVec[2];
    Problem::Array& k2 = *kVec[3];
    Problem::Array& k3 = *kVec[4];
    Problem::Array& k4 = *kVec[5];
    Problem::Array& k5 = *kVec[6];
      
    switch(stage_) {
    case 0:
      k0=rhs;
      k=0.25*k0;
      problem.lhs(cTemp, c, k);
      ready_ = false;
      break;
      
      
    case 1:
      k1 = rhs;
      k=(k0 + k1)/8.;
      problem.lhs(cTemp, c, k);
      break;
      
    case 2:
      k2 = rhs;
      k=(-0.5*k1 + k2);
      problem.lhs(cTemp, c, k);
      break;

    case 3:
      k3 = rhs;
      k=(3./16*k0 + 9./16*k3);
      problem.lhs(cTemp, c, k);
      break;
      
    case 4:
      k4 = rhs;
      k=(-3./7*k0 + 2./7*k1 + 12./7*k2 - 12./7*k3 + 8./7*k4);
      problem.lhs(cTemp, c, k);
      break;
            
    case 5:
      k5 = rhs;
      k=(7*k0 + 32*k2 + 12*k3 + 32*k4 + 7*k5)/90;
      problem.lhs(cTemp, c, k);
      ready_ = true;
      stage_ = -1;
      break;
    }

    ++stage_;
  }
};

class Rk6 : public Rk
{
public:

  Rk6(double dt, Problem::Array const& arrayTemplate) : Rk(dt) {
    stage_ = 0;
    stageNumber_ = 7;
    kVec.resize(8);
    for(size_t i=0; i<kVec.size(); i++)
      kVec[i] = new Problem::Array(arrayTemplate);
  }

  void advance(Problem::Array& cTemp, Problem::Array const& c, Problem::Array const& rhs) {
    Problem::Array& k = *kVec[0];
    Problem::Array& k1 = *kVec[1];
    Problem::Array& k2 = *kVec[2];
    Problem::Array& k3 = *kVec[3];
    Problem::Array& k4 = *kVec[4];
    Problem::Array& k5 = *kVec[5];
    Problem::Array& k6 = *kVec[6];
    Problem::Array& k7 = *kVec[7];
      
    switch(stage_) {
    case 0:
      k1 = rhs;
      problem.lhs(cTemp, c, k1);
      ready_ = false;
      break;
      
      
    case 1:
      k2 = rhs;
      k = (3*k1 + k2)/8.;
      problem.lhs(cTemp, c, k);
      break;
      
    case 2:
      k3 = rhs;
      k = (8*k1 + 2*k2 + 8*k3)/27.;
      problem.lhs(cTemp, c, k);
      break;

    case 3:
      k4 = rhs;
      k = (+ 3*(3*sqrt(21)-7)*k1
	   - 8*(7-sqrt(21))*k2
	   + 48*(7-sqrt(21))*k3
	   - 3*(21 - sqrt(21))*k4)/392;
      problem.lhs(cTemp, c, k);
      break;
      
    case 4:
      k5 = rhs;
      k = (- 5*(231+51*sqrt(21))*k1
	   - 40*(7+sqrt(21))*k2
	   - 320*sqrt(21)*k3
	   + 3*(21+121*sqrt(21))*k4
	   + 392*(6+sqrt(21))*k5)/1960;
      problem.lhs(cTemp, c, k);
      break;
            
    case 5:
      k6 = rhs;
      k = (+ 15*(22+7*sqrt(21))*k1
	   + 120*k2
	   + 40*(7*sqrt(21)-5)*k3
	   - 63*(3*sqrt(21)-2)*k4
	   - 14*(9*sqrt(21)+49)*k5
	   + 70*(7-sqrt(21))*k6)/180;
      problem.lhs(cTemp, c, k);
      break;

    case 6:
      k7 = rhs;
      k = (9*k1 + 64*k3 + 49*k5 + 49*k6 + 9*k7)/180.;
      problem.lhs(cTemp, c, k);
      ready_ = true;
      stage_ = -1;
      break;
    }

    ++stage_;
  }
};

inline std::unique_ptr<Rk> getRkObject(double dt, Problem::Array const& c) {
  std::unique_ptr<Rk> rkPtr;
  
  switch(problem.get_rkOrder()) {

  case 3:
    rkPtr = std::make_unique<Rk3>(dt, c);
    break;
  case 4:
    rkPtr = std::make_unique<Rk4>(dt, c);
    break;
  case 5:
    rkPtr = std::make_unique<Rk5>(dt, c);
    break;
  case 6:
    rkPtr = std::make_unique<Rk6>(dt, c);
    break;

  default:
    std::cerr << "ERROR in getRkObject: \n";
    std::cerr << "problem.get_rkOrder() = " << problem.get_rkOrder() << " is not supported!\n";
    
    exit(0);
  }
  return rkPtr;
  
}


// old implementations --------------------------------------------------------------------------------------------------

template<class Array, class Rhs, class BoundaryCondition, class Problem>
void rk1(Array& cnew, Array const& c, double dt, Rhs f, BoundaryCondition boundaryCondition, Problem const& problem)
{
  Array k1(c.dim());
  f(c, k1, boundaryCondition);
  problem.lhs(cnew, c, k1);
  //  boundaryCondition(cnew);
}

template<class Array, class Rhs, class BoundaryCondition, class Problem>
void rk2(Array& cnew, Array const& c, double dt, Rhs f, BoundaryCondition boundaryCondition, Problem const& problem)
{
  Array c2(c.dim());
  Array k(c.dim());
  Array k1(c.dim());

  f(c, k1, boundaryCondition);
  k=k1/2.;
  problem.lhs(c2, c, k);
  // boundaryCondition(c2);

  f(c2, k1, boundaryCondition);
  problem.lhs(cnew, c, k);
  // boundaryCondition(cnew);
}

template<class Array, class Rhs, class BoundaryCondition, class Problem>
void rk3(Array& cnew, Array const& c, double dt, Rhs f, BoundaryCondition boundaryCondition, Problem const& problem)
{
  Array cTemp(c.dim());
  Array k(c.dim());
  Array k1(c.dim());
  Array k2(c.dim());
  Array k3(c.dim());

  f(c, k1, boundaryCondition);
  k=k1/3.;
  problem.lhs(cTemp, c, k);
  // boundaryCondition(cTemp);

  f(cTemp, k2, boundaryCondition);
  k=2*k2/3.;
  problem.lhs(cTemp, c, k);
  // boundaryCondition(cTemp);

  f(cTemp, k3, boundaryCondition);
  k=(k1 + 3*k3)/4.;
  problem.lhs(cnew, c, k);
  // boundaryCondition(cnew);
}

template<class Array, class Rhs, class BoundaryCondition, class Problem>
void rk3_vec(std::vector<Array> & cnew, std::vector<Array> const& c, double dt, Rhs f, BoundaryCondition boundaryCondition, Problem const& problem)
{
  std::vector<Array> cTemp(c.size(), Array(c[0].dim()));
  std::vector<Array> k(c.size(), Array(c[0].dim()));
  std::vector<Array> k1(c.size(), Array(c[0].dim()));
  std::vector<Array> k2(c.size(), Array(c[0].dim()));
  std::vector<Array> k3(c.size(), Array(c[0].dim()));

  for(size_t i=0; i<c.size(); i++) {
    f(c[i], k1[i], boundaryCondition);
    k[i]=k1[i]/3.;
    problem.lhs(cTemp[i], c[i], k[i]);
  }

  // boundaryCondition(cTemp);

  for(size_t i=0; i<c.size(); i++) {
    f(cTemp[i], k2[i], boundaryCondition);
    k[i]=2*k2[i]/3.;
    problem.lhs(cTemp[i], c[i], k[i]);
  }
  // boundaryCondition(cTemp);

  for(size_t i=0; i<c.size(); i++) {
    f(cTemp[i], k3[i], boundaryCondition);
    k[i]=(k1[i] + 3*k3[i])/4.;
    problem.lhs(cnew[i], c[i], k[i]);
  }
  // boundaryCondition(cnew);
}

template<class Array, class Rhs, class BoundaryCondition, class Problem>
void rk4(Array& cnew, Array const& c, double dt, Rhs f, BoundaryCondition boundaryCondition, Problem const& problem)
{
  Array cTemp(c.dim());
  Array k(c.dim());
  Array k1(c.dim());
  Array k2(c.dim());
  Array k3(c.dim());
  Array k4(c.dim());

  f(c, k1, boundaryCondition);
  k=0.5*k1;
  problem.lhs(cTemp, c, k);
  // boundaryCondition(cTemp);

  f(cTemp, k2, boundaryCondition);
  k=0.5*k2;
  problem.lhs(cTemp, c, k);
  // boundaryCondition(cTemp);

  f(cTemp, k3, boundaryCondition);
  problem.lhs(cTemp, c, k3);
  // boundaryCondition(cTemp);

  f(cTemp, k4, boundaryCondition);
  k=(k1 + 2*(k2 + k3) + k4)/6.;
  problem.lhs(cnew, c, k);
  // boundaryCondition(cnew);
}

template<class Array, class Rhs, class BoundaryCondition, class Problem>
void rk5(Array& cnew, Array const& c, double dt, Rhs f, BoundaryCondition boundaryCondition, Problem const& problem)
{
  Array cTemp(c.dim());
  Array k(c.dim());
  Array k0(c.dim());
  Array k1(c.dim());
  Array k2(c.dim());
  Array k3(c.dim());
  Array k4(c.dim());
  Array k5(c.dim());

  f(c, k0, boundaryCondition);
  k=0.25*k0;
  problem.lhs(cTemp, c, k);
  // boundaryCondition(cTemp);

  f(cTemp, k1, boundaryCondition);
  k=(k0 + k1)/8.;
  problem.lhs(cTemp, c, k);
  // boundaryCondition(cTemp);

  f(cTemp, k2, boundaryCondition);
  k=(-0.5*k1 + k2);
  problem.lhs(cTemp, c, k);
  // boundaryCondition(cTemp);

  f(cTemp, k3, boundaryCondition);
  k=(3./16*k0 + 9./16*k3);
  problem.lhs(cTemp, c, k);
  // boundaryCondition(cTemp);

  f(cTemp, k4, boundaryCondition);
  k=(-3./7*k0 + 2./7*k1 + 12./7*k2 - 12./7*k3 + 8./7*k4);
  problem.lhs(cTemp, c, k);
  // boundaryCondition(cTemp);

  f(cTemp, k5, boundaryCondition);
  k=(7*k0 + 32*k2 + 12*k3 + 32*k4 + 7*k5)/90;
  problem.lhs(cnew, c, k);
  // boundaryCondition(cnew);
}

template<class Array, class Rhs, class BoundaryCondition, class Problem>
void rk6(Array& cnew, Array const& c, double dt, Rhs f, BoundaryCondition boundaryCondition, Problem const& problem)
{
  Array cTemp(c.dim());
  Array k(c.dim());
  Array k1(c.dim());
  Array k2(c.dim());
  Array k3(c.dim());
  Array k4(c.dim());
  Array k5(c.dim());
  Array k6(c.dim());
  Array k7(c.dim());
  
  f(c, k1, boundaryCondition);
  problem.lhs(cTemp, c, k1);
  // boundaryCondition(cTemp);

  f(cTemp, k2, boundaryCondition);
  k = (3*k1 + k2)/8.;
  problem.lhs(cTemp, c, k);
  // boundaryCondition(cTemp);

  f(cTemp, k3, boundaryCondition);
  k = (8*k1 + 2*k2 + 8*k3)/27.;
  problem.lhs(cTemp, c, k);
  // boundaryCondition(cTemp);

  f(cTemp, k4, boundaryCondition);
  k = (+ 3*(3*sqrt(21)-7)*k1
       - 8*(7-sqrt(21))*k2
       + 48*(7-sqrt(21))*k3
       - 3*(21 - sqrt(21))*k4)/392;
  problem.lhs(cTemp, c, k);
  //  boundaryCondition(cTemp);

  f(cTemp, k5, boundaryCondition);
  k = (- 5*(231+51*sqrt(21))*k1
       - 40*(7+sqrt(21))*k2
       - 320*sqrt(21)*k3
       + 3*(21+121*sqrt(21))*k4
       + 392*(6+sqrt(21))*k5)/1960;
  problem.lhs(cTemp, c, k);
  
  //  boundaryCondition(cTemp);

  f(cTemp, k6, boundaryCondition);
  k = (+ 15*(22+7*sqrt(21))*k1
       + 120*k2
       + 40*(7*sqrt(21)-5)*k3
       - 63*(3*sqrt(21)-2)*k4
       - 14*(9*sqrt(21)+49)*k5
       + 70*(7-sqrt(21))*k6)/180;
  problem.lhs(cTemp, c, k);
  //   boundaryCondition(cTemp);

  f(cTemp, k7, boundaryCondition);
  k = (9*k1 + 64*k3 + 49*k5 + 49*k6 + 9*k7)/180.;
  problem.lhs(cnew, c, k);
  //  boundaryCondition(cnew);
}

template<class Array, class Rhs, class BoundaryCondition, class Problem>
void rk(Array& cnew, Array const& c, double dt, Rhs f, BoundaryCondition boundaryCondition, Problem const& problem)
{
  size_t rkOrder = problem.get_rkOrder();
  
  if(rkOrder == 1)
    rk1(cnew, c, dt, f, boundaryCondition, problem);
  
  if(rkOrder == 2)
    rk2(cnew, c, dt, f, boundaryCondition, problem);
  
  if(rkOrder == 3)
    rk3(cnew, c, dt, f, boundaryCondition, problem);
  
  if(rkOrder == 4)
    rk4(cnew, c, dt, f, boundaryCondition, problem);
  
  if(rkOrder == 5)
    rk5(cnew, c, dt, f, boundaryCondition, problem);
  
  if(rkOrder == 6)
    rk6(cnew, c, dt, f, boundaryCondition, problem);  
}


#endif
