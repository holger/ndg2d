#ifndef geo_cubedSphere_hpp
#define geo_cubedSphere_hpp

#include <memory>
#include <cmath>

#include <BasisKlassen/array.hpp>
#include <BasisKlassen/vector.hpp>

#include "dg2d.hpp"
#include "rk.hpp"
#include "writeArray2d.hpp"

typedef BK::Array<BK::Array<double,2>, 2> Mat2x2;

inline void matrixMult(Mat2x2& result, Mat2x2 const& a, Mat2x2 const& b) {
  for(int i=0; i<2; i++)
    for(int j=0; j<2; j++) {
      result[i][j] = 0;
      for(int l=0; l<2; l++)
	result[i][j] += a[i][l]*b[l][j];
    }
}

inline void matrixMultVec(BK::Array<double,2>& result, Mat2x2 const& a, BK::Array<double,2> const& b) {
  for(int i=0; i<2; i++) {
    result[i] = 0;
    for(int l=0; l<2; l++)
      result[i] += a[i][l]*b[l];
  }
}

inline bool matrixIdentityTest(Mat2x2 const& a)
{
  bool identity = true;

  if(fabs(a[0][0]) - 1 > 1e-15) identity = false;
  if(fabs(a[0][1])     > 1e-15) identity = false;
  if(fabs(a[1][0])     > 1e-15) identity = false;
  if(fabs(a[1][1]) - 1 > 1e-15) identity = false;
  
  return identity;
}

class GeoCubedSphere
{
public:

  
  GeoCubedSphere(size_t ncelX, size_t ncelY) : ncel_(ncelX) {
    cVec.resize(6, Problem::Array(ncel_, ncel_, order, order));
    rhsVec.resize(6, Rhs2d(ncel_, ncel_, problem.get_dx(), problem.get_dy()));
    rkVec.resize(6);

    da_ = M_PI/2./ncel_;
    db_ = M_PI/2./ncel_;

    for(int i=0;i<6;i++) {
      transMatam[i].resize(ncel_*order);
      transMatap[i].resize(ncel_*order);
      transMatbm[i].resize(ncel_*order);
      transMatbp[i].resize(ncel_*order);
      transInvMatam[i].resize(ncel_*order);
      transInvMatap[i].resize(ncel_*order);
      transInvMatbm[i].resize(ncel_*order);
      transInvMatbp[i].resize(ncel_*order);
    }

    transMat12.resize(ncel_*order);
    transMat21.resize(ncel_*order);
    transMat23.resize(ncel_*order);
    transMat32.resize(ncel_*order);
    transMat34.resize(ncel_*order);
    transMat43.resize(ncel_*order);
    transMat41.resize(ncel_*order);
    transMat14.resize(ncel_*order);

    transMat15.resize(ncel_*order);
    transMat51.resize(ncel_*order);
    transMat53.resize(ncel_*order);
    transMat35.resize(ncel_*order);
    transMat36.resize(ncel_*order);
    transMat63.resize(ncel_*order);
    transMat61.resize(ncel_*order);
    transMat16.resize(ncel_*order);

    transMat52.resize(ncel_*order);
    transMat25.resize(ncel_*order);
    transMat26.resize(ncel_*order);
    transMat62.resize(ncel_*order);
    transMat64.resize(ncel_*order);
    transMat46.resize(ncel_*order);
    transMat45.resize(ncel_*order);
    transMat54.resize(ncel_*order);
    
    Mat2x2 test = {{11,12},{21,22}};


      for(size_t o = 0; o<order; o++) {
	double point = (points[order-3][o]+1)/2;

	for(size_t ai = 0; ai < ncel_; ai++) {

	  // face 1 ----------------------------------------------------------------------
	  double a0 = -M_PI/4;
	  
	  double a = a0 + ai*da_ + point*da_;	  

	  double b = -M_PI/4;                   // computation of bottom boundary
	  compTransMat1(a,b, transMatam[0][ai*order + o], transInvMatam[0][ai*order + o]);
	  
	  b = M_PI/4; 	                        // computation of top boundary
	  compTransMat1(a,b, transMatap[0][ai*order + o], transInvMatap[0][ai*order + o]); 
	  
	  // face 5 (north pole) --------------------------------------------------------------
	  
	  b = -M_PI/4;                          // computation of bottom boundary
	  compTransMat5(a,b, transMatam[4][ai*order + o], transInvMatam[4][ai*order + o]);
	  
	  b = M_PI/4;                           // computation of top boundary
	  compTransMat5(a,b, transMatap[4][ai*order + o], transInvMatap[4][ai*order + o]);
	}
	
      	for(size_t bi = 0; bi < ncel_; bi++) {

	  // face 1 ----------------------------------------------------------------------
	  double b0 = -M_PI/4;
	  
	  double b = b0 + bi*db_ + point*db_;	  

	  double a = -M_PI/4;                   // computation of left boundary
	  compTransMat1(a,b, transMatbm[0][bi*order + o], transInvMatbm[0][bi*order + o]);
	  
	  a = M_PI/4; 	                        // computation of right boundary
	  compTransMat1(a,b, transMatbp[0][bi*order + o], transInvMatbp[0][bi*order + o]); 
	  
	  // face 5 (north pole) --------------------------------------------------------------
	  
	  a = -M_PI/4;                          // computation of bottom boundary
	  compTransMat5(a,b, transMatbm[4][bi*order + o], transInvMatbm[4][bi*order + o]);
	  
	  a = M_PI/4;                           // computation of top boundary
	  compTransMat5(a,b, transMatbp[4][bi*order + o], transInvMatbp[4][bi*order + o]);
	}
}

    // face 2 - 4 ----------------------------------------------------------------------
    
    for(int face = 1; face < 4; face++) {
      transMatam[face] = transMatam[0];
      transMatap[face] = transMatap[0];
      transInvMatam[face] = transInvMatam[0];
      transInvMatap[face] = transInvMatap[0];
      transMatbm[face] = transMatbm[0];
      transMatbp[face] = transMatbp[0];
      transInvMatbm[face] = transInvMatbm[0];
      transInvMatbp[face] = transInvMatbp[0];
    }

    // face 6 (south pole) --------------------------------------------------------------
    transMatam[5] = -transMatam[4]; 
    transMatap[5] = -transMatap[4];
    transInvMatam[5] = -transInvMatam[4];
    transInvMatap[5] = -transInvMatap[4];
    transMatbm[5] = -transMatbm[4]; 
    transMatbp[5] = -transMatbp[4];
    transInvMatbm[5] = -transInvMatbm[4];
    transInvMatbp[5] = -transInvMatbp[4];

    // transMatbm[2][0] = {{1,2},{3,4}};
    // transInvMatbp[0][0] = {{5,6},{7,8}};

    Mat2x2 identity;


    for(size_t p=0; p<ncel_*order; p++) {
      matrixMult(transMat12[p], transMatbm[1][p], transInvMatbp[0][p]);
      matrixMult(transMat21[p], transMatbp[0][p], transInvMatbm[1][p]);
      matrixMult(identity, transMat12[p], transMat21[p]);
      if(!matrixIdentityTest(identity)) { std::cout << "ERROR 12\n"; exit(1);}
      
      matrixMult(transMat23[p], transMatbm[2][p], transInvMatbp[1][p]);
      matrixMult(transMat32[p], transMatbp[1][p], transInvMatbm[2][p]);
      matrixMult(identity, transMat23[p], transMat32[p]);
      if(!matrixIdentityTest(identity)) { std::cout << "ERROR 23\n"; exit(1);}
      
      matrixMult(transMat34[p], transMatbm[3][p], transInvMatbp[2][p]);
      matrixMult(transMat43[p], transMatbp[2][p], transInvMatbm[3][p]);
      matrixMult(identity, transMat34[p], transMat43[p]);
      if(!matrixIdentityTest(identity)) { std::cout << "ERROR 34\n"; exit(1);}
      
      matrixMult(transMat41[p], transMatbm[0][p], transInvMatbp[3][p]);
      matrixMult(transMat14[p], transMatbp[3][p], transInvMatbm[0][p]);
      matrixMult(identity, transMat41[p], transMat14[p]);
      if(!matrixIdentityTest(identity)) { std::cout << "ERROR 41\n"; exit(1);}

      // --
      
      matrixMult(transMat15[p], transMatam[4][p], transInvMatap[0][p]);
      matrixMult(transMat51[p], transMatap[0][p], transInvMatam[4][p]);
      matrixMult(identity, transMat15[p], transMat51[p]);
      if(!matrixIdentityTest(identity)) { std::cout << "ERROR 15\n"; exit(1);}

      matrixMult(transMat53[p], transMatap[2][p], transInvMatap[4][p]);
      matrixMult(transMat35[p], transMatap[4][p], transInvMatap[2][p]);
      matrixMult(identity, transMat53[p], transMat35[p]);
      if(!matrixIdentityTest(identity)) { std::cout << "ERROR 53\n"; exit(1);}

      matrixMult(transMat36[p], transMatam[5][p], transInvMatam[2][p]);
      matrixMult(transMat63[p], transMatam[2][p], transInvMatam[5][p]);
      matrixMult(identity, transMat36[p], transMat63[p]);
      if(!matrixIdentityTest(identity)) { std::cout << "ERROR 36\n"; exit(1);}

      matrixMult(transMat61[p], transMatam[0][p], transInvMatap[5][p]);
      matrixMult(transMat16[p], transMatap[5][p], transInvMatam[0][p]);
      matrixMult(identity, transMat61[p], transMat16[p]);
      if(!matrixIdentityTest(identity)) { std::cout << "ERROR 61\n"; exit(1);}

      // --

      matrixMult(transMat52[p], transMatap[1][p], transInvMatbp[4][p]);
      matrixMult(transMat25[p], transMatbp[4][p], transInvMatap[1][p]);
      matrixMult(identity, transMat52[p], transMat25[p]);
      if(!matrixIdentityTest(identity)) { std::cout << "ERROR 52\n"; exit(1);}

      matrixMult(transMat26[p], transMatbp[5][p], transInvMatam[1][p]);
      matrixMult(transMat62[p], transMatam[1][p], transInvMatbp[5][p]);
      matrixMult(identity, transMat26[p], transMat62[p]);
      if(!matrixIdentityTest(identity)) { std::cout << "ERROR 26\n"; exit(1);}

      matrixMult(transMat64[p], transMatam[3][p], transInvMatbm[5][p]);
      matrixMult(transMat46[p], transMatbm[5][p], transInvMatam[3][p]);
      matrixMult(identity, transMat64[p], transMat46[p]);
      if(!matrixIdentityTest(identity)) { std::cout << "ERROR 64\n"; exit(1);}

      matrixMult(transMat45[p], transMatbm[4][p], transInvMatap[3][p]);
      matrixMult(transMat54[p], transMatap[3][p], transInvMatbm[4][p]);
      matrixMult(identity, transMat45[p], transMat54[p]);
      if(!matrixIdentityTest(identity)) { std::cout << "ERROR 45\n"; exit(1);}
    }
  }

  void compTransMat1(double a, double b, Mat2x2 & transMat, Mat2x2 & transInvMat) {
    auto xyz = ab2xyz_P1(a,b);
    auto pt = xyz2pt(xyz[0], xyz[1], xyz[2]);
    double phi = pt[0];
    double theta = pt[1];
    double x = tan(phi);
    double y = 1./tan(theta)*cos(phi);
    double delta = 1+x*x+y*y;
    double c = sqrt(1+x*x);
    double d = sqrt(1+y*y);
    
    transMat = {{0, c*d/pow(delta,0.5)},{-1, x*y/pow(delta,0.5)}};
    transInvMat = {{x*y/(c*d), -1},{sqrt(delta)/(c*d), 0}};
  }

  void compTransMat5(double a, double b, Mat2x2 & transMat, Mat2x2 & transInvMat) {
    auto xyz = ab2xyz_P5(a,b);
    auto pt = xyz2pt(xyz[0], xyz[1], xyz[2]);
    double phi = pt[0];
    double theta = pt[1];
    double x = tan(theta)*sin(phi);
    double y = -tan(theta)*cos(phi);
    double delta = 1+x*x+y*y;
    double c = sqrt(1+x*x);
    double d = sqrt(1+y*y);
    
    transMat = {{d*x/sqrt(delta-1), -d*y/sqrt(delta)/sqrt(delta-1)},
		{c*y/sqrt(delta-1), c*x/sqrt(delta)/sqrt(delta-1)}};
    
    transInvMat = {{x*sqrt(delta-1)/(d*(x*x+y*y)), y*sqrt(delta-1)/(c*(x*x+y*y))},
		   {-sqrt(delta)*sqrt(delta-1)*y/(d*(x*x+y*y)), sqrt(delta)*sqrt(delta-1)*x/(c*(x*x+y*y))}};
  }

  BK::Array<double, 3> ab2xyz_P1(double a, double b)
  {
    double R=1;
    double xTemp = tan(a);
    double yTemp = tan(b);
    double r=sqrt(1 + BK::sqr(xTemp) + BK::sqr(yTemp));

    return BK::Array<double, 3>{R/r, R*xTemp/r, R*yTemp/r};
  }

  BK::Array<double, 3> ab2xyz_P2(double a, double b)
  {
    double R=1;
    double xTemp = tan(a);
    double yTemp = tan(b);
    double r=sqrt(1 + BK::sqr(xTemp) + BK::sqr(yTemp));

    return BK::Array<double, 3>{-R*xTemp/r, R/r, R*yTemp/r};
  }

  BK::Array<double, 3> ab2xyz_P3(double a, double b)
  {
    double R=1;
    double xTemp = tan(a);
    double yTemp = tan(b);
    double r=sqrt(1 + BK::sqr(xTemp) + BK::sqr(yTemp));

    return BK::Array<double, 3>{-R/r, -R*xTemp/r, R*yTemp/r};
  } 

  BK::Array<double, 3> ab2xyz_P4(double a, double b)
  {
    double R=1;
    double xTemp = tan(a);
    double yTemp = tan(b);
    double r=sqrt(1 + BK::sqr(xTemp) + BK::sqr(yTemp));

    return BK::Array<double, 3>{R*xTemp/r, -R/r, R*yTemp/r};
  } 

  BK::Array<double, 3> ab2xyz_P5(double a, double b)
  {
    double R=1;
    double xTemp = tan(a);
    double yTemp = tan(b);
    double r=sqrt(1 + BK::sqr(xTemp) + BK::sqr(yTemp));

    return BK::Array<double, 3>{-R*yTemp/r, R*xTemp/r, R/r};
  } 

  BK::Array<double, 3> ab2xyz_P6(double a, double b)
  {
    double R=1;
    double xTemp = tan(a);
    double yTemp = tan(b);
    double r=sqrt(1 + BK::sqr(xTemp) + BK::sqr(yTemp));

    return BK::Array<double, 3>{R*yTemp/r, R*xTemp/r, -R/r};
  } 


  BK::Array<double, 2> xyz2pt(double x, double y, double z)
  {
    double R=1;
    return BK::Array<double, 2>{atan(y/(x+1e-10)), acos(z/R)};
  }
  
  double initialCondition(double phi, double theta)
  {
    double h0 = 1;
    double r0 = 0.5;

    double phi0 = 1.5*M_PI;
    double theta0 = 0;

    double deltaPhi = fabs(phi0 - phi);
    if(deltaPhi > M_PI)
      deltaPhi = fabs(M_PI - deltaPhi);
    
    double centralAngle = acos(sin(theta-M_PI/2)*sin(theta0-M_PI/2) + cos(theta-M_PI/2)*cos(theta0-M_PI/2)*cos(deltaPhi));

    double R=1;
    double rd = R*centralAngle;
    double value = h0/2*(1+cos(M_PI*rd/r0));
    if(rd > r0)
      value = 0;
      
    return value;
  }

  void init() {
    std::fill(cVec[0].begin(), cVec[0].end(), 0);
    std::fill(cVec[1].begin(), cVec[1].end(), 0);

    BK::Vector<double> xi = points[order-3];
    
    Problem::Array& c1 = cVec[0];
    Problem::Array& c2 = cVec[1];
    Problem::Array& c3 = cVec[2];
    Problem::Array& c4 = cVec[3];
    Problem::Array& c5 = cVec[4];
    Problem::Array& c6 = cVec[5];
    
    for(size_t a = 0; a<ncel_; a++) {
      auto transA = [a, this](double x) { return -M_PI/4. + a*da_+ 0.5*da_ + x*0.5*da_;};
      for(size_t b = 0; b<ncel_; b++) {
	auto transB = [b, this](double y) { return -M_PI/4. + b*db_+ 0.5*db_ + y*0.5*db_;};
	for(size_t ia = 0; ia < order; ia++)
	  for(size_t ib = 0; ib < order; ib++) {
	    
	    BK::Array<double,3> xyz = ab2xyz_P1(transA(xi[ia]), transB(xi[ib]));
	    BK::Array<double,2> pt = xyz2pt(xyz[0], xyz[1], xyz[2]);
	    c1(a, b, ia, ib) = initialCondition(pt[0], pt[1]);

	    xyz = ab2xyz_P2(transA(xi[ia]), transB(xi[ib]));
	    pt = xyz2pt(xyz[0], xyz[1], xyz[2]);
	    c2(a, b, ia, ib) = initialCondition(pt[0], pt[1]);
	    
	    xyz = ab2xyz_P3(transA(xi[ia]), transB(xi[ib]));
	    pt = xyz2pt(xyz[0], xyz[1], xyz[2]);
	    c3(a, b, ia, ib) = initialCondition(pt[0], pt[1]);

	    xyz = ab2xyz_P4(transA(xi[ia]), transB(xi[ib]));
	    pt = xyz2pt(xyz[0], xyz[1], xyz[2]);
	    c4(a, b, ia, ib) = initialCondition(pt[0], pt[1]);

	    xyz = ab2xyz_P5(transA(xi[ia]), transB(xi[ib]));
	    pt = xyz2pt(xyz[0], xyz[1], xyz[2]);
	    c5(a, b, ia, ib) = initialCondition(pt[0], pt[1]);

	    xyz = ab2xyz_P6(transA(xi[ia]), transB(xi[ib]));
	    pt = xyz2pt(xyz[0], xyz[1], xyz[2]);
	    c6(a, b, ia, ib) = initialCondition(pt[0], pt[1]);
	  }
      }
    }

    BK::Vector<BK::Array<double,3>> points;
    std::ofstream out("points.csv");
    for(size_t a = 0; a<ncel_; a++) {
      auto transA = [a, this](double x) { return -M_PI/4. + a*da_+ 0.5*da_ + x*0.5*da_;};
      for(size_t b = 0; b<ncel_; b++) {
	auto transB = [b, this](double y) { return -M_PI/4. + b*db_ + 0.5*db_ + y*0.5*db_;};
	for(size_t ia = 0; ia < order; ia++)
	  for(size_t ib = 0; ib < order; ib++) {
	    BK::Array<double,3> xyz = ab2xyz_P1(transA(xi[ia]), transB(xi[ib]));
	    out << xyz[0] << "," << xyz[1] << "," << xyz[2] << "," << c1(a, b, ia, ib)[0] << std::endl;
	    xyz = ab2xyz_P2(transA(xi[ia]), transB(xi[ib]));
	    out << xyz[0] << "," << xyz[1] << "," << xyz[2] << "," << c2(a, b, ia, ib)[0] << std::endl;
	    xyz = ab2xyz_P3(transA(xi[ia]), transB(xi[ib]));
	    out << xyz[0] << "," << xyz[1] << "," << xyz[2] << "," << c3(a, b, ia, ib)[0] << std::endl;
	    xyz = ab2xyz_P4(transA(xi[ia]), transB(xi[ib]));
	    out << xyz[0] << "," << xyz[1] << "," << xyz[2] << "," << c4(a, b, ia, ib)[0] << std::endl;
	    xyz = ab2xyz_P5(transA(xi[ia]), transB(xi[ib]));
	    out << xyz[0] << "," << xyz[1] << "," << xyz[2] << "," << c5(a, b, ia, ib)[0] << std::endl;
	    xyz = ab2xyz_P6(transA(xi[ia]), transB(xi[ib]));
	    out << xyz[0] << "," << xyz[1] << "," << xyz[2] << "," << c6(a, b, ia, ib)[0] << std::endl;
	  }
      }
    }

    out.close();

    // problem.initialize(cVec[0]);
    // std::cerr << "problem.get_lambda() = " << problem.get_lambda() << std::endl;
    // problem.initialize(cVec[1]);

    std::string dirName = BK::toString("data/", problem.get_name());
    std::cerr << "writing patched data ...\n";

    
    writeFine(cVec[0], dirName + "/initCubed_" +  std::to_string(order) + "-" +  std::to_string(3) + "_" + std::to_string(ncel_) + "_" + std::to_string(ncel_) + "-0", 10, problem.get_dx(), problem.get_dy());
    writeFine(cVec[1], dirName + "/initCubed_" +  std::to_string(order) + "-" +  std::to_string(3) + "_" + std::to_string(ncel_) + "_" + std::to_string(ncel_) + "-1", 10, problem.get_dx(), problem.get_dy());
    
  }

  void boundaryCondition(BK::Vector<Rhs2d> & rhsVec);

  void solve() {

    double dt = problem.get_dt();
    
    BK::Vector<Problem::Array> cnewVec(6);
    for(size_t i=0; i<6; i++) {
      cnewVec[i].resize(ncel_, ncel_, order, order);
      cnewVec[i] = cVec[i];
      rkVec[i] = getRkObject(dt, cnewVec[i]);
    }

    double time = 0;
    Problem::Array rhs(cVec[0].dim());
    for(int t=0;t<problem.get_nt()/5;t++){
      std::cout << "t = " << t << "\t time = " << time << std::endl;

      for(int stage = 0; stage < rkVec[0]->get_stageNumber(); stage++) {
	for(size_t i = 0; i<6; i++) 
	  problem.computeFluxes(rhsVec[i].fluxArray, rhsVec[i].fluxInfoBordArrayX, rhsVec[i].fluxInfoBordArrayY, cnewVec[i]);
	
	boundaryCondition(rhsVec);
	
	for(size_t i = 0; i<6; i++) {
	  rhsVec[i](cnewVec[i], rhs);
	  rkVec[stage]->advance(cnewVec[i], cVec[i], rhs);
	}
      }

      for(size_t i = 0; i<6; i++) {
	cVec[i]=cnewVec[i];
      }
      
      time += dt;
    }
  }

  BK::Vector<Problem::Array> const& get_c() {
    return cVec;
  }

private:
  CONSTPARA(size_t, ncel);
  CONSTPARA(double, da);
  CONSTPARA(double, db);
  
  BK::Vector<Problem::Array> cVec;
  BK::Vector<Rhs2d> rhsVec;
  BK::Vector<std::shared_ptr<Rk>> rkVec;

  BK::Array<BK::Vector<Mat2x2>,6> transMatam;
  BK::Array<BK::Vector<Mat2x2>,6> transMatap;
  BK::Array<BK::Vector<Mat2x2>,6> transMatbm;
  BK::Array<BK::Vector<Mat2x2>,6> transMatbp;
  BK::Array<BK::Vector<Mat2x2>,6> transInvMatam;
  BK::Array<BK::Vector<Mat2x2>,6> transInvMatap;
  BK::Array<BK::Vector<Mat2x2>,6> transInvMatbm;
  BK::Array<BK::Vector<Mat2x2>,6> transInvMatbp;

  BK::Vector<Mat2x2> transMat12;
  BK::Vector<Mat2x2> transMat21;
  BK::Vector<Mat2x2> transMat23;
  BK::Vector<Mat2x2> transMat32;
  BK::Vector<Mat2x2> transMat34;
  BK::Vector<Mat2x2> transMat43;
  BK::Vector<Mat2x2> transMat41;
  BK::Vector<Mat2x2> transMat14;

  BK::Vector<Mat2x2> transMat15;
  BK::Vector<Mat2x2> transMat51;
  BK::Vector<Mat2x2> transMat53;
  BK::Vector<Mat2x2> transMat35;
  BK::Vector<Mat2x2> transMat36;
  BK::Vector<Mat2x2> transMat63;
  BK::Vector<Mat2x2> transMat61;
  BK::Vector<Mat2x2> transMat16;

  BK::Vector<Mat2x2> transMat52;
  BK::Vector<Mat2x2> transMat25;
  BK::Vector<Mat2x2> transMat26;
  BK::Vector<Mat2x2> transMat62;
  BK::Vector<Mat2x2> transMat64;
  BK::Vector<Mat2x2> transMat46;
  BK::Vector<Mat2x2> transMat45;
  BK::Vector<Mat2x2> transMat54;
};

#endif
