#ifndef problemBase2d_hpp
#define problemBase2d_hpp

template<int varDim>
class ProblemBase2d
{
public:

  ProblemBase2d() {
    initialized_ = false;
    
    varDim_ = varDim;
  }

  void logParameter() {
    std::cerr << "name = " << name_ << std::endl;
    std::cerr << PAR(parameterFilename_) << std::endl;
    std::cerr << "ncelx = " << ncelx_ << std::endl;
    std::cerr << "ncely = " << ncely_ << std::endl;
    std::cerr << "lx = " << lx_ << std::endl;
    std::cerr << "ly = " << ly_ << std::endl;
    std::cerr << "T = " << T_ << "\t cfl = " << cfl_ << "\t dt = " << dt_ << std::endl;
    std::cerr << "nt = " << nt_ << std::endl;
    std::cerr << "dx = " << dx_ << std::endl;
    std::cerr << "dy = " << dy_ << std::endl;
    std::cerr << "nt = " << nt_ << std::endl;
  }

  PROTEPARA(std::string, parameterFilename);
  PROTEPARA(std::string, name);
  PROTEPARA(size_t, ncelx);
  PROTEPARA(size_t, ncely);
  PROTEPARA(double, lx);
  PROTEPARA(double, ly);
  PROTEPARA(double, dx);
  PROTEPARA(double, dy);
  PROTEPARA(double, T);
  PROTEPARA(double, dt);
  PROTEPARA(double, cfl);
  PROTEPARA(int, nt);
  PROTEPARA(bool, initialized);
  PROTEPARA(int, varDim);
  PROTEPARA(int, rkOrder);
};

#endif
