#ifndef dg2d_hpp
#define dg2d_hpp

#include "order.hpp"
#include "problem2d.hpp"

// 2D version -----------------------------------------------------------------------

class Rhs2d
{
public:

  Rhs2d(size_t ncelx, size_t ncely, double dx, double dy) : coeffX{order, order, order}, 
							    coeffY{order, order, order}
  {
    dx_ = dx;
    dy_ = dy;
    ncelX_ = ncelx ;
    ncelY_ = ncely ;
    
    wi = weights[order-3];
    DLagrangeMatrix<order> dLagrangeMatrix;
    
    for(int orderX=0; orderX<order; orderX++)
      for(int orderY=0; orderY<order; orderY++)
	for(int i=0; i<order; i++) {
	  coeffX(orderX, orderY, i) = dy_/2.*wi[i]*wi[orderY]*dLagrangeMatrix(orderX,i);
	  coeffY(orderX, orderY, i) = dx_/2.*wi[orderX]*wi[i]*dLagrangeMatrix(orderY,i);
	}

    fluxArray.resize(ncelX_, ncelY_, order, order);
    fluxInfoBordArrayX.resize(ncelX_+1, ncelY_, order);
    fluxInfoBordArrayY.resize(ncelX_, ncelY_+1, order);
    fluxVecArrayX.resize(ncelX_+1, ncelY_, order);
    fluxVecArrayY.resize(ncelX_, ncelY_+1, order);
  }  

  template<class Array>
  void operator()(Array const& c, Array & rhs);

  Problem::FluxArray fluxArray;
  Problem::FluxInfoBordArray fluxInfoBordArrayX;
  Problem::FluxInfoBordArray fluxInfoBordArrayY;
  Problem::FluxVecArray fluxVecArrayX;
  Problem::FluxVecArray fluxVecArrayY;

private:
  
  BK::MultiArray<3, double> coeffX;
  BK::MultiArray<3, double> coeffY;

  
  BK::Vector<double> wi;

  CONSTPARA(double, dx);
  CONSTPARA(double, dy);
  CONSTPARA(size_t, ncelX);
  CONSTPARA(size_t, ncelY);
};

template<class Array>
void Rhs2d::operator()(Array const& c, Array & rhs) {

  problem.computeNumericalFlux(fluxVecArrayX, fluxVecArrayY, fluxInfoBordArrayX, fluxInfoBordArrayY);
  
  for(int cellX=0; cellX<int(c.size(0)); cellX++)
    for(int cellY=0; cellY<int(c.size(1)); cellY++)
      for(int orderX=0; orderX<int(c.size(2)); orderX++)
	for(int orderY=0; orderY<int(c.size(3)); orderY++) {
	  
	  rhs(cellX, cellY, orderX, orderY) = 0;
	  
	  for(int i=0;i<order;i++) 
	    rhs(cellX, cellY, orderX, orderY) += fluxArray(cellX, cellY, i, orderY)[0]*coeffX(orderX, orderY, i)
	      + fluxArray(cellX, cellY, orderX, i)[1]*coeffY(orderX, orderY, i);
	}
  
  for(int cellX=0; cellX<int(c.size(0)-1); cellX++)
    for(int cellY=0; cellY<int(c.size(1)); cellY++)
      for(int orderY=0; orderY<int(c.size(3)); orderY++) {
  	Problem::Vector fluxXR = fluxVecArrayX(cellX+1, cellY, orderY)*problem.get_dy()/2.*wi[orderY];
  	rhs(cellX, cellY, order-1, orderY) -= fluxXR;
  	rhs(cellX+1, cellY, 0, orderY) += fluxXR;
      }

  for(int cellY=0; cellY<int(c.size(1)); cellY++)
    for(int orderY=0; orderY<int(c.size(3)); orderY++) {
      rhs(0, cellY, 0, orderY) += fluxVecArrayX(0, cellY, orderY)*problem.get_dy()/2.*wi[orderY];
      rhs(int(ncelX_)-1, cellY, order-1, orderY) -= fluxVecArrayX(int(ncelX_), cellY, orderY)*problem.get_dy()/2.*wi[orderY];
    }
  
  
  for(int cellX=0; cellX<int(c.size(0)); cellX++)
    for(int cellY=0; cellY<int(c.size(1)-1); cellY++)
      for(int orderX=0; orderX<int(c.size(2)); orderX++) {
  	Problem::Vector fluxYT = fluxVecArrayY(cellX, cellY+1, orderX)*problem.get_dx()/2.*wi[orderX];
  	rhs(cellX, cellY, orderX, order-1) -= fluxYT;
  	rhs(cellX, cellY+1, orderX, 0) += fluxYT;
      }
  
  for(int cellX=0; cellX<int(c.size(0)); cellX++)
    for(int orderX=0; orderX<int(c.size(2)); orderX++) {
      rhs(cellX, 0, orderX, 0) += fluxVecArrayY(cellX, 0, orderX)*problem.get_dx()/2.*wi[orderX];
      rhs(cellX, int(ncelY_)-1, orderX, order-1) -= fluxVecArrayY(cellX, int(ncelY_), orderX)*problem.get_dx()/2.*wi[orderX];
    }

  for(int cellX=0; cellX<int(c.size(0)); cellX++)
    for(int cellY=0; cellY<int(c.size(1)); cellY++)
      for(int orderX=0; orderX<int(c.size(2)); orderX++)
	for(int orderY=0; orderY<int(c.size(3)); orderY++) {
	  double norm = 4./(problem.get_dx()*problem.get_dy()*wi[orderX]*wi[orderY]);
	  rhs(cellX, cellY, orderX, orderY) *= norm;
	}
}

#endif
