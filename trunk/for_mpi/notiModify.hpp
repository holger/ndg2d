#ifndef notiModify_hpp
#define notiModify_hpp

#include <BasisKlassen/notificationHandler.hpp>
#include <BasisKlassen/toString.hpp>

#include "types.hpp"

extern const int spaceDim;
extern const int varDim;

template<class Signature>
class CallBack;

/** @brief CallBack
  *
  * Class for holding a Callback used by the noti/modifaction handlers
  */
template<class TResult, typename ...TArgs>
class CallBack<TResult(TArgs...)>
{
public:
  CallBack(std::function<TResult(TArgs...)> callBack, std::string name) : callBack_(callBack), name_(name) {}

  TResult operator()(TArgs... args) const {
    return callBack_(args...);
  }

  bool operator==(CallBack callBack) {
    return callBack.name_ == name_;
  }

  bool operator==(std::string name) {
    return name == name_;
  }

  std::string get_name() const {
    return name_;
  }
  
private:
  std::function<TResult(TArgs...)> callBack_;
  std::string name_;
};

// ------------------------------------------------------------------------------------------------------------------------
// --------------------------- FIELDS -------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------

typedef CallBack<void(DataTypes<spaceDim, varDim>::Array const*)> FieldNotifyCallBack;
typedef CallBack<void(DataTypes<spaceDim, varDim>::Array *)> FieldModifyCallBack;

/** @brief FieldNotify
  *
  * Class for handling notifications for fields
  */
class FieldNotify : public BK::NotificationHandler<void, CallBack, DataTypes<spaceDim, varDim>::Array const*>
{
 public:
  void exec(std::string key, DataTypes<spaceDim, varDim>::Array const* fieldVec) {

    //    LOGL(3,BK::toString("FieldNotify::exec(",key,",",fieldVec[0]->get_name(),")"));
    
    BK::NotificationHandler<void, CallBack, DataTypes<spaceDim, varDim>::Array const*>::exec(key, fieldVec);
    
    //    LOGL(4,BK::toString("FieldNotify::exec(",key,",",fieldVec[0]->get_name(),")-end"));
  }
};

extern FieldNotify& notifyField;
//extern FieldModify& modifyField;

#endif
