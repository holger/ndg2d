#ifndef boundaryCondition_hpp
#define boundaryCondition_hpp
#include <mpiFlux.h>
// class PeriodicBoundaryCondition1d
// {
// public:
//   template<class Array>
//   void operator()(Array& c)
//   {
//     int ncelX = c.size(0);
//     int orderX = c.size(1);
    
//     for(int ox=0; ox<orderX; ox++) {
//       c(0, ox) = c(ncelX-2, ox);
//       c(ncelX-1, ox) = c(1, ox);
//     }
//   }
// };

class PeriodicBoundaryCondition1dFlux
{
public:
  template<class FluxInfoVecBordArray>
  void operator()(FluxInfoVecBordArray& fluxInfoArray)
  {
    size_t ncel = fluxInfoArray.size();

    enum Val { minus, plus };
    
    fluxInfoArray(0)[Val::minus] = fluxInfoArray(ncel-1)[Val::minus];
    fluxInfoArray(ncel-1)[Val::plus] = fluxInfoArray(0)[Val::plus];  
  }
};

class PeriodicBoundaryCondition2dFlux
{
public:
  template<class FluxInfoVecBordArray>
  void operator()(FluxInfoVecBordArray& fluxInfoArrayX, FluxInfoVecBordArray& fluxInfoArrayY)
  {
    //    std::cerr << "PeriodicBoundaryCondition2dFlux::operator()\n";
    
    enum Val { minus, plus };

    auto dimensionsX = fluxInfoArrayX.dim();
    int ncelx = dimensionsX[0];
    int ncely = dimensionsX[1];

    /*for(int iy=0; iy<ncely; iy++) 
      for(int oy=0; oy<order; oy++){
      fluxInfoArrayX(0, iy, oy)[Val::minus] = fluxInfoArrayX(ncelx-1, iy, oy)[Val::minus];
      fluxInfoArrayX(ncelx-1, iy, oy)[Val::plus] = fluxInfoArrayX(0, iy, oy)[Val::plus];
      }*/
  
    XfluxMPI (const int& ncely, FluxInfoVecBordArray& fluxInfoArrayX );

    auto dimensionsY = fluxInfoArrayY.dim();
    ncelx = dimensionsY[0];
    ncely = dimensionsY[1];

    /*for(int ix=0; ix<ncelx; ix++)
      for(int ox=0; ox<order; ox++) {
      fluxInfoArrayY(ix, 0, ox)[Val::minus] = fluxInfoArrayY(ix, ncely-1, ox)[Val::minus];
      fluxInfoArrayY(ix, ncely-1, ox)[Val::plus] = fluxInfoArrayY(ix, 0, ox)[Val::plus];  
      }*/

    YfluxMPI (const int& ncelx, FluxInfoVecBordArray& fluxInfoArrayY );
    
  }
};

class PeriodicBoundaryCondition2dZero
{
public:
  template<class FluxInfoVecBordArray>
  void operator()(FluxInfoVecBordArray& fluxInfoArrayX, FluxInfoVecBordArray& fluxInfoArrayY)
  {
    std::cerr << "PeriodicBoundaryCondition2dZero::operator()\n";
    
    enum Val { minus, plus };

    auto dimensionsX = fluxInfoArrayX.dim();
    int ncelx = dimensionsX[0];
    int ncely = dimensionsX[1];

    for(int iy=0; iy<ncely; iy++) 
      for(int oy=0; oy<order; oy++){
    	fluxInfoArrayX(0, iy, oy)[Val::minus] = 0;
    	fluxInfoArrayX(ncelx-1, iy, oy)[Val::plus] = 0;
      }

    auto dimensionsY = fluxInfoArrayY.dim();
    ncelx = dimensionsY[0];
    ncely = dimensionsY[1];

    for(int ix=0; ix<ncelx; ix++)
      for(int ox=0; ox<order; ox++) {
    	fluxInfoArrayY(ix, 0, ox)[Val::minus] = 0;
    	fluxInfoArrayY(ix, ncely-1, ox)[Val::plus] = 0;
      }
  }
};

class PeriodicBoundaryCondition3d
{
public:
  template<class Array>
  void operator()(Array& c)
  {
    int ncelX = c.size(0);
    int ncelY = c.size(1);
    int ncelZ = c.size(2);
    int orderX = c.size(3);
    int orderY = c.size(4);
    int orderZ = c.size(5);
    
    for(int ox=0; ox<orderX; ox++)
      for(int oy=0; oy<orderY; oy++)
        for(int oz=0; oz<orderZ; oz++)	{
	
	  for(int iy=0; iy<ncelY; iy++)
	    for(int iz=0; iz<ncelZ; iz++){
	      c(0, iy,iz, ox, oy, oz) = c(ncelX-2, iy,iz,  ox, oy, oz);
	      c(ncelX-1, iy,iz,  ox, oy, oz) = c(1, iy, iz, ox, oy, oz);
	    }
	
	  for(int ix=0; ix<ncelX; ix++)
	    for(int iz=0; iz<ncelZ; iz++){
	      c(ix, 0, iz, ox, oy,oz) = c(ix, ncelY-2, iz, ox, oy, oz);
	      c(ix, ncelY-1, iz, ox, oy, oz) = c(ix, 1, iz, ox, oy, oz);
	    }
	  for(int ix=0; ix<ncelX; ix++)
	    for(int iy=0; iy<ncelY; iy++){
	      c(ix, iy, 0, ox, oy,oz) = c(ix, iy, ncelZ-2, ox, oy, oz);
	      c(ix, iy, ncelZ-1, ox, oy, oz) = c(ix, iy, 1, ox, oy, oz);
	
	    }
	}
  }
};


class PeriodicBoundaryCondition2d
{
public:
  template<class Array>
  void operator()(Array& c)
  {
    int ncelX = c.size(0);
    int ncelY = c.size(1);
    int orderX = c.size(2);
    int orderY = c.size(3);
    
    for(int ox=0; ox<orderX; ox++)
      for(int oy=0; oy<orderY; oy++){
	
	for(int iy=0; iy<ncelY; iy++) {
	  c(0, iy, ox, oy) = c(ncelX-2, iy, ox, oy);
	  c(ncelX-1, iy, ox, oy) = c(1, iy, ox, oy);
	}
	
	for(int ix=0; ix<ncelX; ix++) {
	  c(ix, 0, ox, oy) = c(ix, ncelY-2, ox, oy);
	  c(ix, ncelY-1, ox, oy) = c(ix, 1, ox, oy);
	}
      }
  }
};

#endif
