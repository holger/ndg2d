#include "geo_cubedSphere.hpp"

void GeoCubedSphere::boundaryCondition(BK::Vector<Rhs2d> & rhsVec)
  {
    std::cerr << "applying boundary conditions on cubed sphere ...\n";

    std::cout << "number of vectors in fluxInfo = " << problem.vecIndizes.size() << std::endl;
    std::cout << "vector indizes[0] = " << problem.vecIndizes[0] << std::endl;
    std::cout << "vector indizes[1] = " << problem.vecIndizes[1] << std::endl;

    int numOfVectors = problem.vecIndizes.size();

    for(int cell=0; cell< int(ncel_); cell++)
      for(int index=0; index<order; index++) {

	for(int vi=0; vi<numOfVectors; vi++) {
	  auto vecIndizes = problem.vecIndizes[vi];
	  
	  // from face 2 to 1
	  BK::Array<double, 2> vecF1;
	  double fx = rhsVec[1].fluxInfoBordArrayX(0, cell, index)[Val::plus][vecIndizes[0]];
	  double fy = rhsVec[1].fluxInfoBordArrayX(0, cell, index)[Val::plus][vecIndizes[1]];
	  BK::Array<double, 2> vecF2 = {fx, fy};
	  matrixMultVec(vecF1, transMat21[cell*order + index], vecF2);
	  rhsVec[0].fluxInfoBordArrayX(int(ncel_-1), cell, index)[Val::plus][vecIndizes[0]] = vecF1[0];
	  rhsVec[0].fluxInfoBordArrayX(int(ncel_-1), cell, index)[Val::plus][vecIndizes[1]] = vecF1[1];

	  // from face 1 to 4
	  fx = rhsVec[0].fluxInfoBordArrayX(0, cell, index)[Val::plus][vecIndizes[0]];
	  fy = rhsVec[0].fluxInfoBordArrayX(0, cell, index)[Val::plus][vecIndizes[1]];
	  vecF2 = {fx, fy};
	  matrixMultVec(vecF1, transMat14[cell*order + index], vecF2);
	  rhsVec[3].fluxInfoBordArrayX(int(ncel_-1), cell, index)[Val::plus][vecIndizes[0]] = vecF1[0];
	  rhsVec[3].fluxInfoBordArrayX(int(ncel_-1), cell, index)[Val::plus][vecIndizes[1]] = vecF1[1];

	  // from face 4 to 3
	  fx = rhsVec[3].fluxInfoBordArrayX(0, cell, index)[Val::plus][vecIndizes[0]];
	  fy = rhsVec[3].fluxInfoBordArrayX(0, cell, index)[Val::plus][vecIndizes[1]];
	  vecF2 = {fx, fy};
	  matrixMultVec(vecF1, transMat43[cell*order + index], vecF2);
	  rhsVec[2].fluxInfoBordArrayX(int(ncel_-1), cell, index)[Val::plus][vecIndizes[0]] = vecF1[0];
	  rhsVec[2].fluxInfoBordArrayX(int(ncel_-1), cell, index)[Val::plus][vecIndizes[1]] = vecF1[1];

	  // from face 3 to 2
	  fx = rhsVec[2].fluxInfoBordArrayX(0, cell, index)[Val::plus][vecIndizes[0]];
	  fy = rhsVec[2].fluxInfoBordArrayX(0, cell, index)[Val::plus][vecIndizes[1]];
	  vecF2 = {fx, fy};
	  matrixMultVec(vecF1, transMat32[cell*order + index], vecF2);
	  rhsVec[1].fluxInfoBordArrayX(int(ncel_-1), cell, index)[Val::plus][vecIndizes[0]] = vecF1[0];
	  rhsVec[1].fluxInfoBordArrayX(int(ncel_-1), cell, index)[Val::plus][vecIndizes[1]] = vecF1[1];

	  std::cout << vecF1 << "\t" << vecF2 << std::endl;

	  // -----------------------------------
	  
	  // from face 4 to 1	  
	  fx = rhsVec[3].fluxInfoBordArrayX(int(ncel_-1), cell, index)[Val::minus][vecIndizes[0]];
	  fy = rhsVec[3].fluxInfoBordArrayX(int(ncel_-1), cell, index)[Val::minus][vecIndizes[1]];
	  vecF2 = {fx, fy};
	  matrixMultVec(vecF1, transMat41[cell*order + index], vecF2);
	  rhsVec[0].fluxInfoBordArrayX(0, cell, index)[Val::minus][vecIndizes[0]] = vecF1[0];
	  rhsVec[0].fluxInfoBordArrayX(0, cell, index)[Val::minus][vecIndizes[1]] = vecF1[1];

	  // from face 1 to 2
	  fx = rhsVec[0].fluxInfoBordArrayX(int(ncel_-1), cell, index)[Val::minus][vecIndizes[0]];
	  fy = rhsVec[0].fluxInfoBordArrayX(int(ncel_-1), cell, index)[Val::minus][vecIndizes[1]];
	  vecF2 = {fx, fy};
	  matrixMultVec(vecF1, transMat12[cell*order + index], vecF2);
	  rhsVec[1].fluxInfoBordArrayX(0, cell, index)[Val::minus][vecIndizes[0]] = vecF1[0];
	  rhsVec[1].fluxInfoBordArrayX(0, cell, index)[Val::minus][vecIndizes[1]] = vecF1[1];

	  // from face 2 to 3
	  fx = rhsVec[1].fluxInfoBordArrayX(int(ncel_-1), cell, index)[Val::minus][vecIndizes[0]];
	  fy = rhsVec[1].fluxInfoBordArrayX(int(ncel_-1), cell, index)[Val::minus][vecIndizes[1]];
	  vecF2 = {fx, fy};
	  matrixMultVec(vecF1, transMat23[cell*order + index], vecF2);
	  rhsVec[2].fluxInfoBordArrayX(0, cell, index)[Val::minus][vecIndizes[0]] = vecF1[0];
	  rhsVec[2].fluxInfoBordArrayX(0, cell, index)[Val::minus][vecIndizes[1]] = vecF1[1];

	  // from face 3 to 4
	  fx = rhsVec[2].fluxInfoBordArrayX(int(ncel_-1), cell, index)[Val::minus][vecIndizes[0]];
	  fy = rhsVec[2].fluxInfoBordArrayX(int(ncel_-1), cell, index)[Val::minus][vecIndizes[1]];
	  vecF2 = {fx, fy};
	  matrixMultVec(vecF1, transMat34[cell*order + index], vecF2);
	  rhsVec[3].fluxInfoBordArrayX(0, cell, index)[Val::minus][vecIndizes[0]] = vecF1[0];
	  rhsVec[3].fluxInfoBordArrayX(0, cell, index)[Val::minus][vecIndizes[1]] = vecF1[1];

	  // -----------------------------------

	  // from face 1 to 5
	  fx = rhsVec[0].fluxInfoBordArrayY(int(ncel_-1), cell, index)[Val::minus][vecIndizes[0]];
	  fy = rhsVec[0].fluxInfoBordArrayY(int(ncel_-1), cell, index)[Val::minus][vecIndizes[1]];
	  vecF2 = {fx, fy};
	  matrixMultVec(vecF1, transMat15[cell*order + index], vecF2);
	  rhsVec[4].fluxInfoBordArrayY(0, cell, index)[Val::minus][vecIndizes[0]] = vecF1[0];
	  rhsVec[4].fluxInfoBordArrayY(0, cell, index)[Val::minus][vecIndizes[1]] = vecF1[1];

	  // from face 5 to 3
	  fx = rhsVec[4].fluxInfoBordArrayY(int(ncel_-1), cell, index)[Val::minus][vecIndizes[0]];
	  fy = rhsVec[4].fluxInfoBordArrayY(int(ncel_-1), cell, index)[Val::minus][vecIndizes[1]];
	  vecF2 = {fx, fy};
	  matrixMultVec(vecF1, transMat53[cell*order + index], vecF2);
	  rhsVec[2].fluxInfoBordArrayY(int(ncel_-1), int(ncel_-1-cell), order-1-index)[Val::plus][vecIndizes[0]] = vecF1[0];
	  rhsVec[2].fluxInfoBordArrayY(int(ncel_-1), int(ncel_-1-cell), order-1-index)[Val::plus][vecIndizes[1]] = vecF1[1];

	  // from face 3 to 6
	  fx = rhsVec[2].fluxInfoBordArrayY(0, cell, index)[Val::plus][vecIndizes[0]];
	  fy = rhsVec[2].fluxInfoBordArrayY(0, cell, index)[Val::plus][vecIndizes[1]];
	  vecF2 = {fx, fy};
	  matrixMultVec(vecF1, transMat36[cell*order + index], vecF2);
	  rhsVec[5].fluxInfoBordArrayY(int(ncel_-1), int(ncel_-1-cell), order-1-index)[Val::minus][vecIndizes[0]] = vecF1[0];
	  rhsVec[5].fluxInfoBordArrayY(int(ncel_-1), int(ncel_-1-cell), order-1-index)[Val::minus][vecIndizes[1]] = vecF1[1];

	  // from face 6 to 1
	  fx = rhsVec[5].fluxInfoBordArrayY(int(ncel_-1), cell, index)[Val::minus][vecIndizes[0]];
	  fy = rhsVec[5].fluxInfoBordArrayY(int(ncel_-1), cell, index)[Val::minus][vecIndizes[1]];
	  vecF2 = {fx, fy};
	  matrixMultVec(vecF1, transMat61[cell*order + index], vecF2);
	  rhsVec[0].fluxInfoBordArrayY(0, int(ncel_-1-cell), order-1-index)[Val::minus][vecIndizes[0]] = vecF1[0];
	  rhsVec[0].fluxInfoBordArrayY(0, int(ncel_-1-cell), order-1-index)[Val::minus][vecIndizes[1]] = vecF1[1];

	  // -----------------------------------

	  // from face 5 to 1
	  fx = rhsVec[4].fluxInfoBordArrayY(0, cell, index)[Val::plus][vecIndizes[0]];
	  fy = rhsVec[4].fluxInfoBordArrayY(0, cell, index)[Val::plus][vecIndizes[1]];
	  vecF2 = {fx, fy};
	  matrixMultVec(vecF1, transMat51[cell*order + index], vecF2);
	  rhsVec[0].fluxInfoBordArrayY(int(ncel_-1), cell, index)[Val::plus][vecIndizes[0]] = vecF1[0];
	  rhsVec[0].fluxInfoBordArrayY(int(ncel_-1), cell, index)[Val::plus][vecIndizes[1]] = vecF1[1];

	  // from face 1 to 6
	  fx = rhsVec[0].fluxInfoBordArrayY(0, cell, index)[Val::plus][vecIndizes[0]];
	  fy = rhsVec[0].fluxInfoBordArrayY(0, cell, index)[Val::plus][vecIndizes[1]];
	  vecF2 = {fx, fy};
	  matrixMultVec(vecF1, transMat16[cell*order + index], vecF2);
	  rhsVec[5].fluxInfoBordArrayY(int(ncel_-1), cell, index)[Val::plus][vecIndizes[0]] = vecF1[0];
	  rhsVec[5].fluxInfoBordArrayY(int(ncel_-1), cell, index)[Val::plus][vecIndizes[1]] = vecF1[1];

	  // from face 6 to 3
	  fx = rhsVec[5].fluxInfoBordArrayY(0, cell, index)[Val::plus][vecIndizes[0]];
	  fy = rhsVec[5].fluxInfoBordArrayY(0, cell, index)[Val::plus][vecIndizes[1]];
	  vecF2 = {fx, fy};
	  matrixMultVec(vecF1, transMat63[cell*order + index], vecF2);
	  rhsVec[2].fluxInfoBordArrayY(0, int(ncel_-1-cell), order-1-index)[Val::minus][vecIndizes[0]] = vecF1[0];
	  rhsVec[2].fluxInfoBordArrayY(0, int(ncel_-1-cell), order-1-index)[Val::minus][vecIndizes[1]] = vecF1[1];

	  // from face 3 to 5
	  fx = rhsVec[2].fluxInfoBordArrayY(int(ncel_-1), cell, index)[Val::minus][vecIndizes[0]];
	  fy = rhsVec[2].fluxInfoBordArrayY(int(ncel_-1), cell, index)[Val::minus][vecIndizes[1]];
	  vecF2 = {fx, fy};
	  matrixMultVec(vecF1, transMat35[cell*order + index], vecF2);
	  rhsVec[2].fluxInfoBordArrayY(int(ncel_-1), int(ncel_-1-cell), order-1-index)[Val::plus][vecIndizes[0]] = vecF1[0];
	  rhsVec[2].fluxInfoBordArrayY(int(ncel_-1), int(ncel_-1-cell), order-1-index)[Val::plus][vecIndizes[1]] = vecF1[1];

	}
	exit(0);
      }
  }
