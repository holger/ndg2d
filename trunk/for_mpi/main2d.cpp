// g++ -o main main.cpp -g -Wall -std=c++11 -DCPP11
//

#include <iostream>
#include <cstdlib>
#include <cmath>
#include <vector>
#include <fstream>
#include <functional>
#include <chrono>
#include <mpi.h>

#include <BasisKlassen/parseFromFile.hpp>

#include "gaussLobatto.hpp"
#include "writeArray2d.hpp"
#include "geo_cart2d.hpp"
#include "geo_cubedSphere.hpp"
#include "notiModify.hpp"

FieldNotify& notifyField = BK::StackSingleton<FieldNotify>::instance();

int main(int argc, char** argv)
{
 MPI_Init(&argc,&argv);//Initialising MPI functions

 int commRank; // id of MPI process
 int commSize; // tot. number of  MPI processes
 MPI_Comm_rank(MPI_COMM_WORLD,&commRank);
 MPI_Comm_size(MPI_COMM_WORLD,&commSize);

 if (commRank == 0 ) {
  std::cout << "Discontinuous Galerkin simulation starts ...\n";

  else if(argc < 2) {
    std::cerr << "ERROR: argc = " << argc << " < 2!.\n";
    std::cerr << "usage: main <parameterFilename>\n";
    exit(1);
  } 
 }

  std::string parameterFilename = argv[1];
  BK::ReadInitFile initFile(parameterFilename,true);

  int ncelxMin = initFile.getParameter<int>("ncelxMin") ; 
  int ncelxMax = initFile.getParameter<int>("ncelxMax") ;
  // int ncelyMin = initFile.getParameter<int>("ncelxMin");
  // int ncelyMax = initFile.getParameter<int>("ncelxMax");
  
  for(int ncelx=ncelxMin;ncelx<=ncelxMax;ncelx*=2) {
    //int ncelx = ncelxx / commSize ; // localising here
    int  ncely=ncelx;
    problem.init(ncelx, ncely, parameterFilename);
    double dx = problem.get_dx();
    double dy = problem.get_dy();
	
    //int m=2;
    int nPoParCel=10;
    
    Problem::Matrix u(ncelx*nPoParCel,ncely*nPoParCel);
    Problem::Matrix pos(ncelx*nPoParCel,ncely*nPoParCel);
    Problem::Matrix uref(ncelx*nPoParCel,ncely*nPoParCel);

    std::cout << "ncelx = " << ncelx << "\t ncely= " << ncely << "\t dx = " << dx <<  "\t dy = " << dy << std::endl;
    std::cout << "lx = " << problem.get_lx() << ", ly = " << problem.get_ly() << std::endl;

    // GeoCubedSphere geoCubed(ncelx, ncely);
    // geoCubed.init();

    // geoCubed.solve();

    std::string dirName = BK::toString("data/", problem.get_name());
    // writeFine(geoCubed.get_c()[0], dirName + "/solCubed_" +  std::to_string(order) + "-" +  std::to_string(3) + "_" + std::to_string(ncelx) + "_" + std::to_string(ncely) + "-0", nPoParCel, dx, dy);
    // writeFine(geoCubed.get_c()[1], dirName + "/solCubed_" +  std::to_string(order) + "-" +  std::to_string(3) + "_" + std::to_string(ncelx) + "_" + std::to_string(ncely) + "-1", nPoParCel, dx, dy);
    
    // exit(0);
    
    GeoCart2d geoCart(ncelx, ncely, parameterFilename );
    geoCart.init();

    size_t rkOrder = problem.get_rkOrder();
    writeFine(geoCart.get_c(), dirName + "/init_" +  std::to_string(order) + "-" +  std::to_string(rkOrder) + "_" + std::to_string(ncelx) + "_" + std::to_string(ncely), nPoParCel, dx, dy);

    auto start = std::chrono::system_clock::now();
    
    geoCart.solve();

    std::cout << "time = " << geoCart.get_time() << std::endl;
    
    auto end = std::chrono::system_clock::now();
    
    std::chrono::duration<double> elapsed_seconds = end-start;
    std::time_t end_time = std::chrono::system_clock::to_time_t(end);
    
    std::cout << "finished computation at " << std::ctime(&end_time)
	      << "elapsed time: " << elapsed_seconds.count() << "s\n";


    int mkdir = system(BK::toString("mkdir data").c_str());
    mkdir = system(BK::toString("mkdir ",dirName).c_str());
    std::cerr << mkdir << std::endl;

    std::string fileName = BK::toString(dirName + "/chrono-ndg-",order,"-",problem.get_rkOrder(),".txt");
    std::ofstream out(fileName.c_str(), std::ios::app);
    out << ncelx << "\t" << elapsed_seconds.count() << std::endl;
    out.close();  

    notifyField.exec("fieldFinal", &geoCart.get_c());

    writeArray(geoCart.get_c(), dirName + "/c.data");

    writeArray(geoCart.get_c(), dirName + "/cnew.data");
    writeFine(geoCart.get_c(), dirName + "/sol_" +  std::to_string(order) + "-" + std::to_string(rkOrder) +"_" + std::to_string(ncelx)+"_"+std::to_string(ncely), nPoParCel, dx, dy);
    // writeBin(geoCart.get_c(), dirName + "/sol_" +  std::to_string(order) + "-" + std::to_string(rkOrder) +"_" + std::to_string(ncelx) + "_" + std::to_string(ncely), 100, dx, dy);
    
    // double errResult2=0;
    // for(int i=0;i<ncel;i++){
    //   double xi=i*dx;
    //   for(int j=0;j<nPoParCel;j++){
    // 	u[i*nPoParCel + j]=c(0,i)+(-1 + 2./(nPoParCel)*j)*c(1,i)+0.5*(3*(2./nPoParCel*j-1)*(2./nPoParCel*j-1)-1)*c(2,i);
    // 	double x=xi+j*(dx/nPoParCel);
    // 	posx[i*nPoParCel+j]=x;
    // 	errResult2+=pow((sin(2*M_PI*x/L)-u[i*nPoParCel + j]),2)*dx;
    // 	uref[i*nPoParCel + j]=sin(2*M_PI*x/L);
    //   }
	
    // }
    // double errResult=sqrt(errResult2);
    // filename = "unew_" + std::to_string(ncel) + ".data";
    // out.open(filename);
    // for(int i=0;i<ncel*nPoParCel;i++){
    //   out<<posx[i]<<"\t"<<u[i]<<"\t"<<uref[i]<<std::endl;
    // }
      
    // out.close();

    // out.open(errResultfilename,std::ios::app);
    // out<<ncel<<"\t"<<errResult<<std::endl;
    // out.close();
    
  }
  MPI_Finalize();
  return 0;
}


 
