#ifndef types_hpp
#define types_hpp

#include <BasisKlassen/multiArray.hpp>
#include <BasisKlassen/array.hpp>

enum Val { minus, plus };

template<int spaceDim, int varDim>
class DataTypes
{
public:
  typedef BK::MultiArray<spaceDim,double> Matrix;
  typedef BK::Array<double,varDim> Vector;
  typedef BK::Array<Vector,spaceDim> VecVec;
  typedef BK::MultiArray<spaceDim*2,Vector> Array;
  typedef BK::MultiArray<spaceDim*2,VecVec> FluxArray;
};
  
#endif
