#ifndef geo_cart2d_hpp
#define geo_cart2d_hpp

#include "dg2d.hpp"
#include "rk.hpp"
#include "problem2d.hpp"
#include "boundaryCondition.hpp"
#include "order.hpp"
#include <mpi.h>

Problem& problem = BK::StackSingleton<Problem>::instance();

class GeoCart2d
{

public:

  GeoCart2d(size_t ncelX, size_t ncelY, std::string parameterFilename) : ncelX_(ncelX), ncelY_(ncelY), parameterFilename_(parameterFilename) {}
  
  int commRank; // id of MPI process
  int commSize; // tot. number of  MPI processes
  MPI_Comm_rank(MPI_COMM_WORLD,&commRank);
  MPI_Comm_size(MPI_COMM_WORLD,&commSize);
  
  void init() {
    std::fill(c.begin(), c.end(), 0);

    BK::ReadInitFile initFile(parameterFilename_,true);

    lx_ = initFile.getParameter<double>("lx");
    ly_ = initFile.getParameter<double>("ly");

    dx_ = lx_/ncelX_;
    dy_ = ly_/ncelY_;
  
    double startvalx = 0.0 + commRank * (lx/commSize);
    double startvaly = 0.0 + commRank * (ly/commSize);

    initialize(c);

    std::cerr << "lx = " << lx_ << std::endl;
    std::cerr << "ly = " << ly_ << std::endl;
    std::cerr << "dx = " << dx_ << std::endl;
    std::cerr << "dy = " << dy_ << std::endl;
  }
  

  void initialize(Problem::Array & c) {
    BK::Vector<double> xi = points[order-3];
    BK::Vector<double> wi = weights[order-3];
    ncelXloc_ = ncelX_ / commSize ; // local here
    ncelYloc_ = ncelY_ / commSize ; // local here
    for(int ix=0;ix<int(ncelXloc_);ix++) {
      auto transX = [ix, this](double x) { return startvalx + ix*dx_+0.5*dx_+x*0.5*dx_;};
      for(int iy=0;iy<int(ncelYloc_);iy++) {
	auto transY = [iy, this](double y) { return startvaly + iy*dy_+0.5*dy_+y*0.5*dy_;};
	
    	for(int ox=0; ox<order;ox++)
    	  for(int oy=0;oy<order;oy++) 
	    c(ix,iy,ox,oy) = problem.initialCondition(transX(xi[ox]), transY(xi[oy]));    	  
      }
    }
  }

  void solve() {
    Problem::Array cnew(c);
    
    Rhs2d rhs2d(c.size(0), c.size(1), problem.get_dx(), problem.get_dy());

    PeriodicBoundaryCondition2dFlux periodicBoundaryCondition2d;
    
    double dt = problem.get_dt();

    std::unique_ptr<Rk> rk = getRkObject(dt, cnew);
    
    time_ = 0;
    cnew=c;
    for(int t=0;t<problem.get_nt();t++){
      std::cout << "t = " << t << "\t time = " << time_ << std::endl;
      
      Problem::Array rhs(c.dim());
      for(int stage = 0; stage < rk->get_stageNumber(); stage++) {
	
	problem.computeFluxes(rhs2d.fluxArray, rhs2d.fluxInfoBordArrayX, rhs2d.fluxInfoBordArrayY, cnew);

	periodicBoundaryCondition2d(rhs2d.fluxInfoBordArrayX, rhs2d.fluxInfoBordArrayY);
	
	rhs2d(cnew, rhs);

      	rk->advance(cnew, c, rhs);
      }
      c=cnew;
      
      time_ += dt;
    }
  }

  Problem::Array const& get_c() {
    return c;
  }

private:
  CONSTPARA(size_t, ncelX);
  CONSTPARA(size_t, ncelY);
  CONSTPARA(double, time);

  PROTEPARA(std::string, parameterFilename);
  CONSTPARA(double, lx);
  CONSTPARA(double, ly);
  CONSTPARA(double, dx);
  CONSTPARA(double, dy);
  
  Problem::Array c{ncelX_, ncelY_, order, order};
};

#endif
