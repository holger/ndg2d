#ifndef problem2d_hpp
#define problem2d_hpp

#ifdef PROBLEM_ADVECTION2D
#include "problem_advection2d.hpp"
#endif

#ifdef PROBLEM_BURGERSDENSITY2D
#include "problem_burgersDensity2d.hpp"
#endif

#ifdef PROBLEM_EULERISO2D
#include "problem_eulerIso2d.hpp"
#endif

#endif
