#ifndef mpiFlux_hpp
#define mpiFlux_hpp
#include <mpi.h>


extern int commRank; // id of MPI process
extern int commSize; // tot. number of  MPI processes
extern MPI_Comm_rank(MPI_COMM_WORLD,&commRank);
extern MPI_Comm_size(MPI_COMM_WORLD,&commSize);

MPI_Status status_send;
MPI_Status status_recv;
MPI_Request request_recv;
MPI_Request request_send;// Identifiers for mpi operation

inline void XfluxMPI (const int& ncely, FluxInfoVecBordArray& fluxInfoArrayX ){
  for(int iy=0; iy<ncely; iy++) 
    for(int oy=0; oy<order; oy++){
      // non-blocking boundary value exchanges
      // MPI_Isend(buf, count, datatype, dest, tag, comm, request) // Central and lax-friedrich flux MPI from here
      MPI_Isend(&fluxInfoArrayX(ncelx-1, iy, oy)[Val::minus], 1, MPI_DOUBLE,(commRank-1+commSize)%commSize,1,MPI_COMM_WORLD, &request_send);
      MPI_Isend(&fluxInfoArrayX(0, iy, oy)[Val::plus], 1, MPI_DOUBLE,(commRank-1+commSize)%commSize,2,MPI_COMM_WORLD, &request_send);
      // MPI_Irecv(buf, count, datatype, source, tag, comm, request) 
      MPI_Irecv(&fluxInfoArrayX(0, iy, oy)[Val::minus], 1, MPI_DOUBLE, (commRank+1)%commSize, 1, MPI_COMM_WORLD, &request_recv);
      MPI_Irecv(&fluxInfoArrayX(ncelx-1, iy, oy)[Val::plus], 1, MPI_DOUBLE, (commRank+1)%commSize, 2, MPI_COMM_WORLD, &request_recv);
      // wait until send and receive completed
      MPI_Wait(&request_send, &status_send);
      MPI_Wait(&request_recv, &status_recv);
    }
}

inline void YfluxMPI (const int& ncelx, FluxInfoVecBordArray& fluxInfoArrayY ){
  for(int iy=0; iy<ncelx; iy++) 
    for(int oy=0; oy<order; oy++){
      // non-blocking boundary value exchanges
      // MPI_Isend(buf, count, datatype, dest, tag, comm, request) // Central and lax-friedrich flux MPI from here
      MPI_Isend(&fluxInfoArrayY(ix, ncely-1, ox)[Val::minus], 1, MPI_DOUBLE,(commRank-1+commSize)%commSize,1,MPI_COMM_WORLD, &request_send);
      MPI_Isend(&fluxInfoArrayY(ix, 0, ox)[Val::plus], 1, MPI_DOUBLE,(commRank-1+commSize)%commSize,2,MPI_COMM_WORLD, &request_send);
      // MPI_Irecv(buf, count, datatype, source, tag, comm, request) 
      MPI_Irecv(&fluxInfoArrayY(ix, 0, ox)[Val::minus], 1, MPI_DOUBLE, (commRank+1)%commSize, 1, MPI_COMM_WORLD, &request_recv);
      MPI_Irecv(&fluxInfoArrayY(ix, ncely-1, ox)[Val::plus], 1, MPI_DOUBLE, (commRank+1)%commSize, 2, MPI_COMM_WORLD, &request_recv);
      // wait until send and receive completed
      MPI_Wait(&request_send, &status_send);
      MPI_Wait(&request_recv, &status_recv);
    }
}
    
MPI_Barrier(MPI_COMM_WORLD);
#endif
