#ifndef problem_advection2d_hpp
#define problem_advection2d_hpp

#include <BasisKlassen/parameter.hpp>
#include <BasisKlassen/singleton.hpp>
#include <BasisKlassen/assert.hpp>
#include <BasisKlassen/parseFromFile.hpp>

const int varDim = 1;
const int spaceDim = 2;

#include "types.hpp"
#include "cfl.hpp"
#include "notiModify.hpp"
#include "problemBase2d.hpp"
#include "value2d.hpp"
#include "laxFriedrichs1d.hpp"

// linear advection 2D
class Problem_Advection2d : public ProblemBase2d<varDim>, public DataTypes<spaceDim, varDim>
{
public:

  typedef BK::Array<double, 5> FluxInfo;                            // [fx, fy, dfx, dfy, value] 
  typedef BK::Array<FluxInfo, 2> FluxInfoBord;                      // [+-][fx, fy, dfx, dfy, value]
  typedef BK::MultiArray<spaceDim+1, FluxInfoBord> FluxInfoBordArray; // (cell)[+-][fx, fy, dfx, dfy, value]
  typedef BK::MultiArray<spaceDim+1, Vector> FluxVecArray;
  
  friend class BK::StackSingleton<Problem_Advection2d>;
  
  Problem_Advection2d() {}

  void errorComp(Array const* c) {
    Array cRef(ncelx_, ncely_, order, order);

    getReference(cRef);

    // writeFine(cRef, dirName + "/ref_" +  std::to_string(order) + "-" + std::to_string(rkOrder_) +"_" + std::to_string(ncelx_)+"_"+std::to_string(ncely_), nPoParCel);

    double error = 0;
    size_t nx = c->size(0);
    size_t ny = c->size(1);
    for(size_t ix = 0; ix < nx; ix++)
      for(size_t iy = 0; iy < ny; iy++) {
	error += BK::sqr(value(ix, iy, 0., 0., *c)[0] - value(ix, iy, 0., 0., cRef)[0])*dx_*dy_;
      }
    error = sqrt(error);

    std::string dirName = BK::toString("data/", name_);
    int mkdir = system(BK::toString("mkdir data").c_str());
    mkdir = system(BK::toString("mkdir ",dirName).c_str());
    std::cerr << mkdir << std::endl;

    std::cerr << "writing error to " << BK::toString(dirName + "/error-ndg-",order,"-",rkOrder_,".txt") << std::endl;

    std::ofstream outChrono(BK::toString(dirName + "/error-ndg-",order,"-",rkOrder_,".txt"), std::ios::app);
    outChrono << nx << "\t" << error << std::endl;
    outChrono.close();
  }
      
  void init(int ncelx, int ncely, std::string parameterFilename) {

    if(!initialized_)
      notifyField.add("fieldFinal", FieldNotifyCallBack(std::bind(&Problem_Advection2d::errorComp,this,std::placeholders::_1), "Problem_Advection2d::errorComp"));
    
    initialized_ = true;

    ncelx_ = ncelx;
    ncely_ = ncely;

    parameterFilename_ = parameterFilename;
    
    BK::ReadInitFile initFile(parameterFilename,true);
    rkOrder_ = initFile.getParameter<size_t>("rkOrder");
    lx_ = initFile.getParameter<double>("lx");
    ly_ = initFile.getParameter<double>("ly");
    ax_ = initFile.getParameter<double>("ax");
    ay_ = initFile.getParameter<double>("ay");
    double cflFactor = initFile.getParameter<double>("cfl");
    int nt = initFile.getParameter<int>("nt");
    double T = initFile.getParameter<double>("T");

    cfl_ = cflFactor*cflLimit[rkOrder_-1][order-1];
    
    // w_ = 2;
    // ox_ = 0.5;
    // oy_ = 0.5;

    dx_ = lx_/ncelx;
    dy_ = ly_/ncely;
    dt_ = fabs(cfl_*dx_);

    name_ = "advection2d";

    if(nt > 0 && T > 0) {
      std::cerr << "ERROR in Problem_Advection1d::init(): nt = " <<
	nt << " > 0 && T = " << T << " > 0." << std::endl;
      exit(1);						   
    }

    double a;
    if(ax_ > 0) a = ax_;
    else a = ay_;
    
    if(T < 0) {
      T_ = -T*fabs(lx_/a);
    }
    else
      T_ = T;

    if(nt > 0)
      nt_ = nt;
    else
      nt_ = int(T_/dt_) + 1;

    dt_ *= T_/(nt_*dt_);

    // T_ = fabs(lx_/ax_);
    // nt_ = size_t(T_/dt_) + 1;
    // dt_ *= T_/(nt_*dt_);
    
    logParameter();
  }

  Vector initialCondition(double x, double y, double shift = 0) {
    assert(initialized_);
    // double x0 = 0.35;
    // double y0 = 0.35;

    // int kMax = 10;
    // double temp = 0;
    // for(int k=1; k<=kMax; k++)
    //   temp += pow(k,-5./6)*sin(2*M_PI*k/lambda_*(x-shift-2*M_PI/(k*k)));

    // return temp;

    int k;
    int kDir;
    bool firstTime = true;
    if(firstTime) {
      BK::ReadInitFile initFile(parameterFilename_,true);
      k = initFile.getParameter<double>("k");
      kDir = initFile.getParameter<double>("kDir");      
      firstTime = false;
    }

    if(kDir == 0)
      return Vector{sin(2*M_PI*k*(x-shift)/lx_)};
    else if(kDir == 1)
      return Vector{sin(2*M_PI*k*(y-shift)/ly_)};
    else {
      std::cerr << "ERROR in Problem_Advetion2d::initialCondition: kDir < 0 || kDir > 1\n";
      exit(0);
    }
    // return Vector{sin(2*M_PI*y/ly)};

    //    return Vector{exp(-((x-x0)*(x-x0)+(y-y0)*(y-y0))/(0.1*0.1))};
  }

  void getReference(Array & c) {
    BK::Vector<double> xi = points[order-3];
    BK::Vector<double> wi = weights[order-3];
    
    for(size_t ix=0;ix<ncelx_;ix++) {
      auto transX = [ix, this](double x) { return ix*dx_+0.5*dx_+x*0.5*dx_;};
      for(size_t iy=0;iy<ncely_;iy++) {
	auto transY = [iy, this](double y) { return iy*dy_+0.5*dy_+y*0.5*dy_;};
	
    	for(size_t ox=0; ox<order;ox++)
    	  for(size_t oy=0;oy<order;oy++) 
	    c(ix,iy,ox,oy) = initialCondition(transX(xi[ox]), transY(xi[oy]), T_*ax_);
	
      }
    }
  }

  void lhs(Array& cNew, Array const& c, Array const& rhs) const {
    cNew = c + dt_*rhs;
  }    

  VecVec flux_vector(int cellX, int cellY, int indexX, int indexY, Array const& c) {
    assert(initialized_);
    
    // double rox = (cellX+x)*dx-ox_;
    // double roy = (cellY+y)*dy-oy_;
    // double vx = -w_ * roy;
    // double vy = w_ * rox;
    
    double vx = ax_;
    double vy = ay_;

    return VecVec{Vector{vx*c(cellX, cellY, indexX, indexY)}, Vector{vy*c(cellX, cellY, indexX, indexY)}};
  }

  VecVec flux_vectorR(int cellX, int cellY, int indexY, Array const& c) {
    assert(initialized_);
    return flux_vector(cellX, cellY, order-1, indexY, c);
  }

  VecVec flux_vectorL(int cellX, int cellY, int indexY, Array const& c) {
    assert(initialized_);
    return flux_vector(cellX, cellY, 0, indexY, c);
  }

  VecVec flux_vectorT(int cellX, int cellY, int indexX, Array const& c) {
    assert(initialized_);
    return flux_vector(cellX, cellY, indexX, order-1, c);
  }

  VecVec flux_vectorB(int cellX, int cellY, int indexX, Array const& c) {
    assert(initialized_);
    return flux_vector(cellX, cellY, indexX, 0, c);
  }

  BK::Array<double,2> dFlux_value(int cellX, int cellY, int indexX, int indexY, Array const& c) {
    assert(initialized_);
    
    // double rox = (cellX+x)*dx-ox_;
    // double roy = (cellY+y)*dy-oy_;
    // double vx = -w_ * roy;
    // double vy = w_ * rox;

    double vx = ax_;
    double vy = ay_;
    
    return BK::Array<double,2>({vx, vy});
  }


  enum Var { fx, fy, dfx, dfy, c};
  enum Dir { x, y};

  void computeFluxes(FluxArray& fluxArray,
		     FluxInfoBordArray& fluxInfoBordArrayX,
		     FluxInfoBordArray& fluxInfoBordArrayY,
		     Array const& c) {

    for(int cellX=0; cellX<int(c.size(0)); cellX++)
      for(int cellY=0; cellY<int(c.size(1)); cellY++)
	for(int indexX=0; indexX<int(c.size(2)); indexX++)
	  for(int indexY=0; indexY<int(c.size(3)); indexY++) {
	    VecVec flux = flux_vector(cellX, cellY, indexX, indexY, c);
	    fluxArray(cellX, cellY, indexX, indexY) = flux;
	    if(indexX == 0) {
	      fluxInfoBordArrayX(cellX, cellY, indexY)[Val::plus][Var::fx] = flux[Dir::x][0];
	      fluxInfoBordArrayX(cellX, cellY, indexY)[Val::plus][Var::fy] = flux[Dir::y][0];
	      fluxInfoBordArrayX(cellX, cellY, indexY)[Val::plus][Var::dfx] = dFlux_value(cellX, cellY, indexX, indexY, c)[Dir::x];
	      fluxInfoBordArrayX(cellX, cellY, indexY)[Val::plus][Var::dfy] = dFlux_value(cellX, cellY, indexX, indexY, c)[Dir::y];
	      fluxInfoBordArrayX(cellX, cellY, indexY)[Val::plus][Var::c] = c(cellX, cellY, indexX, indexY)[0];
	    }
	    if(indexX == order-1) {
	      fluxInfoBordArrayX(cellX+1, cellY, indexY)[Val::minus][Var::fx] = flux[Dir::x][0];
	      fluxInfoBordArrayX(cellX+1, cellY, indexY)[Val::minus][Var::fy] = flux[Dir::y][0];
	      fluxInfoBordArrayX(cellX+1, cellY, indexY)[Val::minus][Var::dfx] = dFlux_value(cellX, cellY, indexX, indexY, c)[Dir::x];
	      fluxInfoBordArrayX(cellX+1, cellY, indexY)[Val::minus][Var::dfy] = dFlux_value(cellX, cellY, indexX, indexY, c)[Dir::y];
	      fluxInfoBordArrayX(cellX+1, cellY, indexY)[Val::minus][Var::c] = c(cellX, cellY, indexX, indexY)[0];
	    }
	    if(indexY == 0) {
	      fluxInfoBordArrayY(cellX, cellY, indexX)[Val::plus][Var::fx] = flux[Dir::x][0];
	      fluxInfoBordArrayY(cellX, cellY, indexX)[Val::plus][Var::fy] = flux[Dir::y][0];
	      fluxInfoBordArrayY(cellX, cellY, indexX)[Val::plus][Var::dfx] = dFlux_value(cellX, cellY, indexX, indexY, c)[Dir::x];
	      fluxInfoBordArrayY(cellX, cellY, indexX)[Val::plus][Var::dfy] = dFlux_value(cellX, cellY, indexX, indexY, c)[Dir::y];
	      fluxInfoBordArrayY(cellX, cellY, indexX)[Val::plus][Var::c] = c(cellX, cellY, indexX, indexY)[0];

	    }
	    if(indexY == order-1) {
	      fluxInfoBordArrayY(cellX, cellY+1, indexX)[Val::minus][Var::fx] = flux[Dir::x][0];
	      fluxInfoBordArrayY(cellX, cellY+1, indexX)[Val::minus][Var::fy] = flux[Dir::y][0];
	      fluxInfoBordArrayY(cellX, cellY+1, indexX)[Val::minus][Var::dfx] = dFlux_value(cellX, cellY, indexX, indexY, c)[Dir::x];
	      fluxInfoBordArrayY(cellX, cellY+1, indexX)[Val::minus][Var::dfy] = dFlux_value(cellX, cellY, indexX, indexY, c)[Dir::y];
	      fluxInfoBordArrayY(cellX, cellY+1, indexX)[Val::minus][Var::c] = c(cellX, cellY, indexX, indexY)[0];
	    }
	  }
  }

  void computeNumericalFlux(FluxVecArray& fluxVecArrayX, FluxVecArray& fluxVecArrayY,
			    FluxInfoBordArray& fluxInfoArrayX, FluxInfoBordArray& fluxInfoArrayY) {

    for(size_t cellX=0; cellX<fluxVecArrayX.size(0); cellX++)
      for(size_t cellY=0; cellY<fluxVecArrayX.size(1); cellY++)
	for(size_t orderY=0; orderY<order; orderY++) {
	  double fl = fluxInfoArrayX(cellX, cellY, orderY)[Val::minus][Var::fx];
	  double fr = fluxInfoArrayX(cellX, cellY, orderY)[Val::plus][Var::fx];
	  double dfl = fluxInfoArrayX(cellX, cellY, orderY)[Val::minus][Var::dfx];
	  double dfr = fluxInfoArrayX(cellX, cellY, orderY)[Val::plus][Var::dfx];
	  double valL = fluxInfoArrayX(cellX, cellY, orderY)[Val::minus][Var::c];
	  double valR = fluxInfoArrayX(cellX, cellY, orderY)[Val::plus][Var::c];

	  fluxVecArrayX(cellX, cellY, orderY)[0] =  laxFriedrichs1d_flux_numeric(fl, fr, dfl, dfr, valL, valR);
	}

    for(size_t cellX=0; cellX<fluxVecArrayY.size(0); cellX++)
      for(size_t cellY=0; cellY<fluxVecArrayY.size(1); cellY++)
	for(size_t orderY=0; orderY<order; orderY++) {
	  double fl = fluxInfoArrayY(cellX, cellY, orderY)[Val::minus][Var::fy];
	  double fr = fluxInfoArrayY(cellX, cellY, orderY)[Val::plus][Var::fy];
	  double dfl = fluxInfoArrayY(cellX, cellY, orderY)[Val::minus][Var::dfy];
	  double dfr = fluxInfoArrayY(cellX, cellY, orderY)[Val::plus][Var::dfy];
	  double valL = fluxInfoArrayY(cellX, cellY, orderY)[Val::minus][Var::c];
	  double valR = fluxInfoArrayY(cellX, cellY, orderY)[Val::plus][Var::c];

	  fluxVecArrayY(cellX, cellY, orderY)[0] =  laxFriedrichs1d_flux_numeric(fl, fr, dfl, dfr, valL, valR);
	}

  }

  CONSTPARA(double, w);
  CONSTPARA(double, ox);
  CONSTPARA(double, oy);
  CONSTPARA(double, ax);
  CONSTPARA(double, ay);
  
  const BK::Array<BK::Array<int, 2>, 2> vecIndizes = {{0,1},{2,3}};
};

typedef Problem_Advection2d Problem;
extern Problem& problem;

#endif
