#ifndef value2d_hpp
#define value2d_hpp

#include <type_traits>

#include "order.hpp"
#include "lagrange.hpp"

// 2D versions ---------------------------------------------------------------------

template<class Array>
inline auto value(int cellX, int cellY, double x, double y, Array const& c) -> typename std::remove_const<typename std::remove_reference<decltype(*c.begin())>::type>::type
{
  int orderX=c.size(2);
  int orderY=c.size(3);
  
  typename std::remove_const<typename std::remove_reference<decltype(*c.begin())>::type>::type u;
  u = 0.;
  for(int oy=0; oy<orderY; oy++)
    for(int ox=0; ox<orderX; ox++)
      u += c(cellX, cellY, ox, oy)*lagrange(ox, order, x)*lagrange(oy, order, y);
    
   return u;
}

template<class Array>
inline auto valueR(int cellX, int cellY, int indexY, Array const& c) -> typename std::remove_const<typename std::remove_reference<decltype(*c.begin())>::type>::type
{
  return c(cellX, cellY, order-1, indexY);
}

template<class Array>
inline auto valueL(int cellX, int cellY, int indexY, Array const& c) -> typename std::remove_const<typename std::remove_reference<decltype(*c.begin())>::type>::type
{
  return c(cellX, cellY, 0, indexY);
}

template<class Array>
inline auto valueT(int cellX, int cellY, int indexX, Array const& c) -> typename std::remove_const<typename std::remove_reference<decltype(*c.begin())>::type>::type
{
  return c(cellX, cellY, indexX, order-1);
}

template<class Array>
inline auto valueB(int cellX, int cellY, int indexX, Array const& c) -> typename std::remove_const<typename std::remove_reference<decltype(*c.begin())>::type>::type
{
  return c(cellX, cellY, indexX, 0);
}

template<class Array>
inline auto value(double x, double y, Array const& c, double dx, double dy) -> typename std::remove_const<typename std::remove_reference<decltype(*c.begin())>::type>::type
{
  size_t orderX=c.size(2);
  size_t orderY=c.size(3);

  size_t cellX = size_t(x/dx);
  size_t cellY = size_t(y/dy);
  double xInCell = 2*(x-dx*cellX)/dx-1;
  double yInCell = 2*(y-dy*cellY)/dy-1;

  typename std::remove_const<typename std::remove_reference<decltype(*c.begin())>::type>::type u;
  u = 0.;
  for(size_t ox=0; ox<orderX; ox++)
    for(size_t oy=0; oy<orderY; oy++)
      u += c(cellX, cellY, ox, oy)*lagrange(ox, orderX, xInCell)*lagrange(oy, orderY, yInCell);
    
  return u;
}										     
#endif
