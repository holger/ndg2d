#include "cfl.hpp"

BK::Array<BK::Array<double, 9>,6> cflLimit = {{1., 0., 0., 0., 0., 0., 0., 0., 0.},
					      {1., 1./3, 0, 0, 0, 0, 0., 0., 0.},
					      {1.256, 0.409, 0.209, 0.130, 0.089, 0.066, 0.051, 0.040, 0.033},
					      {1.392, 0.464, 0.235, 0.145, 0.100, 0.073, 0.056, 0.045, 0.037}, 
					      {1.608, 0.534, 0.271, 0.167, 0.115, 0.085, 0.065, 0.052, 0.042},
					      {1.776, 0.592, 0.300, 0.185, 0.127, 0.093, 0.072, 0.057, 0.047}};
