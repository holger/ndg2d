#ifndef problem_heat2d_hpp
#define problem_heat2d_hpp

#include <BasisKlassen/parameter.hpp>
#include <BasisKlassen/array.hpp>

const int varDim = 3;   // six variables: val, qx, qy
                        // qx = d_x val;  qy = d_y val
const int spaceDim = 2; // 2d space

#include "types.hpp"
#include "cfl.hpp"
#include "problemBase2d.hpp"
#include "laxFriedrichs1d.hpp"
#include "centralFlux1d.hpp"
#include "gaussLobatto.hpp"

// Heat equation in 2d
class Problem_Heat2d : public ProblemBase2d<varDim>
{
public:
  
  struct Var {
    static const int val = 0;    // val
    static const int qx = 1;     // qx
    static const int qy = 2;     // qy
  };

  friend class BK::StackSingleton<Problem_Heat2d>;

  Problem_Heat2d() {}

  void init(int ncelx, int ncely, std::string parameterFilename) {

    name_ = "heat2d";

    ProblemBase2d::init(ncelx, ncely, parameterFilename);

    BK::ReadInitFile initFile(parameterFilename,true);
    nu_ = initFile.getParameter<double>("nu");
    v0x_ = initFile.getParameter<double>("v0x");
    v0y_ = initFile.getParameter<double>("v0y");

    sqrtNu_ = sqrt(nu_);
    double nt = nt_;

    if(nt > 0 && T_ > 0) {
      std::cerr << "ERROR in Problem_heat2d::init(): nt = " <<
	nt << " > 0 && T = " << T_ << " > 0." << std::endl;
      exit(1);						   
    }

    assert(dx_ == dy_);
    
    double dtAd = fabs(cfl_*dx_);
    double dtHeat = fabs(cfl_*dx_*dx_/nu_)/4.;

    std::cout << "dtAd = " << dtAd << "\t dtHeat = " << dtHeat << std::endl;

    if(nu_ == 0)
      dt_ = dtAd;
    else
      dt_ = std::min(dtAd,dtHeat);

    if(nt > 0) {
      nt_ = nt;
      T_ = nt_*dt_;
    }
    else
      nt_ = int(T_/dt_) + 1;

    dt_ *= T_/(nt_*dt_);

    logParameter();

    LOGL(0,PAR(nu_));
    LOGL(0,PAR(sqrtNu_));
    LOGL(0,PAR(v0x_));
    LOGL(0,PAR(v0y_));

    initialized_ = true;
  }
  
  FluxInfo bcYT(int ix, int ox, BK::Type2Type<FluxInfo> t) {
    BK::Array<double,2> xy = ij2xy(ix,ncely_-1,ox,order-1);
    //    BK::Array<double,2> xy = ij2xy(ix,0,ox,0);
    
    return initialCondition(xy[0], xy[1]);
    //    return {0, 1, 0, 0, 0, 0};
  }

  FluxInfo bcYB(int ix, int ox, BK::Type2Type<FluxInfo> t) {
    BK::Array<double,2> xy = ij2xy(ix,0,ox,0);
    //BK::Array<double,2> xy = ij2xy(ix,ncely_-1,ox,order-1);
    
    return initialCondition(xy[0], xy[1]);

    //    return {0, -1, 0, 0, 0, 0};
  }

  FluxInfo bcXR(int iy, int oy, BK::Type2Type<FluxInfo> t) {
    BK::Array<double,2> xy = ij2xy(ncelx_-1,iy,order-1,oy);
    //    BK::Array<double,2> xy = ij2xy(ncelx_-1,iy,order-1,oy);
    return initialCondition(xy[0], xy[1]);

    //    return {1, 0, 0, 0, 0, 0};
  }

  FluxInfo bcXL(int iy, int oy, BK::Type2Type<FluxInfo> t) {
    BK::Array<double,2> xy = ij2xy(0,iy,0,oy);
    
    return initialCondition(xy[0], xy[1]);
    //    return {-1, 0, 0, 0, 0, 0};
  }

  Vector initialCondition(double x, double y) {
    double factor = 0.1;
    double xc = 0.5;
    double yc = 0.5;

    double value = exp(-(BK::sqr(x-xc) + BK::sqr(y-yc))/BK::sqr(factor));
    double qx = 0;
    double qy = 0;

    return Vector{value, qx, qy};
  }
  
  void getReference(Array & c) {}

  void computeSourceTerm(Array const& c, Array & s) {
    
    // double InvSqrt2Xi = 1./sqrt(2)/xi_;
    // for(int cellX=0; cellX<int(c.size(0)); cellX++)
    //   for(int cellY=0; cellY<int(c.size(1)); cellY++)
    // 	for(int indexX=0; indexX<int(c.size(2)); indexX++)
    // 	  for(int indexY=0; indexY<int(c.size(3)); indexY++) {
    // 	    double psix = c(cellX,cellY,indexX,indexY)[Var::psix];
    // 	    double psiy = c(cellX,cellY,indexX,indexY)[Var::psiy];
    // 	    double psi2 = psix*psix + psiy*psiy;
	    
    // 	    s(cellX,cellY,indexX,indexY)[Var::psix] = InvSqrt2Xi*(-psiy + psi2*psiy);
    // 	    s(cellX,cellY,indexX,indexY)[Var::psiy] = InvSqrt2Xi*( psix - psi2*psix);
    // 	  }
  }
  
  void lhs(Array& cNew, Array const& c, Array const& rhs) const {

    for(size_t i=0; i<c.size(); i++)
      cNew[i][Var::val] = c[i][Var::val] + dt_*rhs[i][Var::val];
    
    for(size_t i=0; i<c.size(); i++) {
      cNew[i][Var::qx] = sqrtNu_*rhs[i][Var::qx];
      cNew[i][Var::qy] = sqrtNu_*rhs[i][Var::qy];
    }
  }    

  VecVec flux_vector_nonLin(Vector const& varArray) {
    assert(initialized_);

    double val = varArray[Var::val];
    
    return VecVec{ Vector{v0x_*val, 0, 0},
		   Vector{v0y_*val, 0, 0} };
  }

  VecVec flux_vector_diffusion(Vector const& varArray) {
    assert(initialized_);
    
    double val = varArray[Var::val];
    double qx = varArray[Var::qx];
    double qy = varArray[Var::qy];

    return VecVec{Vector{sqrtNu_*qx, sqrtNu_*val, 0},
		  Vector{sqrtNu_*qy, 0, sqrtNu_*val}};
    // return VecVec{Vector{factor*qxy, -factor*qxx, sqrtXi_*psix, sqrtXi_*psiy, 0, 0},
    // 		  Vector{factor*qyy, -factor*qyx, 0, 0, sqrtXi_*psix, sqrtXi_*psiy}};
  }
  
  VecVec flux_vector(Vector const& varArray) {
    assert(initialized_);

    auto nonLin = flux_vector_nonLin(varArray);
    auto diffusion = flux_vector_diffusion(varArray);

    auto flux = nonLin + diffusion;

    return flux;
  }

  BK::Array<double,2> dFlux_value(Vector const& varArray) {
    return BK::Array<double,2>({std::max(v0x_, v0y_)}); 

  }
  
  void computeNumericalFlux(FluxVecArray& fluxVecArrayX, FluxVecArray& fluxVecArrayY,
			    FluxInfoBordArray& fluxInfoArrayX, FluxInfoBordArray& fluxInfoArrayY) {

    FluxVecArray fluxVecArrayX_nl(ncelx_+1, ncely_, order);
    FluxVecArray fluxVecArrayY_nl(ncelx_, ncely_+1, order);

    auto fluxFunc_nl = [this](Vector const& varArray) { return this->flux_vector_nonLin(varArray);};
    auto dfluxFunc = [this](Vector const& varArray) { return this->dFlux_value(varArray);};

    computeNumericalFlux_LF(fluxVecArrayX_nl, fluxVecArrayY_nl, fluxInfoArrayX, fluxInfoArrayY, fluxFunc_nl, dfluxFunc);
    
    for(size_t cellX=0; cellX<fluxVecArrayX.size(0); cellX++)
      for(size_t cellY=0; cellY<fluxVecArrayX.size(1); cellY++)
	for(size_t orderY=0; orderY<order; orderY++) {
	  fluxVecArrayX_nl(cellX, cellY, orderY)[Var::qx] = 0;
	  fluxVecArrayX_nl(cellX, cellY, orderY)[Var::qy] = 0;
	}
    
    for(size_t cellX=0; cellX<fluxVecArrayY.size(0); cellX++)
      for(size_t cellY=0; cellY<fluxVecArrayY.size(1); cellY++)
	for(size_t orderX=0; orderX<order; orderX++) {
	  fluxVecArrayY_nl(cellX, cellY, orderX)[Var::qx] = 0;
	  fluxVecArrayY_nl(cellX, cellY, orderX)[Var::qy] = 0;
	}
    
    FluxVecArray fluxVecArrayX_d(ncelx_+1, ncely_, order);
    FluxVecArray fluxVecArrayY_d(ncelx_, ncely_+1, order);

    auto fluxFunc_d = [this](Vector const& varArray) { return this->flux_vector_diffusion(varArray);};
    
    computeNumericalFlux_Central(fluxVecArrayX_d, fluxVecArrayY_d, fluxInfoArrayX, fluxInfoArrayY, fluxFunc_d);

    fluxVecArrayX = fluxVecArrayX_nl + fluxVecArrayX_d;
    fluxVecArrayY = fluxVecArrayY_nl + fluxVecArrayY_d;
  }
  
  CONSTPARA(double, nu);
  CONSTPARA(double, sqrtNu);
  CONSTPARA(double, v0x);
  CONSTPARA(double, v0y);
};

typedef Problem_Heat2d Problem;
extern Problem& problem;

#endif
