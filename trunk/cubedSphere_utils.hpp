#ifndef cubedSphere_utils_hpp
#define cubedSphere_utils_hpp

#include <functional>
#include <BasisKlassen/multiArray.hpp>

#include "value2d.hpp"

inline double sqr(double a)
{
  return a*a;
}

inline double sec(double phi)
{
  return 1./cos(phi);
};

typedef BK::Array<BK::Array<double,2>, 2> Mat2x2;
typedef BK::Array<BK::Array<double,2>, 3> Mat3x2;
typedef BK::Array<BK::Array<double,3>, 2> Mat2x3;
typedef BK::Array<BK::Array<double,3>, 3> Mat3x3;

class CubedSphere_utils
{
public:

  CubedSphere_utils() {}
  
  void init(int ncel) {
    ncel_ = ncel;
    da_ = M_PI/2./ncel_;
    db_ = M_PI/2./ncel_;

    xi_ = points[order-1];
    double R = 1;

    sqrtG.resize(ncel_, ncel_, order, order);
    
    G11.resize(ncel_, ncel_, order, order);
    G12.resize(ncel_, ncel_, order, order);
    G21.resize(ncel_, ncel_, order, order);
    G22.resize(ncel_, ncel_, order, order);

    G.resize(ncel_, ncel_, order, order);
    
    GI11.resize(ncel_, ncel_, order, order);
    GI12.resize(ncel_, ncel_, order, order);
    GI21.resize(ncel_, ncel_, order, order);
    GI22.resize(ncel_, ncel_, order, order);

    GI.resize(ncel_, ncel_, order, order);
    
    gamma111.resize(ncel_, ncel_, order, order);
    gamma112.resize(ncel_, ncel_, order, order);
    gamma121.resize(ncel_, ncel_, order, order);
    gamma122.resize(ncel_, ncel_, order, order);
    gamma211.resize(ncel_, ncel_, order, order);
    gamma212.resize(ncel_, ncel_, order, order);
    gamma221.resize(ncel_, ncel_, order, order);
    gamma222.resize(ncel_, ncel_, order, order);

    normDA.resize(ncel_, ncel_, order, order);
    normDB.resize(ncel_, ncel_, order, order);

    nAa.resize(ncel_, ncel_, order, order);
    nAb.resize(ncel_, ncel_, order, order);
    nBa.resize(ncel_, ncel_, order, order);
    nBb.resize(ncel_, ncel_, order, order);
    
    for(int a = 0; a<ncel_; a++) {
      auto alphaFunc = [a, this](double x) { return -M_PI/4. + a*da_+ 0.5*da_ + x*0.5*da_;};
      for(int b = 0; b<ncel_; b++) {
	auto betaFunc = [b, this](double y) { return -M_PI/4. + b*db_+ 0.5*db_ + y*0.5*db_;};
	for(int ia = 0; ia < order; ia++)
	  for(int ib = 0; ib < order; ib++) {

	    double alpha = alphaFunc(xi_[ia]);
	    double beta = betaFunc(xi_[ib]);

	    double rho2 = 1 + tan(alpha)*tan(alpha) + tan(beta)*tan(beta);

	    sqrtG(a, b, ia, ib) = R*R/(pow(rho2,1.5)*cos(alpha)*cos(alpha)*cos(beta)*cos(beta));
	    
	    double rho4 = sqr(rho2);
	    double metric_denominator = (rho4*sqr(cos(alpha))*sqr(cos(beta)));
	    
	    G11(a, b, ia, ib) = (1 + sqr(tan(alpha)))/metric_denominator;
	    G12(a, b, ia, ib) = -tan(alpha)*tan(beta)/metric_denominator;
	    G21(a, b, ia, ib) = G12(a, b, ia, ib);
	    G22(a, b, ia, ib) = (1 + sqr(tan(beta)))/metric_denominator;

	    G(a, b, ia, ib) = {{G11(a,b,ia,ib),G12(a,b,ia,ib)},{G21(a,b,ia,ib),G22(a,b,ia,ib)}};
	    // double metric_denominator = BK::sqr(BK::sqr(1./cos(beta)) + BK::sqr(tan(alpha)));
	    // G11(a, b, ia, ib) = BK::sqr(R)*BK::intPow<4>(1./cos(alpha))*BK::sqr(1./cos(beta))/metric_denominator;
	    // G12(a, b, ia, ib) = -BK::sqr(R)*BK::sqr(1./cos(alpha))*BK::sqr(1./cos(beta))*tan(alpha)*tan(beta)/metric_denominator;
	    // G21(a, b, ia, ib) = G12(a, b, ia, ib);
	    // G22(a, b, ia, ib) = BK::sqr(R)*BK::intPow<4>(1./cos(beta))*BK::sqr(1./cos(alpha))/metric_denominator;

	    
	    GI11(a, b, ia, ib) = G22(a, b, ia, ib)/(sqr(sqrtG(a, b, ia, ib)));
	    GI12(a, b, ia, ib) = -G12(a, b, ia, ib)/(sqr(sqrtG(a, b, ia, ib)));
	    GI21(a, b, ia, ib) = GI12(a, b, ia, ib);
	    GI22(a, b, ia, ib) = G11(a, b, ia, ib)/(sqr(sqrtG(a, b, ia, ib)));

	    GI(a, b, ia, ib) = {{GI11(a,b,ia,ib),GI12(a,b,ia,ib)},{GI21(a,b,ia,ib),GI22(a,b,ia,ib)}};
	    
	    // G11(a, b, ia, ib) = 1.;
	    // G12(a, b, ia, ib) = 0.;
	    // G21(a, b, ia, ib) = 0.;
	    // G22(a, b, ia, ib) = 1.;


	    // sqrtG(a, b, ia, ib) = 1.;

	    double gamma_denominator1 = BK::sqr(1./cos(beta))+BK::sqr(tan(alpha));
	    gamma111(a, b, ia, ib) = 2*tan(alpha)*BK::sqr(tan(beta))/gamma_denominator1;
	    gamma112(a, b, ia, ib) = -BK::sqr(1./cos(beta))*tan(beta)/gamma_denominator1;
	    //	    gamma121(a, b, ia, ib) = gamma112(a, b, ia, ib);
	    gamma122(a, b, ia, ib) = 0;

	    double gamma_denominator2 = 3+cos(2*alpha)+2*cos(2*beta)*BK::sqr(sin(alpha));
	    gamma211(a, b, ia, ib) = 0;
	    gamma212(a, b, ia, ib) = -4*BK::sqr(cos(beta))*tan(alpha)/gamma_denominator2;
	    //	    gamma221(a, b, ia, ib) = gamma212(a, b, ia, ib);
	    gamma222(a, b, ia, ib) = 4*BK::sqr(sin(alpha))*sin(2*beta)/gamma_denominator2;

	    // gamma111(a, b, ia, ib) = 0.;
	    // gamma112(a, b, ia, ib) = 0.;
	    // gamma121(a, b, ia, ib) = 0.;
	    // gamma122(a, b, ia, ib) = 0.;
	    
	    // gamma211(a, b, ia, ib) = 0.;
	    // gamma212(a, b, ia, ib) = 0.;
	    // gamma221(a, b, ia, ib) = 0.;
	    // gamma222(a, b, ia, ib) = 0.;

	    double temp = pow(pow(sec(beta),2) + pow(tan(alpha),2),1.5);
	    normDA(a, b, ia, ib) = pow(sec(alpha),2)*sqrt(pow(sec(beta),4)+
							  pow(tan(alpha),2)*(1+pow(tan(beta),2)))/temp;

	    normDB(a, b, ia, ib) = pow(sec(beta),2)*sqrt(pow(sec(alpha),4)+
							 pow(tan(beta),2)*(1+pow(tan(alpha),2)))/temp;

	    double temp2 = pow(pow(sec(beta),2) + pow(tan(alpha),2),5);
	    nAa(a, b, ia, ib) = -pow(cos(alpha),4)*pow(cos(beta),4)*sqrt(pow(sec(alpha),6)*pow(sec(beta),8)/temp2)*pow(pow(sec(beta),2) + pow(tan(alpha),2),3);

	    nAb(a, b, ia, ib) = -sin(alpha)*pow(cos(alpha) + pow(cos(beta),2)*sin(alpha)*tan(alpha),3)*sqrt(pow(cos(beta),2)*pow(sec(alpha),6)/pow(1+pow(cos(beta),2)*pow(tan(alpha),2),5))*tan(beta);

	    nBa(a, b, ia, ib) = pow(cos(alpha),5)*sin(alpha)*sin(beta)*sqrt(pow(sec(alpha),8)*pow(sec(beta),6)/pow(pow(sec(beta),2) + pow(tan(alpha),2),5))*pow(sec(beta)+cos(beta)*pow(tan(alpha),2),3);

	    nBb(a, b, ia, ib) =  -pow(cos(alpha),4)*pow(cos(beta),4)*sqrt(pow(sec(beta),6)*pow(sec(alpha),8)/temp2)*pow(pow(sec(beta),2) + pow(tan(alpha),2),3);
	    
	  }
      }
    }
  }

  BK::Array<double,2> get_contraVariant(BK::Array<double,2> u_coVariant, double alpha, double beta)
  {
    int cellA = a2cell(alpha);
    int cellB = b2cell(beta);

    if(cellA == ncel_) cellA = ncel_-1;
    if(cellB == ncel_) cellB = ncel_-1;
    double x = alpha2LagrangeInterval(alpha);
    double y = alpha2LagrangeInterval(beta);

    auto gI11 = value(cellA, cellB, x, y, GI11);
    auto gI12 = value(cellA, cellB, x, y, GI12);
    auto gI21 = gI12;
    auto gI22 = value(cellA, cellB, x, y, GI22);
    
    BK::Array<double,2> u_contraVariant = {gI11*u_coVariant[0] + gI12*u_coVariant[1], gI21*u_coVariant[0] + gI22*u_coVariant[1]};
    return u_contraVariant;
  }

  BK::Array<double,2> get_coVariant(BK::Array<double,2> u_contraVariant, double alpha, double beta)
  {
    int cellA = a2cell(alpha);
    int cellB = b2cell(beta);

    if(cellA == ncel_) cellA = ncel_-1;
    if(cellB == ncel_) cellB = ncel_-1;
    double x = alpha2LagrangeInterval(alpha);
    double y = alpha2LagrangeInterval(beta);

    auto g11 = value(cellA, cellB, x, y, G11);
    auto g12 = value(cellA, cellB, x, y, G12);
    auto g21 = g12;
    auto g22 = value(cellA, cellB, x, y, G22);
    
    BK::Array<double,2> u_coVariant = {g11*u_contraVariant[0] + g12*u_contraVariant[1], g21*u_contraVariant[0] + g22*u_contraVariant[1]};
    return u_coVariant;
  }
  
  BK::Array<double,2> get_contraVariant(BK::Array<double,2> u_coVariant, int a, int b, int ia, int ib)
  {
    BK::Array<double,2> u_contraVariant = {GI11(a,b,ia,ib)*u_coVariant[0] + GI12(a,b,ia,ib)*u_coVariant[1], GI21(a,b,ia,ib)*u_coVariant[0] + GI22(a,b,ia,ib)*u_coVariant[1]};
    return u_contraVariant;
  }

  BK::Array<double,2> get_coVariant(BK::Array<double,2> u_contraVariant, int a, int b, int ia, int ib)
  {
    BK::Array<double,2> u_coVariant = {G11(a,b,ia,ib)*u_contraVariant[0] + G12(a,b,ia,ib)*u_contraVariant[1], G21(a,b,ia,ib)*u_contraVariant[0] + G22(a,b,ia,ib)*u_contraVariant[1]};
    return u_coVariant;
  }

  void get_contraVariant(BK::MultiArray<spaceDim*2, BK::Array<double,2>> const& cCo, BK::MultiArray<spaceDim*2, BK::Array<double,2>>& cContra)
  {
    for(int a = 0; a<ncel_; a++) 
      for(int b = 0; b<ncel_; b++) 
	for(int ia = 0; ia < order; ia++)
	  for(int ib = 0; ib < order; ib++) {
	    cContra(a,b,ia,ib) = get_contraVariant(cCo(a,b,ia,ib),a,b,ia,ib);
	  }
  }

  void get_coVariant(BK::MultiArray<spaceDim*2, BK::Array<double,2>> const& cContra, BK::MultiArray<spaceDim*2, BK::Array<double,2>>& cCo)
  {
    for(int a = 0; a<ncel_; a++) 
      for(int b = 0; b<ncel_; b++) 
	for(int ia = 0; ia < order; ia++)
	  for(int ib = 0; ib < order; ib++) {
	    cCo(a,b,ia,ib) = get_coVariant(cContra(a,b,ia,ib),a,b,ia,ib);
	  }
  }

  double get_alpha(int a, double x) { return -M_PI/4. + a*da_+ 0.5*da_ + x*0.5*da_;};
  double get_beta(int b, double y) { return -M_PI/4. + b*db_+ 0.5*db_ + y*0.5*db_;};
  double get_alpha(int a, int ia) { return -M_PI/4. + a*da_+ 0.5*da_ + xi_[ia]*0.5*da_;};
  double get_beta(int b, int ib) { return -M_PI/4. + b*db_+ 0.5*db_ + xi_[ib]*0.5*db_;};

  // computes cell and index from alpha, index = -1 if not found
  void get_cellIndex(double alpha, int& cell, int& index) {
    cell = (alpha+M_PI/4)/da_;
    index = -1;
    
    double indexDouble = alpha+M_PI/4 - cell*da_;
    for(int i=0;i<order;i++) {
      double indexDiff = indexDouble/da_ - ((xi_[i]+1)/2);
      if(fabs(indexDiff) < 1e-14) index = i;
    }
  };
  
  size_t a2cell(double a) {
    double cell = (a+M_PI/4.)/da_;
    if(cell == ncel_) cell = ncel_-1;
    return cell;
  }

  size_t b2cell(double b) {
    double cell = (b+M_PI/4.)/db_;
    if(cell == ncel_) cell = ncel_-1;
    return cell;
  }

  double alpha2LagrangeInterval(double alpha) {
    int cell = a2cell(alpha);
    double val = (alpha + M_PI/4 - cell*da_)/da_*2 - 1;
    return val;
  }
  
  template<class Array>
  inline auto valueAB(double alpha, double beta, Array const& c) -> typename std::remove_const<typename std::remove_reference<decltype(*c.begin())>::type>::type
  {
    assert((alpha >= -M_PI/4) && (alpha <= M_PI/4));
    assert((beta >= -M_PI/4) && (beta <= M_PI/4));
    
    // int orderX=c.size(2);
    // int orderY=c.size(3);
    
    size_t cellX = a2cell(alpha);
    size_t cellY = b2cell(beta);
    double x = alpha2LagrangeInterval(alpha);
    double y = alpha2LagrangeInterval(beta);

    // std::cout << alpha << "\t" << beta << std::endl;
    // std::cout << cellX << "\t" << cellY << "\t" << x << "\t" << y << std::endl;
    
    return value(cellX, cellY, x, y, c);
  }

  template<class Vector, class Array>
  double integrate(std::function<double(int, int, int ,int,Vector)> func, BK::Vector<Array> const* c) {
    auto w = weights[order-1];
    
    double integral = 0;    
    for(int a = 0; a<ncel_; a++) 
      for(int b = 0; b<ncel_; b++) 
	for(int ia = 0; ia < order; ia++)
	  for(int ib = 0; ib < order; ib++) {
	    double sqrtGVal = sqrtG(a, b, ia, ib);
	    for(int face = 0; face <6; face++) {
	      auto data = (*c)[face](a, b, ia, ib);
	      
	      integral += func(a,b,ia,ib,data)*sqrtGVal*w[ia]*w[ib];
	    }
	  }
    integral *= (da_*db_)/4;
    return integral;
  }


  BK::MultiArray<spaceDim*2, double> G11;
  BK::MultiArray<spaceDim*2, double> G12;
  BK::MultiArray<spaceDim*2, double> G21;
  BK::MultiArray<spaceDim*2, double> G22;
  BK::MultiArray<spaceDim*2, double> GI11;
  BK::MultiArray<spaceDim*2, double> GI12;
  BK::MultiArray<spaceDim*2, double> GI21;
  BK::MultiArray<spaceDim*2, double> GI22;

  BK::MultiArray<spaceDim*2, Mat2x2> G;
  BK::MultiArray<spaceDim*2, Mat2x2> GI;
  
  BK::MultiArray<spaceDim*2, double> sqrtG;

  BK::MultiArray<spaceDim*2, double> gamma111;
  BK::MultiArray<spaceDim*2, double> gamma112;
  BK::MultiArray<spaceDim*2, double> gamma121;
  BK::MultiArray<spaceDim*2, double> gamma122;
  BK::MultiArray<spaceDim*2, double> gamma211;
  BK::MultiArray<spaceDim*2, double> gamma212;
  BK::MultiArray<spaceDim*2, double> gamma221;
  BK::MultiArray<spaceDim*2, double> gamma222;

  BK::MultiArray<spaceDim*2, double> normDA;
  BK::MultiArray<spaceDim*2, double> normDB;
  BK::MultiArray<spaceDim*2, double> nAa;
  BK::MultiArray<spaceDim*2, double> nAb;
  BK::MultiArray<spaceDim*2, double> nBa;
  BK::MultiArray<spaceDim*2, double> nBb;

  CONSTPARA(double, da);
  CONSTPARA(double, db);
  CONSTPARA(int, ncel);
  BK::Vector<double> xi_;
};


inline Mat2x3 get_d_map_P1(double alpha, double beta)
{
  double sqrtab = sqrt(BK::sqr(sec(beta)) + BK::sqr(tan(alpha)));

  double R = 1;
  
  return {{-cos(alpha)*sin(alpha)*sqrtab/R,
	   BK::sqr(cos(alpha))*sqrtab/R, 0},
	  {-cos(beta)*sin(beta)*sqrtab/R, 
	   0, BK::sqr(cos(beta))*sqrtab/R}};
}

inline Mat2x3 get_d_map_P2(double alpha, double beta)
{
  double sqrtab = sqrt(BK::sqr(sec(beta)) + BK::sqr(tan(alpha)));

  double R = 1;
  
  return {{-BK::sqr(cos(alpha))*sqrtab/R,
	   -cos(alpha)*sin(alpha)*sqrtab/R, 0},
	  {0, -cos(beta)*sin(beta)*sqrtab/R, 
	   BK::sqr(cos(beta))*sqrtab/R}};
}

inline Mat2x3 get_d_map_P3(double alpha, double beta)
{
  double sqrtab = sqrt(BK::sqr(sec(beta)) + BK::sqr(tan(alpha)));

  double R = 1;

  return {{cos(alpha)*sin(alpha)*sqrtab/R,
	   -BK::sqr(cos(alpha))*sqrtab/R, 0},
	  {cos(beta)*sin(beta)*sqrtab/R, 
	   0, BK::sqr(cos(beta))*sqrtab/R}};
}

inline Mat2x3 get_d_map_P4(double alpha, double beta)
{
  double sqrtab = sqrt(BK::sqr(sec(beta)) + BK::sqr(tan(alpha)));

  double R = 1;

  return {{BK::sqr(cos(alpha))*sqrtab/R,
	   cos(alpha)*sin(alpha)*sqrtab/R, 0},
	  {0,cos(beta)*sin(beta)*sqrtab/R, 
	   BK::sqr(cos(beta))*sqrtab/R}};
}

inline Mat2x3 get_d_map_P5(double alpha, double beta)
{
  double sqrtab = sqrt(BK::sqr(sec(beta)) + BK::sqr(tan(alpha)));

  double R = 1;
  
  return {{0, BK::sqr(cos(alpha))*sqrtab/R,
	   -cos(alpha)*sin(alpha)*sqrtab/R},
	  {-BK::sqr(cos(beta))*sqrtab/R, 
	   0, -cos(beta)*sin(beta)*sqrtab/R}};
}

inline Mat2x3 get_d_map_P6(double alpha, double beta)
{
  double sqrtab = sqrt(BK::sqr(sec(beta)) + BK::sqr(tan(alpha)));

  double R = 1;
  
  return {{0, BK::sqr(cos(alpha))*sqrtab/R,
	   cos(alpha)*sin(alpha)*sqrtab/R},
	  {BK::sqr(cos(beta))*sqrtab/R, 
	   0, cos(beta)*sin(beta)*sqrtab/R}};
}

extern BK::Array<std::function<Mat2x3(double, double)>, 6> get_d_map;

inline Mat3x2 get_d_para_P1(double alpha, double beta)
{
  double R = 1;
  double a = 1;
  double a3 = BK::intPow<3>(a);
  double denominator = pow(BK::sqr(a)*(BK::sqr(sec(beta)) + BK::sqr(tan(alpha))), 1.5);

  return {{-a3*R*BK::sqr(sec(alpha))*tan(alpha)/denominator, -a3*R*BK::sqr(sec(beta))*tan(beta)/denominator},
	  { a3*R*BK::sqr(sec(alpha))*BK::sqr(sec(beta))/denominator, -a3*R*BK::sqr(sec(beta))*tan(alpha)*tan(beta)/denominator},
	  {-a3*R*BK::sqr(sec(alpha))*tan(alpha)*tan(beta)/denominator, a3*R*BK::sqr(sec(alpha))*BK::sqr(sec(beta))/denominator}};
}

inline Mat3x2 get_d_para_P2(double alpha, double beta)
{
  double R = 1;
  double a = 1;
  double a3 = BK::intPow<3>(a);
  double denominator = pow(BK::sqr(a)*(BK::sqr(sec(beta)) + BK::sqr(tan(alpha))), 1.5);

  return {{-a3*R*BK::sqr(sec(alpha))*BK::sqr(sec(beta))/denominator, a3*R*BK::sqr(sec(beta))*tan(alpha)*tan(beta)/ denominator},
	  {-a3*R*BK::sqr(sec(alpha))*tan(alpha)/denominator, -a3*R*BK::sqr(sec(beta))*tan(beta)/denominator},
	  {-a3*R*BK::sqr(sec(alpha))*tan(alpha)*tan(beta)/denominator, a3*R*BK::sqr(sec(alpha))*BK::sqr(sec(beta))/denominator}};
}

inline Mat3x2 get_d_para_P3(double alpha, double beta)
{
  double R = 1;
  double a = 1;
  double a3 = BK::intPow<3>(a);
  double denominator = pow(BK::sqr(a)*(BK::sqr(sec(beta)) + BK::sqr(tan(alpha))), 1.5);

  return {{a3*R*BK::sqr(sec(alpha))*tan(alpha)/denominator, a3*R*BK::sqr(sec(beta))*tan(beta)/denominator},
	  {-a3*R*BK::sqr(sec(alpha))*BK::sqr(sec(beta))/denominator, a3*R*BK::sqr(sec(beta))*tan(alpha)*tan(beta)/denominator},
	  {-a3*R*BK::sqr(sec(alpha))*tan(alpha)*tan(beta)/denominator, a3*R*BK::sqr(sec(alpha))*BK::sqr(sec(beta))/denominator}};
}

inline Mat3x2 get_d_para_P4(double alpha, double beta)
{
  double R = 1;
  double a = 1;
  double a3 = BK::intPow<3>(a);
  double denominator = pow(BK::sqr(a)*(BK::sqr(sec(beta)) + BK::sqr(tan(alpha))), 1.5);

  return {{a3*R*BK::sqr(sec(alpha))*BK::sqr(sec(beta))/denominator, -a3*R*BK::sqr(sec(beta))*tan(alpha)*tan(beta)/denominator},
	  {a3*R*BK::sqr(sec(alpha))*tan(alpha)/denominator, a3*R*BK::sqr(sec(beta))*tan(beta)/ denominator},
	  {-a3*R*BK::sqr(sec(alpha))*tan(alpha)*tan(beta)/denominator, a3*R*BK::sqr(sec(alpha))*BK::sqr(sec(beta))/denominator}};
}

inline Mat3x2 get_d_para_P5(double alpha, double beta)
{
  double R = 1;
  double a = 1;
  double a3 = BK::intPow<3>(a);
  double denominator = pow(BK::sqr(a)*(BK::sqr(sec(beta)) + BK::sqr(tan(alpha))), 1.5);

  return {{a3*R*BK::sqr(sec(alpha))*tan(alpha)*tan(beta)/denominator, -a3*R*BK::sqr(sec(alpha))*BK::sqr(sec(beta))/denominator},
	  {a3*R*BK::sqr(sec(alpha))*BK::sqr(sec(beta))/denominator, -a3*R*BK::sqr(sec(beta))*tan(alpha)*tan(beta)/denominator},
	  {-a3*R*BK::sqr(sec(alpha))*tan(alpha)/denominator, -a3*R*BK::sqr(sec(beta))*tan(beta)/denominator}};
}

inline Mat3x2 get_d_para_P6(double alpha, double beta)
{
  double R = 1;
  double a = 1;
  double a3 = BK::intPow<3>(a);
  double denominator = pow(BK::sqr(a)*(BK::sqr(sec(beta)) + BK::sqr(tan(alpha))), 1.5);

  return {{-a3*R*BK::sqr(sec(alpha))*tan(alpha)*tan(beta)/denominator, a3*R*BK::sqr(sec(alpha))*BK::sqr(sec(beta))/denominator},
	  { a3*R*BK::sqr(sec(alpha))*BK::sqr(sec(beta))/denominator, -a3*R*BK::sqr(sec(beta))*tan(alpha)*tan(beta)/denominator},
	  { a3*R*BK::sqr(sec(alpha))*tan(alpha)/denominator, a3*R*BK::sqr(sec(beta))*tan(beta)/denominator}};
}

extern BK::Array<std::function<Mat3x2(double, double)>, 6> get_d_para;

inline void tensor_xyz2ab(Mat2x2& result, Mat3x3 tensor_xyz, Mat2x3 d_map, Mat3x2 d_para)
{
  for(int a=0; a<2; a++)
    for(int b=0; b<2; b++) {
      result[a][b] = 0;
      for(int i=0; i<3; i++)
	for(int j=0; j<3; j++)
	  result[a][b] += d_map[a][i]*tensor_xyz[i][j]*d_para[j][b];
    }
}

inline void tensor_ab2xyz(Mat3x3& result, Mat2x2 tensor_ab, Mat2x3 d_map, Mat3x2 d_para)
{
  for(int a=0; a<3; a++)
    for(int b=0; b<3; b++) {
      result[a][b] = 0;
      for(int i=0; i<2; i++)
	for(int j=0; j<2; j++)
	  result[a][b] += d_para[a][i]*tensor_ab[i][j]*d_map[j][b];
    }
}

inline void matrixMult2(Mat2x2& result, Mat2x2 const& a, Mat2x2 const& b) {
  for(int i=0; i<2; i++)
    for(int j=0; j<2; j++) {
      result[i][j] = 0;
      for(int l=0; l<2; l++)
	result[i][j] += a[i][l]*b[l][j];
    }
}

inline void matrixMult3(Mat2x2& result, Mat2x2 const& a, Mat2x2 const& b, Mat2x2 const& c) {
  for(int i=0; i<2; i++)
    for(int j=0; j<2; j++) {
      result[i][j] = 0;
      for(int k=0; k<2; k++)
	for(int l=0; l<2; l++) 
	  result[i][j] += a[i][k]*b[k][l]*c[l][j];
    }
}

inline void matrixMultVec(BK::Array<double,2>& result, Mat2x2 const& a, BK::Array<double,2> const& b) {
  for(int i=0; i<2; i++) {
    result[i] = 0;
    for(int l=0; l<2; l++)
      result[i] += a[i][l]*b[l];
  }
}

inline bool matrixIdentityTest(Mat2x2 const& a)
{
  bool identity = true;

  if(fabs(a[0][0]) - 1 > 1e-15) identity = false;
  if(fabs(a[0][1])     > 1e-15) identity = false;
  if(fabs(a[1][0])     > 1e-15) identity = false;
  if(fabs(a[1][1]) - 1 > 1e-15) identity = false;
  
  return identity;
}

inline BK::Array<double, 2> xyz2pt(double x, double y, double z)
{
  double R=1;
  // double phi = atan2(y,x);
  // if(phi < -0.75*M_PI)
  //   phi += 2*M_PI;
  return BK::Array<double, 2>{atan2(y,x), acos(z/R)};
}

inline BK::Array<double, 3> pt2xyz(double phi, double theta)
{
  double R=1;
  return {R*sin(theta)*cos(phi), R*sin(theta)*sin(phi), R*cos(theta)};
}

inline BK::Array<double, 2> xyz2ab_P1(double x, double y, double z)
{
  //return {atan2(y,x),atan2(z,x)};
  return {atan(y/x),atan(z/x)};
}

inline BK::Array<double, 2> xyz2ab_P2(double x, double y, double z)
{
  //return {atan2(-x,y),atan2(z,y)};
  return {atan(-x/y),atan(z/y)};
}

inline BK::Array<double, 2> xyz2ab_P3(double x, double y, double z)
{
  //return {atan2(y,x),atan2(-z,x)};
  return {atan(y/x),atan(-z/x)};
}

inline BK::Array<double, 2> xyz2ab_P4(double x, double y, double z)
{
  //return {atan2(-x,y),atan2(-z,y)};
  return {atan(-x/y),atan(-z/y)};
}

inline BK::Array<double, 2> xyz2ab_P5(double x, double y, double z)
{
  //return {atan2(y,z),atan2(-x,z)};
  return {atan(y/z),atan(-x/z)};
}

inline BK::Array<double, 2> xyz2ab_P6(double x, double y, double z)
{
  //return {atan2(-y,z),atan2(-x,z)};
  return {atan(-y/z),atan(-x/z)};
}

extern BK::Array<std::function<BK::Array<double, 2>(double x, double y, double z)>, 6> xyz2ab;

inline BK::Array<double, 3> ab2xyz_P1(double a, double b)
{
  double R=1;
  double xTemp = tan(a);
  double yTemp = tan(b);
  double r=sqrt(1 + BK::sqr(xTemp) + BK::sqr(yTemp));
  
  return BK::Array<double, 3>{R/r, R*xTemp/r, R*yTemp/r};
}

inline BK::Array<double, 3> ab2xyz_P2(double a, double b)
{
  double R=1;
  double xTemp = tan(a);
  double yTemp = tan(b);
  double r=sqrt(1 + BK::sqr(xTemp) + BK::sqr(yTemp));

  return BK::Array<double, 3>{-R*xTemp/r, R/r, R*yTemp/r};
}

inline BK::Array<double, 3> ab2xyz_P3(double a, double b)
{
  double R=1;
  double xTemp = tan(a);
  double yTemp = tan(b);
  double r=sqrt(1 + BK::sqr(xTemp) + BK::sqr(yTemp));

  return BK::Array<double, 3>{-R/r, -R*xTemp/r, R*yTemp/r};
} 

inline BK::Array<double, 3> ab2xyz_P4(double a, double b)
{
  double R=1;
  double xTemp = tan(a);
  double yTemp = tan(b);
  double r=sqrt(1 + BK::sqr(xTemp) + BK::sqr(yTemp));

  return BK::Array<double, 3>{R*xTemp/r, -R/r, R*yTemp/r};
} 

inline BK::Array<double, 3> ab2xyz_P5(double a, double b)
{
  double R=1;
  double xTemp = tan(a);
  double yTemp = tan(b);
  double r=sqrt(1 + BK::sqr(xTemp) + BK::sqr(yTemp));

  return BK::Array<double, 3>{-R*yTemp/r, R*xTemp/r, R/r};
} 

inline BK::Array<double, 3> ab2xyz_P6(double a, double b)
{
  double R=1;
  double xTemp = tan(a);
  double yTemp = tan(b);
  double r=sqrt(1 + BK::sqr(xTemp) + BK::sqr(yTemp));

  return BK::Array<double, 3>{R*yTemp/r, R*xTemp/r, -R/r};
}

extern BK::Array<std::function<BK::Array<double, 3>(double, double)>, 6> ab2xyz;



inline BK::Array<double, 2> xyz2ab_face(double x, double y, double z, int& face)
{
  face = -1;

  //  std::cerr << "x = " << x << ", y = " << y << ",z = " << z <<  std::endl;

  BK::Array<double, 2> ab;
  BK::Array<double, 2> abFmod;

  ab  = xyz2ab[0](x,y,z);
  if((ab[0] <= M_PI/4. && ab[0] >= -M_PI/4.) && (ab[1] <= M_PI/4. && ab[1] >= -M_PI/4.) && x > 0) { face = 0; return ab;}

  ab  = xyz2ab[1](x,y,z);
  if((ab[0] <= M_PI/4. && ab[0] >= -M_PI/4.) && (ab[1] <= M_PI/4. && ab[1] >= -M_PI/4.) && y > 0) { face = 1;  return ab;}

  ab  = xyz2ab[2](x,y,z);
  if((ab[0] <= M_PI/4. && ab[0] >= -M_PI/4.) && (ab[1] <= M_PI/4. && ab[1] >= -M_PI/4.) && x < 0) { face = 2;  return ab;}

  ab  = xyz2ab[3](x,y,z);
  if((ab[0] <= M_PI/4. && ab[0] >= -M_PI/4.) && (ab[1] <= M_PI/4. && ab[1] >= -M_PI/4.) && y < 0) { face = 3;  return ab;}

  ab  = xyz2ab[4](x,y,z);
  if((ab[0] <= M_PI/4. && ab[0] >= -M_PI/4.) && (ab[1] <= M_PI/4. && ab[1] >= -M_PI/4.) && z > 0) { face = 4;  return ab;}

  ab  = xyz2ab[5](x,y,z);
  if((ab[0] <= M_PI/4. && ab[0] >= -M_PI/4.) && (ab[1] <= M_PI/4. && ab[1] >= -M_PI/4.) && z < 0) { face = 5;  return ab;}

  // double l = 1./sqrt(3.);
  
  // if(x >= l) {
  //   face = 0;
  //   ab  = xyz2ab[face](x,y,z);
  //   return ab;
  // }
  // if(y >= l) {
  //   face = 1;
  //   ab  = xyz2ab[face](x,y,z);
  //   return ab;
  // }
  // if(x <= -l) {
  //   face = 2;
  //   ab  = xyz2ab[face](x,y,z);
  //   return ab;
  // }
  // if(y <= -l) {
  //   face = 3;
  //   ab  = xyz2ab[face](x,y,z);
  //   return ab;
  // }
  // if(z >= l) {
  //   face = 4;
  //   ab  = xyz2ab[face](x,y,z);
  //   return ab;
  // }
  // if(z <= -l) {
  //   face = 5;
  //   ab  = xyz2ab[face](x,y,z);
  //   return ab;
  // }

  // std::cerr << face << std::endl;

  
  //   for(int f : {0,1,3,4,5}) {
  //     ab  = xyz2ab[f](x,y,z);
  //     BK::Array<double, 3> diff = ab2xyz[f](ab[0], ab[1]) - BK::Array<double,3>({x,y,z});
  //     if(norm(diff) < 1e-14) {
  // 	face = f;
  // 	std::cerr << diff << "\t" << norm(diff) << std::endl;
  // 	std::cerr << "found " << f << std::endl; return ab;}
  //   }

    
    
    
  // }
  
  // 
  //   ab  = xyz2ab[f](x,y,z);
  //   BK::Array<double, 3> diff = ab2xyz[f](ab[0], ab[1]) - BK::Array<double,3>({x,y,z});
  //   if(norm(diff) < 1e-14) {
  //     face = f;
  //     std::cerr << diff << "\t" << norm(diff) << std::endl;
  //     std::cerr << "found " << f << std::endl; return ab;}
  // }
  // else {
    

  // }
  
  // for(int f=0;f<6;f++) {
  //   ab  = xyz2ab[f](x,y,z);
  //   BK::Array<double, 3> diff = ab2xyz[f](ab[0], ab[1]) - BK::Array<double,3>({x,y,z});
  //   if(norm(diff) < 1e-14) {
  //     face = f;
  //     std::cerr << f << "\t" << ab << "\t" << ab2xyz[f](ab[0], ab[1]) << "\t" << BK::Array<double,3>({x,y,z}) << std::endl;
  //     return ab;
  //   }
  //     //      std::cerr << diff << "\t" << norm(diff) << std::endl;
  //     //      std::cerr << "found " << f << std::endl; return ab;}
  // }
  
    
  // ab  = xyz2ab[0](x,y,z);
  // if(ab2xyz[0](ab[0], ab[1]) == BK::Array<double,3>({x,y,z})) {face = 0;  return ab;}
  // ab  = xyz2ab[1](x,y,z);
  // if(ab2xyz[1](ab[0], ab[1]) == BK::Array<double,3>({x,y,z})) {face = 1;  return ab;}
  
  
  
  // abFmod = BK::Array<double,2>({fmod(ab[0],M_PI/2),fmod(ab[1],M_PI/2)});
  // std::cout << 0 << " = " << ab << "\t" << abFmod << std::endl;
  // if((ab[0] <= M_PI/4. && ab[0] >= -M_PI/4.) && (ab[1] <= M_PI/4. && ab[1] >= -M_PI/4.) && x > 0) {face = 0;  return ab;}

  // ab  = xyz2ab[1](x,y,z);
  // abFmod = BK::Array<double,2>({fmod(ab[0],M_PI/2),fmod(ab[1],M_PI/2)});
  // std::cout << 1 << " = " << ab << "\t" << abFmod << std::endl;
  // if((ab[0] <= M_PI/4. && ab[0] >= -M_PI/4.) && (ab[1] <= M_PI/4. && ab[1] >= -M_PI/4.) && y > 0) {face = 1;  return ab;}

  // ab  = xyz2ab[2](x,y,z);
  // abFmod = BK::Array<double,2>({fmod(ab[0],M_PI/2),fmod(ab[1],M_PI/2)});
  // std::cout << 2 << " = " << ab << "\t" << abFmod << std::endl;
  // if((ab[0] <= M_PI/4. && ab[0] >= -M_PI/4.) && (ab[1] <= M_PI/4. && ab[1] >= -M_PI/4.) && x < 0) {face = 2;  return ab;}

  // ab  = xyz2ab[3](x,y,z);
  // abFmod = BK::Array<double,2>({fmod(ab[0],M_PI/2),fmod(ab[1],M_PI/2)});
  // std::cout << 3 << " = " << ab << "\t" << abFmod << std::endl;
  // if((ab[0] <= M_PI/4. && ab[0] >= -M_PI/4.) && (ab[1] <= M_PI/4. && ab[1] >= -M_PI/4.) && y < 0) {face = 3;  return ab;}

  // ab  = xyz2ab[4](x,y,z);
  // abFmod = BK::Array<double,2>({fmod(ab[0],M_PI/2),fmod(ab[1],M_PI/2)});
  // std::cout << 4 << " = " << ab << "\t" << abFmod << std::endl;
  // if((ab[0] <= M_PI/4. && ab[0] >= -M_PI/4.) && (ab[1] <= M_PI/4. && ab[1] >= -M_PI/4.) && z > 0) {face = 4;  return ab;}

  // ab  = xyz2ab[5](x,y,z);
  // abFmod = BK::Array<double,2>({fmod(ab[0],M_PI/2),fmod(ab[1],M_PI/2)});
  // std::cout << 5 << " = " << ab << "\t" << abFmod << std::endl;
  // if((ab[0] <= M_PI/4. && ab[0] >= -M_PI/4.) && (ab[1] <= M_PI/4. && ab[1] >= -M_PI/4.) && z < 0) {face = 5;  return ab;}

  if(face == -1) {
    std::cerr << "ERROR in xyz2ab_face: face = -1:\n";

    // ab  = xyz2ab[3](x,y,z);
    // auto xyz = ab2xyz[3](ab[0], ab[1]);
    // std::cerr << ab << "\t" << xyz << std::endl;
    // std::cerr << xyz[0] - x << "\t" << xyz[1] - y << "\t" << xyz[2] - z << std::endl;
    
    
    // std::cerr << "x = " << x << ", y = " << y << ",z = " << z <<  std::endl;
    // auto xyz = ab2xyz[2](-M_PI/4.,-M_PI/4.);
    // std::cerr << xyz << std::endl;
    // xyz = ab2xyz[2](2.35619,2.35619);
    // std::cerr << xyz << std::endl;
    // std::cout.precision(12);
    // std::cout << std::scientific << xyz[0] - x << "\t" << xyz[1] - y << "\t" << xyz[2] - z << std::endl;
    exit(1);
  }

  return face;  
}

inline BK::Array<double, 2> ab2pt_P1(double a, double b)
{
  double R=1;
  BK::Array<double, 3> xyz = ab2xyz_P1(a,b);

  double x = xyz[0];
  double y = xyz[1];
  double z = xyz[2];
  
  return BK::Array<double, 2>{atan2(y,x), acos(z/R)};
}

inline BK::Array<double, 2> ab2pt_P2(double a, double b)
{
  double R=1;
  BK::Array<double, 3> xyz = ab2xyz_P2(a,b);

  double x = xyz[0];
  double y = xyz[1];
  double z = xyz[2];
  
  return BK::Array<double, 2>{atan2(y,x), acos(z/R)};
}

inline BK::Array<double, 2> ab2pt_P3(double a, double b)
{
  double R=1;
  BK::Array<double, 3> xyz = ab2xyz_P3(a,b);

  double x = xyz[0];
  double y = xyz[1];
  double z = xyz[2];

  return BK::Array<double, 2>{fmod(atan2(y,x)+2*M_PI,2*M_PI), acos(z/R)};
}

inline BK::Array<double, 2> ab2pt_P4(double a, double b)
{
  double R=1;
  BK::Array<double, 3> xyz = ab2xyz_P4(a,b);

  double x = xyz[0];
  double y = xyz[1];
  double z = xyz[2];
  
  return BK::Array<double, 2>{atan2(y,x), acos(z/R)};
}

inline BK::Array<double, 2> ab2pt_P5(double a, double b)
{
  double R=1;
  BK::Array<double, 3> xyz = ab2xyz_P5(a,b);

  double x = xyz[0];
  double y = xyz[1];
  double z = xyz[2];

  double phi = atan2(y,x);
  if(phi <= -0.75*M_PI)
    phi += 2*M_PI;
  return BK::Array<double, 2>{phi, acos(z/R)};
}

inline BK::Array<double, 2> ab2pt_P6(double a, double b)
{
  double R=1;
  BK::Array<double, 3> xyz = ab2xyz_P6(a,b);

  double x = xyz[0];
  double y = xyz[1];
  double z = xyz[2];

  double phi = atan2(y,x);
  if(phi <= -0.75*M_PI)
    phi += 2*M_PI;
  
  return BK::Array<double, 2>{phi, acos(z/R)};
}

extern BK::Array<std::function<BK::Array<double, 2>(double, double)>, 6> ab2pt;

inline void d_parametrisation_P1(BK::Array<double,3>& v, double ua, double ub, double alpha, double beta)
{
  double R = 1;
  double a = 1;

  double denominator = pow(BK::sqr(a)*(BK::sqr(sec(beta)) + BK::sqr(tan(alpha))), 1.5);

  double a3 = BK::intPow<3>(a);

  v[0] = -a3*R*BK::sqr(sec(alpha))*tan(alpha)/denominator * ua
    -a3*R*BK::sqr(sec(beta))*tan(beta)/denominator * ub;

  v[1] = a3*R*BK::sqr(sec(alpha))*BK::sqr(sec(beta))/denominator * ua
    -a3*R*BK::sqr(sec(beta))*tan(alpha)*tan(beta)/denominator *ub;
  
  v[2] = -a3*R*BK::sqr(sec(alpha))*tan(alpha)*tan(beta)/denominator * ua
    + a3*R*BK::sqr(sec(alpha))*BK::sqr(sec(beta))/denominator * ub;
}

inline void d_parametrisation_P2(BK::Array<double,3>& v, double ua, double ub, double alpha, double beta)
{
  double R = 1;
  double a = 1;

  double a3 = BK::intPow<3>(a);
  
  double denominator = pow(BK::sqr(a)*(BK::sqr(sec(beta)) + BK::sqr(tan(alpha))), 1.5);

  v[0] = -a3*R*BK::sqr(sec(alpha))*BK::sqr(sec(beta))/denominator * ua
    + a3*R*BK::sqr(sec(beta))*tan(alpha)*tan(beta)/ denominator * ub;
  
  v[1] = -a3*R*BK::sqr(sec(alpha))*tan(alpha)/denominator * ua
    -a3*R*BK::sqr(sec(beta))*tan(beta)/denominator * ub;

  v[2] = -a3*R*BK::sqr(sec(alpha))*tan(alpha)*tan(beta)/denominator * ua
    + a3*R*BK::sqr(sec(alpha))*BK::sqr(sec(beta))/denominator * ub;
}

inline void d_parametrisation_P3(BK::Array<double,3>& v, double ua, double ub, double alpha, double beta)
{
  double R = 1;
  double a = 1;

  double a3 = BK::intPow<3>(a);
  
  double denominator = pow(BK::sqr(a)*(BK::sqr(sec(beta)) + BK::sqr(tan(alpha))), 1.5);

  v[0] = a3*R*BK::sqr(sec(alpha))*tan(alpha)/denominator * ua
    + a3*R*BK::sqr(sec(beta))*tan(beta)/denominator * ub;

  v[1] = -a3*R*BK::sqr(sec(alpha))*BK::sqr(sec(beta))/denominator * ua
    + a3*R*BK::sqr(sec(beta))*tan(alpha)*tan(beta)/denominator * ub;

  v[2] = -a3*R*BK::sqr(sec(alpha))*tan(alpha)*tan(beta)/denominator * ua
    + a3*R*BK::sqr(sec(alpha))*BK::sqr(sec(beta))/denominator * ub;
}

inline void d_parametrisation_P4(BK::Array<double,3>& v, double ua, double ub, double alpha, double beta)
{
  double R = 1;
  double a = 1;

  double a3 = BK::intPow<3>(a);
  
  double denominator = pow(BK::sqr(a)*(BK::sqr(sec(beta)) + BK::sqr(tan(alpha))), 1.5);

  v[0] = a3*R*BK::sqr(sec(alpha))*BK::sqr(sec(beta))/denominator * ua
    -a3*R*BK::sqr(sec(beta))*tan(alpha)*tan(beta)/denominator * ub;

  v[1] = a3*R*BK::sqr(sec(alpha))*tan(alpha)/denominator * ua
    + a3*R*BK::sqr(sec(beta))*tan(beta)/ denominator * ub;

  v[2] = -a3*R*BK::sqr(sec(alpha))*tan(alpha)*tan(beta)/denominator * ua
    + a3*R*BK::sqr(sec(alpha))*BK::sqr(sec(beta))/denominator * ub;
}

inline void d_parametrisation_P5(BK::Array<double,3>& v, double ua, double ub, double alpha, double beta)
{
  double R = 1;
  double a = 1;

  double a3 = BK::intPow<3>(a);
  
  double denominator = pow(BK::sqr(a)*(BK::sqr(sec(beta)) + BK::sqr(tan(alpha))), 1.5);

  v[0] = a3*R*BK::sqr(sec(alpha))*tan(alpha)*tan(beta)/denominator * ua
    -a3*R*BK::sqr(sec(alpha))*BK::sqr(sec(beta))/denominator * ub;

  v[1] = a3*R*BK::sqr(sec(alpha))*BK::sqr(sec(beta))/denominator * ua
    -a3*R*BK::sqr(sec(beta))*tan(alpha)*tan(beta)/denominator * ub;

  v[2] = -a3*R*BK::sqr(sec(alpha))*tan(alpha)/denominator * ua
    -a3*R*BK::sqr(sec(beta))*tan(beta)/denominator * ub;
}

inline void d_parametrisation_P6(BK::Array<double,3>& v, double ua, double ub, double alpha, double beta)
{
  double R = 1;
  double a = 1;

  double a3 = BK::intPow<3>(a);
  
  double denominator = pow(BK::sqr(a)*(BK::sqr(sec(beta)) + BK::sqr(tan(alpha))), 1.5);

  v[0] = -a3*R*BK::sqr(sec(alpha))*tan(alpha)*tan(beta)/denominator * ua
    +a3*R*BK::sqr(sec(alpha))*BK::sqr(sec(beta))/denominator * ub;

  v[1] = a3*R*BK::sqr(sec(alpha))*BK::sqr(sec(beta))/denominator * ua
    -a3*R*BK::sqr(sec(beta))*tan(alpha)*tan(beta)/denominator * ub;

  v[2] = a3*R*BK::sqr(sec(alpha))*tan(alpha)/denominator * ua
    +a3*R*BK::sqr(sec(beta))*tan(beta)/denominator * ub;

  //  std::cout << "v[2] = " << v[2] << "\t" << alpha << "\t" << beta << std::endl;
}

extern BK::Array<std::function<void(BK::Array<double,3>& v, double ua, double ub, double alpha, double beta)>, 6> d_parametrisation;

inline void d_map_P1(BK::Array<double,3> v, double& ua, double& ub, BK::Array<double,3> xyz)
{
  double X = xyz[0];
  double Y = xyz[1];
  double Z = xyz[2];
    
  ua = -Y/(X*X+Y*Y)*v[0] + X/(X*X+Y*Y)*v[1];
  ub = -Z/(X*X+Z*Z)*v[0] + X/(X*X+Z*Z)*v[2];
}

inline void d_map_P2(BK::Array<double,3> v, double& ua, double& ub, BK::Array<double,3> xyz)
{
  double X = xyz[0];
  double Y = xyz[1];
  double Z = xyz[2];

  ua = -Y/(X*X+Y*Y)*v[0] + X/(X*X+Y*Y)*v[1];
  ub = -Z/(Y*Y+Z*Z)*v[1] + Y/(Y*Y+Z*Z)*v[2];
}

inline void d_map_P3(BK::Array<double,3> v, double& ua, double& ub, BK::Array<double,3> xyz)
{
  double X = xyz[0];
  double Y = xyz[1];
  double Z = xyz[2];

  ua = -Y/(X*X+Y*Y)*v[0] + X/(X*X+Y*Y)*v[1];
  ub =  Z/(X*X+Z*Z)*v[0] - X/(X*X+Z*Z)*v[2];
}

inline void d_map_P4(BK::Array<double,3> v, double& ua, double& ub, BK::Array<double,3> xyz)
{
  double X = xyz[0];
  double Y = xyz[1];
  double Z = xyz[2];

  ua = -Y/(X*X+Y*Y)*v[0] + X/(X*X+Y*Y)*v[1];
  ub =  Z/(Y*Y+Z*Z)*v[1] - Y/(Y*Y+Z*Z)*v[2];
}

inline void d_map_P5(BK::Array<double,3> v, double& ua, double& ub, BK::Array<double,3> xyz)
{
  double X = xyz[0];
  double Y = xyz[1];
  double Z = xyz[2];

  ua = Z/(Y*Y+Z*Z)*v[1] - Y/(Y*Y+Z*Z)*v[2];
  ub = -Z/(X*X+Z*Z)*v[0] + X/(X*X+Z*Z)*v[2];
}
  
inline void d_map_P6(BK::Array<double,3> v, double& ua, double& ub, BK::Array<double,3> xyz)
{
  double X = xyz[0];
  double Y = xyz[1];
  double Z = xyz[2];

  ua = -Z/(Y*Y+Z*Z)*v[1] + Y/(Y*Y+Z*Z)*v[2];
  ub = -Z/(X*X+Z*Z)*v[0] + X/(X*X+Z*Z)*v[2];
}

extern BK::Array<std::function<void(BK::Array<double,3>, double&, double&, BK::Array<double,3>)>, 6> d_map;

// spherical coordinate transformations

inline void d_map_spherical(BK::Array<double,3> v, double& u_theta, double& u_phi, double theta, double phi)
{
  // u_theta = 0;
  // u_phi = 0;
  // if(fabs(theta) > 1e-8) {
  //   u_theta = v[2]/sqrt(1-cos(theta)*cos(theta));
  //   u_phi = -1./sin(theta)/cos(phi)*tan(phi)/(1+tan(phi)*tan(phi))*v[0] + 1./sin(theta)/cos(phi)/(1+tan(phi)*tan(phi))*v[1];
  // }

  u_theta = 0;
  u_phi = 0;
  if(fabs(sin(theta)) > 1e-8) {
    u_theta = cos(theta)*cos(phi)*v[0] + cos(theta)*sin(phi)*v[1] - sin(theta)*v[2];
    u_phi = -sin(phi)/sin(theta)*v[0] + cos(phi)/sin(theta)*v[1];
  }
}

inline void d_para_spherical(BK::Array<double,3>& v, double u_theta, double u_phi, double theta, double phi)
{
  v[0] = -u_phi*sin(phi)*sin(theta)+ u_theta*cos(theta)*cos(phi);
  v[1] = u_phi*cos(phi)*sin(theta) + u_theta*cos(theta)*sin(phi);
  v[2] = -u_theta*sin(theta);
}



#endif
