#ifndef mpiFlux3d_hpp
#define mpiFlux3d_hpp

#include <mpi.h>
#include <BasisKlassen/multiArray.hpp>
#include "mpiBase.hpp"

class XFluxMPI3d
{
public:

  XFluxMPI3d(){};

  void init(int ncelx, int ncely,int ncelz) {
    XplusFirstCell.resize(ncely, order,ncelz,order);
    XplusLastCell.resize(ncely, order,ncelz,order);
    XminusFirstCell.resize(ncely, order,ncelz,order);
    XminusLastCell.resize(ncely, order,ncelz,order);

    ncelx_ = ncelx;
    ncely_ = ncely;
    ncelz_ = ncelz;
  }

  int ncelx_;
  int ncely_;
  int ncelz_;

  template<class FluxInfoVecBordArray>
  void begin(FluxInfoVecBordArray& fluxInfoArrayX );

  template<class FluxInfoVecBordArray>
  void finish(FluxInfoVecBordArray& fluxInfoArrayX ) {

    for(int iy=0; iy<ncely_; iy++) 
      for(int oy=0; oy<order; oy++)
	for(int iz=0; iz<ncelz_; iz++) 
	  for(int oz=0; oz<order; oz++){
	    fluxInfoArrayX(0, iy,iz,oy,oz)[Val::minus] = XminusFirstCell(iy, oy,iz,oz) ;
	    fluxInfoArrayX(ncelx_-1, iy,iz,oy,oz)[Val::plus] = XplusLastCell(iy, oy,iz,oz) ; }
  }

  BK::MultiArray<4,BK::Array<double, Problem::varDim>> XplusFirstCell;
  BK::MultiArray<4,BK::Array<double, Problem::varDim>> XplusLastCell;
  BK::MultiArray<4,BK::Array<double, Problem::varDim>> XminusFirstCell;
  BK::MultiArray<4,BK::Array<double, Problem::varDim>> XminusLastCell;
  
  //BK::Vector<BK::Array<BK::Array<double, Problem::fluxInfoNumber>, order>> XplusFirstCell;
  //BK::Vector<BK::Array<BK::Array<double, Problem::fluxInfoNumber>, order>> XplusLastCell;
  //BK::Vector<BK::Array<BK::Array<double, Problem::fluxInfoNumber>, order>> XminusFirstCell;
  //BK::Vector<BK::Array<BK::Array<double, Problem::fluxInfoNumber>, order>> XminusLastCell;
};

class YFluxMPI3d
{
public:

  YFluxMPI3d() {}

  void init(int ncelx, int ncely,int ncelz) {
    YplusFirst.resize(ncelx, order, ncelz, order);
    YplusLast.resize(ncelx, order, ncelz, order);
    YminusFirst.resize(ncelx, order, ncelz, order);
    YminusLast.resize(ncelx, order, ncelz, order);

    ncelx_ = ncelx;
    ncely_ = ncely;
    ncelz_ = ncelz;
  }

  int ncelx_;
  int ncely_;
  int ncelz_;

  template<class FluxInfoVecBordArray>
  void begin(FluxInfoVecBordArray& fluxInfoArrayY);
  
  template<class FluxInfoVecBordArray>
  void finish(FluxInfoVecBordArray& fluxInfoArrayY) {

    for(int ix=0; ix<ncelx_; ix++) 
      for(int ox=0; ox<order; ox++)
	 for(int iz=0; iz<ncelz_; iz++) 
	   for(int oz=0; oz<order; oz++){
	     fluxInfoArrayY(ix, ncely_-1,iz, ox,oz)[Val::plus] = YplusLast(ix, ox, iz, oz) ;
	     fluxInfoArrayY(ix, 0, iz, ox,oz)[Val::minus] =  YminusFirst(ix, ox, iz, oz) ; }
  }

  BK::MultiArray<4,BK::Array<double, Problem::varDim>> YplusFirst; 
  BK::MultiArray<4,BK::Array<double, Problem::varDim>> YplusLast;  
  BK::MultiArray<4,BK::Array<double, Problem::varDim>> YminusFirst;
  BK::MultiArray<4,BK::Array<double, Problem::varDim>> YminusLast; 

  // BK::Vector<BK::Array<BK::Array<double, Problem::fluxInfoNumber>, order>> YplusFirst;
  // BK::Vector<BK::Array<BK::Array<double, Problem::fluxInfoNumber>, order>> YplusLast;
  // BK::Vector<BK::Array<BK::Array<double, Problem::fluxInfoNumber>, order>> YminusFirst;
  // BK::Vector<BK::Array<BK::Array<double, Problem::fluxInfoNumber>, order>> YminusLast; 
};

class ZFluxMPI3d
{
public:

  ZFluxMPI3d() {}

  void init(int ncelx, int ncely,int ncelz) {
    ZplusFirst.resize(ncelx, order, ncely, order);
    ZplusLast.resize(ncelx, order, ncely, order);
    ZminusFirst.resize(ncelx, order, ncely, order);
    ZminusLast.resize(ncelx, order, ncely, order);

    ncelx_ = ncelx;
    ncely_ = ncely;
    ncelz_ = ncelz;
  }

  int ncelx_;
  int ncely_;
  int ncelz_;

  template<class FluxInfoVecBordArray>
  void begin(FluxInfoVecBordArray& fluxInfoArrayZ);
  
  template<class FluxInfoVecBordArray>
  void finish(FluxInfoVecBordArray& fluxInfoArrayZ) {

    for(int ix=0; ix<ncelx_; ix++) 
      for(int ox=0; ox<order; ox++)
	 for(int iy=0; iy<ncely_; iy++) 
	   for(int oy=0; oy<order; oy++){
	     fluxInfoArrayZ(ix, iy,ncelz_-1, ox,oy)[Val::plus] = ZplusLast(ix, ox, iy, oy) ;
	     fluxInfoArrayZ(ix, iy, 0, ox,oy)[Val::minus] =  ZminusFirst(ix, ox, iy, oy) ; }
  }

  BK::MultiArray<4,BK::Array<double, Problem::varDim>> ZplusFirst; 
  BK::MultiArray<4,BK::Array<double, Problem::varDim>> ZplusLast;  
  BK::MultiArray<4,BK::Array<double, Problem::varDim>> ZminusFirst;
  BK::MultiArray<4,BK::Array<double, Problem::varDim>> ZminusLast; 

  // BK::Vector<BK::Array<BK::Array<double, Problem::fluxInfoNumber>, order>> ZplusFirst;
  // BK::Vector<BK::Array<BK::Array<double, Problem::fluxInfoNumber>, order>> ZplusLast;
  // BK::Vector<BK::Array<BK::Array<double, Problem::fluxInfoNumber>, order>> ZminusFirst;
  // BK::Vector<BK::Array<BK::Array<double, Problem::fluxInfoNumber>, order>> ZminusLast; 
};

// X
template<class FluxInfoVecBordArray>
void XFluxMPI3d::begin(FluxInfoVecBordArray& fluxInfoArrayX) {
  
  for(int iy=0; iy<ncely_; iy++) 
    for(int oy=0; oy<order; oy++)
      for(int iz=0; iz<ncelz_; iz++) 
	  for(int oz=0; oz<order; oz++){
	    XminusLastCell(iy, oy, iz, oz) = fluxInfoArrayX(ncelx_-1, iy, iz, oy, oz)[Val::minus] ;
	    XplusFirstCell(iy, oy, iz, oz) = fluxInfoArrayX(0, iy, iz, oy, oz)[Val::plus] ;}

  
  int numberOfMpiElements = XminusLastCell.size()*Problem::varDim;
  // non-blocking boundary value exchanges
  // MPI_Isend(buf, count, datatype, dest, tag, comm, &request) 
  MPI_Isend( XminusLastCell.data(), numberOfMpiElements, MPI_DOUBLE,mpiBase.get_commRankNeighboorP(0),1,mpiBase.get_cartcomm(), mpiBase.get_request());
  MPI_Isend( XplusFirstCell.data(), numberOfMpiElements, MPI_DOUBLE,mpiBase.get_commRankNeighboorM(0),2,mpiBase.get_cartcomm(), mpiBase.get_request());
  // MPI_Irecv(buf, count, datatype, source, tag, comm, request)
  MPI_Irecv( XminusFirstCell.data(), numberOfMpiElements, MPI_DOUBLE, mpiBase.get_commRankNeighboorM(0), 1, mpiBase.get_cartcomm(), mpiBase.get_request());
  MPI_Irecv( XplusLastCell.data(), numberOfMpiElements, MPI_DOUBLE, mpiBase.get_commRankNeighboorP(0), 2, mpiBase.get_cartcomm(), mpiBase.get_request());
}

// Y
template<class FluxInfoVecBordArray>
void YFluxMPI3d::begin(FluxInfoVecBordArray& fluxInfoArrayY) {
  
  for(int ix=0; ix<ncelx_; ix++) 
    for(int ox=0; ox<order; ox++)
       for(int iz=0; iz<ncelz_; iz++) 
	   for(int oz=0; oz<order; oz++){
	     YminusLast(ix, ox, iz, oz) = fluxInfoArrayY(ix, ncely_-1, iz, ox, oz)[Val::minus] ;
	     YplusFirst(ix, ox, iz, oz) = fluxInfoArrayY(ix, 0, iz, ox, oz)[Val::plus] ; }

  int numberOfMpiElements = YminusLast.size()*Problem::varDim;
  // non-blocking boundary value exchanges
  // MPI_Isend(buf, count, datatype, dest, tag, comm, request) 
  MPI_Isend(YminusLast.data(), numberOfMpiElements, MPI_DOUBLE,mpiBase.get_commRankNeighboorP(1),3,mpiBase.get_cartcomm(), mpiBase.get_request());
  MPI_Isend(YplusFirst.data(), numberOfMpiElements, MPI_DOUBLE, mpiBase.get_commRankNeighboorM(1),4,mpiBase.get_cartcomm(), mpiBase.get_request());
  // MPI_Irecv(buf, count, datatype, source, tag, comm, request) 
  MPI_Irecv(YminusFirst.data(), numberOfMpiElements, MPI_DOUBLE, mpiBase.get_commRankNeighboorM(1), 3, mpiBase.get_cartcomm(), mpiBase.get_request());
  MPI_Irecv(YplusLast.data(), numberOfMpiElements, MPI_DOUBLE, mpiBase.get_commRankNeighboorP(1) , 4, mpiBase.get_cartcomm(), mpiBase.get_request());
}

// Z
template<class FluxInfoVecBordArray>
void ZFluxMPI3d::begin(FluxInfoVecBordArray& fluxInfoArrayZ) {
  
  for(int ix=0; ix<ncelx_; ix++) 
    for(int ox=0; ox<order; ox++)
       for(int iy=0; iy<ncely_; iy++) 
	   for(int oy=0; oy<order; oy++){
	     ZminusLast(ix, ox, iy, oy) = fluxInfoArrayZ(ix, iy, ncelz_-1, ox, oy)[Val::minus] ;
	     ZplusFirst(ix, ox, iy, oy) = fluxInfoArrayZ(ix, iy, 0, ox, oy)[Val::plus] ; }

  int numberOfMpiElements = ZminusLast.size()*Problem::varDim;
  // non-blocking boundary value exchanges
  // MPI_Isend(buf, count, datatype, dest, tag, comm, request) 
  MPI_Isend(ZminusLast.data(), numberOfMpiElements, MPI_DOUBLE,mpiBase.get_commRankNeighboorP(2),5,mpiBase.get_cartcomm(), mpiBase.get_request());
  MPI_Isend(ZplusFirst.data(), numberOfMpiElements, MPI_DOUBLE, mpiBase.get_commRankNeighboorM(2),6,mpiBase.get_cartcomm(), mpiBase.get_request());
  // MPI_Irecv(buf, count, datatype, source, tag, comm, request) 
  MPI_Irecv(ZminusFirst.data(), numberOfMpiElements, MPI_DOUBLE, mpiBase.get_commRankNeighboorM(2), 5, mpiBase.get_cartcomm(), mpiBase.get_request());
  MPI_Irecv(ZplusLast.data(), numberOfMpiElements, MPI_DOUBLE, mpiBase.get_commRankNeighboorP(2) , 6, mpiBase.get_cartcomm(), mpiBase.get_request());
}
    
#endif
