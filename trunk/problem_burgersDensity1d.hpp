#ifndef problem_burgersDensity1d_hpp
#define problem_burgersDensity1d_hpp

#include <BasisKlassen/parameter.hpp>
#include <BasisKlassen/singleton.hpp>
#include <BasisKlassen/assert.hpp>
#include <BasisKlassen/parseFromFile.hpp>

const int varDim = 2;    // two variables: rho and rho*u
const int spaceDim = 1;  // one space dimension

#include "value1d.hpp"
#include "cfl.hpp"
#include "laxFriedrichs1d.hpp"
#include "notiModify.hpp"
#include "problemBase1d.hpp"

// Burgers equation with (passiv) density in 1D
class Problem_BurgersDensity1d : public ProblemBase1d<varDim>
{
public:

  friend class BK::StackSingleton<Problem_BurgersDensity1d>;

  Problem_BurgersDensity1d() {}

  void init(int ncel, std::string parameterFilename) {

    //    notifyField.add("fieldFinal", FieldNotifyCallBack(std::bind(&Problem_BurgersDensity1d::errorComp,this,std::placeholders::_1), "Problem_BurgersDensity1d::errorComp"));
    name_ = "burgersDensity1d";
    
    ProblemBase1d::init(ncel, parameterFilename);

    // computing dt
    dt_ = fabs(cfl_*dx_);
    if(nt_ > 0) {
      T_ = nt_*dt_;
    }
    else {
      nt_ = int(T_/dt_) + 1;
    }
    dt_ *= T_/(nt_*dt_);

    initialized_ = true;
    
    logParameter();
  }

  void errorComp(Array const* c);
  
  Vector initialCondition(double x) {
    assert(initialized_);

    return initialConditionShift(x);
  }

  Vector initialConditionShift(double x, double shift = 0) {
    assert(initialized_);
    // double x0 = 0.35;
    // double y0 = 0.35;

    // int kMax = 10;
    // double temp = 0;
    // for(int k=1; k<=kMax; k++)
    //   temp += pow(k,-5./6)*sin(2*M_PI*k/lambda_*(x-shift-2*M_PI/(k*k)));

    // return temp;

    //    double k=1;
    return Vector{1.,sin(2*M_PI*x/lx_)};
  }

  void lhs(Array& cNew, Array const& c, Array const& rhs) const {
    cNew = c + dt_*rhs;
  }    

  VecVec flux_vector(Vector cellVec, size_t cell=0, size_t index=0) {
    assert(initialized_);

    double rho = cellVec[0];
    double rhoU = cellVec[1];
    double u = rhoU/rho;
    
    return VecVec{Vector{rhoU, rhoU*u}};
  }

  double dFlux_value(Vector cellVec, size_t cell=0, size_t index=0) {
    assert(initialized_);
    
    double rho = cellVec[0];
    double rhoU = cellVec[1];
    double u = rhoU/rho;

    return u;
  }

  void compute_fluxArray(FluxVecArray& fluxArray, FluxInfoBordArray const& fluxInfoArray)
  {
    auto fluxFunc = [this](Vector const& varArray, size_t cell, size_t index) { return this->flux_vector(varArray, cell, index);};
    auto dfluxFunc = [this](Vector const& varArray, size_t cell, size_t index) { return this->dFlux_value(varArray, cell, index);};
    
    compute_fluxArray_LF(fluxArray, fluxInfoArray, fluxFunc, dfluxFunc);
  }
};

typedef Problem_BurgersDensity1d Problem;
extern Problem& problem;

#endif
