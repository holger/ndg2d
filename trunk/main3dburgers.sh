#! /bin/bash

rm -r data/burgersDensity3d

cp order.hpp_template order.hpp
sed -i 's/static constexpr int order = 3/static constexpr int order = 3/g' order.hpp
sed -i 's/static constexpr int rkOrder = 3/static constexpr int rkOrder = 3/g' order.hpp
cd release
make burgersDensity3d

./burgersDensity3d ../problem_burgersDensity3d3.init

cd ..


cp order.hpp_template order.hpp
sed -i 's/static constexpr int order = 3/static constexpr int order = 4/g' order.hpp
sed -i 's/static constexpr int rkOrder = 3/static constexpr int rkOrder = 4/g' order.hpp
cd release
make burgersDensity3d

./burgersDensity3d ../problem_burgersDensity3d4.init

cd ..


cp order.hpp_template order.hpp
sed -i 's/static constexpr int order = 3/static constexpr int order = 5/g' order.hpp
sed -i 's/static constexpr int rkOrder = 3/static constexpr int rkOrder = 5/g' order.hpp
cd release
make burgersDensity3d

./burgersDensity3d ../problem_burgersDensity3d5.init

cd ..

cp order.hpp_template order.hpp
sed -i 's/static constexpr int order = 3/static constexpr int order = 6/g' order.hpp
sed -i 's/static constexpr int rkOrder = 3/static constexpr int rkOrder = 6/g' order.hpp
cd release
make burgersDensity3d

./burgersDensity3d ../problem_burgersDensity3d6.init

cd ..

cp order.hpp_template order.hpp
sed -i 's/static constexpr int order = 3/static constexpr int order = 6/g' order.hpp
sed -i 's/static constexpr int rkOrder = 3/static constexpr int rkOrder = 3/g' order.hpp
cd release
make burgersDensity3d

./burgersDensity3d ../problem_burgersDensity3d6.init

cd ..

cp order.hpp_template order.hpp
sed -i 's/static constexpr int order = 3/static constexpr int order = 7/g' order.hpp
sed -i 's/static constexpr int rkOrder = 3/static constexpr int rkOrder = 3/g' order.hpp
cd release
make burgersDensity3d

./burgersDensity3d ../problem_burgersDensity3d6.init

cd ..

cp order.hpp_template order.hpp
sed -i 's/static constexpr int order = 3/static constexpr int order = 8/g' order.hpp
sed -i 's/static constexpr int rkOrder = 3/static constexpr int rkOrder = 3/g' order.hpp
cd release
make burgersDensity3d

./burgersDensity3d ../problem_burgersDensity3d6.init

cd ..



