#ifndef notiModify_hpp
#define notiModify_hpp

#include <BasisKlassen/notificationHandler.hpp>
#include <BasisKlassen/toString.hpp>
#include <BasisKlassen/vector.hpp>

#include "types.hpp"
#include "logger.hpp"

extern const int spaceDim;
extern const int varDim;

template<class Signature>
class CallBack;

/** @brief CallBack
  *
  * Class for holding a Callback used by the noti/modifaction handlers
  */
template<class TResult, typename ...TArgs>
class CallBack<TResult(TArgs...)>
{
public:
  CallBack(std::function<TResult(TArgs...)> callBack, std::string name) : callBack_(callBack), name_(name) {}

  TResult operator()(TArgs... args) const {
    return callBack_(args...);
  }

  bool operator==(CallBack callBack) {
    return callBack.name_ == name_;
  }

  bool operator==(std::string name) {
    return name == name_;
  }

  std::string get_name() const {
    return name_;
  }
  
private:
  std::function<TResult(TArgs...)> callBack_;
  std::string name_;
};

// ------------------------------------------------------------------------------------------------------------------------
// --------------------------- FIELDS -------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------

typedef CallBack<void(DataTypes<spaceDim, varDim>::Array const*)> FieldNotifyCallBack;
typedef CallBack<void(DataTypes<spaceDim, varDim>::Array *)> FieldModifyCallBack;

/** @brief FieldNotify
  *
  * Class for handling notifications for fields
  */
class FieldNotify : public BK::NotificationHandler<void, CallBack, DataTypes<spaceDim, varDim>::Array const*>
{
 public:
  void exec(std::string key, DataTypes<spaceDim, varDim>::Array const* fieldVec) {

    //    LOGL(3,BK::toString("FieldNotify::exec(",key,",",fieldVec[0]->get_name(),")"));
    
    BK::NotificationHandler<void, CallBack, DataTypes<spaceDim, varDim>::Array const*>::exec(key, fieldVec);
    
    //    LOGL(4,BK::toString("FieldNotify::exec(",key,",",fieldVec[0]->get_name(),")-end"));
  }
};

/** @brief FieldModify
  *
  * Class for handling modifications for fields
  */
class FieldModify : public BK::NotificationHandler<void, CallBack, DataTypes<spaceDim, varDim>::Array const*>
{
 public:
  void exec(std::string key, DataTypes<spaceDim, varDim>::Array const* fieldVec) {
    //    LOGL(3,BK::toString("FieldModify::exec(",key,",",fieldVec[0]->get_name(),")"));
    
    BK::NotificationHandler<void, CallBack, DataTypes<spaceDim, varDim>::Array const*>::exec(key, fieldVec);
    
    //   LOGL(4,BK::toString("FieldModify::exec(",key,",",fieldVec[0]->get_name(),")-end"));
  }
};

extern FieldNotify& notifyField;
extern FieldModify& modifyField;

// ------------------------------------------------------------------------------------------------------------------------
// --------------------------- VECTOR OF FIELDS ---------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------

typedef CallBack<void(BK::Vector<DataTypes<spaceDim, varDim>::Array> const*)> FieldVecNotifyCallBack;
typedef CallBack<void(BK::Vector<DataTypes<spaceDim, varDim>::Array> *)> FieldVecModifyCallBack;

/** @brief FieldNotify
  *
  * Class for handling notifications for fields
  */
class FieldVecNotify : public BK::NotificationHandler<void, CallBack, BK::Vector<DataTypes<spaceDim, varDim>::Array> const*>
{
 public:
  void exec(std::string key, BK::Vector<DataTypes<spaceDim, varDim>::Array> const* fieldVec) {

    //    LOGL(3,BK::toString("FieldNotify::exec(",key,",",fieldVec[0]->get_name(),")"));
    
    BK::NotificationHandler<void, CallBack, BK::Vector<DataTypes<spaceDim, varDim>::Array> const*>::exec(key, fieldVec);
    
    //    LOGL(4,BK::toString("FieldNotify::exec(",key,",",fieldVec[0]->get_name(),")-end"));
  }
};

/** @brief FieldModify
  *
  * Class for handling notifications for fields
  */
class FieldVecModify : public BK::NotificationHandler<void, CallBack, BK::Vector<DataTypes<spaceDim, varDim>::Array> *>
{
 public:
  void exec(std::string key, BK::Vector<DataTypes<spaceDim, varDim>::Array> * fieldVec) {

    //    LOGL(3,BK::toString("FieldNotify::exec(",key,",",fieldVec[0]->get_name(),")"));
    
    BK::NotificationHandler<void, CallBack, BK::Vector<DataTypes<spaceDim, varDim>::Array> *>::exec(key, fieldVec);
    
    //    LOGL(4,BK::toString("FieldNotify::exec(",key,",",fieldVec[0]->get_name(),")-end"));
  }
};

extern FieldVecNotify& notifyFieldVec;
extern FieldModify& modifyField;

// ------------------------------------------------------------------------------------------------------------------------
// --------------------------- EVENT --------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------

typedef CallBack<void()> EventNotifyCallBack;

/** @brief EventModify
  *
  * Class for handling notifications of events
  */

class EventNotify : public BK::NotificationHandler<void, CallBack>
{
 public:
  void exec(std::string key) {
    LOGL(3,BK::toString("EventNotify::exec(",key,")"));
    
    BK::NotificationHandler<void, CallBack>::exec(key);
    
    LOGL(4,BK::toString("EventNotify::exec(",key,")-end"));
  }
};

//typedef BK::NotificationHandler<void, CallBack> EventNotify;

extern EventNotify& notifyEvent;

#endif
