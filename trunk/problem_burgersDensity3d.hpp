#ifndef problem_burgersDensity3d_hpp
#define problem_burgersDensity3d_hpp

#include <BasisKlassen/parameter.hpp>
#include <BasisKlassen/singleton.hpp>
#include <BasisKlassen/assert.hpp>
#include <BasisKlassen/parseFromFile.hpp>
#include <BasisKlassen/vector.hpp>
#include <BasisKlassen/assert.hpp>

const int varDim = 4;     // four variables: rho, rho*ux, rho*uy, rho*uz
const int spaceDim = 3;   // 3d space

#include "types.hpp"
#include "cfl.hpp"
#include "notiModify.hpp"
#include "problemBase3d.hpp"
#include "laxFriedrichs1d.hpp"
#include "order.hpp"
#include "gaussLobatto.hpp"
#include "logger.hpp"

// LOGGING: ----------------------------------------------------
// #undef LOGLEVEL
// #define LOGLEVEL 4

// 3D Burgers equations with density
class Problem_BurgersDensity3d : public ProblemBase3d<varDim>
{
public:

  struct Var {
    static const int rho = 0;  // rho
    static const int rhoUx = 1;  // rho*ux
    static const int rhoUy = 2;  // rho*uy
    static const int rhoUz = 3;  // rho*uz
  };

  friend class BK::StackSingleton<Problem_BurgersDensity3d>;

  Problem_BurgersDensity3d() {}
  
  void init(int ncelx, int ncely, int ncelz, std::string parameterFilename) {
    //    notifyField.add("fieldFinal", FieldNotifyCallBack(std::bind(&Problem_BurgersDensity3d::errorComp, this, std::placeholders::_1), "Problem_BurgersDensity3d::errorComp"));

    name_ = "burgersDensity3d";
    ProblemBase3d::init(ncelx, ncely, ncelz, parameterFilename);
    
    double nt = nt_;
    
    if(nt > 0 && T_ > 0) {
      std::cerr << "ERROR in Problem_burgersDensity2d::init(): nt = " <<
	nt << " > 0 && T = " << T_ << " > 0." << std::endl;
      exit(1);						   
    }
    
    dt_ = fabs(cfl_*dx_);

    if(nt > 0) {
      nt_ = nt;
      T_ = nt_*dt_;
    }
    else
      nt_ = int(T_/dt_) + 1;

    dt_ *= T_/(nt_*dt_);

    logParameter();

    initialized_ = true;
  }

  void errorComp(Array const* c);

  Vector initialCondition(double x, double y, double z) {
    return Vector{1., 0, 0, sin(2*M_PI*z/lz_)};
  }

  void getReference(Array & c) {}

  void lhs(Array& cNew, Array const& c, Array const& rhs) const {
    cNew = c + dt_*rhs;
  }    

  VecVec flux_vector(Vector const& varArray) const {
    double rho = varArray[Var::rho];
    double rhoUx = varArray[Var::rhoUx];
    double rhoUy = varArray[Var::rhoUy];
    double rhoUz = varArray[Var::rhoUz];
    
    double ux = rhoUx/rho;
    double uy = rhoUy/rho;
    double uz = rhoUz/rho;
    
    return VecVec{ Vector{rhoUx, rhoUx*ux, rhoUx*uy, rhoUx*uz},
            	   Vector{rhoUy, rhoUy*ux, rhoUy*uy, rhoUy*uz},
	           Vector{rhoUz, rhoUz*ux, rhoUz*uy, rhoUz*uz} };
  }

  BK::Array<double,3> dFlux_value(Vector const& varArray) {
    double rho = varArray[Var::rho];
    double rhoUx = varArray[Var::rhoUx];
    double rhoUy = varArray[Var::rhoUy];
    double rhoUz = varArray[Var::rhoUz];
    
    double ux = rhoUx/rho;
    double uy = rhoUy/rho;
    double uz = rhoUz/rho;

    return BK::Array<double,3>({ux, uy, uz});
  }
  
  void computeNumericalFlux(FluxVecArray& fluxVecArrayX, FluxVecArray& fluxVecArrayY, FluxVecArray& fluxVecArrayZ,
			    FluxInfoBordArray& fluxInfoArrayX, FluxInfoBordArray& fluxInfoArrayY, FluxInfoBordArray& fluxInfoArrayZ) {
    auto fluxFunc = [this](Vector const& varArray) { return this->flux_vector(varArray);};
    auto dfluxFunc = [this](Vector const& varArray) { return this->dFlux_value(varArray);};
    
    computeNumericalFlux_LF(fluxVecArrayX, fluxVecArrayY, fluxVecArrayZ, fluxInfoArrayX, fluxInfoArrayY, fluxInfoArrayZ, fluxFunc, dfluxFunc);
  }
};

typedef Problem_BurgersDensity3d Problem;
extern Problem& problem;

#endif
