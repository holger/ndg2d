#ifndef problem_advection2d_hpp
#define problem_advection2d_hpp

#include <BasisKlassen/parameter.hpp>
#include <BasisKlassen/singleton.hpp>
#include <BasisKlassen/assert.hpp>
#include <BasisKlassen/parseFromFile.hpp>

const int varDim = 1;    // one variable - scalar transport 
const int spaceDim = 2;  // 2d space

#include "cfl.hpp"
#include "notiModify.hpp"
#include "problemBase2d.hpp"
#include "value2d.hpp"
#include "laxFriedrichs1d.hpp"
#include "mpiBase.hpp"
#include "logger.hpp"
//#include <mpi.h>

// LOGGING: ----------------------------------------------------
// #undef LOGLEVEL
// #define LOGLEVEL 4

// linear advection 2D
class Problem_Advection2d : public ProblemBase2d<varDim>
{
public:

  struct Var {
    static const int val = 0;
  };
  
  friend class BK::StackSingleton<Problem_Advection2d>;
  
  Problem_Advection2d() {}

  void errorComp(Array const* c) {

    int ncelXloc = ncelx_ / mpiBase.get_dimSize()[0] ; // local here
    int ncelYloc = ncely_ / mpiBase.get_dimSize()[1] ; // local here
    
    Array cRef(ncelXloc,  ncelYloc , order, order);
    
    getReference(cRef);

    // writeFine(cRef, dirName + "/ref_" +  std::to_string(order) + "-" + std::to_string(rkOrder_) +"_" + std::to_string(ncelx_)+"_"+std::to_string(ncely_), nPoParCel);

    double errorr, error ;
    double errorloc = 0 ;
    size_t nx = c->size(0);
    size_t ny = c->size(1);
    for(size_t ix = 0; ix < nx; ix++)
      for(size_t iy = 0; iy < ny; iy++) {
	errorloc += BK::sqr(value(ix, iy, 0., 0., *c)[Var::val] - value(ix, iy, 0., 0., cRef)[Var::val])*dx_*dy_;
	//std::cout << value(ix, iy, 0., 0., *c)[0] <<"\t" << - value(ix, iy, 0., 0., cRef)[0] <<"\t" <<  errorloc << "\t" << dx_ <<"\t" <<dy_<< std::endl;
	
      }
    //MPI_Reduce(&errorloc, &errorr, 1.,MPI_DOUBLE,MPI_SUM,0,MPI_COMM_WORLD);
    mpiBase.mpiAllReduceSum(&errorloc, &errorr, 1);
    error = sqrt(errorr);
     if (mpiBase.get_commRank() == 0){
    std::string dirName = BK::toString("data/", name_);
    int mkdir = system(BK::toString("mkdir data").c_str());
    mkdir = system(BK::toString("mkdir ",dirName).c_str());
    std::cerr << mkdir << std::endl;
    std::cerr << "writing error to " << BK::toString(dirName + "/error-ndg-",order,"-",rkOrder_,".txt") << std::endl;
    
    /*if (mpiBase.get_commRank() == 0){
      double errorz = errorloc;
      std::ofstream outChrono(BK::toString(dirName + "/error-ndg-",order,"-",rkOrder_,".txt"), std::ios::app);
      outChrono << "mpi0err:" << "\t" << errorz << std::endl;
      outChrono.close(); }
    if (mpiBase.get_commRank() == 1){
      double erroro = errorloc;
      std::ofstream outChrono(BK::toString(dirName + "/error-ndg-",order,"-",rkOrder_,".txt"), std::ios::app);
      outChrono << "mpi1err:" << "\t" << erroro << std::endl;
      outChrono.close(); }*/
    
   
    std::ofstream outChrono(BK::toString(dirName + "/error-ndg-",order,"-",rkOrder_,".txt"), std::ios::app);
    outChrono << ncelx_ << "\t" << error << std::endl;
    outChrono.close(); }
  }
      
  void init(int ncelx, int ncely, std::string parameterFilename) {
    LOGL(1,"Problem_Advection2d:init");

    name_ = "advection2d";
    
    if(!initialized_)
      notifyField.add("fieldFinal", FieldNotifyCallBack(std::bind(&Problem_Advection2d::errorComp,this,std::placeholders::_1), "Problem_Advection2d::errorComp"));
    
    ProblemBase2d::init(ncelx, ncely, parameterFilename);
    
    BK::ReadInitFile initFile(parameterFilename_,true);
    k_ = initFile.getParameter<double>("k");
    kDir_ = initFile.getParameter<double>("kDir");      
    ax_ = initFile.getParameter<double>("ax");
    ay_ = initFile.getParameter<double>("ay");

    int nt = nt_;
    double T = T_;

    // w_ = 2;
    // ox_ = 0.5;
    // oy_ = 0.5;

    dt_ = fabs(cfl_*dx_);

    if(nt > 0 && T > 0) {
      std::cerr << "ERROR in Problem_advection2d::init(): nt = " <<
	nt << " > 0 && T = " << T << " > 0." << std::endl;
      exit(1);						   
    }

    double a;
    if(ax_ > 0) a = ax_;
    else a = ay_;
    
    if(T < 0) {
      T_ = -T*fabs(lx_/a);
    }
    else
      T_ = T;

    if(nt > 0) {
      nt_ = nt;
      T_ = nt_*dt_;
    }
    else
      nt_ = int(T_/dt_) + 1;

    dt_ *= T_/(nt_*dt_);

    // T_ = fabs(lx_/ax_);
    // nt_ = size_t(T_/dt_) + 1;
    // dt_ *= T_/(nt_*dt_);
    
    logParameter();
    LOGL(1,PAR(ax_));
    LOGL(1,PAR(ay_));
    LOGL(1,PAR(kDir_));
    LOGL(1,PAR(k_));

    initialized_ = true;
    
    LOGL(3,"Problem_Advection2d:init-end");
  }

  Vector initialCondition(double x, double y, double shift = 0) {
    assert(initialized_);
    // double x0 = 0.35;
    // double y0 = 0.35;

    // int kMax = 10;
    // double temp = 0;
    // for(int k=1; k<=kMax; k++)
    //   temp += pow(k,-5./6)*sin(2*M_PI*k/lambda_*(x-shift-2*M_PI/(k*k)));

    // return temp;

    if(kDir_ == 0)
      return Vector{sin(2*M_PI*k_*(x-shift)/lx_)};
    else if(kDir_ == 1)
      return Vector{sin(2*M_PI*k_*(y-shift)/ly_)};
    else {
      std::cerr << "ERROR in Problem_Advetion2d::initialCondition: kDir < 0 || kDir > 1\n";
      exit(0);
    }
    // return Vector{sin(2*M_PI*y/ly)};

    //    return Vector{exp(-((x-x0)*(x-x0)+(y-y0)*(y-y0))/(0.1*0.1))};
  }

  void getReference(Array & c) {
    BK::Vector<double> xi = points[order-1];
    BK::Vector<double> wi = weights[order-1];
    
    size_t ncelXloc = ncelx_ / mpiBase.get_dimSize()[0] ; // local here
    size_t ncelYloc = ncely_ / mpiBase.get_dimSize()[1] ; // local here
    //int ncelYloc = ncely_ ;

    // double startvalx = 0.0 + mpiBase.getCartCoords(mpiBase.get_commRank())[0] * (lx_/ mpiBase.get_dimSize()[0]);
    //double startvaly = 0.0 + mpiBase.getCartCoords(mpiBase.get_commRank())[1] * (ly_/ mpiBase.get_dimSize()[1]);
    double startvalx = 0.0 +  mpiBase.procCoord()[0] * (lx_/ mpiBase.get_dimSize()[0]);
    double startvaly = 0.0 +  mpiBase.procCoord()[1] * (ly_/ mpiBase.get_dimSize()[1]);
    //double startvaly = 0.0 ;
    
    for(size_t ix=0;ix<ncelXloc ;ix++) {
      auto transX = [ix,startvalx,this](double x) { return startvalx + ix*dx_+0.5*dx_+x*0.5*dx_;};
      for(size_t iy=0;iy< ncelYloc;iy++) {
	auto transY = [iy,startvaly,this](double y) { return startvaly +  iy*dy_+0.5*dy_+y*0.5*dy_;};
	
    	for(size_t ox=0; ox<order;ox++)
    	  for(size_t oy=0;oy<order;oy++) 
	    c(ix,iy,ox,oy) = initialCondition(transX(xi[ox]), transY(xi[oy]));
	
      }
    }
  }

  void lhs(Array& cNew, Array const& c, Array const& rhs) const {
    cNew = c + dt_*rhs;
  }    

  VecVec flux_vector(Vector const& varArray) {
    assert(initialized_);
    
    // double rox = (cellX+x)*dx-ox_;
    // double roy = (cellY+y)*dy-oy_;
    // double vx = -w_ * roy;
    // double vy = w_ * rox;
    
    return VecVec{Vector{ax_*varArray}, Vector{ay_*varArray}};
  }

  BK::Array<double,2> dFlux_value(Vector const& varArray) {
    assert(initialized_);
    
    // double rox = (cellX+x)*dx-ox_;
    // double roy = (cellY+y)*dy-oy_;
    // double vx = -w_ * roy;
    // double vy = w_ * rox;

    return BK::Array<double,2>({ax_, ay_});
  }

  void computeNumericalFlux(FluxVecArray& fluxVecArrayX, FluxVecArray& fluxVecArrayY,
			    FluxInfoBordArray& fluxInfoArrayX, FluxInfoBordArray& fluxInfoArrayY) {
    auto fluxFunc = [this](Vector const& varArray) { return this->flux_vector(varArray);};
    auto dfluxFunc = [this](Vector const& varArray) { return this->dFlux_value(varArray);};

    computeNumericalFlux_LF(fluxVecArrayX, fluxVecArrayY, fluxInfoArrayX, fluxInfoArrayY, fluxFunc, dfluxFunc);
  }

  CONSTPARA(double, w);
  CONSTPARA(double, ox);
  CONSTPARA(double, oy);
  CONSTPARA(double, ax);
  CONSTPARA(double, ay);
  CONSTPARA(int, k);
  CONSTPARA(int, kDir);
  
  const BK::Array<BK::Array<int, 2>, 2> vecIndizes = {{0,1},{2,3}};
};

typedef Problem_Advection2d Problem;
extern Problem& problem;

#endif
