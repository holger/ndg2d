#ifndef problem2d_hpp
#define problem2d_hpp

#ifdef PROBLEM_ADVECTION2D
#include "problem_advection2d.hpp"
#endif

#ifdef PROBLEM_ADVECTIONSOURCE2D
#include "problem_advectionSource2d.hpp"
#endif

#ifdef PROBLEM_ADVECTIONCUBED
#include "problem_advectionCubed.hpp"
#endif

#ifdef PROBLEM_ADVECTIONCUBEDNAIR
#include "problem_advectionCubedNair.hpp"
#endif

#ifdef PROBLEM_BURGERSDENSITY2D
#include "problem_burgersDensity2d.hpp"
#endif

#ifdef PROBLEM_BURGERSDENSITYVISCOSITY2D
#include "problem_burgersDensityViscosity2d.hpp"
#endif

#ifdef PROBLEM_EULERISO2D
#include "problem_eulerIso2d.hpp"
typedef Problem_EulerIso2d Problem;
#endif

#ifdef PROBLEM_EULERISOSCALAR2D
#include "problem_eulerIsoScalar2d.hpp"
typedef Problem_EulerIsoScalar2d Problem;
#endif

#ifdef PROBLEM_EULERISOCUBED
#include "problem_eulerIsoCubed.hpp"
typedef Problem_EulerIsoCubed Problem;
#endif

#ifdef PROBLEM_EULERISOVISCOSITY2D
#include "problem_eulerIsoViscosity2d.hpp"
#endif

#ifdef PROBLEM_NLS2D
#include "problem_nls2d.hpp"
#endif

#ifdef PROBLEM_HEAT2D
#include "problem_heat2d.hpp"
#endif

#ifdef PROBLEM_SHALLOWWATER2D
#include "problem_shallowWater2d.hpp"
typedef Problem_ShallowWater2d Problem;
#endif

#ifdef PROBLEM_ADVECTIONSOURCECUBED
#include "problem_advectionSourceCubed.hpp"
#endif

#ifdef PROBLEM_BURGERSDENSITYCUBED
#include "problem_burgersDensityCubed.hpp"
#endif

#ifdef PROBLEM_SHALLOWWATERCUBED
#include "problem_shallowWaterCubed.hpp"
#endif

#ifdef PROBLEM_SHALLOWWATERCUBEDNAIR
#include "problem_shallowWaterCubedNair.hpp"
#endif

extern Problem& problem;

#endif
