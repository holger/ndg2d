#ifndef problem1d_hpp
#define problem1d_hpp

#ifdef PROBLEM_ADVECTION1D
#include "problem_advection1d.hpp"
#endif

#ifdef PROBLEM_ADVECTIONSOURCE1D
#include "problem_advectionSource1d.hpp"
#endif

#ifdef PROBLEM_BURGERS1D
#include "problem_burgers1d.hpp"
#endif

#ifdef PROBLEM_BURGERSSOURCE1D
#include "problem_burgersSource1d.hpp"
#endif

#ifdef PROBLEM_BURGERSDENSITY1D
#include "problem_burgersDensity1d.hpp"
#endif

#ifdef PROBLEM_BURGERSDENSITYVISCOSITY1D
#include "problem_burgersDensityViscosity1d.hpp"
#endif

#ifdef PROBLEM_EULERISO1D
#include "problem_eulerIso1d.hpp"
#endif

#ifdef PROBLEM_SHALLOWWATER1D
#include "problem_shallowWater1d.hpp"
#endif

#ifdef PROBLEM_EULERISOVISCOSITY1D
#include "problem_eulerIsoViscosity1d.hpp"
#endif

#ifdef PROBLEM_HEAT1D
#include "problem_heat1d.hpp"
#endif

#endif
