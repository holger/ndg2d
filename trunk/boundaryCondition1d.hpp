#ifndef boundaryCondition1d_hpp
#define boundaryCondition1d_hpp

// class PeriodicBoundaryCondition1d
// {
// public:
//   template<class Array>
//   void operator()(Array& c)
//   {
//     int ncelX = c.size(0);
//     int orderX = c.size(1);
    
//     for(int ox=0; ox<orderX; ox++) {
//       c(0, ox) = c(ncelX-2, ox);
//       c(ncelX-1, ox) = c(1, ox);
//     }
//   }
// };

class PeriodicBoundaryCondition1dFlux
{
public:
  template<class FluxInfoVecBordArray>
  void operator()(FluxInfoVecBordArray& fluxInfoArray)
  {
    size_t ncel = fluxInfoArray.size();

    enum Val { minus, plus };
    
    fluxInfoArray(0)[Val::minus] = fluxInfoArray(ncel-1)[Val::minus];
    fluxInfoArray(ncel-1)[Val::plus] = fluxInfoArray(0)[Val::plus];  
  }
};

#endif
