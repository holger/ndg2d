#ifndef upwind_hpp
#define upwind_hpp

#include "problem.hpp"

// upwind
double flux_numeric_XR(int cellX, int cellY, double y, Array const& c)
{
  return flux_vector(cellX, cellY, 1, y, c)[0];
}

double flux_numeric_XL(int cellX, int cellY, double y, Array const& c)
{
  return flux_vector(cellX-1, cellY, 1, y, c)[0];
}

double flux_numeric_YT(int cellX, int cellY, double x, Array const& c)
{
  return flux_vector(cellX, cellY, x, 1, c)[1];
}

double flux_numeric_YB(int cellX, int cellY, double x, Array const& c)
{
  return flux_vector(cellX, cellY-1, x, 1, c)[1];
}

#endif
