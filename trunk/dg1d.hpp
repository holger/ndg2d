#ifndef dg1d_hpp
#define dg1d_hpp

#include "order.hpp"
#include "problem1d.hpp"

// 1D version -----------------------------------------------------------------------

class Rhs1d
{
public:

  Rhs1d(size_t ncel, double dx) : coeffX{order, order} {
    
    dx_ = dx;
    ncel_ = ncel;

    coeffSource.resize(order);
    
    wi = weights[order-1];
    DLagrangeMatrix<order> dLagrangeMatrix;
    dLagrangeMatrix.init();
    LagrangeMatrix<order> lagrangeMatrix;
    
    for(int index=0; index<order; index++) {
      coeffSource[index] = wi[index]*problem.get_dx()/2;
      for(int i=0; i<order; i++) {
	coeffX(index, i) = wi[i]*dLagrangeMatrix(index,i);
      }
    }
    
    sourceArray.resize(ncel_, order);
    sourceArray = Problem::Vector(0.);
  }  

  template<class Array, class BoundaryCondition>
  void operator()(Array const& c, Array & rhs, BoundaryCondition boundaryCondition);

  Problem::Array sourceArray;
  
private:
  
  BK::MultiArray<2, double> coeffX;
  BK::Vector<double> coeffSource;
  
  CONSTPARA(double, dx);
  CONSTPARA(size_t, ncel);
  
  BK::Vector<double> wi;
};

template<class Array, class BoundaryCondition>
void Rhs1d::operator()(Array const& c, Array & rhs, BoundaryCondition boundaryCondition) {
  
  for(int cellX=0; cellX<int(c.size(0)); cellX++)
    for(int orderX=0; orderX<int(c.size(1)); orderX++) {
      
      rhs(cellX, orderX) = 0;
      
      for(int i=0; i<order;i++) 
	rhs(cellX, orderX) += problem.flux_vector(c(cellX, i), cellX, i)[0]*coeffX(orderX, i); 

      rhs(cellX, orderX) += sourceArray(cellX, orderX)*coeffSource[orderX];
    }

  size_t ncel = c.size(0);
  
  Problem::FluxInfoBordArray fluxInfoArray(ncel+1);
  problem.compute_fluxInfoArray(fluxInfoArray, c);

  boundaryCondition(fluxInfoArray);

  Problem::FluxVecArray fluxArray(ncel+1);
  problem.compute_fluxArray(fluxArray, fluxInfoArray);
  
  // treating fluxes from borders 1 to ncel-1
  for(int cellX=0; cellX<int(c.size(0)-1); cellX++) {
    Problem::Vector fluxXR = fluxArray(cellX+1);
    rhs(cellX, order-1) -= fluxXR;
    rhs(cellX+1, 0) += fluxXR;
  }

  rhs(0, 0) += fluxArray(0);
  rhs(int(ncel)-1, order-1) -= fluxArray(ncel);
  
  for(int cellX=0; cellX<int(c.size(0)); cellX++)
    for(int orderX=0; orderX<int(c.size(1)); orderX++) {
      double norm = 2./(problem.get_dx()*wi[orderX]);
      rhs(cellX, orderX) *= norm;
    }
}

#endif
