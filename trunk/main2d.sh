#! /bin/bash

cp order.hpp_template order.hpp
sed -i 's/static constexpr int order = 3/static constexpr int order = 3/g' order.hpp
sed -i 's/static constexpr int rkOrder = 3/static constexpr int rkOrder = 3/g' order.hpp
cd release
make advection2d

./advection2d ../problem_advection2d3.init

cd ..

cp order.hpp_template order.hpp
sed -i 's/static constexpr int order = 3/static constexpr int order = 4/g' order.hpp
sed -i 's/static constexpr int rkOrder = 3/static constexpr int rkOrder = 4/g' order.hpp
cd release
make advection2d

./advection2d ../problem_advection2d4.init

cd ..


cp order.hpp_template order.hpp
sed -i 's/static constexpr int order = 3/static constexpr int order = 5/g' order.hpp
sed -i 's/static constexpr int rkOrder = 3/static constexpr int rkOrder = 5/g' order.hpp
cd release
make advection2d

./advection2d ../problem_advection2d5.init

cd ..


cp order.hpp_template order.hpp
sed -i 's/static constexpr int order = 3/static constexpr int order = 6/g' order.hpp
sed -i 's/static constexpr int rkOrder = 3/static constexpr int rkOrder = 6/g' order.hpp
cd release
make advection2d

./advection2d ../problem_advection2d6.init


# ./main2d 4 70

# cp order.hpp_template order.hpp
# sed -i 's/static constexpr int order = 3/static constexpr int order = 5/g' order.hpp
# sed -i 's/static constexpr int rkOrder = 3/static constexpr int rkOrder = 5/g' order.hpp
# make clean; make main2d

# ./main2d 4 40

# cp order.hpp_template order.hpp
# sed -i 's/static constexpr int order = 3/static constexpr int order = 6/g' order.hpp
# sed -i 's/static constexpr int rkOrder = 3/static constexpr int rkOrder = 6/g' order.hpp
# make clean; make main2d

# ./main2d 4 24

# cp order.hpp_template order.hpp
# sed -i 's/static constexpr int order = 3/static constexpr int order = 6/g' order.hpp
# sed -i 's/static constexpr int rkOrder = 3/static constexpr int rkOrder = 3/g' order.hpp
# make clean; make main2d

# ./main2d 4 24


