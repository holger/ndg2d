#ifndef problem_advectionSourceCubed_hpp
#define problem_advectionSourceCubed_hpp

#include <BasisKlassen/parameter.hpp>
#include <BasisKlassen/array.hpp>
#include <BasisKlassen/parseFromFile.hpp>

const int varDim = 3;   // three variables: sqrtG*h, sqrtG*h*ux, sqrtG*h*uy
const int spaceDim = 2; // 2d space

#include "types.hpp"
#include "cfl.hpp"
#include "problemBase2d.hpp"
#include "laxFriedrichs1d.hpp"
#include "gaussLobatto.hpp"
#include "order.hpp"
#include "cubedSphere_utils.hpp"

extern CubedSphere_utils& cs_utils;

// 2D isothermal gaz on cubed sphere geometry
class Problem_AdvectionSourceCubed : public ProblemBase2d<varDim>
{
public:

  struct Var {
    static const int sqrtG_h = 0;      // sqrt(G)*h
    static const int sqrtG_h_ua = 1;   // sqrt(G)*h*ua
    static const int sqrtG_h_ub = 2;   // sqrt(G)*h*ub
  };

  static const int fluxInfoNumber = 3;
  
  const BK::Array<BK::Array<int, 2>, 1> fluxVarVecIndizes = {{Var::sqrtG_h_ua, Var::sqrtG_h_ub}};
  const BK::Array<int, 1> fluxVarScalarIndizes = {Var::sqrtG_h};

  typedef BK::Array<double, varDim> FluxInfo;                         // [sqrtG_h, sqrtG_h_ua, sqrtG_h_ub] 
  typedef BK::Array<FluxInfo, 2> FluxInfoBord;                        // [+-][sqrtG_h, sqrtG_h_ua, sqrtG_h_ub] 
  typedef BK::MultiArray<spaceDim+1, FluxInfoBord> FluxInfoBordArray; // (cell)[+-][sqrtG_h, sqrtG_h_ua, sqrtG_h_ub]
  typedef BK::MultiArray<spaceDim+1, Vector> FluxVecArray;
							       
  friend class BK::StackSingleton<Problem_AdvectionSourceCubed>;

  Problem_AdvectionSourceCubed() {}
  
  void lhs(Array& cNew, Array const& c, Array const& rhs) const {
    cNew = c + dt_*rhs;
  }    

  void init(int ncelx, int ncely, std::string parameterFilename) {

    initialized_ = true;
    
    ncelx_ = ncelx;
    ncely_ = ncely;

    parameterFilename_ = parameterFilename;

    BK::ReadInitFile initFile(parameterFilename,true);
    rkOrder_ = initFile.getParameter<size_t>("rkOrder");
    lx_ = initFile.getParameter<double>("lx");
    ly_ = initFile.getParameter<double>("ly");
    T_ = initFile.getParameter<double>("T");
    double nt = initFile.getParameter<double>("nt");
    double cflFactor = initFile.getParameter<double>("cfl");

    uaVec.resize(6, BK::MultiArray<spaceDim*2,double>(ncelx_, ncely, order, order));
    ubVec.resize(6, BK::MultiArray<spaceDim*2,double>(ncelx_, ncely, order, order));

    duabVec.resize(6, BK::MultiArray<spaceDim*2,Mat2x2>(ncelx_, ncely, order, order));

    initVelocity();
    
    if(nt > 0 && T_ > 0) {
      std::cerr << "ERROR in Problem_advectionSource2d::init(): nt = " <<
	nt << " > 0 && T = " << T_ << " > 0." << std::endl;
      exit(1);						   
    }

    cfl_ = cflFactor*cflLimit[rkOrder_-1][order-1];
    
    dx_ = lx_/ncelx;
    dy_ = ly_/ncely;

    assert(dx_ == dy_);
    
    dt_ = fabs(cfl_*dx_);

    if(nt > 0) {
      nt_ = nt;
      T_ = nt_*dt_;
    }
    else
      nt_ = int(T_/dt_) + 1;

    dt_ *= T_/(nt_*dt_);

    da_ = M_PI/2./ncelx_;
    db_ = M_PI/2./ncely_;
    
    name_ = "advectionSourceCubed";

    //    dt_ /= 10;
    
    logParameter();
  }

  void initVelocity() {
    std::ofstream outx("initVx.csv"); outx << "x,y,z,vx,vy,vz,dv00,dv01,dv10,dv11" << std::endl;
    
    BK::Vector<double> xi = points[order-1];

    da_ = M_PI/2./ncelx_;
    db_ = M_PI/2./ncely_;

    for(size_t a = 0; a<ncelx_; a++) {
      auto alphaFunc = [a, this](double x) { return -M_PI/4. + a*da_+ 0.5*da_ + x*0.5*da_;};
      for(size_t b = 0; b<ncely_; b++) {
	auto betaFunc = [b, this](double y) { return -M_PI/4. + b*db_+ 0.5*db_ + y*0.5*db_;};
	for(size_t ia = 0; ia < order; ia++)
	  for(size_t ib = 0; ib < order; ib++) {

	    double alpha = alphaFunc(xi[ia]);
	    double beta = betaFunc(xi[ib]);
	    BK::Array<double,3> xyz;
	    BK::Array<double,3> v;
	    BK::Array<double,3> v2;

	    for(int face=0; face<6;face++) {
	      xyz = ab2xyz[face](alpha, beta);
	      setVelocity(xyz,v);
	      setDVelocity(duabVec[face](a,b,ia,ib), alpha, beta, face);
	      outx<<xyz[0]<<","<<xyz[1]<<"," <<xyz[2]<<","<<v[0]<<","<<v[1]<<","<<v[2]<<","
		  <<duabVec[face](a,b,ia,ib)[0][0]<<","<<duabVec[face](a,b,ia,ib)[0][1]
		  <<duabVec[face](a,b,ia,ib)[1][0]<<","<<duabVec[face](a,b,ia,ib)[1][1]<<std::endl;
	      
	      d_map[face](v, uaVec[face](a, b, ia, ib), ubVec[face](a, b, ia, ib), xyz);
	      d_parametrisation[face](v2, uaVec[face](a, b, ia, ib), ubVec[face](a, b, ia, ib), alpha, beta);
	      
	      // tensor_xyz2ab(duabVec[face](a,b,ia,ib), dv_xyz, get_d_map[face](alpha, beta), get_d_para[face](alpha,beta));
	      
	      // tensor_ab2xyz(dv_xyz2, duabVec[face](a,b,ia,ib), get_d_map[face](alpha, beta), get_d_para[face](alpha,beta));

	      outx<<xyz[0]<<","<<xyz[1]<<"," <<xyz[2]<<","<<v2[0]<<","<<v2[1]<<","<<v2[2]<<","
		  <<duabVec[face](a,b,ia,ib)[0][0]<<","<<duabVec[face](a,b,ia,ib)[0][1]
		  <<duabVec[face](a,b,ia,ib)[1][0]<<","<<duabVec[face](a,b,ia,ib)[1][1]<<std::endl;

	      //	      std::cout << "alpha, beta = " << alpha << "," << beta << std::endl;

	    }
	  }
      }
    }
    outx.close();
  }
  
  inline double sec(double phi)
  {
    return 1./cos(phi);
  };
  
  void setDVelocity(Mat2x2& dv, double alpha, double beta, int face) {
    double omega = 1;
    switch(face) {
    case 0:
      dv = {{0, 0}, {omega*cos(beta)*BK::sqr(sec(alpha))*sin(beta), 
		     omega*cos(2*beta)*tan(alpha)}};
      break;
    case 1:
      dv = {{0, 0}, {omega*cos(beta)*BK::sqr(sec(alpha))*sin(beta), 
		     omega*cos(2*beta)*tan(alpha)}};
      break;
    case 2:
      dv = {{0, 0}, {omega*cos(beta)*BK::sqr(sec(alpha))*sin(beta), 
		     omega*cos(2*beta)*tan(alpha)}};
      break;
    case 3:
      dv = {{0, 0}, {omega*cos(beta)*BK::sqr(sec(alpha))*sin(beta), 
		     omega*cos(2*beta)*tan(alpha)}};
      break;
    case 4:
      dv = {{omega*sin(2*alpha)*tan(beta), -omega*BK::sqr(cos(alpha))*BK::sqr(sec(beta))},
	     {omega*BK::sqr(cos(beta))* BK::sqr(sec(alpha)), -omega*sin(2*beta)*tan(alpha)}};

      break;
    case 5:
      dv = {{-omega*sin(2*alpha)*tan(beta), omega*BK::sqr(cos(alpha))*BK::sqr(sec(beta))},
	    {-omega*BK::sqr(cos(beta))* BK::sqr(sec(alpha)), omega*sin(2*beta)*tan(alpha)}};
     
    break;
    }
  }
  
  void setVelocity(BK::Array<double,3> xyz, BK::Array<double,3>& v){
    double omega = 1;    
    double X = xyz[0];
    double Y = xyz[1];
    //    double Z = xyz[2];

    // // Deformational flow
    // auto pt = xyz2pt(X,Y,Z);
    // double theta = pt[1];
    // double r0 = 1;
    // double rho = r0*cos(theta);
    // double vt;
    
    // if(rho == 0)
    //   vt = 0;
    // else
    //   vt = 1.5*sqrt(3)*1./(cosh(rho)*cosh(rho))*tanh(rho);

    // omega = vt/rho;

    // rotation around z
    v[0] = -omega*Y;
    v[1] = omega*X;
    v[2] = 0;

    // v[0] = 0;
    // v[1] = 0;
    // v[2] = 0;

    // rotation around x
    // v[0] = 0;
    // v[1] = -omega*Z;
    // v[2] = omega*Y;

    // rotation around y
    // v[0] = omega*Z;
    // v[1] = 0;
    // v[2] = -omega*X;
  }

  Vector initialCondition(int a, int b, int ia, int ib, int face, double shiftAngle = 0) {
    auto alphaFunc = [a, this](double x) { return -M_PI/4. + a*da_+ 0.5*da_ + x*0.5*da_;};
    auto betaFunc = [b, this](double y) { return -M_PI/4. + b*db_+ 0.5*db_ + y*0.5*db_;};

    BK::Vector<double> xi = points[order-1];
    
    double alpha = alphaFunc(xi[ia]);
    double beta = betaFunc(xi[ib]);
    
    BK::Array<double,3> xyz = ab2xyz[face](alpha, beta);
    BK::Array<double,2> pt = xyz2pt(xyz[0], xyz[1], xyz[2]);

    // BK::Array<double,3> v;
    // Mat3x3 dv;
    // setVelocity(xyz, v, dv);

    // double ua, ub;
    // d_map[face](v, ua, ub, xyz);

    // // saving mapped velocities in arrays
    // uaVec[face](a,b,ia,ib) = ua;
    // ubVec[face](a,b,ia,ib) = ub; 

    double ua = uaVec[face](a,b,ia,ib);
    double ub = ubVec[face](a,b,ia,ib);
    
    // double dua, dub;
    // d_map[face](dv, dua, dub, xyz);

    // // saving mapped derivatives of velocities in arrays
    // duaVec[face](a,b,ia,ib) = dua;
    // dubVec[face](a,b,ia,ib) = dub; 


    //double value = cos(pt[0])*sin(pt[1]);
    double value = 2+cos(pt[0]+M_PI/2.)*sin(pt[1]);
    //double value = 1;

    double sqrtG = cs_utils.sqrtG(a, b, ia, ib);
    
    return {sqrtG*value, sqrtG*value*ua, sqrtG*value*ub};
    //    return {sqrtG*value, value*ua, value*ub};
  }

  // Vector initialCondition(double phi, double theta) {
  //   // Blob --------------------------------------------------
  //   double h0 = 1;
  //   double deltaC = 0.4;
    
  //   double phi0 = 0;//M_PI/2;
  //   double theta0 = M_PI/2;
    
  //   double deltaPhi = fabs(phi0 - phi);
  //   double deltaTheta = fabs(theta0 - theta);
  //   double centralAngle = sqrt(deltaPhi*deltaPhi + deltaTheta*deltaTheta);

  //   double value = 1+h0/2*(1+cos(M_PI*centralAngle/deltaC));
  //   if(centralAngle > deltaC)
  //     value = 1;

  //   value = 2+cos(phi)*sin(theta);

  //   return Vector{value, 0, 0};
  //   //    return Vector{1+0.1*exp(-(BK::sqr(x-lx_/2)+BK::sqr(y-ly_/2))/BK::sqr(0.5)), 0, 0};
  // }

  std::string toWriteCSV_info()
  {
    return "x,y,z,h,ua,ub,vx,vy,vz,sqrtG,gamma111,gamma112,gamma122,gamma211,gamma212,gamma222";
  }

  BK::Vector<double> toWriteCSV(Array const& array, int a, int b, int ia, int ib, int face)
  {
    auto data = array(a, b, ia, ib);
    double sqrtG = cs_utils.sqrtG(a, b, ia, ib);
    
    double rho = data[0]/sqrtG;
    double ua = data[1]/data[0];
    double ub = data[2]/data[0];

    auto alphaF = [a, this](double x) { return -M_PI/4. + a*da_+ 0.5*da_ + x*0.5*da_;};
    auto betaF = [b, this](double y) { return -M_PI/4. + b*db_ + 0.5*db_ + y*0.5*db_;};

    BK::Vector<double> xi = points[order-1];

    ua = uaVec[face](a, b, ia, ib);
    ub = ubVec[face](a, b, ia, ib);

    double alpha = alphaF(xi[ia]);
    double beta = betaF(xi[ib]);
    BK::Array<double,3> v;
    d_parametrisation[face](v, ua, ub, alpha, beta);

    Mat3x3 dv_xyz;
    tensor_ab2xyz(dv_xyz, duabVec[face](a,b,ia,ib), get_d_map[face](alpha,beta), get_d_para[face](alpha,beta));
    
    double gamma111 = cs_utils.gamma111(a,b,ia,ib);
    double gamma112 = cs_utils.gamma112(a,b,ia,ib);
    double gamma122 = cs_utils.gamma122(a,b,ia,ib);
    double gamma211 = cs_utils.gamma211(a,b,ia,ib);
    double gamma212 = cs_utils.gamma212(a,b,ia,ib);
    double gamma222 = cs_utils.gamma222(a,b,ia,ib);
  
    BK::Vector<double> toWriteVec({rho,ua,ub,v[0],v[1],v[2], sqrtG, gamma111, gamma112, gamma122, gamma211,gamma212, gamma222});
    
    return toWriteVec;
  }

  BK::Array<double,3> computeT(Vector data)
  {
    double q = data[0];
    double ma = data[1];
    double mb = data[2];
    
    double T11 = ma*ma/q;
    double T12 = ma*mb/q;
    double T22 = mb*mb/q;

    return {T11, T12, T22};
  }

  VecVec flux_vector(Vector data) {

    BK::Array<double,3> T = computeT(data);
    // BK::Array<double,3> T = {0,0,0};
    // f0x = sqrtG*h*Ux; f1x = sqrtG*h*ux*ux; f2x = sqrtG*h*ux*uy
    // f0y = sqrtG*h*Uy; f1y = sqrtG*h*uy*ux; f2y = sqrtG*h*uy*uy
    double ma = data[Var::sqrtG_h_ua];
    double mb = data[Var::sqrtG_h_ub];
    
    auto flux = VecVec{ Vector{ma, T[0], T[1]},
			Vector{mb, T[1], T[2]} };

    return flux;
  }

  BK::Array<double,2> dFlux_value(Vector data) {

    double sqrtG_h = data[0];
    double sqrtG_hUx = data[1];
    double sqrtG_hUy = data[2];
    
    double ux = sqrtG_hUx/sqrtG_h;
    double uy = sqrtG_hUy/sqrtG_h;

    auto return_value = BK::Array<double,2>({fabs(ux), fabs(uy)});
    
    return return_value;
  }

  void computeSourceTerm(Array const& c, Array & s, int face) {
    
    for(int a=0; a<int(c.size(0)); a++)
      for(int b=0; b<int(c.size(1)); b++)
	for(int ia=0; ia<int(c.size(2)); ia++)
	  for(int ib=0; ib<int(c.size(3)); ib++) {
	    // BK::Array<double,3> T = computeT(a,b,ia,ib, c(a,b,ia,ib));
	    // double gamma111 = cs_utils.gamma111(a,b,ia,ib);
	    // double gamma112 = cs_utils.gamma112(a,b,ia,ib);
	    // double gamma122 = cs_utils.gamma122(a,b,ia,ib);
	    // double gamma211 = cs_utils.gamma211(a,b,ia,ib);
	    // double gamma212 = cs_utils.gamma212(a,b,ia,ib);
	    // double gamma222 = cs_utils.gamma222(a,b,ia,ib);
	    
	    // s(a,b,ia,ib)[1] = (-gamma111*T[0] - 2*gamma112*T[1] - gamma112*T[2]);
	    
	    // s(a,b,ia,ib)[2] = (-gamma211*T[0] - 2*gamma212*T[1] - gamma222*T[2]);

	    double sqrtG_hUx = c(a, b, ia, ib)[1];
	    double sqrtG_hUy = c(a, b, ia, ib)[2];
    
	    s(a,b,ia,ib)[1] = sqrtG_hUx*duabVec[face](a,b,ia,ib)[0][0] + sqrtG_hUy*duabVec[face](a,b,ia,ib)[0][1];
	    
	    s(a,b,ia,ib)[2] = sqrtG_hUx*duabVec[face](a,b,ia,ib)[1][0] + sqrtG_hUy*duabVec[face](a,b,ia,ib)[1][1];

	    // s(a,b,ia,ib)[1] += sqrtG_hUx*(duabVec[face](a,b,ia,ib)[0][0]
	    // 				  + gamma111*uaVec[face](a,b,ia,ib)
	    // 				  + gamma112*ubVec[face](a,b,ia,ib))

	    //                  + sqrtG_hUy*(duabVec[face](a,b,ia,ib)[0][1]
	    // 				  + gamma112*uaVec[face](a,b,ia,ib)
	    // 				  + gamma122*ubVec[face](a,b,ia,ib));
	    

	    // s(a,b,ia,ib)[2] += sqrtG_hUx*(duabVec[face](a,b,ia,ib)[1][0]
	    // 				  + gamma211*uaVec[face](a,b,ia,ib)
	    // 				  + gamma212*ubVec[face](a,b,ia,ib))

	    //                  + sqrtG_hUy*(duabVec[face](a,b,ia,ib)[1][1]
	    // 				  + gamma212*uaVec[face](a,b,ia,ib)
	    // 				  + gamma222*ubVec[face](a,b,ia,ib));

	  }
  }

  void computeFluxes(FluxArray& fluxArray,
		     FluxInfoBordArray& fluxInfoBordArrayX,
		     FluxInfoBordArray& fluxInfoBordArrayY,
		     Array const& c, int face) {
    
    for(int cellX=0; cellX<int(c.size(0)); cellX++)
      for(int cellY=0; cellY<int(c.size(1)); cellY++)
	for(int indexX=0; indexX<int(c.size(2)); indexX++)
	  for(int indexY=0; indexY<int(c.size(3)); indexY++) {

	    Vector data = c(cellX, cellY, indexX, indexY);
	    
	    VecVec flux = flux_vector(data);
	    
	    fluxArray(cellX, cellY, indexX, indexY) = flux;
	    if(indexX == 0) 
	      fillFluxInfoBordArray(cellX, cellY, indexY, Val::plus, c, fluxInfoBordArrayX, cellX, cellY, indexX, indexY);
	    
	    if(indexX == order-1) 
	      fillFluxInfoBordArray(cellX+1, cellY, indexY, Val::minus, c, fluxInfoBordArrayX, cellX, cellY, indexX, indexY);
	    
	    if(indexY == 0) 
	      fillFluxInfoBordArray(cellX, cellY, indexX, Val::plus, c, fluxInfoBordArrayY, cellX, cellY, indexX, indexY);

	    if(indexY == order-1) 
	      fillFluxInfoBordArray(cellX, cellY+1, indexX, Val::minus, c, fluxInfoBordArrayY, cellX, cellY, indexX, indexY);
	    
	  }
  }

  void computeNumericalFlux(FluxVecArray& fluxVecArrayX, FluxVecArray& fluxVecArrayY,
			    FluxInfoBordArray& fluxInfoArrayX, FluxInfoBordArray& fluxInfoArrayY, int face) {
    
    for(size_t posX=0; posX<fluxVecArrayX.size(0); posX++)
      for(size_t posY=0; posY<fluxVecArrayX.size(1); posY++)
	for(size_t orderY=0; orderY<order; orderY++) {

	  Vector valM = fluxInfoArrayX(posX, posY, orderY)[Val::minus];
	  VecVec fm = flux_vector(valM);

	  Vector valP = fluxInfoArrayX(posX, posY, orderY)[Val::plus];
	  VecVec fp = flux_vector(valP);
				  
	  double dfm = dFlux_value(valM)[Dir::x];
	  double dfp = dFlux_value(valP)[Dir::x];

	  fluxVecArrayX(posX, posY, orderY) =  laxFriedrichs1d_flux_numeric(fm[Dir::x], fp[Dir::x], dfm, dfp, valM, valP);
	}

    for(size_t posX=0; posX<fluxVecArrayY.size(0); posX++)
      for(size_t posY=0; posY<fluxVecArrayY.size(1); posY++)
	for(size_t orderX=0; orderX<order; orderX++) {

	  Vector valM = fluxInfoArrayY(posX, posY, orderX)[Val::minus];
	  VecVec fm = flux_vector(valM);
	  
	  Vector valP = fluxInfoArrayY(posX, posY, orderX)[Val::plus];
	  VecVec fp = flux_vector(valP);
	    
	  double dfm = dFlux_value(valM)[Dir::y];
	  double dfp = dFlux_value(valP)[Dir::y];

	  fluxVecArrayY(posX, posY, orderX) =  laxFriedrichs1d_flux_numeric(fm[Dir::y], fp[Dir::y], dfm, dfp, valM, valP);
	}
  }

  void fillFluxInfoBordArray(int cellXf, int cellYf, int index, int plusMinus, Array const& c, FluxInfoBordArray& fluxInfoBordArray, int cellX, int cellY, int indexX, int indexY) {
    fluxInfoBordArray(cellXf, cellYf, index)[plusMinus][Var::sqrtG_h] = c(cellX, cellY, indexX, indexY)[Var::sqrtG_h];
    fluxInfoBordArray(cellXf, cellYf, index)[plusMinus][Var::sqrtG_h_ua] = c(cellX, cellY, indexX, indexY)[Var::sqrtG_h_ua];
    fluxInfoBordArray(cellXf, cellYf, index)[plusMinus][Var::sqrtG_h_ub] = c(cellX, cellY, indexX, indexY)[Var::sqrtG_h_ub];
  }

  CONSTPARA(double, da);
  CONSTPARA(double, db);

  BK::Vector<BK::MultiArray<spaceDim*2, double>> uaVec;
  BK::Vector<BK::MultiArray<spaceDim*2, double>> ubVec;

  BK::Vector<BK::MultiArray<spaceDim*2, Mat2x2>> duabVec;
};

typedef Problem_AdvectionSourceCubed Problem;
extern Problem& problem;

#endif
