#ifndef dg2d_hpp
#define dg2d_hpp

#include "order.hpp"
#include "problem2d.hpp"
#include <mpi.h>
#include "mpiBase.hpp"
#include "mpiFlux2d.hpp"
#include "lagrange.hpp"

extern XFluxMPI2d& xFluxMPI;
extern YFluxMPI2d& yFluxMPI;

// 2D version -----------------------------------------------------------------------

class Rhs2d
{
public:

  Rhs2d(size_t ncelx, size_t ncely, double dx, double dy) : coeffX{order, order, order}, 
							    coeffY{order, order, order},
							    coeffSource{order, order}
  {
    
    dx_ = dx;
    dy_ = dy;
    ncelX_ = ncelx;
    ncelY_ = ncely;
    
    wi = weights[order-1];
    DLagrangeMatrix<order> dLagrangeMatrix;
    dLagrangeMatrix.init();
    
    for(int indexX=0; indexX<order; indexX++)
      for(int indexY=0; indexY<order; indexY++) {
	coeffSource(indexX, indexY) = wi[indexX]*wi[indexY]*dx_*dy_/4;
    
	for(int i=0; i<order; i++) {
	  coeffX(indexX, indexY, i) = dy_/2.*wi[i]*wi[indexY]*dLagrangeMatrix(indexX,i);
	  coeffY(indexX, indexY, i) = dx_/2.*wi[indexX]*wi[i]*dLagrangeMatrix(indexY,i);
	}
      }
    
    fluxArray.resize(ncelX_, ncelY_, order, order);
    sourceArray.resize(ncelX_, ncelY_, order, order);
    sourceArray = Problem::Vector(0.);
    fluxInfoBordArrayX.resize(ncelX_+1, ncelY_, order);
    fluxInfoBordArrayY.resize(ncelX_, ncelY_+1, order);
    
    fluxVecArrayX.resize(ncelX_+1, ncelY_, order);
    fluxVecArrayY.resize(ncelX_, ncelY_+1, order);
  }  

  template<class Array>
  void addIntegralAndSource(Array & rhs);

  template<class Array>
  void addBorderTerm(Array & rhs);

  Problem::Array sourceArray;
  Problem::FluxArray fluxArray;
  Problem::FluxInfoBordArray fluxInfoBordArrayX;
  Problem::FluxInfoBordArray fluxInfoBordArrayY;
  Problem::FluxVecArray fluxVecArrayX;
  Problem::FluxVecArray fluxVecArrayY;

private:
  
  BK::MultiArray<3, double> coeffX;
  BK::MultiArray<3, double> coeffY;
  BK::MultiArray<2, double> coeffSource;
  
  BK::Vector<double> wi;

  CONSTPARA(double, dx);
  CONSTPARA(double, dy);
  CONSTPARA(size_t, ncelX);
  CONSTPARA(size_t, ncelY);
};

template<class Array>
void Rhs2d::addIntegralAndSource(Array & rhs) {
  
  for(int cellX=0; cellX<int(rhs.size(0)); cellX++)
    for(int cellY=0; cellY<int(rhs.size(1)); cellY++)
      for(int indexX=0; indexX<int(rhs.size(2)); indexX++)
	for(int indexY=0; indexY<int(rhs.size(3)); indexY++) {
	  
	  rhs(cellX, cellY, indexX, indexY) = 0;
	  
	  for(int i=0;i<order;i++) 
	    rhs(cellX, cellY, indexX, indexY) += fluxArray(cellX, cellY, i, indexY)[0]*coeffX(indexX, indexY, i)
	      + fluxArray(cellX, cellY, indexX, i)[1]*coeffY(indexX, indexY, i);

	  rhs(cellX, cellY, indexX, indexY) += sourceArray(cellX, cellY, indexX, indexY)*coeffSource(indexX, indexY);
	}

  mpiBase.waitAll();
  
  xFluxMPI.finish(fluxInfoBordArrayX);
  yFluxMPI.finish(fluxInfoBordArrayY);
}

template<class Array>
void Rhs2d::addBorderTerm(Array & rhs) {

  //  problem.computeNumericalFlux(fluxVecArrayX, fluxVecArrayY, fluxInfoBordArrayX, fluxInfoBordArrayY);
  
  for(int cellX=0; cellX<int(rhs.size(0)-1); cellX++)
    for(int cellY=0; cellY<int(rhs.size(1)); cellY++)
      for(int orderY=0; orderY<int(rhs.size(3)); orderY++) {
  	Problem::Vector fluxXR = fluxVecArrayX(cellX+1, cellY, orderY)*problem.get_dy()/2.*wi[orderY];
  	rhs(cellX, cellY, order-1, orderY) -= fluxXR;
  	rhs(cellX+1, cellY, 0, orderY) += fluxXR;
      }

  for(int cellY=0; cellY<int(rhs.size(1)); cellY++)
    for(int orderY=0; orderY<int(rhs.size(3)); orderY++) {
      rhs(0, cellY, 0, orderY) += fluxVecArrayX(0, cellY, orderY)*problem.get_dy()/2.*wi[orderY];
      rhs(int(ncelX_)-1, cellY, order-1, orderY) -= fluxVecArrayX(int(ncelX_), cellY, orderY)*problem.get_dy()/2.*wi[orderY];
    }
  
  for(int cellX=0; cellX<int(rhs.size(0)); cellX++)
    for(int cellY=0; cellY<int(rhs.size(1)-1); cellY++)
      for(int orderX=0; orderX<int(rhs.size(2)); orderX++) {
  	Problem::Vector fluxYT = fluxVecArrayY(cellX, cellY+1, orderX)*problem.get_dx()/2.*wi[orderX];
  	rhs(cellX, cellY, orderX, order-1) -= fluxYT;
  	rhs(cellX, cellY+1, orderX, 0) += fluxYT;
      }
  
  for(int cellX=0; cellX<int(rhs.size(0)); cellX++)
    for(int orderX=0; orderX<int(rhs.size(2)); orderX++) {
      rhs(cellX, 0, orderX, 0) += fluxVecArrayY(cellX, 0, orderX)*problem.get_dx()/2.*wi[orderX];
      rhs(cellX, int(ncelY_)-1, orderX, order-1) -= fluxVecArrayY(cellX, int(ncelY_), orderX)*problem.get_dx()/2.*wi[orderX];
    }


  for(int cellX=0; cellX<int(rhs.size(0)); cellX++)
    for(int cellY=0; cellY<int(rhs.size(1)); cellY++)
      for(int orderX=0; orderX<int(rhs.size(2)); orderX++)
  	for(int orderY=0; orderY<int(rhs.size(3)); orderY++) {
  	  double norm = 4./(problem.get_dx()*problem.get_dy()*wi[orderX]*wi[orderY]);
  	  rhs(cellX, cellY, orderX, orderY) *= norm;
  	}

}

#endif
