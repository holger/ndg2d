#ifndef problem_eulerIsoViscosity2d_hpp
#define problem_eulerIsoViscosity2d_hpp

#include <BasisKlassen/parameter.hpp>
#include <BasisKlassen/array.hpp>

const int varDim = 7;   // seven variables: rho, mx = rho*ux, my = rho*uy, qxx, qxy, qyx, qyy
                        // qxx = d_x u_x; qxy = d_x u_y; qyx = d_y u_x; qyy = d_y u_y
const int spaceDim = 2; // 2d space

#include "types.hpp"
#include "cfl.hpp"
#include "problemBase2d.hpp"
#include "laxFriedrichs1d.hpp"
#include "centralFlux1d.hpp"
#include "gaussLobatto.hpp"

// Burgers with density and viscosity 2D
class Problem_EulerIsoViscosity2d : public ProblemBase2d<varDim>
{
public:
  
  struct Var {
    static const int rho = 0;    // rho
    static const int mx = 1;     // mx = rho*ux
    static const int my = 2;     // my = rho*uy
    static const int qxx = 3;    // qxx
    static const int qxy = 4;    // qxy
    static const int qyx = 5;    // qyx
    static const int qyy = 6;    // qyy
  };

  friend class BK::StackSingleton<Problem_EulerIsoViscosity2d>;

  Problem_EulerIsoViscosity2d() {}

  void init(int ncelx, int ncely, std::string parameterFilename) {

    name_ = "eulerIsoViscosity2d";

    ProblemBase2d::init(ncelx, ncely, parameterFilename);

    BK::ReadInitFile initFile(parameterFilename,true);
    nu_ = initFile.getParameter<double>("nu");
    a_ = initFile.getParameter<double>("a");
    
    double nt = nt_;

    sqrtNu_ = sqrt(nu_);
    
    if(nt > 0 && T_ > 0) {
      std::cerr << "ERROR in Problem_burgersDensityViscosity2d::init(): nt = " <<
	nt << " > 0 && T = " << T_ << " > 0." << std::endl;
      exit(1);						   
    }

    assert(dx_ == dy_);

    double dtAd = fabs(cfl_*dx_/(1+a_));
    //    double dtAd = fabs(cfl_*dx_ + a_);
    double dtHeat = fabs(cfl_*dx_*dx_/nu_)/4.;

    std::cout << "dtAd = " << dtAd << "\t dtHeat = " << dtHeat << std::endl;

    if(nu_ == 0)
      dt_ = dtAd;
    else
      dt_ = std::min(dtAd,dtHeat);

    if(nt > 0) {
      nt_ = nt;
      T_ = nt_*dt_;
    }
    else
      nt_ = int(T_/dt_) + 1;

    dt_ *= T_/(nt_*dt_);

    logParameter();

    LOGL(0,PAR(nu_));
    LOGL(0,PAR(a_));

    initialized_ = true;
  }

  Vector initialCondition(double x, double y) {
    double k=1;
    return Vector{1, sin(2*M_PI*k*x/lx_), 0, 2*M_PI*k/lx_*cos(2*M_PI*k*(x)/lx_) ,0, 0, 0};
  }

  void getReference(Array & c) {}

  void lhs(Array& cNew, Array const& c, Array const& rhs) const {
    //    cNew = c + dt_*rhs;
    for(size_t i=0; i<c.size(); i++) {
      cNew[i][0] = c[i][0] + dt_*rhs[i][0];
      cNew[i][1] = c[i][1] + dt_*rhs[i][1];
      cNew[i][2] = c[i][2] + dt_*rhs[i][2];
      cNew[i][3] = rhs[i][3];
      cNew[i][4] = rhs[i][4];
      cNew[i][5] = rhs[i][5];
      cNew[i][6] = rhs[i][6];
    }
  }    

  VecVec flux_vector_nonLin(Vector const& varArray) {
    assert(initialized_);
    
    double rho = varArray[Var::rho];
    double rhoUx = varArray[Var::mx];
    double rhoUy = varArray[Var::my];
    double ux = rhoUx/rho;
    double uy = rhoUy/rho;

    // f0x = rhoUx; f1x = rhoUx*ux; f2x = rhoUx*uy;
    // f0y = rhoUy; f1y = rhoUy*ux; f2y = rhoUy*uy;
    return VecVec{ Vector{rhoUx, rhoUx*ux + rho*a_*a_, rhoUx*uy, 0, 0, 0, 0},
	Vector{rhoUy, rhoUy*ux, rhoUy*uy + rho*a_*a_, 0, 0, 0, 0} };
  }

  VecVec flux_vector_diffusion(Vector const& varArray) {
    assert(initialized_);
    
    double rho = varArray[Var::rho];
    double rhoUx = varArray[Var::mx];
    double rhoUy = varArray[Var::my];
    double qxx = varArray[Var::qxx];
    double qxy = varArray[Var::qxy];
    double qyx = varArray[Var::qyx];
    double qyy = varArray[Var::qyy];
    double ux = rhoUx/rho;
    double uy = rhoUy/rho;

    return VecVec{Vector{0,-rho*sqrtNu_*qxx,-rho*sqrtNu_*qxy,-sqrtNu_*ux,-sqrtNu_*uy, 0, 0},
	Vector{0,-rho*sqrtNu_*qyx,-rho*sqrtNu_*qyy, 0, 0, -sqrtNu_*ux, -sqrtNu_*uy}};
  }
  
  VecVec flux_vector(Vector const& varArray) {
    assert(initialized_);

    auto nonLin = flux_vector_nonLin(varArray);
    auto diffusion = flux_vector_diffusion(varArray);

    auto flux = nonLin + diffusion;

    return flux;
  }

  BK::Array<double,2> dFlux_value(Vector const& varArray) {
    double rho = varArray[Var::rho];
    double rhoUx = varArray[Var::mx];
    double rhoUy = varArray[Var::my];

    double ux = rhoUx/rho;
    double uy = rhoUy/rho;

    return BK::Array<double,2>({std::max({fabs(ux), fabs(ux + a_), fabs(ux - a_)}), 
	std::max({fabs(uy), fabs(uy + a_), fabs(uy - a_)})});
  }
  
  void computeNumericalFlux(FluxVecArray& fluxVecArrayX, FluxVecArray& fluxVecArrayY,
			    FluxInfoBordArray& fluxInfoArrayX, FluxInfoBordArray& fluxInfoArrayY) {

    FluxVecArray fluxVecArrayX_nl(ncelx_+1, ncely_, order);
    FluxVecArray fluxVecArrayY_nl(ncelx_, ncely_+1, order);

    auto fluxFunc_nl = [this](Vector const& varArray) { return this->flux_vector_nonLin(varArray);};
    auto dfluxFunc = [this](Vector const& varArray) { return this->dFlux_value(varArray);};

    computeNumericalFlux_LF(fluxVecArrayX_nl, fluxVecArrayY_nl, fluxInfoArrayX, fluxInfoArrayY, fluxFunc_nl, dfluxFunc);
    
    for(size_t cellX=0; cellX<fluxVecArrayX.size(0); cellX++)
      for(size_t cellY=0; cellY<fluxVecArrayX.size(1); cellY++)
	for(size_t orderY=0; orderY<order; orderY++) {
	  fluxVecArrayX_nl(cellX, cellY, orderY)[Var::qxx] = 0;
	  fluxVecArrayX_nl(cellX, cellY, orderY)[Var::qxy] = 0;
	  fluxVecArrayX_nl(cellX, cellY, orderY)[Var::qyx] = 0;
	  fluxVecArrayX_nl(cellX, cellY, orderY)[Var::qyy] = 0;
	}
    
    for(size_t cellX=0; cellX<fluxVecArrayY.size(0); cellX++)
      for(size_t cellY=0; cellY<fluxVecArrayY.size(1); cellY++)
	for(size_t orderX=0; orderX<order; orderX++) {
	  fluxVecArrayY_nl(cellX, cellY, orderX)[Var::qxx] = 0;
	  fluxVecArrayY_nl(cellX, cellY, orderX)[Var::qxy] = 0;
	  fluxVecArrayY_nl(cellX, cellY, orderX)[Var::qyx] = 0;
	  fluxVecArrayY_nl(cellX, cellY, orderX)[Var::qyy] = 0;
	}
    
    FluxVecArray fluxVecArrayX_d(ncelx_+1, ncely_, order);
    FluxVecArray fluxVecArrayY_d(ncelx_, ncely_+1, order);

    auto fluxFunc_d = [this](Vector const& varArray) { return this->flux_vector_diffusion(varArray);};
    
    computeNumericalFlux_Central(fluxVecArrayX_d, fluxVecArrayY_d, fluxInfoArrayX, fluxInfoArrayY, fluxFunc_d);

    fluxVecArrayX = fluxVecArrayX_nl + fluxVecArrayX_d;
    fluxVecArrayY = fluxVecArrayY_nl + fluxVecArrayY_d;

    // for(size_t cellX=0; cellX<fluxVecArrayY.size(0); cellX++) {
    //   std::cout << fluxVecArrayX(cellX, 0 , 0) << "\t" << fluxVecArrayX_nl(cellX, 0 , 0) << "\t" << fluxVecArrayX_d(cellX, 0 , 0) << std::endl;
    // }
    // std::cout << "***" << std::endl;
    // for(size_t cellX=0; cellX<fluxVecArrayY.size(0); cellX++) {
    //   std::cout << fluxVecArrayY(cellX, 0 , 0) << "\t" << fluxVecArrayY_nl(cellX, 0 , 0) << "\t" << fluxVecArrayY_d(cellX, 0 , 0) << std::endl;
    // }
    // exit(0);
      
  }
  
  CONSTPARA(double, nu);
  CONSTPARA(double, sqrtNu);
  CONSTPARA(double, a);
};

typedef Problem_EulerIsoViscosity2d Problem;
extern Problem& problem;

#endif
