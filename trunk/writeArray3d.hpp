#ifndef writeArray3d_hpp
#define writeArray3d_hpp

#include <iostream>
#include <fstream>
#include <string>

#include <BasisKlassen/multiArray.hpp>

#include "value3d.hpp"
#include "mpiBase.hpp"

#ifdef NETCDF
#include <netcdf.h>
#include <netcdf_par.h>
#include "writeArray3d_netcdf.hpp"
#endif

template<class Array>
void writeArray(const Array& array)
{
  for(int m=0; m<array.sizeOrderZ();m++){
  for(int l=0; l<array.sizeOrderY();l++){
    for(int k=0; k<array.sizeOrderX();k++){
      for(int i=0; i<array.size(0);i++){
	for(int j=0;j<array.size(1);j++){
	  for(int r=0;r<array.size(2);r++){
	    std::cout<<array(i,j,r,k,l,m)<<"\t";
	  }
	}std::cout<<std::endl;
	}std::cout<<std::endl;
      }std::cout<<std::endl;
    }std::cout<<std::endl;
  }std::cout<<std::endl;
  }

template<class Array>
void writeArray(const Array& array, std::string filename)
{
  int varDim = array.begin()->size();
  
  for(size_t var = 0; var < varDim; var++) {

    std::ofstream out(BK::toString(filename,"-",var,".data"));
    
    for(size_t ox=0;ox<array.size(3);ox++){
      for(size_t oy=0;oy<array.size(4);oy++){
	for(size_t oz=0;oz<array.size(5);oz++){
	  for(size_t r=0;r<array.size(2);r++){
	for(size_t j=0;j<array.size(1);j++){
	  for(size_t i=0;i<array.size(0);i++){
	    out << i << "," << j << "," << r << "," << array(i,j,r,ox,oy,oz)[var]<<std::endl;
	  }
	}
      }
      }
      }
    }
    out.close();
  }
}
  
template<class Array>
void writeFine(const Array& array, std::string filename, int nPoParCel, double dx, double dy, double dz)
{
  std::cout << "void writeFine(const Array& array, std::string filename, int nPoParCel)" << std::endl;
  
  size_t varDim = (*array.begin()).size();

  // for(size_t var = 0; var < varDim; var++) {
    
  //   typedef BK::MultiArray<3,double> Matrix;
  //   Matrix u(array.size(0)*nPoParCel, array.size(1)*nPoParCel, array.size(2)*nPoParCel);
    
  //   for(size_t ix=0;ix<array.size(0);ix++)
  //     for(size_t iy=0;iy<array.size(1);iy++)
  // 	for(size_t iz=0;iz<array.size(2);iz++)
  // 	for(int jx=0;jx<nPoParCel;jx++)
  // 	  for(int jy=0;jy<nPoParCel;jy++)
  // 	    for(int jz=0;jz<nPoParCel;jz++)
  // 	      u(ix*nPoParCel + jx,iy*nPoParCel+jy,iz*nPoParCel+jz) = value(ix, iy,iz, -1 + jx*2./(nPoParCel), -1 + jx*2./(nPoParCel),-1 + jx*2./(nPoParCel), array)[var];
  
  //     std::ofstream out(BK::toString(filename,"-",var,".data"));
  //   for(size_t k=0;k<array.size(2)*nPoParCel;k++){
  //   for(size_t j=0;j<array.size(1)*nPoParCel;j++){
  //     for(size_t i=0;i<array.size(0)*nPoParCel;i++){
  // 	out<<i << "," << j << "," << k << "," <<u(i,j,k) <<std::endl;
  //     }
  //     }
  //   }
  //   out.close();
  // }
for(size_t var = 0; var < varDim; var++) {

  typedef BK::MultiArray<1,double> Array1;
    Array1 ux(array.size(0)*nPoParCel);
  for(size_t ix=0;ix<array.size(0);ix++)
    for(int jx=0;jx<nPoParCel;jx++)
      ux(ix*nPoParCel+jx) = value(ix, 0, 0, -1 + jx*2./(nPoParCel), 0., 0., array)[var];

   double lx = dx * array.size(0) * mpiBase.get_dimSize()[0];
   double startvalx = 0.0 +  mpiBase.procCoord()[0] * (lx/ mpiBase.get_dimSize()[0]);
  
  std::ofstream out(BK::toString(filename,"-",var,"-x.data"));
  for(size_t i=0;i<array.size(0)*nPoParCel;i++)
    out << startvalx + i*dx/nPoParCel << "\t" << ux(i) <<std::endl;
  
  out.close();

 Array1  uy(array.size(1)*nPoParCel);
  
  for(size_t iy=0;iy<array.size(1);iy++)
    for(int jy=0;jy<nPoParCel;jy++)
      uy(iy*nPoParCel+jy) = value(0, iy,0, 0., -1 + jy*2./(nPoParCel), 0., array)[var];

   double ly = dy * array.size(1) * mpiBase.get_dimSize()[1];
   double startvaly = 0.0 +  mpiBase.procCoord()[1] * (ly/ mpiBase.get_dimSize()[1]);
  
  out.open(BK::toString(filename,"-",var,"-y.data"));
  for(size_t j=0;j<array.size(1)*nPoParCel;j++)
    out << startvaly + j*dy/nPoParCel << "\t" << uy(j) <<std::endl;
  
  out.close();


    Array1 uz(array.size(2)*nPoParCel);
  
  for(size_t iz=0;iz<array.size(2);iz++)
    for(int jz=0;jz<nPoParCel;jz++)
      uz(iz*nPoParCel+jz) = value(0, 0, iz, 0., 0., -1 + jz*2./(nPoParCel), array)[var];

  double lz = dz * array.size(2) * mpiBase.get_dimSize()[2];
  double startvalz = 0.0 +  mpiBase.procCoord()[2] * (lz/ mpiBase.get_dimSize()[2]);
  
  out.open(BK::toString(filename,"-",var,"-z.data"));
  for(size_t k=0;k<array.size(2)*nPoParCel;k++)
    out << startvalz + k*dz/nPoParCel << "\t" << uz(k) <<std::endl;
  
  out.close();

  typedef BK::MultiArray<2,double> Matrix;
    Matrix uxy(array.size(0)*nPoParCel, array.size(1)*nPoParCel);
  
for(size_t ix=0;ix<array.size(0);ix++)
    for(int jx=0;jx<nPoParCel;jx++)
      for(size_t iy=0;iy<array.size(1);iy++)
	for(int jy=0;jy<nPoParCel;jy++)
	  uxy(ix*nPoParCel + jx,iy*nPoParCel+jy) = value(ix, iy, 0, -1 + jx*2./(nPoParCel), -1 + jy*2./(nPoParCel), 0., array)[var];
  
  out.open(BK::toString(filename,"-",var,"-xy.data"));
    for(size_t j=0;j<array.size(1)*nPoParCel;j++){
      for(size_t i=0;i<array.size(0)*nPoParCel;i++){
  	out<<"\t"<<uxy(i,j);
      }
      out<<std::endl;
    }
    
  out.close();

 typedef BK::MultiArray<2,double> Matrix;
    Matrix uxz(array.size(0)*nPoParCel, array.size(2)*nPoParCel);
  
  for(size_t ix=0;ix<array.size(0);ix++)
    for(int jx=0;jx<nPoParCel;jx++)
      for(size_t iz=0;iz<array.size(2);iz++)
	for(int jz=0;jz<nPoParCel;jz++)
	  uxz(ix*nPoParCel + jx,iz*nPoParCel + jz) = value(ix, 0, iz, -1 + jx*2./(nPoParCel),0., -1 + jz*2./(nPoParCel), array)[var];
  
  out.open(BK::toString(filename,"-",var,"-xz.data"));
    for(size_t k=0;k<array.size(2)*nPoParCel;k++){
      for(size_t i=0;i<array.size(0)*nPoParCel;i++){
  	out<<"\t" <<uxz(i,k);
      }
      out<<std::endl;
    }
    
  out.close();

   typedef BK::MultiArray<2,double> Matrix;
    Matrix uyz(array.size(1)*nPoParCel, array.size(2)*nPoParCel);

  for(size_t iy=0;iy<array.size(1);iy++)
    for(int jy=0;jy<nPoParCel;jy++)
      for(size_t iz=0;iz<array.size(2);iz++)
	for(int jz=0;jz<nPoParCel;jz++)
	  uyz(iy*nPoParCel + jy,iz*nPoParCel+jz) = value(0, iy, iz,0., -1 + jy*2./(nPoParCel), -1 + jz*2./(nPoParCel), array)[var];
  
  out.open(BK::toString(filename,"-",var,"-yz.data"));
    for(size_t k=0;k<array.size(2)*nPoParCel;k++){
      for(size_t j=0;j<array.size(1)*nPoParCel;j++){
  	out<<"\t"<<uyz(j,k);
      }
      out<<std::endl;
    }
    
  out.close();
 }
}

template<class Array>
void writeBin(const Array& array, std::string filename, int n, double dx, double dy, double dz)
{
  size_t varDim = (*array.begin()).size();

  BK::MultiArray<3,double> data(n,n,n);
  // double dx = problem.get_lxInterior()/(n-1);
  // double dy = problem.get_lyInterior()/(n-1);
  // double dz = problem.get_lzInterior()/(n-1);
  for(int i=0; i<n; i++) 
    for(int j=0; j<n; j++)
     for(int k=0; k<n; k++) {
       data(i,j,k) = i*dx;
    }

  std::ofstream outFile;
  outFile.open(filename + "_x.bin", std::ios::out | std::ios::binary);
  outFile.write( (char*)data.data(), sizeof(double)*data.size());
  outFile.close();

  for(int i=0; i<n; i++) 
    for(int j=0; j<n; j++)
      for(int k=0; k<n; k++){
	data(i,j,k) = j*dy;
    }

  outFile.open(filename + "_y.bin", std::ios::out | std::ios::binary);
  outFile.write( (char*)data.data(), sizeof(double)*data.size());
  outFile.close();

  for(int i=0; i<n; i++) 
    for(int j=0; j<n; j++)
      for(int k=0; k<n; k++){
	data(i,j,k) = k*dz;
    }

  outFile.open(filename + "_z.bin", std::ios::out | std::ios::binary);
  outFile.write( (char*)data.data(), sizeof(double)*data.size());
  outFile.close();

  
  for(size_t var = 0; var < varDim; var++) {
    for(int i=0; i<n;i++) 
      for(int j=0; j<n;j++)
         for(int k=0; k<n;k++){
	   data(i,j,k) = value(i*dx, j*dy, k*dz, array)[var]; 
      }
    
    std::ofstream outFile;
    outFile.open(filename + "-" + std::to_string(var) + ".bin", std::ios::out | std::ios::binary);
    outFile.write( (char*)data.data(), sizeof(double)*data.size());
    outFile.close();
  }
}

#endif
