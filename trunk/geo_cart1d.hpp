#ifndef geo_cart1d_hpp
#define geo_cart1d_hpp

#include "dg1d.hpp"
#include "rk_1d.hpp"
#include "problem1d.hpp"
#include "boundaryCondition1d.hpp"
#include "order.hpp"

Problem& problem = BK::StackSingleton<Problem>::instance();

class GeoCart1d
{

public:

  GeoCart1d(size_t ncelX) : ncelX_(ncelX) {}

  void init() {
    std::fill(c.begin(), c.end(), 0);

    // using std::placeholders::_1;
    // std::function<Problem::Vector(double)> func = std::bind(&Problem_BurgersDensity1d::initialCondition, problem, _1);
    // problem.initialize(c, func);
    using std::placeholders::_1;
    std::function<Problem::Vector(double)> initialFunction = std::bind( &Problem::initialCondition, problem, _1);

    problem.initialize(c, initialFunction);
  }

  void solve() {
    Problem::Array cnew(c);
    
    Rhs1d rhs1d(ncelX_, problem.get_dx());
    PeriodicBoundaryCondition1dFlux periodicBoundaryCondition1dFlux;

    double dt = problem.get_dt();
    problem.set_time() = 0;

    //    std::string dirName = BK::toString("data/",problem.get_name());
    
    for(int t=0;t<problem.get_nt();t++){
      std::cout << "t = " << t << "\t time = " << problem.get_time() << std::endl;
      
      // if(t%1 == 0)
      // 	writeFine(c, BK::toString(dirName,"/sol--",t), 10, problem.get_dx());

      // 	writeFine(c, dirName + "/sol_" +  std::to_string(order) + "-" + std::to_string(rkOrder) +"_" + std::to_string(ncelx)+"_"+std::to_string(ncely)+"-"+std::to_string(t), nPoParCel);
      
      rk(cnew, c, dt, rhs1d, periodicBoundaryCondition1dFlux, problem);
      
      c=cnew;
      
      problem.set_time() += dt;
      ++problem.set_step();
      notifyField.exec("field", &c);
    }
  }

  Problem::Array const& get_c() {
    return c;
  }

private:
  CONSTPARA(size_t, ncelX);

  Problem::Array c{ncelX_, order};
};

#endif
