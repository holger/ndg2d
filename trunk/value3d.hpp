#ifndef value3d_hpp
#define value3d_hpp

#include <type_traits>

#include "order.hpp"
#include "lagrange.hpp"
//#include "problem3d.hpp"

// 3D versions ---------------------------------------------------------------------

template<class Array>
inline auto value(int cellX, int cellY, int cellZ,  double x, double y, double z, Array const& c) -> typename std::remove_const<typename std::remove_reference<decltype(*c.begin())>::type>::type
{
  int orderX=c.size(3);
  int orderY=c.size(4);
  int orderZ=c.size(5);
  
  typename std::remove_const<typename std::remove_reference<decltype(*c.begin())>::type>::type u;
  u = 0.;
  for(int oz=0; oz<orderZ; oz++)
  for(int oy=0; oy<orderY; oy++)
    for(int ox=0; ox<orderX; ox++)
      u += c(cellX, cellY,cellZ, ox, oy,oz)*lagrange(ox, order, x)*lagrange(oy, order, y)*lagrange(oz, order, z);
    
   return u;
}

template<class Array>
inline auto valueR(int cellX, int cellY, int cellZ,  int indexY, int indexZ, Array const& c) -> typename std::remove_const<typename std::remove_reference<decltype(*c.begin())>::type>::type
{
  return c(cellX, cellY, cellZ, order-1, indexY, indexZ);
}

template<class Array>
inline auto valueL(int cellX, int cellY, int cellZ,  int indexY, int indexZ, Array const& c) -> typename std::remove_const<typename std::remove_reference<decltype(*c.begin())>::type>::type
{
  return c(cellX, cellY, cellZ, 0, indexY, indexZ);
}

template<class Array>
inline auto valueT(int cellX, int cellY, int cellZ,  int indexX, int indexZ,  Array const& c) -> typename std::remove_const<typename std::remove_reference<decltype(*c.begin())>::type>::type
{
  return c(cellX, cellY,cellZ, indexX, order-1, indexZ);
}

template<class Array>
inline auto valueB(int cellX, int cellY, int cellZ,  int indexX, int indexZ,  Array const& c) -> typename std::remove_const<typename std::remove_reference<decltype(*c.begin())>::type>::type
{
  return c(cellX, cellY, cellZ, indexX, 0, indexZ);
}

template<class Array>
inline auto valueBa(int cellX, int cellY, int cellZ,  int indexX, int indexY,  Array const& c) -> typename std::remove_const<typename std::remove_reference<decltype(*c.begin())>::type>::type
{
  return c(cellX, cellY,cellZ, indexX, indexY, 0);
}

template<class Array>
inline auto valueF(int cellX, int cellY, int cellZ,  int indexX, int indexY,  Array const& c) -> typename std::remove_const<typename std::remove_reference<decltype(*c.begin())>::type>::type
{
  return c(cellX, cellY, cellZ, indexX, indexY, order-1);
}

template<class Array>
inline auto value(double x, double y, double z, Array const& c, double dx, double dy, double dz) -> typename std::remove_const<typename std::remove_reference<decltype(*c.begin())>::type>::type
{
  size_t orderX=c.size(3);
  size_t orderY=c.size(4);
  size_t orderZ=c.size(5);

  // double dx = problem.get_dx();
  // double dy = problem.get_dy();
  // double dz = problem.get_dz();
  size_t cellX = size_t(x/dx);
  size_t cellY = size_t(y/dy);
  size_t cellZ = size_t(z/dz);
  double xInCell = 2*(x-dx*cellX)/dx-1;
  double yInCell = 2*(y-dy*cellY)/dy-1;
  double zInCell = 2*(z-dz*cellZ)/dz-1;

  typename std::remove_const<typename std::remove_reference<decltype(*c.begin())>::type>::type u;
  u = 0.;
  for(size_t ox=0; ox<orderX; ox++)
    for(size_t oy=0; oy<orderY; oy++)
      for(size_t oz=0; oz<orderZ; oz++)
	u += c(cellX, cellY,cellZ, ox, oy, oz)*lagrange(ox, orderX, xInCell)*lagrange(oy, orderY, yInCell)*lagrange(oz, orderZ, zInCell);
    
  return u;
}										     
#endif
