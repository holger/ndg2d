#ifndef writeArray1d_hpp
#define writeArray1d_hpp

#include <iostream>
#include <fstream>
#include <string>

#include <BasisKlassen/multiArray.hpp>

#include "value1d.hpp"

// template<class Array>
// void writeArray(const Array& array)
// {
//   for(int l=0; l<array.sizeOrderY();l++){
//     for(int k=0; k<array.sizeOrderX();k++){
//       for(int i=0; i<array.size(0);i++){
// 	for(int j=0;j<array.size(1);j++){
// 	  std::cout<<array(i,j,k,l)<<"\t";
// 	}std::cout<<std::endl;
//       }std::cout<<std::endl;
//     }std::cout<<std::endl;
//   }
// }

template<class Array>
void writeArray(const Array& array, std::string filename)
{
  int varDim = array.begin()->size();
  
  for(size_t var = 0; var < varDim; var++) {

    std::ofstream out(BK::toString(filename,"-",var,".data"));
    
    for(size_t ox=0;ox<array.size(1);ox++){
      for(size_t i=0;i<array.size(0);i++){
	out << "\t" << array(i,ox)[var];
      }
      out<<std::endl;
    }
    out.close();
  }
}
  
template<class Array>
void writeFine(const Array& array, std::string filename, int nPoParCel, double dx)
{
  size_t varDim = (*array.begin()).size();

  for(size_t var = 0; var < varDim; var++) {
    
    typedef BK::MultiArray<1,double> Matrix;
    Matrix u(array.size(0)*nPoParCel);
  
    for(size_t ix=0;ix<array.size(0);ix++)
      for(int jx=0;jx<nPoParCel;jx++)
	u(ix*nPoParCel + jx) = value(ix, -1 + jx*2./(nPoParCel-1), array)[var];
  
    std::ofstream out(BK::toString(filename,"-",var,".data"));
    
    // for(size_t i=0;i<array.size(0)*nPoParCel;i++){
    //   out << i*dx/(nPoParCel-1) <<  "\t"<<u(i);
    //   out<<std::endl;
    // }
    for(size_t ix=0;ix<array.size(0);ix++)
      for(int jx=0;jx<nPoParCel;jx++) {
	out << ix*dx + jx*dx/(nPoParCel-1) <<  "\t"<< u(ix*nPoParCel + jx);
	out<<std::endl;
    }
    out.close();
  }
}

template<class Array>
void writeBin(const Array& array, std::string filename, int n, double dx)
{
  size_t varDim = (*array.begin()).size();

  std::vector<double> data(n);
  //  double dx = problem.get_lxInterior()/(n-1);

  size_t number = 0;
  for(int i=0; i<n;i++) {
    data[number] = i*dx;
    ++number;
  }

  std::ofstream outFile;
  outFile.open(filename + "_x.bin", std::ios::out | std::ios::binary);
  outFile.write( (char*)data.data(), sizeof(double)*data.size());
  outFile.close();
  
  for(size_t var = 0; var < varDim; var++) {
    size_t number = 0;
    for(int i=0; i<n;i++) {
      data[number] = value(i*dx, array)[var]; 
      ++number;
    }
    std::ofstream outFile;
    outFile.open(filename + "-" + std::to_string(var) + ".bin", std::ios::out | std::ios::binary);
    outFile.write( (char*)data.data(), sizeof(double)*data.size());
    outFile.close();
  }
}

#endif
