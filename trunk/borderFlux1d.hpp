#ifndef borderFlux1d_hpp
#define borderFlux1d_hpp

#include "order.hpp"
#include "laxFriedrichs1d.hpp"
//#include "centralFlux1d.hpp"
#include "problem1d.hpp"

// 1D Flux -----------------------------------------------------------------------------------------------------

template<class Array>
auto borderFlux(int cell, int index, Array const& c)
{
  typedef typename Array::value_type Vector;
  
  if(index == order-1) {
    return Vector{problem.flux_numeric_R(cell, c)};
  }

  else if(index == 0)
    return Vector{-problem.flux_numeric_L(cell, c)};
  
  else return Vector(0);
}

#endif
