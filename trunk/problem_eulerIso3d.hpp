#ifndef problem_eulerIso3d_hpp
#define problem_eulerIso3d_hpp

#include <BasisKlassen/parameter.hpp>
#include <BasisKlassen/singleton.hpp>
#include <BasisKlassen/assert.hpp>
#include <BasisKlassen/parseFromFile.hpp>
#include <BasisKlassen/vector.hpp>
#include <BasisKlassen/assert.hpp>

const int varDim = 4;     // four variables: rho, rho*ux, rho*uy, rho*uz
const int spaceDim = 3;   // 3d space

#include "types.hpp"
#include "cfl.hpp"
#include "notiModify.hpp"
#include "problemBase3d.hpp"
#include "laxFriedrichs1d.hpp"
#include "order.hpp"
#include "gaussLobatto.hpp"

// 3D Burgers equations with density
class Problem_EulerIso3d : public ProblemBase3d<varDim>
{
public:

  struct Var {
    static const int rho = 0;  // rho
    static const int mx = 1;  // rho*ux
    static const int my = 2;  // rho*uy
    static const int mz = 3;  // rho*uz
  };
  
  friend class BK::StackSingleton<Problem_EulerIso3d>;

  Problem_EulerIso3d() {}
  
  void init(int ncelx, int ncely, int ncelz, std::string parameterFilename) {
    //    notifyField.add("fieldFinal", FieldNotifyCallBack(std::bind(&Problem_EulerIso3d::errorComp, this, std::placeholders::_1), "Problem_EulerIso3d::errorComp"));    

    name_ = "eulerIso3d";
    ProblemBase3d::init(ncelx, ncely, ncelz, parameterFilename);
    
    BK::ReadInitFile initFile(parameterFilename,true);
    double nt = nt_;
    a_ = initFile.getParameter<double>("a");
	
    if(nt > 0 && T_ > 0) {
      std::cerr << "ERROR in Problem_burgersDensity2d::init(): nt = " <<
	nt << " > 0 && T = " << T_ << " > 0." << std::endl;
      exit(1);						   
    }

    ASSERT((dx_ == dy_) && (dy_ == dz_) , BK::toString("dx_ = ", dx_, "; dy_ = ", dy_,"; dz_ = ", dz_));
    
    dt_ = std::min(fabs(cfl_*dx_),fabs(cfl_*dx_/a_));

    if(nt > 0) {
      nt_ = nt;
      T_ = nt_*dt_;
    }
    else
      nt_ = int(T_/dt_) + 1;

    dt_ *= T_/(nt_*dt_);

    logParameter();

    initialized_ = true;
  }

  void errorComp(Array const* c);

  Vector initialCondition(double x, double y, double z) {
    return Vector{1., sin(2*M_PI*x/lx_), 0, 0};
  }

  void getReference(Array & c) {}

  void lhs(Array& cNew, Array const& c, Array const& rhs) const {
    cNew = c + dt_*rhs;
  }    

  VecVec flux_vector(Vector const& varArray) const {
    double rho = varArray[0];
    double rhoUx = varArray[1];
    double rhoUy = varArray[2];
    double rhoUz = varArray[3];
    
    double ux = rhoUx/rho;
    double uy = rhoUy/rho;
    double uz = rhoUz/rho;
    
    return {{rhoUx, rhoUx*ux + rho*a_*a_, rhoUx*uy, rhoUx*uz},
	    {rhoUy, rhoUy*ux, rhoUy*uy + rho*a_*a_, rhoUy*uz},
	    {rhoUz, rhoUz*ux, rhoUz*uy, rhoUz*uz + rho*a_*a_}};
  }

  BK::Array<double,3> dFlux_value(Vector const& varArray) const {
    double rho = varArray[Var::rho];
    double rhoUx = varArray[Var::mx];
    double rhoUy = varArray[Var::my];
    double rhoUz = varArray[Var::mz];
    
    double ux = rhoUx/rho;
    double uy = rhoUy/rho;
    double uz = rhoUz/rho;

    return {std::max({fabs(ux), fabs(ux + a_), fabs(ux - a_)}), 
	    std::max({fabs(uy), fabs(uy + a_), fabs(uy - a_)}),
	    std::max({fabs(uz), fabs(uz + a_), fabs(uz - a_)})};
  }

  void computeNumericalFlux(FluxVecArray& fluxVecArrayX, FluxVecArray& fluxVecArrayY, FluxVecArray& fluxVecArrayZ,
			    FluxInfoBordArray& fluxInfoArrayX, FluxInfoBordArray& fluxInfoArrayY, FluxInfoBordArray& fluxInfoArrayZ) {
    auto fluxFunc = [this](Vector const& varArray) { return this->flux_vector(varArray);};
    auto dfluxFunc = [this](Vector const& varArray) { return this->dFlux_value(varArray);};
    
    computeNumericalFlux_LF(fluxVecArrayX, fluxVecArrayY, fluxVecArrayZ, fluxInfoArrayX, fluxInfoArrayY, fluxInfoArrayZ, fluxFunc, dfluxFunc);
  }
  
  CONSTPARA(double, a);
};

typedef Problem_EulerIso3d Problem;
extern Problem& problem;

#endif
