#ifndef writeArray2d_netcdf_hpp
#define writeArray2d_netcdf_hpp

#include "logger.hpp"

// #undef LOGLEVEL
// #define LOGLEVEL 3

inline void check(bool result) {
  assert(!result);
}

template<class Array>
void writeNetCDF(const Array& array, std::string filename, size_t nxGlobal, size_t nyGlobal, double dx, double dy)
{
  LOGL(3,"void writeNetCDF(const Array& array, std::string filename, size_t nxGlobal, size_t nyGlobal, double dx, double dy)");
  
  const int varDim = Array::value_type::N_length;

  std::cout << mpiBase.get_commRank() << ": varDim = " << varDim << std::endl;
  
  BK::Array<int, 4> dims = array.dim();
  size_t nx = dims[0]*dims[2];
  size_t ny = dims[1]*dims[3];

  std::cout << mpiBase.get_commRank() << ": nx = " << nx << "; ny = " << ny << std::endl;
  
  size_t startXcell = mpiBase.procCoord()[0]*dims[0];
  size_t startYcell = mpiBase.procCoord()[1]*dims[1];

  std::cout << mpiBase.get_commRank() << ": startXcell = " << startXcell << "; startYcell = " << startYcell << std::endl;

  filename += ".nc";
  
  MPI_Info info = MPI_INFO_NULL;
  int ncid;

  check(nc_create_par(filename.c_str(), NC_NETCDF4|NC_MPIIO, MPI_COMM_WORLD, 
		      info, &ncid));
  //  res = nc_create(filename.c_str(), NC_NETCDF4, &ncid);
  
  int dimids[2];
  check(nc_def_dim(ncid, "d1", nxGlobal*order, &dimids[0]));
  check(nc_def_dim(ncid, "d2", nyGlobal*order, &dimids[1]));

  //  BK::Array<int, Array::value_type::N_length*2+2> varVec;
  BK::Array<int, varDim*4+2> varVec;
  std::cout << "varVec.size() = " << varVec.size() << std::endl;

  check(nc_def_var(ncid, "x", NC_DOUBLE, 2, dimids, &varVec[0]));
  check(nc_def_var(ncid, "y", NC_DOUBLE, 2, dimids, &varVec[1]));

  std::cout << "varDim = " << varDim << std::endl;
  for(int i=0; i<varDim; i++) {
    check(nc_def_var(ncid, BK::toString("varDump-",i).c_str(), NC_DOUBLE, 2, dimids, &varVec[i+2]));
    check(nc_var_par_access(ncid, varVec[i+2], NC_COLLECTIVE));
  }
  
  for(int i=0; i<varDim; i++) {
    check(nc_def_var(ncid, BK::toString("var-",i).c_str(), NC_DOUBLE, 2, dimids, &varVec[i+2+varDim]));
    check(nc_var_par_access(ncid, varVec[i+2+varDim], NC_COLLECTIVE));
  }
  
  check(nc_enddef(ncid));
		     
  check(nc_put_att_int(ncid, NC_GLOBAL, "ncelx", NC_INT, 1, &dims[0]));
  check(nc_put_att_int(ncid, NC_GLOBAL, "ncely", NC_INT, 1, &dims[1]));
  check(nc_put_att_int(ncid, NC_GLOBAL, "order", NC_INT, 1, &order));

  size_t startXid = mpiBase.procCoord()[0]*nx;
  size_t startYid = mpiBase.procCoord()[1]*ny;

  size_t start[2];
  size_t count[2];
  start[0] = startXid;
  start[1] = startYid;
  count[0] = nx;
  count[1] = ny;

  BK::MultiArray<2,double> data(nx,ny);

  BK::Vector<double> xi = points[order-1];
  
  for(size_t ix=0; ix<array.dim()[0]; ix++)
    for(size_t iy=0; iy<array.dim()[1]; iy++) 
      for(size_t ox=0; ox<order; ox++)
	for(size_t oy=0; oy<order; oy++) 
	  data(ix*order + ox, iy*order + oy) = startXcell*dx + dx/2 + ix*dx + xi[ox]*dx/2;
  
  check(nc_put_vara_double(ncid, varVec[0], start, count, 
  			   data.data()));

  for(size_t ix=0; ix<array.dim()[0]; ix++)
    for(size_t iy=0; iy<array.dim()[1]; iy++) 
      for(size_t ox=0; ox<order; ox++)
	for(size_t oy=0; oy<order; oy++) 
	  data(ix*order + ox, iy*order + oy) = startYcell*dy + dy/2 + iy*dy + xi[oy]*dy/2;
  
  check(nc_put_vara_double(ncid, varVec[1], start, count, 
   			   data.data()));

  for(int var=0; var<varDim; var++) {
    for(size_t ix=0; ix<array.dim()[0]; ix++)
      for(size_t iy=0; iy<array.dim()[1]; iy++) 
  	for(size_t ox=0; ox<order; ox++)
  	  for(size_t oy=0; oy<order; oy++) 
  	    data(ix*order + ox, iy*order + oy) = array(ix, iy, ox, oy)[var];

    check(nc_put_vara_double(ncid, varVec[2+var], start, count, 
     			     data.data()));
  }

  for(int var=0; var<varDim; var++) {
    for(int var = 0; var < varDim; var++) {
      for(size_t ix=0; ix<array.dim()[0]; ix++)
  	for(size_t iy=0; iy<array.dim()[1]; iy++) 
  	  for(size_t jx=0; jx<order; jx++)
  	    for(size_t jy=0; jy<order; jy++) 
  	      data(ix*order + jx, iy*order + jy) = value(ix, iy, -1 + jx*2./order, -1 + jy*2./order, array)[var];
      
      check(nc_put_vara_double(ncid, varVec[2+varDim+var], start, count, 
       			       data.data()));
    }
  }
  
  check(nc_close(ncid));

  LOGL(4,"void writeNetCDF(const Array& array, std::string filename, size_t nxGlobal, size_t nyGlobal, double dx, double dy)-end");

  // std::ofstream outFile;
  // outFile.open(filename + "_x.bin", std::ios::out | std::ios::binary);
  // outFile.write( (char*)data.data(), sizeof(double)*data.size());
  // outFile.close();

  // for(int i=0; i<n; i++) 
  //   for(int j=0; j<n; j++) {
  //     data(i,j) = j*dy;
  //   }

  // outFile.open(filename + "_y.bin", std::ios::out | std::ios::binary);
  // outFile.write( (char*)data.data(), sizeof(double)*data.size());
  // outFile.close();

  
  // for(size_t var = 0; var < varDim; var++) {
  //   for(int i=0; i<n;i++) 
  //     for(int j=0; j<n;j++) {
  // 	data(i,j) = value(i*dx, j*dy, array, dx, dy)[var]; 
  //     }
    
  //   std::ofstream outFile;
  //   outFile.open(filename + "-" + std::to_string(var) + ".bin", std::ios::out | std::ios::binary);
  //   outFile.write( (char*)data.data(), sizeof(double)*data.size());
  //   outFile.close();
  // }
}

inline void writeNetCDF_2D(BK::MultiArray<2, BK::Vector<double>> const& array, BK::Vector<std::string> varNames, std::string filename, size_t nxGlobal, double dx, double dy, std::string xName, std::string yName, double startX, double startY)
{
  LOGL(3,"void writeNetCDF_2D(BK::MultiArray<2, BK::Vector<double>> const& array, BK::Vector<std::string> varNames, std::string filename, size_t nxGlobal, double dx, double dy, std::string xName, std::string yName, double startX, double startY)");
       
  int varDim = varNames.size();

  std::cout << mpiBase.get_commRank() << ": varDim = " << varDim << std::endl;
  std::cout << mpiBase.get_commRank() << ": varNames = " << varNames << std::endl;
  
  BK::Array<int, 2> dims = array.dim();
  std::cerr << "dims = " << dims << std::endl;
  size_t nx = dims[0];
  size_t ny = dims[1];

  std::cout << mpiBase.get_commRank() << ": nx = " << nx << "; ny = " << ny << std::endl;
  
  size_t startXcell = mpiBase.procCoord()[0]*dims[0];
  size_t startYcell = mpiBase.procCoord()[1]*dims[1];

  std::cout << mpiBase.get_commRank() << ": startXcell = " << startXcell << "; startYcell = " << startYcell << std::endl;
  std::cout << mpiBase.get_commRank() << ": startX = " << startX << "; startY = " << startY << std::endl;

  filename += ".nc";
  
  MPI_Info info = MPI_INFO_NULL;
  int ncid;

  check(nc_create_par(filename.c_str(), NC_NETCDF4|NC_MPIIO, MPI_COMM_WORLD, 
		      info, &ncid));

  nxGlobal = nx;
  int dimids[2];
  check(nc_def_dim(ncid, xName.c_str(), nxGlobal, &dimids[0]));
  check(nc_def_dim(ncid, yName.c_str(), nxGlobal, &dimids[1]));

  BK::Vector<int> varVec(varDim+2);
  std::cout << "varVec.size() = " << varVec.size() << std::endl;

  std::cout << "varDim = " << varDim << std::endl;
  for(int i=0; i<varDim; i++) {
    check(nc_def_var(ncid, varNames[i].c_str(), NC_DOUBLE, 2, dimids, &varVec[i]));
    check(nc_var_par_access(ncid, varVec[i], NC_COLLECTIVE));
  }

  check(nc_def_var(ncid, xName.c_str(), NC_DOUBLE, 1, &dimids[0], &varVec[varDim]));
  check(nc_var_par_access(ncid, varVec[varDim], NC_COLLECTIVE));
  check(nc_def_var(ncid, yName.c_str(), NC_DOUBLE, 1, &dimids[1], &varVec[varDim+1]));
  check(nc_var_par_access(ncid, varVec[varDim+1], NC_COLLECTIVE));

  if(xName == "latitude" && yName == "longitude") {
    nc_put_att_text(ncid, varVec[varDim], "units",
		    13, "degrees_north");
    
    nc_put_att_text(ncid, varVec[varDim+1], "units",
		    12, "degrees_east");
  }
  
  check(nc_enddef(ncid));
    
  check(nc_put_att_int(ncid, NC_GLOBAL, "ncelx", NC_INT, 1, &dims[0]));
  check(nc_put_att_int(ncid, NC_GLOBAL, "ncely", NC_INT, 1, &dims[1]));
  check(nc_put_att_int(ncid, NC_GLOBAL, "order", NC_INT, 1, &order));

  size_t startXid = mpiBase.procCoord()[0]*nx;
  size_t startYid = mpiBase.procCoord()[1]*ny;

  size_t start[2];
  size_t count[2];
  start[0] = startXid;
  start[1] = startYid;
  count[0] = nx;
  count[1] = ny;

  BK::MultiArray<2,double> data(nx,ny);

  for(int i=0; i<varDim; i++) {
    for(size_t ix=0; ix<nx; ix++)
      for(size_t iy=0; iy<ny; iy++) 
  	data(ix, iy) = array(ix, iy)[i];
    
    check(nc_put_vara_double(ncid, varVec[i], start, count, 
  			     data.data()));
  }

  BK::Vector<double> dataCoord(nx);
  for(size_t ix=0; ix<nx; ix++) {
    dataCoord[ix] = startX + startXcell*dx + ix*dx;
    //    std::cout << ix << "\t" << dataCoord[ix] << std::endl;
  }

  check(nc_put_vara_double(ncid, varVec[varDim], start, &nx, 
  			   dataCoord.data()));

  for(size_t ix=0; ix<nx; ix++)
    dataCoord[ix] = startY + startYcell*dy + ix*dy;// - 3./4*M_PI;

  
  check(nc_put_vara_double(ncid, varVec[varDim+1], start, &nx, 
  			   dataCoord.data()));
  
  check(nc_close(ncid));

  LOGL(4,"void writeNetCDF_2D(BK::MultiArray<2, BK::Vector<double>> const& array, BK::Vector<std::string> varNames, std::string filename, size_t nxGlobal, double dx, double dy, std::string xName, std::string yName, double startX, double startY)-end");
}


#endif
