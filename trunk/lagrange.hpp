#ifndef lagrange_hpp
#define lagrange_hpp

#include <BasisKlassen/vector.hpp>

//#include "legendre.hpp"
#include "gaussLobatto.hpp"
#include "order.hpp"
#include "logger.hpp"

// #undef LOGLEVEL
// #define LOGLEVEL 4

inline double lagrange_imp(int k, int N, double x)
{
  static BK::Vector<double> xi = points[N-1];

  double value;
  int nextI = 1;
  if(k!=0)
    value = (x - xi[0])/(xi[k] - xi[0]);
  else {
    value = (x - xi[1])/(xi[k] - xi[1]);
    nextI = 2;
  }
  
  for(int i = nextI; i<N; i++) {
    if(i == k) continue;
    value *= (x - xi[i])/(xi[k] - xi[i]);
  }
  
  return value;
  
  // if(x == xi[k])
  //   return (x-1)*Legendre::DPn(N,x)/(N*(N+1)*Legendre::Pn(N,xi[k]));
  // else
  //   return (x-1)*(x+1)*Legendre::DPn(N,x)/(N*(N+1)*Legendre::Pn(N,xi[k])*(x-xi[k]));
}

inline double lagrange(int k, int N, double x)
{
  assert(k < N);
  
  if(N == 1)
    return 1;
  else
    return lagrange_imp(k,N,x);
}

inline double dLagrange_imp(int k, int N, double x)
{
  assert(k < N);
  
  static BK::Vector<double> xi = points[N-1];

  double value = 0;
  for(int term = 0; term < N; term++) {
    if(term == k) continue;
    double valueTerm = 1./(xi[k] - xi[term]);
    
    for(int i = 0; i<N; i++) {
      if(i == k) continue;
      if(i == term) continue;
      valueTerm *= (x - xi[i])/(xi[k] - xi[i]);
    }
    value += valueTerm;
  }

  return value;
}

inline double dLagrange(int k, int N, double x)
{
  assert(k < N);
  
  if(N == 1)
    return 0;
  else
    return dLagrange_imp(k,N,x);
}

template<int order>
class LagrangeMatrix
{
public:
  LagrangeMatrix() {
    initialized_ = false;
  };

  void init() {
    static BK::Vector<double> xi = points[order-1];
    
    for(size_t k=0;k<lagrangeMatrix_.size();k++)
      for(size_t point=0;point<lagrangeMatrix_[k].size(); point++) {
	lagrangeMatrix_[k][point] = lagrange(k,order, xi[point]);
      }

    initialized_ = true;
  }    

  double operator()(int k, int point) const {
    assert(initialized_);
    return lagrangeMatrix_[k][point];
  }

private:

  BK::Array<BK::Array<double,order>,order> lagrangeMatrix_;
  CONSTPARA(bool, initialized);
};

template<int order>
class DLagrangeMatrix
{
public:
  DLagrangeMatrix() {
    initialized_ = false;
  }

  void init() {

    std::cerr << "t\n";
    LOGL(3,"DLagrangeMatrix::DLagrangeMatrix()");
    std::cerr << "order = " << order << std::endl;
    static BK::Vector<double> xi = points[order-1];
    std::cerr << xi.size() << std::endl;
    std::cerr << points << std::endl;
    
    for(size_t k=0;k<dLagrangeMatrix_.size();k++)
      for(size_t point=0;point<dLagrangeMatrix_[k].size(); point++) {
	dLagrangeMatrix_[k][point] = dLagrange(k,order, xi[point]);
	// if(k != point)
	//   dLagrangeMatrix_[k][point] = Legendre::Pn(order-1, xi[k])/(Legendre::Pn(order-1, xi[point])*(xi[k]-xi[point]));
	// else if((k==0) && (point==0))
	//   dLagrangeMatrix_[k][point] = -order*(order-1)/4.;
	// else if((k==order-1) && (point==order-1))
	//   dLagrangeMatrix_[k][point] = order*(order-1)/4.;
	// else
	//   dLagrangeMatrix_[k][point] = 0.;
    LOGL(4,"DLagrangeMatrix::DLagrangeMatrix()-end");
      }
    initialized_ = true;
  }

  double operator()(int k, int point) const {
    assert(initialized_);
    return dLagrangeMatrix_[k][point];
  }

private:

  BK::Array<BK::Array<double,order>,order> dLagrangeMatrix_;
  CONSTPARA(bool, initialized);
};

#endif
