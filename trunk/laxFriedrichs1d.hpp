#ifndef laxFriedrichs1d_hpp
#define laxFriedrichs1d_hpp

#include <cmath>

inline double laxFriedrichs1d_flux_numeric(double fluxL, double fluxR, double dFluxL, double dFluxR, double valueL, double valueR)
{
  double fr = 0.5*(fluxL + fluxR);

  double cr = std::max(fabs(dFluxL), fabs(dFluxR));
  
  return fr - 0.5*cr*(valueR - valueL);
}

template<class Vector>
inline Vector laxFriedrichs1d_flux_numeric(Vector const& fluxM, Vector const& fluxP, double dFluxM, double dFluxP, Vector const& valueM, Vector const& valueP)
{
  Vector f = 0.5*(fluxM + fluxP);

  double c = std::max(fabs(dFluxM), fabs(dFluxP));
  
  return f - 0.5*c*(valueP - valueM);
}

// template<class Vec>
// Vec laxFriedrichs1d_flux_numeric_L(Vec fluxL, Vec fluxR, double dFluxL, double dFluxR, Vec valueL, Vec valueR)
// {
//   Vec fl = 0.5*(fluxL + fluxR);
  
//   double cl = std::max(fabs(dFluxL), fabs(dFluxR));

//   Vec result = fl + 0.5*cl*(valueL - valueR);

//   return result;
// }

#endif
