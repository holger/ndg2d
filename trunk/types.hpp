#ifndef types_hpp
#define types_hpp

#include <BasisKlassen/multiArray.hpp>
#include <BasisKlassen/array.hpp>

struct Val
{
  static const int minus = 0;
  static const int plus = 1;
};

template<int spaceDim, int varDim_>
class DataTypes
{
public:
  static const int varDim = varDim_;
  
  typedef BK::MultiArray<spaceDim,double> Matrix;
  typedef BK::Array<double,varDim> Vector;
  typedef BK::Array<Vector,spaceDim> VecVec;
  typedef BK::MultiArray<spaceDim*2,Vector> Array;
  typedef BK::MultiArray<spaceDim*2,VecVec> FluxArray;
  
  typedef BK::Array<double, varDim> FluxInfo;                         // [val1, val2, ...] 
  typedef BK::Array<FluxInfo, 2> FluxInfoBord;                        // [+-][val, val2, ...]
  typedef BK::MultiArray<spaceDim+spaceDim-1, FluxInfoBord> FluxInfoBordArray; // (cell)[+-][val, val2, ...]
  
  // data type for flux storage
  typedef BK::MultiArray<spaceDim+spaceDim-1, Vector> FluxVecArray;
};
  
#endif
