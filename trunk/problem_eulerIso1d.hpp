#ifndef problem_eulerIso1d_hpp
#define problem_eulerIso1d_hpp

#include <BasisKlassen/parameter.hpp>
#include <BasisKlassen/singleton.hpp>
#include <BasisKlassen/assert.hpp>
#include <BasisKlassen/parseFromFile.hpp>

const int varDim = 2;
const int spaceDim = 1;

#include "value1d.hpp"
#include "cfl.hpp"
#include "laxFriedrichs1d.hpp"
#include "notiModify.hpp"
#include "problemBase1d.hpp"

// Isothermal 1D Euler gaz
class Problem_EulerIso1d : public ProblemBase1d<varDim>
{
public:

  friend class BK::StackSingleton<Problem_EulerIso1d>;

  Problem_EulerIso1d() {}

  void init(int ncel, std::string parameterFilename) {
    
    name_ = "eulerIso1d";
    
    ProblemBase1d::init(ncel, parameterFilename);

    BK::ReadInitFile initFile(parameterFilename,true);
    a_ = initFile.getParameter<double>("a");
    
    // computing dt
    dt_ = fabs(cfl_*dx_);
    if(nt_ > 0) {
      T_ = nt_*dt_;
    }
    else {
      nt_ = int(T_/dt_) + 1;
    }
    dt_ *= T_/(nt_*dt_);

    logParameter();
    
    std::cerr << "a = " << a_ << std::endl;

    initialized_ = true;
  }

  Vector initialCondition(double x) {
    assert(initialized_);

    //    double k=1;
    return initialConditionShift(x,0);

    // Riemann-problem test
    // double rho0;
    // if(x > lx_/2)
    //   rho0 = 0.5;
    // else rho0 = 1;
    // return Vector{rho0, 0};
  }

  Vector initialConditionShift(double x, double shift = 0) {
    assert(initialized_);
    // double x0 = 0.35;
    // double y0 = 0.35;

    // int kMax = 10;
    // double temp = 0;
    // for(int k=1; k<=kMax; k++)
    //   temp += pow(k,-5./6)*sin(2*M_PI*k/lambda_*(x-shift-2*M_PI/(k*k)));

    // return temp;

    double k=1;
    return Vector{1., sin(2*M_PI*k*(x-shift)/lx_)};
  }

  void lhs(Array& cNew, Array const& c, Array const& rhs) const {
    cNew = c + dt_*rhs;
  }    

  VecVec flux_vector(Vector cellVec, size_t cell = 0, size_t index = 0) {
    assert(initialized_);

    double rho = cellVec[0];
    double rhoU = cellVec[1];
    double u = rhoU/rho;

    return {{rhoU, rhoU*u + a_*a_*rho}};
    // double rho0 = 1;
    // return {{rho0*u, a_*a_*rho}};
  }

  double dFlux_value(Vector cellVec, size_t cell=0, size_t index=0) {
    assert(initialized_);
    
    double rho = cellVec[0];
    double rhoU = cellVec[1];
    double u = rhoU/rho;

    return std::max(fabs(u+a_), fabs(u-a_));
  }

  void compute_fluxArray(FluxVecArray& fluxArray, FluxInfoBordArray const& fluxInfoArray)
  {
    switch(numFluxType_) {
    case 0: {
      auto fluxFunc = [this](Vector const& varArray, size_t cell, size_t index) { return this->flux_vector(varArray, cell, index);};
      auto dfluxFunc = [this](Vector const& varArray, size_t cell, size_t index) { return this->dFlux_value(varArray, cell, index);};
      
      compute_fluxArray_LF(fluxArray, fluxInfoArray, fluxFunc, dfluxFunc);

      //      compute_fluxArray_LF(fluxArray, fluxInfoArray, c);
      break;
    }
      //   case 1: compute_fluxArray_Godunov(fluxArray, fluxInfoArray, c); break;
    default: std::cout << "ERROR in Burgers1d::compute_fluxArray\n"; exit(1);
    }
  }
  
  // void compute_fluxArray_Godunov(FluxVecArray& fluxArray, FluxInfoVecBordArray const& fluxInfoArray, Array const& c)
  // {
  //   for(size_t cellInterface=0; cellInterface<ncel_+1; cellInterface++) {

  //     double rhoM = fluxInfoArray(cellInterface)[Val::minus][FluxVar::val0];
  //     double mM = fluxInfoArray(cellInterface)[Val::minus][FluxVar::val1];
  //     double rhoP = fluxInfoArray(cellInterface)[Val::plus][FluxVar::val0];
  //     double mP = fluxInfoArray(cellInterface)[Val::plus][FluxVar::val1];

  //     double d = mP/rhoP - mM/rhoM;
  //     double c = a_/sqrt(rhoM) + a_/sqrt(rhoP);
  //     double e = -a_*(sqrt(rhoM)+sqrt(rhoP));
      
  //     double p = d/c;
  //     double q = e/c;

  //     double zStar = p/2 + sqrt((p/2)*(p/2)-q);
  //     double rhoStar = sqrt(zStar);

  //     double mStar = rhoStar*mM/rhoM - a_*sqrt(rhoStar/rhoM)*(rhoStar-rhoM);

  //     Vector state = {rhoStar, mStar};

  //     fluxArray(cellInterface) = flux_vector(state);
  //   }
  // }

  CONSTPARA(double,a);
};

typedef Problem_EulerIso1d Problem;
extern Problem& problem;

#endif
