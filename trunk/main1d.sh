#! /bin/bash

cp order.hpp_template order.hpp
sed -i 's/static constexpr int order = 3/static constexpr int order = 3/g' order.hpp

cp problem_advection1d.init_template problem_advection1d.init
sed -i 's/rkOrder		6/rkOrder		3/g' problem_advection1d.init

cd release
make advection1d

./advection1d ../problem_advection1d.init

cd ..

cp order.hpp_template order.hpp
sed -i 's/static constexpr int order = 3/static constexpr int order = 4/g' order.hpp

cp problem_advection1d.init_template problem_advection1d.init
sed -i 's/rkOrder		6/rkOrder		4/g' problem_advection1d.init

cd release
make advection1d

./advection1d ../problem_advection1d.init

cd ..

cp order.hpp_template order.hpp
sed -i 's/static constexpr int order = 3/static constexpr int order = 5/g' order.hpp

cp problem_advection1d.init_template problem_advection1d.init
sed -i 's/rkOrder		6/rkOrder		5/g' problem_advection1d.init

cd release
make advection1d

./advection1d ../problem_advection1d.init

cd ..

cp order.hpp_template order.hpp
sed -i 's/static constexpr int order = 3/static constexpr int order = 5/g' order.hpp

cp problem_advection1d.init_template problem_advection1d.init
sed -i 's/rkOrder		6/rkOrder		5/g' problem_advection1d.init

cd release
make advection1d

./advection1d ../problem_advection1d.init

cd ..

cp order.hpp_template order.hpp
sed -i 's/static constexpr int order = 3/static constexpr int order = 6/g' order.hpp

cp problem_advection1d.init_template problem_advection1d.init
sed -i 's/rkOrder		6/rkOrder		6/g' problem_advection1d.init

cd release
make advection1d

./advection1d ../problem_advection1d.init

cd ..

cp order.hpp_template order.hpp
sed -i 's/static constexpr int order = 3/static constexpr int order = 6/g' order.hpp

cp problem_advection1d.init_template problem_advection1d.init
sed -i 's/rkOrder		6/rkOrder		3/g' problem_advection1d.init

cd release
make advection1d

./advection1d ../problem_advection1d.init

cd ..
