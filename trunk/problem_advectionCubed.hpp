#ifndef problem_advectionCubed_hpp
#define problem_advectionCubed_hpp

#include <BasisKlassen/parameter.hpp>
#include <BasisKlassen/singleton.hpp>
#include <BasisKlassen/assert.hpp>
#include <BasisKlassen/parseFromFile.hpp>

const int varDim = 1;    // one variable - scalar transport 
const int spaceDim = 2;  // 2d space

#include "types.hpp"
#include "cfl.hpp"
#include "notiModify.hpp"
#include "problemBase2d.hpp"
#include "value2d.hpp"
#include "laxFriedrichs1d.hpp"
#include "mpiBase.hpp"
#include "cubedSphere_utils.hpp"
//#include <mpi.h>

extern CubedSphere_utils& cs_utils;
// linear advection on the cubed sphere
class Problem_AdvectionCubed : public ProblemBase2d<varDim>
{
public:

  struct Var {
    static const int val = 0;    // sqrt(G)*scalar
  };

  const BK::Array<BK::Array<int, 2>, 0> fluxVarVecIndizes;
  const BK::Array<int, 1> fluxVarScalarIndizes = {Var::val};

  static const int fluxVarianceType = 0; // 0=contra-variant; 1=co-variant
                                         // does not matter here because
                                         // only scalar quantity
                                         // (no fluxVarVecIndizes)

  // necessary data types for numerical flux computation
  static const int fluxInfoNumber = 1;
  typedef BK::Array<double, fluxInfoNumber> FluxInfo;                 // [fa, fb, dfa, dfb, value] 
  typedef BK::Array<FluxInfo, 2> FluxInfoBord;                        // [+-][fa, fb, dfa, dfb, value]
  typedef BK::MultiArray<spaceDim+1, FluxInfoBord> FluxInfoBordArray; // (cell)[+-][fa, fb, dfa, dfb, value]

  // data type for flux storage
  typedef BK::MultiArray<spaceDim+1, Vector> FluxVecArray;
  
  friend class BK::StackSingleton<Problem_AdvectionCubed>;
  
  Problem_AdvectionCubed() {}

  void errorComp(BK::Vector<Array> const* c) {

    std::cout << "errorComp(BK::Vector<Array> const* c)\n";
    
    int ncelXloc = ncelx_ / mpiBase.get_dimSize()[0] ; // local here
    int ncelYloc = ncely_ / mpiBase.get_dimSize()[1] ; // local here
    
    BK::Vector<Array> cRef;
    cRef.resize(6, Array(ncelXloc,  ncelYloc , order, order));
    
    getReference(cRef);

    Array const& c0 = (*c)[0];
    Array const& c1 = (*c)[1];
    Array const& c2 = (*c)[2];
    Array const& c3 = (*c)[3];
    Array const& c4 = (*c)[4];
    Array const& c5 = (*c)[5];

    Array const& cRef0 = cRef[0];
    Array const& cRef1 = cRef[1];
    Array const& cRef2 = cRef[2];
    Array const& cRef3 = cRef[3];
    Array const& cRef4 = cRef[4];
    Array const& cRef5 = cRef[5];

    double errorr, error ;
    double errorloc = 0 ;
    size_t nx = (*c)[0].size(0);
    size_t ny = (*c)[0].size(1);
    for(int ix = 0; ix < int(nx); ix++)
      for(int iy = 0; iy < int(ny); iy++) {
	errorloc += BK::sqr(c0(ix, iy, 0, 0)[Var::val] - cRef0(ix, iy, 0, 0)[Var::val])*dx_*dy_;
	errorloc += BK::sqr(c1(ix, iy, 0, 0)[Var::val] - cRef1(ix, iy, 0, 0)[Var::val])*dx_*dy_;
	errorloc += BK::sqr(c2(ix, iy, 0, 0)[Var::val] - cRef2(ix, iy, 0, 0)[Var::val])*dx_*dy_;
	errorloc += BK::sqr(c3(ix, iy, 0, 0)[Var::val] - cRef3(ix, iy, 0, 0)[Var::val])*dx_*dy_;
	errorloc += BK::sqr(c4(ix, iy, 0, 0)[Var::val] - cRef4(ix, iy, 0, 0)[Var::val])*dx_*dy_;
	errorloc += BK::sqr(c5(ix, iy, 0, 0)[Var::val] - cRef5(ix, iy, 0, 0)[Var::val])*dx_*dy_;
	//std::cout << value(ix, iy, 0., 0., *c)[0] <<"\t" << - value(ix, iy, 0., 0., cRef)[0] <<"\t" <<  errorloc << "\t" << dx_ <<"\t" <<dy_<< std::endl;
	
      }
    //MPI_Reduce(&errorloc, &errorr, 1.,MPI_DOUBLE,MPI_SUM,0,MPI_COMM_WORLD);
    mpiBase.mpiAllReduceSum(&errorloc, &errorr, 1);
    error = sqrt(errorr);
    if (mpiBase.get_commRank() == 0){
      std::string dirName = BK::toString("data/", name_);
      int mkdir = system(BK::toString("mkdir data").c_str());
      mkdir = system(BK::toString("mkdir ",dirName).c_str());
      std::cerr << mkdir << std::endl;
      std::cerr << "writing error to " << BK::toString(dirName + "/error-ndg-",order,"-",rkOrder_,".txt") << std::endl;
    
      /*if (mpiBase.get_commRank() == 0){
	double errorz = errorloc;
	std::ofstream outChrono(BK::toString(dirName + "/error-ndg-",order,"-",rkOrder_,".txt"), std::ios::app);
	outChrono << "mpi0err:" << "\t" << errorz << std::endl;
	outChrono.close(); }
	if (mpiBase.get_commRank() == 1){
	double erroro = errorloc;
	std::ofstream outChrono(BK::toString(dirName + "/error-ndg-",order,"-",rkOrder_,".txt"), std::ios::app);
      outChrono << "mpi1err:" << "\t" << erroro << std::endl;
      outChrono.close(); }*/
    
   
    std::ofstream outChrono(BK::toString(dirName + "/error-ndg-",order,"-",rkOrder_,".txt"), std::ios::app);
    outChrono << ncelx_ << "\t" << error << std::endl;
    outChrono.close(); }
  }
      
  void init(int ncelx, int ncely, std::string parameterFilename) {

    if(!initialized_)
      notifyFieldVec.add("fieldFinal", FieldVecNotifyCallBack(std::bind(&Problem_AdvectionCubed::errorComp,this,std::placeholders::_1), "Problem_AdvectionCubed::errorComp"));
    
    ProblemBase2d::init(ncelx, ncely, parameterFilename);

    double nt = nt_;
    double T = T_;
    
    dt_ = fabs(cfl_*dx_);

    name_ = "advectionCubed";

    if(nt > 0 && T > 0) {
      std::cerr << "ERROR in Problem_advectionCubed::init(): nt = " <<
	nt << " > 0 && T = " << T << " > 0." << std::endl;
      exit(1);						   
    }

    double a = 1;
    
    if(T < 0) {
      T_ = -T*fabs(lx_/a);
    }
    else
      T_ = T;

    if(nt > 0) {
      nt_ = nt;
      T_ = nt_*dt_;
    }
    else
      nt_ = int(T_/dt_) + 1;

    dt_ *= T_/(nt_*dt_);

    uaVec.resize(6, BK::MultiArray<spaceDim*2,double>(ncelx_, ncely, order, order));
    ubVec.resize(6, BK::MultiArray<spaceDim*2,double>(ncelx_, ncely, order, order));

    initVelocity();

    logParameter();

    initialized_ = true;
  }

  void setVelocity(BK::Array<double,3> xyz,BK::Array<double,3>& v){
    double omega = 1;    
    double X = xyz[0];
    double Y = xyz[1];
    // double Z = xyz[2];

    // // Deformational flow
    // auto pt = xyz2pt(X,Y,Z);
    // double theta = pt[1];
    // double r0 = 1;
    // double rho = r0*cos(theta);
    // double vt;
    
    // if(rho == 0)
    //   vt = 0;
    // else
    //   vt = 1.5*sqrt(3)*1./(cosh(rho)*cosh(rho))*tanh(rho);

    // omega = vt/rho;
    
    v[0] = -omega*Y;
    v[1] = omega*X;
    v[2] = 0;

    // rotation around x
    // v[0] = 0;
    // v[1] = -omega*Z;
    // v[2] = omega*Y;

    // rotation around y
    // v[0] = omega*Z;
    // v[1] = 0;
    // v[2] = -omega*X;
  }
  
  void initVelocity() {
    std::ofstream outx("initVx.csv"); outx << "x,y,z,vx,vy,vz" << std::endl;
    
    BK::Vector<double> xi = points[order-1];

    da_ = M_PI/2./ncelx_;
    db_ = M_PI/2./ncely_;

    // for(size_t a = 0; a<ncelx_; a++) {
    //   auto transA = [a, this](double x) { return -M_PI/4. + a*da_+ 0.5*da_ + x*0.5*da_;};
    //   for(size_t b = 0; b<ncely_; b++) {
    // 	auto transB = [b, this](double y) { return -M_PI/4. + b*db_+ 0.5*db_ + y*0.5*db_;};
    // 	for(size_t ia = 0; ia < order; ia++)
    // 	  for(size_t ib = 0; ib < order; ib++) {

    // 	    BK::MultiArray<spaceDim*2,double>& ua1 = uaVec[0];
    // 	    BK::MultiArray<spaceDim*2,double>& ub1 = ubVec[0];
    // 	    BK::MultiArray<spaceDim*2,double>& ua2 = uaVec[1];
    // 	    BK::MultiArray<spaceDim*2,double>& ub2 = ubVec[1];
    // 	    BK::MultiArray<spaceDim*2,double>& ua3 = uaVec[2];
    // 	    BK::MultiArray<spaceDim*2,double>& ub3 = ubVec[2];
    // 	    BK::MultiArray<spaceDim*2,double>& ua4 = uaVec[3];
    // 	    BK::MultiArray<spaceDim*2,double>& ub4 = ubVec[3];
    // 	    BK::MultiArray<spaceDim*2,double>& ua5 = uaVec[4];
    // 	    BK::MultiArray<spaceDim*2,double>& ub5 = ubVec[4];
    // 	    BK::MultiArray<spaceDim*2,double>& ua6 = uaVec[5];
    // 	    BK::MultiArray<spaceDim*2,double>& ub6 = ubVec[5];
	    
    // 	    // face 1;
    // 	    BK::Array<double,3> xyz = ab2xyz_P1(transA(xi[ia]), transB(xi[ib]));
    // 	    BK::Array<double,3> v;
    //         setVelocity(xyz,v);
    // 	    outx<<xyz[0]<<","<<xyz[1]<<"," <<xyz[2]<<","<<v[0]<<","<<v[1]<<","<<v[2]<<std::endl;
    // 	    d_map_P1(v, ua1(a, b, ia, ib), ub1(a, b, ia, ib), xyz);

    // 	    // face 2;
    // 	    xyz = ab2xyz_P2(transA(xi[ia]), transB(xi[ib]));
    // 	    setVelocity(xyz,v);
    // 	    outx<<xyz[0]<<","<<xyz[1]<<"," <<xyz[2]<<","<<v[0]<<","<<v[1]<<","<<v[2]<<std::endl;
    // 	    d_map_P2(v, ua2(a, b, ia, ib), ub2(a, b, ia, ib), xyz);
	    
    // 	    // face 3;
    // 	    xyz = ab2xyz_P3(transA(xi[ia]), transB(xi[ib]));
    // 	    setVelocity(xyz,v);
    // 	    outx<<xyz[0]<<","<<xyz[1]<<"," <<xyz[2]<<","<<v[0]<<","<<v[1]<<","<<v[2]<<std::endl;
    // 	    d_map_P3(v, ua3(a, b, ia, ib), ub3(a, b, ia, ib), xyz);
	    
    // 	    // face 4;
    // 	    xyz = ab2xyz_P4(transA(xi[ia]), transB(xi[ib]));
    // 	    setVelocity(xyz,v);
    // 	    outx<<xyz[0]<<","<<xyz[1]<<"," <<xyz[2]<<","<<v[0]<<","<<v[1]<<","<<v[2]<<std::endl;
    // 	    d_map_P4(v, ua4(a, b, ia, ib), ub4(a, b, ia, ib), xyz);
	    
    // 	    // face 5;
    // 	    xyz = ab2xyz_P5(transA(xi[ia]), transB(xi[ib]));
    // 	    setVelocity(xyz,v);
    // 	    outx<<xyz[0]<<","<<xyz[1]<<"," <<xyz[2]<<","<<v[0]<<","<<v[1]<<","<<v[2]<<std::endl;
    // 	    d_map_P5(v, ua5(a, b, ia, ib), ub5(a, b, ia, ib), xyz);
	    
    // 	    // face 6;
    // 	    xyz = ab2xyz_P6(transA(xi[ia]), transB(xi[ib]));
    // 	    setVelocity(xyz,v);
    // 	    outx<<xyz[0]<<","<<xyz[1]<<"," <<xyz[2]<<","<<v[0]<<","<<v[1]<<","<<v[2]<<std::endl;
    // 	    d_map_P6(v, ua6(a, b, ia, ib), ub6(a, b, ia, ib), xyz);
    // 	  }
    //   }
    // }

    for(size_t a = 0; a<ncelx_; a++) {
      auto alphaFunc = [a, this](double x) { return -M_PI/4. + a*da_+ 0.5*da_ + x*0.5*da_;};
      for(size_t b = 0; b<ncely_; b++) {
    	auto betaFunc = [b, this](double y) { return -M_PI/4. + b*db_+ 0.5*db_ + y*0.5*db_;};
    	for(size_t ia = 0; ia < order; ia++)
    	  for(size_t ib = 0; ib < order; ib++) {

    	    double alpha = alphaFunc(xi[ia]);
    	    double beta = betaFunc(xi[ib]);
    	    BK::Array<double,3> xyz;
    	    BK::Array<double,3> v;

    	    for(int face=0; face<6;face++) {
    	      xyz = ab2xyz[face](alpha, beta);
    	      setVelocity(xyz,v);
    	      outx<<xyz[0]<<","<<xyz[1]<<"," <<xyz[2]<<","<<v[0]<<","<<v[1]<<","<<v[2]<<std::endl;
    	      d_map[face](v, uaVec[face](a, b, ia, ib), ubVec[face](a, b, ia, ib), xyz);
    	    }
    	  }
      }
    }

    outx.close();
  }

  Vector initialCondition(int a, int b, int ia, int ib, int face, double shiftAngle = 0) {
    auto alphaFunc = [a, this](double x) { return -M_PI/4. + a*da_+ 0.5*da_ + x*0.5*da_;};
    auto betaFunc = [b, this](double y) { return -M_PI/4. + b*db_+ 0.5*db_ + y*0.5*db_;};

    BK::Vector<double> xi = points[order-1];
    
    double alpha = alphaFunc(xi[ia]);
    double beta = betaFunc(xi[ib]);
    
    BK::Array<double,3> xyz = ab2xyz[face](alpha, beta);
    BK::Array<double,2> pt = xyz2pt(xyz[0], xyz[1], xyz[2]);

    return cs_utils.sqrtG(a, b, ia, ib)*initialCondition(pt[0], pt[1]);
  }
  
  Vector initialCondition(double phi, double theta, double shiftAngle = 0) {
    assert(initialized_);

    // Blob --------------------------------------------------
    // double h0 = 1;
    // double deltaC = 0.4;

    // double phi0 = 0;//M_PI/2;
    // double theta0 = M_PI/2;
    
    // double deltaPhi = fabs(phi0 - phi);
    // double deltaTheta = fabs(theta0 - theta);
    // double centralAngle = sqrt(deltaPhi*deltaPhi + deltaTheta*deltaTheta);

    // double value = h0/2*(1+cos(M_PI*centralAngle/deltaC));
    // if(centralAngle > deltaC)
    //   value = 0;

    // smooth cosine ------------------------------------------
    double value = cos(phi)*sin(theta);
    
    return value;
  }

  void getReference(BK::Vector<Array> & cVec) {
    
    for(int a = 0; a<int(ncelx_); a++) 
      for(int b = 0; b<int(ncely_); b++) 
	for(int ia = 0; ia < order; ia++)
	  for(int ib = 0; ib < order; ib++) 
	    for(int face = 0; face <6; face++)
	      cVec[face](a, b, ia, ib) = initialCondition(a, b, ia, ib, face);
  }

  std::string toWriteCSV_info()
  {
    return "x,y,z,p,t,a,b,face,scalar,vx,vy,vz";
  }

  // std::string toWritePhiThetaCSV_info()
  // {
  //   return "p,t,z,scalar,vx,vy,vz";
  // }

  BK::Vector<std::string> toWriteNetCDF_info()
  {
    return {"scalar","vx","vy","vz"};
  }

  BK::Vector<double> toWriteCSV(Array const& array, BK::Array<double,2> ab, int face)
  {
    double alpha = ab[0];
    size_t cellA = cs_utils.a2cell(alpha);
    
    double beta = ab[1];
    size_t cellB = cs_utils.b2cell(beta);

    if(cellA == ncelx_) cellA = ncelx_-1;
    if(cellB == ncely_) cellB = ncely_-1;

    double x = cs_utils.alpha2LagrangeInterval(alpha);
    double y = cs_utils.alpha2LagrangeInterval(beta);

    auto data = value(cellA, cellB, x, y, array);
    double sqrtG = value(cellA, cellB, x, y, cs_utils.sqrtG);

    double scalar = data[0]/sqrtG;
    double ax = value(cellA, cellB, x, y, uaVec[face]);
    double ay = value(cellA, cellB, x, y, ubVec[face]);

    BK::Array<double,3> v;
    d_parametrisation[face](v, ax, ay, alpha, beta);
    
    BK::Vector<double> toWriteVec({scalar, v[0], v[1], v[2]});
    
    return toWriteVec;
  }

  BK::Vector<double> toWriteCSV(Array const& array, int a, int b, int ia, int ib, int face)
  {
    double scalar = array(a, b, ia, ib)[0]/cs_utils.sqrtG(a, b, ia, ib);
    double ax = uaVec[face](a, b, ia, ib);
    double ay = ubVec[face](a, b, ia, ib);

    BK::Vector<double> xi = points[order-1];
    auto alphaFunc = [a, this](double x) { return -M_PI/4. + a*da_+ 0.5*da_ + x*0.5*da_;};
    auto betaFunc = [b, this](double y) { return -M_PI/4. + b*db_+ 0.5*db_ + y*0.5*db_;};
    double alpha = alphaFunc(xi[ia]);
    double beta = betaFunc(xi[ib]);
    
    BK::Array<double,3> v;
    d_parametrisation[face](v, ax, ay, alpha, beta);
    
    BK::Vector<double> toWriteVec({scalar, v[0], v[1], v[2]});
    
    return toWriteVec;
  }

  void lhs(Array& cNew, Array const& c, Array const& rhs) const {
    cNew = c + dt_*rhs;
  }    

  VecVec compFlux(Vector const& varArray, size_t a, size_t b, size_t ia, size_t ib, int face) {
    assert(initialized_);

    // double sizeA = cs_utils.sqrtG.size(0);
    // if(a == sizeA) {
    //   a = sizeA-1;
    //   ia = order-1;
    // }

    // double sizeB = cs_utils.sqrtG.size(1);
    // if(b == sizeB) {
    //   b = sizeB-1;
    //   ib = order-1;
    // }

    return {{uaVec[face](a, b, ia, ib)*varArray}, {ubVec[face](a, b, ia, ib)*varArray}};
  }

  BK::Array<double,2> compDFlux(Vector const& varArray, size_t a, size_t b, size_t ia, size_t ib, int face) {
    assert(initialized_);
    
    // double sizeA = cs_utils.sqrtG.size(0);
    // if(a == sizeA) {
    //   a = sizeA-1;
    //   ia = order-1;
    // }

    // double sizeB = cs_utils.sqrtG.size(1);
    // if(b == sizeB) {
    //   b = sizeB-1;
    //   ib = order-1;
    // }

    double ax = uaVec[face](a, b, ia, ib);
    double ay = ubVec[face](a, b, ia, ib);

    // if(face == 0)
    //   std::cout << a << "\t" << b << "\t" << ia << "\t" << ib << "\t" << "a = " << ax << "\t" << ay << "\t" << BK::Array<double,2>({ax , ay}) << std::endl;
    
    return BK::Array<double,2>({ax , ay});
    //return {0.,0.};
  }

  void computeFluxes(FluxArray& fluxArray,
		     FluxInfoBordArray& fluxInfoBordArrayX,
		     FluxInfoBordArray& fluxInfoBordArrayY,
		     Array const& c, int face) {

    for(int cellX=0; cellX<int(c.size(0)); cellX++)
      for(int cellY=0; cellY<int(c.size(1)); cellY++)
	for(int indexX=0; indexX<int(c.size(2)); indexX++)
	  for(int indexY=0; indexY<int(c.size(3)); indexY++) {
	    VecVec flux = compFlux(c(cellX, cellY, indexX, indexY), cellX, cellY, indexX, indexY, face);
	    
	    fluxArray(cellX, cellY, indexX, indexY) = flux;
	    if(indexX == 0)
	      fillFluxInfoBordArray(cellX, cellY, indexY, Val::plus, c, fluxInfoBordArrayX, cellX, cellY, indexX, indexY);
	    if(indexX == order-1) 
	      fillFluxInfoBordArray(cellX+1, cellY, indexY, Val::minus, c, fluxInfoBordArrayX, cellX, cellY, indexX, indexY);
	    if(indexY == 0) 
	      fillFluxInfoBordArray(cellX, cellY, indexX, Val::plus, c, fluxInfoBordArrayY, cellX, cellY, indexX, indexY);
	    if(indexY == order-1) 
	      fillFluxInfoBordArray(cellX, cellY+1, indexX, Val::minus, c, fluxInfoBordArrayY, cellX, cellY, indexX, indexY);
	  }
  }

  void computeNumericalFlux(FluxVecArray& fluxVecArrayX, FluxVecArray& fluxVecArrayY,
			    FluxInfoBordArray& fluxInfoArrayX, FluxInfoBordArray& fluxInfoArrayY, int face) {

    size_t sizeA = fluxVecArrayX.size(0);
    size_t sizeB = fluxVecArrayX.size(1);
    // a-loop only up to sizeA-2 because fluxVecArrayX.size(0) = c.size(0) + 1!
    for(size_t a=0; a<sizeA-1; a++)
      for(size_t b=0; b<sizeB; b++)
	for(size_t ib=0; ib<order; ib++) {
	  
	  Vector valM = fluxInfoArrayX(a, b, ib)[Val::minus];
	  VecVec fm = compFlux(valM, a, b, 0, ib, face);
	  Vector valP = fluxInfoArrayX(a, b, ib)[Val::plus];
	  VecVec fp = compFlux(valP, a, b, 0, ib, face);
	  double dfm = compDFlux(valM, a, b, 0, ib, face)[Dir::x];
	  double dfp = compDFlux(valP, a, b, 0, ib, face)[Dir::x];

	  fluxVecArrayX(a, b, ib) =  laxFriedrichs1d_flux_numeric(fm[Dir::x], fp[Dir::x], dfm, dfp, valM, valP);
	}

    for(size_t b=0; b<sizeB; b++) 
      for(size_t ib=0; ib<order; ib++) {
	
      Vector valM = fluxInfoArrayX(sizeA-1, b, ib)[Val::minus];
      VecVec fm = compFlux(valM, sizeA-2, b, order-1, ib, face);
      Vector valP = fluxInfoArrayX(sizeA-1, b, ib)[Val::plus];
      VecVec fp = compFlux(valP, sizeA-2, b, order-1, ib, face);
      double dfm = compDFlux(valM, sizeA-2, b, order-1, ib, face)[Dir::x];
      double dfp = compDFlux(valP, sizeA-2, b, order-1, ib, face)[Dir::x];
      
      fluxVecArrayX(sizeA-1, b, ib) =  laxFriedrichs1d_flux_numeric(fm[Dir::x], fp[Dir::x], dfm, dfp, valM, valP);
    }

    // a-loop only up to sizeB-2 because fluxVecArrayY.size(1) = c.size(1) + 1!
    sizeA = fluxVecArrayY.size(0);
    sizeB = fluxVecArrayY.size(1);
    for(size_t a=0; a<sizeA; a++)
      for(size_t b=0; b<sizeB-1; b++)
	for(size_t ia=0; ia<order; ia++) {

	  Vector valM = fluxInfoArrayY(a, b, ia)[Val::minus];
	  VecVec fm = compFlux(valM, a, b, ia, 0, face);
	  Vector valP = fluxInfoArrayY(a, b, ia)[Val::plus];
	  VecVec fp = compFlux(valP, a, b, ia, 0, face);
	  double dfm = compDFlux(valM, a, b, ia, 0, face)[Dir::y];
	  double dfp = compDFlux(valP, a, b, ia, 0, face)[Dir::y];
	  
	  fluxVecArrayY(a, b, ia) =  laxFriedrichs1d_flux_numeric(fm[Dir::y], fp[Dir::y], dfm, dfp, valM, valP);
	}

    for(size_t a=0; a<sizeA; a++)
      for(size_t ia=0; ia<order; ia++) {
	
	Vector valM = fluxInfoArrayY(a, sizeB-1, ia)[Val::minus];
	VecVec fm = compFlux(valM, a, sizeB-2, ia, order-1, face);
	
	Vector valP = fluxInfoArrayY(a, sizeB-1, ia)[Val::plus];
	VecVec fp = compFlux(valP, a, sizeB-2, ia, order-1, face);
	
	double dfm = compDFlux(valM, a, sizeB-2, ia, order-1, face)[Dir::y];
	double dfp = compDFlux(valP, a, sizeB-2, ia, order-1, face)[Dir::y];
	
	fluxVecArrayY(a, sizeB-1, ia) =  laxFriedrichs1d_flux_numeric(fm[Dir::y], fp[Dir::y], dfm, dfp, valM, valP);
      }
  }

  CONSTPARA(double, da);
  CONSTPARA(double, db);

  BK::Vector<BK::MultiArray<spaceDim*2,double>> uaVec;
  BK::Vector<BK::MultiArray<spaceDim*2,double>> ubVec;
};

typedef Problem_AdvectionCubed Problem;
extern Problem& problem;

#endif
