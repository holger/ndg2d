#ifndef logger2_h
#define logger2_h
//-----------------------------------------------------------------------------
#include <BasisKlassen/logger.hpp>
/** @file logger.hpp
  * @brief logging functionality
  *
  * Implements methods for logging certain events, where the events are logged 
  * depending on a LOGLEVEL-constant.
  * 
  * 0 : unconditional logging
  * 1 : all constructors, very important events
  * 2 : important events, functions, called once only
  * 3 : member function calls, once per timestep
  * 4 : end of member function calls
  * 5 : internal parameters
  * 6 : less important information
  */
//-----------------------------------------------------------------------------
/// append give message to , if this is the master process
std::string appendMessage(const std::string& message);
/// append give message to with new line, if this is the master process
std::string appendMessageL(const std::string& message);
/// returns name of the log file
std::string getLogFilename();
/// set the log level
#define LOG(i,x)                 \
  FILELOG(i,getLogFilename(),appendMessage(x));	\
  ERRORLOG(i,appendMessage(x));
 
  
/// set the log level
#define LOGL(i,x)                   \
  FILELOG(i,getLogFilename(),appendMessageL(x));	\
  ERRORLOG(i,appendMessageL(x));
  

#define WRITE(i,x)                 \
  ERRORLOG(i,appendMessage(x));
/// set the log level
#define WRITEL(i,x)                   \
  ERRORLOG(i,appendMessageL(x));
#endif

// LOGGING: ----------------------------------------------------
#undef LOGLEVEL
/// the log-level
#define LOGLEVEL 1

// LOGLEVEL:
// 0: guaranteed output
// 1: very important information, all constructors
// 2: important informations, uniquely called functions
// 3: member function calls (once per time step)
// 4: end of member function calls
// 5: inside loops
// 6: unimportant things
