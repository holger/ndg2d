#ifndef problem_advection3d_hpp
#define problem_advection3d_hpp

#include <BasisKlassen/parameter.hpp>
#include <BasisKlassen/singleton.hpp>
#include <BasisKlassen/assert.hpp>
#include <BasisKlassen/parseFromFile.hpp>

const int varDim = 1;       // one variable - scalar transport 
const int spaceDim = 3;     // 3d space

#include "types.hpp"
#include "cfl.hpp"
#include "notiModify.hpp"
#include "problemBase3d.hpp"
#include "value3d.hpp"
#include "laxFriedrichs1d.hpp"
#include "mpiBase.hpp"
#include "logger.hpp"

// LOGGING: ----------------------------------------------------
// #undef LOGLEVEL
// #define LOGLEVEL 4

// linear advection 3D
class Problem_Advection3d : public ProblemBase3d<varDim>
{
public:

  struct Var {
    static const int val = 0;
  };
  
  friend class BK::StackSingleton<Problem_Advection3d>;

  Problem_Advection3d() {}

  void errorComp(Array const* c) {

    int ncelXloc = ncelx_ /  mpiBase.get_dimSize()[0] ; // local here
    int ncelYloc = ncely_ /  mpiBase.get_dimSize()[1] ; // local here
    int ncelZloc = ncelz_ /  mpiBase.get_dimSize()[2] ; // local here

    Array cRef(ncelXloc, ncelYloc, ncelZloc, order, order, order);
    
    getReference(cRef);
    
    // writeFine(cRef, dirName + "/ref_" +  std::to_string(order) + "-" + std::to_string(rkOrder) +"_" + std::to_string(ncelx)+"_"+std::to_string(ncely), nPoParCel);

    double errorr, error ;
    double errorloc = 0;
    size_t nx = c->size(0);
    size_t ny = c->size(1);
    size_t nz = c->size(2);
    for(size_t ix = 0; ix < nx; ix++)
      for(size_t iy = 0; iy < ny; iy++)
   	for(size_t iz = 0; iz < nz; iz++){
   	  errorloc += BK::sqr(value(ix, iy, iz, 0., 0., 0., *c)[0] - value(ix, iy, iz, 0., 0., 0., cRef)[0])*dx_*dy_*dz_;
	}
    
    //MPI_Reduce(&errorloc, &errorr, 1.,MPI_DOUBLE,MPI_SUM,0,MPI_COMM_WORLD);
    mpiBase.mpiAllReduceSum(&errorloc, &errorr, 1);
    error = sqrt(errorr);
    
    if (mpiBase.get_commRank() == 0){
      std::string dirName = BK::toString("data/", name_);
      int mkdir = system(BK::toString("mkdir data").c_str());
      mkdir = system(BK::toString("mkdir ",dirName).c_str());
      std::cerr << mkdir << std::endl;
      std::cerr << "writing error to " << BK::toString(dirName + "/error-ndg-",order,"-",rkOrder_,".txt") << std::endl;

      std::ofstream outChrono(BK::toString(dirName + "/error-ndg-",order,"-",rkOrder_,".txt"), std::ios::app);
      outChrono << nx << "\t" << error << std::endl;
      outChrono.close();
    }
  }
  
  void init(int ncelx, int ncely, int ncelz, std::string parameterFilename) {
    LOGL(1,"Problem_Advection3d::init");
	
    assert(!initialized_);
    
    notifyField.add("fieldFinal", FieldNotifyCallBack(std::bind(&Problem_Advection3d::errorComp, this, std::placeholders::_1), "Problem_Advection3d::errorComp"));

    name_ = "advection3d";
    ProblemBase3d::init(ncelx, ncely, ncelz, parameterFilename);

    BK::ReadInitFile initFile(parameterFilename,true);
    ax_ = initFile.getParameter<double>("ax");
    ay_ = initFile.getParameter<double>("ay");
    az_ = initFile.getParameter<double>("az");

    int nt = nt_;
    double T = T_;

    // w_ = 2;
    // ox_ = 0.5;
    // oy_ = 0.5;
    // oz_ = 0.5;

    dt_ = fabs(cfl_*dx_);

    if(nt > 0 && T > 0) {
      std::cerr << "ERROR in Problem_advection2d::init(): nt = " <<
	nt << " > 0 && T = " << T << " > 0." << std::endl;
      exit(1);						   
    }

    double a;
    if(ax_ > 0) a = ax_;
    else if(ay_ > 0) a = ay_;
    else a = az_;
    
    if(T < 0) {
      T_ = -T*fabs(lx_/a);
    }
    else
      T_ = T;

    if(nt > 0) {
      nt_ = nt;
      T_ = nt_*dt_;
    }
    else
      nt_ = int(T_/dt_) + 1;

    dt_ *= T_/(nt_*dt_);

    logParameter();
    LOGL(1,PAR(ax_));
    LOGL(1,PAR(ay_));
    LOGL(1,PAR(az_));

    initialized_ = true;
      
    LOGL(4,"Problem_Advection3d::init-end");
  }

  Vector initialCondition(double x, double y, double z, double shift = 0) {
    assert(initialized_);
    // double x0 = 0.35;
    // double y0 = 0.35;

    // int kMax = 10;
    // double temp = 0;
    // for(int k=1; k<=kMax; k++)
    //   temp += pow(k,-5./6)*sin(2*M_PI*k/lambda_*(x-shift-2*M_PI/(k*k)));

    // return temp;

    int k;
    int kDir;
    bool firstTime = true;
    if(firstTime) {
      BK::ReadInitFile initFile(parameterFilename_,true);
      k = initFile.getParameter<double>("k");
      kDir = initFile.getParameter<double>("kDir");      
      firstTime = false;
    }

    if(kDir == 0)
      return Vector{sin(2*M_PI*k*(x-shift)/lx_)};
    else if(kDir == 1)
      return Vector{sin(2*M_PI*k*(y-shift)/ly_)};
    else if(kDir == 2)
      return Vector{sin(2*M_PI*k*(z-shift)/lz_)};
    else {
      std::cerr << "ERROR in Problem_Advetion2d::initialCondition: kDir < 0 || kDir > 2\n";
      exit(0);
    }
    
    // double k=1;
    // return Vector{sin(2*M_PI*k*(z-shift)/lx_)}; // ???? generalize to ly, lz...
    // return Vector{sin(2*M_PI*y/ly)};

    //    return Vector{exp(-((x-x0)*(x-x0)+(y-y0)*(y-y0))/(0.1*0.1))};
  }

  void getReference(Array & c) {
    BK::Vector<double> xi = points[order-1];
    BK::Vector<double> wi = weights[order-1];

    size_t ncelXloc = ncelx_ /  mpiBase.get_dimSize()[0] ; // local here
    size_t ncelYloc = ncely_ /  mpiBase.get_dimSize()[1] ; // local here
    size_t ncelZloc = ncelz_ /  mpiBase.get_dimSize()[2] ; // local here

    double startvalx = 0.0 +  mpiBase.procCoord()[0] * (lx_/ mpiBase.get_dimSize()[0]);
    double startvaly = 0.0 +  mpiBase.procCoord()[1] * (ly_/ mpiBase.get_dimSize()[1]);
    double startvalz = 0.0 +  mpiBase.procCoord()[2] * (lz_/ mpiBase.get_dimSize()[2]);
    
    for(size_t ix=0;ix<ncelXloc;ix++) {
      auto transX = [ix, startvalx, this](double x) { return startvalx + ix*dx_+0.5*dx_+x*0.5*dx_;};
      for(size_t iy=0;iy<ncelYloc;iy++) {
	auto transY = [iy, startvaly, this](double y) { return startvaly + iy*dy_+0.5*dy_+y*0.5*dy_;};
	for(size_t iz=0;iz<ncelZloc;iz++) {
	  auto transZ = [iz, startvalz, this](double z) { return startvalz + iz*dz_+0.5*dz_+z*0.5*dz_;};
	
	  for(size_t ox=0; ox<order; ox++)
	    for(size_t oy=0; oy<order; oy++)
	      for(size_t oz=0; oz<order; oz++)
		c(ix,iy,iz,ox,oy,oz) = initialCondition(transX(xi[ox]), transY(xi[oy]),transZ(xi[oz]), T_*ax_);
	
	}
      }
    }
  }

  void lhs(Array& cNew, Array const& c, Array const& rhs) const {

    LOGL(3,"Problem_Advection3d::lhs");
      
    cNew = c + dt_*rhs;

    LOGL(4,"Problem_Advection3d::lhs-end");
  }    

  VecVec flux_vector(Vector const& varArray) const {
    assert(initialized_);

    // double rox = (cellX+x)*dx-ox_;
    // double roy = (cellY+y)*dy-oy_;
    // double vx = -w_ * roy;
    // double vy = w_ * rox;
    
    return {ax_*varArray, ay_*varArray, az_*varArray};
  }

  BK::Array<double,3> dFlux_value(Vector const& varArray) const {
    assert(initialized_);
    
    // double rox = (cellX+x)*dx-ox_;
    // double roy = (cellY+y)*dy-oy_;
    // double vx = -w_ * roy;
    // double vy = w_ * rox;

    return {ax_, ay_, az_};
  }

  void computeNumericalFlux(FluxVecArray& fluxVecArrayX, FluxVecArray& fluxVecArrayY, FluxVecArray& fluxVecArrayZ,
			    FluxInfoBordArray& fluxInfoArrayX, FluxInfoBordArray& fluxInfoArrayY, FluxInfoBordArray& fluxInfoArrayZ) {
    auto fluxFunc = [this](Vector const& varArray) { return this->flux_vector(varArray);};
    auto dfluxFunc = [this](Vector const& varArray) { return this->dFlux_value(varArray);};
    
    computeNumericalFlux_LF(fluxVecArrayX, fluxVecArrayY, fluxVecArrayZ, fluxInfoArrayX, fluxInfoArrayY, fluxInfoArrayZ, fluxFunc, dfluxFunc);
  }

  CONSTPARA(double, w);
  CONSTPARA(double, ox);
  CONSTPARA(double, oy);
  CONSTPARA(double, oz);
  CONSTPARA(double, ax);
  CONSTPARA(double, ay);
  CONSTPARA(double, az);
};

typedef Problem_Advection3d Problem;
extern Problem& problem;

#endif
