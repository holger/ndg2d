#! /bin/bash

echo $1
echo $2

if [ -e data/eulerIsoViscosity1d ]
then
    rm -rf data/eulerIsoViscosity1d
fi

rsync -c $2/tests/order.hpp $2/order.hpp

make eulerIsoViscosity1d

if [ $? == 2 ]
then
    exit 1
fi

$1/eulerIsoViscosity1d $2/tests/eulerIsoViscosity1d/problem_eulerIsoViscosity1d.init

diff -q $2/tests/gold/eulerIsoViscosity1d/sol_6-6_8-0.data $1/data/eulerIsoViscosity1d/sol_6-6_8-0.data

exit $?
