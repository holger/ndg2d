#! /bin/bash

echo $1
echo $2

if [ -e data/eulerIsoScalar2d ]
then
    rm -rf data/eulerIsoScalar2d
fi

rsync -c $2/tests/order.hpp $2/order.hpp

make eulerIsoScalar2d

if [ $? == 2 ]
then
    exit 1
fi

$1/eulerIsoScalar2d $2/tests/eulerIsoScalar2d/problem_eulerIsoScalar2d.init

diff -q $2/tests/gold/eulerIsoScalar2d/sol_6-6_8_8_0-1-x.data $1/data/eulerIsoScalar2d/sol_6-6_8_8_0-1-x.data

exit $?
