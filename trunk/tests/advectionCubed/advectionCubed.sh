#! /bin/bash

echo $1
echo $2

if [ -e data/advectionCubed ]
then
    rm -rf data/advectionCubed
fi

rsync -c $2/tests/order.hpp $2/order.hpp

make advectionCubed

if [ $? == 2 ]
then
    exit 1
fi
    
$1/advectionCubed $2/tests/advectionCubed/problem_advectionCubed.init

diff -q $2/tests/gold/advectionCubed/final_6-6_4_4.csv $1/data/advectionCubed/final_6-6_4_4.csv

exit $?
