#! /bin/bash

echo $1
echo $2

if [ -e data/burgersDensity3d ]
then
    rm -rf data/burgersDensity3d
fi

rsync -c $2/tests/order.hpp $2/order.hpp

make burgersDensity3d

if [ $? == 2 ]
then
    exit 1
fi

$1/burgersDensity3d $2/tests/burgersDensity3d/problem_burgersDensity3d.init

diff -q $2/tests/gold/burgersDensity3d/sol_6-6_4_4_4-0-z.data $1/data/burgersDensity3d/sol_6-6_4_4_4_0-0-z.data

exit $?
