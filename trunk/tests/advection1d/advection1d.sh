#! /bin/bash

echo $1
echo $2

if [ -e data/advection1d ]
then
    rm -rf data/advection1d
fi

rsync -c $2/tests/order.hpp $2/order.hpp

make advection1d

if [ $? == 2 ]
then
    exit 1
fi

$1/advection1d $2/tests/advection1d/problem_advection1d.init

diff -q $2/tests/gold/advection1d/error-ndg-6-6.txt $1/data/advection1d/error-ndg-6-6.txt

exit $?
