#! /bin/bash

echo $1
echo $2

if [ -e data/shallowWaterCubed ]
then
    rm -rf data/shallowWaterCubed
fi

rsync -c $2/tests/order.hpp $2/order.hpp

make shallowWaterCubed

if [ $? == 2 ]
then
    exit 1
fi
    
$1/shallowWaterCubed $2/tests/shallowWaterCubed/problem_shallowWaterCubed.init

diff -q $2/tests/gold/shallowWaterCubed/final_6-6_4_4.csv $1/data/shallowWaterCubed/final_6-6_4_4.csv

exit $?
