#! /bin/bash

echo $1
echo $2

if [ -e data/eulerIso3d ]
then
    rm -rf data/eulerIso3d
fi

rsync -c $2/tests/order.hpp $2/order.hpp

make eulerIso3d

if [ $? == 2 ]
then
    exit 1
fi

$1/eulerIso3d $2/tests/eulerIso3d/problem_eulerIso3d.init

diff -q $2/tests/gold/eulerIso3d/sol_6-6_2_2_2_0-0-x.data $1/data/eulerIso3d/sol_6-6_2_2_2_0-0-x.data

exit $?
