#! /bin/bash

echo $1
echo $2

if [ -e data/advectionSource2d ]
then
    rm -rf data/advectionSource2d
fi

rsync -c $2/tests/order.hpp $2/order.hpp

make advectionSource2d

if [ $? == 2 ]
then
    exit 1
fi

$1/advectionSource2d $2/tests/advectionSource2d/problem_advectionSource2d.init

diff -q $2/tests/gold/advectionSource2d/sol_6-6_16_16_0-1-x.data $1/data/advectionSource2d/sol_6-6_16_16_0-1-x.data

exit $?
