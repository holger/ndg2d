#! /bin/bash

echo $1
echo $2

if [ -e data/burgersDensity1d ]
then
    rm -rf data/burgersDensity1d
fi

rsync -c $2/tests/order.hpp $2/order.hpp

make burgersDensity1d

if [ $? == 2 ]
then
    exit 1
fi

$1/burgersDensity1d $2/tests/burgersDensity1d/problem_burgersDensity1d.init

diff -q $2/tests/gold/burgersDensity1d/sol_6-6_32-0.data $1/data/burgersDensity1d/sol_6-6_32-0.data

exit $?
