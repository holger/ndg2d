#! /bin/bash

echo $1
echo $2

if [ -e data/eulerIsoViscosity2d ]
then
    rm -rf data/eulerIsoViscosity2d
fi

rsync -c $2/tests/order.hpp $2/order.hpp

make eulerIsoViscosity2d

if [ $? == 2 ]
then
    exit 1
fi

$1/eulerIsoViscosity2d $2/tests/eulerIsoViscosity2d/problem_eulerIsoViscosity2d.init

diff -q $2/tests/gold/eulerIsoViscosity2d/sol_6-6_8_8_0-0.data $1/data/eulerIsoViscosity2d/sol_6-6_8_8_0-0.data

exit $?
