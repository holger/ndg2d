#! /bin/bash

echo $1
echo $2

if [ -e data/shallowWaterCubedNair ]
then
    rm -rf data/shallowWaterCubedNair
fi

rsync -c $2/tests/order.hpp $2/order.hpp

make shallowWaterCubedNair

if [ $? == 2 ]
then
    exit 1
fi
    
$1/shallowWaterCubedNair $2/tests/shallowWaterCubedNair/problem_shallowWaterCubedNair.init

diff -q $2/tests/gold/shallowWaterCubedNair/final_6-6_4_4.csv $1/data/shallowWaterCubedNair/final_6-6_4_4.csv

exit $?
