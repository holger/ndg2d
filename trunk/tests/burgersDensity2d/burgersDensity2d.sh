#! /bin/bash

echo $1
echo $2

if [ -e data/burgersDensity2d ]
then
    rm -rf data/burgersDensity2d
fi

rsync -c $2/tests/order.hpp $2/order.hpp

make burgersDensity2d

if [ $? == 2 ]
then
    exit 1
fi

$1/burgersDensity2d $2/tests/burgersDensity2d/problem_burgersDensity2d.init

diff -q $2/tests/gold/burgersDensity2d/sol_6-6_16_16_0-0.data $1/data/burgersDensity2d/sol_6-6_16_16_0-0.data

exit $?
