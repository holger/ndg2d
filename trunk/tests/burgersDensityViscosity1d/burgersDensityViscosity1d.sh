#! /bin/bash

echo $1
echo $2

if [ -e data/burgersDensityViscosity1d ]
then
    rm -rf data/burgersDensityViscosity1d
fi

rsync -c $2/tests/order.hpp $2/order.hpp

make burgersDensityViscosity1d

if [ $? == 2 ]
then
    exit 1
fi

$1/burgersDensityViscosity1d $2/tests/burgersDensityViscosity1d/problem_burgersDensityViscosity1d.init

diff -q $2/tests/gold/burgersDensityViscosity1d/sol_6-6_8-0.data $1/data/burgersDensityViscosity1d/sol_6-6_8-0.data

exit $?
