#! /bin/bash

echo $1
echo $2

if [ -e data/advectionSource1d ]
then
    rm -rf data/advectionSource1d
fi

rsync -c $2/tests/order.hpp $2/order.hpp

make advectionSource1d

if [ $? == 2 ]
then
    exit 1
fi

$1/advectionSource1d $2/tests/advectionSource1d/problem_advectionSource1d.init

diff -q $2/tests/gold/advectionSource1d/sol_6-6_16-0.data $1/data/advectionSource1d/sol_6-6_16-0.data

exit $?
