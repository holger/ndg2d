#! /bin/bash

echo $1
echo $2

if [ -e data/heat1d ]
then
    rm -rf data/heat1d
fi

make heat1d

if [ $? == 2 ]
then
    exit 1
fi

$1/heat1d $2/tests/heat1d/problem_heat1d.init

diff -q $2/tests/gold/heat1d/sol_6-6_32-0.data $1/data/heat1d/sol_6-6_32-0.data

exit $?
