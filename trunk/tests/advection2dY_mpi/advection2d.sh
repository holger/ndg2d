#! /bin/bash

echo $1
echo $2

if [ -e data/advection2d ]
then
    rm -rf data/advection2d
fi

rsync -c $2/tests/order.hpp $2/order.hpp

make advection2d

if [ $? == 2 ]
then
    exit 1
fi
    
mpirun --oversubscribe -np 4 $1/advection2d $2/tests/advection2dY_mpi/problem_advection2d.init

diff -q $2/tests/gold/advection2dY/error-ndg-6-6.txt $1/data/advection2d/error-ndg-6-6.txt

exit $?
