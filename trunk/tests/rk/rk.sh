#! /bin/bash

echo $1
echo $2

if [ -e data/ ]
then
    rm -rf data/advection1d
fi

rsync -c $2/tests/order.hpp $2/order.hpp
make advection1d

echo "************ starting rk3 advection *************"
rsync -c $2/tests/rk/rk.init_template $2/tests/rk/rk.init
sed -i 's/rkOrder		6/rkOrder		3/g' $2/tests/rk/rk.init
$1/advection1d $2/tests/rk/rk.init
diff -q $2/tests/gold/rk/error-ndg-6-3.txt $1/data/advection1d/error-ndg-6-3.txt

echo $?

let rk3=!$?

if (( $rk3 != 1)); then
    let diff=!$rk3
    echo "************ rk3 failed *************"
    exit $diff
fi

echo "************ starting rk4 advection *************"
rsync -c $2/tests/rk/rk.init_template $2/tests/rk/rk.init
sed -i 's/rkOrder		6/rkOrder		4/g' $2/tests/rk/rk.init
$1/advection1d $2/tests/rk/rk.init
diff -q $2/tests/gold/rk/error-ndg-6-4.txt $1/data/advection1d/error-ndg-6-4.txt

let rk4=!$?

if (( $rk4 != 1)); then
    let diff=!$rk4
    echo "************ rk4 failed *************"
    exit $diff
fi

echo "************ starting rk5 advection *************"
rsync -c $2/tests/rk/rk.init_template $2/tests/rk/rk.init
sed -i 's/rkOrder		6/rkOrder		5/g' $2/tests/rk/rk.init
$1/advection1d $2/tests/rk/rk.init
diff -q $2/tests/gold/rk/error-ndg-6-5.txt $1/data/advection1d/error-ndg-6-5.txt

let rk5=!$?

if (( $rk5 != 1)); then
    let diff=!$rk5
    echo "************ rk5 failed *************"
    exit $diff
fi

echo "************ starting rk6 advection *************"
rsync -c $2/tests/rk/rk.init_template $2/tests/rk/rk.init
sed -i 's/rkOrder		6/rkOrder		6/g' $2/tests/rk/rk.init
$1/advection1d $2/tests/rk/rk.init
diff -q $2/tests/gold/rk/error-ndg-6-6.txt $1/data/advection1d/error-ndg-6-6.txt

let rk6=!$?

if (( $rk6 != 1)); then
    let diff=!$rk6
    echo "************ rk6 failed *************"
    exit $diff
fi

let rk=$rk3*$rk4*$rk5*$rk6

let diff=!$rk
echo $diff

exit $diff
