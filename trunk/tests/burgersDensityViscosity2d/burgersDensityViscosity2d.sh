#! /bin/bash

echo $1
echo $2

if [ -e data/burgersDensityViscosity2d ]
then
    rm -rf data/burgersDensityViscosity2d
fi

rsync -c $2/tests/order.hpp $2/order.hpp

make burgersDensityViscosity2d

if [ $? == 2 ]
then
    exit 1
fi

$1/burgersDensityViscosity2d $2/tests/burgersDensityViscosity2d/problem_burgersDensityViscosity2d.init

diff -q $2/tests/gold/burgersDensityViscosity2d/sol_6-6_8_8_0-0.data $1/data/burgersDensityViscosity2d/sol_6-6_8_8_0-0.data

exit $?
