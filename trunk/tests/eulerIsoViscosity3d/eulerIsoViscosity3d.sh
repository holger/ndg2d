#! /bin/bash

echo $1
echo $2

if [ -e data/eulerIsoViscosity2d ]
then
    rm -rf data/eulerIsoViscosity3d
fi

rsync -c $2/tests/order.hpp $2/order.hpp

make eulerIsoViscosity3d

if [ $? == 2 ]
then
    exit 1
fi

$1/eulerIsoViscosity3d $2/tests/eulerIsoViscosity3d/problem_eulerIsoViscosity3d.init

diff -q $2/tests/gold/eulerIsoViscosity3d/sol_6-6_2_2_2_0-0-x.data $1/data/eulerIsoViscosity3d/sol_6-6_2_2_2_0-0-x.data

exit $?
