#! /bin/bash

echo $1
echo $2

if [ -e data/eulerIso2d ]
then
    rm -rf data/eulerIso2d
fi

rsync -c $2/tests/order.hpp $2/order.hpp

make eulerIso2d

if [ $? == 2 ]
then
    exit 1
fi

$1/eulerIso2d $2/tests/eulerIso2d_KH/problem_eulerIso2d_KH.init

diff -q $2/tests/gold/eulerIso2d_KH/sol_6-6_8_8_0-0.data $1/data/eulerIso2d/sol_6-6_8_8_0-0.data

exit $?
