#! /bin/bash

echo $1
echo $2

if [ -e data/burgersDensityViscosity3d ]
then
    rm -rf data/burgersDensityViscosity3d
fi

rsync -c $2/tests/order.hpp $2/order.hpp

make burgersDensityViscosity3d

if [ $? == 2 ]
then
    exit 1
fi

$1/burgersDensityViscosity3d $2/tests/burgersDensityViscosity3d/problem_burgersDensityViscosity3d.init

diff -q $2/tests/gold/burgersDensityViscosity3d/sol_6-6_2_2_2_0-0-x.data $1/data/burgersDensityViscosity3d/sol_6-6_2_2_2_0-0-x.data

exit $?
