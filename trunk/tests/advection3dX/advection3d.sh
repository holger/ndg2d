#! /bin/bash

echo $1
echo $2

if [ -e data/advection3d ]
then
    rm -rf data/advection3d
fi

rsync -c $2/tests/order.hpp $2/order.hpp

make advection3d

if [ $? == 2 ]
then
    exit 1
fi

$1/advection3d $2/tests/advection3dX/problem_advection3d.init

diff -q $2/tests/gold/advection3dX/sol_6-6_4_4_4-0-z.data $1/data/advection3d/sol_6-6_4_4_4_0-0-z.data

exit $?
