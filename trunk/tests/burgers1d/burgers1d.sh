#! /bin/bash

echo $1
echo $2

if [ -e data/burgers1d ]
then
    rm -rf data/burgers1d
fi

rsync -c $2/tests/order.hpp $2/order.hpp

make burgers1d

if [ $? == 2 ]
then
    exit 1
fi

$1/burgers1d $2/tests/burgers1d/problem_burgers1d.init

diff -q $2/tests/gold/burgers1d/sol_6-6_32-0.data $1/data/burgers1d/sol_6-6_32-0.data

exit $?
