#! /bin/bash

echo $1
echo $2

if [ -e data/eulerIso1d ]
then
    rm -rf data/eulerIso1d
fi

rsync -c $2/tests/order.hpp $2/order.hpp

make eulerIso1d

if [ $? == 2 ]
then
    exit 1
fi

$1/eulerIso1d $2/tests/eulerIso1d/problem_eulerIso1d.init

diff -q $2/tests/gold/eulerIso1d/sol_6-6_32-0.data $1/data/eulerIso1d/sol_6-6_32-0.data

exit $?
