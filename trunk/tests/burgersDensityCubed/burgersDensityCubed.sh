#! /bin/bash

echo $1
echo $2

if [ -e data/burgersDensityCubed ]
then
    rm -rf data/burgersDensityCubed
fi

rsync -c $2/tests/order.hpp $2/order.hpp

make burgersDensityCubed

if [ $? == 2 ]
then
    exit 1
fi
    
$1/burgersDensityCubed $2/tests/burgersDensityCubed/problem_burgersDensityCubed.init

diff -q $2/tests/gold/burgersDensityCubed/final_6-6_4_4.csv $1/data/burgersDensityCubed/final_6-6_4_4.csv

exit $?
