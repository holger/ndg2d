#ifndef boundaryCondition3d_hpp
#define boundaryCondition3d_hpp

#include "mpiFlux3d.hpp"

extern XFluxMPI3d& xFluxMPI;
extern YFluxMPI3d& yFluxMPI;
extern ZFluxMPI3d& zFluxMPI;

class PeriodicBoundaryCondition3dFlux
{
public:
  template<class FluxInfoVecBordArray>
  void operator()(FluxInfoVecBordArray& fluxInfoArrayX, FluxInfoVecBordArray& fluxInfoArrayY, FluxInfoVecBordArray& fluxInfoArrayZ)
  {
    //    std::cerr << "PeriodicBoundaryCondition2dFlux::operator()\n";
 
    xFluxMPI.begin(fluxInfoArrayX);
    yFluxMPI.begin(fluxInfoArrayY);
    zFluxMPI.begin(fluxInfoArrayZ);
    
  } 
};

// class PeriodicBoundaryCondition3dFlux
// {
// public:
//   template<class FluxInfoVecBordArray>
//   void operator()(FluxInfoVecBordArray& fluxInfoArrayX, FluxInfoVecBordArray& fluxInfoArrayY, FluxInfoVecBordArray& fluxInfoArrayZ)
//   {
//     //    std::cerr << "PeriodicBoundaryCondition3dFlux::operator()\n";
//     enum Val { minus, plus };

//     auto dimensions = fluxInfoArrayX.dim();
//     int ncelx = dimensions[0];
//     int ncely = dimensions[1];
//     int ncelz = dimensions[2];
    
//     for(int iy=0; iy<ncely; iy++)
//       for(int iz=0; iz<ncelz; iz++) 
// 	for(int oy=0; oy<order; oy++)
// 	  for(int oz=0; oz<order; oz++){
// 	    fluxInfoArrayX(0, iy, iz, oy, oz)[Val::minus] = fluxInfoArrayX(ncelx-1, iy, iz, oy, oz)[Val::minus];
// 	    fluxInfoArrayX(ncelx-1, iy, iz, oy, oz)[Val::plus] = fluxInfoArrayX(0, iy, iz, oy, oz)[Val::plus];
// 	  }

//     dimensions = fluxInfoArrayY.dim();
//     ncelx = dimensions[0];
//     ncely = dimensions[1];
//     ncelz = dimensions[2];

//     for(int ix=0; ix<ncelx; ix++)
//       for(int iz=0; iz<ncelz; iz++)
// 	for(int ox=0; ox<order; ox++) 
// 	  for(int oz=0; oz<order; oz++) {
// 	    fluxInfoArrayY(ix, 0, iz, ox, oz)[Val::minus] = fluxInfoArrayY(ix, ncely-1, iz, ox, oz)[Val::minus];
// 	    fluxInfoArrayY(ix, ncely-1, iz, ox, oz)[Val::plus] = fluxInfoArrayY(ix, 0, iz, ox, oz)[Val::plus];  
// 	  }
    
//     dimensions = fluxInfoArrayZ.dim();
//     ncelx = dimensions[0];
//     ncely = dimensions[1];
//     ncelz = dimensions[2];

//     for(int ix=0; ix<ncelx; ix++)
//       for(int iy=0; iy<ncely; iy++)
// 	for(int ox=0; ox<order; ox++) 
// 	  for(int oy=0; oy<order; oy++) {
// 	    fluxInfoArrayZ(ix, iy, 0, ox, oy)[Val::minus] = fluxInfoArrayZ(ix, iy, ncelz-1, ox, oy)[Val::minus];
// 	    fluxInfoArrayZ(ix, iy, ncelz-1, ox, oy)[Val::plus] = fluxInfoArrayZ(ix, iy, 0, ox, oy)[Val::plus];
// 	  }

//   }
// };

class PeriodicBoundaryCondition3d
{
public:
  template<class Array>
  void operator()(Array& c)
  {
    int ncelX = c.size(0);
    int ncelY = c.size(1);
    int ncelZ = c.size(2);
    int orderX = c.size(3);
    int orderY = c.size(4);
    int orderZ = c.size(5);
    
    for(int ox=0; ox<orderX; ox++)
      for(int oy=0; oy<orderY; oy++)
        for(int oz=0; oz<orderZ; oz++)	{
	
	  for(int iy=0; iy<ncelY; iy++)
	    for(int iz=0; iz<ncelZ; iz++){
	      c(0, iy,iz, ox, oy, oz) = c(ncelX-2, iy,iz,  ox, oy, oz);
	      c(ncelX-1, iy,iz,  ox, oy, oz) = c(1, iy, iz, ox, oy, oz);
	    }
	
	  for(int ix=0; ix<ncelX; ix++)
	    for(int iz=0; iz<ncelZ; iz++){
	      c(ix, 0, iz, ox, oy,oz) = c(ix, ncelY-2, iz, ox, oy, oz);
	      c(ix, ncelY-1, iz, ox, oy, oz) = c(ix, 1, iz, ox, oy, oz);
	    }
	  for(int ix=0; ix<ncelX; ix++)
	    for(int iy=0; iy<ncelY; iy++){
	      c(ix, iy, 0, ox, oy,oz) = c(ix, iy, ncelZ-2, ox, oy, oz);
	      c(ix, iy, ncelZ-1, ox, oy, oz) = c(ix, iy, 1, ox, oy, oz);
	
	    }
	}
  }
};

#endif
