#ifndef problem_shallowWater1d_hpp
#define problem_shallowWater1d_hpp

#include <BasisKlassen/parameter.hpp>
#include <BasisKlassen/singleton.hpp>
#include <BasisKlassen/assert.hpp>
#include <BasisKlassen/parseFromFile.hpp>

const int varDim = 2;
const int spaceDim = 1;

#include "value1d.hpp"
#include "cfl.hpp"
#include "laxFriedrichs1d.hpp"
#include "notiModify.hpp"
#include "problemBase1d.hpp"

// 1D shallow water model
class Problem_ShallowWater1d : public ProblemBase1d<varDim>
{
public:

  friend class BK::StackSingleton<Problem_ShallowWater1d>;

  static const int fluxInfoNumber = 5;
  
  // f0=h*u, f1=h*u*u + 0.5*g*h*h, df=u +- sqrt(g*h), val0=rho, val1=h*u
  struct FluxVar {
    static const int f0 = 0;
    static const int f1 = 1;
    static const int df = 2;
    static const int val0 = 3;
    static const int val1 = 4;
  };
    
  typedef BK::Array<double, fluxInfoNumber> FluxInfo;  
  typedef BK::Array<FluxInfo, 2> FluxInfoBord;         // [+-][f0, f1, df, val0, val1]
  typedef BK::MultiArray<spaceDim, FluxInfoBord> FluxInfoVecBordArray; // (cell)[+-][f0, f1, df, val0, val1]
  typedef BK::MultiArray<spaceDim, Vector> FluxVecArray; 
  
  Problem_ShallowWater1d() {}

  void init(int ncel, std::string parameterFilename) {
    
    initialized_ = true;
    
    ncel_ = ncel;

    BK::ReadInitFile initFile(parameterFilename,true);
    rkOrder_ = initFile.getParameter<size_t>("rkOrder");
    lx_ = initFile.getParameter<double>("lx");
    int nt = initFile.getParameter<int>("nt");
    double T = initFile.getParameter<double>("T");
    cflFactor_ = initFile.getParameter<double>("cfl");
    g_ = initFile.getParameter<double>("g");
    
    name_ = "shallowWater1d";

    if(nt > 0 && T > 0) {
      std::cerr << "ERROR in Problem_ShallowWater1d::init(): nt = " <<
	nt << " > 0 && T = " << T << " > 0." << std::endl;
      exit(1);						   
    }
    
    cfl_ = cflFactor_*cflLimit[rkOrder_-1][order-1];

    dx_ = lx_/ncel_;
    dt_ = fabs(cfl_*dx_);
    
    if(nt > 0) {
      nt_ = nt;
      T_ = nt_*dt_;
    }
    else {
      T_ = T;
      nt_ = int(T_/dt_) + 1;
    }

    dt_ *= T_/(nt_*dt_);

    logParameter();
    
    std::cerr << "g = " << g_ << std::endl;
  }

  Vector initialCondition(double x) {
    assert(initialized_);
    // double x0 = 0.35;
    // double y0 = 0.35;

    // int kMax = 10;
    // double temp = 0;
    // for(int k=1; k<=kMax; k++)
    //   temp += pow(k,-5./6)*sin(2*M_PI*k/lambda_*(x-shift-2*M_PI/(k*k)));

    // return temp;

    //    double k=1;
    //    return Vector{1., 0.5+sin(2*M_PI*k*(x)/lx_)};
    
    return Vector{1+0.1*exp(-BK::sqr(x-lx_/2)/BK::sqr(0.1)), 0};
  }

  Vector initialConditionShift(double x, double shift = 0) {
    assert(initialized_);
    // double x0 = 0.35;
    // double y0 = 0.35;

    // int kMax = 10;
    // double temp = 0;
    // for(int k=1; k<=kMax; k++)
    //   temp += pow(k,-5./6)*sin(2*M_PI*k/lambda_*(x-shift-2*M_PI/(k*k)));

    // return temp;

    double k=1;
    return Vector{1., sin(2*M_PI*k*(x-shift)/lx_)};
  }

  void lhs(Array& cNew, Array const& c, Array const& rhs) const {
    cNew = c + dt_*rhs;
  }    

  VecVec flux_vector(Vector cellVec) {
    assert(initialized_);

    double h = cellVec[0];
    double hU = cellVec[1];
    double u = hU/h;
    
    return {{hU, hU*u + 0.5*g_*h*h}};
  }

  double dFlux_value(int cell, int index, Array const& c) {
    assert(initialized_);
    
    double h = c(cell, index)[0];
    double hU = c(cell, index)[1];
    double u = hU/h;

    return std::max(fabs(u+sqrt(g_*h)), fabs(u-sqrt(g_*h)));
  }

  void compute_fluxInfoArray(FluxInfoVecBordArray& fluxInfoArray, Array const& c)
  {
    for(size_t cellX=0; cellX<ncel_; cellX++) {
      fluxInfoArray(cellX+1)[Val::minus] = fluxInfo_vectorR(cellX, c);
      fluxInfoArray(cellX)[Val::plus] = fluxInfo_vectorL(cellX, c);
    }
  }

  void compute_fluxArray(FluxVecArray& fluxArray, FluxInfoVecBordArray const& fluxInfoArray, Array const& c)
  {
    for(size_t cellInterface=0; cellInterface<ncel_+1; cellInterface++) {

      Vector fm;
      fm[0] = fluxInfoArray(cellInterface)[Val::minus][FluxVar::f0];
      fm[1] = fluxInfoArray(cellInterface)[Val::minus][FluxVar::f1];

      Vector fp;
      fp[0] = fluxInfoArray(cellInterface)[Val::plus][FluxVar::f0];
      fp[1] = fluxInfoArray(cellInterface)[Val::plus][FluxVar::f1];

      double dfm = fluxInfoArray(cellInterface)[Val::minus][FluxVar::df];
      double dfp = fluxInfoArray(cellInterface)[Val::plus][FluxVar::df];

      Vector valM;
      valM[0] = fluxInfoArray(cellInterface)[Val::minus][FluxVar::val0];
      valM[1] = fluxInfoArray(cellInterface)[Val::minus][FluxVar::val1];

      Vector valP;
      valP[0] = fluxInfoArray(cellInterface)[Val::plus][FluxVar::val0];
      valP[1] = fluxInfoArray(cellInterface)[Val::plus][FluxVar::val1];
      
      fluxArray(cellInterface) =  laxFriedrichs1d_flux_numeric(fm, fp, dfm, dfp, valM, valP);
    }
  }

  FluxInfo fluxInfo_vector(int cell, int index, Array const& c) {
    assert(initialized_);
    
    FluxInfo fluxInfoVec;
    VecVec fluxVec = flux_vector(c(cell, index));
    fluxInfoVec[FluxVar::f0] = fluxVec[0][0];
    fluxInfoVec[FluxVar::f1] = fluxVec[0][1];
    fluxInfoVec[FluxVar::df] = dFlux_value(cell, index, c);
    fluxInfoVec[FluxVar::val0] = c(cell, index)[0];
    fluxInfoVec[FluxVar::val1] = c(cell, index)[1];
    
    return fluxInfoVec;
  }

  FluxInfo fluxInfo_vectorR(int cell, Array const& c) {
    assert(initialized_);

    return fluxInfo_vector(cell, order-1, c);
  }

  FluxInfo fluxInfo_vectorL(int cell, Array const& c) {
    assert(initialized_);

    return fluxInfo_vector(cell, 0, c);
  }

  CONSTPARA(double, g);
};

typedef Problem_ShallowWater1d Problem;
extern Problem& problem;

#endif
