#ifndef problemBase3d_hpp
#define problemBase3d_hpp

#include <BasisKlassen/filesystem.hpp>

#include "laxFriedrichs1d.hpp"
#include "centralFlux1d.hpp"
#include "lagrange.hpp"

template<int varDim>
class ProblemBase3d : public DataTypes<3, varDim>
{
public:

  struct Dir {
    static const int x = 0;
    static const int y = 1;
    static const int z = 2;
  };

  ProblemBase3d() {
    initialized_ = false;
    
    varDim_ = varDim;
  }

  void init(int ncelx, int ncely, int ncelz, std::string const& parameterFilename) {
    LOGL(1,"ProblemBase3d::init(int ncelx, int ncely, int ncely, int ncelz, std::string const& parameterFilename)");
    
    ncelx_ = ncelx;
    ncely_ = ncely;
    ncelz_ = ncelz;

    parameterFilename_ = parameterFilename;
    readBaseParameterFromFile(parameterFilename);

    dx_ = lx_/ncelx;
    dy_ = ly_/ncely;
    dz_ = lz_/ncelz;

    ASSERT((dx_ == dy_) && (dy_ == dz_) , BK::toString("dx_ = ", dx_, "; dy_ = ", dy_,"; dz_ = ", dz_));
	
    cfl_ = cflFactor_*cflLimit[rkOrder_-1][order-1];

    lagrangeMatrix_.init();
    dLagrangeMatrix_.init();
    
    // if(!initialized_)
    //   notifyField.add("field", FieldNotifyCallBack(std::bind(&ProblemBase3d::write,this,std::placeholders::_1), "ProblemBase3d::write")); !!!

    LOGL(4,"ProblemBase2d::init(int ncelx, int ncely, int ncelz, std::string const& parameterFilename)-end");
  }

  void readBaseParameterFromFile(std::string parameterFilename) {
    LOGL(1,"ProblemBase3d::readBaseParameterFromFile(std::string parameterFilename)");
    
    BK::ReadInitFile initFile(parameterFilename,true);
    std::string pathToBaseInitFile = initFile.getParameter<std::string>("problemBase3dInitFilePath");

    BK::path path(parameterFilename);
    BK::path branch_path = path.branch_path();
    //    std::cout << "branch_path = " << branch_path.string() << std::endl;
    std::string parameterBaseFilename = branch_path.string() + "/problemBase3d.init";
    
    BK::ReadInitFile initBaseFile(parameterBaseFilename,true);
    rkOrder_ = initBaseFile.getParameter<size_t>("rkOrder");
    lx_ = initBaseFile.getParameter<double>("lx");
    ly_ = initBaseFile.getParameter<double>("ly");
    lz_ = initBaseFile.getParameter<double>("lz");
    T_ = initBaseFile.getParameter<double>("T");
    nt_ = initBaseFile.getParameter<double>("nt");
    cflFactor_ = initBaseFile.getParameter<double>("cfl");
    diagOutputInterval_ = initBaseFile.getParameter<double>("diagOutputInterval");
    initBaseFile.getList("outputFormat",outputFormat_);
    writeTimeInterval_ = initBaseFile.getParameter<double>("writeTimeInterval");
    writeStepInterval_ = initBaseFile.getParameter<int>("writeStepInterval");
    if(!initBaseFile.parameterExists("bcX")) bcX_ = 0;
    else bcX_ = initBaseFile.getParameter<int>("bcX");
    if(!initBaseFile.parameterExists("bcY")) bcY_ = 0;
    else bcY_ = initBaseFile.getParameter<int>("bcY");
    if(!initBaseFile.parameterExists("bcZ")) bcZ_ = 0;
    else bcZ_ = initBaseFile.getParameter<int>("bcZ");

    LOGL(4,"ProblemBase3d::readBaseParameterFromFile(std::string parameterFilename)-end");
  }

  void logParameter() {
    LOGL(1,PAR(name_));
    LOGL(1,PAR(parameterFilename_));
    LOGL(1,PAR(ncelx_));
    LOGL(1,PAR(ncely_));
    LOGL(1,PAR(ncelz_));
    LOGL(1,PAR(lx_));
    LOGL(1,PAR(ly_));
    LOGL(1,PAR(lz_));
    LOGL(1,PAR(T_));
    LOGL(1,PAR(cfl_));
    LOGL(1,PAR(cflFactor_));
    LOGL(1,PAR(dt_));
    LOGL(1,PAR(nt_));
    LOGL(1,PAR(dx_));
    LOGL(1,PAR(dy_));
    LOGL(1,PAR(dz_));
    LOGL(1,PAR(dt_));
    LOGL(1,PAR(varDim_));
    LOGL(1,PAR(order));
    LOGL(1,PAR(get_stopStep()));
    LOGL(1,PAR(diagOutputInterval_));
    LOGL(1,PAR(bcX_));
    LOGL(1,PAR(bcY_));
    LOGL(1,PAR(bcZ_));
  }

  int get_stopStep() {
    return startStep_ + nt_;
  }

  template<typename Array, typename FluxInfoBordArray>
  void fillFluxInfoBordArray(int cellXf, int cellYf, int cellZf, int index0, int index1, int plusMinus,
			     Array const& c, FluxInfoBordArray& fluxInfoBordArray, int cellX, int cellY, int cellZ, int indexX, int indexY, int indexZ) const {

    auto data = c(cellX, cellY, cellZ, indexX, indexY, indexZ);
    for(int var=0;var<varDim;var++)
      fluxInfoBordArray(cellXf, cellYf, cellZf, index0, index1)[plusMinus][var] = data[var];

  }

  template<typename FluxArray, typename FluxInfoBordArray, typename Array, typename Problem>
  void computeFluxes(FluxArray& fluxArray,
		       FluxInfoBordArray& fluxInfoBordArrayX,
		       FluxInfoBordArray& fluxInfoBordArrayY,
		       FluxInfoBordArray& fluxInfoBordArrayZ,
		     Array const& c, Problem const& problem) const {

      LOGL(3,"Problem_Advection3d::computeFluxes");
      
      for(int cellX=0; cellX<int(c.size(0)); cellX++)
	for(int cellY=0; cellY<int(c.size(1)); cellY++)
	  for(int cellZ=0; cellZ<int(c.size(2)); cellZ++)
	    for(int indexX=0; indexX<int(c.size(3)); indexX++)
	      for(int indexY=0; indexY<int(c.size(4)); indexY++) 
		for(int indexZ=0; indexZ<int(c.size(5)); indexZ++) {
		  auto flux = problem.flux_vector(c(cellX, cellY, cellZ, indexX, indexY, indexZ));
		  fluxArray(cellX, cellY, cellZ, indexX, indexY, indexZ) = flux;

		  if(indexX == 0)
		    fillFluxInfoBordArray(cellX, cellY, cellZ, indexY, indexZ, Val::plus, c, fluxInfoBordArrayX, cellX, cellY, cellZ, indexX, indexY, indexZ);
		  if(indexX == order-1) 
		    fillFluxInfoBordArray(cellX+1, cellY, cellZ, indexY, indexZ, Val::minus, c, fluxInfoBordArrayX, cellX, cellY, cellZ, indexX, indexY, indexZ);
		  if(indexY == 0) 
		    fillFluxInfoBordArray(cellX, cellY, cellZ, indexX, indexZ, Val::plus, c, fluxInfoBordArrayY, cellX, cellY, cellZ, indexX, indexY, indexZ);
		  if(indexY == order-1) 
		    fillFluxInfoBordArray(cellX, cellY+1, cellZ, indexX, indexZ, Val::minus, c, fluxInfoBordArrayY, cellX, cellY, cellZ, indexX, indexY, indexZ);
		  if(indexZ == 0) 
		    fillFluxInfoBordArray(cellX, cellY, cellZ, indexX, indexY, Val::plus, c, fluxInfoBordArrayZ, cellX, cellY, cellZ, indexX, indexY, indexZ);
		  if(indexZ == order-1) 
		    fillFluxInfoBordArray(cellX, cellY, cellZ+1, indexX, indexY, Val::minus, c, fluxInfoBordArrayZ, cellX, cellY, cellZ, indexX, indexY, indexZ);
		}
      
      LOGL(4,"Problem_Advection3d::computeFluxes-end");
  }
  
  template<typename FluxVecArray, typename FluxInfoBordArray, typename FluxFunc, typename DFluxFunc>
  void computeNumericalFlux_LF(FluxVecArray& fluxVecArrayX, FluxVecArray& fluxVecArrayY, FluxVecArray& fluxVecArrayZ,
			       FluxInfoBordArray& fluxInfoArrayX, FluxInfoBordArray& fluxInfoArrayY, FluxInfoBordArray& fluxInfoArrayZ, FluxFunc fluxFunc, DFluxFunc dFluxFunc) {
    
    LOGL(3,"ProblemBase3d::computeNumericalFlux_LF");
    
    for(size_t cellX=0; cellX<fluxVecArrayX.size(0); cellX++)
      for(size_t cellY=0; cellY<fluxVecArrayX.size(1); cellY++)
	  for(size_t cellZ=0; cellZ<fluxVecArrayX.size(2); cellZ++)
	    for(size_t indexY=0; indexY<order; indexY++) 
	      for(size_t indexZ=0; indexZ<order; indexZ++) {

		auto valM = fluxInfoArrayX(cellX, cellY, cellZ, indexY, indexZ)[Val::minus];   // Vector valM
		auto fm = fluxFunc(valM);	  // VecVec fm
		auto valP = fluxInfoArrayX(cellX, cellY, cellZ, indexY, indexZ)[Val::plus];
		auto fp = fluxFunc(valP);
		double dfm = dFluxFunc(valM)[Dir::x];
		double dfp = dFluxFunc(valP)[Dir::x];

		fluxVecArrayX(cellX, cellY, cellZ, indexY, indexZ) =  laxFriedrichs1d_flux_numeric(fm[Dir::x], fp[Dir::x], dfm, dfp, valM, valP);
	      }

    for(size_t cellX=0; cellX<fluxVecArrayY.size(0); cellX++)
	for(size_t cellY=0; cellY<fluxVecArrayY.size(1); cellY++)
	  for(size_t cellZ=0; cellZ<fluxVecArrayY.size(2); cellZ++)
	    for(size_t indexX=0; indexX<order; indexX++) 
	      for(size_t indexZ=0; indexZ<order; indexZ++) {

		auto valM = fluxInfoArrayY(cellX, cellY, cellZ, indexX, indexZ)[Val::minus];   // Vector valM
		auto fm = fluxFunc(valM);	  // VecVec fm
		auto valP = fluxInfoArrayY(cellX, cellY, cellZ, indexX, indexZ)[Val::plus];
		auto fp = fluxFunc(valP);
		double dfm = dFluxFunc(valM)[Dir::y];
		double dfp = dFluxFunc(valP)[Dir::y];

		fluxVecArrayY(cellX, cellY, cellZ, indexX, indexZ) =  laxFriedrichs1d_flux_numeric(fm[Dir::y], fp[Dir::y], dfm, dfp, valM, valP);
	      }

      for(size_t cellX=0; cellX<fluxVecArrayZ.size(0); cellX++)
	for(size_t cellY=0; cellY<fluxVecArrayZ.size(1); cellY++)
	  for(size_t cellZ=0; cellZ<fluxVecArrayZ.size(2); cellZ++)
	    for(size_t indexX=0; indexX<order; indexX++) 
	      for(size_t indexY=0; indexY<order; indexY++) {

		auto valM = fluxInfoArrayZ(cellX, cellY, cellZ, indexX, indexY)[Val::minus];   // Vector valM
		auto fm = fluxFunc(valM);	  // VecVec fm
		auto valP = fluxInfoArrayZ(cellX, cellY, cellZ, indexX, indexY)[Val::plus];
		auto fp = fluxFunc(valP);
		double dfm = dFluxFunc(valM)[Dir::z];
		double dfp = dFluxFunc(valP)[Dir::z];

		fluxVecArrayZ(cellX, cellY, cellZ, indexX, indexY) =  laxFriedrichs1d_flux_numeric(fm[Dir::z], fp[Dir::z], dfm, dfp, valM, valP);
	      }

      LOGL(4,"ProblemBase3d::computeNumericalFlux_LF-end");
    }

  template<typename FluxVecArray, typename FluxInfoBordArray, typename FluxFunc>
  void computeNumericalFlux_Central(FluxVecArray& fluxVecArrayX, FluxVecArray& fluxVecArrayY, FluxVecArray& fluxVecArrayZ,
			       FluxInfoBordArray& fluxInfoArrayX, FluxInfoBordArray& fluxInfoArrayY, FluxInfoBordArray& fluxInfoArrayZ, FluxFunc fluxFunc) {
    
    LOGL(3,"ProblemBase3d::computeNumericalFlux_Central");
    
    for(size_t cellX=0; cellX<fluxVecArrayX.size(0); cellX++)
      for(size_t cellY=0; cellY<fluxVecArrayX.size(1); cellY++)
	for(size_t cellZ=0; cellZ<fluxVecArrayX.size(2); cellZ++)
	  for(size_t indexY=0; indexY<order; indexY++) 
	    for(size_t indexZ=0; indexZ<order; indexZ++) {
	      
		auto valM = fluxInfoArrayX(cellX, cellY, cellZ, indexY, indexZ)[Val::minus];   // Vector valM
		auto fm = fluxFunc(valM);	  // VecVec fm
		auto valP = fluxInfoArrayX(cellX, cellY, cellZ, indexY, indexZ)[Val::plus];
		auto fp = fluxFunc(valP);

		fluxVecArrayX(cellX, cellY, cellZ, indexY, indexZ) = central1d_flux_numeric(fm[Dir::x], fp[Dir::x]);
	      }

    for(size_t cellX=0; cellX<fluxVecArrayY.size(0); cellX++)
	for(size_t cellY=0; cellY<fluxVecArrayY.size(1); cellY++)
	  for(size_t cellZ=0; cellZ<fluxVecArrayY.size(2); cellZ++)
	    for(size_t indexX=0; indexX<order; indexX++) 
	      for(size_t indexZ=0; indexZ<order; indexZ++) {

		auto valM = fluxInfoArrayY(cellX, cellY, cellZ, indexX, indexZ)[Val::minus];   // Vector valM
		auto fm = fluxFunc(valM);	  // VecVec fm
		auto valP = fluxInfoArrayY(cellX, cellY, cellZ, indexX, indexZ)[Val::plus];
		auto fp = fluxFunc(valP);

		fluxVecArrayY(cellX, cellY, cellZ, indexX, indexZ) = central1d_flux_numeric(fm[Dir::y], fp[Dir::y]);
	      }

      for(size_t cellX=0; cellX<fluxVecArrayZ.size(0); cellX++)
	for(size_t cellY=0; cellY<fluxVecArrayZ.size(1); cellY++)
	  for(size_t cellZ=0; cellZ<fluxVecArrayZ.size(2); cellZ++)
	    for(size_t indexX=0; indexX<order; indexX++) 
	      for(size_t indexY=0; indexY<order; indexY++) {

		auto valM = fluxInfoArrayZ(cellX, cellY, cellZ, indexX, indexY)[Val::minus];   // Vector valM
		auto fm = fluxFunc(valM);	  // VecVec fm
		auto valP = fluxInfoArrayZ(cellX, cellY, cellZ, indexX, indexY)[Val::plus];
		auto fp = fluxFunc(valP);

		fluxVecArrayZ(cellX, cellY, cellZ, indexX, indexY) = central1d_flux_numeric(fm[Dir::z], fp[Dir::z]);
	      }

      LOGL(4,"ProblemBase3d::computeNumericalFlux_Central-end");
  }


  PROTEPARA(std::string, parameterFilename);
  PROTEPARA(std::string, name);
  PROTEPARA(size_t, ncelx);
  PROTEPARA(size_t, ncely);
  PROTEPARA(size_t, ncelz);
  PROTEPARA(double, lx);
  PROTEPARA(double, ly);
  PROTEPARA(double, lz);
  PROTEPARA(double, dx);
  PROTEPARA(double, dy);
  PROTEPARA(double, dz);
  PROTEPARA(double, T);
  PROTEPARA(double, dt);
  PROTEPARA(double, cfl);
  PROTEPARA(double, cflFactor);
  PROTEPARA(int, nt);
  PROTEPARA(bool, initialized);
  PROTEPARA(int, varDim);
  PROTEPARA(int, rkOrder);
  PROTEPARA(int, step);
  PROTEPARA(int, startStep);
  PROTEPARA(double, time);
  PROTEPARA(int, diagOutputInterval);
  PROTEPARA(BK::Vector<int>, outputFormat);
  PROTEPARA(double, writeTimeInterval);
  PROTEPARA(int, writeStepInterval);
  CONSTPARA(int, bcX);
  CONSTPARA(int, bcY);
  CONSTPARA(int, bcZ);

  DLagrangeMatrix<order> dLagrangeMatrix_;
  LagrangeMatrix<order> lagrangeMatrix_;
};

#endif
