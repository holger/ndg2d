#ifndef writeArray3d_netcdf_hpp
#define writeArray3d_netcdf_hpp

inline void check(bool result) {
  assert(!result);
}

template<class Array>
void writeNetCDF(const Array& array, std::string filename, size_t nxGlobal, size_t nyGlobal, size_t nzGlobal, double dx, double dy, double dz)
{
  const int varDim = Array::value_type::N_length;

  std::cout << mpiBase.get_commRank() << ": varDim = " << varDim << std::endl;
  
  BK::Array<int, 6> dims = array.dim();
  size_t nx = dims[0]*dims[3];
  size_t ny = dims[1]*dims[4];
  size_t nz = dims[2]*dims[5];

  std::cout << mpiBase.get_commRank() << ": nx = " << nx << "; ny = " << ny << "; nz = " << nz <<std::endl;
  
  unsigned int startXcell = mpiBase.procCoord()[0]*dims[0];
  unsigned int startYcell = mpiBase.procCoord()[1]*dims[1];
  unsigned int startZcell = mpiBase.procCoord()[2]*dims[2];

  std::cout << mpiBase.get_commRank() << ": startXcell = " << startXcell << "; startYcell = " << startYcell << "; startZcell = " << startZcell << std::endl;

  filename += ".nc";
  
  MPI_Info info = MPI_INFO_NULL;
  int ncid;

  check(nc_create_par(filename.c_str(), NC_NETCDF4|NC_MPIIO, MPI_COMM_WORLD, 
    		      info, &ncid));
  //  res = nc_create(filename.c_str(), NC_NETCDF4, &ncid);
  
  int dimids[3];
  check(nc_def_dim(ncid, "d1", nxGlobal*order, &dimids[0]));
  check(nc_def_dim(ncid, "d2", nyGlobal*order, &dimids[1]));
  check(nc_def_dim(ncid, "d3", nzGlobal*order, &dimids[2]));

  //  BK::Array<int, Array::value_type::N_length*2+2> varVec;
  BK::Array<int, varDim*2+3> varVec;
  std::cout << "varVec.size() = " << varVec.size() << std::endl;

  check(nc_def_var(ncid, "d1", NC_DOUBLE, 1, &dimids[0], &varVec[0]));
  check(nc_def_var(ncid, "d2", NC_DOUBLE, 1, &dimids[1], &varVec[1]));
  check(nc_def_var(ncid, "d3", NC_DOUBLE, 1, &dimids[2], &varVec[2]));

  std::cout << "varDim = " << varDim << std::endl;
  for(int i=0; i<varDim; i++) {
    check(nc_def_var(ncid, BK::toString("varDump-",i).c_str(), NC_DOUBLE, 3, dimids, &varVec[i+3]));
    check(nc_var_par_access(ncid, varVec[i+3], NC_COLLECTIVE));
  }
  
  for(int i=0; i<varDim; i++) {
    check(nc_def_var(ncid, BK::toString("var-",i).c_str(), NC_DOUBLE, 3, dimids, &varVec[i+3+varDim]));
    check(nc_var_par_access(ncid, varVec[i+3+varDim], NC_COLLECTIVE));
  }
  
  check(nc_enddef(ncid));

  check(nc_put_att_int(ncid, NC_GLOBAL, "ncelx", NC_INT, 1, &dims[0]));
  check(nc_put_att_int(ncid, NC_GLOBAL, "ncely", NC_INT, 1, &dims[1]));
  check(nc_put_att_int(ncid, NC_GLOBAL, "ncelz", NC_INT, 1, &dims[2]));
  check(nc_put_att_uint(ncid, NC_GLOBAL, "startXcell", NC_UINT, 1, &startXcell));
  check(nc_put_att_uint(ncid, NC_GLOBAL, "startYcell", NC_UINT, 1, &startYcell));
  check(nc_put_att_uint(ncid, NC_GLOBAL, "startZcell", NC_UINT, 1, &startZcell));
  check(nc_put_att_int(ncid, NC_GLOBAL, "order", NC_INT, 1, &order));

  size_t startXid = mpiBase.procCoord()[0]*nx;
  size_t startYid = mpiBase.procCoord()[1]*ny;
  size_t startZid = mpiBase.procCoord()[2]*nz;

  size_t start[3];
  size_t count[3];
  start[0] = startXid;
  start[1] = startYid;
  start[2] = startZid;
  count[0] = nx;
  count[1] = ny;
  count[2] = nz;

  BK::MultiArray<3,double> data(nx,ny,nz);

  BK::Vector<double> xi = points[order-1];
  
  for(size_t ix=0; ix<array.dim()[0]; ix++)
    for(size_t iy=0; iy<array.dim()[1]; iy++)
      for(size_t iz=0; iz<array.dim()[2]; iz++) 
	for(size_t ox=0; ox<order; ox++)
	  for(size_t oy=0; oy<order; oy++)
	    for(size_t oz=0; oz<order; oz++) 
	      data(ix*order + ox, iy*order + oy, iz*order + oz) = startXcell*dx + dx/2 + ix*dx + xi[ox]*dx/2;


  BK::Vector<double> dataCoord(nx);
  for(size_t ix=0; ix<array.dim()[0]; ix++)
    for(size_t ox=0; ox<order; ox++)
      dataCoord[ix*order + ox] = startXcell*dx + dx/2 + ix*dx + xi[ox]*dx/2;
  
  check(nc_put_vara_double(ncid, varVec[0], start, &nx, 
  			   dataCoord.data()));

  for(size_t iy=0; iy<array.dim()[1]; iy++)
    for(size_t oy=0; oy<order; oy++)
      dataCoord[iy*order + oy] = startYcell*dy + dy/2 + iy*dy + xi[oy]*dy/2;
  
  check(nc_put_vara_double(ncid, varVec[1], start, &ny, 
  			   dataCoord.data()));

  for(size_t iz=0; iz<array.dim()[2]; iz++)
    for(size_t oz=0; oz<order; oz++)
      dataCoord[iz*order + oz] = startYcell*dz + dz/2 + iz*dz + xi[oz]*dz/2;
  
  check(nc_put_vara_double(ncid, varVec[2], start, &nz, 
  			   dataCoord.data()));
  
  for(int var=0; var<varDim; var++) {
    for(size_t ix=0; ix<array.dim()[0]; ix++)
      for(size_t iy=0; iy<array.dim()[1]; iy++)
  	for(size_t iz=0; iz<array.dim()[2]; iz++) 
  	  for(size_t ox=0; ox<order; ox++)
  	    for(size_t oy=0; oy<order; oy++)
  	      for(size_t oz=0; oz<order; oz++) 
  		data(ix*order + ox, iy*order + oy, iz*order + oz) = array(ix, iy, iz, ox, oy, oz)[var];
    
    check(nc_put_vara_double(ncid, varVec[3+var], start, count, 
     			     data.data()));
  }

  for(int var = 0; var < varDim; var++) {
    for(size_t ix=0; ix<array.dim()[0]; ix++)
      for(size_t iy=0; iy<array.dim()[1]; iy++)
  	for(size_t iz=0; iz<array.dim()[2]; iz++) 
  	  for(size_t jx=0; jx<order; jx++)
  	    for(size_t jy=0; jy<order; jy++)
  	      for(size_t jz=0; jz<order; jz++)
  		data(ix*order + jx, iy*order + jy, iz*order + jz) = value(ix, iy, iz, -1 + jx*2./order, -1 + jy*2./order, -1 + jz*2./order, array)[var];
    
    check(nc_put_vara_double(ncid, varVec[3+varDim+var], start, count, 
			     data.data()));
  }
  
  check(nc_close(ncid));
}

#endif
