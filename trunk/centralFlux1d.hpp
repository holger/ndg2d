#ifndef centralFlux1d_hpp
#define centralFlux1d_hpp

template<class Vec>
Vec central1d_flux_numeric(Vec fluxL, Vec fluxR)
{
  Vec fr = 0.5*(fluxL + fluxR);
  
  return fr;
}

#endif
