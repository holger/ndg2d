#ifndef problem_advectionSource1d_hpp
#define problem_advectionSource1d_hpp

#include <BasisKlassen/parameter.hpp>
#include <BasisKlassen/singleton.hpp>
#include <BasisKlassen/assert.hpp>
#include <BasisKlassen/parseFromFile.hpp>

const int varDim = 2;    // two variables: rho and rho*u
const int spaceDim = 1;  // one space dimension

#include "cfl.hpp"
#include "laxFriedrichs1d.hpp"
#include "notiModify.hpp"
#include "problemBase1d.hpp"

// #undef LOGLEVEL
// #define LOGLEVEL 3

// Burgers equation with (passiv) density in 1D
class Problem_AdvectionSource1d : public ProblemBase1d<varDim>
{
public:

  friend class BK::StackSingleton<Problem_AdvectionSource1d>;

  struct Var {
    static const int rho = 0;    // rho
    static const int rhoU = 1;   // rho*u
  };

  Problem_AdvectionSource1d() {}

  void init(int ncel, std::string parameterFilename) {

    if(!initialized_) {
      notifyField.add("fieldFinal", FieldNotifyCallBack(std::bind(&Problem_AdvectionSource1d::errorComp,this,std::placeholders::_1), "Problem_AdvectionSource1d::errorComp"));
      // notifyField.add("fieldInit", FieldNotifyCallBack(std::bind(&Problem_AdvectionSource1d::computeDerive,this,std::placeholders::_1), "Problem_AdvectionSource1d::computeDerive"));
    }
    
    name_ = "advectionSource1d";

    ProblemBase1d::init(ncel, parameterFilename);

    // computing dt
    dt_ = fabs(cfl_*dx_);
    if(nt_ > 0) {
      T_ = nt_*dt_;
    }
    else {
      nt_ = int(T_/dt_) + 1;
    }
    dt_ *= T_/(nt_*dt_);

    initialized_ = true;
    
    velArray.resize(ncel_, order);
    dvelArray.resize(ncel_, order);

    std::ofstream out(BK::toString(dirName_,"/velocity.txt"));

    BK::Vector<double> xi = points[order-1];
    for(size_t cell=0; cell < ncel_; cell++) 
      for(size_t index=0; index < order; index++) {
	auto transX = [cell, this](double x) { return cell*dx_+0.5*dx_+x*0.5*dx_;};
	velArray(cell, index) = velocity(transX(xi[index]));
	dvelArray(cell, index) = dvelocity(transX(xi[index]));
	out << transX(xi[index]) << "\t" << velArray(cell, index) << "\t" << dvelArray(cell, index) << std::endl;
      }

    out.close();
    
    logParameter();
  }

  void errorComp(Array const* c) {
    
    Array cRef(ncel_,order);

    std::function<Vector(double)> initialFunction = std::bind( &Problem_AdvectionSource1d::initialCondition, *this, std::placeholders::_1);
    initialize(cRef, initialFunction);

     // writeFine(cRef, dirName + "/ref_" +  std::to_string(order) + "-" + std::to_string(rkOrder) +"_" + std::to_string(ncelx), nPoParCel);
    
    double error = 0;
    size_t nx = c->size(0);
    for(size_t ix = 0; ix < nx; ix++) {
      error += BK::sqr(value(ix, 0.,*c)[Var::rho]/value(ix, 0.,*c)[Var::rhoU] - value(ix, 0., cRef)[Var::rho]/value(ix, 0., cRef)[Var::rhoU])*dx_;
    }
    error = sqrt(error);

    std::cerr << "writing error to " << BK::toString(dirName_ + "/error-ndg-",order,"-",rkOrder_,".txt") << std::endl;
    std::ofstream outError(BK::toString(dirName_ + "/error-ndg-",order,"-",rkOrder_,".txt"), std::ios::app);
    outError << ncel_ << "\t" << error << std::endl;
    outError.close();
  }
  
  Vector initialCondition(double x) {
    assert(initialized_);

    return initialConditionShift(x);
  }

  Vector initialConditionShift(double x, double shift = 0) {
    assert(initialized_);
    // double x0 = 0.35;
    // double y0 = 0.35;

    // int kMax = 10;
    // double temp = 0;
    // for(int k=1; k<=kMax; k++)
    //   temp += pow(k,-5./6)*sin(2*M_PI*k/lambda_*(x-shift-2*M_PI/(k*k)));

    // return temp;

    //    double k=1;
    return Vector{rho(x),rho(x)*velocity(x)};
  }

  double rho(double x) {
    //return 1;
    return 3+sin(2*M_PI*x/lx_);
  } 

  double velocity(double x) {
    return sin(2*M_PI*x/lx_);
    //return 1;
  } 

  double dvelocity(double x) {
    return 2*M_PI/lx_*cos(2*M_PI*x/lx_);
    //    return 0;
  }

  // Vector dInitialCondition(double x) {
  //   assert(initialized_);

  //   return Vector{dvelocity(x),0};
  // }

  // void computeDerive(Array const* c) {
  //   Array dc(ncel_, order);


  //   std::function<Vector(double)> initialFunction = std::bind( &Problem_AdvectionSource1d::initialCondition, *this, std::placeholders::_1);

  //   Array cRef(ncel_,order);
  //   initialize(cRef, initialFunction);
    
  //   auto uFunc = [](Array const& c, int cell, int index) {
  //     double rhoU = c(cell, index)[Var::rhoU];
  //     double rho = c(cell, index)[Var::rho];
  //     double u = rhoU/rho;
  //     return u;
  //   };
    
  //   for(int cell=0; cell<int(c->size(0)); cell++)
  //     for(int index=0; index<int(c->size(1)); index++) {

  // 	double dvel = derivative(uFunc, *c, cell, index);

  // 	double rhoU = cRef(cell, index)[Var::rhoU];
  // 	double rho = cRef(cell, index)[Var::rho];
  // 	double u = rhoU/rho;
	
  // 	dc(cell, index)[0] = dvel;
  // 	dc(cell, index)[1] = uFunc(*c, cell, index)-u;
  //     }
    
  //   writeFine(dc, dirName_ + "/dc_" +  std::to_string(order) + "-" + std::to_string(rkOrder_) +"_" + std::to_string(ncel_) + "-" + std::to_string(step_), 10,dx_);


  //   Array dref(ncel_, order);
  //   std::function<Vector(double)> dRefFunc = std::bind( &Problem_AdvectionSource1d::dInitialCondition, *this, std::placeholders::_1);
  //   initialize(dref, dRefFunc);
    
  //   double error = 0;
  //   for(size_t ix = 0; ix < ncel_; ix++) {
  //     error += BK::sqr(dc(ix,5)[0] - dref(ix,5)[0])*dx_;
  //   }
  //   error = sqrt(error);

  //   std::cerr << "writing error to " << BK::toString(dirName_ + "/error-ndg-",order,"-",rkOrder_,".txt") << std::endl;
  //   std::ofstream outError(BK::toString(dirName_ + "/errorD-ndg-",order,"-",rkOrder_,".txt"), std::ios::app);
  //   outError << ncel_ << "\t" << error << std::endl;
  //   outError.close();
    
  //   //    std::ofstream out("dc.txt");
    
  // }
  
  void computeSourceTerm(Array const& c, Array & s) const {
    LOGL(3,"Problem_AdvectionSource1d::computeSourceTerm");

    auto uFunc = [](Array const& c, int cell, int index) {
      double rhoU = c(cell, index)[Var::rhoU];
      double rho = c(cell, index)[Var::rho];
      double u = rhoU/rho;
      return u;
    };

    auto u2Func = [](Array const& c, int cell, int index) {
      double rhoU = c(cell, index)[Var::rhoU];
      double rho = c(cell, index)[Var::rho];
      double u = rhoU/rho;
      return u*u;
    };

    for(int cell=0; cell<int(c.size(0)); cell++)
      for(int index=0; index<int(c.size(1)); index++) {

	double dvel = derivative(uFunc, c, cell, index);

	double rho = c(cell, index)[Var::rho];
	//	double rhoU = c(cell, index)[Var::rhoU];

	double dvel2 = derivative(u2Func, c, cell, index);
	
	s(cell, index)[0] = rho * dvel;
	//s(cell, index)[1] = 2*rhoU * dvel;
	s(cell, index)[1] = rho * dvel2;
      }

    LOGL(4,"Problem_AdvectionSource1d::computeSourceTerm-end");
  }

  void lhs(Array& cNew, Array const& c, Array const& rhs) const {
    cNew = c + dt_*rhs;
  }    

  VecVec flux_vector(Vector cellVec, size_t cell = 0, size_t index  = 0) {
    assert(initialized_);

    double rho = cellVec[0];
    double rhoU = cellVec[1];
    double u = rhoU/rho;
    
    return VecVec{Vector{rhoU, rhoU*u}};
  }

  double dFlux_value(Vector cellVec, size_t cell = 0, size_t index  = 0) {
    assert(initialized_);
    
    double rho = cellVec[Var::rho];
    double rhoU = cellVec[Var::rhoU];
    double u = rhoU/rho;

    return u;
  }

  void compute_fluxArray(FluxVecArray& fluxArray, FluxInfoBordArray const& fluxInfoArray)
  {
      auto fluxFunc = [this](Vector const& varArray, size_t cell, size_t index) { return this->flux_vector(varArray, cell, index);};
      auto dfluxFunc = [this](Vector const& varArray, size_t cell, size_t index) { return this->dFlux_value(varArray, cell, index);};
      compute_fluxArray_LF(fluxArray, fluxInfoArray, fluxFunc, dfluxFunc);
  }

  BK::MultiArray<spaceDim*2, double> velArray;
  BK::MultiArray<spaceDim*2, double> dvelArray;
  };

typedef Problem_AdvectionSource1d Problem;
extern Problem& problem;

#endif
