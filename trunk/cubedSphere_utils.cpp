#include <functional>
#include <BasisKlassen/array.hpp>

#include "problem2d.hpp"
#include "cubedSphere_utils.hpp"

BK::Array<std::function<BK::Array<double, 3>(double, double)>, 6> ab2xyz({ab2xyz_P1, ab2xyz_P2, ab2xyz_P3, ab2xyz_P4, ab2xyz_P5, ab2xyz_P6});
BK::Array<std::function<BK::Array<double, 2>(double x, double y, double z)>, 6> xyz2ab({xyz2ab_P1, xyz2ab_P2, xyz2ab_P3, xyz2ab_P4, xyz2ab_P5, xyz2ab_P6});

BK::Array<std::function<BK::Array<double, 2>(double, double)>, 6> ab2pt({ab2pt_P1, ab2pt_P2, ab2pt_P3, ab2pt_P4, ab2pt_P5, ab2pt_P6});

BK::Array<std::function<void(BK::Array<double,3>&, double, double, double, double)>, 6> d_parametrisation({d_parametrisation_P1,
													   d_parametrisation_P2,
													   d_parametrisation_P3,
													   d_parametrisation_P4,
													   d_parametrisation_P5,
													   d_parametrisation_P6});

BK::Array<std::function<void(BK::Array<double,3>, double&, double&, BK::Array<double,3>)>, 6> d_map({d_map_P1, d_map_P2, d_map_P3, d_map_P4, d_map_P5, d_map_P6});

BK::Array<std::function<Mat2x3(double, double)>, 6> get_d_map({get_d_map_P1, get_d_map_P2, get_d_map_P3, get_d_map_P4, get_d_map_P5, get_d_map_P6});

BK::Array<std::function<Mat3x2(double, double)>, 6> get_d_para({get_d_para_P1, get_d_para_P2, get_d_para_P3, get_d_para_P4, get_d_para_P5, get_d_para_P6});

