#ifndef problem_burgers1d_hpp
#define problem_burgers1d_hpp

#include <BasisKlassen/parameter.hpp>
#include <BasisKlassen/singleton.hpp>
#include <BasisKlassen/assert.hpp>

const int varDim = 1;
const int spaceDim = 1;

#include "cfl.hpp"
#include "laxFriedrichs1d.hpp"
#include "notiModify.hpp"
#include "problemBase1d.hpp"

// Burgers 1D
class Problem_Burgers1d : public ProblemBase1d<varDim>
{
public:

  enum Var {c};
  
  friend class BK::StackSingleton<Problem_Burgers1d>;
  
  Problem_Burgers1d() {}

  void init(int ncel, std::string parameterFilename) {

    name_ = "burgers1d";
    
    ProblemBase1d::init(ncel, parameterFilename);

    // computing dt
    dt_ = fabs(cfl_*dx_);
    if(nt_ > 0) {
      T_ = nt_*dt_;
    }
    else {
      nt_ = int(T_/dt_) + 1;
    }
    dt_ *= T_/(nt_*dt_);
    
    initialized_ = true;

    logParameter();
  }

  Vector initialCondition(double x) {
    assert(initialized_);
    // double x0 = 0.35;
    // double y0 = 0.35;

    // int kMax = 10;
    // double temp = 0;
    // for(int k=1; k<=kMax; k++)
    //   temp += pow(k,-5./6)*sin(2*M_PI*k/lambda_*(x-shift-2*M_PI/(k*k)));

    // return temp;

    double k=1;
    return Vector{sin(2*M_PI*k*(x)/lx_)};
    // return Vector{sin(2*M_PI*y/ly)};

    //    return Vector{exp(-((x-x0)*(x-x0)+(y-y0)*(y-y0))/(0.1*0.1))};
  }

  Vector initialConditionShift(double x, double shift = 0) {
    assert(initialized_);
    // double x0 = 0.35;
    // double y0 = 0.35;

    // int kMax = 10;
    // double temp = 0;
    // for(int k=1; k<=kMax; k++)
    //   temp += pow(k,-5./6)*sin(2*M_PI*k/lambda_*(x-shift-2*M_PI/(k*k)));

    // return temp;

    double k=1;
    return Vector{sin(2*M_PI*k*(x-shift)/lx_)};
    // return Vector{sin(2*M_PI*y/ly)};

    //    return Vector{exp(-((x-x0)*(x-x0)+(y-y0)*(y-y0))/(0.1*0.1))};
  }

  void lhs(Array& cNew, Array const& c, Array const& rhs) const {
    cNew = c + dt_*rhs;
  }    

  VecVec flux_vector(Vector cellVec, size_t cell = 0, size_t index = 0) {
    assert(initialized_);
    
    return VecVec{Vector{cellVec[0]*cellVec[0]/2.}};
  }

  double dFlux_value(Vector const& varArray, size_t cell = 0, size_t index = 0) {
    assert(initialized_);
    
    return varArray[0];
  }

  void compute_fluxArray(FluxVecArray& fluxArray, FluxInfoBordArray const& fluxInfoArray)
  {
    switch(numFluxType_) {
    case 0: {
      auto fluxFunc = [this](Vector const& varArray, size_t cell, size_t index) { return this->flux_vector(varArray, cell, index);};
      auto dfluxFunc = [this](Vector const& varArray, size_t cell, size_t index) { return this->dFlux_value(varArray, cell, index);};
      
      compute_fluxArray_LF(fluxArray, fluxInfoArray, fluxFunc, dfluxFunc);
      break;
    }
    case 1: compute_fluxArray_Godunov(fluxArray, fluxInfoArray); break;
    default: std::cout << "ERROR in Burgers1d::compute_fluxArray\n"; exit(1);
    }
  }
  
  void compute_fluxArray_Godunov(FluxVecArray& fluxArray, FluxInfoBordArray const& fluxInfoArray)
  {
    for(size_t cellInterface=0; cellInterface<ncel_+1; cellInterface++) {

      double valM = fluxInfoArray(cellInterface)[Val::minus][Var::c];
      double valP = fluxInfoArray(cellInterface)[Val::plus][Var::c];
 
      // shock:
      if(valM >= valP) {
	// shock-speed
	double s = 0.5*(valM + valP);
      
	if(s >= 0) 
	  fluxArray(cellInterface) = flux_vector(valM)[0][0];
	else
	  fluxArray(cellInterface) = flux_vector(valP)[0][0];
      }

      // rarefaction wave
      else {
	if(valM >= 0)
	  fluxArray(cellInterface) = flux_vector(valM)[0][0];
	else if(valP <= 0)
	  fluxArray(cellInterface) = flux_vector(valP)[0][0];
	else
	  fluxArray(cellInterface) = 0;
      }
    }
  }
};

typedef Problem_Burgers1d Problem;
extern Problem& problem;

#endif
