#ifndef geo_cubedSphere_hpp
#define geo_cubedSphere_hpp

#include <memory>
#include <cmath>

#include <BasisKlassen/array.hpp>
#include <BasisKlassen/vector.hpp>
#include <BasisKlassen/timer.hpp>

#include "dg2d.hpp"
#include "rk.hpp"
#include "writeArray2d.hpp"
#include "notiModify.hpp"
#include "cubedSphere_utils.hpp"

extern CubedSphere_utils& cs_utils;

// LOGGING: ----------------------------------------------------
// #undef LOGLEVEL
// #define LOGLEVEL 4

class GeoCubedSphere
{
public:
  
  GeoCubedSphere(size_t ncelX, size_t ncelY, std::string parameterFilename) : ncel_(ncelX), parameterFilename_(parameterFilename) {
    cVec.resize(6, Problem::Array(ncel_, ncel_, order, order));
    rhs.resize(6, Problem::Array(ncel_, ncel_, order, order));
    rhsVec.resize(6, Rhs2d(ncel_, ncel_, problem.get_dx(), problem.get_dy()));
    rkVec.resize(6);

    da_ = M_PI/2./ncel_;
    db_ = M_PI/2./ncel_;

    for(int i=0; i<6; i++)
      std::fill(cVec[i].begin(), cVec[i].end(), 0);

    initialize();

    notifyFieldVec.exec("fieldInitial", &get_c());
    
    mpiBase.MPICartdimCalcul( 2 ) ;

    for(int i=0;i<6;i++) {
      transMatam[i].resize(ncel_*order);
      transMatap[i].resize(ncel_*order);
      transMatbm[i].resize(ncel_*order);
      transMatbp[i].resize(ncel_*order);
      transInvMatam[i].resize(ncel_*order);
      transInvMatap[i].resize(ncel_*order);
      transInvMatbm[i].resize(ncel_*order);
      transInvMatbp[i].resize(ncel_*order);
    }

    transMat12.resize(ncel_*order);
    transMat21.resize(ncel_*order);
    transMat23.resize(ncel_*order);
    transMat32.resize(ncel_*order);
    transMat34.resize(ncel_*order);
    transMat43.resize(ncel_*order);
    transMat41.resize(ncel_*order);
    transMat14.resize(ncel_*order);

    transMat15.resize(ncel_*order);
    transMat51.resize(ncel_*order);
    transMat53.resize(ncel_*order);
    transMat35.resize(ncel_*order);
    transMat36.resize(ncel_*order);
    transMat63.resize(ncel_*order);
    transMat61.resize(ncel_*order);
    transMat16.resize(ncel_*order);

    transMat52.resize(ncel_*order);
    transMat25.resize(ncel_*order);
    transMat26.resize(ncel_*order);
    transMat62.resize(ncel_*order);
    transMat64.resize(ncel_*order);
    transMat46.resize(ncel_*order);
    transMat45.resize(ncel_*order);
    transMat54.resize(ncel_*order);

    for(int o = 0; o<order; o++) {
      double point = (points[order-1][o]+1)/2;
      
      for(int ai = 0; ai < ncel_; ai++) {
	
       	double a0 = -M_PI/4;
       	double a = a0 + ai*da_ + point*da_;	  

	transMat15[ai*order + o] = {{1, -sin(2*a)},{0,1}};
	transMat51[ai*order + o] = {{1, sin(2*a)},{0,1}};
	transMat53[ai*order + o] = {{-1, sin(2*a)},{0,-1}};
	transMat35[ai*order + o] = {{-1, sin(2*a)},{0,-1}};
	transMat36[ai*order + o] = {{-1, -sin(2*a)},{0,-1}};
	transMat63[ai*order + o] = {{-1, -sin(2*a)},{0,-1}};
	transMat61[ai*order + o] = {{1, -sin(2*a)},{0,1}};
	transMat16[ai*order + o] = {{1, sin(2*a)},{0,1}};
	transMat45[ai*order + o] = {{0, 1},{-1, sin(2*a)}};
	transMat25[ai*order + o] = {{0, -1},{1, -sin(2*a)}};
	transMat26[ai*order + o] = {{0, 1},{-1, -sin(2*a)}};
	transMat46[ai*order + o] = {{0, -1},{1, sin(2*a)}};
	
	if(problem.fluxVarianceType == 1) {
	
	  Mat2x2 GI= cs_utils.GI(ai,ncel_-1,o,order-1);
	  Mat2x2 G= cs_utils.G(ai,0,o,0);
	  Mat2x2 tm = transMat15[ai*order + o];
	  matrixMult3(transMat15[ai*order + o],G,tm,GI);
	  
	  GI= cs_utils.GI(ai,0,o,0);
	  G= cs_utils.G(ai,ncel_-1,o,order-1);
	  tm = transMat51[ai*order + o];
	  matrixMult3(transMat51[ai*order + o],G,tm,GI);
	  
	  GI= cs_utils.GI(ai,ncel_-1,o,order-1);
	  G= cs_utils.G(reverseCell(ai),ncel_-1,reverseIndex(o),order-1);
	  tm = transMat53[ai*order + o];
	  matrixMult3(transMat53[ai*order + o],G,tm,GI);
	  
	  GI= cs_utils.GI(ai,ncel_-1,o,order-1);
	  G= cs_utils.G(reverseCell(ai),ncel_-1,reverseIndex(o),order-1);
	  tm = transMat35[ai*order + o];
	  matrixMult3(transMat35[ai*order + o],G,tm,GI);

	  GI= cs_utils.GI(ai,0,o,0);
	  G= cs_utils.G(reverseCell(ai),0,reverseIndex(o),0);
	  tm = transMat36[ai*order + o];
	  matrixMult3(transMat36[ai*order + o],G,tm,GI);
	  
	  GI= cs_utils.GI(ai,0,o,0);
	  G= cs_utils.G(reverseCell(ai),0,reverseIndex(o),0);
	  tm = transMat63[ai*order + o];
	  matrixMult3(transMat63[ai*order + o],G,tm,GI);

	  GI= cs_utils.GI(ai,ncel_-1,o,order-1);
	  G= cs_utils.G(ai,0,o,0);
	  tm = transMat61[ai*order + o];
	  matrixMult3(transMat61[ai*order + o],G,tm,GI);

	  GI= cs_utils.GI(ai,0,o,0);
	  G= cs_utils.G(ai,ncel_-1,o,order-1);
	  tm = transMat16[ai*order + o];
	  matrixMult3(transMat16[ai*order + o],G,tm,GI);
	
	  GI= cs_utils.GI(ai,ncel_-1,o,order-1);
	  G= cs_utils.G(0,reverseCell(ai),0,reverseIndex(o));
	  tm = transMat45[ai*order + o];
	  matrixMult3(transMat45[ai*order + o],G,tm,GI);
	  
	  GI= cs_utils.GI(ai,ncel_-1,o,order-1);
	  G= cs_utils.G(ncel_-1,ai,order-1,o);
	  tm = transMat25[ai*order + o];
	  matrixMult3(transMat25[ai*order + o],G,tm,GI);
	  
	  GI= cs_utils.GI(ai,0,o,0);
	  G= cs_utils.G(ncel_-1,reverseCell(ai),order-1,reverseIndex(o));
	  tm = transMat26[ai*order + o];
	  matrixMult3(transMat26[ai*order + o],G,tm,GI);
	  
	  GI= cs_utils.GI(ai,0,o,0);
	  G= cs_utils.G(0,ai,0,o);
	  tm = transMat46[ai*order + o];
	  matrixMult3(transMat46[ai*order + o],G,tm,GI);
	}
      }
	
      for(int bi = 0; bi < ncel_; bi++) {
	
	double b0 = -M_PI/4;
	double b = b0 + bi*db_ + point*db_;	  

	transMat12[bi*order + o] = {{1, 0},{-2*cos(b)*sin(b),1}};
	transMat21[bi*order + o] = {{1, 0},{sin(2*b),1}};
	transMat54[bi*order + o] = {{-2*cos(b)*sin(b), -1},{1, 0}};
	transMat52[bi*order + o] = {{-2*cos(b)*sin(b), 1},{-1, 0}};
	transMat62[bi*order + o] = {{sin(2*b), -1},{1, 0}};
	transMat64[bi*order + o] = {{sin(2*b), 1},{-1, 0}};
	
	if(problem.fluxVarianceType == 1) {
	  
	  Mat2x2 GI= cs_utils.GI(ncel_-1,bi,order-1,o);
	  Mat2x2 G= cs_utils.G(0,bi,0,o);
	  Mat2x2 tm = transMat12[bi*order + o];
	  matrixMult3(transMat12[bi*order + o],G,tm,GI);
	  
	  GI= cs_utils.GI(0,bi,0,o);
	  G= cs_utils.G(ncel_-1,bi,order-1,o);
	  tm = transMat21[bi*order + o];
	  matrixMult3(transMat21[bi*order + o],G,tm,GI);

	  GI= cs_utils.GI(0,bi,0,o);
	  G= cs_utils.G(bi,reverseCell(ncel_-1),o,reverseIndex(order-1));
	  tm = transMat54[bi*order + o];
	  matrixMult3(transMat54[bi*order + o],G,tm,GI);
	  
	  GI= cs_utils.GI(ncel_-1,bi,order-1,o);
	  G= cs_utils.G(bi,ncel_-1,o,order-1);
	  tm = transMat52[bi*order + o];
	  matrixMult3(transMat52[bi*order + o],G,tm,GI);

	  GI= cs_utils.GI(ncel_-1,bi,order-1,o);
	  G= cs_utils.G(reverseCell(bi),0,reverseIndex(o),0);
	  tm = transMat62[bi*order + o];
	  matrixMult3(transMat62[bi*order + o],G,tm,GI);

	  GI= cs_utils.GI(0,bi,0,o);
	  G= cs_utils.G(bi,0,o,0);
	  tm = transMat64[bi*order + o];
	  matrixMult3(transMat64[bi*order + o],G,tm,GI);
	}
      }
    }
    
    transMat23 = transMat12;
    transMat34 = transMat12;
    transMat41 = transMat12;
    transMat14 = transMat21;
    transMat43 = transMat21;
    transMat32 = transMat21;
  }

  void init() {}
  
  void initialize() {

    for(int a = 0; a<ncel_; a++) 
      for(int b = 0; b<ncel_; b++) 
	for(int ia = 0; ia < order; ia++)
	  for(int ib = 0; ib < order; ib++) 
	    for(int face = 0; face <6; face++)
	      cVec[face](a, b, ia, ib) = problem.initialCondition(a, b, ia, ib, face);
    
    std::string dirName = BK::toString("data/", problem.get_name());
       std::cout << "writing initial data to " << dirName << std::endl;
    writeCSV(cVec, BK::toString(dirName + "/initial_" +  std::to_string(order) + "-" + std::to_string(problem.get_rkOrder()) +"_" + std::to_string(problem.get_ncelx()) + "_" + std::to_string(problem.get_ncely()) + ".csv"));
    writeCSV(cVec, BK::toString(dirName + "/initial.csv"));

    // writePhiThetaCSV(cVec, BK::toString(dirName + "/initial_phi_theta_" +  std::to_string(order) + "-" + std::to_string(problem.get_rkOrder()) +"_" + std::to_string(problem.get_ncelx()) + "_" + std::to_string(problem.get_ncely()) + ".csv"),rhs);
    // writePhiThetaCSV(cVec, BK::toString(dirName + "/initial_phi_theta.csv"),rhs);

    // problem.initialize(cVec[0]);
    // std::cerr << "problem.get_lambda() = " << problem.get_lambda() << std::endl;
    // problem.initialize(cVec[1]);

    // std::string dirName = BK::toString("data/", problem.get_name());
    // std::cerr << "writing patched data ...\n";

    // writeFine(cVec[0], dirName + "/initCubed_" +  std::to_string(order) + "-" +  std::to_string(3) + "_" + std::to_string(ncel_) + "_" + std::to_string(ncel_) + "-0", 10, problem.get_dx(), problem.get_dy());
    // writeFine(cVec[1], dirName + "/initCubed_" +  std::to_string(order) + "-" +  std::to_string(3) + "_" + std::to_string(ncel_) + "_" + std::to_string(ncel_) + "-1", 10, problem.get_dx(), problem.get_dy());
    
  }

  void writeCSV(BK::Vector<Problem::Array> const& dataVec, std::string filename) {
    LOGL(3,"GeoCubedSphere::writeCSV");

    timerM.get("GeoCubedSphere::writeCSV").start();
    
    std::ofstream out(filename.c_str());
    out << problem.toWriteCSV_info() << std::endl;

    BK::Vector<double> xi = points[order-1];
    
    for(int a = 0; a<ncel_; a++) {
      for(int b = 0; b<ncel_; b++) {
	for(int ia = 0; ia < order; ia++)
	  for(int ib = 0; ib < order; ib++) {
	    double alpha = cs_utils.get_alpha(a,ia);
	    double beta = cs_utils.get_beta(b,ib);

	    for(int face = 0; face < 6; face++) {
	      
	      BK::Array<double,3> xyz = ab2xyz[face](alpha, beta);
	      out << xyz[0] << "," << xyz[1] << "," << xyz[2] << ",";

	      BK::Array<double,2> pt = ab2pt[face](alpha, beta);
	      out << pt[0] << "," << pt[1] << ",";

	      if(face < 4)
		out << alpha+M_PI/2*face << "," << beta << "," << face << ",";

	      if(face == 4)
		out << alpha << "," << beta+M_PI/2 << "," << face << ",";

	      if(face == 5)
		out << alpha << "," << beta-M_PI/2 << "," << face << ",";
		      
	      BK::Vector<double> valueVec = problem.toWriteCSV(dataVec[face], a, b, ia, ib, face);
	      for(size_t val=0; val<valueVec.size()-1; val++)
		out << valueVec[val] << ",";
	    
	      out << valueVec[valueVec.size()-1] << std::endl;
	    }
	  }
      }
    }

    out.close();

    timerM.get("GeoCubedSphere::writeCSV").stop();

    LOGL(4,"GeoCubedSphere::writeCSV-end");
  }

  // void writePhiThetaCSV(BK::Vector<Problem::Array> const& dataVec, std::string filename, BK::Vector<Problem::Array> const& rhsVec) {
    
  //   //    BK::Vector<BK::Array<double,3>> points;
  //   std::ofstream out(filename.c_str());
  //   out << problem.toWritePhiThetaCSV_info() << ",rhs0,rhs1,rhs2" << std::endl;

  //   BK::Vector<double> xi = points[order-1];
    
  //   for(int a = 0; a<ncel_; a++) {
  //     auto alphaF = [a, this](double x) { return (-M_PI/4. + a*da_+ 0.5*da_ + x*0.5*da_);};
  //     for(int b = 0; b<ncel_; b++) {
  // 	auto betaF = [b, this](double y) { return (-M_PI/4. + b*db_ + 0.5*db_ + y*0.5*db_);};
  // 	for(int ia = 0; ia < order; ia++)
  // 	  for(int ib = 0; ib < order; ib++) {

  // 	    for(int face = 0; face < 6; face++) {
	    
  // 	      BK::Array<double,3> xyz = ab2xyz[face](alphaF(xi[ia]), betaF(xi[ib]));
  // 	      // BK::Array<double,2> pt = xyz2pt(xyz[0], xyz[1], xyz[2]);
  // 	      BK::Array<double,2> pt = ab2pt[face](alphaF(xi[ia]), betaF(xi[ib]));

  // 	      // if(face == 0) {
  // 	      // 	static double phiMin = 10;
  // 	      // 	static double phiMax = -10;

  // 	      // 	if(pt[0] < phiMin) phiMin = pt[0];
  // 	      // 	if(pt[0] > phiMax) phiMax = pt[0];

  // 	      // 	std::cout << "phiMin = " << phiMin << "\t, phiMax = " << phiMax << std::endl;
  // 	      // }
	      
  // 	      // int foundFace = face;
  // 	      // auto ab = xyz2ab_face(xyz[0], xyz[1], xyz[2], foundFace);
  // 	      // std::cout << face << "------\t" << a << "\t" << b << "\t" << ia << "\t" << ib << " :\t" << ab[0] << "\t" << ab[1] << "\t" << xyz << "\t" << foundFace << std::endl;

  // 	      out << pt[0] << "," << pt[1] << "," << 0 << ",";
	      
  // 	      BK::Vector<double> valueVec = problem.toWriteCSV(dataVec[face], a, b, ia, ib, face);
  // 	      for(size_t val=0; val<valueVec.size()-1; val++)
  // 		out << valueVec[val] << ",";
	    
  // 	      out << valueVec[valueVec.size()-1];// << std::endl;

  // 	      double alpha = alphaF(xi[ia]);
  // 	      double beta = betaF(xi[ib]);

  // 	      if(rhsVec[face](a,b,ia,ib).size() == 3) {
  // 		double rhsA = rhsVec[face](a,b,ia,ib)[1];
  // 		double rhsB = rhsVec[face](a,b,ia,ib)[2];
  // 		// double rhsA = 0;
  // 		// double rhsB = 0;
  // 		BK::Array<double,3> rhs;
  // 		d_parametrisation[face](rhs, rhsA, rhsB, alpha, beta);
		
  // 		double theta = pt[1];
  // 		double phi = pt[0];
  // 		double rhs_theta;
  // 		double rhs_phi;
  // 		d_map_spherical(rhs, rhs_theta, rhs_phi, theta, phi);
		
  // 		out << "," << rhsVec[face](a,b,ia,ib)[0] << "," << rhs_theta << "," << rhs_phi << std::endl;
  // 	      }
  // 	    }
  // 	  }
  //     }
  //   }

  //   out.close();
  //   //  exit(0);
  // }

  void writeCSV_PhiThetaUniform(BK::Vector<Problem::Array> const& dataVec, std::string filename) {
    
    std::ofstream out(filename.c_str());
    out << problem.toWriteCSV_info() << std::endl;
    
    int nPhi = 100;
    int nTheta = 100;

    double gap = 0.02;
    double dPhi = 2*M_PI/(nPhi-1);
    double dTheta = (M_PI-2*gap)/(nTheta-1);
    double startPhi = -3./4*M_PI;
    double startTheta = gap;

    std::cout << "dTheta = " << dTheta << std::endl;
    std::cout << "dPhi = " << dPhi << std::endl;

    for(int p=0; p<nPhi; p++)
      for(int t=0; t<nTheta; t++) {
	double phi = p*dPhi +  startPhi;
	double theta = t*dTheta + startTheta;

	auto xyz = pt2xyz(phi, theta);

	
	out << xyz[0] << "," <<  xyz[1] << "," <<  xyz[2] << "," << phi << "," << theta << ",";
	
	int face;
	auto ab = xyz2ab_face(xyz[0], xyz[1], xyz[2], face);
	double alpha = ab[0];
	double beta = ab[1];
	
	if(face < 4)
	  out << alpha+M_PI/2*face << "," << beta << "," << face << ",";
	
	if(face == 4)
	  out << alpha << "," << beta+M_PI/2 << "," << face << ",";
	
	if(face == 5)
	  out << alpha << "," << beta-M_PI/2 << "," << face << ",";
	
	BK::Vector<double> valueVec = problem.toWriteCSV(dataVec[face], ab, face);
	for(size_t val=0; val<valueVec.size()-1; val++)
	  out << valueVec[val] << ",";
	
	out << valueVec[valueVec.size()-1] << std::endl;
      }
    
    out.close();
    //  exit(0);
  }

  void boundaryCondition(BK::Vector<Rhs2d> & rhsVec);

  void solve() {

    timerM.get("GeoCubedSphere::solve").start();
    
    std::string dirName = BK::toString("data/", problem.get_name());
#ifdef NETCDF
    writeNetCDF(cVec, dirName, "init_" +  std::to_string(order) + "-" + std::to_string(problem.get_rkOrder()) +"_" + std::to_string(ncel_));
#endif

    writeCSV(cVec, BK::toString(dirName + "/init_" +  std::to_string(order) + "-" + std::to_string(problem.get_rkOrder()) +"_" + std::to_string(problem.get_ncelx()) + "_" + std::to_string(problem.get_ncely()) + ".csv"));
    writeCSV(cVec, BK::toString(dirName + "/init.csv"));
    writeCSV_PhiThetaUniform(cVec, BK::toString(dirName + "/init_phi_theta_uniform.csv"));
    
    double dt = problem.get_dt();
    
    BK::Vector<Problem::Array> cnewVec(6);
    for(size_t i=0; i<6; i++) {
      cnewVec[i].resize(ncel_, ncel_, order, order);
      cnewVec[i] = cVec[i];
      rkVec[i] = getRkObject(dt, cnewVec[i]);
    }

    time_ = 0;
    Problem::Array rhsTmp(cVec[0].dim());
    //    for(int step=problem.get_step();step<problem.get_nt();step++){
    while(problem.get_step() < problem.get_stopStep()) {
      
      std::cout << "step = " << problem.get_step() << "\t time = " << time_ << std::endl;

      for(int stage = 0; stage < rkVec[0]->get_stageNumber(); stage++) {
	//for(int stage = 0; stage < 1; stage++) {
	for(size_t face = 0; face<6; face++) {
	  
	  problem.computeFluxes(rhsVec[face].fluxArray, rhsVec[face].fluxInfoBordArrayX, rhsVec[face].fluxInfoBordArrayY, cnewVec[face], face);
	  problem.computeSourceTerm(cnewVec[face], rhsVec[face].sourceArray, face);
	}

	auto dims = rhsVec[0].fluxInfoBordArrayX.dim();
	  
	boundaryCondition(rhsVec);
	
	for(size_t face = 0; face<6; face++) {
	  rhsVec[face].addIntegralAndSource(rhs[face]);
	  problem.computeNumericalFlux(rhsVec[face].fluxVecArrayX, rhsVec[face].fluxVecArrayY, rhsVec[face].fluxInfoBordArrayX, rhsVec[face].fluxInfoBordArrayY, face);
	  rhsVec[face].addBorderTerm(rhs[face]);
	  
	  rkVec[face]->advance(cnewVec[face], cVec[face], rhs[face]);
	}
      }
      
      for(size_t face = 0; face<6; face++) {
	cVec[face]=cnewVec[face];
      }      
      time_ += dt;
      ++problem.set_step();

      notifyFieldVec.exec("field", &get_c());
    }

    notifyFieldVec.exec("fieldFinal", &get_c());


    writeCSV(cnewVec, BK::toString(dirName + "/final_" +  std::to_string(order) + "-" + std::to_string(problem.get_rkOrder()) +"_" + std::to_string(problem.get_ncelx()) + "_" + std::to_string(problem.get_ncely()) + ".csv"));
    writeCSV(cnewVec, BK::toString(dirName + "/final.csv"));

    // writePhiThetaCSV(cnewVec, BK::toString(dirName + "/final_phi_theta_" +  std::to_string(order) + "-" + std::to_string(problem.get_rkOrder()) +"_" + std::to_string(problem.get_ncelx()) + "_" + std::to_string(problem.get_ncely()) + ".csv"),rhs);
    // writePhiThetaCSV(cnewVec, BK::toString(dirName + "/final_phi_theta.csv"),rhs)
      ;
    writeCSV_PhiThetaUniform(cnewVec, BK::toString(dirName + "/final_phi_theta_uniform.csv"));

#ifdef NETCDF
    writeNetCDF(cVec, dirName, "sol_" +  std::to_string(order) + "-" + std::to_string(problem.get_rkOrder()) +"_" + std::to_string(ncel_));
#endif

    timerM.get("GeoCubedSphere::solve").stop();
  }

  void writeNetCDF(BK::Vector<Problem::Array> const& cVec, std::string dirName, std::string filename)
  {
    int nPhi = 100;
    int nTheta = 100;
    
    BK::MultiArray<2, BK::Vector<double>> dataArray(nTheta, nPhi);
    auto varNames = problem.toWriteNetCDF_info();

    double dPhi = 2*M_PI/(nPhi-1);
    double dTheta = M_PI/nTheta;
    double startPhi = 0;
    double startTheta = dTheta/2;

    //   std::cout << "startPhi = " << startPhi << ", startTheta = " << startTheta << std::endl;

    for(int p=0; p<nPhi; p++)
      for(int t=0; t<nTheta; t++) {
	double phi = p*dPhi + startPhi;
	double theta = t*dTheta + startTheta;
	
	auto xyz = pt2xyz(phi, theta);
	
	int face;
	auto ab = xyz2ab_face(xyz[0], xyz[1], xyz[2], face);

	//	std::cout << phi << "\t" << theta << "\t" << ab[0] << "\t" << ab[1] << std::endl;
	BK::Vector<double> valueVec = problem.toWriteCSV(cVec[face], ab, face);
	dataArray(p,t).resize(varNames.size());
	dataArray(p,t) = valueVec;
      }

#ifdef NETCDF
    writeNetCDF_2D(dataArray, varNames, dirName + "/2d_"+filename, ncel_, dTheta, dPhi, "theta","phi", startTheta, startPhi);
#endif
    
    dPhi = 360./nPhi;
    dTheta = 180./nTheta;
    
    // using -dTheta instead of dTheta as netcdf uses math convention for theta in [-pi/2, pi/2] and code phys convention for theta in [0, pi] with inversed increasing direction
#ifdef NETCDF
    writeNetCDF_2D(dataArray, varNames, dirName + "/3d_" + filename, ncel_, -dTheta, dPhi, "latitude","longitude",-90,0);
#endif
  }

  BK::Vector<Problem::Array> const& get_c() {
    return cVec;
  }

private:
  
  void exchangeValues_toLeft(int from, int to);
  void exchangeValues_toRight(int from, int to);
  void exchangeValues_toUp(int from, int to);
  void exchangeValues_toDown(int from, int to);

  int reverseCell(int cell) {
    return ncel_ - cell - 1;
  }
  
  int reverseIndex(int index) {
    return order - index - 1;
  }
    
  CONSTPARA(int, ncel);
  CONSTPARA(double, da);
  CONSTPARA(double, db);
  CONSTPARA(double, time);
  PROTEPARA(std::string, parameterFilename);
  
  BK::Vector<Problem::Array> cVec;
  BK::Vector<Problem::Array> rhs;
  BK::Vector<Rhs2d> rhsVec;
  BK::Vector<std::shared_ptr<Rk>> rkVec;
  //  BK::MultiArray<spaceDim*2, double> sqrtG;

  BK::Array<BK::Vector<Mat2x2>,6> transMatam;
  BK::Array<BK::Vector<Mat2x2>,6> transMatap;
  BK::Array<BK::Vector<Mat2x2>,6> transMatbm;
  BK::Array<BK::Vector<Mat2x2>,6> transMatbp;
  BK::Array<BK::Vector<Mat2x2>,6> transInvMatam;
  BK::Array<BK::Vector<Mat2x2>,6> transInvMatap;
  BK::Array<BK::Vector<Mat2x2>,6> transInvMatbm;
  BK::Array<BK::Vector<Mat2x2>,6> transInvMatbp;

 
  BK::Vector<Mat2x2> transMat12;
  BK::Vector<Mat2x2> transMat21;
  BK::Vector<Mat2x2> transMat23;
  BK::Vector<Mat2x2> transMat32;
  BK::Vector<Mat2x2> transMat34;
  BK::Vector<Mat2x2> transMat43;
  BK::Vector<Mat2x2> transMat41;
  BK::Vector<Mat2x2> transMat14;

  BK::Vector<Mat2x2> transMat15;
  BK::Vector<Mat2x2> transMat51;
  BK::Vector<Mat2x2> transMat53;
  BK::Vector<Mat2x2> transMat35;
  BK::Vector<Mat2x2> transMat36;
  BK::Vector<Mat2x2> transMat63;
  BK::Vector<Mat2x2> transMat61;
  BK::Vector<Mat2x2> transMat16;

  BK::Vector<Mat2x2> transMat52;
  BK::Vector<Mat2x2> transMat25;
  BK::Vector<Mat2x2> transMat26;
  BK::Vector<Mat2x2> transMat62;
  BK::Vector<Mat2x2> transMat64;
  BK::Vector<Mat2x2> transMat46;
  BK::Vector<Mat2x2> transMat45;
  BK::Vector<Mat2x2> transMat54;
};

#endif
