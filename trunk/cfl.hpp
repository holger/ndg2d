#ifndef cfl_hpp
#define cfl_hpp

#include <BasisKlassen/array.hpp>

// cfl limits depending on the rkOrder and order
extern BK::Array<BK::Array<double, 9>,6> cflLimit;

#endif
