#ifndef problem_burgersDensityViscosity1d_hpp
#define problem_burgersDensityViscosity1d_hpp

#include <BasisKlassen/parameter.hpp>
#include <BasisKlassen/singleton.hpp>
#include <BasisKlassen/assert.hpp>

const int varDim = 3;    // three variables: rho, rho*u and q
const int spaceDim = 1;

#include "types.hpp"
#include "value1d.hpp"
#include "cfl.hpp"
#include "laxFriedrichs1d.hpp"
#include "centralFlux1d.hpp"
#include "notiModify.hpp"
#include "problemBase1d.hpp"

// Burgers equations with (passiv) density and viscosity in 1d
class Problem_BurgersDensityViscosity1d : public ProblemBase1d<varDim>
{
public:

  friend class BK::StackSingleton<Problem_BurgersDensityViscosity1d>;
  
  enum Var {rho, rhoU, q};

  typedef BK::Array<double, varDim> FluxInfo;  
  typedef BK::Array<FluxInfo, 2> FluxInfoBord;         // [+-][fnl0, ... , val2]
  typedef BK::MultiArray<spaceDim, FluxInfoBord> FluxInfoVecBordArray; // (cell)[+-][fnl0, ... , val2]
  typedef BK::MultiArray<spaceDim, Vector> FluxVecArray; 

  Problem_BurgersDensityViscosity1d() {}

  void init(int ncel, std::string parameterFilename) {

    name_ = "burgersDensityViscosity1d";

    ProblemBase1d::init(ncel, parameterFilename);
    initialized_ = true;
    
    BK::ReadInitFile initFile(parameterFilename,true);
    nu_ = initFile.getParameter<double>("nu");

    double dtAd = fabs(cfl_*dx_);
    double dtHeat = fabs(cfl_*dx_*dx_/nu_)/2.;
    
    std::cout << "dtAd = " << dtAd << "\t dtHeat = " << dtHeat << std::endl;

    if(nu_ == 0)
      dt_ = dtAd;
    else
      dt_ = std::min(dtAd,dtHeat);

    if(nt_ > 0) {
      T_ = nt_*dt_;
    }
    else {
      nt_ = int(T_/dt_) + 1;
    }

    dt_ *= T_/(nt_*dt_);

    logParameter();

    std::cerr << "nu = " << nu_ << std::endl;
  }

  Vector initialCondition(double x) {
    assert(initialized_);

    return initialConditionShift(x);
    //    return Vector{1., sin(2*M_PI*k*(x-shift)/lambda_), -2};
  }

  Vector initialConditionShift(double x, double shift = 0) {
    assert(initialized_);
    // double x0 = 0.35;
    // double y0 = 0.35;

    // int kMax = 10;
    // double temp = 0;
    // for(int k=1; k<=kMax; k++)
    //   temp += pow(k,-5./6)*sin(2*M_PI*k/lambda_*(x-shift-2*M_PI/(k*k)));

    // return temp;

    double k=1;
    return Vector{1., sin(2*M_PI*k*(x-shift)/lx_), 2*M_PI*k/lx_*cos(2*M_PI*k*(x-shift)/lx_)};
    //    return Vector{1., sin(2*M_PI*k*(x-shift)/lambda_), -2};
  }

  void lhs(Array& cNew, Array const& c, Array const& rhs) const {
    for(size_t i=0; i<c.size(); i++)
      cNew[i][0] = c[i][0] + dt_*rhs[i][0];

    for(size_t i=0; i<c.size(); i++)
      cNew[i][1] = c[i][1] + dt_*rhs[i][1];

    for(size_t i=0; i<c.size(); i++)
      cNew[i][2] = rhs[i][2];
  }    

  VecVec flux_vector_nonLin(Vector const& varArray, size_t cell = 0, size_t index = 0) {
    assert(initialized_);

    double rho = varArray[Var::rho];
    double rhoU = varArray[Var::rhoU];
    double u = rhoU/rho;

    Vector result = {rhoU, rhoU*u, 0};
    
    return VecVec{result};
  }

  VecVec flux_vector_diffusion(Vector const& varArray, size_t cell = 0, size_t index = 0) {
    assert(initialized_);

    double rho = varArray[Var::rho];
    double rhoU = varArray[Var::rhoU];
    double u = rhoU/rho;
    double q = varArray[Var::q];
    
    return VecVec{Vector{0, -rho*sqrt(nu_)*q, -sqrt(nu_)*u}};
  }

  VecVec flux_vector(Vector const& varArray, size_t cell = 0, size_t index = 0) {
    assert(initialized_);
    
    Vector nonLin = flux_vector_nonLin(varArray)[0];
    Vector diffusion = flux_vector_diffusion(varArray)[0];

    Vector flux = nonLin + diffusion;

    return VecVec{flux};
  }

  double dFlux_value(Vector const& varArray, size_t cell = 0, size_t index = 0) {
    assert(initialized_);
    
    double rho = varArray[Var::rho];
    double rhoU = varArray[Var::rhoU];
    double u = rhoU/rho;

    return u;
  }

  void compute_fluxArray(FluxVecArray& fluxArray, FluxInfoBordArray const& fluxInfoArray)
  {
    FluxVecArray fluxArray_nl(ncel_+1);
    
    auto fluxFunc_nl = [this](Vector const& varArray, size_t cell, size_t index) { return this->flux_vector_nonLin(varArray, cell, index);};
    auto dfluxFunc = [this](Vector const& varArray, size_t cell, size_t index) { return this->dFlux_value(varArray, cell, index);};
    compute_fluxArray_LF(fluxArray_nl, fluxInfoArray, fluxFunc_nl, dfluxFunc);

    // setting wrong q component of flux to zero
    for(size_t cellInterface=0; cellInterface<ncel_+1; cellInterface++)
      fluxArray_nl(cellInterface)[Var::q] = 0;
    
    FluxVecArray fluxArray_d(ncel_+1);

    auto fluxFunc_d = [this](Vector const& varArray) { return this->flux_vector_diffusion(varArray);};
    compute_fluxArray_Central(fluxArray_d, fluxInfoArray, fluxFunc_d);
    
    fluxArray = fluxArray_nl + fluxArray_d;
  }
  
  CONSTPARA(double, nu);
};

typedef Problem_BurgersDensityViscosity1d Problem;
extern Problem& problem;

#endif






















































