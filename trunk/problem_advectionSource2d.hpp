#ifndef problem_advectionSource2d_hpp
#define problem_advectionSource2d_hpp

#include <BasisKlassen/parameter.hpp>
#include <BasisKlassen/array.hpp>

const int varDim = 3;   // three variables: rho, rho*ux, rho*uy
const int spaceDim = 2; // 2d space

#include "types.hpp"
#include "cfl.hpp"
#include "problemBase2d.hpp"
#include "gaussLobatto.hpp"
#include "logger.hpp"

// #undef LOGLEVEL
// #define LOGLEVEL 4

// 2d advection with auxiliary velocity field
class Problem_AdvectionSource2d : public ProblemBase2d<varDim>
{
public:
  
  struct Var {
    static const int rho = 0;     // rho
    static const int rhoUx = 1;   // rho*ux
    static const int rhoUy = 2;   // rho*uy
  };

  friend class BK::StackSingleton<Problem_AdvectionSource2d>;

  Problem_AdvectionSource2d() {}

  void init(int ncelx, int ncely, std::string parameterFilename) {
    LOGL(1,"Problem_AdvectionSource2d::init(int ncelx, int ncely, std::string parameterFilename)");
    
    ProblemBase2d::init(ncelx, ncely, parameterFilename);
    
    double nt = nt_;

    velXArray.resize(ncelx_, ncely_, order, order);
    velYArray.resize(ncelx_, ncely_, order, order);
    dxVelXArray.resize(ncelx_, ncely_, order, order);
    dxVelYArray.resize(ncelx_, ncely_, order, order);
    dyVelXArray.resize(ncelx_, ncely_, order, order);
    dyVelYArray.resize(ncelx_, ncely_, order, order);

    if(nt > 0 && T_ > 0) {
      std::cerr << "ERROR in Problem_advectionSource2d::init(): nt = " <<
	nt << " > 0 && T = " << T_ << " > 0." << std::endl;
      exit(1);						   
    }

    dt_ = fabs(cfl_*dx_);

    if(nt > 0) {
      nt_ = nt;
      T_ = nt_*dt_;
    }
    else
      nt_ = int(T_/dt_) + 1;

    dt_ *= T_/(nt_*dt_);
    
    name_ = "advectionSource2d";

    BK::Vector<double> xi = points[order-1];
    for(int ix=0;ix<int(ncelx_);ix++) {
      auto transX = [ix, this](double x) { return ix*dx_+0.5*dx_+x*0.5*dx_;};
      for(int iy=0;iy<int(ncely_);iy++) {
	auto transY = [iy, this](double y) { return iy*dy_+0.5*dy_+y*0.5*dy_;};
    	for(int ox=0; ox<order;ox++)
    	  for(int oy=0;oy<order;oy++) {
	    velocity(velXArray(ix,iy,ox,oy), velYArray(ix,iy,ox,oy), transX(xi[ox]), transY(xi[oy]));
	    dxVel(dxVelXArray(ix,iy,ox,oy), dxVelYArray(ix,iy,ox,oy), transX(xi[ox]), transY(xi[oy]));
	    dyVel(dyVelXArray(ix,iy,ox,oy), dyVelYArray(ix,iy,ox,oy), transX(xi[ox]), transY(xi[oy]));
	  }
      }
    }

    logParameter();

    initialized_ = true;

    LOGL(4,"Problem_AdvectionSource2d::init(int ncelx, int ncely, std::string parameterFilename)-end");
  }

  void velocity(double& vx, double& vy, double x, double y) {
    vx = sin(2*M_PI*x/lx_);
    vy = 0;
  }  

  void dxVel(double& dxVelX, double& dxVelY, double x, double y) {
    dxVelX = 2*M_PI/lx_*cos(2*M_PI*x/lx_);
    dxVelY = 0;
  }  

  void dyVel(double& dyVelX, double& dyVelY, double x, double y) {
    dyVelX = 0;
    dyVelY = 0;
  }  

  double rho(double x) {
    return 1;
    //    return 3+sin(2*M_PI*x/lx_);
  } 

  double velocity(double x) {
    return sin(2*M_PI*x/lx_);
    //return 1;
  } 

  Vector initialCondition(double x, double y) {
    LOGL(4,"Problem_AdvectionSource2d::initialCondition(double x, double y)");
    
    return Vector{rho(x), rho(x)*velocity(x), 0};
  }

  void getReference(Array & c) {}

  void lhs(Array& cNew, Array const& c, Array const& rhs) const {
    cNew = c + dt_*rhs;
  }    

  // void computeSourceTerm(Array const& c, Array & s) {
  //   for(int a=0; a<int(c.size(0)); a++)
  //     for(int b=0; b<int(c.size(1)); b++)
  // 	for(int ia=0; ia<int(c.size(2)); ia++)
  // 	  for(int ib=0; ib<int(c.size(3)); ib++) {
  // 	    s(a,b,ia,ib)[1] = c(a,b,ia,ib)[1]*dxVelXArray(a,b,ia,ib) + c(a,b,ia,ib)[2]*dyVelXArray(a,b,ia,ib);
  // 	    s(a,b,ia,ib)[2] = c(a,b,ia,ib)[1]*dxVelYArray(a,b,ia,ib) + c(a,b,ia,ib)[2]*dyVelYArray(a,b,ia,ib);
  // 	  }
  // }

  void computeSourceTerm(Array const& c, Array & s) {
    auto uxFunc = [](Array const& c, int cellX, int cellY, int indexX, int indexY) {
      double rhoUx = c(cellX, cellY, indexX, indexY)[Var::rhoUx];
      double rho = c(cellX, cellY, indexX, indexY)[Var::rho];
      double ux = rhoUx/rho;
      return ux;
    };
    
    auto uyFunc = [](Array const& c, int cellX, int cellY, int indexX, int indexY) {
      double rhoUy = c(cellX, cellY, indexX, indexY)[Var::rhoUy];
      double rho = c(cellX, cellY, indexX, indexY)[Var::rho];
      double uy = rhoUy/rho;
      return uy;
    };
      
    for(int cellX=0; cellX<int(c.size(0)); cellX++)
      for(int cellY=0; cellY<int(c.size(1)); cellY++)
	for(int indexX=0; indexX<int(c.size(2)); indexX++)
	  for(int indexY=0; indexY<int(c.size(3)); indexY++) {
	    double rho = c(cellX,cellY,indexX,indexY)[Var::rho];
	    double ux = c(cellX,cellY,indexX,indexY)[Var::rhoUx]/rho;
	    double uy = c(cellX,cellY,indexX,indexY)[Var::rhoUy]/rho;
	    
	    double dux_x = derivativeX(uxFunc, c, cellX, cellY, indexX, indexY);
	    double duy_y = derivativeY(uyFunc, c, cellX, cellY, indexX, indexY);
	    double dux_y = derivativeY(uxFunc, c, cellX, cellY, indexX, indexY);
	    double duy_x = derivativeX(uyFunc, c, cellX, cellY, indexX, indexY);
	    
	    s(cellX,cellY,indexX,indexY)[0] = rho*(dux_x + duy_y);
	    s(cellX,cellY,indexX,indexY)[1] = rho*(2*ux*dux_x + ux*duy_y + uy*dux_y);
	    s(cellX,cellY,indexX,indexY)[2] = rho*(ux*duy_x + uy*dux_x + 2*uy*duy_y);
	  }
  }

  VecVec flux_vector(Vector data) {
    double rho = data[Var::rho];
    double rhoUx = data[Var::rhoUx];
    double rhoUy = data[Var::rhoUy];

    double ux = rhoUx/rho;
    double uy = rhoUy/rho;

    // f0x = rhoUx; f1x = rhoUx*ux; f2x = rhoUx*uy;
    // f0y = rhoUy; f1y = rhoUy*ux; f2y = rhoUy*uy;
    return VecVec{ Vector{rhoUx, rhoUx*ux, rhoUx*uy},
                   Vector{rhoUy, rhoUy*ux, rhoUy*uy} };
  }

  BK::Array<double,2> dFlux_value(Vector data) {
    double rho = data[Var::rho];
    double rhoUx = data[Var::rhoUx];
    double rhoUy = data[Var::rhoUy];

    double ux = rhoUx/rho;
    double uy = rhoUy/rho;

    return BK::Array<double,2>({ux, uy});
  }

  void computeNumericalFlux(FluxVecArray& fluxVecArrayX, FluxVecArray& fluxVecArrayY,
			    FluxInfoBordArray& fluxInfoArrayX, FluxInfoBordArray& fluxInfoArrayY) {
    
    computeNumericalFlux_LF(fluxVecArrayX, fluxVecArrayY, fluxInfoArrayX, fluxInfoArrayY, *this);
  }

  BK::MultiArray<spaceDim*2, double> velXArray;
  BK::MultiArray<spaceDim*2, double> velYArray;
  BK::MultiArray<spaceDim*2, double> dxVelXArray;
  BK::MultiArray<spaceDim*2, double> dyVelXArray;
  BK::MultiArray<spaceDim*2, double> dxVelYArray;
  BK::MultiArray<spaceDim*2, double> dyVelYArray;
};

typedef Problem_AdvectionSource2d Problem;
extern Problem& problem;

#endif
