#! /bin/bash

rm -r data/burgersDensity1d

cp order.hpp_template order.hpp
sed -i 's/static constexpr int order = 3/static constexpr int order = 3/g' order.hpp
sed -i 's/static constexpr int rkOrder = 3/static constexpr int rkOrder = 3/g' order.hpp
cd release
make burgersDensity1d

./burgersDensity1d ../problem_burgersDensity1d3.init

cd ..

cp order.hpp_template order.hpp
sed -i 's/static constexpr int order = 3/static constexpr int order = 4/g' order.hpp
sed -i 's/static constexpr int rkOrder = 3/static constexpr int rkOrder = 4/g' order.hpp
cd release
make burgersDensity1d

./burgersDensity1d ../problem_burgersDensity1d4.init

cd ..

cp order.hpp_template order.hpp
sed -i 's/static constexpr int order = 3/static constexpr int order = 5/g' order.hpp
sed -i 's/static constexpr int rkOrder = 3/static constexpr int rkOrder = 5/g' order.hpp
cd release
make burgersDensity1d

./burgersDensity1d ../problem_burgersDensity1d5.init

cd ..

cp order.hpp_template order.hpp
sed -i 's/static constexpr int order = 3/static constexpr int order = 6/g' order.hpp
sed -i 's/static constexpr int rkOrder = 3/static constexpr int rkOrder = 6/g' order.hpp
cd release
make burgersDensity1d

./burgersDensity1d ../problem_burgersDensity1d6.init

cd ..

cp order.hpp_template order.hpp
sed -i 's/static constexpr int order = 3/static constexpr int order = 6/g' order.hpp
sed -i 's/static constexpr int rkOrder = 3/static constexpr int rkOrder = 3/g' order.hpp
cd release
make burgersDensity1d

./burgersDensity1d ../problem_burgersDensity1d6.init

cd ..

