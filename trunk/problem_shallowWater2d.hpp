#ifndef problem_shallowWater2d_hpp
#define problem_shallowWater2d_hpp

#include <BasisKlassen/parameter.hpp>
#include <BasisKlassen/array.hpp>
#include <BasisKlassen/parseFromFile.hpp>

const int varDim = 3;   // three variables: h, h*ux, h*uy
const int spaceDim = 2; // 2d space

#include "types.hpp"
#include "cfl.hpp"
#include "problemBase2d.hpp"
#include "laxFriedrichs1d.hpp"
#include "gaussLobatto.hpp"
#include "order.hpp"

// 2D isothermal gaz
class Problem_ShallowWater2d : public ProblemBase2d<varDim>
{
public:
  
  struct Var {
    static const int val0 = 0;   // h
    static const int val1 = 1;   // h*ux
    static const int val2 = 2;   // h*uy
  };
  
  struct FluxVar {
    static const int f0x = 0;
    static const int f1x = 1;
    static const int f2x = 2;
    static const int f0y = 3;
    static const int f1y = 4;
    static const int f2y = 5;
    static const int dfx = 6;
    static const int dfy = 7;
    static const int val0 = 8;
    static const int val1 = 9;
    static const int val2 = 10;
  };

  static const int fluxInfoNumber = 11;

  typedef BK::Array<double, fluxInfoNumber> FluxInfo;                 // [f0x, f1x, f2x, f0y, f1y, f2y, dfx, dfy, val0, val1, val2] 
  typedef BK::Array<FluxInfo, 2> FluxInfoBord;                        // [+-][f0x, f1x, f2x, f0y, f1y, f2y, dfx, dfy, val0, val1, val2] 
  typedef BK::MultiArray<spaceDim+1, FluxInfoBord> FluxInfoBordArray; // (cell)[+-][f0x, f1x, f2x, f0y, f1y, f2y, dfx, dfy, val0, val1, val2] 
  typedef BK::MultiArray<spaceDim+1, Vector> FluxVecArray;
  
  friend class BK::StackSingleton<Problem_ShallowWater2d>;

  Problem_ShallowWater2d() {}

  void init(int ncelx, int ncely, std::string parameterFilename) {
    
    initialized_ = true;
    
    ncelx_ = ncelx;
    ncely_ = ncely;

    parameterFilename_ = parameterFilename;

    BK::ReadInitFile initFile(parameterFilename,true);
    rkOrder_ = initFile.getParameter<size_t>("rkOrder");
    lx_ = initFile.getParameter<double>("lx");
    ly_ = initFile.getParameter<double>("ly");
    T_ = initFile.getParameter<double>("T");
    double nt = initFile.getParameter<double>("nt");
    double cflFactor = initFile.getParameter<double>("cfl");
    g_ = initFile.getParameter<double>("g");
    
    if(nt > 0 && T_ > 0) {
      std::cerr << "ERROR in Problem_shallowWater2d::init(): nt = " <<
	nt << " > 0 && T = " << T_ << " > 0." << std::endl;
      exit(1);						   
    }

    cfl_ = cflFactor*cflLimit[rkOrder_-1][order-1];
    
    dx_ = lx_/ncelx;
    dy_ = ly_/ncely;

    assert(dx_ == dy_);
    
    dt_ = std::min(fabs(cfl_*dx_),fabs(cfl_*dx_/(sqrt(g_*1))));

    if(nt > 0) {
      nt_ = nt;
      T_ = nt_*dt_;
    }
    else
      nt_ = int(T_/dt_) + 1;

    dt_ *= T_/(nt_*dt_);

    name_ = "shallowWater2d";

    logParameter();
  }

  Vector initialCondition(double x, double y) {
    return Vector{1+0.1*exp(-BK::sqr(y-ly_/2)/BK::sqr(0.1)), 0, 0};
  }

  void initialize(Array & c) {
    BK::Vector<double> xi = points[order-1];
    BK::Vector<double> wi = weights[order-1];
    
    for(int ix=0;ix<int(ncelx_);ix++) {
      auto transX = [ix, this](double x) { return ix*dx_+0.5*dx_+x*0.5*dx_;};
      for(int iy=0;iy<int(ncely_);iy++) {
	auto transY = [iy, this](double y) { return iy*dy_+0.5*dy_+y*0.5*dy_;};
	
    	for(int ox=0; ox<order;ox++)
    	  for(int oy=0;oy<order;oy++) 
	    c(ix,iy,ox,oy) = initialCondition(transX(xi[ox]), transY(xi[oy]));
    	  
      }
    }
  }

  void getReference(Array & c) {}

  void lhs(Array& cNew, Array const& c, Array const& rhs) const {
    cNew = c + dt_*rhs;
  }    

  VecVec flux_vector(int cellX, int cellY, int indexX, int indexY, Array const& c) {
    double h = c(cellX, cellY, indexX, indexY)[0];
    double hUx = c(cellX, cellY, indexX, indexY)[1];
    double hUy = c(cellX, cellY, indexX, indexY)[2];

    double ux = hUx/h;
    double uy = hUy/h;

    // f0x = hUx; f1x = hUx*ux; f2x = hUx*uy;
    // f0y = hUy; f1y = hUy*ux; f2y = hUy*uy;
    return VecVec{ Vector{hUx, hUx*ux + 0.5*g_*h*h, hUx*uy},
                   Vector{hUy, hUy*ux, hUy*uy + 0.5*g_*h*h} };
  }

  BK::Array<double,2> dFlux_value(int cellX, int cellY, int indexX, int indexY, Array const& c) {
    double h = c(cellX, cellY, indexX, indexY)[0];
    double hUx = c(cellX, cellY, indexX, indexY)[1];
    double hUy = c(cellX, cellY, indexX, indexY)[2];

    double ux = hUx/h;
    double uy = hUy/h;
    double sqrtGH = sqrt(g_*h);

    return BK::Array<double,2>({std::max({fabs(ux), fabs(ux + sqrtGH), fabs(ux - sqrtGH)}), 
	  std::max({fabs(uy), fabs(uy + sqrtGH), fabs(uy - sqrtGH)})});
  }
  
  void fillFluxInfoBordArray(int cellXf, int cellYf, int index, int plusMinus,
			     VecVec const& flux, Array const& c, FluxInfoBordArray& fluxInfoBordArray, int cellX, int cellY, int indexX, int indexY) {
    fluxInfoBordArray(cellXf, cellYf, index)[plusMinus][FluxVar::f0x] = flux[Dir::x][Var::val0];
    fluxInfoBordArray(cellXf, cellYf, index)[plusMinus][FluxVar::f1x] = flux[Dir::x][Var::val1];
    fluxInfoBordArray(cellXf, cellYf, index)[plusMinus][FluxVar::f2x] = flux[Dir::x][Var::val2];
    fluxInfoBordArray(cellXf, cellYf, index)[plusMinus][FluxVar::f0y] = flux[Dir::y][Var::val0];
    fluxInfoBordArray(cellXf, cellYf, index)[plusMinus][FluxVar::f1y] = flux[Dir::y][Var::val1];
    fluxInfoBordArray(cellXf, cellYf, index)[plusMinus][FluxVar::f2y] = flux[Dir::y][Var::val2];
    fluxInfoBordArray(cellXf, cellYf, index)[plusMinus][FluxVar::dfx] = dFlux_value(cellX, cellY, indexX, indexY, c)[Dir::x];
    fluxInfoBordArray(cellXf, cellYf, index)[plusMinus][FluxVar::dfy] = dFlux_value(cellX, cellY, indexX, indexY, c)[Dir::y];
    fluxInfoBordArray(cellXf, cellYf, index)[plusMinus][FluxVar::val0] = c(cellX, cellY, indexX, indexY)[Var::val0];
    fluxInfoBordArray(cellXf, cellYf, index)[plusMinus][FluxVar::val1] = c(cellX, cellY, indexX, indexY)[Var::val1];
    fluxInfoBordArray(cellXf, cellYf, index)[plusMinus][FluxVar::val2] = c(cellX, cellY, indexX, indexY)[Var::val2];
  }
  
  void computeFluxes(FluxArray& fluxArray,
		     FluxInfoBordArray& fluxInfoBordArrayX,
		     FluxInfoBordArray& fluxInfoBordArrayY,
		     Array const& c) {
    
    for(int cellX=0; cellX<int(c.size(0)); cellX++)
      for(int cellY=0; cellY<int(c.size(1)); cellY++)
	for(int indexX=0; indexX<int(c.size(2)); indexX++)
	  for(int indexY=0; indexY<int(c.size(3)); indexY++) {
	    VecVec flux = flux_vector(cellX, cellY, indexX, indexY, c);
	    fluxArray(cellX, cellY, indexX, indexY) = flux;
	    if(indexX == 0) 
	      fillFluxInfoBordArray(cellX, cellY, indexY, Val::plus, flux, c, fluxInfoBordArrayX, cellX, cellY, indexX, indexY);
	    
	    if(indexX == order-1) 
	      fillFluxInfoBordArray(cellX+1, cellY, indexY, Val::minus, flux, c, fluxInfoBordArrayX, cellX, cellY, indexX, indexY);
	    
	    if(indexY == 0) 
	      fillFluxInfoBordArray(cellX, cellY, indexX, Val::plus, flux, c, fluxInfoBordArrayY, cellX, cellY, indexX, indexY);

	    if(indexY == order-1) 
	      fillFluxInfoBordArray(cellX, cellY+1, indexX, Val::minus, flux, c, fluxInfoBordArrayY, cellX, cellY, indexX, indexY);
	    
	  }
  }

  void computeNumericalFlux(FluxVecArray& fluxVecArrayX, FluxVecArray& fluxVecArrayY,
			    FluxInfoBordArray& fluxInfoArrayX, FluxInfoBordArray& fluxInfoArrayY) {

    for(size_t cellX=0; cellX<fluxVecArrayX.size(0); cellX++)
      for(size_t cellY=0; cellY<fluxVecArrayX.size(1); cellY++)
	for(size_t orderY=0; orderY<order; orderY++) {

	  Vector fm;
	  fm[0] = fluxInfoArrayX(cellX, cellY, orderY)[Val::minus][FluxVar::f0x];
	  fm[1] = fluxInfoArrayX(cellX, cellY, orderY)[Val::minus][FluxVar::f1x];
	  fm[2] = fluxInfoArrayX(cellX, cellY, orderY)[Val::minus][FluxVar::f2x];
	  
	  Vector fp;
	  fp[0] = fluxInfoArrayX(cellX, cellY, orderY)[Val::plus][FluxVar::f0x];
	  fp[1] = fluxInfoArrayX(cellX, cellY, orderY)[Val::plus][FluxVar::f1x];
	  fp[2] = fluxInfoArrayX(cellX, cellY, orderY)[Val::plus][FluxVar::f2x];
	    
	  double dfm = fluxInfoArrayX(cellX, cellY, orderY)[Val::minus][FluxVar::dfx];
	  double dfp = fluxInfoArrayX(cellX, cellY, orderY)[Val::plus][FluxVar::dfx];

	  Vector valM;
	  valM[0] = fluxInfoArrayX(cellX, cellY, orderY)[Val::minus][FluxVar::val0];
	  valM[1] = fluxInfoArrayX(cellX, cellY, orderY)[Val::minus][FluxVar::val1];
	  valM[2] = fluxInfoArrayX(cellX, cellY, orderY)[Val::minus][FluxVar::val2];

	  Vector valP;
	  valP[0] = fluxInfoArrayX(cellX, cellY, orderY)[Val::plus][FluxVar::val0];
	  valP[1] = fluxInfoArrayX(cellX, cellY, orderY)[Val::plus][FluxVar::val1];
	  valP[2] = fluxInfoArrayX(cellX, cellY, orderY)[Val::plus][FluxVar::val2];

	  fluxVecArrayX(cellX, cellY, orderY) =  laxFriedrichs1d_flux_numeric(fm, fp, dfm, dfp, valM, valP);
	}

    for(size_t cellX=0; cellX<fluxVecArrayY.size(0); cellX++)
      for(size_t cellY=0; cellY<fluxVecArrayY.size(1); cellY++)
	for(size_t orderX=0; orderX<order; orderX++) {
	  Vector fm;
	  fm[0] = fluxInfoArrayY(cellX, cellY, orderX)[Val::minus][FluxVar::f0y];
	  fm[1] = fluxInfoArrayY(cellX, cellY, orderX)[Val::minus][FluxVar::f1y];
	  fm[2] = fluxInfoArrayY(cellX, cellY, orderX)[Val::minus][FluxVar::f2y];
	  
	  Vector fp;
	  fp[0] = fluxInfoArrayY(cellX, cellY, orderX)[Val::plus][FluxVar::f0y];
	  fp[1] = fluxInfoArrayY(cellX, cellY, orderX)[Val::plus][FluxVar::f1y];
	  fp[2] = fluxInfoArrayY(cellX, cellY, orderX)[Val::plus][FluxVar::f2y];
	    
	  double dfm = fluxInfoArrayY(cellX, cellY, orderX)[Val::minus][FluxVar::dfy];
	  double dfp = fluxInfoArrayY(cellX, cellY, orderX)[Val::plus][FluxVar::dfy];

	  Vector valM;
	  valM[0] = fluxInfoArrayY(cellX, cellY, orderX)[Val::minus][FluxVar::val0];
	  valM[1] = fluxInfoArrayY(cellX, cellY, orderX)[Val::minus][FluxVar::val1];
	  valM[2] = fluxInfoArrayY(cellX, cellY, orderX)[Val::minus][FluxVar::val2];

	  Vector valP;
	  valP[0] = fluxInfoArrayY(cellX, cellY, orderX)[Val::plus][FluxVar::val0];
	  valP[1] = fluxInfoArrayY(cellX, cellY, orderX)[Val::plus][FluxVar::val1];
	  valP[2] = fluxInfoArrayY(cellX, cellY, orderX)[Val::plus][FluxVar::val2];

	  fluxVecArrayY(cellX, cellY, orderX) =  laxFriedrichs1d_flux_numeric(fm, fp, dfm, dfp, valM, valP);
	}
  }

  PROTEPARA(double, g);
};

#endif
