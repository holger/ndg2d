// g++ -o main main.cpp -g -Wall -std=c++11 -DCPP11

#include <iostream>
#include <cstdlib>
#include <cmath>
#include <vector>
#include <fstream>
#include <functional>
#include <chrono>
#include <mpi.h>

#include <BasisKlassen/parseFromFile.hpp>
#include <BasisKlassen/mpiBase.hpp>

#include "gaussLobatto.hpp"
#include "writeArray3d.hpp"
#include "geo_cart3d.hpp"
#include "notiModify.hpp"

FieldNotify& notifyField = BK::StackSingleton<FieldNotify>::instance();
BK::MpiBase& mpiBase = BK::StackSingleton<BK::MpiBase>::instance();
XFluxMPI3d& xFluxMPI = BK::StackSingleton<XFluxMPI3d>::instance();
YFluxMPI3d& yFluxMPI = BK::StackSingleton<YFluxMPI3d>::instance();
ZFluxMPI3d& zFluxMPI = BK::StackSingleton<ZFluxMPI3d>::instance();

int main(int argc, char** argv)
{
  mpiBase.init(argc,&argv);

  int commRank = mpiBase.get_commRank();
  
  if (commRank == 0 ) 
    std::cout << "Discontinuous Galerkin simulation starts ...\n";
  
  if(argc < 2) {
    std::cerr << "ERROR: argc = " << argc << " < 2!.\n";
    std::cerr << "usage: main <parameterFilename>\n";
    exit(1);
  }

  std::string parameterFilename = argv[1];
  BK::ReadInitFile initFile(parameterFilename,true);
  std::string pathToBaseInitFile = initFile.getParameter<std::string>("problemBase3dInitFilePath");
  BK::path path(parameterFilename);
  BK::path branch_path = path.branch_path();
  //    std::cout << "branch_path = " << branch_path.string() << std::endl;
  std::string parameterBaseFilename = branch_path.string() + "/problemBase3d.init";
  BK::ReadInitFile initBaseFile(parameterBaseFilename,true);

  int ncelxMin = initBaseFile.getParameter<int>("ncelxMin");
  int ncelxMax = initBaseFile.getParameter<int>("ncelxMax");
  // int ncelyMin = initFile.getParameter<int>("ncelyMin");
  // int ncelyMax = initFile.getParameter<int>("ncelyMax");
  // int ncelzMin = initFile.getParameter<int>("ncelzMin");
  // int ncelzMax = initFile.getParameter<int>("ncelzMax");
  
  for(int ncelx=ncelxMin;ncelx<=ncelxMax;ncelx*=2) {
    
   int  ncely=ncelx;
   int  ncelz=ncelx;
   
   problem.init(ncelx, ncely, ncelz, parameterFilename);
   
   // int m=2;
   int nPoParCel=10;
   
   Problem::Matrix u(ncelx*nPoParCel,ncely*nPoParCel,ncelz*nPoParCel);
   Problem::Matrix pos(ncelx*nPoParCel,ncely*nPoParCel,ncelz*nPoParCel);
   Problem::Matrix uref(ncelx*nPoParCel,ncely*nPoParCel,ncelz*nPoParCel);
   
   int mkdir = system(BK::toString("mkdir data").c_str());
   std::string dirName = BK::toString("data/",problem.get_name());
   mkdir = system(BK::toString("mkdir ",dirName).c_str());
   std::cerr << mkdir << std::endl;
   
   // writeArray(c, dirName + "/c.data");

   GeoCart3d geoCart(ncelx, ncely, ncelz, parameterBaseFilename);
   geoCart.init();

   size_t rkOrder = problem.get_rkOrder();
   double dx = problem.get_dx();
   double dy = problem.get_dy();
   double dz = problem.get_dz();
   
   writeFine(geoCart.get_c(), dirName + "/init_" +  std::to_string(order) + "-" +  std::to_string(rkOrder) + "_" + std::to_string(ncelx) + "_" + std::to_string(ncely)
	     + "_" + std::to_string(ncelz), nPoParCel, dx, dy, dz);
   
   auto start = std::chrono::system_clock::now();

   geoCart.solve();
   
   std::cout << "time = " << geoCart.get_time() << std::endl;
   
   auto end = std::chrono::system_clock::now();
   
   std::chrono::duration<double> elapsed_seconds = end-start;
   std::time_t end_time = std::chrono::system_clock::to_time_t(end);
   
   std::cout << "finished computation at " << std::ctime(&end_time)
	     << "elapsed time: " << elapsed_seconds.count() << "s\n";
   
   std::string fileName = BK::toString(dirName + "/chrono-ndg-",order,"-",rkOrder,".txt");
   std::ofstream out(fileName.c_str(), std::ios::app);
   out << ncelx << "\t" << elapsed_seconds.count() << std::endl;
   out.close();  

   notifyField.exec("fieldFinal", &geoCart.get_c());
   
   // for(int o=0;o<order;o++) {
   //   for(int i=0;i<ncelx;i++) {
   // 	std::cout << c(i,0,0,o,0,0)[0] << "\t";
   //   }
   //   std::cout << std::endl;
   // }
   
   // writeArray(c, dirName + "/cnew.data");
   writeFine(geoCart.get_c(), dirName + "/sol_" +  std::to_string(order) + "-" + std::to_string(rkOrder) +"_" + std::to_string(ncelx) + "_" + std::to_string(ncely)
	     + "_" + std::to_string(ncelz)+ "_" + std::to_string(mpiBase.get_commRank()), nPoParCel, dx, dy, dz);

#ifdef NETCDF
   writeNetCDF(geoCart.get_c(), dirName + "/sol_" +  std::to_string(order) + "-" + std::to_string(rkOrder) +"_" + std::to_string(ncelx) + "_" + std::to_string(ncely)
	       + "_" + std::to_string(ncelz)+ "_" + std::to_string(mpiBase.get_commRank()), ncelx, ncely, ncelz, dx, dy, dz);
#endif

   // writeBin(c, dirName + "/sol_" +  std::to_string(order) + "-" + std::to_string(rkOrder) +"_" + std::to_string(ncelx) + "_" + std::to_string(ncely), 100);

   // -----------------------------------------------------------------------------------------------------------
     
   // double errResult2=0;
   // for(int i=0;i<ncel;i++){
   //   double xi=i*dx;
   //   for(int j=0;j<nPoParCel;j++){
   // 	u[i*nPoParCel + j]=c(0,i)+(-1 + 2./(nPoParCel)*j)*c(1,i)+0.5*(3*(2./nPoParCel*j-1)*(2./nPoParCel*j-1)-1)*c(2,i);
   // 	double x=xi+j*(dx/nPoParCel);
   // 	posx[i*nPoParCel+j]=x;
   // 	errResult2+=pow((sin(2*M_PI*x/L)-u[i*nPoParCel + j]),2)*dx;
   // 	uref[i*nPoParCel + j]=sin(2*M_PI*x/L);
   //   }
	
   // }
   // double errResult=sqrt(errResult2);
   // filename = "unew_" + std::to_string(ncel) + ".data";
   // out.open(filename);
   // for(int i=0;i<ncel*nPoParCel;i++){
   //   out<<posx[i]<<"\t"<<u[i]<<"\t"<<uref[i]<<std::endl;
   // }
      
   // out.close();

   // out.open(errResultfilename,std::ios::app);
   // out<<ncel<<"\t"<<errResult<<std::endl;
   // out.close();
  }
  return 0;
}


 
