#ifndef problemBase1d_hpp
#define problemBase1d_hpp

#include <BasisKlassen/logger.hpp>

#include "types.hpp"
#include "writeArray1d.hpp"
#include "lagrange.hpp"

template<int varDim>
class ProblemBase1d : public DataTypes<1, varDim>
{
public:

  typedef typename DataTypes<1, varDim>::Vector Vector;
  typedef typename DataTypes<1, varDim>::Array Array;

  ProblemBase1d() {
    initialized_ = false;
    
    varDim_ = varDim;

    time_ = 0;
    step_ = 0;
  }

  void init(int ncel, std::string const& parameterFilename) {
    ncel_ = ncel;

    parameterFilename_ = parameterFilename;

    BK::ReadInitFile initFile(parameterFilename,true);
    rkOrder_ = initFile.getParameter<size_t>("rkOrder");
    lx_ = initFile.getParameter<double>("lx");
    nt_ = initFile.getParameter<int>("nt");
    T_ = initFile.getParameter<double>("T");
    cflFactor_ = initFile.getParameter<double>("cfl");
    numFluxType_ = initFile.getParameter<int>("numFluxType");
    writeTimeInterval_ = initFile.getParameter<double>("writeTimeInterval");
    writeStepInterval_ = initFile.getParameter<int>("writeStepInterval");
    dirName_ = BK::toString("data/", name_);
    
    std::cout << "using numFluxType = " << numFluxType_ << std::endl;
    
    if(nt_ > 0 && T_ > 0) {
      std::cerr << "ERROR in Problem_Advection1d::init(): nt = " <<
	nt_ << " > 0 && T = " << T_ << " > 0." << std::endl;
      exit(1);						   
    }

    cfl_ = cflFactor_*cflLimit[rkOrder_-1][order-1];
    dx_ = lx_/ncel_;

    dLagrangeMatrix.init();

    if(!initialized_)
      notifyField.add("field", FieldNotifyCallBack(std::bind(&ProblemBase1d::write,this,std::placeholders::_1), "ProblemBase1d::write"));
  }

  void computeSourceTerm(Array const& c, Array & s) const {}
  
  template<typename Functor>
  double derivative(Functor uFunc, Array const& c, int cell, int index) const {
    double du=0;
    
    for(int j=0; j<order;j++) {
      double u = uFunc(c, cell, j);
      
      du += dLagrangeMatrix(j, index)*u;
    }
    
    du = du/dx_*2;
    
    return du;
  }

  void initialize(Array & c, std::function<Vector(double)> initialCondition) {
    BK::Vector<double> xi = points[order-1];
    BK::Vector<double> wi = weights[order-1];

    for(size_t ix=0; ix<ncel_; ix++) {
      auto transX = [ix, this](double x) { return ix*dx_+0.5*dx_+x*0.5*dx_;};
	
      for(size_t ox=0; ox<order; ox++) {
  	c(ix,ox) = initialCondition(transX(xi[ox]));
      }
    }
  }

  void write(Array const* c) {
    if((writeTimeInterval_ > 0 && fmod(time_, writeTimeInterval_) < dt_) || (writeStepInterval_ > 0 && (step_ % writeStepInterval_) == 0)) {
      // std::cout << "Burgers1d::write(Array const* c)\n";
      // std::cout << "step = " << step_ << std::endl;
      // std::cout << "time = " << time_ << std::endl;
      std::string dirName = BK::toString("data/",name_);

      int nPoParCel=10;
      writeFine(*c, dirName + "/sol_" +  std::to_string(order) + "-" + std::to_string(rkOrder_) +"_" + std::to_string(ncel_) + "-" + std::to_string(step_), nPoParCel, dx_);
    }
  }

  template<typename FluxInfoVecBordArray, typename Array>
  void compute_fluxInfoArray(FluxInfoVecBordArray& fluxInfoArray, Array const& c)
  {
    for(size_t cell=0; cell<ncel_; cell++) {
      fluxInfoArray(cell+1)[Val::minus] = c(cell, size_t(order-1));
      fluxInfoArray(cell)[Val::plus] = c(cell, size_t(0));
    }
  }

  template<typename FluxVecArray, typename FluxInfoVecBordArray, typename FluxFunc, typename DFluxFunc>
  void compute_fluxArray_LF(FluxVecArray& fluxArray, FluxInfoVecBordArray const& fluxInfoArray, FluxFunc fluxFunc, DFluxFunc dFluxFunc)
  {
    for(size_t cellInterface=0; cellInterface<ncel_; cellInterface++) {
      Vector valL = fluxInfoArray(cellInterface)[Val::minus];
      Vector valR = fluxInfoArray(cellInterface)[Val::plus];
      Vector fl = fluxFunc(valL, cellInterface, 0)[0];
      Vector fr = fluxFunc(valR, cellInterface, 0)[0];
      double dfl = dFluxFunc(valL, cellInterface, 0);
      double dfr = dFluxFunc(valR, cellInterface, 0);
    
      fluxArray(cellInterface) =  laxFriedrichs1d_flux_numeric(fl, fr, dfl, dfr, valL, valR);
    }

    size_t cellInterface = ncel_;
    Vector valL = fluxInfoArray(cellInterface)[Val::minus];
    Vector valR = fluxInfoArray(cellInterface)[Val::plus];
    Vector fl = fluxFunc(valL, cellInterface-1, order-1)[0];
    Vector fr = fluxFunc(valR, cellInterface-1, order-1)[0];
    double dfl = dFluxFunc(valL, cellInterface-1, order-1);
    double dfr = dFluxFunc(valR, cellInterface-1, order-1);
    
    fluxArray(cellInterface) =  laxFriedrichs1d_flux_numeric(fl, fr, dfl, dfr, valL, valR);

  }

  template<typename FluxVecArray, typename FluxInfoVecBordArray, typename FluxFunc>
  void compute_fluxArray_Central(FluxVecArray& fluxArray, FluxInfoVecBordArray const& fluxInfoArray, FluxFunc fluxFunc)
  {
    for(size_t cellInterface=0; cellInterface<ncel_+1; cellInterface++) {
      Vector valM = fluxInfoArray(cellInterface)[Val::minus];
      Vector valP = fluxInfoArray(cellInterface)[Val::plus];
      
      Vector fm = fluxFunc(valM)[0];
      Vector fp = fluxFunc(valP)[0];
      
      fluxArray(cellInterface) = central1d_flux_numeric(fm, fp);
    }
  }
  
  void logParameter() {
    assert(initialized_);
    
    std::cerr << "name = " << name_ << std::endl;
    std::cerr << PAR(parameterFilename_) << std::endl;
    std::cerr << "ncel = " << ncel_ << std::endl;
    std::cerr << "lx = " << lx_ << std::endl;
    std::cerr << "T = " << T_ << "\t cfl = " << cfl_ << "\t dt = " << dt_ << std::endl;
    std::cerr << "nt = " << nt_ << std::endl;
    std::cerr << "dx = " << dx_ << std::endl;
    std::cerr << "dt = " << dt_ << std::endl;
    std::cerr << "nt = " << nt_ << std::endl;
    std::cerr << "cflFactor = " << cflFactor_ << std::endl;
    std::cerr << "cfl = " << cfl_ << std::endl;
    std::cerr << "order = " << order << std::endl;
    std::cerr << "rkOrder = " << rkOrder_ << std::endl;
    std::cerr << "writeTimeInterval_ = " << writeTimeInterval_ << std::endl;
    std::cerr << "writeStepInterval_ = " << writeStepInterval_ << std::endl;
  }

  PROTEPARA(std::string, parameterFilename);
  PROTEPARA(std::string, name);
  PROTEPARA(std::string, dirName);
  PROTEPARA(size_t, ncel);
  PROTEPARA(double, lx);
  PROTEPARA(double, dx);
  PROTEPARA(double, T);
  PROTEPARA(double, dt);
  PROTEPARA(double, cfl);
  PROTEPARA(double, cflFactor);
  PROTEPARA(int, nt);
  PROTEPARA(bool, initialized);
  PROTEPARA(int, varDim);
  PROTEPARA(int, rkOrder);
  PROTEPARA(double, writeTimeInterval);
  PROTEPARA(int, writeStepInterval);
  PROTEPARA(double, time);
  PROTEPARA(int, step);
  PROTEPARA(int, numFluxType);

  DLagrangeMatrix<order> dLagrangeMatrix;
};

#endif
