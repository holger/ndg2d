#ifndef problem_advection1d_hpp
#define problem_advection1d_hpp

#include <BasisKlassen/parameter.hpp>
#include <BasisKlassen/singleton.hpp>
#include <BasisKlassen/assert.hpp>

const int varDim = 1;
const int spaceDim = 1;

#include "cfl.hpp"
#include "laxFriedrichs1d.hpp"
#include "notiModify.hpp"
#include "problemBase1d.hpp"

// linear advection 1D
class Problem_Advection1d : public ProblemBase1d<varDim>
{
public:

  enum Var {c};

  friend class BK::StackSingleton<Problem_Advection1d>;
  
  Problem_Advection1d() { }

  void errorComp(Array const* c) {
    
    Array cRef(ncel_,order);

    std::function<Vector(double)> initialFunction = std::bind( &Problem_Advection1d::initialCondition, *this, std::placeholders::_1);
    initialize(cRef, initialFunction);

     // writeFine(cRef, dirName + "/ref_" +  std::to_string(order) + "-" + std::to_string(rkOrder) +"_" + std::to_string(ncelx), nPoParCel);
    
    double error = 0;
    size_t nx = c->size(0);
    for(size_t ix = 0; ix < nx; ix++) {
      error += BK::sqr(value(ix, 0.,*c)[0] - value(ix, 0., cRef)[0])*dx_;
    }
    error = sqrt(error);

    std::string dirName = BK::toString("data/", name_);
    
    std::cerr << "writing error to " << BK::toString(dirName + "/error-ndg-",order,"-",rkOrder_,".txt") << std::endl;
    std::ofstream outChrono(BK::toString(dirName + "/error-ndg-",order,"-",rkOrder_,".txt"), std::ios::app);
    outChrono << ncel_ << "\t" << error << std::endl;
    outChrono.close();
  }
  
  void init(int ncel, std::string parameterFilename) {
    if(!initialized_)
      notifyField.add("fieldFinal", FieldNotifyCallBack(std::bind(&Problem_Advection1d::errorComp,this,std::placeholders::_1), "Problem_Advection1d::errorComp"));

    name_ = "advection1d";

    ProblemBase1d::init(ncel, parameterFilename);

    // computing dt
    dt_ = fabs(cfl_*dx_);
    if(nt_ > 0) {
      T_ = nt_*dt_;
    }
    else {
      nt_ = int(T_/dt_) + 1;
    }
    dt_ *= T_/(nt_*dt_);

    initialized_ = true;
    
    std::string dirName = BK::toString("data/", name_);
    std::ofstream out(BK::toString(dirName,"/velocity.txt"));
    BK::Vector<double> xi = points[order-1];
    
    velArray.resize(ncel_, order);
    for(size_t cell=0; cell < ncel_; cell++) 
      for(size_t index=0; index < order; index++) {
	auto transX = [cell, this](double x) { return cell*dx_+0.5*dx_+x*0.5*dx_;};
	velArray(cell, index) = velocity(transX(xi[index]));
	out << transX(xi[index]) << "\t" << velArray(cell, index) << std::endl;
      }

    out.close();

    logParameter();
    
    std::cerr << "ax = " << ax_ << std::endl;
  }

  double velocity(double x) {
    assert(initialized_);
    // double x0 = 0.35;
    // double y0 = 0.35;

    // int kMax = 10;
    // double temp = 0;
    // for(int k=1; k<=kMax; k++)
    //   temp += pow(k,-5./6)*sin(2*M_PI*k/lambda_*(x-shift-2*M_PI/(k*k)));

    // return temp;

    int k;
    bool firstTime = true;
    if(firstTime) {
      BK::ReadInitFile initFile(parameterFilename_,true);
      k = initFile.getParameter<double>("k");      
      firstTime = false;
    }
    
    //   return sin(2*M_PI*k*x/lx_);
    return 1;
  }

  Vector initialCondition(double x) {
    assert(initialized_);
    
    return initialConditionShift(x,0);
  }

  Vector initialConditionShift(double x, double shift) {
    assert(initialized_);
    // double x0 = 0.35;
    // double y0 = 0.35;

    // int kMax = 10;
    // double temp = 0;
    // for(int k=1; k<=kMax; k++)
    //   temp += pow(k,-5./6)*sin(2*M_PI*k/lambda_*(x-shift-2*M_PI/(k*k)));

    // return temp;

    int k;
    bool firstTime = true;
    if(firstTime) {
      BK::ReadInitFile initFile(parameterFilename_,true);
      k = initFile.getParameter<double>("k");      
      firstTime = false;
    }
    
    return Vector{sin(2*M_PI*k*(x-shift)/lx_)};
    // return Vector{sin(2*M_PI*y/ly)};

    //    return Vector{exp(-((x-x0)*(x-x0)+(y-y0)*(y-y0))/(0.1*0.1))};
  }

  void lhs(Array& cNew, Array const& c, Array const& rhs) const {
    cNew = c + dt_*rhs;
  }    
  
  VecVec flux_vector(Vector const& varArray, size_t cell, size_t index) {
    assert(initialized_);
    
    return {{velArray(cell, index)*varArray}};
  }

  double dFlux_value(Vector const& varArray, size_t cell, size_t index) {
    assert(initialized_);
    
    return fabs(velArray(cell, index));
  }
  

  void compute_fluxArray(FluxVecArray& fluxArray, FluxInfoBordArray const& fluxInfoArray)
  {
    switch(numFluxType_) {
    case 0: {
      auto fluxFunc = [this](Vector const& varArray, size_t cell, size_t index) { return this->flux_vector(varArray, cell, index);};
      auto dfluxFunc = [this](Vector const& varArray, size_t cell, size_t index) { return this->dFlux_value(varArray, cell, index);};
      compute_fluxArray_LF(fluxArray, fluxInfoArray, fluxFunc, dfluxFunc);

      break;
    }
    case 1: compute_fluxArray_Godunov(fluxArray, fluxInfoArray); break;
    }
  }
  
  void compute_fluxArray_Godunov(FluxVecArray& fluxArray, FluxInfoBordArray const& fluxInfoArray)
  {    
    for(size_t cellInterface=0; cellInterface<ncel_; cellInterface++) {
      double val;
      if(velArray(cellInterface,size_t(0)) >= 0) 
  	val = fluxInfoArray(cellInterface)[Val::minus][Var::c];
      else 
  	val = fluxInfoArray(cellInterface)[Val::plus][Var::c];
      
      fluxArray(cellInterface) = flux_vector(val, cellInterface, 0)[0][0];	
    }

    size_t cellInterface = ncel_;
    double val;
    if(velArray(cellInterface-1,size_t(order-1)) >= 0) 
      val = fluxInfoArray(cellInterface)[Val::minus][Var::c];
    else 
      val = fluxInfoArray(cellInterface)[Val::plus][Var::c];
    
    fluxArray(cellInterface) = flux_vector(val, cellInterface-1, order-1)[0][0];	
  }

  BK::MultiArray<spaceDim*2, double> velArray;
  CONSTPARA(double, ax);
  CONSTPARA(int, numFluxType);
};

typedef Problem_Advection1d Problem;
extern Problem& problem;

#endif
