#ifndef problem_burgersDensity2d_hpp
#define problem_burgersDensity2d_hpp

#include <BasisKlassen/parameter.hpp>
#include <BasisKlassen/array.hpp>

const int varDim = 3;   // three variables: rho, rho*ux, rho*uy
const int spaceDim = 2; // 2d space

#include "types.hpp"
#include "cfl.hpp"
#include "problemBase2d.hpp"
#include "laxFriedrichs1d.hpp"
#include "gaussLobatto.hpp"

// Burgers with density 2D
class Problem_BurgersDensity2d : public ProblemBase2d<varDim>
{
public:
  
  struct Var {
    static const int rho = 0;     // rho
    static const int rhoUx = 1;   // rho*ux
    static const int rhoUy = 2;   // rho*uy
  };
  
  friend class BK::StackSingleton<Problem_BurgersDensity2d>;

  Problem_BurgersDensity2d() {}

  void init(int ncelx, int ncely, std::string parameterFilename) {

    name_ = "burgersDensity2d";    

    ProblemBase2d::init(ncelx, ncely, parameterFilename);
    
    double nt = nt_;
    
    if(nt > 0 && T_ > 0) {
      std::cerr << "ERROR in Problem_burgersDensity2d::init(): nt = " <<
	nt << " > 0 && T = " << T_ << " > 0." << std::endl;
      exit(1);						   
    }

    assert(dx_ == dy_);
    
    dt_ = fabs(cfl_*dx_);

    if(nt > 0) {
      nt_ = nt;
      T_ = nt_*dt_;
    }
    else
      nt_ = int(T_/dt_) + 1;

    dt_ *= T_/(nt_*dt_);

    logParameter();
    
    initialized_ = true;
  }

  Vector initialCondition(double x, double y) {
    return Vector{1, sin(2*M_PI*x/lx_), 0};
  }

  void getReference(Array & c) {}

  void lhs(Array& cNew, Array const& c, Array const& rhs) const {
    cNew = c + dt_*rhs;
  }    

  VecVec flux_vector(Vector const& varArray) {
    double rho = varArray[Var::rho];
    double rhoUx = varArray[Var::rhoUx];
    double rhoUy = varArray[Var::rhoUy];

    double ux = rhoUx/rho;
    double uy = rhoUy/rho;

    // f0x = rhoUx; f1x = rhoUx*ux; f2x = rhoUx*uy;
    // f0y = rhoUy; f1y = rhoUy*ux; f2y = rhoUy*uy;
    return VecVec{ Vector{rhoUx, rhoUx*ux, rhoUx*uy},
                   Vector{rhoUy, rhoUy*ux, rhoUy*uy} };
  }

  BK::Array<double,2> dFlux_value(Vector const& varArray) {
    double rho = varArray[Var::rho];
    double rhoUx = varArray[Var::rhoUx];
    double rhoUy = varArray[Var::rhoUy];

    double ux = rhoUx/rho;
    double uy = rhoUy/rho;

    return BK::Array<double,2>({ux, uy});
  }
  
  void computeNumericalFlux(FluxVecArray& fluxVecArrayX, FluxVecArray& fluxVecArrayY,
			    FluxInfoBordArray& fluxInfoArrayX, FluxInfoBordArray& fluxInfoArrayY) {

    auto fluxFunc = [this](Vector const& varArray) { return this->flux_vector(varArray);};
    auto dfluxFunc = [this](Vector const& varArray) { return this->dFlux_value(varArray);};
    
    computeNumericalFlux_LF(fluxVecArrayX, fluxVecArrayY, fluxInfoArrayX, fluxInfoArrayY, fluxFunc, dfluxFunc);
  }
};

typedef Problem_BurgersDensity2d Problem;
extern Problem& problem;

#endif
