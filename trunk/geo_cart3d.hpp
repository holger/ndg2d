#ifndef geo_cart3d_hpp
#define geo_cart3d_hpp

#include "dg3d.hpp"
#include "rk.hpp"
#include "problem3d.hpp"
#include "boundaryCondition3d.hpp"
#include "order.hpp"
#include "mpiBase.hpp"
#include "logger.hpp"

// LOGGING: ----------------------------------------------------
// #undef LOGLEVEL
// #define LOGLEVEL 4

Problem& problem = BK::StackSingleton<Problem>::instance();

class GeoCart3d
{

public:

  GeoCart3d(size_t ncelX, size_t ncelY, size_t ncelZ, std::string parameterFilename) : ncelX_(ncelX), ncelY_(ncelY), ncelZ_(ncelZ), parameterFilename_(parameterFilename) {}
  
  void init() {
    LOGL(1,"GeoCart3d::init()");
    
    std::fill(c.begin(), c.end(), 0);

    BK::ReadInitFile initFile(parameterFilename_,true);

    lx_ = initFile.getParameter<double>("lx");
    ly_ = initFile.getParameter<double>("ly");
    lz_ = initFile.getParameter<double>("lz");

    dx_ = lx_/ncelX_;
    dy_ = ly_/ncelY_;
    dz_ = ly_/ncelZ_;
    
    mpiBase.MPICartdimCalcul(3) ;
    
    int ncelXloc = ncelX_ /  mpiBase.get_dimSize()[0] ; // local here
    int ncelYloc = ncelY_ /  mpiBase.get_dimSize()[1] ; // local here
    int ncelZloc = ncelZ_ /  mpiBase.get_dimSize()[2] ; // local here
    //int ncelYloc = ncelY_ ;
    c.resize(ncelXloc, ncelYloc, ncelZloc, order, order, order);
    initialize(c);

    std::cerr << "lx = " << lx_ << std::endl;
    std::cerr << "ly = " << ly_ << std::endl;
    std::cerr << "lz = " << lz_ << std::endl;
    std::cerr << "dx = " << dx_ << std::endl;
    std::cerr << "dy = " << dy_ << std::endl;
    std::cerr << "dz = " << dz_ << std::endl;

    LOGL(4,"GeoCart3d::init()-end");
  }

  void initialize(Problem::Array & c) {
    LOGL(2,"GeoCart3d::initialize(Problem::Array & c)");
    
    BK::Vector<double> xi = points[order-1];
    BK::Vector<double> wi = weights[order-1];
    
    int ncelXloc = ncelX_ /  mpiBase.get_dimSize()[0] ; // local here
    int ncelYloc = ncelY_ /  mpiBase.get_dimSize()[1] ; // local here
    int ncelZloc = ncelZ_ /  mpiBase.get_dimSize()[2] ; // local here
    //int ncelYloc = ncelY_ ;

    //double startvalx = 0.0 +  mpiBase.getCartCoords(mpiBase.get_commRank())[0] * (lx_/ mpiBase.get_dimSize()[0]);
    //double startvaly = 0.0 +  mpiBase.getCartCoords(mpiBase.get_commRank())[1] * (ly_/ mpiBase.get_dimSize()[1]);
    double startvalx = 0.0 +  mpiBase.procCoord()[0] * (lx_/ mpiBase.get_dimSize()[0]);
    double startvaly = 0.0 +  mpiBase.procCoord()[1] * (ly_/ mpiBase.get_dimSize()[1]);
    double startvalz = 0.0 +  mpiBase.procCoord()[2] * (lz_/ mpiBase.get_dimSize()[2]);

    /*if (mpiBase.get_commRank() == 0) {
      std::cout<<mpiBase.get_commRank() <<"\t" <<mpiBase.procCoord()[0] << "\t" <<mpiBase.procCoord()[1] << "\t" <<  startvalx <<","<< startvaly << std::endl;  };
    if (mpiBase.get_commRank() == 1) {
      std::cout<<mpiBase.get_commRank() <<"\t" <<mpiBase.procCoord()[0] << "\t" <<mpiBase.procCoord()[1] << "\t" <<  startvalx <<","<< startvaly << std::endl;  };
    if (mpiBase.get_commRank() == 2) {
      std::cout<<mpiBase.get_commRank() <<"\t" <<mpiBase.procCoord()[0] << "\t" <<mpiBase.procCoord()[1] << "\t" <<  startvalx <<","<< startvaly << std::endl;  } ;
    if (mpiBase.get_commRank() == 3) {
      std::cout<<mpiBase.get_commRank() <<"\t" <<mpiBase.procCoord()[0] << "\t" <<mpiBase.procCoord()[1] << "\t" <<  startvalx <<","<< startvaly << std::endl;  } ;
      exit(0);*/
    
    for(int ix=0;ix<int(ncelXloc);ix++) {
      auto transX = [ix, startvalx, this](double x) { return startvalx + ix*dx_+0.5*dx_+x*0.5*dx_;};
      for(int iy=0;iy<int(ncelYloc);iy++) {
	auto transY = [iy, startvaly, this](double y) { return startvaly + iy*dy_+0.5*dy_+y*0.5*dy_;};
	for(int iz=0;iz<int(ncelZloc);iz++) {
	  auto transZ = [iz, startvalz, this](double z) { return startvalz + iz*dz_+0.5*dz_+z*0.5*dz_;};
	  
	  for(int ox=0; ox<order;ox++)
	    for(int oy=0;oy<order;oy++)
	      for(int oz=0;oz<order;oz++) 
		c(ix,iy,iz, ox,oy,oz) = problem.initialCondition(transX(xi[ox]), transY(xi[oy]), transZ(xi[oz]));    	  
	}
      }
    }
    
    LOGL(4,"GeoCart3d::initialize(Problem::Array & c)-end");
  }
  
  void solve() {
    LOGL(2,"GeoCart3d::solve()");
    
    Problem::Array cnew(c);
    
    Rhs3d rhs3d(c.size(0), c.size(1), c.size(2), problem.get_dx(), problem.get_dy(), problem.get_dz());
    
    auto dimensionsX = rhs3d.fluxInfoBordArrayX.dim();
    int ncelx = dimensionsX[0];
    int ncely = dimensionsX[1];
    int ncelz = dimensionsX[2];
    xFluxMPI.init(ncelx, ncely, ncelz);

    auto dimensionsY = rhs3d.fluxInfoBordArrayY.dim();
    ncelx = dimensionsY[0];
    ncely = dimensionsY[1];
    ncelz = dimensionsY[2];
    yFluxMPI.init(ncelx, ncely, ncelz);

    auto dimensionsZ = rhs3d.fluxInfoBordArrayZ.dim();
    ncelx = dimensionsZ[0];
    ncely = dimensionsZ[1];
    ncelz = dimensionsZ[2];
    zFluxMPI.init(ncelx, ncely, ncelz);

    PeriodicBoundaryCondition3dFlux periodicBoundaryCondition3d;
    
    double dt = problem.get_dt();
    
    std::unique_ptr<Rk> rk = getRkObject(dt, cnew);
    
    time_ = 0;
    cnew=c;
    
    for(int t=0;t<problem.get_nt();t++){
      if(mpiBase.get_commRank() == 0)
	std::cout << "t = " << t << "\t time = " << time_ << std::endl;
      
      Problem::Array rhs(c.dim());
      for(int stage = 0; stage < rk->get_stageNumber(); stage++) {
	problem.computeFluxes(rhs3d.fluxArray, rhs3d.fluxInfoBordArrayX, rhs3d.fluxInfoBordArrayY, rhs3d.fluxInfoBordArrayZ, cnew, problem);
	periodicBoundaryCondition3d(rhs3d.fluxInfoBordArrayX, rhs3d.fluxInfoBordArrayY, rhs3d.fluxInfoBordArrayZ);
	rhs3d(rhs);
      	rk->advance(cnew, c, rhs);
      }
      c=cnew;
      
      time_ += dt;
    }

    LOGL(4,"GeoCart3d::solve()-end");
  }

  Problem::Array const& get_c() {
    return c;
  }

private:
  CONSTPARA(size_t, ncelX);
  CONSTPARA(size_t, ncelY);
  CONSTPARA(size_t, ncelZ);
  CONSTPARA(double, time);

  PROTEPARA(std::string, parameterFilename);
  CONSTPARA(double, lx);
  CONSTPARA(double, ly);
  CONSTPARA(double, lz);
  CONSTPARA(double, dx);
  CONSTPARA(double, dy);
  CONSTPARA(double, dz);
  
  Problem::Array c;
};

#endif
