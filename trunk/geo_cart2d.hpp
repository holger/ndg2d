#ifndef geo_cart2d_hpp
#define geo_cart2d_hpp

#include <fstream>

#include "dg2d.hpp"
#include "rk.hpp"
#include "problem2d.hpp"
#include "boundaryCondition2d.hpp"
#include "order.hpp"
#include "mpiBase.hpp"
#include "notiModify.hpp"
#include "writeArray2d.hpp"
#include "logger.hpp"

Problem& problem = BK::StackSingleton<Problem>::instance();

// #undef LOGLEVEL
// #define LOGLEVEL 4

class GeoCart2d
{

public:

  GeoCart2d(size_t ncelX, size_t ncelY, std::string parameterFilename) : ncelX_(ncelX), ncelY_(ncelY), parameterFilename_(parameterFilename) {}
  
  void init() {
    LOGL(1,"GeoCart2d::init()");
    
    std::fill(c.begin(), c.end(), 0);

    BK::ReadInitFile initFile(parameterFilename_,true);

    lx_ = initFile.getParameter<double>("lx");
    ly_ = initFile.getParameter<double>("ly");

    dx_ = lx_/ncelX_;
    dy_ = ly_/ncelY_;
    
    mpiBase.MPICartdimCalcul( 2 ) ;
    
    int ncelXloc = ncelX_ /  mpiBase.get_dimSize()[0] ; // local here
    int ncelYloc = ncelY_ /  mpiBase.get_dimSize()[1] ; // local here
    //int ncelYloc = ncelY_ ;
    c.resize(ncelXloc, ncelYloc, order, order);
    initialize(c);

    std::cerr << "lx = " << lx_ << std::endl;
    std::cerr << "ly = " << ly_ << std::endl;
    std::cerr << "dx = " << dx_ << std::endl;
    std::cerr << "dy = " << dy_ << std::endl;

    LOGL(4,"GeoCart2d::init()-end");
  }  

  void initialize(Problem::Array & c) {
    LOGL(1,"GeoCart2d::initialize(Problem::Array & c)");
    
    BK::Vector<double> xi = points[order-1];
    BK::Vector<double> wi = weights[order-1];
    
    int ncelXloc = ncelX_ /  mpiBase.get_dimSize()[0] ; // local here
    int ncelYloc = ncelY_ /  mpiBase.get_dimSize()[1] ; // local here
    //int ncelYloc = ncelY_ ;

    //double startvalx = 0.0 +  mpiBase.getCartCoords(mpiBase.get_commRank())[0] * (lx_/ mpiBase.get_dimSize()[0]);
    //double startvaly = 0.0 +  mpiBase.getCartCoords(mpiBase.get_commRank())[1] * (ly_/ mpiBase.get_dimSize()[1]);
    double startvalx = 0.0 +  mpiBase.procCoord()[0] * (lx_/ mpiBase.get_dimSize()[0]);
    double startvaly = 0.0 +  mpiBase.procCoord()[1] * (ly_/ mpiBase.get_dimSize()[1]);
    

    for (int i=0;i<mpiBase.get_commSize();i++){
      if (i==mpiBase.get_commRank()){
      std::cout << "mpiBase.get_dimSize()[0]" <<  mpiBase.get_dimSize()[0] << " "<<  "mpiBase.get_dimSize()[1]"<<  mpiBase.get_dimSize()[1]<< std::endl;
      std::cout<<mpiBase.get_commRank() <<"\t" <<mpiBase.procCoord()[0] << "\t" <<mpiBase.procCoord()[1] << "\t" <<  startvalx <<","<< startvaly <<  std::endl;
      }
    }
    //exit(0);
   
    for(int ix=0;ix<int(ncelXloc);ix++) {
      auto transX = [ix, startvalx, this](double x) { return startvalx + ix*dx_+0.5*dx_+x*0.5*dx_;};
      for(int iy=0;iy<int(ncelYloc);iy++) {
	auto transY = [iy, startvaly, this](double y) { return startvaly + iy*dy_+0.5*dy_+y*0.5*dy_;};
	
    	for(int ox=0; ox<order;ox++)
    	  for(int oy=0;oy<order;oy++) 
	    c(ix,iy,ox,oy) = problem.initialCondition(transX(xi[ox]), transY(xi[oy]));    	  
      }
    }

    // std::ofstream out("test.txt");

    // auto uxFunc = [](Problem::Array const& c, int cellX, int cellY, int indexX, int indexY) {
    //   double rhoUx = c(cellX, cellY, indexX, indexY)[Problem::Var::rhoUx];
    //   double rho = c(cellX, cellY, indexX, indexY)[Problem::Var::rho];
    //   double ux = rhoUx/rho;
    //   return ux;
    // };
    
    // for(int ix=0;ix<int(ncelXloc);ix++) {
    //   auto transX = [ix, this](double x) { return ix*dx_+0.5*dx_+x*0.5*dx_;};


				
    //   for(int ox=0; ox<order;ox++) {
    // 	double ux_x = problem.derivativeX(uxFunc,c,ix,0,ox,0);
	
    // 	out <<  transX(xi[ox]) << "\t" << problem.initialCondition(transX(xi[ox]),0)[1]/problem.initialCondition(transX(xi[ox]),0)[0] << "\t" << ux_x << std::endl;
    //   }
    // }

    // out.close();
    
    // exit(0);

    //    penalty(c);
    LOGL(4,"GeoCart2d::initialize(Problem::Array & c)-end");
  }

  double posX(int cellX, int indexX) {
    return (cellX + (1 + points[order-1][indexX])*0.5)*dx_;
  }
  
  double posY(int cellY, int indexY) {
    return (cellY + (1 + points[order-1][indexY])*0.5)*dy_;
  }

  void penalty(Problem::Array & c) {
       
    for(int cellX=0; cellX<int(c.size(0)); cellX++)
      for(int cellY=0; cellY<int(c.size(1)); cellY++)
	for(int indexX=0; indexX<int(c.size(2)); indexX++)
	  for(int indexY=0; indexY < int(c.size(3)); indexY++)
	    if(BK::sqr(posX(cellX, indexX) - double(c.size(0))/2*dx_)
	       + BK::sqr(posY(cellY, indexY) - double(c.size(1))/2*dy_) < BK::sqr(lx_/4)) {
	      c(cellX, cellY, indexX, indexY)[0] = 1;
	      c(cellX, cellY, indexX, indexY)[1] = 0;
	      c(cellX, cellY, indexX, indexY)[2] = 0;
	      c(cellX, cellY, indexX, indexY)[3] = 0;
	      c(cellX, cellY, indexX, indexY)[4] = 0;
	    }
  }
  
  void solve() {
    Problem::Array cnew(c);
    
    Rhs2d rhs2d(c.size(0), c.size(1), problem.get_dx(), problem.get_dy());
    
    auto dimensionsX = rhs2d.fluxInfoBordArrayX.dim();
    int nFluxcelx = dimensionsX[0];
    int nFluxcely = dimensionsX[1];
    xFluxMPI.init(nFluxcelx, nFluxcely);

    auto dimensionsY = rhs2d.fluxInfoBordArrayY.dim();
    nFluxcelx = dimensionsY[0];
    nFluxcely = dimensionsY[1];
    yFluxMPI.init(nFluxcelx, nFluxcely);
    
    PeriodicBoundaryCondition2dFlux periodicBoundaryCondition2d;
    
    double dt = problem.get_dt();
    
    std::unique_ptr<Rk> rk = getRkObject(dt, cnew);
    
    time_ = 0;
    cnew=c;

    int mkdir = system(BK::toString("mkdir data").c_str());
    std::string dirName = BK::toString("data/", problem.get_name());
    mkdir = system(BK::toString("mkdir ", dirName).c_str());
    std::cerr << mkdir << std::endl;
    int nPoParCel = 10;
    
    writeFine(get_c(), dirName + "/init_" +  std::to_string(order) + "-" + std::to_string(problem.get_rkOrder()) +"_" + std::to_string(ncelX_)+"_"+std::to_string(ncelY_)+ "_" + std::to_string(mpiBase.get_commRank()), nPoParCel, dx_, dy_);

#ifdef NETCDF
    size_t rkOrder = problem.get_rkOrder();
    writeNetCDF(get_c(), dirName + "/init_" +  std::to_string(order) + "-" + std::to_string(rkOrder) +"_" + std::to_string(ncelX_) + "_" + std::to_string(ncelY_), ncelX_, ncelY_, problem.get_dx(), problem.get_dy());
#endif

    notifyField.exec("field", &c);
    
    for(int t=0;t<problem.get_nt();t++){
      if(mpiBase.get_commRank() == 0)
	std::cout << "t = " << t << "\t time = " << time_ << std::endl;
      
      Problem::Array rhs(c.dim());
      for(int stage = 0; stage < rk->get_stageNumber(); stage++) {
	
	problem.computeFluxes(rhs2d.fluxArray, rhs2d.fluxInfoBordArrayX, rhs2d.fluxInfoBordArrayY, cnew, problem);
	problem.computeSourceTerm(cnew, rhs2d.sourceArray);
	
	periodicBoundaryCondition2d(rhs2d.fluxInfoBordArrayX, rhs2d.fluxInfoBordArrayY);
	
	rhs2d.addIntegralAndSource(rhs);
	  
	problem.computeNumericalFlux(rhs2d.fluxVecArrayX, rhs2d.fluxVecArrayY, rhs2d.fluxInfoBordArrayX, rhs2d.fluxInfoBordArrayY);

	rhs2d.addBorderTerm(rhs);
      	rk->advance(cnew, c, rhs);
      }
      c=cnew;

      //      penalty(c);
      
      time_ += dt;
      ++problem.set_step();
      notifyField.exec("field", &c);
    }

    notifyField.exec("fieldFinal", &get_c());


    writeFine(get_c(), dirName + "/sol_" +  std::to_string(order) + "-" + std::to_string(problem.get_rkOrder()) + "_" + std::to_string(ncelX_) + "_" + std::to_string(ncelY_) + "_" + std::to_string(mpiBase.get_commRank()), nPoParCel, dx_, dy_);

#ifdef NETCDF
    writeNetCDF(get_c(), dirName + "/sol_" +  std::to_string(order) + "-" + std::to_string(rkOrder) +"_" + std::to_string(ncelX_) + "_" + std::to_string(ncelY_), ncelX_, ncelY_, problem.get_dx(), problem.get_dy());
#endif
    
  }

  Problem::Array const& get_c() {
    return c;
  }

private:
  CONSTPARA(size_t, ncelX);
  CONSTPARA(size_t, ncelY);
  CONSTPARA(double, time);

  PROTEPARA(std::string, parameterFilename);
  CONSTPARA(double, lx);
  CONSTPARA(double, ly);
  CONSTPARA(double, dx);
  CONSTPARA(double, dy);
  
  Problem::Array c;
};

#endif
