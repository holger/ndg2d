#ifndef problem3d_hpp
#define problem3d_hpp

#ifdef PROBLEM_ADVECTION3D
#include "problem_advection3d.hpp"
#endif

#ifdef PROBLEM_BURGERSDENSITY3D
#include "problem_burgersDensity3d.hpp"
#endif

#ifdef PROBLEM_BURGERSDENSITYVISCOSITY3D
#include "problem_burgersDensityViscosity3d.hpp"
#endif

#ifdef PROBLEM_EULERISO3D
#include "problem_eulerIso3d.hpp"
#endif

#ifdef PROBLEM_EULERISOVISCOSITY3D
#include "problem_eulerIsoViscosity3d.hpp"
#endif

#endif
