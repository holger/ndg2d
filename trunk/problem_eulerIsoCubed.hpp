#ifndef problem_eulerIsoCubed_hpp
#define problem_eulerIsoCubed_hpp

#include <BasisKlassen/parameter.hpp>
#include <BasisKlassen/array.hpp>

#include "problem_eulerIso2d.hpp"

// 2D isothermal gaz on cubed sphere geometry
class Problem_EulerIsoCubed : public Problem_EulerIso2d
{
public:

  const BK::Array<BK::Array<int, 2>, 2> fluxVarVecIndizes = {{0,3},{1,4},{2,5},{6,7},{9,10}};
  const BK::Array<int, 1> fluxVarScalarIndizes = {8};

  friend class BK::StackSingleton<Problem_EulerIsoCubed>;

  Problem_EulerIsoCubed() {}

  void init(int ncelx, int ncely, std::string parameterFilename) {

    Problem_EulerIso2d::init(ncelx, ncely, parameterFilename);

    da_ = M_PI/2./ncelx_;
    db_ = M_PI/2./ncely_;
    
    name_ = "eulerIsoCubed";

    logParameter();
  }

    Vector initialCondition(double phi, double theta, double shiftAngle = 0) {
    assert(initialized_);

    // Blob --------------------------------------------------
    // double h0 = 1;
    // double deltaC = 0.4;

    // double phi0 = 0;//M_PI/2;
    // double theta0 = M_PI/2;
    
    // double deltaPhi = fabs(phi0 - phi);
    // double deltaTheta = fabs(theta0 - theta);
    // double centralAngle = sqrt(deltaPhi*deltaPhi + deltaTheta*deltaTheta);

    // double value = h0/2*(1+cos(M_PI*centralAngle/deltaC));
    // if(centralAngle > deltaC)
    //   value = 0;

    // smooth cosine ------------------------------------------
    double value = cos(phi)*sin(theta);
    
    return Vector{1, value, 0};
  }

  void initialize(Array & c) {
    BK::Vector<double> xi = points[order-1];
    BK::Vector<double> wi = weights[order-1];
    
    for(int ix=0;ix<int(ncelx_);ix++) {
      auto transX = [ix, this](double x) { return ix*dx_+0.5*dx_+x*0.5*dx_;};
      for(int iy=0;iy<int(ncely_);iy++) {
	auto transY = [iy, this](double y) { return iy*dy_+0.5*dy_+y*0.5*dy_;};
	
    	for(int ox=0; ox<order;ox++)
    	  for(int oy=0;oy<order;oy++) 
	    c(ix,iy,ox,oy) = initialCondition(transX(xi[ox]), transY(xi[oy]));
    	  
      }
    }
  }

  void getReference(Array & c) {}

  void lhs(Array& cNew, Array const& c, Array const& rhs) const {
    cNew = c + dt_*rhs;
  }    

  VecVec flux_vector(int cellX, int cellY, int indexX, int indexY, Array const& c) {
    double rho = c(cellX, cellY, indexX, indexY)[0];
    double rhoUx = c(cellX, cellY, indexX, indexY)[1];
    double rhoUy = c(cellX, cellY, indexX, indexY)[2];

    double ux = rhoUx/rho;
    double uy = rhoUy/rho;

    // f0x = rhoUx; f1x = rhoUx*ux; f2x = rhoUx*uy;
    // f0y = rhoUy; f1y = rhoUy*ux; f2y = rhoUy*uy;
    return VecVec{ Vector{rhoUx, rhoUx*ux + rho*a_*a_, rhoUx*uy},
                   Vector{rhoUy, rhoUy*ux, rhoUy*uy + rho*a_*a_} };
  }

  BK::Array<double,2> dFlux_value(int cellX, int cellY, int indexX, int indexY, Array const& c) {
    double rho = c(cellX, cellY, indexX, indexY)[0];
    double rhoUx = c(cellX, cellY, indexX, indexY)[1];
    double rhoUy = c(cellX, cellY, indexX, indexY)[2];

    double ux = rhoUx/rho;
    double uy = rhoUy/rho;

    return BK::Array<double,2>({std::max({fabs(ux), fabs(ux + a_), fabs(ux - a_)}), 
	  std::max({fabs(uy), fabs(uy + a_), fabs(uy - a_)})});
  }
  
  void fillFluxInfoBordArray(int cellXf, int cellYf, int index, int plusMinus,
			     VecVec const& flux, Array const& c, FluxInfoBordArray& fluxInfoBordArray, int cellX, int cellY, int indexX, int indexY) {
    fluxInfoBordArray(cellXf, cellYf, index)[plusMinus][FluxVar::f0x] = flux[Dir::x][Var::val0];
    fluxInfoBordArray(cellXf, cellYf, index)[plusMinus][FluxVar::f1x] = flux[Dir::x][Var::val1];
    fluxInfoBordArray(cellXf, cellYf, index)[plusMinus][FluxVar::f2x] = flux[Dir::x][Var::val2];
    fluxInfoBordArray(cellXf, cellYf, index)[plusMinus][FluxVar::f0y] = flux[Dir::y][Var::val0];
    fluxInfoBordArray(cellXf, cellYf, index)[plusMinus][FluxVar::f1y] = flux[Dir::y][Var::val1];
    fluxInfoBordArray(cellXf, cellYf, index)[plusMinus][FluxVar::f2y] = flux[Dir::y][Var::val2];
    fluxInfoBordArray(cellXf, cellYf, index)[plusMinus][FluxVar::dfx] = dFlux_value(cellX, cellY, indexX, indexY, c)[Dir::x];
    fluxInfoBordArray(cellXf, cellYf, index)[plusMinus][FluxVar::dfy] = dFlux_value(cellX, cellY, indexX, indexY, c)[Dir::y];
    fluxInfoBordArray(cellXf, cellYf, index)[plusMinus][FluxVar::val0] = c(cellX, cellY, indexX, indexY)[Var::val0];
    fluxInfoBordArray(cellXf, cellYf, index)[plusMinus][FluxVar::val1] = c(cellX, cellY, indexX, indexY)[Var::val1];
    fluxInfoBordArray(cellXf, cellYf, index)[plusMinus][FluxVar::val2] = c(cellX, cellY, indexX, indexY)[Var::val2];
  }
  
  void computeFluxes(FluxArray& fluxArray,
		     FluxInfoBordArray& fluxInfoBordArrayX,
		     FluxInfoBordArray& fluxInfoBordArrayY,
		     Array const& c) {
    
    for(int cellX=0; cellX<int(c.size(0)); cellX++)
      for(int cellY=0; cellY<int(c.size(1)); cellY++)
	for(int indexX=0; indexX<int(c.size(2)); indexX++)
	  for(int indexY=0; indexY<int(c.size(3)); indexY++) {
	    VecVec flux = flux_vector(cellX, cellY, indexX, indexY, c);
	    fluxArray(cellX, cellY, indexX, indexY) = flux;
	    if(indexX == 0) 
	      fillFluxInfoBordArray(cellX, cellY, indexY, Val::plus, flux, c, fluxInfoBordArrayX, cellX, cellY, indexX, indexY);
	    
	    if(indexX == order-1) 
	      fillFluxInfoBordArray(cellX+1, cellY, indexY, Val::minus, flux, c, fluxInfoBordArrayX, cellX, cellY, indexX, indexY);
	    
	    if(indexY == 0) 
	      fillFluxInfoBordArray(cellX, cellY, indexX, Val::plus, flux, c, fluxInfoBordArrayY, cellX, cellY, indexX, indexY);

	    if(indexY == order-1) 
	      fillFluxInfoBordArray(cellX, cellY+1, indexX, Val::minus, flux, c, fluxInfoBordArrayY, cellX, cellY, indexX, indexY);
	    
	  }
  }

  void computeNumericalFlux(FluxVecArray& fluxVecArrayX, FluxVecArray& fluxVecArrayY,
			    FluxInfoBordArray& fluxInfoArrayX, FluxInfoBordArray& fluxInfoArrayY) {

    for(size_t cellX=0; cellX<fluxVecArrayX.size(0); cellX++)
      for(size_t cellY=0; cellY<fluxVecArrayX.size(1); cellY++)
	for(size_t orderY=0; orderY<order; orderY++) {

	  Vector fm;
	  fm[0] = fluxInfoArrayX(cellX, cellY, orderY)[Val::minus][FluxVar::f0x];
	  fm[1] = fluxInfoArrayX(cellX, cellY, orderY)[Val::minus][FluxVar::f1x];
	  fm[2] = fluxInfoArrayX(cellX, cellY, orderY)[Val::minus][FluxVar::f2x];
	  
	  Vector fp;
	  fp[0] = fluxInfoArrayX(cellX, cellY, orderY)[Val::plus][FluxVar::f0x];
	  fp[1] = fluxInfoArrayX(cellX, cellY, orderY)[Val::plus][FluxVar::f1x];
	  fp[2] = fluxInfoArrayX(cellX, cellY, orderY)[Val::plus][FluxVar::f2x];
	    
	  double dfm = fluxInfoArrayX(cellX, cellY, orderY)[Val::minus][FluxVar::dfx];
	  double dfp = fluxInfoArrayX(cellX, cellY, orderY)[Val::plus][FluxVar::dfx];

	  Vector valM;
	  valM[0] = fluxInfoArrayX(cellX, cellY, orderY)[Val::minus][FluxVar::val0];
	  valM[1] = fluxInfoArrayX(cellX, cellY, orderY)[Val::minus][FluxVar::val1];
	  valM[2] = fluxInfoArrayX(cellX, cellY, orderY)[Val::minus][FluxVar::val2];

	  Vector valP;
	  valP[0] = fluxInfoArrayX(cellX, cellY, orderY)[Val::plus][FluxVar::val0];
	  valP[1] = fluxInfoArrayX(cellX, cellY, orderY)[Val::plus][FluxVar::val1];
	  valP[2] = fluxInfoArrayX(cellX, cellY, orderY)[Val::plus][FluxVar::val2];

	  fluxVecArrayX(cellX, cellY, orderY) =  laxFriedrichs1d_flux_numeric(fm, fp, dfm, dfp, valM, valP);
	}

    for(size_t cellX=0; cellX<fluxVecArrayY.size(0); cellX++)
      for(size_t cellY=0; cellY<fluxVecArrayY.size(1); cellY++)
	for(size_t orderX=0; orderX<order; orderX++) {
	  Vector fm;
	  fm[0] = fluxInfoArrayY(cellX, cellY, orderX)[Val::minus][FluxVar::f0y];
	  fm[1] = fluxInfoArrayY(cellX, cellY, orderX)[Val::minus][FluxVar::f1y];
	  fm[2] = fluxInfoArrayY(cellX, cellY, orderX)[Val::minus][FluxVar::f2y];
	  
	  Vector fp;
	  fp[0] = fluxInfoArrayY(cellX, cellY, orderX)[Val::plus][FluxVar::f0y];
	  fp[1] = fluxInfoArrayY(cellX, cellY, orderX)[Val::plus][FluxVar::f1y];
	  fp[2] = fluxInfoArrayY(cellX, cellY, orderX)[Val::plus][FluxVar::f2y];
	    
	  double dfm = fluxInfoArrayY(cellX, cellY, orderX)[Val::minus][FluxVar::dfy];
	  double dfp = fluxInfoArrayY(cellX, cellY, orderX)[Val::plus][FluxVar::dfy];

	  Vector valM;
	  valM[0] = fluxInfoArrayY(cellX, cellY, orderX)[Val::minus][FluxVar::val0];
	  valM[1] = fluxInfoArrayY(cellX, cellY, orderX)[Val::minus][FluxVar::val1];
	  valM[2] = fluxInfoArrayY(cellX, cellY, orderX)[Val::minus][FluxVar::val2];

	  Vector valP;
	  valP[0] = fluxInfoArrayY(cellX, cellY, orderX)[Val::plus][FluxVar::val0];
	  valP[1] = fluxInfoArrayY(cellX, cellY, orderX)[Val::plus][FluxVar::val1];
	  valP[2] = fluxInfoArrayY(cellX, cellY, orderX)[Val::plus][FluxVar::val2];

	  fluxVecArrayY(cellX, cellY, orderX) =  laxFriedrichs1d_flux_numeric(fm, fp, dfm, dfp, valM, valP);
	}
  }

  CONSTPARA(double, da);
  CONSTPARA(double, db);

  CONSTPARA(double, a);
};

#endif
