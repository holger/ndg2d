#include "problem_burgersDensity1d.hpp"

void Problem_BurgersDensity1d::errorComp(Array const* c) {

  //  double lx = lx_;
  double np = 1000;
  double dxn = lx_/np;

  std::vector<Vector> solVec(np);

  for(size_t i=0; i<solVec.size(); i++)
    solVec[i] = value(i*dxn, *c, dx_);
    
  // Create ref file for error comparison

  // std::string filename = BK::toString(dirName,"/solRef_burgersDensity1d_0_15-", order,"-",rkOrder);
  // std::ofstream solOut;
  // solOut.open(filename + ".bin", std::ios::out | std::ios::binary);
  // solOut.write( (char*)solVec.data(), sizeof(Vector)*solVec.size());
  // solOut.close();

  std::ifstream solIn;
  std::string filename = "../tests/gold/burgersDensity1d/solRef_burgersDensity1d_0_1-6-6";
  solIn.open(filename + ".bin", std::ios::in | std::ios::binary);
  if(!solIn.is_open()) {
    std::cerr << "ERROR: unable to open file " << filename + ".bin" << std::endl;
    exit(1);
  }
			
  std::vector<Vector> solVec2(np);
  solIn.read((char*)solVec2.data(), sizeof(Vector)*solVec2.size());
  solIn.close();

  std::cerr << "writing " << BK::toString("../",filename,".txt") << std::endl;
  std::ofstream solOutAscii(BK::toString("../",filename,".txt"));
  for(int i=0; i<np; i++) {
    solOutAscii << i*dxn << "\t" << solVec2[i][1] << "\t" << solVec[i][1]<< std::endl;
  }

  solOutAscii.close();

  double err = 0;
  for(size_t i=0; i<solVec.size(); i++)
    err +=BK::sqr(solVec[i][1] - solVec2[i][1])*dxn;

  err = sqrt(err);

  std::string dirName = BK::toString("data/",name_);
	
  std::cerr << "writing error to " << BK::toString(dirName + "/error-ndg-",order,"-",rkOrder_,".txt") << std::endl;
  std::ofstream outChrono(BK::toString(dirName + "/error-ndg-",order,"-",rkOrder_,".txt"), std::ios::app);
  outChrono << ncel_ << "\t" << err << std::endl;
  outChrono.close();
}    
