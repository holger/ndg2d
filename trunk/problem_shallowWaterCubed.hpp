#ifndef problem_shallowWaterCubed_hpp
#define problem_shallowWaterCubed_hpp

#include <BasisKlassen/parameter.hpp>
#include <BasisKlassen/array.hpp>
#include <BasisKlassen/parseFromFile.hpp>

const int varDim = 3;   // three variables: sqrtG*h, sqrtG*h*ux, sqrtG*h*uy
const int spaceDim = 2; // 2d space

#include "types.hpp"
#include "cfl.hpp"
#include "problemBase2d.hpp"
#include "laxFriedrichs1d.hpp"
#include "gaussLobatto.hpp"
#include "notiModify.hpp"
#include "order.hpp"
#include "cubedSphere_utils.hpp"
#include "value2d.hpp"
#include "mpiBase.hpp"
#include "logger.hpp"

// LOGGING: ----------------------------------------------------
// #undef LOGLEVEL
// #define LOGLEVEL 4

extern CubedSphere_utils& cs_utils;

// 2D isothermal gaz on cubed sphere geometry
class Problem_ShallowWaterCubed : public ProblemBase2d<varDim>
{
public:

  struct Var {
    static const int sqrtG_h = 0;      // sqrt(G)*h
    static const int sqrtG_h_ua = 1;   // sqrt(G)*h*ua
    static const int sqrtG_h_ub = 2;   // sqrt(G)*h*ub
  };

  static const int fluxInfoNumber = 3;
  
  const BK::Array<BK::Array<int, 2>, 1> fluxVarVecIndizes = {{Var::sqrtG_h_ua, Var::sqrtG_h_ub}};
  const BK::Array<int, 1> fluxVarScalarIndizes = {Var::sqrtG_h};

  static const int fluxVarianceType = 0; // 0=contra-variant; 1=co-variant
  
  typedef BK::Array<double, varDim> FluxInfo;                         // [sqrtG_h, sqrtG_h_ua, sqrtG_h_ub] 
  typedef BK::Array<FluxInfo, 2> FluxInfoBord;                        // [+-][sqrtG_h, sqrtG_h_ua, sqrtG_h_ub] 
  typedef BK::MultiArray<spaceDim+1, FluxInfoBord> FluxInfoBordArray; // (cell)[+-][sqrtG_h, sqrtG_h_ua, sqrtG_h_ub]
  typedef BK::MultiArray<spaceDim+1, Vector> FluxVecArray;
							       
  friend class BK::StackSingleton<Problem_ShallowWaterCubed>;

  Problem_ShallowWaterCubed() {}
  
  void lhs(Array& cNew, Array const& c, Array const& rhs) const {
    cNew = c + dt_*rhs;
  }    

  void init(int ncelx, int ncely, std::string parameterFilename) {

    LOGL(1,"Problem_ShallowWaterCubed::init(int ncelx, int ncely, std::string parameterFilename)");
    
    if(!initialized_) {
      notifyFieldVec.add("fieldFinal", FieldVecNotifyCallBack(std::bind(&Problem_ShallowWaterCubed::errorComp,this,std::placeholders::_1), "Problem_ShallowWaterCubed::errorComp"));
      notifyFieldVec.add("fieldInitial", FieldVecNotifyCallBack(std::bind(&Problem_ShallowWaterCubed::diagnostic,this,std::placeholders::_1), "Problem_ShallowWaterCubed::diagnostic"));
      notifyFieldVec.add("field", FieldVecNotifyCallBack(std::bind(&Problem_ShallowWaterCubed::diagnostic,this,std::placeholders::_1), "Problem_ShallowWaterCubed::diagnostic"));
    }

    ProblemBase2d::init(ncelx, ncely, parameterFilename);
    
    BK::ReadInitFile initFile(parameterFilename,true);
    double nt = nt_;
    g_ = initFile.getParameter<double>("g");
    omega_ = initFile.getParameter<double>("omega");

    uPhiVec.resize(6, BK::MultiArray<spaceDim*2,double>(ncelx_, ncely, order, order));
    uThetaVec.resize(6, BK::MultiArray<spaceDim*2,double>(ncelx_, ncely, order, order));
    
    if(nt > 0 && T_ > 0) {
      std::cerr << "ERROR in Problem_shallowWater2d::init(): nt = " <<
	nt << " > 0 && T = " << T_ << " > 0." << std::endl;
      exit(1);						   
    }

    assert(dx_ == dy_);
    
    dt_ = std::min(fabs(cfl_*dx_),fabs(cfl_*dx_/(sqrt(g_*1))));

    if(nt > 0) {
      nt_ = nt;
      T_ = nt_*dt_;
    }
    else
      nt_ = int(T_/dt_) + 1;

    dt_ *= T_/(nt_*dt_);

    da_ = M_PI/2./ncelx_;
    db_ = M_PI/2./ncely_;
    
    name_ = "shallowWaterCubed";

    logParameter();

    LOGL(1,PAR(omega_));
    LOGL(1,PAR(g_));

    initialized_ = true;
    
    LOGL(4,"Problem_ShallowWaterCubed::init(int ncelx, int ncely, std::string parameterFilename)-end");
  }

  void diagnostic(BK::Vector<Array> const* c) {
    // std::cout << "diagnostic(BK::Vector<Array> const* c)\n";
    
    if(step_ % diagOutputInterval_ != 0) return;
    
    auto w = weights[order-1];

    std::function<double(int, int, int ,int, Vector)> f_sqrtG_h = [&](int a, int b, int ia, int ib, Vector data) {return data[Var::sqrtG_h];};
    double int_sqrtG_h = cs_utils.integrate(f_sqrtG_h, c);

    std::function<double(int, int, int ,int, Vector)> f_sqrtG_h_ua = [&](int a, int b, int ia, int ib, Vector data) {return data[Var::sqrtG_h_ua];};
    double int_sqrtG_h_ua = cs_utils.integrate(f_sqrtG_h_ua, c);

    std::function<double(int, int, int ,int, Vector)> f_sqrtG_h_ub = [&](int a, int b, int ia, int ib, Vector data) {return data[Var::sqrtG_h_ub];};
    double int_sqrtG_h_ub = cs_utils.integrate(f_sqrtG_h_ub, c);

    std::function<double(int, int, int ,int, Vector)> f_energy = [&](int a, int b, int ia, int ib, Vector data) {
      double g11 = cs_utils.G11(a,b,ia,ib);
      double g12 = cs_utils.G12(a,b,ia,ib);
      double g22 = cs_utils.G22(a,b,ia,ib);
      double sqrtG = cs_utils.sqrtG(a, b, ia, ib);
      double h = data[0]/sqrtG;
      double ua = data[1]/data[0];
      double ub = data[2]/data[0];

      return g_*h + 0.5*(ua*ua*g11 + 2*ua*ub*g12 + ub*ub*g22);};

    double energy = cs_utils.integrate(f_energy, c);
    
    std::cout << "int_sqrtG_h = " << int_sqrtG_h << " int_sqrtG_h_ua = " << int_sqrtG_h_ua
	      << " int_sqrtG_h_ub = " << int_sqrtG_h_ub << " energy = " << energy << std::endl;

    std::string dirName = BK::toString("data/", name_);
    std::ofstream out(BK::toString(dirName + "/diag-",order,"-",rkOrder_,".txt"), std::ios::app);
    out << step_ << "\t" << int_sqrtG_h << "\t" << int_sqrtG_h_ua << "\t" << int_sqrtG_h_ub << "\t" << energy << std::endl;
    out.close();
  }

  double integrateCell(int a, int b, Array const& c, int var) {
    double integral = 0;
    auto w = weights[order];

    for(int ia = 0; ia < order; ia++)
      for(int ib = 0; ib < order; ib++) 
	integral += c(a,b,ia,ib)[var]*w[ia]*w[ib];
    
    return integral;
  }    

  void errorComp(BK::Vector<Array> const* c) {
    std::cout << "errorComp(BK::Vector<Array> const* c)\n";

    int ncelXloc = ncelx_ / mpiBase.get_dimSize()[0] ; // local here
    int ncelYloc = ncely_ / mpiBase.get_dimSize()[1] ; // local here
    
    BK::Vector<Array> cRef;
    cRef.resize(6, Array(ncelXloc,  ncelYloc , order, order));
    
    getReference(cRef);

    Array const& c0 = (*c)[0];
    Array const& c1 = (*c)[1];
    Array const& c2 = (*c)[2];
    Array const& c3 = (*c)[3];
    Array const& c4 = (*c)[4];
    Array const& c5 = (*c)[5];

    Array const& cRef0 = cRef[0];
    Array const& cRef1 = cRef[1];
    Array const& cRef2 = cRef[2];
    Array const& cRef3 = cRef[3];
    Array const& cRef4 = cRef[4];
    Array const& cRef5 = cRef[5];

    double errorr, error ;
    double errorloc = 0 ;
    size_t nx = (*c)[0].size(0);
    size_t ny = (*c)[0].size(1);
    for(int ix = 0; ix < int(nx); ix++)
      for(int iy = 0; iy < int(ny); iy++) {
	errorloc += BK::sqr(c0(ix, iy, 0, 0)[Var::sqrtG_h] - cRef0(ix, iy, 0, 0)[Var::sqrtG_h])*dx_*dy_;
	errorloc += BK::sqr(c1(ix, iy, 0, 0)[Var::sqrtG_h] - cRef1(ix, iy, 0, 0)[Var::sqrtG_h])*dx_*dy_;
	errorloc += BK::sqr(c2(ix, iy, 0, 0)[Var::sqrtG_h] - cRef2(ix, iy, 0, 0)[Var::sqrtG_h])*dx_*dy_;
	errorloc += BK::sqr(c3(ix, iy, 0, 0)[Var::sqrtG_h] - cRef3(ix, iy, 0, 0)[Var::sqrtG_h])*dx_*dy_;
	errorloc += BK::sqr(c4(ix, iy, 0, 0)[Var::sqrtG_h] - cRef4(ix, iy, 0, 0)[Var::sqrtG_h])*dx_*dy_;
	errorloc += BK::sqr(c5(ix, iy, 0, 0)[Var::sqrtG_h] - cRef5(ix, iy, 0, 0)[Var::sqrtG_h])*dx_*dy_;
	//std::cout << value(ix, iy, 0., 0., *c)[0] <<"\t" << - value(ix, iy, 0., 0., cRef)[0] <<"\t" <<  errorloc << "\t" << dx_ <<"\t" <<dy_<< std::endl;
	
      }
    //MPI_Reduce(&errorloc, &errorr, 1.,MPI_DOUBLE,MPI_SUM,0,MPI_COMM_WORLD);
    mpiBase.mpiAllReduceSum(&errorloc, &errorr, 1);
    error = sqrt(errorr);
    if (mpiBase.get_commRank() == 0){
      std::string dirName = BK::toString("data/", name_);
      int mkdir = system(BK::toString("mkdir data").c_str());
      mkdir = system(BK::toString("mkdir ",dirName).c_str());
      std::cerr << mkdir << std::endl;
      std::cerr << "writing error to " << BK::toString(dirName + "/error-ndg-",order,"-",rkOrder_,".txt") << std::endl;
    
      /*if (mpiBase.get_commRank() == 0){
	double errorz = errorloc;
	std::ofstream outChrono(BK::toString(dirName + "/error-ndg-",order,"-",rkOrder_,".txt"), std::ios::app);
	outChrono << "mpi0err:" << "\t" << errorz << std::endl;
	outChrono.close(); }
	if (mpiBase.get_commRank() == 1){
	double erroro = errorloc;
	std::ofstream outChrono(BK::toString(dirName + "/error-ndg-",order,"-",rkOrder_,".txt"), std::ios::app);
      outChrono << "mpi1err:" << "\t" << erroro << std::endl;
      outChrono.close(); }*/
    
   
    std::ofstream outChrono(BK::toString(dirName + "/error-ndg-",order,"-",rkOrder_,".txt"), std::ios::app);
    outChrono << ncelx_ << "\t" << error << std::endl;
    outChrono.close(); }
  }

  void setVelocity(BK::Array<double,3> xyz, BK::Array<double,3>& v){
    
    double x = xyz[0];
    double y = xyz[1];
    double z = xyz[2];

    // rotated compression waves
    double alpha = M_PI/4;
    double xRot = x;
    double yRot = y*cos(alpha) - z*sin(alpha);
    double zRot = y*sin(alpha) + z*cos(alpha);

    double phi = xyz2pt(xRot, yRot, zRot)[0];
    double theta = xyz2pt(xRot, yRot, zRot)[1];

    double vTheta = sin(3*theta);
    double vPhi = 0;

    d_para_spherical(v, vTheta, vPhi, theta, phi);

    double vx = v[0];
    double vy = v[1]*cos(alpha) + v[2]*sin(alpha);
    double vz = -v[1]*sin(alpha) + v[2]*cos(alpha);

    v[0] = vx;
    v[1] = vy;
    v[2] = vz;
    
    // solid rotation around axis that is inclined by alpha with
    // respect to the z-axis.
    // double omega = 1;
    // double alpha = M_PI/4.;
    
    // double omegaX = 0;
    // double omegaY = -omega*sin(alpha);
    // double omegaZ = omega*cos(alpha);

    // v[0] = omegaY*Z - omegaZ*Y;
    // v[1] = omegaZ*X - omegaX*Z;
    // v[2] = omegaX*Y - omegaY*X;
      
    // // Deformational flow
    // auto pt = xyz2pt(X,Y,Z);
    // double theta = pt[1];
    // double r0 = 1;
    // double rho = r0*cos(theta);
    // double vt;
    
    // if(rho == 0)
    //   vt = 0;
    // else
    //   vt = 1.5*sqrt(3)*1./(cosh(rho)*cosh(rho))*tanh(rho);

    // omega = vt/rho;

    //rotation around z
    // v[0] = omega*Y;
    // v[1] = -omega*X;
    // v[2] = 0;



    // v[0] = 0;
    // v[1] = 0;
    // v[2] = 0;

    // rotation around x
    // v[0] = 0;
    // v[1] = -omega*Z;
    // v[2] = omega*Y;

    // rotation around y
    // v[0] = omega*Z;
    // v[1] = 0;
    // v[2] = -omega*X;
  }

  Vector initialCondition(int a, int b, int ia, int ib, int face, double shiftAngle = 0) {

    double ua, ub;
    BK::Array<double,3> v;

    double alpha = cs_utils.get_alpha(a, ia);
    double beta = cs_utils.get_alpha(b, ib);
    BK::Array<double,3> xyz = ab2xyz[face](alpha, beta);
    BK::Array<double,2> pt = xyz2pt(xyz[0], xyz[1], xyz[2]);
    double phi = pt[0];
    double theta = pt[1];
    double sqrtG = cs_utils.sqrtG(a, b, ia, ib);

    // testing initial condition
    double h = (1+0.1*exp(-BK::sqr(theta-M_PI/2.)/BK::sqr(0.3)));
    //double h = 1;
    v[0] = 0;
    v[1] = 0;
    v[2] = 0;
    d_map[face](v, ua, ub, xyz);
    
    // initial condition using setVelocity ------------------------------
    // double h = 5;
    // setVelocity(xyz, v);
    // d_map[face](v, ua, ub, xyz);

    // Nair stationary condition ----------------------------------------
    // double alpha0 = M_PI/2.7;
    // //    double a0 = 6378000.;
    // double a0 = 1;
    // //    double u0 = 2.*M_PI*a0/(12*24*3600);
    // double u0 = 1.;
    // //    double h0 = 2.94e4/g_;
    // double h0 = 1./g_;
    
    // double h = h0 - u0/(2*g_)*(2*a0*omega_ + u0)*BK::sqr(sin(theta-M_PI/2)*cos(alpha0) - cos(phi)*cos(theta-M_PI/2)*sin(alpha0));
    // double u_phi = u0*(cos(alpha0) - sin(alpha0)*cos(theta)/sin(theta)*cos(phi));
    // double u_theta = -u0*sin(alpha0)*sin(phi);

    // d_para_spherical(v, u_theta, u_phi, theta, phi);
    // d_map[face](v, ua, ub, xyz);

    // -------------------------------------------------------------------
    
    // setVelocity(xyz, v);
    // d_map[face](v, ua, ub, xyz);

    //    double h = 2+cos(pt[0])*sin(pt[1]);
    //    double value = 2+cos(pt[0]+M_PI/2.)*sin(pt[1]);
    //double value = 1;



    // // Nair initial conditions:
    // double alpha0 = M_PI/4;
    // double a0 = 6378000;
    // double omega = 2*M_PI/(24*3600);
    // double u0 = 2*M_PI*a0/(12*24*3600);
    // double h0 = 2.94e4/g_;
    
    // double phi = pt[0];
    // double theta = pt[1];

    // static double minTheta = 10;
    // static double maxTheta = -10;
    // static double minPhi = 10;
    // static double maxPhi = -10;

    // if(theta < minTheta) minTheta = theta;
    // if(theta > maxTheta) maxTheta = theta;
    // if(phi < minPhi) minPhi = phi;
    // if(phi > maxPhi) maxPhi = phi;    

    // std::cout << minPhi << "\t" << maxPhi << "\t" << minTheta << "\t" << maxTheta << std::endl;
    
    // double h = h0 - u0/(2*g_)*(2*a0*omega + u0)*BK::sqr(sin(theta-M_PI/2)*cos(alpha0) - cos(phi)*cos(theta-M_PI/2)*sin(alpha0));
    // double u_phi = u0*(cos(alpha0)*cos(theta-M_PI/2)+sin(alpha0)*cos(phi)*sin(theta-M_PI/2));
    // double u_theta = -u0*sin(alpha0)*sin(phi);

    // rotation around x with alpha
    // double u_phi = u0*(cos(alpha0) + sin(alpha0)*cos(theta)/sin(theta)*sin(phi));
    // double u_theta = -u0*sin(alpha0)*cos(phi);

    // rotation around x with alpha
    // double h = h0 - u0/(2*g_)*(2*a0*omega + u0)*BK::sqr(sin(theta-M_PI/2)*cos(alpha0) - cos(phi)*cos(theta-M_PI/2)*sin(alpha0));
    // double u_phi = u0*(cos(alpha0) - sin(alpha0)*cos(theta)/sin(theta)*cos(phi));
    // double u_theta = -u0*sin(alpha0)*sin(phi);

    
    // //setVelocity(xyz, v);
    // d_para_spherical(v, u_theta, u_phi, theta, phi);
    // d_map[face](v, ua, ub, xyz);

    // if(fabs(theta) < 1e-5 ||fabs(theta-M_PI) < 1e-5 ) {
    //   std::cout << "theta = " << theta << "\t phi = " << phi << "\t" << a << "\t" << b << "\t" << ia << "\t" << ib << "\t" << face << std::endl;
    //   std::cout << xyz << "\t" << alpha << "\t" << beta << std::endl;
    // }
    
    // h0 = 6;
    // u0 = 1;
    // a0 = 1;
    // omega = 1;
    //double h = h0 - u0/(2*g_)*(2*a0*omega + u0)*BK::sqr(sin(theta)*cos(alpha0) - cos(phi)*cos(theta)*sin(alpha0));
    // double h = h0 - sqr(sin(theta)*cos(alpha0) - cos(phi)*cos(theta)*sin(alpha0));

 
    
    //  double h = 1;
    
    // phi += M_PI/2+M_PI/2+M_PI;
    // theta += M_PI/2+M_PI/2;
    // theta += M_PI;
    // double u_phi = u0*(cos(alpha0)*cos(theta)+sin(alpha0)*cos(phi)*sin(theta));
    // double u_theta = -u0*sin(alpha0)*sin(phi);
    
    // uPhiVec[face](a,b,ia,ib) = u_phi;
    // uThetaVec[face](a,b,ia,ib) = u_theta;

    //    double h = 1;
    // double u_phi = 0;
    // double u_theta = sin(3*theta);//1+0.1*exp(-BK::sqr(theta-M_PI/2.)/BK::sqr(0.3));

    // v[0] = -u_phi*sin(phi) + u_theta*cos(theta)*cos(phi);
    // v[1] = u_phi*cos(phi) + u_theta*cos(theta)*sin(phi);
    // v[2] = -u_theta*sin(theta);

    // u_phi = 1;
    // u_theta = 0;
    // h = 1;

    // phi += M_PI/2;
    // theta -= M_PI/2;


    // d_parametrisation[face](v, ua, ub, alpha, beta);
    // double uThetaT = 0;
    // double uPhiT = 0;
    // d_map_spherical(v, uThetaT, uPhiT, theta, phi);

    // std::cout << "uThetaT, uPhiT = " << face << "\t" << uThetaT << "\t" << uPhiT << std::endl;

    
    return {sqrtG*h, sqrtG*h*ua, sqrtG*h*ub};
  }

  void getReference(BK::Vector<Array> & cVec) {
    
    for(int a = 0; a<int(ncelx_); a++) 
      for(int b = 0; b<int(ncely_); b++) 
	for(int ia = 0; ia < order; ia++)
	  for(int ib = 0; ib < order; ib++) 
	    for(int face = 0; face <6; face++)
	      cVec[face](a, b, ia, ib) = initialCondition(a, b, ia, ib, face);
  }

  // Vector initialCondition(double phi, double theta) {
  //   // Blob --------------------------------------------------
  //   double h0 = 1;
  //   double deltaC = 0.4;
    
  //   double phi0 = 0;//M_PI/2;
  //   double theta0 = M_PI/2;
    
  //   double deltaPhi = fabs(phi0 - phi);
  //   double deltaTheta = fabs(theta0 - theta);
  //   double centralAngle = sqrt(deltaPhi*deltaPhi + deltaTheta*deltaTheta);

  //   double value = 1+h0/2*(1+cos(M_PI*centralAngle/deltaC));
  //   if(centralAngle > deltaC)
  //     value = 1;

  //   value = 2+cos(phi)*sin(theta);

  //   return Vector{value, 0, 0};
  //   //    return Vector{1+0.1*exp(-(BK::sqr(x-lx_/2)+BK::sqr(y-ly_/2))/BK::sqr(0.5)), 0, 0};
  // }

  std::string toWriteCSV_info()
  {
    return "x,y,z,p,t,a,b,face,h,ua,ub,uPhi,uTheta,vx,vy,vz,sqrtG,gamma111,gamma112,gamma122,gamma211,gamma212,gamma222,g11,g12,g22";
  }

  // std::string toWritePhiThetaCSV_info()
  // {
  //   return "p,t,z,h,ua,ub,uPhi,uTheta,vx,vy,vz,sqrtG,gamma111,gamma112,gamma122,gamma211,gamma212,gamma222,g11,g12,g22";
  // }

  // std::string toWriteAllCSV_info()
  // {
  //   return "x,y,z,p,t,h,ua,ub,uPhi,uTheta,vx,vy,vz,sqrtG,gamma111,gamma112,gamma122,gamma211,gamma212,gamma222,g11,g12,g22";
  // }

  BK::Vector<std::string> toWriteNetCDF_info()
  {
    return {"h","ua","ub","uPhi","uTheta","vx","vy","vz","sqrtG","gamma111","gamma112","gamma122","gamma211","gamma212","gamma222","g11","g12","g22"};
  }

  BK::Vector<double> toWriteCSV(Array const& array, int a, int b, int ia, int ib, int face)
  {

    double alpha = cs_utils.get_alpha(a, ia);
    double beta = cs_utils.get_beta(b, ib);
    
    BK::Array<double,3> xyz = ab2xyz[face](alpha, beta);
    BK::Array<double,2> pt = xyz2pt(xyz[0], xyz[1], xyz[2]);

    //    auto data = array(a, b, ia, ib);
    auto data = cs_utils.valueAB(alpha,beta,array);
    double sqrtG = cs_utils.sqrtG(a, b, ia, ib);
    
    double h = data[0]/sqrtG;
    double ua = data[1]/data[0];
    double ub = data[2]/data[0];

    BK::Array<double,3> v;
    d_parametrisation[face](v, ua, ub, alpha, beta);

    double theta = pt[1];
    double phi = pt[0];

    double uTheta = 0;
    double uPhi = 0;
    d_map_spherical(v, uTheta, uPhi, theta, phi);
    uPhi *= sin(theta);
    
    // if(face == 5) {
    // std::cout << "face = " << face << std::endl;
    // std::ofstream out("test.txt");
    // for(int i=0; i<100; i++) {
    //   double dalpha = M_PI/2/100;
    //   double alpha = i*dalpha-M_PI/4.;
    //   double beta = i*dalpha-M_PI/4.;
      
    //   //    auto data = array(a, b, ia, ib);
    //   auto data = cs_utils.valueAB(alpha,beta,array);
    //   double sqrtG = cs_utils.valueAB(alpha,beta,cs_utils.sqrtG);
      
    //   double rho = data[0]/sqrtG;
    //   double ua = data[1]/data[0];
    //   double ub = data[2]/data[0];
      
    //   BK::Array<double,3> v;
    //   d_parametrisation[face](v, ua, ub, alpha, beta);

    //   BK::Array<double,3> xyz = ab2xyz[face](alpha, beta);
    //   BK::Array<double,2> pt = xyz2pt(xyz[0], xyz[1], xyz[2]);

    //   double theta = pt[1];
    //   double phi = pt[0];
      
    //   double uTheta = 0;
    //   double uPhi = 0;
    //   d_map_spherical(v, uTheta, uPhi, theta, phi);

    //   out << i << "\t" << alpha << "\t" << theta << "\t" << phi << "\t" << uTheta << "\t" << uPhi << std::endl;
    // }
    // out.close();
    // exit(0);
    //  }
    // std::cout << "uTheta, uPhi = " << alpha << "\t" << beta << "\t" << uTheta << "\t" << uPhi << std::endl;


    // std::cout << "array = " << array(a,b,ia,ib) << std::endl;
    // std::cout << a << "\t" << b << "\t" << ia << "\t" << ib << ": " << alpha << "\t" << beta << std::endl;

    // std::cout << "value = " << cs_utils.valueAB(alpha,beta,array) << std::endl;
    // if(fabs((array(a,b,ia,ib) - cs_utils.valueAB(alpha,beta,array))[0]) > 1e-8)
    //    std::cout << array(a,b,ia,ib) - cs_utils.valueAB(alpha,beta,array) << std::endl;
    
    // exit(0);
    
    // if(fabs(fabs(cos(theta))-1) > 1e-1) {
    //   uTheta = -v[2]/sqrt(1-cos(theta)*cos(theta));
    //   uPhi = -1./sin(theta)/cos(phi)*tan(phi)/(1+tan(phi)*tan(phi))*v[0] + 1./sin(theta)/cos(phi)/(1+tan(phi)*tan(phi))*v[1];
    // }
    
    double gamma111 = cs_utils.gamma111(a,b,ia,ib);
    double gamma112 = cs_utils.gamma112(a,b,ia,ib);
    double gamma122 = cs_utils.gamma122(a,b,ia,ib);
    double gamma211 = cs_utils.gamma211(a,b,ia,ib);
    double gamma212 = cs_utils.gamma212(a,b,ia,ib);
    double gamma222 = cs_utils.gamma222(a,b,ia,ib);
    double g11 = cs_utils.G11(a,b,ia,ib);
    double g12 = cs_utils.G12(a,b,ia,ib);
    double g22 = cs_utils.G22(a,b,ia,ib);
  
    BK::Vector<double> toWriteVec({h, ua, ub, uPhi, uTheta, v[0], v[1], v[2], sqrtG, gamma111, gamma112, gamma122, gamma211,gamma212, gamma222,g11,g12,g22});
      
    return toWriteVec;
  }

  BK::Vector<double> toWriteCSV(Array const& array, BK::Array<double,2> ab, int face)
  {
    // double alpha = ab[0];
    // double beta = ab[1];
    
    // BK::Array<double,3> xyz = ab2xyz[face](alpha, beta);
    // BK::Array<double,2> pt = xyz2pt(xyz[0], xyz[1], xyz[2]);
    
    // //    auto data = array(a, b, ia, ib);
    // auto data = cs_utils.valueAB(alpha,beta,array);
    // double sqrtG = cs_utils.valueAB(alpha,beta,cs_utils.sqrtG);
    
    // double rho = data[0]/sqrtG;
    // double ua = data[1]/data[0];
    // double ub = data[2]/data[0];
    
    // BK::Array<double,3> v;
    // d_parametrisation[face](v, ua, ub, alpha, beta);
    
    // double theta = pt[1];
    // double phi = pt[0];
    
    // double uTheta = 0;
    // double uPhi = 0;
    // d_map_spherical(v, uTheta, uPhi, theta, phi);
    
    //    std::cout << alpha << "\t" << theta << "\t" << phi << "\t" << uTheta << "\t" << uPhi << std::endl;

    double alpha = ab[0];
    double beta = ab[1];
    
    BK::Array<double,3> xyz = ab2xyz[face](alpha, beta);
    BK::Array<double,2> pt = xyz2pt(xyz[0], xyz[1], xyz[2]);
    
    auto data = cs_utils.valueAB(alpha,beta,array);
    double sqrtG = cs_utils.valueAB(alpha,beta,cs_utils.sqrtG);
      
    double rho = data[0]/sqrtG;
    double ua = data[1]/data[0];
    double ub = data[2]/data[0];
      
    BK::Array<double,3> v;
    d_parametrisation[face](v, ua, ub, alpha, beta);
    double theta = pt[1];
    double phi = pt[0];
    double uTheta = 0;
    double uPhi = 0;
    d_map_spherical(v, uTheta, uPhi, theta, phi);
    uPhi *= sin(theta);
    
    // if(face == 4)
    //   std::cout << "*uTheta, uPhi = " << alpha << "\t" << beta << "\t" << face << "\t" << phi << "\t" << theta << "\t" << uTheta << "\t" << uPhi << std::endl;

    // double alpha = ab[0];
    // double beta = ab[1];

    // auto data = cs_utils.valueAB(alpha,beta,array);

    // double sqrtG = cs_utils.valueAB(alpha,beta, cs_utils.sqrtG);

    // double rho = data[0]/sqrtG;
    // double ua = data[1]/data[0];
    // double ub = data[2]/data[0];

    // BK::Array<double,3> v;
    // d_parametrisation[face](v, ua, ub, alpha, beta);

    // BK::Array<double,3> xyz = ab2xyz[face](alpha, beta);
    // BK::Array<double,2> pt = xyz2pt(xyz[0], xyz[1], xyz[2]);
    // double theta = pt[1];
    // double phi = pt[0];

    // double uTheta = 0;
    // double uPhi = 0;
    // d_map_spherical(v, uTheta, uPhi, theta, phi);

    // std::cout << "*uTheta, uPhi = " << phi << "\t" << theta << "\t" << uTheta << "\t" << uPhi << std::endl;

    // static int counter = 0;
    // ++counter;

    // if(counter == 20) exit(0);
    // // // if(fabs(fabs(cos(theta))-1) > 1e-1) {
    // // //   uTheta = -v[2]/sqrt(1-cos(theta)*cos(theta));
    // // //   uPhi = -1./sin(theta)/cos(phi)*tan(phi)/(1+tan(phi)*tan(phi))*v[0] + 1./sin(theta)/cos(phi)/(1+tan(phi)*tan(phi))*v[1];
    // // // }
    
    double gamma111 = cs_utils.valueAB(alpha,beta, cs_utils.gamma111);
    double gamma112 = cs_utils.valueAB(alpha,beta, cs_utils.gamma112);
    double gamma122 = cs_utils.valueAB(alpha,beta, cs_utils.gamma122);
    double gamma211 = cs_utils.valueAB(alpha,beta, cs_utils.gamma211);
    double gamma212 = cs_utils.valueAB(alpha,beta, cs_utils.gamma212);
    double gamma222 = cs_utils.valueAB(alpha,beta, cs_utils.gamma222);
    double g11 = cs_utils.valueAB(alpha,beta, cs_utils.G11);
    double g12 = cs_utils.valueAB(alpha,beta, cs_utils.G12);
    double g22 = cs_utils.valueAB(alpha,beta, cs_utils.G22);
  
    BK::Vector<double> toWriteVec({rho, ua, ub, uPhi, uTheta, v[0], v[1], v[2], sqrtG, gamma111, gamma112, gamma122, gamma211,gamma212, gamma222,g11,g12,g22});

    return toWriteVec;
  }

  BK::Array<double,3> computeT(int a, int b, int ia, int ib, Vector data)
  {
    double sizeA = cs_utils.sqrtG.size(0);
    if(a == sizeA) {
      a = sizeA-1;
      ia = order-1;
    }

    double sizeB = cs_utils.sqrtG.size(1);
    if(b == sizeB) {
      b = sizeB-1;
      ib = order-1;
    }
       
    double q = data[0];
    double ma = data[1];
    double mb = data[2];
    double sqrtG = cs_utils.sqrtG(a, b, ia, ib);
    
    double T11 = ma*ma/q + g_*q*q*cs_utils.GI11(a,b,ia,ib)/2/sqrtG;
    double T12 = ma*mb/q + g_*q*q*cs_utils.GI12(a,b,ia,ib)/2/sqrtG;
    double T22 = mb*mb/q + g_*q*q*cs_utils.GI22(a,b,ia,ib)/2/sqrtG;

    return {T11, T12, T22};
  }

  VecVec flux_vector(int a, int b, int ia, int ib, Vector data) {

    BK::Array<double,3> T = computeT(a,b,ia,ib, data);
    // f0x = sqrtG*h*Ux; f1x = sqrtG*h*ux*ux + 0.5*sqrtG*g*h*h*G11; f2x = sqrtG*h*ux*uy + 0.5*sqrtG*g*h*h*G12
    // f0y = sqrtG*h*Uy; f1y = sqrtG*h*uy*ux + 0.5*sqrtG*g*h*h*G21; f2y = sqrtG*h*uy*uy + 0.5*sqrtG*g*h*h*G22
    double ma = data[Var::sqrtG_h_ua];
    double mb = data[Var::sqrtG_h_ub];
    
    auto flux = VecVec{ Vector{ma, T[0], T[1]},
			Vector{mb, T[1], T[2]} };

    return flux;
  }

  BK::Array<double,2> dFlux_value(int a, int b, int ia, int ib, Vector data) {

    double sizeA = cs_utils.sqrtG.size(0);
    if(a == sizeA) {
      a = sizeA-1;
      ia = order-1;
    }

    double sizeB = cs_utils.sqrtG.size(1);
    if(b == sizeB) {
      b = sizeB-1;
      ib = order-1;
    }

    double sqrtG_h = data[0];
    double sqrtG_hUx = data[1];
    double sqrtG_hUy = data[2];
    double sqrt_h = sqrt(sqrtG_h/cs_utils.sqrtG(a, b, ia, ib));
    
    double ux = sqrtG_hUx/sqrtG_h;
    double uy = sqrtG_hUy/sqrtG_h;
    double sqrtG11 = sqrt(cs_utils.G11(a, b, ia, ib));
    double sqrtG22 = sqrt(cs_utils.G22(a, b, ia, ib));

    double sqrt_g = sqrt(g_);

    auto return_value = BK::Array<double,2>({std::max({fabs(ux), fabs(ux + sqrt_g*sqrt_h*sqrtG11), fabs(ux - sqrt_g*sqrt_h*sqrtG11)}), 
    					     std::max({fabs(uy), fabs(uy + sqrt_g*sqrt_h*sqrtG22), fabs(uy - sqrt_g*sqrt_h*sqrtG22)})});
    
    return return_value;
  }

  void computeSourceTerm(Array const& c, Array & s, int face) {
    for(int a=0; a<int(c.size(0)); a++)
      for(int b=0; b<int(c.size(1)); b++)
	for(int ia=0; ia<int(c.size(2)); ia++)
	  for(int ib=0; ib<int(c.size(3)); ib++) {
	    BK::Array<double,3> T = computeT(a,b,ia,ib, c(a,b,ia,ib));
	    
	    s(a,b,ia,ib)[0] = 0;
	    s(a,b,ia,ib)[1] = (-cs_utils.gamma111(a,b,ia,ib)*T[0]
			       - 2*cs_utils.gamma112(a,b,ia,ib)*T[1]
			       - cs_utils.gamma122(a,b,ia,ib)*T[2]);
	    
	    s(a,b,ia,ib)[2] = (-cs_utils.gamma211(a,b,ia,ib)*T[0]
			       - 2*cs_utils.gamma212(a,b,ia,ib)*T[1]
			       - cs_utils.gamma222(a,b,ia,ib)*T[2]);
	    
	  }

    if(omega_ != 0) {
      for(int a=0; a<int(c.size(0)); a++)
	for(int b=0; b<int(c.size(1)); b++)
	  for(int ia=0; ia<int(c.size(2)); ia++)
	    for(int ib=0; ib<int(c.size(3)); ib++) {
	      double alpha = cs_utils.get_alpha(a, ia);
	      double beta = cs_utils.get_beta(b, ib);
	      
	      auto pt = ab2pt[face](alpha, beta);
	      //double phi = pt[0];
	      double theta = pt[1];

	      Vector data = c(a, b, ia, ib);
	      double sqrtG = cs_utils.sqrtG(a,b,ia,ib);
	      double h = data[0]/sqrtG;
	      double ua = data[1]/data[0];
	      double ub = data[2]/data[0];
	      double factor = 2*omega_*sin(theta)/sqrtG;
	      double g11 = cs_utils.G11(a,b,ia,ib);
	      double g12 = cs_utils.G12(a,b,ia,ib);
	      double g22 = cs_utils.G22(a,b,ia,ib);
	      s(a,b,ia,ib)[1] += factor*h*(g12*ua + g22*ub);
	      s(a,b,ia,ib)[2] -= factor*h*(g11*ua + g12*ub);
	    }
    }
  }

  void computeFluxes(FluxArray& fluxArray,
		     FluxInfoBordArray& fluxInfoBordArrayX,
		     FluxInfoBordArray& fluxInfoBordArrayY,
		     Array const& c, int face) {
    
    for(int cellX=0; cellX<int(c.size(0)); cellX++)
      for(int cellY=0; cellY<int(c.size(1)); cellY++)
	for(int indexX=0; indexX<int(c.size(2)); indexX++)
	  for(int indexY=0; indexY<int(c.size(3)); indexY++) {

	    Vector data = c(cellX, cellY, indexX, indexY);
	    
	    VecVec flux = flux_vector(cellX, cellY, indexX, indexY, data);
	    
	    fluxArray(cellX, cellY, indexX, indexY) = flux;
	    if(indexX == 0) 
	      fillFluxInfoBordArray(cellX, cellY, indexY, Val::plus, c, fluxInfoBordArrayX, cellX, cellY, indexX, indexY);
	    
	    if(indexX == order-1) 
	      fillFluxInfoBordArray(cellX+1, cellY, indexY, Val::minus, c, fluxInfoBordArrayX, cellX, cellY, indexX, indexY);
	    
	    if(indexY == 0) 
	      fillFluxInfoBordArray(cellX, cellY, indexX, Val::plus, c, fluxInfoBordArrayY, cellX, cellY, indexX, indexY);

	    if(indexY == order-1) 
	      fillFluxInfoBordArray(cellX, cellY+1, indexX, Val::minus, c, fluxInfoBordArrayY, cellX, cellY, indexX, indexY);
	    
	  }
  }

  void computeNumericalFlux(FluxVecArray& fluxVecArrayX, FluxVecArray& fluxVecArrayY,
			    FluxInfoBordArray& fluxInfoArrayX, FluxInfoBordArray& fluxInfoArrayY, int face) {
    
    for(size_t posX=0; posX<fluxVecArrayX.size(0); posX++)
      for(size_t posY=0; posY<fluxVecArrayX.size(1); posY++)
	for(size_t orderY=0; orderY<order; orderY++) {

	  Vector valM = fluxInfoArrayX(posX, posY, orderY)[Val::minus];
	  VecVec fm = flux_vector(posX, posY, 0, orderY, valM);

	  Vector valP = fluxInfoArrayX(posX, posY, orderY)[Val::plus];
	  VecVec fp = flux_vector(posX, posY, 0, orderY, valP);
				  
	  double dfm = dFlux_value(posX, posY, 0, orderY, valM)[Dir::x];
	  double dfp = dFlux_value(posX, posY, 0, orderY, valP)[Dir::x];

	  fluxVecArrayX(posX, posY, orderY) =  laxFriedrichs1d_flux_numeric(fm[Dir::x], fp[Dir::x], dfm, dfp, valM, valP);
	}

    for(size_t posX=0; posX<fluxVecArrayY.size(0); posX++)
      for(size_t posY=0; posY<fluxVecArrayY.size(1); posY++)
	for(size_t orderX=0; orderX<order; orderX++) {

	  Vector valM = fluxInfoArrayY(posX, posY, orderX)[Val::minus];
	  VecVec fm = flux_vector(posX, posY, orderX, 0, valM);
	  
	  Vector valP = fluxInfoArrayY(posX, posY, orderX)[Val::plus];
	  VecVec fp = flux_vector(posX, posY, orderX, 0, valP);
	    
	  double dfm = dFlux_value(posX, posY, orderX, 0, valM)[Dir::y];
	  double dfp = dFlux_value(posX, posY, orderX, 0, valP)[Dir::y];

	  fluxVecArrayY(posX, posY, orderX) =  laxFriedrichs1d_flux_numeric(fm[Dir::y], fp[Dir::y], dfm, dfp, valM, valP);
	}
  }

  CONSTPARA(double, da);
  CONSTPARA(double, db);
  PROTEPARA(double, g);
  PROTEPARA(double, omega);
  
  BK::Vector<BK::MultiArray<spaceDim*2, double>> uPhiVec;
  BK::Vector<BK::MultiArray<spaceDim*2, double>> uThetaVec;
  
};

typedef Problem_ShallowWaterCubed Problem;
extern Problem& problem;

#endif
