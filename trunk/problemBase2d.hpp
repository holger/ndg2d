#ifndef problemBase2d_hpp
#define problemBase2d_hpp

#include <algorithm>

#include <BasisKlassen/filesystem.hpp>
#include <BasisKlassen/TypeManip.hpp>
#include <BasisKlassen/output.hpp>

#include "types.hpp"
#include "order.hpp"
#include "notiModify.hpp"
#include "writeArray2d.hpp"
#include "laxFriedrichs1d.hpp"
#include "centralFlux1d.hpp"
#include "logger.hpp"

// LOGGING: ----------------------------------------------------
// #undef LOGLEVEL
// #define LOGLEVEL 4

template<int varDim>
class ProblemBase2d : public DataTypes<2, varDim>
{
public:

  typedef typename DataTypes<2, varDim>::Array Array;

  struct Dir {
    static const int x = 0;
    static const int y = 1;
  };

  ProblemBase2d() : varDim_(varDim) {
    initialized_ = false;
    step_ = 0;
    time_ = 0;
    startStep_ = 0;
  }

  void init(int ncelx, int ncely, std::string const& parameterFilename) {
    LOGL(1,"ProblemBase2d::init(int ncelx, int ncely, std::string const& parameterFilename)");
    
    ncelx_ = ncelx;
    ncely_ = ncely;

    parameterFilename_ = parameterFilename;
    readBaseParameterFromFile(parameterFilename);

    dx_ = lx_/ncelx;
    dy_ = ly_/ncely;
    cfl_ = cflFactor_*cflLimit[rkOrder_-1][order-1];

    lagrangeMatrix_.init();
    dLagrangeMatrix_.init();
    
    if(!initialized_)
      notifyField.add("field", FieldNotifyCallBack(std::bind(&ProblemBase2d::write,this,std::placeholders::_1), "ProblemBase2d::write"));

    LOGL(4,"ProblemBase2d::init(int ncelx, int ncely, std::string const& parameterFilename)-end");
  }

  BK::Array<double,2> ij2xy(int ix, int iy, int ox, int oy) {
    double startvalx = 0.0 +  mpiBase.procCoord()[0] * (lx_/ mpiBase.get_dimSize()[0]);
    double startvaly = 0.0 +  mpiBase.procCoord()[1] * (ly_/ mpiBase.get_dimSize()[1]);

    BK::Vector<double> xi = points[order-1];

    double x = xi[ox];
    double y = xi[oy];
    return {startvalx + ix*dx_+0.5*dx_+x*0.5*dx_, startvaly + iy*dy_+0.5*dy_+y*0.5*dy_};
  }
  
  template<typename Functor>
  double derivativeX(Functor uFunc, Array const& c, int cellX, int cellY, int indexX, int indexY) const {
    double dxu=0;

    for(int m=0; m<order; m++) 
      for(int n=0; n<order; n++) {
	double u = uFunc(c, cellX, cellY, m, n);
	dxu += u*dLagrangeMatrix_(m, indexX)* lagrangeMatrix_(n, indexY);
      }
    
    dxu = dxu/dx_*2;
    
    return dxu;
  }

  template<typename Functor>
  double derivativeY(Functor uFunc, Array const& c, int cellX, int cellY, int indexX, int indexY) const {
    double dyu=0;

    for(int m=0; m<order; m++) 
      for(int n=0; n<order; n++) {
	double u = uFunc(c, cellX, cellY, m, n);
      
	dyu += u*lagrangeMatrix_(m, indexX)* dLagrangeMatrix_(n, indexY);
    }
    
    dyu = dyu/dy_*2;
    
    return dyu;
  }

  template<typename FluxInfo>
  FluxInfo bcYT(int ix, int ox, BK::Type2Type<FluxInfo> t) {
    std::cerr << "ERROR: bcYT not defined in problem class\n";
    exit(1);
    FluxInfo vec;
    return vec;
  }

  template<typename FluxInfo>
  FluxInfo bcYB(int ix, int ox, BK::Type2Type<FluxInfo> t) {
    std::cerr << "ERROR: bcYB not defined in problem class\n";
    exit(1);
    FluxInfo vec;
    return vec;
  }

  template<typename FluxInfo>
  FluxInfo bcXL(int ix, int ox, BK::Type2Type<FluxInfo> t) {
    std::cerr << "ERROR: bcYL not defined in problem class\n";
    exit(1);
    FluxInfo vec;
    return vec;
  }

  template<typename FluxInfo>
  FluxInfo bcXR(int ix, int ox, BK::Type2Type<FluxInfo> t) {
    std::cerr << "ERROR: bcYR not defined in problem class\n";
    exit(1);
    FluxInfo vec;
    return vec;
  }

  void computeSourceTerm(Array const& c, Array & s, int face=0) {
    assert(initialized_);
  }

  void readBaseParameterFromFile(std::string parameterFilename) {
    BK::ReadInitFile initFile(parameterFilename,true);
    std::string pathToBaseInitFile = initFile.getParameter<std::string>("problemBase2dInitFilePath");

    BK::path path(parameterFilename);
    BK::path branch_path = path.branch_path();
    //    std::cout << "branch_path = " << branch_path.string() << std::endl;
    std::string parameterBaseFilename = branch_path.string() + "/problemBase2d.init";
    
    BK::ReadInitFile initBaseFile(parameterBaseFilename,true);
    rkOrder_ = initBaseFile.getParameter<size_t>("rkOrder");
    lx_ = initBaseFile.getParameter<double>("lx");
    ly_ = initBaseFile.getParameter<double>("ly");
    T_ = initBaseFile.getParameter<double>("T");
    nt_ = initBaseFile.getParameter<double>("nt");
    cflFactor_ = initBaseFile.getParameter<double>("cfl");
    diagOutputInterval_ = initBaseFile.getParameter<double>("diagOutputInterval");
    initBaseFile.getList("outputFormat",outputFormat_);
    writeTimeInterval_ = initBaseFile.getParameter<double>("writeTimeInterval");
    writeStepInterval_ = initBaseFile.getParameter<int>("writeStepInterval");
    if(!initBaseFile.parameterExists("bcX")) bcX_ = 0;
    else bcX_ = initBaseFile.getParameter<int>("bcX");
    if(!initBaseFile.parameterExists("bcY")) bcY_ = 0;
    else bcY_ = initBaseFile.getParameter<int>("bcY");
  }

  void write(Array const* c) {
    LOGL(3,"ProblemBase2d::write");
    
    if((writeTimeInterval_ > 0 && fmod(time_, writeTimeInterval_) < dt_) || (writeStepInterval_ > 0 && (step_ % writeStepInterval_) == 0)) {
      LOGL(4,BK::toString("write ",step_));
      
      std::string dirName = BK::toString("data/",name_);

      int nPoParCel=10;

      size_t n_zero = 7;
      std::string old_str = std::to_string(step_);
      auto new_step_str = std::string(n_zero - std::min(n_zero, old_str.length()), '0') + old_str;

      if(std::find(outputFormat_.begin(), outputFormat_.end(), 0) != std::end(outputFormat_))
	writeFine(*c, dirName + "/sol_" +  std::to_string(order) + "-" + std::to_string(rkOrder_) +"_" + std::to_string(ncelx_) + "_" + std::to_string(ncely_) + "_" + std::to_string(mpiBase.get_commRank()) + "-" + new_step_str, nPoParCel, dx_, dy_);

      
#ifdef NETCDF
      if(std::find(outputFormat_.begin(), outputFormat_.end(), 1) != std::end(outputFormat_))
	writeNetCDF(*c, dirName + "/sol_" +  std::to_string(order) + "-" + std::to_string(rkOrder_) +"_" + std::to_string(ncelx_) + "_" + std::to_string(ncely_) + "_" + std::to_string(mpiBase.get_commRank()) + "-" + new_step_str, ncelx_, ncely_, dx_, dy_);

#endif
    }
    LOGL(4,"ProblemBase2d::write-end");
  }

  template<typename FluxVecArray, typename FluxInfoBordArray, typename FluxFunc, typename DFluxFunc>
  void computeNumericalFlux_LF(FluxVecArray& fluxVecArrayX, FluxVecArray& fluxVecArrayY,
			       FluxInfoBordArray& fluxInfoArrayX, FluxInfoBordArray& fluxInfoArrayY, FluxFunc fluxFunc, DFluxFunc dFluxFunc) {
    
    for(size_t cellX=0; cellX<fluxVecArrayX.size(0); cellX++)
      for(size_t cellY=0; cellY<fluxVecArrayX.size(1); cellY++)
	for(size_t orderY=0; orderY<order; orderY++) {

	  auto valM = fluxInfoArrayX(cellX, cellY, orderY)[Val::minus];   // Vector valM
	  auto fm = fluxFunc(valM);	  // VecVec fm

	  auto valP = fluxInfoArrayX(cellX, cellY, orderY)[Val::plus];
	  auto fp = fluxFunc(valP);
				  
	  double dfm = dFluxFunc(valM)[Dir::x];
	  double dfp = dFluxFunc(valP)[Dir::x];

	  fluxVecArrayX(cellX, cellY, orderY) =  laxFriedrichs1d_flux_numeric(fm[Dir::x], fp[Dir::x], dfm, dfp, valM, valP);
	}

    for(size_t cellX=0; cellX<fluxVecArrayY.size(0); cellX++)
      for(size_t cellY=0; cellY<fluxVecArrayY.size(1); cellY++)
	for(size_t orderX=0; orderX<order; orderX++) {

	  auto valM = fluxInfoArrayY(cellX, cellY, orderX)[Val::minus];
	  auto fm = fluxFunc(valM);
	  
	  auto valP = fluxInfoArrayY(cellX, cellY, orderX)[Val::plus];
	  auto fp = fluxFunc(valP);
	    
	  double dfm = dFluxFunc(valM)[Dir::y];
	  double dfp = dFluxFunc(valP)[Dir::y];

	  fluxVecArrayY(cellX, cellY, orderX) =  laxFriedrichs1d_flux_numeric(fm[Dir::y], fp[Dir::y], dfm, dfp, valM, valP);
	}
  }

  template<typename FluxVecArray, typename FluxInfoBordArray, typename FluxFunc>
  void computeNumericalFlux_Central(FluxVecArray& fluxVecArrayX, FluxVecArray& fluxVecArrayY,
				    FluxInfoBordArray& fluxInfoArrayX, FluxInfoBordArray& fluxInfoArrayY, FluxFunc fluxFunc) {
    
    for(size_t cellX=0; cellX<fluxVecArrayX.size(0); cellX++)
      for(size_t cellY=0; cellY<fluxVecArrayX.size(1); cellY++)
	for(size_t orderY=0; orderY<order; orderY++) {

	  auto valM = fluxInfoArrayX(cellX, cellY, orderY)[Val::minus];   // Vector valM
	  auto fm = fluxFunc(valM);	  // VecVec fm

	  auto valP = fluxInfoArrayX(cellX, cellY, orderY)[Val::plus];
	  auto fp = fluxFunc(valP);
				  
	  fluxVecArrayX(cellX, cellY, orderY) =  central1d_flux_numeric(fm[Dir::x], fp[Dir::x]);
	}

    for(size_t cellX=0; cellX<fluxVecArrayY.size(0); cellX++)
      for(size_t cellY=0; cellY<fluxVecArrayY.size(1); cellY++)
	for(size_t orderX=0; orderX<order; orderX++) {

	  auto valM = fluxInfoArrayY(cellX, cellY, orderX)[Val::minus];
	  auto fm = fluxFunc(valM);
	  
	  auto valP = fluxInfoArrayY(cellX, cellY, orderX)[Val::plus];
	  auto fp = fluxFunc(valP);
	    
	  fluxVecArrayY(cellX, cellY, orderX) =  central1d_flux_numeric(fm[Dir::y], fp[Dir::y]);
	}
  }

  template<typename Array, typename FluxInfoBordArray>
  void fillFluxInfoBordArray(int cellXf, int cellYf, int index, int plusMinus,
			     Array const& c, FluxInfoBordArray& fluxInfoBordArray, int cellX, int cellY, int indexX, int indexY) {
    
    auto data = c(cellX, cellY, indexX, indexY);
    for(int var=0;var<varDim;var++)
      fluxInfoBordArray(cellXf, cellYf, index)[plusMinus][var] = data[var];
  }

  template<typename FluxArray, typename FluxInfoBordArray, typename Array, typename Problem>
  void computeFluxes(FluxArray& fluxArray,
		     FluxInfoBordArray& fluxInfoBordArrayX,
		     FluxInfoBordArray& fluxInfoBordArrayY,
		     Array const& c, Problem& problem) {
    
    for(int cellX=0; cellX<int(c.size(0)); cellX++)
      for(int cellY=0; cellY<int(c.size(1)); cellY++)
	for(int indexX=0; indexX<int(c.size(2)); indexX++)
	  for(int indexY=0; indexY<int(c.size(3)); indexY++) {
	    auto flux = problem.flux_vector(c(cellX, cellY, indexX, indexY)); // VecVec flux
	    fluxArray(cellX, cellY, indexX, indexY) = flux;
	    if(indexX == 0) 
	      fillFluxInfoBordArray(cellX, cellY, indexY, Val::plus, c, fluxInfoBordArrayX, cellX, cellY, indexX, indexY);
	    
	    if(indexX == order-1) 
	      fillFluxInfoBordArray(cellX+1, cellY, indexY, Val::minus, c, fluxInfoBordArrayX, cellX, cellY, indexX, indexY);
	    
	    if(indexY == 0) 
	      fillFluxInfoBordArray(cellX, cellY, indexX, Val::plus, c, fluxInfoBordArrayY, cellX, cellY, indexX, indexY);

	    if(indexY == order-1) 
	      fillFluxInfoBordArray(cellX, cellY+1, indexX, Val::minus, c, fluxInfoBordArrayY, cellX, cellY, indexX, indexY);
	    
	  }
  }

  void logParameter() {
    LOGL(1,PAR(name_));
    LOGL(1,PAR(parameterFilename_));
    LOGL(1,PAR(ncelx_));
    LOGL(1,PAR(ncely_));
    LOGL(1,PAR(lx_));
    LOGL(1,PAR(ly_));
    LOGL(1,PAR(T_));
    LOGL(1,PAR(cfl_));
    LOGL(1,PAR(cflFactor_));
    LOGL(1,PAR(dt_));
    LOGL(1,PAR(nt_));
    LOGL(1,PAR(dx_));
    LOGL(1,PAR(dy_));
    LOGL(1,PAR(dt_));
    LOGL(1,PAR(varDim_));
    LOGL(1,PAR(order));
    LOGL(1,PAR(get_stopStep()));
    LOGL(1,PAR(diagOutputInterval_));
    LOGL(1,PAR(bcX_));
    LOGL(1,PAR(bcY_));
  }

  int get_stopStep() {
    return startStep_ + nt_;
  }

  PROTEPARA(std::string, parameterFilename);
  PROTEPARA(std::string, name);
  PROTEPARA(size_t, ncelx);
  PROTEPARA(size_t, ncely);
  PROTEPARA(double, lx);
  PROTEPARA(double, ly);
  PROTEPARA(double, dx);
  PROTEPARA(double, dy);
  PROTEPARA(double, T);
  PROTEPARA(double, dt);
  PROTEPARA(double, cfl);
  PROTEPARA(double, cflFactor);
  PROTEPARA(int, nt);
  PROTEPARA(bool, initialized);
  PROCONPAR(int, varDim);
  PROTEPARA(int, rkOrder);
  PROTEPARA(int, step);
  PROTEPARA(int, startStep);
  PROTEPARA(double, time);
  PROTEPARA(int, diagOutputInterval);
  PROTEPARA(BK::Vector<int>, outputFormat);
  PROTEPARA(double, writeTimeInterval);
  PROTEPARA(int, writeStepInterval);
  CONSTPARA(int, bcX);
  CONSTPARA(int, bcY);

  DLagrangeMatrix<order> dLagrangeMatrix_;
  LagrangeMatrix<order> lagrangeMatrix_;
};

#endif
