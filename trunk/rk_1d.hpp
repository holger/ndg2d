#ifndef rk_1d_hpp
#define rk_1d_hpp

template<class Array, class Rhs, class BoundaryCondition, class Problem>
void rk1(Array& cnew, Array const& c, double dt, Rhs f, BoundaryCondition boundaryCondition, Problem const& problem)
{
  Array k1(c.dim());
  problem.computeSourceTerm(c, f.sourceArray);
  f(c, k1, boundaryCondition);
  problem.lhs(cnew, c, k1);
}

template<class Array, class Rhs, class BoundaryCondition, class Problem>
void rk2(Array& cnew, Array const& c, double dt, Rhs f, BoundaryCondition boundaryCondition, Problem const& problem)
{
  Array c2(c.dim());
  Array k(c.dim());
  Array k1(c.dim());

  problem.computeSourceTerm(c, f.sourceArray);
  f(c, k1, boundaryCondition);
  k=k1/2.;
  problem.lhs(c2, c, k);

  problem.computeSourceTerm(c2, f.sourceArray);
  f(c2, k1, boundaryCondition);
  problem.lhs(cnew, c, k);
}

template<class Array, class Rhs, class BoundaryCondition, class Problem>
void rk3(Array& cnew, Array const& c, double dt, Rhs f, BoundaryCondition boundaryCondition, Problem const& problem)
{
  Array cTemp(c.dim());
  Array k(c.dim());
  Array k1(c.dim());
  Array k2(c.dim());
  Array k3(c.dim());

  problem.computeSourceTerm(c, f.sourceArray);
  f(c, k1, boundaryCondition);
  k=k1/3.;
  problem.lhs(cTemp, c, k);

  problem.computeSourceTerm(cTemp, f.sourceArray);
  f(cTemp, k2, boundaryCondition);
  k=2*k2/3.;
  problem.lhs(cTemp, c, k);

  problem.computeSourceTerm(cTemp, f.sourceArray);
  f(cTemp, k3, boundaryCondition);
  k=(k1 + 3*k3)/4.;
  problem.lhs(cnew, c, k);
}

template<class Array, class Rhs, class BoundaryCondition, class Problem>
void rk3_vec(std::vector<Array> & cnew, std::vector<Array> const& c, double dt, Rhs f, BoundaryCondition boundaryCondition, Problem const& problem)
{
  std::vector<Array> cTemp(c.size(), Array(c[0].dim()));
  std::vector<Array> k(c.size(), Array(c[0].dim()));
  std::vector<Array> k1(c.size(), Array(c[0].dim()));
  std::vector<Array> k2(c.size(), Array(c[0].dim()));
  std::vector<Array> k3(c.size(), Array(c[0].dim()));

  for(size_t i=0; i<c.size(); i++) {
    f(c[i], k1[i], boundaryCondition);
    k[i]=k1[i]/3.;
    problem.lhs(cTemp[i], c[i], k[i]);
  }

  for(size_t i=0; i<c.size(); i++) {
    f(cTemp[i], k2[i], boundaryCondition);
    k[i]=2*k2[i]/3.;
    problem.lhs(cTemp[i], c[i], k[i]);
  }

  for(size_t i=0; i<c.size(); i++) {
    f(cTemp[i], k3[i], boundaryCondition);
    k[i]=(k1[i] + 3*k3[i])/4.;
    problem.lhs(cnew[i], c[i], k[i]);
  }
}

template<class Array, class Rhs, class BoundaryCondition, class Problem>
void rk4(Array& cnew, Array const& c, double dt, Rhs f, BoundaryCondition boundaryCondition, Problem const& problem)
{
  Array cTemp(c.dim());
  Array k(c.dim());
  Array k1(c.dim());
  Array k2(c.dim());
  Array k3(c.dim());
  Array k4(c.dim());

  problem.computeSourceTerm(c, f.sourceArray);
  f(c, k1, boundaryCondition);
  k=0.5*k1;
  problem.lhs(cTemp, c, k);

  problem.computeSourceTerm(cTemp, f.sourceArray);
  f(cTemp, k2, boundaryCondition);
  k=0.5*k2;
  problem.lhs(cTemp, c, k);

  problem.computeSourceTerm(cTemp, f.sourceArray);
  f(cTemp, k3, boundaryCondition);
  problem.lhs(cTemp, c, k3);

  problem.computeSourceTerm(cTemp, f.sourceArray);
  f(cTemp, k4, boundaryCondition);
  k=(k1 + 2*(k2 + k3) + k4)/6.;
  problem.lhs(cnew, c, k);
}

template<class Array, class Rhs, class BoundaryCondition, class Problem>
void rk5(Array& cnew, Array const& c, double dt, Rhs f, BoundaryCondition boundaryCondition, Problem const& problem)
{
  Array cTemp(c.dim());
  Array k(c.dim());
  Array k0(c.dim());
  Array k1(c.dim());
  Array k2(c.dim());
  Array k3(c.dim());
  Array k4(c.dim());
  Array k5(c.dim());

  problem.computeSourceTerm(c, f.sourceArray);
  f(c, k0, boundaryCondition);
  k=0.25*k0;
  problem.lhs(cTemp, c, k);

  problem.computeSourceTerm(cTemp, f.sourceArray);
  f(cTemp, k1, boundaryCondition);
  k=(k0 + k1)/8.;
  problem.lhs(cTemp, c, k);

  problem.computeSourceTerm(cTemp, f.sourceArray);
  f(cTemp, k2, boundaryCondition);
  k=(-0.5*k1 + k2);
  problem.lhs(cTemp, c, k);

  problem.computeSourceTerm(cTemp, f.sourceArray);
  f(cTemp, k3, boundaryCondition);
  k=(3./16*k0 + 9./16*k3);
  problem.lhs(cTemp, c, k);

  problem.computeSourceTerm(cTemp, f.sourceArray);
  f(cTemp, k4, boundaryCondition);
  k=(-3./7*k0 + 2./7*k1 + 12./7*k2 - 12./7*k3 + 8./7*k4);
  problem.lhs(cTemp, c, k);

  problem.computeSourceTerm(cTemp, f.sourceArray);
  f(cTemp, k5, boundaryCondition);
  k=(7*k0 + 32*k2 + 12*k3 + 32*k4 + 7*k5)/90;
  problem.lhs(cnew, c, k);
}

template<class Array, class Rhs, class BoundaryCondition, class Problem>
void rk6(Array& cnew, Array const& c, double dt, Rhs f, BoundaryCondition boundaryCondition, Problem const& problem)
{
  Array cTemp(c.dim());
  Array k(c.dim());
  Array k1(c.dim());
  Array k2(c.dim());
  Array k3(c.dim());
  Array k4(c.dim());
  Array k5(c.dim());
  Array k6(c.dim());
  Array k7(c.dim());

  
  problem.computeSourceTerm(c, f.sourceArray);
  f(c, k1, boundaryCondition);


  problem.lhs(cTemp, c, k1);


  problem.computeSourceTerm(cTemp, f.sourceArray);
  f(cTemp, k2, boundaryCondition);
  k = (3*k1 + k2)/8.;
  problem.lhs(cTemp, c, k);


  problem.computeSourceTerm(cTemp, f.sourceArray);
  f(cTemp, k3, boundaryCondition);
  k = (8*k1 + 2*k2 + 8*k3)/27.;
  problem.lhs(cTemp, c, k);
  
  // for(size_t cellX=0; cellX<c.size(0); cellX++) 
  //   std::cout << cTemp(cellX, 0) << "\t" << c(cellX, 0) << "\t" << k(cellX, 0) << std::endl;
  // exit(0);

  problem.computeSourceTerm(cTemp, f.sourceArray);
  f(cTemp, k4, boundaryCondition);
  k = (+ 3*(3*sqrt(21)-7)*k1
       - 8*(7-sqrt(21))*k2
       + 48*(7-sqrt(21))*k3
       - 3*(21 - sqrt(21))*k4)/392;
  problem.lhs(cTemp, c, k);

  problem.computeSourceTerm(cTemp, f.sourceArray);
  f(cTemp, k5, boundaryCondition);
  k = (- 5*(231+51*sqrt(21))*k1
       - 40*(7+sqrt(21))*k2
       - 320*sqrt(21)*k3
       + 3*(21+121*sqrt(21))*k4
       + 392*(6+sqrt(21))*k5)/1960;
  problem.lhs(cTemp, c, k);
  
  problem.computeSourceTerm(cTemp, f.sourceArray);
  f(cTemp, k6, boundaryCondition);
  k = (+ 15*(22+7*sqrt(21))*k1
       + 120*k2
       + 40*(7*sqrt(21)-5)*k3
       - 63*(3*sqrt(21)-2)*k4
       - 14*(9*sqrt(21)+49)*k5
       + 70*(7-sqrt(21))*k6)/180;
  problem.lhs(cTemp, c, k);

  problem.computeSourceTerm(cTemp, f.sourceArray);
  f(cTemp, k7, boundaryCondition);
  k = (9*k1 + 64*k3 + 49*k5 + 49*k6 + 9*k7)/180.;
  problem.lhs(cnew, c, k);
}

template<class Array, class Rhs, class BoundaryCondition, class Problem>
void rk(Array& cnew, Array const& c, double dt, Rhs f, BoundaryCondition boundaryCondition, Problem const& problem)
{
  size_t rkOrder = problem.get_rkOrder();
  
  if(rkOrder == 1)
    rk1(cnew, c, dt, f, boundaryCondition, problem);
  
  if(rkOrder == 2)
    rk2(cnew, c, dt, f, boundaryCondition, problem);
  
  if(rkOrder == 3)
    rk3(cnew, c, dt, f, boundaryCondition, problem);
  
  if(rkOrder == 4)
    rk4(cnew, c, dt, f, boundaryCondition, problem);
  
  if(rkOrder == 5)
    rk5(cnew, c, dt, f, boundaryCondition, problem);
  
  if(rkOrder == 6)
    rk6(cnew, c, dt, f, boundaryCondition, problem);  
}

#endif
