#ifndef borderFlux2d_hpp
#define borderFlux2d_hpp

#include "order.hpp"
#include "laxFriedrichs2d.hpp"
#include "problem2d.hpp"

// 2D Flux -----------------------------------------------------------------------------------------------------

template<class Array>

typename Array::value_type borderFluxX(int cellX, int cellY, int orderX, int orderY, Array const& c)
{
  static BK::Vector<double> wi = weights[order-1];

  typedef typename Array::value_type Vector;
  
  if(orderX == order-1) 
    return Vector{flux_numeric_XR(cellX, cellY, orderY, c)*problem.get_dy()/2.*wi[orderY]};

  else if(orderX == 0)
    return Vector{-flux_numeric_XL(cellX, cellY, orderY, c)*problem.get_dy()/2.*wi[orderY]};
  
  else return Vector(0);
}

template<class Array>
typename Array::value_type borderFluxY(int cellX, int cellY, int orderX, int orderY, Array const& c)
{
  static BK::Vector<double> wi = weights[order-1];

  typedef typename Array::value_type Vector;
  
  if(orderY == order-1)
    return Vector{flux_numeric_YT(cellX, cellY, orderX, c)*problem.get_dx()/2.*wi[orderX]};

  else if(orderY == 0)
    return Vector{-flux_numeric_YB(cellX, cellY, orderX, c)*problem.get_dx()/2.*wi[orderX]};
  
  else return Vector(0);
}

#endif
