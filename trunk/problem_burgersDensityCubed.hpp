#ifndef problem_burgersDensityCubed_hpp
#define problem_burgersDensityCubed_hpp

#include <BasisKlassen/parameter.hpp>
#include <BasisKlassen/array.hpp>
#include <BasisKlassen/parseFromFile.hpp>

const int varDim = 3;   // three variables: sqrtG*h, sqrtG*h*ux, sqrtG*h*uy
const int spaceDim = 2; // 2d space

#include "types.hpp"
#include "cfl.hpp"
#include "problemBase2d.hpp"
#include "laxFriedrichs1d.hpp"
#include "gaussLobatto.hpp"
#include "notiModify.hpp"
#include "order.hpp"
#include "cubedSphere_utils.hpp"

extern CubedSphere_utils& cs_utils;

// 2D Burgers on cubed sphere geometry
class Problem_BurgersDensityCubed : public ProblemBase2d<varDim>
{
public:

  struct Var {
    static const int sqrtG_h = 0;      // sqrt(G)*h
    static const int sqrtG_h_ua = 1;   // sqrt(G)*h*ua
    static const int sqrtG_h_ub = 2;   // sqrt(G)*h*ub
  };

  static const int fluxInfoNumber = 3;

  const BK::Array<BK::Array<int, 2>, 1> fluxVarVecIndizes = {{Var::sqrtG_h_ua, Var::sqrtG_h_ub}};
  const BK::Array<int, 1> fluxVarScalarIndizes = {Var::sqrtG_h};

  static const int fluxVarianceType = 0; // 0=contra-variant; 1=co-variant
  
  typedef BK::Array<double, varDim> FluxInfo;                         // [sqrtG_h, sqrtG_h_ua, sqrtG_h_ub] 
  typedef BK::Array<FluxInfo, 2> FluxInfoBord;                        // [+-][sqrtG_h, sqrtG_h_ua, sqrtG_h_ub] 
  typedef BK::MultiArray<spaceDim+1, FluxInfoBord> FluxInfoBordArray; // (cell)[+-][sqrtG_h, sqrtG_h_ua, sqrtG_h_ub]
  typedef BK::MultiArray<spaceDim+1, Vector> FluxVecArray;
							       
  friend class BK::StackSingleton<Problem_BurgersDensityCubed>;

  Problem_BurgersDensityCubed() {}
  
  void lhs(Array& cNew, Array const& c, Array const& rhs) const {
    cNew = c + dt_*rhs;
  }    

  void init(int ncelx, int ncely, std::string parameterFilename) {
    if(!initialized_) {
      notifyFieldVec.add("field", FieldVecNotifyCallBack(std::bind(&Problem_BurgersDensityCubed::diagnostic,this,std::placeholders::_1), "Problem_BurgersDensityCubed::diagnostic"));
    }

    ProblemBase2d::init(ncelx, ncely, parameterFilename);
    double nt = nt_;
    
    uaVec.resize(6, BK::MultiArray<spaceDim*2,double>(ncelx_, ncely_, order, order));
    ubVec.resize(6, BK::MultiArray<spaceDim*2,double>(ncelx_, ncely_, order, order));

    initVelocity();
    
    if(nt > 0 && T_ > 0) {
      std::cerr << "ERROR in Problem_burgersDensityCubed::init(): nt = " <<
	nt << " > 0 && T = " << T_ << " > 0." << std::endl;
      exit(1);						   
    }

    assert(dx_ == dy_);
    
    dt_ = fabs(cfl_*dx_);

    if(nt > 0) {
      nt_ = nt;
      T_ = nt_*dt_;
    }
    else
      nt_ = int(T_/dt_) + 1;

    dt_ *= T_/(nt_*dt_);

    da_ = M_PI/2./ncelx_;
    db_ = M_PI/2./ncely_;
    
    name_ = "burgersDensityCubed";

    logParameter();

    initialized_ = true;
  }

  void diagnostic(BK::Vector<Array> const* c) {
    LOGL(3,"Problem_BurgersDensityCubed::diagnostic");
    
    if(step_ % diagOutputInterval_ != 0) {
      
      LOGL(4,"Problem_BurgersDensityCubed::diagnostic--end");
      return;
    }
    
    // auto w = weights[order-1];

    // std::function<double(int, int, int ,int, Vector)> f_sqrtG_h = [&](int a, int b, int ia, int ib, Vector data) {return data[Var::sqrtG_h];};
    // double int_sqrtG_h = cs_utils.integrate(f_sqrtG_h, c);

    // std::function<double(int, int, int ,int, Vector)> f_sqrtG_h_ua = [&](int a, int b, int ia, int ib, Vector data) {return data[Var::sqrtG_h_ua];};
    // double int_sqrtG_h_ua = cs_utils.integrate(f_sqrtG_h_ua, c);

    // std::function<double(int, int, int ,int, Vector)> f_sqrtG_h_ub = [&](int a, int b, int ia, int ib, Vector data) {return data[Var::sqrtG_h_ub];};
    // double int_sqrtG_h_ub = cs_utils.integrate(f_sqrtG_h_ub, c);

    // std::function<double(int, int, int ,int, Vector)> f_energy = [&](int a, int b, int ia, int ib, Vector data) {
    //   double g11 = cs_utils.G11(a,b,ia,ib);
    //   double g12 = cs_utils.G12(a,b,ia,ib);
    //   double g22 = cs_utils.G22(a,b,ia,ib);
    //   double sqrtG = cs_utils.sqrtG(a, b, ia, ib);
    //   double h = data[0]/sqrtG;
    //   double ua = data[1]/data[0];
    //   double ub = data[2]/data[0];

    //   return 0.5*(ua*ua*g11 + 2*ua*ub*g12 + ub*ub*g22);};

    // double energy = cs_utils.integrate(f_energy, c);
    
    // std::cout << "int_sqrtG_h = " << int_sqrtG_h << " int_sqrtG_h_ua = " << int_sqrtG_h_ua
    // 	      << " int_sqrtG_h_ub = " << int_sqrtG_h_ub << " energy = " << energy << std::endl;

    // std::string dirName = BK::toString("data/", name_);
    // std::ofstream out(BK::toString(dirName + "/diag-",order,"-",rkOrder_,".txt"), std::ios::app);
    // out << step_ << "\t" << int_sqrtG_h << "\t" << int_sqrtG_h_ua << "\t" << int_sqrtG_h_ub << "\t" << energy << std::endl;
    // out.close();

    LOGL(4,"Problem_BurgersDensityCubed::diagnostic-end");
  }

  void initVelocity() {
    std::ofstream outx("initVx.csv"); outx << "x,y,z,vx,vy,vz,dv00,dv01,dv10,dv11" << std::endl;
    
    BK::Vector<double> xi = points[order-1];

    da_ = M_PI/2./ncelx_;
    db_ = M_PI/2./ncely_;

    for(size_t a = 0; a<ncelx_; a++) {
      auto alphaFunc = [a, this](double x) { return -M_PI/4. + a*da_+ 0.5*da_ + x*0.5*da_;};
      for(size_t b = 0; b<ncely_; b++) {
	auto betaFunc = [b, this](double y) { return -M_PI/4. + b*db_+ 0.5*db_ + y*0.5*db_;};
	for(size_t ia = 0; ia < order; ia++)
	  for(size_t ib = 0; ib < order; ib++) {

	    double alpha = alphaFunc(xi[ia]);
	    double beta = betaFunc(xi[ib]);
	    BK::Array<double,3> xyz;
	    BK::Array<double,3> v;
	    BK::Array<double,3> v2;

	    for(int face=0; face<6;face++) {
	      xyz = ab2xyz[face](alpha, beta);
	      setVelocity(xyz,v);
	      
	      d_map[face](v, uaVec[face](a, b, ia, ib), ubVec[face](a, b, ia, ib), xyz);
	      d_parametrisation[face](v2, uaVec[face](a, b, ia, ib), ubVec[face](a, b, ia, ib), alpha, beta);
	    }
	  }
      }
    }
    outx.close();
  }
  
  inline double sec(double phi)
  {
    return 1./cos(phi);
  };
  
  void setVelocity(BK::Array<double,3> xyz, BK::Array<double,3>& v){
    double omega = 1;    
    double X = xyz[0];
    double Y = xyz[1];
    //    double Z = xyz[2];

    // // Deformational flow
    // auto pt = xyz2pt(X,Y,Z);
    // double theta = pt[1];
    // double r0 = 1;
    // double rho = r0*cos(theta);
    // double vt;
    
    // if(rho == 0)
    //   vt = 0;
    // else
    //   vt = 1.5*sqrt(3)*1./(cosh(rho)*cosh(rho))*tanh(rho);

    // omega = vt/rho;

    // rotation around z
    double phi = xyz2pt(xyz[0], xyz[1], xyz[2])[0];
    double theta = xyz2pt(xyz[0], xyz[1], xyz[2])[1];
    // v[0] = -omega*Y*sin(phi+M_PI/5.)*sin(theta+M_PI/7.);
    // v[1] = omega*X*sin(phi+M_PI/5.)*sin(theta+M_PI/7.);
    // v[0] = -omega*Y;
    // v[1] = omega*X;
    // v[0] = -omega*Y*sin(theta);
    // v[1] = omega*X*sin(theta);
    // v[2] = 0;

    double vTheta = sin(3*theta);
    //    double vTheta = 0;
    double vPhi = 0;

    d_para_spherical(v, vTheta, vPhi, theta, phi);
		    
    // v[0] = 0;
    // v[1] = 0;
    // v[2] = 0;

    // rotation around x
    // v[0] = 0;
    // v[1] = -omega*Z;
    // v[2] = omega*Y;

    // rotation around y
    // v[0] = omega*Z;
    // v[1] = 0;
    // v[2] = -omega*X;
  }

  Vector initialCondition(int a, int b, int ia, int ib, int face, double shiftAngle = 0) {
    auto alphaFunc = [a, this](double x) { return -M_PI/4. + a*da_+ 0.5*da_ + x*0.5*da_;};
    auto betaFunc = [b, this](double y) { return -M_PI/4. + b*db_+ 0.5*db_ + y*0.5*db_;};

    BK::Vector<double> xi = points[order-1];
    
    double alpha = alphaFunc(xi[ia]);
    double beta = betaFunc(xi[ib]);
    
    BK::Array<double,3> xyz = ab2xyz[face](alpha, beta);
    BK::Array<double,2> pt = xyz2pt(xyz[0], xyz[1], xyz[2]);

    // BK::Array<double,3> v;
    // Mat3x3 dv;
    // setVelocity(xyz, v, dv);

    // double ua, ub;
    // d_map[face](v, ua, ub, xyz);

    // // saving mapped velocities in arrays
    // uaVec[face](a,b,ia,ib) = ua;
    // ubVec[face](a,b,ia,ib) = ub; 

    double ua = uaVec[face](a,b,ia,ib);
    double ub = ubVec[face](a,b,ia,ib);
    
    // double dua, dub;
    // d_map[face](dv, dua, dub, xyz);

    // // saving mapped derivatives of velocities in arrays
    // duaVec[face](a,b,ia,ib) = dua;
    // dubVec[face](a,b,ia,ib) = dub; 


    //double value = cos(pt[0])*sin(pt[1]);
    double value = 1;

    double sqrtG = cs_utils.sqrtG(a, b, ia, ib);
    
    return {sqrtG*value, sqrtG*value*ua, sqrtG*value*ub};
    //    return {sqrtG*value, value*ua, value*ub};
  }

  // Vector initialCondition(double phi, double theta) {
  //   // Blob --------------------------------------------------
  //   double h0 = 1;
  //   double deltaC = 0.4;
    
  //   double phi0 = 0;//M_PI/2;
  //   double theta0 = M_PI/2;
    
  //   double deltaPhi = fabs(phi0 - phi);
  //   double deltaTheta = fabs(theta0 - theta);
  //   double centralAngle = sqrt(deltaPhi*deltaPhi + deltaTheta*deltaTheta);

  //   double value = 1+h0/2*(1+cos(M_PI*centralAngle/deltaC));
  //   if(centralAngle > deltaC)
  //     value = 1;

  //   value = 2+cos(phi)*sin(theta);

  //   return Vector{value, 0, 0};
  //   //    return Vector{1+0.1*exp(-(BK::sqr(x-lx_/2)+BK::sqr(y-ly_/2))/BK::sqr(0.5)), 0, 0};
  // }

  std::string toWriteCSV_info()
  {
    return "x,y,z,p,t,a,b,face,rho,ua,ub,uPhi,uTheta,vx,vy,vz,sqrtG,gamma111,gamma112,gamma122,gamma211,gamma212,gamma222";
  }

  // std::string toWritePhiThetaCSV_info()
  // {
  //   return "p,t,z,rho,ua,ub,uPhi,uTheta,vx,vy,vz,sqrtG,gamma111,gamma112,gamma122,gamma211,gamma212,gamma222";
  // }

  BK::Vector<std::string> toWriteNetCDF_info()
  {
    return {"rho","ua","ub","uPhi","uTheta","vx","vy","vz","sqrtG","gamma111","gamma112","gamma122","gamma211","gamma212","gamma222"};
  }

  BK::Vector<double> toWriteCSV(Array const& array, BK::Array<double,2> ab, int face)
  {
    double alpha = ab[0];
    size_t cellA = cs_utils.a2cell(alpha);
    
    double beta = ab[1];
    size_t cellB = cs_utils.b2cell(beta);

    if(cellA == ncelx_) cellA = ncelx_-1;
    if(cellB == ncely_) cellB = ncely_-1;

    double x = cs_utils.alpha2LagrangeInterval(alpha);
    double y = cs_utils.alpha2LagrangeInterval(beta);

    auto data = value(cellA, cellB, x, y, array);

    double sqrtG = value(cellA, cellB, x, y, cs_utils.sqrtG);

    double rho = data[0]/sqrtG;
    double ua = data[1]/data[0];
    double ub = data[2]/data[0];

    BK::Array<double,3> xyz = ab2xyz[face](alpha, beta);
    BK::Array<double,2> pt = xyz2pt(xyz[0], xyz[1], xyz[2]);

    BK::Array<double,3> v;
    d_parametrisation[face](v, ua, ub, alpha, beta);

    double theta = pt[1];
    double phi = pt[0];

    double uTheta = 0;
    double uPhi = 0;
    d_map_spherical(v, uTheta, uPhi, theta, phi);
    uPhi *= sin(theta);
    
    // // if(fabs(fabs(cos(theta))-1) > 1e-1) {
    // //   uTheta = -v[2]/sqrt(1-cos(theta)*cos(theta));
    // //   uPhi = -1./sin(theta)/cos(phi)*tan(phi)/(1+tan(phi)*tan(phi))*v[0] + 1./sin(theta)/cos(phi)/(1+tan(phi)*tan(phi))*v[1];
    // // }
    
    double gamma111 = value(cellA, cellB, x, y, cs_utils.gamma111);
    double gamma112 = value(cellA, cellB, x, y, cs_utils.gamma112);
    double gamma122 = value(cellA, cellB, x, y, cs_utils.gamma122);
    double gamma211 = value(cellA, cellB, x, y, cs_utils.gamma211);
    double gamma212 = value(cellA, cellB, x, y, cs_utils.gamma212);
    double gamma222 = value(cellA, cellB, x, y, cs_utils.gamma222);
  
    BK::Vector<double> toWriteVec({rho, ua, ub, uPhi, uTheta, v[0], v[1], v[2], sqrtG, gamma111, gamma112, gamma122, gamma211,gamma212, gamma222});

    return toWriteVec;
  }

  BK::Vector<double> toWriteCSV(Array const& array, int a, int b, int ia, int ib, int face)
  {
    auto data = array(a, b, ia, ib);
    double sqrtG = cs_utils.sqrtG(a, b, ia, ib);
    
    double rho = data[0]/sqrtG;
    double ua = data[1]/data[0];
    double ub = data[2]/data[0];

    double alpha = cs_utils.get_alpha(a, ia);
    double beta = cs_utils.get_beta(b, ib);

    BK::Array<double,3> xyz = ab2xyz[face](alpha, beta);
    BK::Array<double,2> pt = xyz2pt(xyz[0], xyz[1], xyz[2]);

    BK::Array<double,3> v;
    d_parametrisation[face](v, ua, ub, alpha, beta);

    double theta = pt[1];
    double phi = pt[0];
    double u_theta;
    double u_phi;
    d_map_spherical(v, u_theta, u_phi, theta, phi);
    
    double gamma111 = cs_utils.gamma111(a,b,ia,ib);
    double gamma112 = cs_utils.gamma112(a,b,ia,ib);
    double gamma122 = cs_utils.gamma122(a,b,ia,ib);
    double gamma211 = cs_utils.gamma211(a,b,ia,ib);
    double gamma212 = cs_utils.gamma212(a,b,ia,ib);
    double gamma222 = cs_utils.gamma222(a,b,ia,ib);
  
    BK::Vector<double> toWriteVec({rho,ua,ub, u_phi, u_theta, v[0],v[1],v[2], sqrtG, gamma111, gamma112, gamma122, gamma211,gamma212, gamma222});
    
    return toWriteVec;
  }

  BK::Array<double,3> computeT(Vector data)
  {
    double q = data[0];
    double ma = data[1];
    double mb = data[2];
    
    double T11 = ma*ma/q;
    double T12 = ma*mb/q;
    double T22 = mb*mb/q;

    return {T11, T12, T22};
  }

  VecVec flux_vector(Vector data) {

    BK::Array<double,3> T = computeT(data);
    // BK::Array<double,3> T = {0,0,0};
    // f0x = sqrtG*h*Ux; f1x = sqrtG*h*ux*ux; f2x = sqrtG*h*ux*uy
    // f0y = sqrtG*h*Uy; f1y = sqrtG*h*uy*ux; f2y = sqrtG*h*uy*uy
    double ma = data[Var::sqrtG_h_ua];
    double mb = data[Var::sqrtG_h_ub];
    
    auto flux = VecVec{ Vector{ma, T[0], T[1]},
			Vector{mb, T[1], T[2]} };

    return flux;
  }

  BK::Array<double,2> dFlux_value(Vector data) {

    double sqrtG_h = data[0];
    double sqrtG_hUx = data[1];
    double sqrtG_hUy = data[2];
    
    double ux = sqrtG_hUx/sqrtG_h;
    double uy = sqrtG_hUy/sqrtG_h;

    auto return_value = BK::Array<double,2>({fabs(ux), fabs(uy)});
    
    return return_value;
  }

  void computeSourceTerm(Array const& c, Array & s, int face) {
    for(int a=0; a<int(c.size(0)); a++)
      for(int b=0; b<int(c.size(1)); b++)
	for(int ia=0; ia<int(c.size(2)); ia++)
	  for(int ib=0; ib<int(c.size(3)); ib++) {
	    BK::Array<double,3> T = computeT(c(a,b,ia,ib));
	    
	    s(a,b,ia,ib)[0] = 0;
	    s(a,b,ia,ib)[1] = (-cs_utils.gamma111(a,b,ia,ib)*T[0]
			       - 2*cs_utils.gamma112(a,b,ia,ib)*T[1]
			       - cs_utils.gamma122(a,b,ia,ib)*T[2]);
	    
	    s(a,b,ia,ib)[2] = (-cs_utils.gamma211(a,b,ia,ib)*T[0]
			       - 2*cs_utils.gamma212(a,b,ia,ib)*T[1]
			       - cs_utils.gamma222(a,b,ia,ib)*T[2]);
	    
	  }
  }

  void computeFluxes(FluxArray& fluxArray,
		     FluxInfoBordArray& fluxInfoBordArrayX,
		     FluxInfoBordArray& fluxInfoBordArrayY,
		     Array const& c, int face) {
    
    for(int cellX=0; cellX<int(c.size(0)); cellX++)
      for(int cellY=0; cellY<int(c.size(1)); cellY++)
	for(int indexX=0; indexX<int(c.size(2)); indexX++)
	  for(int indexY=0; indexY<int(c.size(3)); indexY++) {

	    Vector data = c(cellX, cellY, indexX, indexY);
	    
	    VecVec flux = flux_vector(data);
	    
	    fluxArray(cellX, cellY, indexX, indexY) = flux;
	    if(indexX == 0) 
	      fillFluxInfoBordArray(cellX, cellY, indexY, Val::plus, c, fluxInfoBordArrayX, cellX, cellY, indexX, indexY);
	    
	    if(indexX == order-1) 
	      fillFluxInfoBordArray(cellX+1, cellY, indexY, Val::minus, c, fluxInfoBordArrayX, cellX, cellY, indexX, indexY);
	    
	    if(indexY == 0) 
	      fillFluxInfoBordArray(cellX, cellY, indexX, Val::plus, c, fluxInfoBordArrayY, cellX, cellY, indexX, indexY);

	    if(indexY == order-1) 
	      fillFluxInfoBordArray(cellX, cellY+1, indexX, Val::minus, c, fluxInfoBordArrayY, cellX, cellY, indexX, indexY);
	    
	  }
  }

  void computeNumericalFlux(FluxVecArray& fluxVecArrayX, FluxVecArray& fluxVecArrayY,
			    FluxInfoBordArray& fluxInfoArrayX, FluxInfoBordArray& fluxInfoArrayY, int face) {
    
    for(size_t posX=0; posX<fluxVecArrayX.size(0); posX++)
      for(size_t posY=0; posY<fluxVecArrayX.size(1); posY++)
	for(size_t orderY=0; orderY<order; orderY++) {

	  Vector valM = fluxInfoArrayX(posX, posY, orderY)[Val::minus];
	  VecVec fm = flux_vector(valM);

	  Vector valP = fluxInfoArrayX(posX, posY, orderY)[Val::plus];
	  VecVec fp = flux_vector(valP);
				  
	  double dfm = dFlux_value(valM)[Dir::x];
	  double dfp = dFlux_value(valP)[Dir::x];

	  fluxVecArrayX(posX, posY, orderY) =  laxFriedrichs1d_flux_numeric(fm[Dir::x], fp[Dir::x], dfm, dfp, valM, valP);
	}

    for(size_t posX=0; posX<fluxVecArrayY.size(0); posX++)
      for(size_t posY=0; posY<fluxVecArrayY.size(1); posY++)
	for(size_t orderX=0; orderX<order; orderX++) {

	  Vector valM = fluxInfoArrayY(posX, posY, orderX)[Val::minus];
	  VecVec fm = flux_vector(valM);
	  
	  Vector valP = fluxInfoArrayY(posX, posY, orderX)[Val::plus];
	  VecVec fp = flux_vector(valP);
	    
	  double dfm = dFlux_value(valM)[Dir::y];
	  double dfp = dFlux_value(valP)[Dir::y];

	  fluxVecArrayY(posX, posY, orderX) =  laxFriedrichs1d_flux_numeric(fm[Dir::y], fp[Dir::y], dfm, dfp, valM, valP);
	}
  }

  CONSTPARA(double, da);
  CONSTPARA(double, db);

  BK::Vector<BK::MultiArray<spaceDim*2, double>> uaVec;
  BK::Vector<BK::MultiArray<spaceDim*2, double>> ubVec;
};

typedef Problem_BurgersDensityCubed Problem;
extern Problem& problem;

#endif
