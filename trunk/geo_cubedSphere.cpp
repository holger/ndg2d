#include "geo_cubedSphere.hpp"

Problem& problem = BK::StackSingleton<Problem>::instance();

void GeoCubedSphere::boundaryCondition(BK::Vector<Rhs2d> & rhsVec)
{

  int numOfScalars = problem.fluxVarScalarIndizes.size();
  for(int cell=0; cell< int(ncel_); cell++)
    for(int index=0; index<order; index++) {

      for(int si=0; si<numOfScalars; si++) {
      	int scalarIndex = problem.fluxVarScalarIndizes[si];

	auto cpToRight = [&] (int from, int to) {
			   rhsVec[to].fluxInfoBordArrayX(0, cell, index)[Val::minus][scalarIndex] = rhsVec[from].fluxInfoBordArrayX(ncel_, cell, index)[Val::minus][scalarIndex];
			 };
	  
	cpToRight(0,1);
	cpToRight(1,2);
	cpToRight(2,3);
	cpToRight(3,0);

	auto cpToLeft = [&] (int from, int to) {
			  rhsVec[to].fluxInfoBordArrayX(ncel_, cell, index)[Val::plus][scalarIndex] = rhsVec[from].fluxInfoBordArrayX(0, cell, index)[Val::plus][scalarIndex];
			};

	cpToLeft(1,0);
	cpToLeft(0,3);
	cpToLeft(3,2);
	cpToLeft(2,1);

	rhsVec[4].fluxInfoBordArrayY(cell, 0, index)[Val::minus][scalarIndex] =
	  rhsVec[0].fluxInfoBordArrayY(cell, ncel_, index)[Val::minus][scalarIndex];
	rhsVec[0].fluxInfoBordArrayY(cell, ncel_, index)[Val::plus][scalarIndex] =
	  rhsVec[4].fluxInfoBordArrayY(cell, 0, index)[Val::plus][scalarIndex];

	rhsVec[2].fluxInfoBordArrayY(reverseCell(cell), ncel_, reverseIndex(index))[Val::plus][scalarIndex] =
	  rhsVec[4].fluxInfoBordArrayY(cell, ncel_, index)[Val::minus][scalarIndex];
	rhsVec[4].fluxInfoBordArrayY(reverseCell(cell), ncel_, reverseIndex(index))[Val::plus][scalarIndex] =
	  rhsVec[2].fluxInfoBordArrayY(cell, ncel_, index)[Val::minus][scalarIndex];

	rhsVec[2].fluxInfoBordArrayY(reverseCell(cell), 0, reverseIndex(index))[Val::minus][scalarIndex] =
	  rhsVec[5].fluxInfoBordArrayY(cell, 0, index)[Val::plus][scalarIndex];
	rhsVec[5].fluxInfoBordArrayY(reverseCell(cell), 0, reverseIndex(index))[Val::minus][scalarIndex]
	  = rhsVec[2].fluxInfoBordArrayY(cell, 0, index)[Val::plus][scalarIndex];

	rhsVec[0].fluxInfoBordArrayY(cell, 0, index)[Val::minus][scalarIndex] =
	  rhsVec[5].fluxInfoBordArrayY(cell, ncel_, index)[Val::minus][scalarIndex];
	rhsVec[5].fluxInfoBordArrayY(cell, ncel_, index)[Val::plus][scalarIndex] =
	  rhsVec[0].fluxInfoBordArrayY(cell, 0, index)[Val::plus][scalarIndex];

	rhsVec[1].fluxInfoBordArrayY(cell, ncel_, index)[Val::plus][scalarIndex] =
	  rhsVec[4].fluxInfoBordArrayX(ncel_, cell, index)[Val::minus][scalarIndex];
	rhsVec[4].fluxInfoBordArrayX(ncel_, cell, index)[Val::plus][scalarIndex] =
	  rhsVec[1].fluxInfoBordArrayY(cell, ncel_, index)[Val::minus][scalarIndex];

	rhsVec[1].fluxInfoBordArrayY(cell, 0, index)[Val::minus][scalarIndex] =
	  rhsVec[5].fluxInfoBordArrayX(ncel_, reverseCell(cell), reverseIndex(index))[Val::minus][scalarIndex];
	rhsVec[5].fluxInfoBordArrayX(ncel_, cell, index)[Val::plus][scalarIndex] =
	  rhsVec[1].fluxInfoBordArrayY(reverseCell(cell), 0, reverseIndex(index))[Val::plus][scalarIndex];

	rhsVec[4].fluxInfoBordArrayX(0, reverseCell(cell), reverseIndex(index))[Val::minus][scalarIndex] =
	  rhsVec[3].fluxInfoBordArrayY(cell, ncel_, index)[Val::minus][scalarIndex];
	rhsVec[3].fluxInfoBordArrayY(reverseCell(cell), ncel_, reverseIndex(index))[Val::plus][scalarIndex] =
	  rhsVec[4].fluxInfoBordArrayX(0, cell, index)[Val::plus][scalarIndex];

	rhsVec[5].fluxInfoBordArrayX(0, cell, index)[Val::minus][scalarIndex] =
	  rhsVec[3].fluxInfoBordArrayY(cell, 0, index)[Val::plus][scalarIndex];
	rhsVec[3].fluxInfoBordArrayY(cell, 0, index)[Val::minus][scalarIndex] =
	  rhsVec[5].fluxInfoBordArrayX(0, cell, index)[Val::plus][scalarIndex];
      }

      int numOfVectors = problem.fluxVarVecIndizes.size();
      for(int vi=0; vi<numOfVectors; vi++) {

	auto vecIndizes = problem.fluxVarVecIndizes[vi];

	//	std::cout << "vecIndizes = " << vecIndizes[0] << "\t" << vecIndizes[1] << std::endl;
	
	auto cpToRight = [&] (int from, int to) {
			   BK::Array<double, 2> vecF1;
			   double fx = rhsVec[from].fluxInfoBordArrayX(int(ncel_), cell, index)[Val::minus][vecIndizes[0]];
			   double fy = rhsVec[from].fluxInfoBordArrayX(int(ncel_), cell, index)[Val::minus][vecIndizes[1]];
			   BK::Array<double, 2> vecF2 = {fx,  fy};

			   matrixMultVec(vecF1, transMat12[cell*order + index], vecF2);

			   rhsVec[to].fluxInfoBordArrayX(0, cell, index)[Val::minus][vecIndizes[0]] = vecF1[0];
			   rhsVec[to].fluxInfoBordArrayX(0, cell, index)[Val::minus][vecIndizes[1]] = vecF1[1];
			 };

	cpToRight(0, 1);
	cpToRight(1, 2);
	cpToRight(2, 3);
	cpToRight(3, 0);

	auto cpToLeft = [&] (int from, int to) {
			  BK::Array<double, 2> vecF1;
			  double fx = rhsVec[from].fluxInfoBordArrayX(0, cell, index)[Val::plus][vecIndizes[0]];
			  double fy = rhsVec[from].fluxInfoBordArrayX(0, cell, index)[Val::plus][vecIndizes[1]];
			  BK::Array<double, 2> vecF2 = {fx,  fy};

			  matrixMultVec(vecF1, transMat21[cell*order + index], vecF2);

			  rhsVec[to].fluxInfoBordArrayX(ncel_, cell, index)[Val::plus][vecIndizes[0]] = vecF1[0];
			  rhsVec[to].fluxInfoBordArrayX(ncel_, cell, index)[Val::plus][vecIndizes[1]] = vecF1[1];
			};

	cpToLeft(1, 0);
	cpToLeft(0, 3);
	cpToLeft(3, 2);
	cpToLeft(2, 1);

	BK::Array<double, 2> vecF1;
	int from = 0;
	int to = 4;
	double fx = rhsVec[from].fluxInfoBordArrayY(cell, ncel_, index)[Val::minus][vecIndizes[0]];
	double fy = rhsVec[from].fluxInfoBordArrayY(cell, ncel_, index)[Val::minus][vecIndizes[1]];
	BK::Array<double, 2> vecF2 = {fx,  fy};
	matrixMultVec(vecF1, transMat15[cell*order + index], vecF2);
	rhsVec[to].fluxInfoBordArrayY(cell, 0, index)[Val::minus][vecIndizes[0]] = vecF1[0];
	rhsVec[to].fluxInfoBordArrayY(cell, 0, index)[Val::minus][vecIndizes[1]] = vecF1[1];

	from = 4;
	to = 0;
	fx = rhsVec[from].fluxInfoBordArrayY(cell, 0, index)[Val::plus][vecIndizes[0]];
	fy = rhsVec[from].fluxInfoBordArrayY(cell, 0, index)[Val::plus][vecIndizes[1]];
	vecF2 = {fx,  fy};
	matrixMultVec(vecF1, transMat51[cell*order + index], vecF2);
	rhsVec[to].fluxInfoBordArrayY(cell, ncel_, index)[Val::plus][vecIndizes[0]] = vecF1[0];
	rhsVec[to].fluxInfoBordArrayY(cell, ncel_, index)[Val::plus][vecIndizes[1]] = vecF1[1];

	from = 4;
	to = 2;
	fx = rhsVec[from].fluxInfoBordArrayY(cell, ncel_, index)[Val::minus][vecIndizes[0]];
	fy = rhsVec[from].fluxInfoBordArrayY(cell, ncel_, index)[Val::minus][vecIndizes[1]];
	vecF2 = {fx,  fy};
	matrixMultVec(vecF1, transMat53[cell*order + index], vecF2);
	rhsVec[to].fluxInfoBordArrayY(reverseCell(cell), ncel_, reverseIndex(index))[Val::plus][vecIndizes[0]] = vecF1[0];
	rhsVec[to].fluxInfoBordArrayY(reverseCell(cell), ncel_, reverseIndex(index))[Val::plus][vecIndizes[1]] = vecF1[1];

	from = 2;
	to = 4;
	fx = rhsVec[from].fluxInfoBordArrayY(cell, ncel_, index)[Val::minus][vecIndizes[0]];
	fy = rhsVec[from].fluxInfoBordArrayY(cell, ncel_, index)[Val::minus][vecIndizes[1]];
	vecF2 = {fx,  fy};
	matrixMultVec(vecF1, transMat35[cell*order + index], vecF2);
	rhsVec[to].fluxInfoBordArrayY(reverseCell(cell), ncel_, reverseIndex(index))[Val::plus][vecIndizes[0]] = vecF1[0];
	rhsVec[to].fluxInfoBordArrayY(reverseCell(cell), ncel_, reverseIndex(index))[Val::plus][vecIndizes[1]] = vecF1[1];

	from = 5;
	to = 2;
	fx = rhsVec[from].fluxInfoBordArrayY(cell, 0, index)[Val::plus][vecIndizes[0]];
	fy = rhsVec[from].fluxInfoBordArrayY(cell, 0, index)[Val::plus][vecIndizes[1]];
	vecF2 = {fx,  fy};
	matrixMultVec(vecF1, transMat63[cell*order + index], vecF2);
	rhsVec[to].fluxInfoBordArrayY(reverseCell(cell), 0, reverseIndex(index))[Val::minus][vecIndizes[0]] = vecF1[0];
	rhsVec[to].fluxInfoBordArrayY(reverseCell(cell), 0, reverseIndex(index))[Val::minus][vecIndizes[1]] = vecF1[1];

	from = 2;
	to = 5;
	fx = rhsVec[from].fluxInfoBordArrayY(cell, 0, index)[Val::plus][vecIndizes[0]];
	fy = rhsVec[from].fluxInfoBordArrayY(cell, 0, index)[Val::plus][vecIndizes[1]];
	vecF2 = {fx,  fy};
	matrixMultVec(vecF1, transMat36[cell*order + index], vecF2);
	rhsVec[to].fluxInfoBordArrayY(reverseCell(cell), 0, reverseIndex(index))[Val::minus][vecIndizes[0]] = vecF1[0];
	rhsVec[to].fluxInfoBordArrayY(reverseCell(cell), 0, reverseIndex(index))[Val::minus][vecIndizes[1]] = vecF1[1];

	from = 5;
	to = 0;
	fx = rhsVec[from].fluxInfoBordArrayY(cell, ncel_, index)[Val::minus][vecIndizes[0]];
	fy = rhsVec[from].fluxInfoBordArrayY(cell, ncel_, index)[Val::minus][vecIndizes[1]];
	vecF2 = {fx,  fy};
	matrixMultVec(vecF1, transMat61[cell*order + index], vecF2);
	rhsVec[to].fluxInfoBordArrayY(cell, 0, index)[Val::minus][vecIndizes[0]] = vecF1[0];
	rhsVec[to].fluxInfoBordArrayY(cell, 0, index)[Val::minus][vecIndizes[1]] = vecF1[1];

	from = 0;
	to = 5;
	fx = rhsVec[from].fluxInfoBordArrayY(cell, 0, index)[Val::plus][vecIndizes[0]];
	fy = rhsVec[from].fluxInfoBordArrayY(cell, 0, index)[Val::plus][vecIndizes[1]];
	vecF2 = {fx,  fy};
	matrixMultVec(vecF1, transMat16[cell*order + index], vecF2);
	rhsVec[to].fluxInfoBordArrayY(cell, ncel_, index)[Val::plus][vecIndizes[0]] = vecF1[0];
	rhsVec[to].fluxInfoBordArrayY(cell, ncel_, index)[Val::plus][vecIndizes[1]] = vecF1[1];

	from = 1;
	to = 4;
	fx = rhsVec[from].fluxInfoBordArrayY(cell, ncel_, index)[Val::minus][vecIndizes[0]];
	fy = rhsVec[from].fluxInfoBordArrayY(cell, ncel_, index)[Val::minus][vecIndizes[1]];
	vecF2 = {fx,  fy};
	matrixMultVec(vecF1, transMat25[cell*order + index], vecF2);
	rhsVec[to].fluxInfoBordArrayX(ncel_, cell, index)[Val::plus][vecIndizes[0]] = vecF1[0];
	rhsVec[to].fluxInfoBordArrayX(ncel_, cell, index)[Val::plus][vecIndizes[1]] = vecF1[1];

	from = 4;
	to = 1;
	fx = rhsVec[from].fluxInfoBordArrayX(ncel_, cell, index)[Val::minus][vecIndizes[0]];
	fy = rhsVec[from].fluxInfoBordArrayX(ncel_, cell, index)[Val::minus][vecIndizes[1]];
	vecF2 = {fx,  fy};
	matrixMultVec(vecF1, transMat52[cell*order + index], vecF2);
	rhsVec[to].fluxInfoBordArrayY(cell, ncel_, index)[Val::plus][vecIndizes[0]] = vecF1[0];
	rhsVec[to].fluxInfoBordArrayY(cell, ncel_, index)[Val::plus][vecIndizes[1]] = vecF1[1];

	from = 5;
	to = 1;
	fx = rhsVec[from].fluxInfoBordArrayX(ncel_, cell, index)[Val::minus][vecIndizes[0]];
	fy = rhsVec[from].fluxInfoBordArrayX(ncel_, cell, index)[Val::minus][vecIndizes[1]];
	vecF2 = {fx,  fy};
	matrixMultVec(vecF1, transMat62[cell*order + index], vecF2);
	rhsVec[to].fluxInfoBordArrayY(reverseCell(cell), 0, reverseIndex(index))[Val::minus][vecIndizes[0]] = vecF1[0];
	rhsVec[to].fluxInfoBordArrayY(reverseCell(cell), 0, reverseIndex(index))[Val::minus][vecIndizes[1]] = vecF1[1];

	from = 1;
	to = 5;
	fx = rhsVec[from].fluxInfoBordArrayY(cell, 0, index)[Val::plus][vecIndizes[0]];
	fy = rhsVec[from].fluxInfoBordArrayY(cell, 0, index)[Val::plus][vecIndizes[1]];
	vecF2 = {fx,  fy};
	matrixMultVec(vecF1, transMat26[cell*order + index], vecF2);
	rhsVec[to].fluxInfoBordArrayX(ncel_, reverseCell(cell), reverseIndex(index))[Val::plus][vecIndizes[0]] = vecF1[0];
	rhsVec[to].fluxInfoBordArrayX(ncel_, reverseCell(cell), reverseIndex(index))[Val::plus][vecIndizes[1]] = vecF1[1];
	
	from = 3;
	to = 4;
	fx = rhsVec[from].fluxInfoBordArrayY(cell, ncel_, index)[Val::minus][vecIndizes[0]];
	fy = rhsVec[from].fluxInfoBordArrayY(cell, ncel_, index)[Val::minus][vecIndizes[1]];
	vecF2 = {fx,  fy};
	matrixMultVec(vecF1, transMat45[cell*order + index], vecF2);
	rhsVec[to].fluxInfoBordArrayX(0, reverseCell(cell), reverseIndex(index))[Val::minus][vecIndizes[0]] = vecF1[0];
	rhsVec[to].fluxInfoBordArrayX(0, reverseCell(cell), reverseIndex(index))[Val::minus][vecIndizes[1]] = vecF1[1];

	from = 4;
	to = 3;
	fx = rhsVec[from].fluxInfoBordArrayX(0, cell, index)[Val::plus][vecIndizes[0]];
	fy = rhsVec[from].fluxInfoBordArrayX(0, cell, index)[Val::plus][vecIndizes[1]];
	vecF2 = {fx,  fy};
	matrixMultVec(vecF1, transMat54[cell*order + index], vecF2);
	rhsVec[to].fluxInfoBordArrayY(reverseCell(cell), ncel_, reverseIndex(index))[Val::plus][vecIndizes[0]] = vecF1[0];
	rhsVec[to].fluxInfoBordArrayY(reverseCell(cell), ncel_, reverseIndex(index))[Val::plus][vecIndizes[1]] = vecF1[1];

	from = 3;
	to = 5;
	fx = rhsVec[from].fluxInfoBordArrayY(cell, 0, index)[Val::plus][vecIndizes[0]];
	fy = rhsVec[from].fluxInfoBordArrayY(cell, 0, index)[Val::plus][vecIndizes[1]];
	vecF2 = {fx,  fy};
	matrixMultVec(vecF1, transMat46[cell*order + index], vecF2);
	rhsVec[to].fluxInfoBordArrayX(0, cell, index)[Val::minus][vecIndizes[0]] = vecF1[0];
	rhsVec[to].fluxInfoBordArrayX(0, cell, index)[Val::minus][vecIndizes[1]] = vecF1[1];

	from = 5;
	to = 3;
	fx = rhsVec[from].fluxInfoBordArrayX(0, cell, index)[Val::plus][vecIndizes[0]];
	fy = rhsVec[from].fluxInfoBordArrayX(0, cell, index)[Val::plus][vecIndizes[1]];
	vecF2 = {fx,  fy};
	matrixMultVec(vecF1, transMat64[cell*order + index], vecF2);
	rhsVec[to].fluxInfoBordArrayY(cell, 0, index)[Val::minus][vecIndizes[0]] = vecF1[0];
	rhsVec[to].fluxInfoBordArrayY(cell, 0, index)[Val::minus][vecIndizes[1]] = vecF1[1];
      }
    }
}
