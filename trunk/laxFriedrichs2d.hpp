#ifndef laxFriedrichs2d_hpp
#define laxFriedrichs2d_hpp

#include "problem2d.hpp"

// lookup table versions:

// 2D Lax-Friedrichs ------------------------------------------------------------------------------------------------------

template<class Array>
typename Array::value_type flux_numeric_XR(int cellX, int cellY, int indexY, Array const& c)
{
  typedef typename Array::value_type Vector;
  
  Vector frX = 0.5*(problem.flux_vectorR(cellX, cellY, indexY, c)[0] + problem.flux_vectorL(cellX+1, cellY, indexY, c)[0]);

  double crX = std::max(fabs(problem.dFlux_value(cellX+1, cellY, 0, indexY, c)[0]),
			fabs(problem.dFlux_value(cellX, cellY, order-1, indexY, c)[0]));
  
  return frX - 0.5*crX*(valueL(cellX+1, cellY, indexY, c) - valueR(cellX, cellY, indexY, c));
}

template<class Array>
typename Array::value_type flux_numeric_XL(int cellX, int cellY, int indexY, Array const& c)
{
  typedef typename Array::value_type Vector;
  
  Vector flX = 0.5*(problem.flux_vectorR(cellX-1, cellY, indexY, c)[0] + problem.flux_vectorL(cellX, cellY, indexY, c)[0]);

  double clX = std::max(fabs(problem.dFlux_value(cellX-1, cellY, order-1, indexY, c)[0]),
			fabs(problem.dFlux_value(cellX, cellY, 0, indexY, c)[0]));
  
  return flX + 0.5*clX*(valueR(cellX-1, cellY, indexY, c) - valueL(cellX, cellY, indexY, c));
}

template<class Array>
typename Array::value_type flux_numeric_YT(int cellX, int cellY, int indexX, Array const& c)
{
  typedef typename Array::value_type Vector;
  
  Vector ftY = 0.5*(problem.flux_vectorT(cellX, cellY, indexX, c)[1] + problem.flux_vectorB(cellX, cellY+1, indexX, c)[1]);

  double ctY = std::max(fabs(problem.dFlux_value(cellX, cellY+1, indexX, 0, c)[1]),
			fabs(problem.dFlux_value(cellX, cellY, indexX, order-1, c)[1]));
  
  return ftY - 0.5*ctY*(valueB(cellX, cellY+1, indexX, c) - valueT(cellX, cellY, indexX, c));
}

template<class Array>
typename Array::value_type flux_numeric_YB(int cellX, int cellY, int indexX, Array const& c)
{
  typedef typename Array::value_type Vector;
  
  Vector fbY = 0.5*(problem.flux_vectorT(cellX, cellY-1, indexX, c)[1] + problem.flux_vectorB(cellX, cellY, indexX, c)[1]);

  double cbY = std::max(fabs(problem.dFlux_value(cellX, cellY-1, indexX, order-1, c)[1]),
			fabs(problem.dFlux_value(cellX, cellY, indexX, 0, c)[1]));
  
  return fbY + 0.5*cbY*(valueT(cellX, cellY-1, indexX, c) - valueB(cellX, cellY, indexX, c));
}

#endif
