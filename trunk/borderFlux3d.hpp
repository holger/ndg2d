#ifndef borderFlux3d_hpp
#define borderFlux3d_hpp

#include "order.hpp"
#include "laxFriedrichs3d.hpp"
#include "problem3d.hpp"

// 3D Flux -----------------------------------------------------------------------------------------------------

template<class Array>

auto borderFluxX(int cellX, int cellY, int cellZ, int orderX, int orderY, int orderZ, Array const& c)
{
  static BK::Vector<double> wi = weights[order-1];

  typedef typename Array::value_type Vector;
  
  if(orderX == order-1) 
    return Vector{flux_numeric_XR(cellX, cellY,cellZ, orderY, orderZ, c)*problem.get_dy()*problem.get_dz()/4.*wi[orderY]*wi[orderZ]};

  else if(orderX == 0)
    return Vector{-flux_numeric_XL(cellX, cellY,cellZ, orderY,orderZ, c)*problem.get_dy()*problem.get_dz()/4.*wi[orderY]*wi[orderZ]};
  
  else return Vector(0);
}

template<class Array>
auto borderFluxY(int cellX, int cellY,int cellZ, int orderX, int orderY,int orderZ,  Array const& c)
{
  static BK::Vector<double> wi = weights[order-1];

  typedef typename Array::value_type Vector;
    
  if(orderY == order-1) {
    
    return Vector{flux_numeric_YT(cellX, cellY,cellZ, orderX,orderZ, c)*problem.get_dx()*problem.get_dz()/4.*wi[orderX]*wi[orderZ]};
  }

  else if(orderY == 0)
    return Vector{-flux_numeric_YB(cellX, cellY,cellZ, orderX,orderZ, c)*problem.get_dx()*problem.get_dz()/4.*wi[orderX]*wi[orderZ]};
  
  else return Vector(0);
}

template<class Array>
auto borderFluxZ(int cellX, int cellY,int cellZ, int orderX, int orderY, int orderZ, Array const& c)
{
  static BK::Vector<double> wi = weights[order-1];

  typedef typename Array::value_type Vector;
  
  if(orderZ == order-1)
    return Vector{flux_numeric_ZF(cellX, cellY,cellZ, orderX,orderY, c)*problem.get_dx()*problem.get_dy()/4.*wi[orderX]*wi[orderY]};

  else if(orderZ == 0)
    return Vector{-flux_numeric_ZB(cellX, cellY,cellZ, orderX,orderY, c)*problem.get_dx()*problem.get_dy()/4.*wi[orderX]*wi[orderY]};
  
  else return Vector(0);
}

#endif
