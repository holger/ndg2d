#ifndef boundaryConditionPatched_hpp
#define boundaryConditionPatched_hpp

class BoundaryCondition2dPatched
{
public:
  template<class Array>
  void operator()(std::vector<Array>& cVec)
  {
    assert(cVec.size() == 2);
    
    Array& c0 = cVec[0];
    
    int ncelX = c0.size(0);
    int ncelY = c0.size(1);
    int orderX = c0.size(2);
    int orderY = c0.size(3);

    for(int ox=0; ox<orderX; ox++)
      for(int oy=0; oy<orderY; oy++){
	for(int iy=0; iy<ncelY; iy++) {
	  cVec[0](0, iy, ox, oy) = cVec[1](ncelX-2, iy, ox, oy);
	  cVec[0](ncelX-1, iy, ox, oy) = cVec[1](1, iy, ox, oy);
	  cVec[1](0, iy, ox, oy) = cVec[0](ncelX-2, iy, ox, oy);
	  cVec[1](ncelX-1, iy, ox, oy) = cVec[0](1, iy, ox, oy);
	}
	for(int ix=0; ix<ncelX; ix++) {
    	  cVec[0](ix, 0, ox, oy) = cVec[0](ix, ncelY-2, ox, oy);
    	  cVec[0](ix, ncelY-1, ox, oy) = cVec[0](ix, 1, ox, oy);
    	  cVec[1](ix, 0, ox, oy) = cVec[1](ix, ncelY-2, ox, oy);
    	  cVec[1](ix, ncelY-1, ox, oy) = cVec[1](ix, 1, ox, oy);
     	}
      }
  }
};

#endif
