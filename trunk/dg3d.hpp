#ifndef dg3d_hpp
#define dg3d_hpp

#include <mpi.h>

#include "order.hpp"
#include "problem3d.hpp"
#include "mpiBase.hpp"
#include "mpiFlux3d.hpp"

extern XFluxMPI3d& xFluxMPI;
extern YFluxMPI3d& yFluxMPI;
extern ZFluxMPI3d& zFluxMPI;

// 3D version -----------------------------------------------------------------------

class Rhs3d
{
public:

  Rhs3d(size_t ncelx, size_t ncely, size_t ncelz, double dx, double dy, double dz) : coeffX{order, order, order, order}, 
    coeffY{order, order, order, order},
    coeffZ{order, order, order, order} {
      
      dx_ = dx;
      dy_ = dy;
      dz_ = dz;

      ncelX_ = ncelx;
      ncelY_ = ncely;
      ncelZ_ = ncelz;
      
      wi = weights[order-1];
      DLagrangeMatrix<order> dLagrangeMatrix;
      dLagrangeMatrix.init();
      
      for(int orderX=0; orderX<order; orderX++)
	for(int orderY=0; orderY<order; orderY++)
	  for(int orderZ=0; orderZ<order; orderZ++) 
	  for(int i=0; i<order; i++) {
	    coeffX(orderX, orderY, orderZ, i) = dy_*dz_/4.*wi[i]*wi[orderY]*wi[orderZ]*dLagrangeMatrix(orderX,i);
	    coeffY(orderX, orderY, orderZ, i) = dx_*dz_/4.*wi[orderX]*wi[i]*wi[orderZ]*dLagrangeMatrix(orderY,i);
	    coeffZ(orderX, orderY, orderZ, i) = dx_*dy_/4.*wi[orderX]*wi[orderY]*wi[i]*dLagrangeMatrix(orderZ,i);
	    
	  }
      
      fluxArray.resize(ncelX_, ncelY_, ncelZ_, order, order, order);
      fluxInfoBordArrayX.resize(ncelX_+1, ncelY_, ncelZ_, order, order);
      fluxInfoBordArrayY.resize(ncelX_, ncelY_+1, ncelZ_, order, order);
      fluxInfoBordArrayZ.resize(ncelX_, ncelY_, ncelZ_+1, order, order);
      numFluxX.resize(ncelX_+1, ncelY_, ncelZ_, order, order);
      numFluxY.resize(ncelX_, ncelY_+1, ncelZ_, order, order);
      numFluxZ.resize(ncelX_, ncelY_, ncelZ_+1, order, order);
    }  
  
  template<class Array>
  void operator()(Array & rhs);

  Problem::FluxArray fluxArray;
  Problem::FluxInfoBordArray fluxInfoBordArrayX;
  Problem::FluxInfoBordArray fluxInfoBordArrayY;
  Problem::FluxInfoBordArray fluxInfoBordArrayZ;
  Problem::FluxVecArray numFluxX;
  Problem::FluxVecArray numFluxY;
  Problem::FluxVecArray numFluxZ;

private:
  
  BK::MultiArray<4, double> coeffX;
  BK::MultiArray<4, double> coeffY;
  BK::MultiArray<4, double> coeffZ;

  CONSTPARA(double, dx);
  CONSTPARA(double, dy);
  CONSTPARA(double, dz);
  CONSTPARA(size_t, ncelX);
  CONSTPARA(size_t, ncelY);
  CONSTPARA(size_t, ncelZ);
  
  BK::Vector<double> wi;
};


template<class Array>
void Rhs3d::operator()(Array & rhs) {
  for(int cellX=0; cellX<int(rhs.size(0)); cellX++)
    for(int cellY=0; cellY<int(rhs.size(1)); cellY++)
      for(int cellZ=0; cellZ<int(rhs.size(2)); cellZ++)
  	for(int orderX=0; orderX<int(rhs.size(3)); orderX++)
  	  for(int orderY=0; orderY<int(rhs.size(4)); orderY++)
  	    for(int orderZ=0; orderZ<int(rhs.size(5)); orderZ++) {

	      rhs(cellX, cellY, cellZ, orderX, orderY, orderZ) = 0;
	      
	      for(int i=0;i<order;i++) 
		rhs(cellX, cellY, cellZ, orderX, orderY, orderZ) += fluxArray(cellX, cellY,cellZ, i, orderY, orderZ)[0]*coeffX(orderX, orderY, orderZ, i)
		  + fluxArray(cellX, cellY,cellZ, orderX, i, orderZ)[1]*coeffY(orderX, orderY, orderZ, i)
		  + fluxArray(cellX, cellY,cellZ, orderX, orderY, i)[2]*coeffZ(orderX, orderY, orderZ, i);
	    }

  mpiBase.waitAll();
  
  xFluxMPI.finish(fluxInfoBordArrayX);
  yFluxMPI.finish(fluxInfoBordArrayY);
  zFluxMPI.finish(fluxInfoBordArrayZ);

  problem.computeNumericalFlux(numFluxX, numFluxY, numFluxZ, fluxInfoBordArrayX, fluxInfoBordArrayY, fluxInfoBordArrayZ);
  
  for(int cellX=0; cellX<int(rhs.size(0))-1; cellX++)
    for(int cellY=0; cellY<int(rhs.size(1)); cellY++)
      for(int cellZ=0; cellZ<int(rhs.size(2)); cellZ++)
  	for(int orderY=0; orderY<int(rhs.size(4)); orderY++)
  	  for(int orderZ=0; orderZ<int(rhs.size(5)); orderZ++) {
	    //	    Problem::Vector fluxXR = flux_numeric_XR(cellX, cellY,cellZ, orderY, orderZ, c)*problem.get_dy()*problem.get_dz()/4.*wi[orderY]*wi[orderZ];
	    Problem::Vector fluxXR = numFluxX(cellX+1, cellY,cellZ, orderY, orderZ)*problem.get_dy()*problem.get_dz()/4.*wi[orderY]*wi[orderZ];
	    rhs(cellX, cellY, cellZ, order-1, orderY, orderZ) -= fluxXR;
	    rhs(cellX+1, cellY, cellZ, 0, orderY, orderZ) += fluxXR;
	  
  	    // rhs(cellX, cellY, cellZ, order-1, orderY, orderZ) -= flux_numeric_XR(cellX, cellY,cellZ, orderY, orderZ, c)
  	    //   *problem.get_dy()*problem.get_dz()/4.*wi[orderY]*wi[orderZ];
  	    // rhs(cellX, cellY, cellZ, 0, orderY, orderZ) += flux_numeric_XL(cellX, cellY,cellZ, orderY,orderZ, c)*problem.get_dy()*problem.get_dz()/4.*wi[orderY]*wi[orderZ];
  	  }

  for(int cellY=0; cellY<int(rhs.size(1)); cellY++)
    for(int cellZ=0; cellZ<int(rhs.size(2)); cellZ++)
      for(int orderY=0; orderY<int(rhs.size(4)); orderY++)
	for(int orderZ=0; orderZ<int(rhs.size(5)); orderZ++) {
	  rhs(0, cellY, cellZ, 0, orderY, orderZ) += numFluxX(0, cellY, cellZ, orderY, orderZ)*problem.get_dy()*problem.get_dz()/4.*wi[orderY]*wi[orderZ];
	  rhs(int(ncelX_)-1, cellY, cellZ, order-1, orderY, orderZ) -= numFluxX(int(ncelX_), cellY, cellZ, orderY, orderZ)*problem.get_dy()*problem.get_dz()/4.*wi[orderY]*wi[orderZ];
	}

  for(int cellX=0; cellX<int(rhs.size(0)); cellX++)
    for(int cellY=0; cellY<int(rhs.size(1)-1); cellY++)
      for(int cellZ=0; cellZ<int(rhs.size(2)); cellZ++)
  	for(int orderX=0; orderX<int(rhs.size(3)); orderX++)
  	  for(int orderZ=0; orderZ<int(rhs.size(5)); orderZ++) {
	    Problem::Vector fluxYT = numFluxY(cellX, cellY+1, cellZ, orderX, orderZ)*problem.get_dx()*problem.get_dz()/4.*wi[orderX]*wi[orderZ];
	    rhs(cellX, cellY, cellZ, orderX, order-1, orderZ) -= fluxYT;
	    rhs(cellX, cellY+1, cellZ, orderX, 0, orderZ) += fluxYT;
  	    // rhs(cellX, cellY, cellZ, orderX, order-1, orderZ) -= flux_numeric_YT(cellX, cellY,cellZ, orderX,orderZ, c)
  	    //   *problem.get_dx()*problem.get_dz()/4.*wi[orderX]*wi[orderZ];
  	    // rhs(cellX, cellY, cellZ, orderX, 0, orderZ) += flux_numeric_YB(cellX, cellY,cellZ, orderX,orderZ, c)*problem.get_dx()*problem.get_dz()/4.*wi[orderX]*wi[orderZ];
  	  }

  for(int cellX=0; cellX<int(rhs.size(0)); cellX++)
    for(int cellZ=0; cellZ<int(rhs.size(2)); cellZ++)
      for(int orderX=0; orderX<int(rhs.size(3)); orderX++)
	for(int orderZ=0; orderZ<int(rhs.size(5)); orderZ++) {
	  rhs(cellX, 0, cellZ, orderX, 0, orderZ) += numFluxY(cellX, 0, cellZ, orderX, orderZ)*problem.get_dx()*problem.get_dz()/4.*wi[orderX]*wi[orderZ];
	  rhs(cellX, int(ncelY_)-1, cellZ, orderX, order-1, orderZ) -= numFluxY(cellX, int(ncelY_), cellZ, orderX, orderZ)*problem.get_dx()*problem.get_dz()/4.*wi[orderX]*wi[orderZ];
	}

  for(int cellX=0; cellX<int(rhs.size(0)); cellX++)
    for(int cellY=0; cellY<int(rhs.size(1)); cellY++)
      for(int cellZ=0; cellZ<int(rhs.size(2)-1); cellZ++)
  	for(int orderX=0; orderX<int(rhs.size(3)); orderX++)
  	  for(int orderY=0; orderY<int(rhs.size(4)); orderY++) {
	    Problem::Vector fluxZF = numFluxZ(cellX, cellY, cellZ+1, orderX, orderY)*problem.get_dx()*problem.get_dy()/4.*wi[orderX]*wi[orderY];
	    rhs(cellX, cellY, cellZ, orderX, orderY, order-1) -= fluxZF;
	    rhs(cellX, cellY, cellZ+1, orderX, orderY, 0) += fluxZF;
	      
  	    // rhs(cellX, cellY, cellZ, orderX, orderY, order-1) -= flux_numeric_ZF(cellX, cellY,cellZ, orderX,orderY, c)
  	    //   *problem.get_dx()*problem.get_dy()/4.*wi[orderX]*wi[orderY];
  	    // rhs(cellX, cellY, cellZ, orderX, orderY, 0) += flux_numeric_ZB(cellX, cellY,cellZ, orderX,orderY, c)*problem.get_dx()*problem.get_dy()/4.*wi[orderX]*wi[orderY];
  	  }

  for(int cellX=0; cellX<int(rhs.size(0)); cellX++)
    for(int cellY=0; cellY<int(rhs.size(1)); cellY++)
      for(int orderX=0; orderX<int(rhs.size(3)); orderX++)
	for(int orderY=0; orderY<int(rhs.size(4)); orderY++) {
	  rhs(cellX, cellY, 0, orderX, orderY, 0) += numFluxZ(cellX, cellY, 0, orderX, orderY)*problem.get_dx()*problem.get_dy()/4.*wi[orderX]*wi[orderY];
	  rhs(cellX, cellY, int(ncelZ_)-1, orderX, orderY, order-1) -= numFluxZ(cellX, cellY, int(ncelZ_), orderX, orderY)*problem.get_dx()*problem.get_dy()/4.*wi[orderX]*wi[orderY];
	}

  for(int cellX=0; cellX<int(rhs.size(0)); cellX++)
    for(int cellY=0; cellY<int(rhs.size(1)); cellY++)
      for(int cellZ=0; cellZ<int(rhs.size(2)); cellZ++)
  	for(int orderX=0; orderX<int(rhs.size(3)); orderX++)
  	  for(int orderY=0; orderY<int(rhs.size(4)); orderY++)
  	    for(int orderZ=0; orderZ<int(rhs.size(5)); orderZ++) {
  	      double norm = 8./(problem.get_dx()*problem.get_dy()*problem.get_dz()*wi[orderX]*wi[orderY]*wi[orderZ]);
  	      rhs(cellX, cellY, cellZ, orderX, orderY, orderZ) *= norm;
  	    }
  
}

#endif
