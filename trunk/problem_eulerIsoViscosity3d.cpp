#include "problem_eulerIsoViscosity3d.hpp"

#include "value3d.hpp"

void Problem_EulerIsoViscosity3d::errorComp(Array const* c) {
  // double lx = problem.get_lx();
  // double ly = problem.get_ly();
  // double lz = problem.get_lz();
  double np = 1000;
  double dxn = lx_/np;
  double dyn = ly_/np;
  double dzn = lz_/np;
  
  std::vector<Problem::Vector> solVecx(np);
  for(size_t i=0; i<solVecx.size(); i++)
    solVecx[i] = value(i*dxn, 0., 0., *c, dx_, dy_, dz_);
    
  std::vector<Problem::Vector> solVecy(np);
  for(size_t i=0; i<solVecy.size(); i++)
    solVecy[i] = value(0., i*dyn, 0., *c, dx_, dy_, dz_);
    
  std::vector<Problem::Vector> solVecz(np);
  for(size_t i=0; i<solVecz.size(); i++)
    solVecz[i] = value(0., 0., i*dzn, *c, dx_, dy_, dz_);
    
  std::ifstream solIn;
  std::string filename = "../tests/gold/eulerIsoViscosity1d/solRef_eulerIsoViscosity1d_0_1-6-6";
  solIn.open(filename + ".bin", std::ios::in | std::ios::binary);
  if (!solIn.is_open()) {
    std::cerr << "ERROR: could not open file " << filename << std::endl;
    exit(1);
  }
      
  std::vector<std::array<double,2>> solVec2(np);
  solIn.read((char*)solVec2.data(), sizeof(std::array<double,2>)*solVec2.size());
  solIn.close();

  double errorx = 0;
  for(size_t i = 0; i < np; i++){
    errorx += BK::sqr(solVecx[i][1] - solVec2[i][1])*dxn;
  }
  errorx = sqrt(errorx);

  std::string dirName = BK::toString("data/", name_);
  
  std::cerr << "writing x-error to " << BK::toString(dirName + "/error-ndg-",order,"-",rkOrder_,"-x",".txt") << std::endl;
  std::ofstream outChronox(BK::toString(dirName + "/error-ndg-",order,"-",rkOrder_,"-x",".txt"), std::ios::app);
  outChronox << ncelx_ << "\t" << errorx << std::endl;
  outChronox.close();
    
  double errory = 0;
  for(size_t i = 0; i < np; i++){
    errory += BK::sqr(solVecy[i][2] - solVec2[i][1])*dyn;
  }
  errory = sqrt(errory);

  std::cerr << "writing y-error to " << BK::toString(dirName + "/error-ndg-",order,"-",rkOrder_,"-y",".txt") << std::endl;
  std::ofstream outChronoy(BK::toString(dirName + "/error-ndg-",order,"-",rkOrder_,"-y",".txt"), std::ios::app);
  outChronoy << ncelx_ << "\t" << errory << std::endl;
  outChronoy.close();
    
  double errorz = 0;
  for(size_t i = 0; i < np; i++){
    errorz += BK::sqr(solVecz[i][3] - solVec2[i][1])*dzn;
  }
  errorz = sqrt(errorz);
    
  std::cerr << "writing z- error to " << BK::toString(dirName + "/error-ndg-",order,"-",rkOrder_,"-z",".txt") << std::endl;
  std::ofstream outChronoz(BK::toString(dirName + "/error-ndg-",order,"-",rkOrder_,"-z",".txt"), std::ios::app);
  outChronoz << ncelx_ << "\t" << errorz << std::endl;
  outChronoz.close();

  std::ofstream outTest(BK::toString(dirName + "/test.txt"));
  for(size_t i = 0; i<np; i++)
    outTest << solVecz[i][3] << "\t" << solVec2[i][1] << std::endl;
    
  outTest.close();

}


