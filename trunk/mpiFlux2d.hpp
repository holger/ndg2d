#ifndef mpiFlux2d_hpp
#define mpiFlux2d_hpp

#include <mpi.h>
#include <BasisKlassen/multiArray.hpp>
#include "mpiBase.hpp"

class XFluxMPI2d
{
public:

  XFluxMPI2d(){};

  void init(int ncelx, int ncely) {
    XplusFirstCell.resize(ncely, order);
    XplusLastCell.resize(ncely, order);
    XminusFirstCell.resize(ncely, order);
    XminusLastCell.resize(ncely, order);

    ncelx_ = ncelx;
    ncely_ = ncely;
  }

  int ncelx_;
  int ncely_;

  template<class FluxInfoVecBordArray>
  void begin(FluxInfoVecBordArray& fluxInfoArrayX );

  template<class FluxInfoVecBordArray>
  void finish(FluxInfoVecBordArray& fluxInfoArrayX ) {

    for(int iy=0; iy<ncely_; iy++) 
      for(int oy=0; oy<order; oy++){
	fluxInfoArrayX(0, iy, oy)[Val::minus] = XminusFirstCell(iy, oy) ;
	fluxInfoArrayX(ncelx_-1, iy, oy)[Val::plus] = XplusLastCell(iy, oy) ;
      }
    
    if(problem.get_bcX() == 1) {
      int sizeY = mpiBase.get_dimSize()[1];
      if(mpiBase.procCoord()[0] == 0) // correct MPI !!!
	for(int iy=0; iy<ncely_; iy++) 
	  for(int oy=0; oy<order; oy++){
	    fluxInfoArrayX(0, iy, oy)[Val::minus] = problem.bcXL(iy, oy, BK::Type2Type<Problem::FluxInfo>());
	  }
      
      if(mpiBase.procCoord()[0] == sizeY-1) 
	for(int iy=0; iy<ncely_; iy++) 
	  for(int oy=0; oy<order; oy++){
	    fluxInfoArrayX(ncelx_-1, iy, oy)[Val::plus] = problem.bcXR(iy, oy, BK::Type2Type<Problem::FluxInfo>());
	  }
    }
  }

  BK::MultiArray<2,BK::Array<double, varDim>> XplusFirstCell;
  BK::MultiArray<2,BK::Array<double, varDim>> XplusLastCell;
  BK::MultiArray<2,BK::Array<double, varDim>> XminusFirstCell;
  BK::MultiArray<2,BK::Array<double, varDim>> XminusLastCell;
  
  //BK::Vector<BK::Array<BK::Array<double, Problem::varDim>, order>> XplusFirstCell;
  //BK::Vector<BK::Array<BK::Array<double, Problem::varDim>, order>> XplusLastCell;
  //BK::Vector<BK::Array<BK::Array<double, Problem::varDim>, order>> XminusFirstCell;
  //BK::Vector<BK::Array<BK::Array<double, Problem::varDim>, order>> XminusLastCell;
};

class YFluxMPI2d
{
public:

  YFluxMPI2d() {}

  void init(int ncelx, int ncely) {
    YplusFirst.resize(ncelx, order);
    YplusLast.resize(ncelx, order);
    YminusFirst.resize(ncelx, order);
    YminusLast.resize(ncelx, order);

    ncelx_ = ncelx;
    ncely_ = ncely;
  }

  int ncelx_;
  int ncely_;

  template<class FluxInfoVecBordArray>
  void begin(FluxInfoVecBordArray& fluxInfoArrayY);

  template<class FluxInfoVecBordArray>
  void finish(FluxInfoVecBordArray& fluxInfoArrayY) {

    for(int ix=0; ix<ncelx_; ix++) 
      for(int ox=0; ox<order; ox++){
	fluxInfoArrayY(ix, ncely_-1, ox)[Val::plus] = YplusLast(ix, ox) ;
	fluxInfoArrayY(ix, 0, ox)[Val::minus] =  YminusFirst(ix, ox) ;
      }

    if(problem.get_bcY() == 1) {
      int sizeX = mpiBase.get_dimSize()[0];
      if(mpiBase.procCoord()[0] == 0) 
	for(int ix=0; ix<ncelx_; ix++) 
	  for(int ox=0; ox<order; ox++){
	    fluxInfoArrayY(ix, 0, ox)[Val::minus] = problem.bcYB(ix, ox, BK::Type2Type<Problem::FluxInfo>());
	  }
      
      if(mpiBase.procCoord()[0] == sizeX-1) 
	for(int ix=0; ix<ncelx_; ix++) 
	  for(int ox=0; ox<order; ox++){
	    fluxInfoArrayY(ix, ncely_-1, ox)[Val::plus] = problem.bcYT(ix, ox, BK::Type2Type<Problem::FluxInfo>());
	  }
    }
  }

  BK::MultiArray<2,BK::Array<double, varDim>> YplusFirst; 
  BK::MultiArray<2,BK::Array<double, varDim>> YplusLast;  
  BK::MultiArray<2,BK::Array<double, varDim>> YminusFirst;
  BK::MultiArray<2,BK::Array<double, varDim>> YminusLast; 

  // BK::Vector<BK::Array<BK::Array<double, Problem::varDim>, order>> YplusFirst;
  // BK::Vector<BK::Array<BK::Array<double, Problem::varDim>, order>> YplusLast;
  // BK::Vector<BK::Array<BK::Array<double, Problem::varDim>, order>> YminusFirst;
  // BK::Vector<BK::Array<BK::Array<double, Problem::varDim>, order>> YminusLast; 
};

template<class FluxInfoVecBordArray>
void XFluxMPI2d::begin(FluxInfoVecBordArray& fluxInfoArrayX) {
  
  for(int iy=0; iy<ncely_; iy++) 
    for(int oy=0; oy<order; oy++){
      XminusLastCell(iy, oy) = fluxInfoArrayX(ncelx_-1, iy, oy)[Val::minus] ;
      XplusFirstCell(iy, oy) = fluxInfoArrayX(0, iy, oy)[Val::plus] ;}

  
  int numberOfMpiElements = XminusLastCell.size()*varDim;
  // non-blocking boundary value exchanges
  // MPI_Isend(buf, count, datatype, dest, tag, comm, &request) 
  MPI_Isend( XminusLastCell.data(), numberOfMpiElements, MPI_DOUBLE,mpiBase.get_commRankNeighboorP(0),1,mpiBase.get_cartcomm(), mpiBase.get_request());
  MPI_Isend( XplusFirstCell.data(), numberOfMpiElements, MPI_DOUBLE,mpiBase.get_commRankNeighboorM(0),2,mpiBase.get_cartcomm(), mpiBase.get_request());
  // MPI_Irecv(buf, count, datatype, source, tag, comm, request)
  MPI_Irecv( XminusFirstCell.data(), numberOfMpiElements, MPI_DOUBLE, mpiBase.get_commRankNeighboorM(0), 1, mpiBase.get_cartcomm(), mpiBase.get_request());
  MPI_Irecv( XplusLastCell.data(), numberOfMpiElements, MPI_DOUBLE, mpiBase.get_commRankNeighboorP(0), 2, mpiBase.get_cartcomm(), mpiBase.get_request());
}

template<class FluxInfoVecBordArray>
void YFluxMPI2d::begin(FluxInfoVecBordArray& fluxInfoArrayY) {
  
  for(int iy=0; iy<ncelx_; iy++) 
    for(int oy=0; oy<order; oy++){
      YminusLast(iy, oy) = fluxInfoArrayY(iy, ncely_-1, oy)[Val::minus] ;
      YplusFirst(iy, oy) = fluxInfoArrayY(iy, 0, oy)[Val::plus] ; }

  int numberOfMpiElements = YminusLast.size()*varDim;
  // non-blocking boundary value exchanges
  // MPI_Isend(buf, count, datatype, dest, tag, comm, request) 
  MPI_Isend(YminusLast.data(), numberOfMpiElements, MPI_DOUBLE,mpiBase.get_commRankNeighboorM(1),3,mpiBase.get_cartcomm(), mpiBase.get_request());
  MPI_Isend(YplusFirst.data(), numberOfMpiElements, MPI_DOUBLE, mpiBase.get_commRankNeighboorP(1),4,mpiBase.get_cartcomm(), mpiBase.get_request());
  // MPI_Irecv(buf, count, datatype, source, tag, comm, request) 
  MPI_Irecv(YminusFirst.data(), numberOfMpiElements, MPI_DOUBLE, mpiBase.get_commRankNeighboorP(1), 3, mpiBase.get_cartcomm(), mpiBase.get_request());
  MPI_Irecv(YplusLast.data(), numberOfMpiElements, MPI_DOUBLE, mpiBase.get_commRankNeighboorM(1) , 4, mpiBase.get_cartcomm(), mpiBase.get_request());
}
    
#endif
