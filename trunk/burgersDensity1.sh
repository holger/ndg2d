#! /bin/bash

cp order.hpp_template order.hpp
sed -i 's/static constexpr int order = 3/static constexpr int order = 3/g' order.hpp
sed -i 's/static constexpr int rkOrder = 3/static constexpr int rkOrder = 3/g' order.hpp
cd release
make burgersDensity1d
cd ..
release/burgersDensity1d problem_burgersDensity1d3.init

cp order.hpp_template order.hpp
sed -i 's/static constexpr int order = 3/static constexpr int order = 4/g' order.hpp
sed -i 's/static constexpr int rkOrder = 3/static constexpr int rkOrder = 4/g' order.hpp
cd release
make burgersDensity1d
cd ..
release/burgersDensity1d problem_burgersDensity1d4.init

cp order.hpp_template order.hpp
sed -i 's/static constexpr int order = 3/static constexpr int order = 5/g' order.hpp
sed -i 's/static constexpr int rkOrder = 3/static constexpr int rkOrder = 5/g' order.hpp
cd release
make burgersDensity1d
cd ..
release/burgersDensity1d problem_burgersDensity1d5.init

cp order.hpp_template order.hpp
sed -i 's/static constexpr int order = 3/static constexpr int order = 6/g' order.hpp
sed -i 's/static constexpr int rkOrder = 3/static constexpr int rkOrder = 6/g' order.hpp
cd release
make burgersDensity1d
cd ..
release/burgersDensity1d problem_burgersDensity1d6.init

cp order.hpp_template order.hpp
sed -i 's/static constexpr int order = 3/static constexpr int order = 6/g' order.hpp
sed -i 's/static constexpr int rkOrder = 3/static constexpr int rkOrder = 3/g' order.hpp
cd release
make burgersDensity1d
cd ..
release/burgersDensity1d problem_burgersDensity1d6.init

cp order.hpp_template order.hpp
sed -i 's/static constexpr int order = 3/static constexpr int order = 7/g' order.hpp
sed -i 's/static constexpr int rkOrder = 3/static constexpr int rkOrder = 3/g' order.hpp
cd release
make burgersDensity1d
cd ..
release/burgersDensity1d problem_burgersDensity1d7.init

cp order.hpp_template order.hpp
sed -i 's/static constexpr int order = 3/static constexpr int order = 8/g' order.hpp
sed -i 's/static constexpr int rkOrder = 3/static constexpr int rkOrder = 6/g' order.hpp
cd release
make burgersDensity1d
cd ..
release/burgersDensity1d problem_burgersDensity1d8.init

cp order.hpp_template order.hpp
sed -i 's/static constexpr int order = 3/static constexpr int order = 8/g' order.hpp
sed -i 's/static constexpr int rkOrder = 3/static constexpr int rkOrder = 3/g' order.hpp
cd release
make burgersDensity1d
cd ..
release/burgersDensity1d problem_burgersDensity1d8.init

