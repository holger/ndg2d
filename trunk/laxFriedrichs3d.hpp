#ifndef laxFriedrichs3d_hpp
#define laxFriedrichs3d_hpp

#include "problem3d.hpp"

// lookup table versions:

// 3D Lax-Friedrichs ------------------------------------------------------------------------------------------------------

template<class Array>
typename Array::value_type flux_numeric_XR(int cellX, int cellY,int cellZ, int indexY,int indexZ, Array const& c)
{
  typedef typename Array::value_type Vector;
  
  Vector frX = 0.5*(problem.flux_vectorR(cellX, cellY, cellZ, indexY,indexZ, c)[0] + problem.flux_vectorL(cellX+1, cellY, cellZ, indexY,indexZ, c)[0]);

  double crX = std::max(fabs(problem.dFlux_value(cellX+1, cellY, cellZ, 0, indexY,indexZ, c)[0]),
			fabs(problem.dFlux_value(cellX, cellY, cellZ, order-1, indexY,indexZ, c)[0]));
  
  return frX - 0.5*crX*(valueL(cellX+1, cellY, cellZ, indexY,indexZ, c) - valueR(cellX, cellY, cellZ, indexY,indexZ, c));
}

template<class Array>
typename Array::value_type flux_numeric_XL(int cellX, int cellY,int cellZ, int indexY,int indexZ, Array const& c)
{
  typedef typename Array::value_type Vector;
  
  Vector flX = 0.5*(problem.flux_vectorR(cellX-1, cellY, cellZ, indexY,indexZ, c)[0] + problem.flux_vectorL(cellX, cellY, cellZ, indexY,indexZ, c)[0]);

  double clX = std::max(fabs(problem.dFlux_value(cellX-1, cellY, cellZ, order-1, indexY,indexZ, c)[0]),
			fabs(problem.dFlux_value(cellX, cellY, cellZ, 0, indexY,indexZ, c)[0]));
  
  return flX + 0.5*clX*(valueR(cellX-1, cellY, cellZ, indexY,indexZ, c) - valueL(cellX, cellY, cellZ, indexY,indexZ, c));
}

template<class Array>
typename Array::value_type flux_numeric_YT(int cellX, int cellY,int cellZ, int indexX,int indexZ, Array const& c)
{
  typedef typename Array::value_type Vector;
  
  Vector ftY = 0.5*(problem.flux_vectorT(cellX, cellY, cellZ, indexX,indexZ, c)[1] + problem.flux_vectorB(cellX, cellY+1, cellZ, indexX,indexZ, c)[1]);

  double ctY = std::max(fabs(problem.dFlux_value(cellX, cellY+1, cellZ, indexX,0, indexZ, c)[1]),
			fabs(problem.dFlux_value(cellX, cellY, cellZ, indexX, order-1,indexZ, c)[1]));
  
  return ftY - 0.5*ctY*(valueB(cellX, cellY+1, cellZ, indexX,indexZ, c) - valueT(cellX, cellY, cellZ, indexX,indexZ, c));
}

template<class Array>
typename Array::value_type flux_numeric_YB(int cellX, int cellY,int cellZ, int indexX,int indexZ, Array const& c)
{
  typedef typename Array::value_type Vector;
  
  Vector fbY = 0.5*(problem.flux_vectorT(cellX, cellY-1, cellZ, indexX,indexZ, c)[1] + problem.flux_vectorB(cellX, cellY, cellZ, indexX,indexZ, c)[1]);

  double cbY = std::max(fabs(problem.dFlux_value(cellX, cellY-1, cellZ, indexX,order-1,indexZ, c)[1]),
			fabs(problem.dFlux_value(cellX, cellY, cellZ, indexX, 0,indexZ, c)[1]));
  
  return fbY + 0.5*cbY*(valueT(cellX, cellY-1, cellZ, indexX,indexZ, c) - valueB(cellX, cellY, cellZ, indexX,indexZ, c));
}

template<class Array>
typename Array::value_type flux_numeric_ZF(int cellX, int cellY,int cellZ, int indexX,int indexY, Array const& c)
{
  typedef typename Array::value_type Vector;
  
  Vector ffZ = 0.5*(problem.flux_vectorF(cellX, cellY, cellZ, indexX,indexY, c)[2] + problem.flux_vectorBa(cellX, cellY, cellZ+1, indexX,indexY, c)[2]);

  double cfZ = std::max(fabs(problem.dFlux_value(cellX, cellY, cellZ+1, indexX, indexY,0, c)[2]),
			fabs(problem.dFlux_value(cellX, cellY, cellZ, indexX,indexY, order-1, c)[2]));
  
  return ffZ - 0.5*cfZ*(valueBa(cellX, cellY, cellZ+1, indexX,indexY, c) - valueF(cellX, cellY, cellZ, indexX,indexY, c));
}

template<class Array>
typename Array::value_type flux_numeric_ZB(int cellX, int cellY,int cellZ, int indexX,int indexY ,Array const& c)
{
  typedef typename Array::value_type Vector;
    
  Vector fbZ = 0.5*(problem.flux_vectorF(cellX, cellY, cellZ-1, indexX,indexY, c)[2] + problem.flux_vectorBa(cellX, cellY, cellZ, indexX,indexY, c)[2]);

  double cbZ = std::max(fabs(problem.dFlux_value(cellX, cellY, cellZ-1, indexX,indexY, order-1, c)[2]),
			fabs(problem.dFlux_value(cellX, cellY, cellZ, indexX,indexY, 0, c)[2]));
  
  return fbZ + 0.5*cbZ*(valueF(cellX, cellY, cellZ-1, indexX,indexY, c) - valueBa(cellX, cellY, cellZ, indexX,indexY, c));
}

#endif
