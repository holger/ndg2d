#ifndef problem_eulerIso2d_hpp
#define problem_eulerIso2d_hpp

#include <BasisKlassen/parameter.hpp>
#include <BasisKlassen/array.hpp>

const int varDim = 3;   // three variables: rho, rho*ux, rho*uy
const int spaceDim = 2; // 2d space

#include "types.hpp"
#include "cfl.hpp"
#include "problemBase2d.hpp"
#include "laxFriedrichs1d.hpp"
#include "gaussLobatto.hpp"

// 2D isothermal gaz
class Problem_EulerIso2d : public ProblemBase2d<varDim>
{
public:

  struct Var {
    static const int rho = 0;     // rho
    static const int rhoUx = 1;   // rho*ux
    static const int rhoUy = 2;   // rho*uy
  };

  friend class BK::StackSingleton<Problem_EulerIso2d>;

  Problem_EulerIso2d() {}

  void init(int ncelx, int ncely, std::string parameterFilename) {

    name_ = "eulerIso2d";

    ProblemBase2d::init(ncelx, ncely, parameterFilename);

    double nt = nt_;
    
    BK::ReadInitFile initFile(parameterFilename,true);
    a_ = initFile.getParameter<double>("a");
    if(!initFile.parameterExists("initCond")) initCond_ = 0;
    else initCond_ = initFile.getParameter<int>("initCond");

    if(nt > 0 && T_ > 0) {
      std::cerr << "ERROR in Problem_eulerIso2d::init(): nt = " <<
	nt << " > 0 && T = " << T_ << " > 0." << std::endl;
      exit(1);						   
    }

    assert(dx_ == dy_);
    
    dt_ = std::min(fabs(cfl_*dx_),fabs(cfl_*dx_/a_));

    if(nt > 0) {
      nt_ = nt;
      T_ = nt_*dt_;
    }
    else
      nt_ = int(T_/dt_) + 1;

    dt_ *= T_/(nt_*dt_);
    
    logParameter();
    LOGL(1,PAR(a_));

    srand(2);

    initialized_ = true;
  }

  FluxInfo bcYT(int ix, int ox, BK::Type2Type<FluxInfo> t) {
    double rho = 1.;
    double vx = 0.25;
    double vy = 0;
    
    return {rho, rho*vx, rho*vy};
  }

  FluxInfo bcYB(int ix, int ox, BK::Type2Type<FluxInfo> t) {
    double rho = 1.;
    double vx = -0.25;
    double vy = 0;
    
    return {rho, rho*vx, rho*vy};
  }

  Vector initialCondition(double x, double y) {
    Vector returnVec;

    switch(initCond_) {
    case 0: {
      returnVec = Vector{1, sin(2*M_PI*x/lx_), 0};
      break;
    }
    case 1: {
      //    Kelvin-Helmholtz initial condition:
      double middle = lx_/2;
      double vx = 0.25;
      double vy = 0;
      returnVec = Vector{1, vx*tanh((y-middle)/0.01) + 0.04*(double(rand())/RAND_MAX-0.5), vy + 0.04*(double(rand())/RAND_MAX-0.5)};
      break;
    }
    default:
      std::cout << "ERROR in Problem_EulerIso2d::initialCondition: unknown case = " << initCond_ << std::endl;
      exit(1);
    }
    
    // not working test for Kelvin-Helmholtz instability:
    // return Vector{1, 0.2*tanh((y-middle-0.1*sin(2*2*M_PI*x/lx_)*exp(-((y-middle)*(y-middle))/0.05))/0.1)+0*0.01*rand()/RAND_MAX, 0.1*cos(2*2*M_PI*x/lx_)*exp(-((y-middle)*(y-middle))/0.05)+0*0.01*rand()/RAND_MAX};
    //    return Vector{0.1*exp(-(x-middle/2)*(x-middle/2)/0.001-(y-middle/2)*(y-middle/2)/0.001)+1,0.2*tanh((y-middle)/0.1),0};
    //    return Vector{0.5*tanh((y-middle)/0.1)+1.5, 0.25*tanh((y-middle)/0.1) + 0.02*(double(rand())/RAND_MAX-0.5), 0 + 0.02*(double(rand())/RAND_MAX-0.5)};
    //    return Vector{1, 5*tanh((y-middle)/0.1), 0.1*sin(2*2*M_PI*x/lx_)*exp(-((y-middle)*(y-middle))/0.05)};

    return returnVec;
  }

  void getReference(Array & c) {}

  void lhs(Array& cNew, Array const& c, Array const& rhs) const {
    cNew = c + dt_*rhs;
  }    

  VecVec flux_vector(double rho, double ux, double uy) {
    return VecVec{ Vector{rho*ux, rho*ux*ux + rho*a_*a_, rho*ux*uy},
		   Vector{rho*uy, rho*uy*ux, rho*uy*uy + rho*a_*a_} };
  }
  
  VecVec flux_vector(Vector const& varArray) {
    double rho = varArray[Var::rho];
    double rhoUx = varArray[Var::rhoUx];
    double rhoUy = varArray[Var::rhoUy];

    double ux = rhoUx/rho;
    double uy = rhoUy/rho;

    // f0x = rhoUx; f1x = rhoUx*ux; f2x = rhoUx*uy;
    // f0y = rhoUy; f1y = rhoUy*ux; f2y = rhoUy*uy;
    return VecVec{ Vector{rhoUx, rhoUx*ux + rho*a_*a_, rhoUx*uy},
                   Vector{rhoUy, rhoUy*ux, rhoUy*uy + rho*a_*a_} };
  }

  BK::Array<double,2> dFlux_value(Vector const& varArray) {
    double rho = varArray[Var::rho];
    double rhoUx = varArray[Var::rhoUx];
    double rhoUy = varArray[Var::rhoUy];

    double ux = rhoUx/rho;
    double uy = rhoUy/rho;

    return BK::Array<double,2>({std::max({fabs(ux), fabs(ux + a_), fabs(ux - a_)}), 
	  std::max({fabs(uy), fabs(uy + a_), fabs(uy - a_)})});
  }

  void computeNumericalFlux(FluxVecArray& fluxVecArrayX, FluxVecArray& fluxVecArrayY,
			    FluxInfoBordArray& fluxInfoArrayX, FluxInfoBordArray& fluxInfoArrayY) {

    auto fluxFunc = [this](Vector const& varArray) { return this->flux_vector(varArray);};
    auto dfluxFunc = [this](Vector const& varArray) { return this->dFlux_value(varArray);};
    
    computeNumericalFlux_LF(fluxVecArrayX, fluxVecArrayY, fluxInfoArrayX, fluxInfoArrayY, fluxFunc, dfluxFunc);
  }
  
  CONSTPARA(double, a);
  CONSTPARA(int, initCond);
};

#endif
