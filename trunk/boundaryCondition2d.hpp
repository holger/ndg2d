#ifndef boundaryCondition2d_hpp
#define boundaryCondition2d_hpp

#include "mpiFlux2d.hpp"

extern XFluxMPI2d& xFluxMPI;
extern YFluxMPI2d& yFluxMPI;

class PeriodicBoundaryCondition2dFlux
{
public:
  template<class FluxInfoVecBordArray>
  void operator()(FluxInfoVecBordArray& fluxInfoArrayX, FluxInfoVecBordArray& fluxInfoArrayY)
  {
    //    std::cerr << "PeriodicBoundaryCondition2dFlux::operator()\n";
 
    xFluxMPI.begin(fluxInfoArrayX);
    yFluxMPI.begin(fluxInfoArrayY);
  }
  
};

// class PeriodicBoundaryCondition2dFlux
// {
// public:
//   template<class FluxInfoVecBordArray>
//   void operator()(FluxInfoVecBordArray& fluxInfoArrayX, FluxInfoVecBordArray& fluxInfoArrayY)
//   {
//     //    std::cerr << "PeriodicBoundaryCondition2dFlux::operator()\n";
    
//     enum Val { minus, plus };

//     auto dimensionsX = fluxInfoArrayX.dim();
//     int ncelx = dimensionsX[0];
//     int ncely = dimensionsX[1];

//     for(int iy=0; iy<ncely; iy++) 
//       for(int oy=0; oy<order; oy++){
// 	fluxInfoArrayX(0, iy, oy)[Val::minus] = fluxInfoArrayX(ncelx-1, iy, oy)[Val::minus];
// 	fluxInfoArrayX(ncelx-1, iy, oy)[Val::plus] = fluxInfoArrayX(0, iy, oy)[Val::plus];
//       }

//     auto dimensionsY = fluxInfoArrayY.dim();
//     ncelx = dimensionsY[0];
//     ncely = dimensionsY[1];

//     for(int ix=0; ix<ncelx; ix++)
//       for(int ox=0; ox<order; ox++) {
//     	fluxInfoArrayY(ix, 0, ox)[Val::minus] = fluxInfoArrayY(ix, ncely-1, ox)[Val::minus];
//     	fluxInfoArrayY(ix, ncely-1, ox)[Val::plus] = fluxInfoArrayY(ix, 0, ox)[Val::plus];  
//       }
//   }
// };



class PeriodicBoundaryCondition2dZero
{
public:
  template<class FluxInfoVecBordArray>
  void operator()(FluxInfoVecBordArray& fluxInfoArrayX, FluxInfoVecBordArray& fluxInfoArrayY)
  {
    std::cerr << "PeriodicBoundaryCondition2dZero::operator()\n";
    
    enum Val { minus, plus };

    auto dimensionsX = fluxInfoArrayX.dim();
    int ncelx = dimensionsX[0];
    int ncely = dimensionsX[1];

    for(int iy=0; iy<ncely; iy++) 
      for(int oy=0; oy<order; oy++){
    	fluxInfoArrayX(0, iy, oy)[Val::minus] = 0;
    	fluxInfoArrayX(ncelx-1, iy, oy)[Val::plus] = 0;
      }

    auto dimensionsY = fluxInfoArrayY.dim();
    ncelx = dimensionsY[0];
    ncely = dimensionsY[1];

    for(int ix=0; ix<ncelx; ix++)
      for(int ox=0; ox<order; ox++) {
    	fluxInfoArrayY(ix, 0, ox)[Val::minus] = 0;
    	fluxInfoArrayY(ix, ncely-1, ox)[Val::plus] = 0;
      }
  }
};

class PeriodicBoundaryCondition2d
{
public:
  template<class Array>
  void operator()(Array& c)
  {
    int ncelX = c.size(0);
    int ncelY = c.size(1);
    int orderX = c.size(2);
    int orderY = c.size(3);
    
    for(int ox=0; ox<orderX; ox++)
      for(int oy=0; oy<orderY; oy++){
	
	for(int iy=0; iy<ncelY; iy++) {
	  c(0, iy, ox, oy) = c(ncelX-2, iy, ox, oy);
	  c(ncelX-1, iy, ox, oy) = c(1, iy, ox, oy);
	}
	
	for(int ix=0; ix<ncelX; ix++) {
	  c(ix, 0, ox, oy) = c(ix, ncelY-2, ox, oy);
	  c(ix, ncelY-1, ox, oy) = c(ix, 1, ox, oy);
	}
      }
  }
};

#endif
