#ifndef problem_eulerIsoScalar2d_hpp
#define problem_eulerIsoScalar2d_hpp

#include <BasisKlassen/parameter.hpp>
#include <BasisKlassen/array.hpp>

const int varDim = 4;   // three variables: scalar, rho, rho*ux, rho*uy
const int spaceDim = 2; // 2d space

#include "types.hpp"
#include "cfl.hpp"
#include "problemBase2d.hpp"
#include "laxFriedrichs1d.hpp"
#include "gaussLobatto.hpp"
#include "logger.hpp"

// #undef LOGLEVEL
// #define LOGLEVEL 3

// 2D isothermal gaz
class Problem_EulerIsoScalar2d : public ProblemBase2d<varDim>
{
public:
  
  struct Var {
    static const int scalar = 0;
    static const int rho = 1;
    static const int rhoUx = 2;
    static const int rhoUy = 3;
  };
  
  friend class BK::StackSingleton<Problem_EulerIsoScalar2d>;

  Problem_EulerIsoScalar2d() {}

  void init(int ncelx, int ncely, std::string parameterFilename) {

    name_ = "eulerIsoScalar2d";
    
    ProblemBase2d::init(ncelx, ncely, parameterFilename);

    double nt = nt_;
    
    BK::ReadInitFile initFile(parameterFilename,true);
    a_ = initFile.getParameter<double>("a");
    if(!initFile.parameterExists("initCond")) initCond_ = 0;
    else initCond_ = initFile.getParameter<int>("initCond");

    if(nt > 0 && T_ > 0) {
      std::cerr << "ERROR in Problem_eulerIsoScalar2d::init(): nt = " <<
	nt << " > 0 && T = " << T_ << " > 0." << std::endl;
      exit(1);						   
    }

    dLagrangeMatrix.init();
    lagrangeMatrix.init();

    assert(dx_ == dy_);
    
    dt_ = std::min(fabs(cfl_*dx_),fabs(cfl_*dx_/a_));

    if(nt > 0) {
      nt_ = nt;
      T_ = nt_*dt_;
    }
    else
      nt_ = int(T_/dt_) + 1;

    dt_ *= T_/(nt_*dt_);
    
    logParameter();
    LOGL(1,PAR(a_));

    srand(2);

    initialized_ = true;
  }

  FluxInfo bcYT(int ix, int ox, BK::Type2Type<FluxInfo> t) {
    double scalar = 1.;
    double rho = 1.;
    double ux = 0.25;
    double uy = 0;
    
    //    VecVec flux = flux_vector(scalar,rho,ux,uy);
    // return {flux[Dir::x][Var::rho], flux[Dir::x][Var::rhoUx], flux[Dir::x][Var::rhoUy],
    // 	    flux[Dir::y][Var::rho], flux[Dir::y][Var::rhoUx], flux[Dir::y][Var::rhoUy],
    // 	    a_, a_, rho, ux, uy};
    return {scalar, rho, rho*ux, rho*uy};
  }

  FluxInfo bcYB(int ix, int ox, BK::Type2Type<FluxInfo> t) {
    double scalar = -1;
    double rho = 1.;
    double ux = -0.25;
    double uy = 0;
    
    //    VecVec flux = flux_vector(scalar,rho,ux,uy);
    // return {flux[Dir::x][Var::rho], flux[Dir::x][Var::rhoUx], flux[Dir::x][Var::rhoUy],
    // 	    flux[Dir::y][Var::rho], flux[Dir::y][Var::rhoUx], flux[Dir::y][Var::rhoUy],
    // 	    a_, a_, rho, ux, uy};
    return {scalar, rho, rho*ux, rho*uy};
  }
  
  void computeSourceTerm(Array const& c, Array & s) {
    LOGL(3,"Problem_EulerIsoScalar2d::computeSourceTerm");
    
    for(int a=0; a<int(c.size(0)); a++)
      for(int b=0; b<int(c.size(1)); b++)
	for(int ia=0; ia<int(c.size(2)); ia++)
	  for(int ib=0; ib<int(c.size(3)); ib++) {

	    double dxUx = 0;
	    double dyUy = 0;

	    for(int m=0; m<order;m++) 
	      for(int n=0; n<order;n++) {
		double rho = c(a, b, ia, ib)[Var::rho];
		double rhoUx = c(a, b, ia, ib)[Var::rhoUx];
		double rhoUy = c(a, b, ia, ib)[Var::rhoUy];
		double ux = rhoUx/rho;
		double uy = rhoUy/rho;

		dxUx += ux*dLagrangeMatrix(m, ia)* lagrangeMatrix(n, ib);
		dyUy += uy* lagrangeMatrix(m, ia)*dLagrangeMatrix(n, ib);
	      }

	    dxUx = dxUx/dx_*2;
	    dyUy = dyUy/dy_*2;

	    s(a,b,ia,ib)[0] = c(a,b,ia,ib)[0]*(dxUx + dyUy);
	  }

    LOGL(4,"Problem_EulerIsoScalar2d::computeSourceTerm-end");
  }
  
  Vector initialCondition(double x, double y) {
    Vector returnVec;

    switch(initCond_) {
    case 0: {
      double middle = lx_/2;
      returnVec = Vector{tanh((y-middle)/0.01),1, sin(2*M_PI*x/lx_), 0};
      break;
    }
    case 1: {
      //    Kelvin-Helmholtz initial condition:
      double middle = lx_/2;
      double vx = 0.1;
      double vy = 0;
      double width = 0.005;
      double noise = 0.01;
      returnVec = Vector{tanh((y-middle)/width), 1, vx*tanh((y-middle)/width) + noise*(double(rand())/RAND_MAX-0.5), vy + noise*(double(rand())/RAND_MAX-0.5)};
      break;
    }
    default:
      std::cout << "ERROR in Problem_EulerIsoScalar2d::initialCondition: unknown case = " << initCond_ << std::endl;
      exit(1);
    }
    
    // not working test for Kelvin-Helmholtz instability:
    // return Vector{1, 0.2*tanh((y-middle-0.1*sin(2*2*M_PI*x/lx_)*exp(-((y-middle)*(y-middle))/0.05))/0.1)+0*0.01*rand()/RAND_MAX, 0.1*cos(2*2*M_PI*x/lx_)*exp(-((y-middle)*(y-middle))/0.05)+0*0.01*rand()/RAND_MAX};
    //    return Vector{0.1*exp(-(x-middle/2)*(x-middle/2)/0.001-(y-middle/2)*(y-middle/2)/0.001)+1,0.2*tanh((y-middle)/0.1),0};
    //    return Vector{0.5*tanh((y-middle)/0.1)+1.5, 0.25*tanh((y-middle)/0.1) + 0.02*(double(rand())/RAND_MAX-0.5), 0 + 0.02*(double(rand())/RAND_MAX-0.5)};
    //    return Vector{1, 5*tanh((y-middle)/0.1), 0.1*sin(2*2*M_PI*x/lx_)*exp(-((y-middle)*(y-middle))/0.05)};

    return returnVec;
  }

  void getReference(Array & c) {}

  void lhs(Array& cNew, Array const& c, Array const& rhs) const {
    cNew = c + dt_*rhs;
  }    

  VecVec flux_vector(double scalar, double rho, double ux, double uy) {
    return VecVec{ Vector{scalar*ux, rho*ux, rho*ux*ux + rho*a_*a_, rho*ux*uy},
		   Vector{scalar*uy, rho*uy, rho*uy*ux, rho*uy*uy + rho*a_*a_} };
  }
  
  VecVec flux_vector(Vector data) {
    double scalar = data[Var::scalar];
    double rho = data[Var::rho];
    double rhoUx = data[Var::rhoUx];
    double rhoUy = data[Var::rhoUy];

    double ux = rhoUx/rho;
    double uy = rhoUy/rho;

    // f0x = rhoUx; f1x = rhoUx*ux; f2x = rhoUx*uy;
    // f0y = rhoUy; f1y = rhoUy*ux; f2y = rhoUy*uy;
    return VecVec{ Vector{scalar*ux, rhoUx, rhoUx*ux + rho*a_*a_, rhoUx*uy},
                   Vector{scalar*uy, rhoUy, rhoUy*ux, rhoUy*uy + rho*a_*a_} };
  }

  BK::Array<double,2> dFlux_value(Vector data) {
    //    double scalar = data[Var::scalar];
    double rho = data[Var::rho];
    double rhoUx = data[Var::rhoUx];
    double rhoUy = data[Var::rhoUy];

    double ux = rhoUx/rho;
    double uy = rhoUy/rho;

    return BK::Array<double,2>({std::max({fabs(ux), fabs(ux + a_), fabs(ux - a_)}), 
	  std::max({fabs(uy), fabs(uy + a_), fabs(uy - a_)})});
  }

  void computeNumericalFlux(FluxVecArray& fluxVecArrayX, FluxVecArray& fluxVecArrayY,
			    FluxInfoBordArray& fluxInfoArrayX, FluxInfoBordArray& fluxInfoArrayY) {

    auto fluxFunc = [this](Vector const& varArray) { return this->flux_vector(varArray);};
    auto dfluxFunc = [this](Vector const& varArray) { return this->dFlux_value(varArray);};
    
    computeNumericalFlux_LF(fluxVecArrayX, fluxVecArrayY, fluxInfoArrayX, fluxInfoArrayY, fluxFunc, dfluxFunc);
  }

  CONSTPARA(double, a);
  CONSTPARA(int, initCond);
  
  DLagrangeMatrix<order> dLagrangeMatrix;
  LagrangeMatrix<order> lagrangeMatrix;
};

#endif
