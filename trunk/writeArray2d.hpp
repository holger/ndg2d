#ifndef writeArray2d_hpp
#define writeArray2d_hpp

#include <iostream>
#include <fstream>
#include <string>

#include <BasisKlassen/multiArray.hpp>

#include "value2d.hpp"
#include "mpiBase.hpp"

#ifdef NETCDF
#include <netcdf.h>
#include <netcdf_par.h>
#include "writeArray2d_netcdf.hpp"
#endif

template<class Array>
void writeArray(const Array& array)
{
  for(int l=0; l<array.sizeOrderY();l++){
    for(int k=0; k<array.sizeOrderX();k++){
      for(int i=0; i<array.size(0);i++){
	for(int j=0;j<array.size(1);j++){
	  std::cout<<array(i,j,k,l)<<"\t";
	}std::cout<<std::endl;
      }std::cout<<std::endl;
    }std::cout<<std::endl;
  }
}

template<class Array>
void writeArray(const Array& array, std::string filename)
{
  size_t varDim = (*array.begin()).size();
  for(size_t var = 0; var < varDim; var++) {

    std::ofstream out(BK::toString(filename,"-",var,".data"));
    
    for(size_t ox=0;ox<array.size(2);ox++){
      for(size_t oy=0;oy<array.size(3);oy++){
	for(size_t j=0;j<array.size(1);j++){
	  for(size_t i=0;i<array.size(0);i++){
	    out << "\t" << array(i,j,ox,oy)[var];
	  }
	  out<<std::endl;
	}
	out<<std::endl;
      }
      out<<std::endl;
    }
    out.close();
  }
}
  
template<class Array>
void writeFine(const Array& array, std::string filename, int nPoParCel, double dx, double dy)
{
  size_t varDim = (*array.begin()).size();

  std::cerr << "varDim = " << varDim << std::endl;
  for(size_t var = 0; var < varDim; var++) {
    
    typedef BK::MultiArray<2,double> Matrix;
    Matrix u(array.size(0)*nPoParCel, array.size(1)*nPoParCel);

    for(size_t ix=0;ix<array.size(0);ix++)
      for(size_t iy=0;iy<array.size(1);iy++)
	for(int jx=0;jx<nPoParCel;jx++)
	  for(int jy=0;jy<nPoParCel;jy++)
	    u(ix*nPoParCel + jx,iy*nPoParCel+jy) = value(ix, iy, -1 + jx*2./(nPoParCel), -1 + jy*2./(nPoParCel), array)[var];
    
    std::ofstream out(BK::toString(filename,"-",var,".data"));
    
    for(size_t j=0;j<array.size(1)*nPoParCel;j++){
      for(size_t i=0;i<array.size(0)*nPoParCel;i++){
	out<<"\t"<<u(i,j);
      }
      out<<std::endl;
    }
    out.close();


    typedef BK::MultiArray<1,double> Array1;
    // std::cerr << "array.size(1) = " << array.size(1) << std::endl;
    Array1 ux(array.size(0)*nPoParCel);
    for(size_t ix=0;ix<array.size(0);ix++)
      for(int jx=0;jx<nPoParCel;jx++)
	ux(ix*nPoParCel+jx) = value(ix, 0, -1 + jx*2./(nPoParCel), 0., array)[var];
    
    double lx = dx * array.size(0) * mpiBase.get_dimSize()[0];
    double startvalx = 0.0 +  mpiBase.procCoord()[0] * (lx/ mpiBase.get_dimSize()[0]);
    
    out.open(BK::toString(filename,"-",var,"-x.data"));
    for(size_t i=0;i<array.size(0)*nPoParCel;i++) {
      out <<startvalx + i*dx/nPoParCel << "\t" << ux(i) <<std::endl;
      //      std::cerr << i << "\t" << dx << "\t" << nPoParCel << "\t" << i*dx/nPoParCel << std::endl;
    }
    
    out.close();
    
    Array1  uy(array.size(1)*nPoParCel);
    
    for(size_t iy=0;iy<array.size(1);iy++)
      for(int jy=0;jy<nPoParCel;jy++)
	uy(iy*nPoParCel+jy) = value(0, iy, 0, -1 + jy*2./(nPoParCel), array)[var];

    double ly = dy * array.size(1) * mpiBase.get_dimSize()[1];
    double startvaly = 0.0 +  mpiBase.procCoord()[1] * (ly/ mpiBase.get_dimSize()[1]);
    
    out.open(BK::toString(filename,"-",var,"-y.data"));
    for(size_t j=0;j<array.size(1)*nPoParCel;j++)
      out << startvaly + j*dy/nPoParCel << "\t" << uy(j) <<std::endl;
    
    out.close();
  }
}

template<class Array>
void writeBin(const Array& array, std::string filename, int n, double dx, double dy)
{
  size_t varDim = (*array.begin()).size();

  BK::MultiArray<2,double> data(n,n);

  for(int i=0; i<n; i++) 
    for(int j=0; j<n; j++) {
      data(i,j) = i*dx;
    }

  std::ofstream outFile;
  outFile.open(filename + "_x.bin", std::ios::out | std::ios::binary);
  outFile.write( (char*)data.data(), sizeof(double)*data.size());
  outFile.close();

  for(int i=0; i<n; i++) 
    for(int j=0; j<n; j++) {
      data(i,j) = j*dy;
    }

  outFile.open(filename + "_y.bin", std::ios::out | std::ios::binary);
  outFile.write( (char*)data.data(), sizeof(double)*data.size());
  outFile.close();

  
  for(size_t var = 0; var < varDim; var++) {
    for(int i=0; i<n;i++) 
      for(int j=0; j<n;j++) {
	data(i,j) = value(i*dx, j*dy, array, dx, dy)[var]; 
      }
    
    std::ofstream outFile;
    outFile.open(filename + "-" + std::to_string(var) + ".bin", std::ios::out | std::ios::binary);
    outFile.write( (char*)data.data(), sizeof(double)*data.size());
    outFile.close();
  }
}

#endif
