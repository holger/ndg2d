#include "logger.hpp"

#include <BasisKlassen/toString.hpp>

#include "mpiBase.hpp"

std::string appendMessage(const std::string& message)
{
  if(LOGLEVEL < 2) {
    if(mpiBase.get_commRank() == 0)
      return BK::toString(mpiBase.get_commRank(),"  ",message);
    else
      return "";
  }
  else
    return BK::toString(mpiBase.get_commRank(),"  ",message);
}

std::string appendMessageL(const std::string& message)
{
  if(LOGLEVEL < 2) {
    if(mpiBase.get_commRank() == 0)
      return BK::toString(mpiBase.get_commRank(),"  ",message,"\n");
    else
      return "";
  }
  else
    return BK::toString(mpiBase.get_commRank(),"  ",message,"\n");
}

std::string getLogFilename()
{
  return "log.txt";
}
