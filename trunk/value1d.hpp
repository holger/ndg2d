#ifndef value1d_hpp
#define value1d_hpp

#include <type_traits>

#include "order.hpp"
#include "lagrange.hpp"

// 1D versions ---------------------------------------------------------------------

template<class Array>
inline auto value(int cellX, double x, Array const& c) -> typename std::remove_const<typename std::remove_reference<decltype(*c.begin())>::type>::type
{
  int orderX=c.size(1);
  
  typename std::remove_const<typename std::remove_reference<decltype(*c.begin())>::type>::type u;
  u = 0.;
  for(int ox=0; ox<orderX; ox++)
    u += c(cellX, ox)*lagrange(ox, order, x);
    
   return u;
}

template<class Array>
inline auto value(double x, Array const& c, double dx) -> typename std::remove_const<typename std::remove_reference<decltype(*c.begin())>::type>::type
{
  size_t orderX=c.size(1);

  //  double dx = problem.get_dx();
  size_t cell = size_t(x/dx);
  double xInCell = 2*(x-dx*cell)/dx-1;

  typename std::remove_const<typename std::remove_reference<decltype(*c.begin())>::type>::type u;
  u = 0.;
  for(size_t ox=0; ox<orderX; ox++)
    u += c(cell, ox)*lagrange(ox, order, xInCell);

  return u;
}

#endif
