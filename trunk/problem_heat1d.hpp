#ifndef problem_heat1d_hpp
#define problem_heat1d_hpp

#include <BasisKlassen/parameter.hpp>
#include <BasisKlassen/singleton.hpp>
#include <BasisKlassen/assert.hpp>

const int varDim = 2;    // two variables: value and q=dval/dx
const int spaceDim = 1;

#include "types.hpp"
#include "value1d.hpp"
#include "cfl.hpp"
#include "centralFlux1d.hpp"
#include "notiModify.hpp"
#include "problemBase1d.hpp"

// Burgers equations with density
class Problem_Heat1d : public ProblemBase1d<varDim>
{
public:

  friend class BK::StackSingleton<Problem_Heat1d>;

  Problem_Heat1d() {}

  void init(int ncel, std::string parameterFilename) {
    
    name_ = "heat1d";
    
    ProblemBase1d::init(ncel, parameterFilename);
	
    BK::ReadInitFile initFile(parameterFilename,true);
    nu_ = initFile.getParameter<double>("nu");

    dt_ = fabs(cfl_*dx_*dx_/nu_);
    if(nt_ > 0) {
      T_ = nt_*dt_;
    }
    else {
      nt_ = int(T_/dt_) + 1;
    }
    dt_ *= T_/(nt_*dt_);

    initialized_ = true;
    
    logParameter();

    std::cerr << "nu = " << nu_ << std::endl;
  }

  Vector initialCondition(double x) {
    assert(initialized_);

    return initialConditionShift(x,0);
  }

  Vector initialConditionShift(double x, double shift = 0) {
    assert(initialized_);
    // double x0 = 0.35;
    // double y0 = 0.35;

    // int kMax = 10;
    // double temp = 0;
    // for(int k=1; k<=kMax; k++)
    //   temp += pow(k,-5./6)*sin(2*M_PI*k/lambda_*(x-shift-2*M_PI/(k*k)));

    // return temp;

    double k=1;
    return Vector{sin(2*M_PI*k*(x-shift)/lx_), 2*M_PI*k/lx_*cos(2*M_PI*k*(x-shift)/lx_)};
  }

  void lhs(Array& cNew, Array const& c, Array const& rhs) const {
    for(size_t i=0; i<c.size(); i++)
      cNew[i][0] = c[i][0] + dt_*rhs[i][0];

    for(size_t i=0; i<c.size(); i++)
      cNew[i][1] = rhs[i][1];
  }

  VecVec flux_vector(Vector const& varArray, size_t cell = 0, size_t index = 0) {
    assert(initialized_);

    double u = varArray[0];
    double q = varArray[1];
    
    return {{sqrt(nu_)*q, sqrt(nu_)*u}};
  }

  void compute_fluxArray(FluxVecArray& fluxArray, FluxInfoBordArray const& fluxInfoArray)
  {
    auto fluxFunc = [this](Vector const& varArray) { return this->flux_vector(varArray);};
    compute_fluxArray_Central(fluxArray, fluxInfoArray, fluxFunc);
  }

  CONSTPARA(double, nu);
};

typedef Problem_Heat1d Problem;
extern Problem& problem;

#endif
