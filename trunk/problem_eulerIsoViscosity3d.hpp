#ifndef problem_eulerIsoViscosity3d_hpp
#define problem_eulerIsoViscosity3d_hpp

#include <BasisKlassen/parameter.hpp>
#include <BasisKlassen/singleton.hpp>
#include <BasisKlassen/assert.hpp>
#include <BasisKlassen/parseFromFile.hpp>
#include <BasisKlassen/vector.hpp>

const int varDim = 13;    // variables: rho, mx, my, mz, qxx, qxy, qxz, qyx, qyy, qyz, qzx, qzy, qzz
                          // mx=rho*ux, my=rho*uy, mz=rho*uz
                          // qxx = d_x u_x; qxy = d_x u_y; qxz = d_x u_z; 
                          // qyx = d_y u_x; qyy = d_y u_y; qyz = d_y u_z; 
                          // qzx = d_z u_x; qzy = d_z u_y; qzz = d_z u_z;

const int spaceDim = 3;   // 3d space

#include "types.hpp"
#include "cfl.hpp"
#include "notiModify.hpp"
#include "problemBase3d.hpp"
#include "laxFriedrichs1d.hpp"
#include "centralFlux1d.hpp"
#include "order.hpp"
#include "gaussLobatto.hpp"

// 3D Burgers equations with density
class Problem_EulerIsoViscosity3d : public ProblemBase3d<varDim>
{
public:

  struct Var {
    static const int rho = 0;  // rho
    static const int mx = 1;   // rho*ux
    static const int my = 2;   // rho*uy
    static const int mz = 3;   // rho*uz
    static const int qxx = 4;  
    static const int qxy = 5;  
    static const int qxz = 6;  
    static const int qyx = 7;  
    static const int qyy = 8;  
    static const int qyz = 9;
    static const int qzx = 10;  
    static const int qzy = 11;  
    static const int qzz = 12;
  };

  friend class BK::StackSingleton<Problem_EulerIsoViscosity3d>;

  Problem_EulerIsoViscosity3d() {}
  
  void init(int ncelx, int ncely, int ncelz, std::string parameterFilename) {
    //    notifyField.add("fieldFinal", FieldNotifyCallBack(std::bind(&Problem_EulerIso3d::errorComp, this, std::placeholders::_1), "Problem_EulerIso3d::errorComp"));    

    name_ = "eulerIsoViscosity3d";
    ProblemBase3d::init(ncelx, ncely, ncelz, parameterFilename);

    BK::ReadInitFile initFile(parameterFilename,true);

    double nt = nt_;
    nu_ = initFile.getParameter<double>("nu");
    sqrtNu_ = sqrt(nu_);
    a_ = initFile.getParameter<double>("a");
    
    if(nt > 0 && T_ > 0) {
      std::cerr << "ERROR in Problem_eulerIsoViscosity2d::init(): nt = " <<
	nt << " > 0 && T = " << T_ << " > 0." << std::endl;
      exit(1);						   
    }

    ASSERT((dx_ == dy_) && (dy_ == dz_) , BK::toString("dx_ = ", dx_, "; dy_ = ", dy_,"; dz_ = ", dz_));

    double dtAd = fabs(cfl_*dx_/(1+a_));
    double dtHeat = fabs(cfl_*dx_*dx_/nu_)/8.;

    std::cout << "dtAd = " << dtAd << "\t dtHeat = " << dtHeat << std::endl;

    if(nu_ == 0)
      dt_ = dtAd;
    else
      dt_ = std::min(dtAd,dtHeat);
    
    if(nt > 0) {
      nt_ = nt;
      T_ = nt_*dt_;
    }
    else
      nt_ = int(T_/dt_) + 1;

    dt_ *= T_/(nt_*dt_);

    logParameter();

    LOGL(1,PAR(nu_));
    LOGL(1,PAR(a_));

    initialized_ = true;
  }

  void errorComp(Array const* c);

  Vector initialCondition(double x, double y, double z) {
    //    return Vector{1., 0, 0, sin(2*M_PI*z/lz_), 0, 0, 0, 0, 0, 0, 0, 0, 0};
    // return Vector{1., 0, sin(2*M_PI*y/ly_), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    return Vector{1., sin(2*M_PI*x/lx_), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
  }

  void getReference(Array & c) {}

  void lhs(Array& cNew, Array const& c, Array const& rhs) const {
    cNew = c + dt_*rhs;
  }    

  VecVec flux_vector(Vector const& varArray) const {
    assert(initialized_);

    auto nonLin = flux_vector_nonLin(varArray);
    auto diffusion = flux_vector_diffusion(varArray);

    auto flux = nonLin + diffusion;

    return flux;
  }

  VecVec flux_vector_nonLin(Vector const& varArray) const {
    double rho = varArray[Var::rho];
    double rhoUx = varArray[Var::mx];
    double rhoUy = varArray[Var::my];
    double rhoUz = varArray[Var::mz];
    
    double ux = rhoUx/rho;
    double uy = rhoUy/rho;
    double uz = rhoUz/rho;
    
    return VecVec{ Vector{rhoUx, rhoUx*ux + rho*a_*a_, rhoUx*uy, rhoUx*uz, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	Vector{rhoUy, rhoUy*ux, rhoUy*uy + rho*a_*a_, rhoUy*uz, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	  Vector{rhoUz, rhoUz*ux, rhoUz*uy, rhoUz*uz + rho*a_*a_, 0, 0, 0, 0, 0, 0, 0, 0, 0} };
  }
  
  VecVec flux_vector_diffusion(Vector const& varArray) const {
    double rho = varArray[Var::rho];
    double rhoUx = varArray[Var::mx];
    double rhoUy = varArray[Var::my];
    double rhoUz = varArray[Var::mz];
    
    double ux = rhoUx/rho;
    double uy = rhoUy/rho;
    double uz = rhoUz/rho;

    double qxx = varArray[Var::qxx];
    double qxy = varArray[Var::qxy];
    double qxz = varArray[Var::qxz];
    double qyx = varArray[Var::qyx];
    double qyy = varArray[Var::qyy];
    double qyz = varArray[Var::qyz];
    double qzx = varArray[Var::qzx];
    double qzy = varArray[Var::qzy];
    double qzz = varArray[Var::qzz];

    return VecVec{Vector{0 ,-rho*sqrtNu_*qxx, -rho*sqrtNu_*qxy, -rho*sqrtNu_*qxz, -sqrtNu_*ux, -sqrtNu_*uy, -sqrtNu_*uz, 0, 0, 0, 0, 0, 0},
 	          Vector{0, -rho*sqrtNu_*qyx, -rho*sqrtNu_*qyy, -rho*sqrtNu_*qyz, 0, 0, 0, -sqrtNu_*ux, -sqrtNu_*uy, -sqrtNu_*uz, 0, 0, 0},
         	  Vector{0, -rho*sqrtNu_*qzx, -rho*sqrtNu_*qzy, -rho*sqrtNu_*qzz, 0, 0, 0, 0, 0, 0, -sqrtNu_*ux, -sqrtNu_*uy, -sqrtNu_*uz}};
  }

  BK::Array<double,3> dFlux_value(Vector const& varArray) {
    double rho = varArray[Var::rho];
    double rhoUx = varArray[Var::mx];
    double rhoUy = varArray[Var::my];
    double rhoUz = varArray[Var::mz];
    
    double ux = rhoUx/rho;
    double uy = rhoUy/rho;
    double uz = rhoUz/rho;

    return BK::Array<double,3>({std::max({fabs(ux), fabs(ux + a_), fabs(ux - a_)}),
				std::max({fabs(uy), fabs(uy + a_), fabs(uy - a_)}),
				std::max({fabs(uz), fabs(uz + a_), fabs(uz - a_)})});
  }

  void computeNumericalFlux(FluxVecArray& fluxVecArrayX, FluxVecArray& fluxVecArrayY, FluxVecArray& fluxVecArrayZ,
			    FluxInfoBordArray& fluxInfoArrayX, FluxInfoBordArray& fluxInfoArrayY, FluxInfoBordArray& fluxInfoArrayZ) {
    
    FluxVecArray fluxVecArrayX_nl(fluxVecArrayX.dim());
    FluxVecArray fluxVecArrayY_nl(fluxVecArrayY.dim());
    FluxVecArray fluxVecArrayZ_nl(fluxVecArrayZ.dim());

    auto fluxFunc_nl = [this](Vector const& varArray) { return this->flux_vector_nonLin(varArray);};
    auto dfluxFunc = [this](Vector const& varArray) { return this->dFlux_value(varArray);};
    
    computeNumericalFlux_LF(fluxVecArrayX_nl, fluxVecArrayY_nl, fluxVecArrayZ_nl, fluxInfoArrayX, fluxInfoArrayY, fluxInfoArrayZ, fluxFunc_nl, dfluxFunc);


    for(size_t cellX=0; cellX<fluxVecArrayX.size(0); cellX++)
      for(size_t cellY=0; cellY<fluxVecArrayX.size(1); cellY++)
	for(size_t cellZ=0; cellZ<fluxVecArrayZ.size(1); cellZ++)
	for(size_t orderY=0; orderY<order; orderY++)
	  for(size_t orderZ=0; orderZ<order; orderZ++) 
	  for(int var=4; var<13; var++) 
	    fluxVecArrayX_nl(cellX, cellY, cellZ, orderY, orderZ)[var] = 0;
    
    for(size_t cellX=0; cellX<fluxVecArrayY.size(0); cellX++)
      for(size_t cellY=0; cellY<fluxVecArrayY.size(1); cellY++)
	for(size_t cellZ=0; cellZ<fluxVecArrayZ.size(1); cellZ++)
	  for(size_t orderX=0; orderX<order; orderX++)
	    for(size_t orderZ=0; orderZ<order; orderZ++) 
	      for(int var=4; var<13; var++) 
		fluxVecArrayY_nl(cellX, cellY, cellZ, orderX, orderZ)[var] = 0;

    for(size_t cellX=0; cellX<fluxVecArrayY.size(0); cellX++)
      for(size_t cellY=0; cellY<fluxVecArrayY.size(1); cellY++)
	for(size_t cellZ=0; cellZ<fluxVecArrayZ.size(1); cellZ++)
	  for(size_t orderX=0; orderX<order; orderX++)
	    for(size_t orderY=0; orderY<order; orderY++) 
	      for(int var=4; var<13; var++) 
		fluxVecArrayZ_nl(cellX, cellY, cellZ, orderX, orderY)[var] = 0;

    FluxVecArray fluxVecArrayX_d(fluxVecArrayX.dim());
    FluxVecArray fluxVecArrayY_d(fluxVecArrayY.dim());
    FluxVecArray fluxVecArrayZ_d(fluxVecArrayZ.dim());

    auto fluxFunc_d = [this](Vector const& varArray) { return this->flux_vector_diffusion(varArray);};
    
    computeNumericalFlux_Central(fluxVecArrayX_d, fluxVecArrayY_d, fluxVecArrayZ_d, fluxInfoArrayX, fluxInfoArrayY, fluxInfoArrayZ, fluxFunc_d);

    fluxVecArrayX = fluxVecArrayX_nl + fluxVecArrayX_d;
    fluxVecArrayY = fluxVecArrayY_nl + fluxVecArrayY_d;
    fluxVecArrayZ = fluxVecArrayZ_nl + fluxVecArrayZ_d;
  }
  
  CONSTPARA(double, nu);
  CONSTPARA(double, sqrtNu);
  CONSTPARA(double, a);
};

typedef Problem_EulerIsoViscosity3d Problem;
extern Problem& problem;

#endif
