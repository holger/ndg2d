// g++ -o main main.cpp -g -Wall -std=c++11 -DCPP11
//

#include <iostream>
#include <cstdlib>
#include <cmath>
#include <vector>
#include <fstream>
#include <functional>
#include <chrono>
#include <mpi.h>

#include <BasisKlassen/parseFromFile.hpp>
#include <BasisKlassen/mpiBase.hpp>
#include <BasisKlassen/timer.hpp>

#ifdef PROBLEM_ADVECTIONCUBED
#include "geo_cubedSphere.hpp"
typedef GeoCubedSphere Geo;
#define CUBED 
#elif PROBLEM_ADVECTIONCUBEDNAIR
#include "geo_cubedSphere.hpp"
typedef GeoCubedSphere Geo;
#define CUBED 
#elif PROBLEM_ADVECTIONSOURCECUBED
#include "geo_cubedSphere.hpp"
typedef GeoCubedSphere Geo;
#define CUBED
#elif PROBLEM_BURGERSDENSITYCUBED
#include "geo_cubedSphere.hpp"
typedef GeoCubedSphere Geo;
#define CUBED
#elif PROBLEM_EULERISOCUBED
#include "geo_cubedSphere.hpp"
typedef GeoCubedSphere Geo;
#define CUBED
#elif PROBLEM_SHALLOWWATERCUBED
#include "geo_cubedSphere.hpp"
typedef GeoCubedSphere Geo;
#define CUBED
#elif PROBLEM_SHALLOWWATERCUBEDNAIR
#include "geo_cubedSphere.hpp"
typedef GeoCubedSphere Geo;
#define CUBED
#else
#include "geo_cart2d.hpp"
typedef GeoCart2d Geo;
#endif

#include "gaussLobatto.hpp"
#include "writeArray2d.hpp"
#include "notiModify.hpp"

FieldNotify& notifyField = BK::StackSingleton<FieldNotify>::instance();
FieldModify& modifyField = BK::StackSingleton<FieldModify>::instance();
FieldVecNotify& notifyFieldVec = BK::StackSingleton<FieldVecNotify>::instance();
FieldVecModify& modifyFieldVec = BK::StackSingleton<FieldVecModify>::instance();
EventNotify& notifyEvent = BK::StackSingleton<EventNotify>::instance();

BK::MpiBase& mpiBase = BK::StackSingleton<BK::MpiBase>::instance();
XFluxMPI2d& xFluxMPI = BK::StackSingleton<XFluxMPI2d>::instance();
YFluxMPI2d& yFluxMPI = BK::StackSingleton<YFluxMPI2d>::instance();

#ifdef CUBED
CubedSphere_utils& cs_utils = BK::StackSingleton<CubedSphere_utils>::instance();
#endif

int main(int argc, char** argv)
{
  mpiBase.init(argc,&argv);

  int commRank = mpiBase.get_commRank();
  //int commSize = mpiBase.get_commSize();
  
  // std::cout << "commRank = " << mpiBase.get_commRank() << "\t commSize = " << mpiBase.get_commSize() << std::endl;
  
  if (commRank == 0 ) {
    std::cout << "Discontinuous Galerkin simulation starts ...\n";
  }
  if(argc < 2) {
    if (commRank == 0 ) {
      std::cerr << "ERROR: argc = " << argc << " < 2!.\n";
      std::cerr << "usage: main <parameterFilename>\n";
    }
    exit(1);
  } 

  std::string parameterFilename = argv[1];
  BK::ReadInitFile initFile(parameterFilename,true);
  std::string pathToBaseInitFile = initFile.getParameter<std::string>("problemBase2dInitFilePath");
  BK::path path(parameterFilename);
  BK::path branch_path = path.branch_path();
  //    std::cout << "branch_path = " << branch_path.string() << std::endl;
  std::string parameterBaseFilename = branch_path.string() + "/problemBase2d.init";
  std::cout << parameterFilename << "\t" << parameterBaseFilename << std::endl;

  BK::ReadInitFile initBaseFile(parameterBaseFilename,true);
  int ncelxMin = initBaseFile.getParameter<int>("ncelxMin") ; 
  int ncelxMax = initBaseFile.getParameter<int>("ncelxMax") ;
  // int ncelyMin = initFile.getParameter<int>("ncelxMin");
  // int ncelyMax = initFile.getParameter<int>("ncelxMax");
  
  for(int ncelx=ncelxMin;ncelx<=ncelxMax;ncelx*=2) {
    
    int  ncely=ncelx;
    problem.init(ncelx, ncely, parameterFilename);


    int mkdir = system(BK::toString("mkdir data").c_str());
    std::string dirName = BK::toString("data/", problem.get_name());
    mkdir = system(BK::toString("mkdir ", dirName).c_str());
    std::cerr << mkdir << std::endl;

#ifdef CUBED
    cs_utils.init(ncelx);
#endif 
    double dx = problem.get_dx();
    double dy = problem.get_dy();


    //int m=2;
    int nPoParCel=10;
    
    Problem::Matrix u(ncelx*nPoParCel,ncely*nPoParCel);
    Problem::Matrix pos(ncelx*nPoParCel,ncely*nPoParCel);
    Problem::Matrix uref(ncelx*nPoParCel,ncely*nPoParCel);

    std::cout << "ncelx = " << ncelx << "\t ncely= " << ncely << "\t dx = " << dx <<  "\t dy = " << dy << std::endl;
    std::cout << "lx = " << problem.get_lx() << ", ly = " << problem.get_ly() << std::endl;

    Geo geo(ncelx, ncely, parameterBaseFilename);
    geo.init();

    //    size_t rkOrder = problem.get_rkOrder();
    // writeFine(geo.get_c(), dirName + "/init_" +  std::to_string(order) + "-" +  std::to_string(rkOrder) + "_" + std::to_string(ncelx) + "_" + std::to_string(ncely), nPoParCel, dx, dy);

    auto start = std::chrono::system_clock::now();
    
    geo.solve();
    
    std::cout << "time = " << geo.get_time() << std::endl;
    
    auto end = std::chrono::system_clock::now();
    
    std::chrono::duration<double> elapsed_seconds = end-start;
    std::time_t end_time = std::chrono::system_clock::to_time_t(end);
    
    std::cout << "finished computation at " << std::ctime(&end_time)
	      << "elapsed time: " << elapsed_seconds.count() << "s\n";


    std::string fileName = BK::toString(dirName + "/chrono-ndg-",order,"-",problem.get_rkOrder(),".txt");
    std::ofstream out(fileName.c_str(), std::ios::app);
    out << ncelx << "\t" << elapsed_seconds.count() << std::endl;
    out.close();

    if(mpiBase.get_commRank() == 0)
      timerM.logEveryAccumulatedTime(BK::toString("timer.log"));
    
    LOGL(0,"-----------------------------");
    
    LOGL(0,BK::toString("Number of modify FIELD functions = ", modifyField.get_addedFunctionNumber(), " :\n"));
    for(auto entry : modifyField.get_map()) {
      auto executedKeyList = modifyField.get_executedKeyList();
      bool executed = std::find(executedKeyList.begin(), executedKeyList.end(), entry.first) != executedKeyList.end();
      
      std::string executedInfo = "not executed";
      if(executed)
	executedInfo = "executed";
      LOGL(0,BK::toString(std::setw(50), std::setfill('.'), std::left, entry.second.get_name(), std::setw(30), std::setfill('.'), std::left, entry.first, executedInfo));
    }
    
    LOGL(0,"-----------------------------");
    
    LOGL(0,BK::toString("Number of notify FIELD functions = ", notifyField.get_addedFunctionNumber(), " :\n"));
    for(auto entry : notifyField.get_map()) {
      auto executedKeyList = notifyField.get_executedKeyList();
      bool executed = std::find(executedKeyList.begin(), executedKeyList.end(), entry.first) != executedKeyList.end();
      
      std::string executedInfo = "not executed";
      if(executed)
	executedInfo = "executed";
      LOGL(0,BK::toString(std::setw(50), std::setfill('.'), std::left, entry.second.get_name(), std::setw(30), std::setfill('.'), std::left, entry.first, executedInfo));
    }

    LOGL(0,"-----------------------------");
    
    LOGL(0,BK::toString("Number of modify FIELD VEC functions = ", modifyFieldVec.get_addedFunctionNumber(), " :\n"));
    for(auto entry : modifyFieldVec.get_map()) {
      auto executedKeyList = modifyFieldVec.get_executedKeyList();
      bool executed = std::find(executedKeyList.begin(), executedKeyList.end(), entry.first) != executedKeyList.end();
      
      std::string executedInfo = "not executed";
      if(executed)
	executedInfo = "executed";
      LOGL(0,BK::toString(std::setw(50), std::setfill('.'), std::left, entry.second.get_name(), std::setw(30), std::setfill('.'), std::left, entry.first, executedInfo));
    }
    
    LOGL(0,"-----------------------------");
    
    LOGL(0,BK::toString("Number of notify FIELD VEC functions = ", notifyFieldVec.get_addedFunctionNumber(), " :\n"));
    for(auto entry : notifyFieldVec.get_map()) {
      auto executedKeyList = notifyFieldVec.get_executedKeyList();
      bool executed = std::find(executedKeyList.begin(), executedKeyList.end(), entry.first) != executedKeyList.end();
      
      std::string executedInfo = "not executed";
      if(executed)
	executedInfo = "executed";
      LOGL(0,BK::toString(std::setw(50), std::setfill('.'), std::left, entry.second.get_name(), std::setw(30), std::setfill('.'), std::left, entry.first, executedInfo));
    }

    LOGL(0,"-----------------------------");
    
    LOGL(0,BK::toString("Number of notify EVENT functions = ", notifyEvent.get_addedFunctionNumber(), " :\n"));
    for(auto entry : notifyEvent.get_map()) {
      auto executedKeyList = notifyEvent.get_executedKeyList();
      bool executed = std::find(executedKeyList.begin(), executedKeyList.end(), entry.first) != executedKeyList.end();
      
      std::string executedInfo = "not executed";
      if(executed)
	executedInfo = "executed";
      LOGL(0,BK::toString(std::setw(50), std::setfill('.'), std::left, entry.second.get_name(), std::setw(30), std::setfill('.'), std::left, entry.first, executedInfo));
    }

    // writeArray(geoCart.get_c(), dirName + "/c.data");

    // writeArray(geoCart.get_c(), dirName + "/cnew.data");
    
    /*int cat  = system(BK::toString("cat data").c_str());
    cat = system(BK::toString("cat ",dirName).c_str());
    std::cerr << cat << std::endl;*/


    // double errResult2=0;
    // for(int i=0;i<ncel;i++){
    //   double xi=i*dx;
    //   for(int j=0;j<nPoParCel;j++){
    // 	u[i*nPoParCel + j]=c(0,i)+(-1 + 2./(nPoParCel)*j)*c(1,i)+0.5*(3*(2./nPoParCel*j-1)*(2./nPoParCel*j-1)-1)*c(2,i);
    // 	double x=xi+j*(dx/nPoParCel);
    // 	posx[i*nPoParCel+j]=x;
    // 	errResult2+=pow((sin(2*M_PI*x/L)-u[i*nPoParCel + j]),2)*dx;
    // 	uref[i*nPoParCel + j]=sin(2*M_PI*x/L);
    //   }
	
    // }
    // double errResult=sqrt(errResult2);
    // filename = "unew_" + std::to_string(ncel) + ".data";
    // out.open(filename);
    // for(int i=0;i<ncel*nPoParCel;i++){
    //   out<<posx[i]<<"\t"<<u[i]<<"\t"<<uref[i]<<std::endl;
    // }
      
    // out.close();

    // out.open(errResultfilename,std::ios::app);
    // out<<ncel<<"\t"<<errResult<<std::endl;
    // out.close();
    
  }
  //mpiBase.finalize();
  return 0;
}


 
