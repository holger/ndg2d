// mpicxx -o main main.cpp -L/home/holger/soft/netcdf/lib -L/usr/lib/x86_64-linux-gnu/hdf5/openmpi -lnetcdf -lhdf5 -lhdf5_hl -lcurl

#include <mpi.h>
#include <iostream>
#include "/home/holger/soft/netcdf/include/netcdf.h"
#include "/home/holger/soft/netcdf/include/netcdf_par.h"

int main(int argc, char** argv)
{

  int mpi_size;
  int mpi_rank;
  char* mpi_name;
  int mpi_namelen;
  bool res;
  int DIMSIZE = 8;
  const int NDIMS = 2;
  int dimids[NDIMS];
  int v1id;

  const char* FILE = "test.nc";
  
/* Initialize MPI. */
    MPI_Init(&argc,&argv);
    MPI_Comm_size(MPI_COMM_WORLD, &mpi_size);
    MPI_Comm_rank(MPI_COMM_WORLD, &mpi_rank);
    //    MPI_Get_processor_name(mpi_name, &mpi_namelen);

    MPI_Info info = MPI_INFO_NULL;
    int ncid;
      
    if (mpi_rank == 1)
       printf("\n*** tst_parallel testing very basic parallel access.\n");

    /* Create a parallel netcdf-4 file. */
    res = nc_create_par(FILE, NC_NETCDF4|NC_MPIIO, MPI_COMM_WORLD, 
			 info, &ncid);

    /* Create two dimensions. */
    
    res = nc_def_dim(ncid, "d1", DIMSIZE, dimids);
    res = nc_def_dim(ncid, "d2", DIMSIZE, &dimids[1]);
      
    /* Create one var. */
    res = nc_def_var(ncid, "v1", NC_DOUBLE, NDIMS, dimids, &v1id);

    res = nc_enddef(ncid);

    size_t start[2];
    size_t count[2];
    /* Set up slab for this process. */
    int localSize = DIMSIZE/mpi_size;

    res = nc_put_att_int(ncid, NC_GLOBAL, "nx", NC_INT, 1, &DIMSIZE);
    res = nc_put_att_int(ncid, NC_GLOBAL, "ny", NC_INT, 1, &localSize);
	
    std::cout << "localSize = " << localSize << std::endl;
    
    start[0] = 0;
    start[1] = mpi_rank*localSize;
    count[0] = DIMSIZE;
    count[1] = localSize;

    double data[localSize*DIMSIZE];
    
    /* Create phoney data. We're going to write a 24x24 array of ints,
       in 4 sets of 144. */
    for (int i=0; i < DIMSIZE; i++)
      for (int j=0; j < localSize; j++) {
	//	std::cout << i << "\t" << j << "\t" << i*localSize+j << "\t" << (i+1)*10 + j + 1 + start[mpi_rank] << std::endl;
	data[i*localSize+j] = (i+1)*10 + j + 1 + start[mpi_rank];
      }

    /*if ((res = nc_var_par_access(ncid, v1id, NC_COLLECTIVE)))
      ERR;*/
    res = nc_var_par_access(ncid, v1id, NC_INDEPENDENT);

    /* Write slabs of phoney data. */
    res = nc_put_vara_double(ncid, v1id, start, count, 
			     &data[0]);

    /* Close the netcdf file. */
    res = nc_close(ncid);
    
    /* Shut down MPI. */
    MPI_Finalize();

    return 0;
}
