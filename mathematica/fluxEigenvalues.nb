(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 12.1' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     11967,        362]
NotebookOptionsPosition[     10673,        333]
NotebookOutlinePosition[     11062,        349]
CellTagsIndexPosition[     11019,        346]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell[BoxData[
 RowBox[{
  RowBox[{"(*", 
   RowBox[{"Euler", " ", "2", "D"}], "*)"}], "\[IndentingNewLine]", 
  RowBox[{
   RowBox[{
    RowBox[{"Fx", "=", 
     RowBox[{"{", 
      RowBox[{"px", ",", " ", 
       RowBox[{
        RowBox[{
         RowBox[{"px", "^", "2"}], "/", "rho"}], " ", "+", " ", 
        RowBox[{"rho", "*", 
         RowBox[{"a", "^", "2"}]}]}], ",", " ", 
       RowBox[{"px", "*", 
        RowBox[{"py", "/", "rho"}]}]}], "}"}]}], ";"}], "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"Fy", "=", 
     RowBox[{"{", 
      RowBox[{"py", ",", " ", 
       RowBox[{"px", "*", 
        RowBox[{"py", "/", "rho"}]}], ",", " ", 
       RowBox[{
        RowBox[{
         RowBox[{"py", "^", "2"}], "/", "rho"}], " ", "+", " ", 
        RowBox[{"rho", "*", 
         RowBox[{"a", "^", "2"}]}]}]}], "}"}]}], ";"}], "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"dFx", "=", 
     RowBox[{"D", "[", 
      RowBox[{"Fx", ",", 
       RowBox[{"{", 
        RowBox[{"{", 
         RowBox[{"rho", ",", " ", "px", ",", " ", "py"}], "}"}], "}"}]}], 
      "]"}]}], " ", ";"}], "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"dFy", "=", 
     RowBox[{"D", "[", 
      RowBox[{"Fy", ",", 
       RowBox[{"{", 
        RowBox[{"{", 
         RowBox[{"rho", ",", " ", "px", ",", " ", "py"}], "}"}], "}"}]}], 
      "]"}]}], " ", ";"}], "\[IndentingNewLine]", 
   RowBox[{"FullSimplify", "[", 
    RowBox[{"Eigenvalues", "[", "dFx", "]"}], "]"}], "\[IndentingNewLine]", 
   RowBox[{"FullSimplify", "[", 
    RowBox[{"Eigenvalues", "[", "dFy", "]"}], "]"}]}]}]], "Input",
 CellChangeTimes->{{3.847251792282679*^9, 3.8472518748799686`*^9}, {
  3.847251933709944*^9, 3.8472519581070137`*^9}, {3.847252213195835*^9, 
  3.84725226042026*^9}, {3.8472523023535557`*^9, 3.847252363592309*^9}},
 CellLabel->"In[1]:=",ExpressionUUID->"87023553-9e1b-4370-b4b6-a3ead616a562"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   FractionBox["px", "rho"], ",", 
   RowBox[{"a", "+", 
    FractionBox["px", "rho"]}], ",", 
   RowBox[{
    RowBox[{"-", "a"}], "+", 
    FractionBox["px", "rho"]}]}], "}"}]], "Output",
 CellChangeTimes->{{3.847251820140429*^9, 3.847251875366735*^9}, {
   3.8472519376671267`*^9, 3.847251958619583*^9}, {3.847252215875599*^9, 
   3.847252260747799*^9}, {3.84725230363384*^9, 3.847252314992751*^9}, 
   3.847252364423072*^9, 3.8472526003172398`*^9},
 CellLabel->"Out[5]=",ExpressionUUID->"f8d76b7b-955e-4e00-87a3-7c03a9b3c6d5"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   FractionBox["py", "rho"], ",", 
   RowBox[{"a", "+", 
    FractionBox["py", "rho"]}], ",", 
   RowBox[{
    RowBox[{"-", "a"}], "+", 
    FractionBox["py", "rho"]}]}], "}"}]], "Output",
 CellChangeTimes->{{3.847251820140429*^9, 3.847251875366735*^9}, {
   3.8472519376671267`*^9, 3.847251958619583*^9}, {3.847252215875599*^9, 
   3.847252260747799*^9}, {3.84725230363384*^9, 3.847252314992751*^9}, 
   3.847252364423072*^9, 3.847252600351956*^9},
 CellLabel->"Out[6]=",ExpressionUUID->"fec9cb5c-56a7-40cd-a011-a3e7c8f7ccda"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"(*", 
   RowBox[{"Shallow", " ", "water", " ", "2", "D"}], "*)"}], 
  "\[IndentingNewLine]", 
  RowBox[{
   RowBox[{
    RowBox[{"Fx", "=", 
     RowBox[{"{", 
      RowBox[{"px", ",", " ", 
       RowBox[{
        RowBox[{
         RowBox[{"px", "^", "2"}], "/", "h"}], " ", "+", " ", 
        RowBox[{"g", "*", 
         RowBox[{
          RowBox[{"h", "^", "2"}], "/", "2"}]}]}], ",", " ", 
       RowBox[{"px", "*", 
        RowBox[{"py", "/", "h"}]}]}], "}"}]}], ";"}], "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"Fy", "=", 
     RowBox[{"{", 
      RowBox[{"py", ",", " ", 
       RowBox[{"px", "*", 
        RowBox[{"py", "/", "h"}]}], ",", " ", 
       RowBox[{
        RowBox[{
         RowBox[{"py", "^", "2"}], "/", "h"}], " ", "+", " ", 
        RowBox[{"g", "*", 
         RowBox[{
          RowBox[{"h", "^", "2"}], "/", "2"}]}]}]}], "}"}]}], ";"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"dFx", "=", 
     RowBox[{"D", "[", 
      RowBox[{"Fx", ",", 
       RowBox[{"{", 
        RowBox[{"{", 
         RowBox[{"h", ",", " ", "px", ",", " ", "py"}], "}"}], "}"}]}], 
      "]"}]}], ";"}], " ", "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"dFy", "=", 
     RowBox[{"D", "[", 
      RowBox[{"Fy", ",", 
       RowBox[{"{", 
        RowBox[{"{", 
         RowBox[{"h", ",", " ", "px", ",", " ", "py"}], "}"}], "}"}]}], 
      "]"}]}], " ", ";"}], "\[IndentingNewLine]", 
   RowBox[{"FullSimplify", "[", 
    RowBox[{"Eigenvalues", "[", "dFx", "]"}], "]"}], "\[IndentingNewLine]", 
   RowBox[{"FullSimplify", "[", 
    RowBox[{"Eigenvalues", "[", "dFy", "]"}], "]"}]}]}]], "Input",
 CellChangeTimes->{{3.847252403259162*^9, 3.847252419808374*^9}, {
  3.847252508494606*^9, 3.847252575680036*^9}, {3.847252669353196*^9, 
  3.84725267915849*^9}, {3.847252892411552*^9, 
  3.847252893224455*^9}},ExpressionUUID->"3921c156-8a02-4734-a0b9-\
6b1c3b0d9526"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   FractionBox["px", "h"], ",", 
   FractionBox[
    RowBox[{
     RowBox[{
      RowBox[{"-", 
       SqrtBox["g"]}], " ", 
      SuperscriptBox["h", 
       RowBox[{"3", "/", "2"}]]}], "+", "px"}], "h"], ",", 
   FractionBox[
    RowBox[{
     RowBox[{
      SqrtBox["g"], " ", 
      SuperscriptBox["h", 
       RowBox[{"3", "/", "2"}]]}], "+", "px"}], "h"]}], "}"}]], "Output",
 CellChangeTimes->{{3.847252565165872*^9, 3.847252600515596*^9}, {
  3.847252670420452*^9, 3.847252679447583*^9}},
 CellLabel->"Out[29]=",ExpressionUUID->"ebd69297-9eea-4331-acaf-4259c9a5906c"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   FractionBox["py", "h"], ",", 
   FractionBox[
    RowBox[{
     RowBox[{
      RowBox[{"-", 
       SqrtBox["g"]}], " ", 
      SuperscriptBox["h", 
       RowBox[{"3", "/", "2"}]]}], "+", "py"}], "h"], ",", 
   FractionBox[
    RowBox[{
     RowBox[{
      SqrtBox["g"], " ", 
      SuperscriptBox["h", 
       RowBox[{"3", "/", "2"}]]}], "+", "py"}], "h"]}], "}"}]], "Output",
 CellChangeTimes->{{3.847252565165872*^9, 3.847252600515596*^9}, {
  3.847252670420452*^9, 3.847252679463538*^9}},
 CellLabel->"Out[30]=",ExpressionUUID->"73f00d0d-012e-47f9-9dac-7dbb77be2c30"]
}, Open  ]],

Cell[BoxData[""], "Input",
 CellChangeTimes->{{3.847252735544669*^9, 3.8472527405017023`*^9}, 
   3.847252788783753*^9},ExpressionUUID->"c0398b09-d738-4a28-8b4e-\
42219a737b24"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"(*", 
   RowBox[{"Shallow", " ", "water", " ", "curved", " ", "2", "d"}], "*)"}], 
  "\[IndentingNewLine]", 
  RowBox[{
   RowBox[{
    RowBox[{"Fx", "=", 
     RowBox[{"{", 
      RowBox[{"mx", ",", " ", 
       RowBox[{
        RowBox[{
         RowBox[{"mx", "^", "2"}], "/", "q"}], " ", "+", " ", 
        RowBox[{"g", "*", 
         RowBox[{
          RowBox[{"q", "^", "2"}], "/", "2"}], "*", 
         RowBox[{"g11", "/", "sqrtG"}]}]}], ",", " ", 
       RowBox[{
        RowBox[{"mx", "*", 
         RowBox[{"my", "/", "q"}]}], "+", 
        RowBox[{"g", "*", 
         RowBox[{
          RowBox[{"q", "^", "2"}], "/", "2"}], "*", 
         RowBox[{"g12", "/", "sqrtG"}]}]}]}], "}"}]}], ";"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"Fy", "=", 
     RowBox[{"{", 
      RowBox[{"my", ",", " ", 
       RowBox[{
        RowBox[{"mx", "*", 
         RowBox[{"my", "/", "q"}]}], " ", "+", " ", 
        RowBox[{"g", "*", 
         RowBox[{
          RowBox[{"q", "^", "2"}], "/", "2"}], "*", 
         RowBox[{"g21", "/", "sqrtG"}]}]}], ",", " ", 
       RowBox[{
        RowBox[{
         RowBox[{"my", "^", "2"}], "/", "q"}], " ", "+", " ", 
        RowBox[{"g", "*", 
         RowBox[{
          RowBox[{"q", "^", "2"}], "/", "2"}], "*", 
         RowBox[{"g22", "/", "sqrtG"}]}]}]}], "}"}]}], ";"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"dFx", "=", 
     RowBox[{"D", "[", 
      RowBox[{"Fx", ",", 
       RowBox[{"{", 
        RowBox[{"{", 
         RowBox[{"q", ",", " ", "mx", ",", " ", "my"}], "}"}], "}"}]}], 
      "]"}]}], ";"}], " ", "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"dFy", "=", 
     RowBox[{"D", "[", 
      RowBox[{"Fy", ",", 
       RowBox[{"{", 
        RowBox[{"{", 
         RowBox[{"q", ",", " ", "mx", ",", " ", "my"}], "}"}], "}"}]}], 
      "]"}]}], " ", ";"}], "\[IndentingNewLine]", 
   RowBox[{"FullSimplify", "[", 
    RowBox[{"Eigenvalues", "[", "dFx", "]"}], "]"}], "\[IndentingNewLine]", 
   RowBox[{"FullSimplify", "[", 
    RowBox[{"Eigenvalues", "[", "dFy", "]"}], "]"}]}]}]], "Input",
 CellChangeTimes->{{3.847252896990539*^9, 3.8472530164366503`*^9}, {
  3.8472536453659163`*^9, 3.8472537639412737`*^9}},
 CellLabel->"In[49]:=",ExpressionUUID->"0e7c6b4c-30c2-4a4f-ac32-f57f2362ea03"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   FractionBox["mx", "q"], ",", 
   FractionBox[
    RowBox[{"mx", "-", 
     FractionBox[
      RowBox[{
       SqrtBox["g"], " ", 
       SqrtBox["g11"], " ", 
       SuperscriptBox["q", 
        RowBox[{"3", "/", "2"}]]}], 
      SqrtBox["sqrtG"]]}], "q"], ",", 
   FractionBox[
    RowBox[{"mx", "+", 
     FractionBox[
      RowBox[{
       SqrtBox["g"], " ", 
       SqrtBox["g11"], " ", 
       SuperscriptBox["q", 
        RowBox[{"3", "/", "2"}]]}], 
      SqrtBox["sqrtG"]]}], "q"]}], "}"}]], "Output",
 CellChangeTimes->{3.847253764957404*^9},
 CellLabel->"Out[53]=",ExpressionUUID->"2a7e87cb-4ee5-4281-ba0f-71a83bb1c014"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   FractionBox["my", "q"], ",", 
   FractionBox[
    RowBox[{"my", "-", 
     FractionBox[
      RowBox[{
       SqrtBox["g"], " ", 
       SqrtBox["g22"], " ", 
       SuperscriptBox["q", 
        RowBox[{"3", "/", "2"}]]}], 
      SqrtBox["sqrtG"]]}], "q"], ",", 
   FractionBox[
    RowBox[{"my", "+", 
     FractionBox[
      RowBox[{
       SqrtBox["g"], " ", 
       SqrtBox["g22"], " ", 
       SuperscriptBox["q", 
        RowBox[{"3", "/", "2"}]]}], 
      SqrtBox["sqrtG"]]}], "q"]}], "}"}]], "Output",
 CellChangeTimes->{3.847253765046774*^9},
 CellLabel->"Out[54]=",ExpressionUUID->"217851e6-3822-4dec-98fd-ddf8df6fa2ee"]
}, Open  ]]
},
WindowSize->{1146, 627},
WindowMargins->{{0, Automatic}, {0, Automatic}},
FrontEndVersion->"12.1 for Linux x86 (64-bit) (June 19, 2020)",
StyleDefinitions->"Default.nb",
ExpressionUUID->"bd9b34f1-045a-427f-ae5e-bf4098db43c6"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[580, 22, 1873, 50, 154, "Input",ExpressionUUID->"87023553-9e1b-4370-b4b6-a3ead616a562"],
Cell[2456, 74, 569, 13, 47, "Output",ExpressionUUID->"f8d76b7b-955e-4e00-87a3-7c03a9b3c6d5"],
Cell[3028, 89, 567, 13, 47, "Output",ExpressionUUID->"fec9cb5c-56a7-40cd-a011-a3e7c8f7ccda"]
}, Open  ]],
Cell[CellGroupData[{
Cell[3632, 107, 1918, 55, 154, "Input",ExpressionUUID->"3921c156-8a02-4734-a0b9-6b1c3b0d9526"],
Cell[5553, 164, 615, 19, 51, "Output",ExpressionUUID->"ebd69297-9eea-4331-acaf-4259c9a5906c"],
Cell[6171, 185, 615, 19, 51, "Output",ExpressionUUID->"73f00d0d-012e-47f9-9dac-7dbb77be2c30"]
}, Open  ]],
Cell[6801, 207, 177, 3, 29, "Input",ExpressionUUID->"c0398b09-d738-4a28-8b4e-42219a737b24"],
Cell[CellGroupData[{
Cell[7003, 214, 2302, 66, 154, "Input",ExpressionUUID->"0e7c6b4c-30c2-4a4f-ac32-f57f2362ea03"],
Cell[9308, 282, 673, 23, 65, "Output",ExpressionUUID->"2a7e87cb-4ee5-4281-ba0f-71a83bb1c014"],
Cell[9984, 307, 673, 23, 88, "Output",ExpressionUUID->"217851e6-3822-4dec-98fd-ddf8df6fa2ee"]
}, Open  ]]
}
]
*)

