(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 11.3' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     11757,        337]
NotebookOptionsPosition[     10734,        313]
NotebookOutlinePosition[     11068,        328]
CellTagsIndexPosition[     11025,        325]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell[BoxData[{
 RowBox[{
  RowBox[{
   RowBox[{"A", "[", 
    RowBox[{"t_", ",", "l_"}], "]"}], ":=", 
   RowBox[{
    RowBox[{"Cos", "[", "t", "]"}], "*", 
    RowBox[{"Cos", "[", "l", "]"}], 
    RowBox[{"(", GridBox[{
       {
        RowBox[{"Cos", "[", "l", "]"}], "0"},
       {
        RowBox[{
         RowBox[{"-", 
          RowBox[{"Sin", "[", "t", "]"}]}], 
         RowBox[{"Sin", "[", "l", "]"}]}], 
        RowBox[{"Cos", "[", "t", "]"}]}
      }], ")"}]}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{
    RowBox[{"AI", "[", 
     RowBox[{"t_", ",", "l_"}], "]"}], ":=", 
    RowBox[{
     RowBox[{"Sec", "[", "t", "]"}], 
     RowBox[{"Sec", "[", "l", "]"}], 
     RowBox[{"(", GridBox[{
        {
         RowBox[{"Sec", "[", "l", "]"}], "0"},
        {
         RowBox[{
          RowBox[{"Tan", "[", "t", "]"}], "  ", 
          RowBox[{"Tan", "[", "l", "]"}]}], 
         RowBox[{"Sec", "[", "t", "]"}]}
       }], ")"}]}]}], ";"}], "\[IndentingNewLine]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"AI", "[", 
   RowBox[{"t", ",", "l"}], "]"}], ".", 
  RowBox[{"A", "[", 
   RowBox[{"t", ",", "l"}], "]"}]}]}], "Input",
 CellChangeTimes->{{3.7854949199457817`*^9, 3.785495082037879*^9}, {
  3.785495282116221*^9, 3.785495333268512*^9}, {3.785495369571465*^9, 
  3.785495433452064*^9}, {3.7854958552295017`*^9, 3.7854958586752996`*^9}},
 CellLabel->"In[9]:=",ExpressionUUID->"401613f9-87f2-40a0-a3de-efcefb1c0883"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{"1", ",", "0"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"0", ",", "1"}], "}"}]}], "}"}]], "Output",
 CellChangeTimes->{3.7854958590083437`*^9, 3.785498774325798*^9},
 CellLabel->"Out[11]=",ExpressionUUID->"12363197-b3bc-4b3d-8e3c-fd191d9b2afe"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{"Clear", "[", 
  RowBox[{"phi", ",", " ", "theta"}], "]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"phi", "=", 
   RowBox[{"Pi", "/", "4"}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"theta", "=", 
   RowBox[{"Pi", "/", "4"}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"x", "=", 
   RowBox[{"Tan", "[", "phi", "]"}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"y", "=", 
   RowBox[{
    RowBox[{"1", "/", 
     RowBox[{"Tan", "[", "theta", "]"}]}], "*", 
    RowBox[{"Cos", "[", "phi", "]"}]}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"delta", "=", 
   RowBox[{"1", "+", 
    RowBox[{"x", "^", "2"}], "+", 
    RowBox[{"y", "^", "2"}]}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"c", "=", 
   RowBox[{
    RowBox[{"(", 
     RowBox[{"1", "+", 
      RowBox[{"x", "^", "2"}]}], ")"}], "^", 
    RowBox[{"(", 
     RowBox[{"1", "/", "2"}], ")"}]}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"d", "=", 
    RowBox[{
     RowBox[{"(", 
      RowBox[{"1", "+", 
       RowBox[{"y", "^", "2"}]}], ")"}], "^", 
     RowBox[{"(", 
      RowBox[{"1", "/", "2"}], ")"}]}]}], ";"}], 
  "\[IndentingNewLine]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"A1", "=", 
   RowBox[{"Simplify", "[", 
    RowBox[{"(", GridBox[{
       {"0", 
        RowBox[{"c", "*", 
         RowBox[{"d", "/", 
          RowBox[{"delta", "^", 
           RowBox[{"(", 
            RowBox[{"1", "/", "2"}], ")"}]}]}]}]},
       {
        RowBox[{"-", "1"}], 
        RowBox[{"x", "*", 
         RowBox[{"y", "/", 
          RowBox[{"delta", "^", 
           RowBox[{"(", 
            RowBox[{"1", "/", "2"}], ")"}]}]}]}]}
      }], ")"}], "]"}]}], "  "}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"AI1", "=", 
   RowBox[{"Inverse", "[", "A", "]"}]}], " ", 
  "\[IndentingNewLine]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"x", "=", 
   RowBox[{
    RowBox[{"-", "1"}], "/", 
    RowBox[{"Tan", "[", "phi", "]"}]}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"y", "=", 
   RowBox[{
    RowBox[{"1", "/", 
     RowBox[{"Tan", "[", "theta", "]"}]}], 
    RowBox[{"Sin", "[", "phi", "]"}]}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"A2", "=", 
   RowBox[{"Simplify", "[", 
    RowBox[{"(", GridBox[{
       {"0", 
        RowBox[{"c", "*", 
         RowBox[{"d", "/", 
          RowBox[{"delta", "^", 
           RowBox[{"(", 
            RowBox[{"1", "/", "2"}], ")"}]}]}]}]},
       {
        RowBox[{"-", "1"}], 
        RowBox[{"x", "*", 
         RowBox[{"y", "/", 
          RowBox[{"delta", "^", 
           RowBox[{"(", 
            RowBox[{"1", "/", "2"}], ")"}]}]}]}]}
      }], ")"}], "]"}]}], "  "}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"AI2", "=", 
   RowBox[{"Inverse", "[", "A2", "]"}]}], " ", 
  "\[IndentingNewLine]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"A1", ".", "AI2"}], " ", "//", "MatrixForm"}], 
  "\[IndentingNewLine]"}], "\[IndentingNewLine]"}], "Input",
 CellChangeTimes->{{3.7854953416301193`*^9, 3.785495385987384*^9}, {
  3.7854987952151203`*^9, 3.7854989757570753`*^9}, {3.785499017782509*^9, 
  3.785499050907693*^9}, {3.785499176613722*^9, 3.7854991800181*^9}, {
  3.78549961533365*^9, 3.785499625868084*^9}, {3.785499659290872*^9, 
  3.7854997821435003`*^9}, {3.785499828915028*^9, 3.785499857359497*^9}, {
  3.785499894379942*^9, 3.785499907932889*^9}, {3.785499968709794*^9, 
  3.7855001237817783`*^9}, {3.785500172717016*^9, 3.785500184498824*^9}, {
  3.7855003990145597`*^9, 3.785500420066227*^9}},
 CellLabel->
  "In[359]:=",ExpressionUUID->"33f36e8d-51bb-4e88-9cd8-f7120cc485bc"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{"0", ",", 
     SqrtBox[
      FractionBox["6", "5"]]}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"-", "1"}], ",", 
     FractionBox["1", 
      SqrtBox["5"]]}], "}"}]}], "}"}]], "Output",
 CellChangeTimes->{{3.785498936134321*^9, 3.785498976254356*^9}, 
   3.785499051337714*^9, 3.7854991822279167`*^9, {3.7854996179095917`*^9, 
   3.785499626745008*^9}, {3.78549966237598*^9, 3.785499722201173*^9}, 
   3.7854997825578117`*^9, {3.7854998361104393`*^9, 3.7854998488134537`*^9}, {
   3.78549988628354*^9, 3.7854999014787903`*^9}, {3.785499979060129*^9, 
   3.785500124572399*^9}, {3.7855001735767508`*^9, 3.7855001852589607`*^9}, {
   3.785500399811369*^9, 3.7855004205074244`*^9}},
 CellLabel->
  "Out[367]=",ExpressionUUID->"98314074-f992-4e6d-837e-dfbf46cb848f"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{
     FractionBox["1", 
      SqrtBox["6"]], ",", 
     RowBox[{"-", "1"}]}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{
     SqrtBox[
      FractionBox["5", "6"]], ",", "0"}], "}"}]}], "}"}]], "Output",
 CellChangeTimes->{{3.785498936134321*^9, 3.785498976254356*^9}, 
   3.785499051337714*^9, 3.7854991822279167`*^9, {3.7854996179095917`*^9, 
   3.785499626745008*^9}, {3.78549966237598*^9, 3.785499722201173*^9}, 
   3.7854997825578117`*^9, {3.7854998361104393`*^9, 3.7854998488134537`*^9}, {
   3.78549988628354*^9, 3.7854999014787903`*^9}, {3.785499979060129*^9, 
   3.785500124572399*^9}, {3.7855001735767508`*^9, 3.7855001852589607`*^9}, {
   3.785500399811369*^9, 3.7855004205118113`*^9}},
 CellLabel->
  "Out[368]=",ExpressionUUID->"1dfdc43e-16c4-4017-bec5-82577b3ef31e"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{"0", ",", 
     SqrtBox[
      FractionBox["6", "5"]]}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"-", "1"}], ",", 
     RowBox[{"-", 
      FractionBox["1", 
       SqrtBox["5"]]}]}], "}"}]}], "}"}]], "Output",
 CellChangeTimes->{{3.785498936134321*^9, 3.785498976254356*^9}, 
   3.785499051337714*^9, 3.7854991822279167`*^9, {3.7854996179095917`*^9, 
   3.785499626745008*^9}, {3.78549966237598*^9, 3.785499722201173*^9}, 
   3.7854997825578117`*^9, {3.7854998361104393`*^9, 3.7854998488134537`*^9}, {
   3.78549988628354*^9, 3.7854999014787903`*^9}, {3.785499979060129*^9, 
   3.785500124572399*^9}, {3.7855001735767508`*^9, 3.7855001852589607`*^9}, {
   3.785500399811369*^9, 3.785500420516185*^9}},
 CellLabel->
  "Out[371]=",ExpressionUUID->"ca5a3041-afce-4252-843c-cc31dbcbc2d6"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{
     RowBox[{"-", 
      FractionBox["1", 
       SqrtBox["6"]]}], ",", 
     RowBox[{"-", "1"}]}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{
     SqrtBox[
      FractionBox["5", "6"]], ",", "0"}], "}"}]}], "}"}]], "Output",
 CellChangeTimes->{{3.785498936134321*^9, 3.785498976254356*^9}, 
   3.785499051337714*^9, 3.7854991822279167`*^9, {3.7854996179095917`*^9, 
   3.785499626745008*^9}, {3.78549966237598*^9, 3.785499722201173*^9}, 
   3.7854997825578117`*^9, {3.7854998361104393`*^9, 3.7854998488134537`*^9}, {
   3.78549988628354*^9, 3.7854999014787903`*^9}, {3.785499979060129*^9, 
   3.785500124572399*^9}, {3.7855001735767508`*^9, 3.7855001852589607`*^9}, {
   3.785500399811369*^9, 3.785500420518051*^9}},
 CellLabel->
  "Out[372]=",ExpressionUUID->"5d5c94de-7dcd-43ea-a8ff-7cd8f23943b1"],

Cell[BoxData[
 TagBox[
  RowBox[{"(", "\[NoBreak]", GridBox[{
     {"1", "0"},
     {
      SqrtBox[
       FractionBox["2", "3"]], "1"}
    },
    GridBoxAlignment->{
     "Columns" -> {{Center}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}}, 
      "RowsIndexed" -> {}},
    GridBoxSpacings->{"Columns" -> {
        Offset[0.27999999999999997`], {
         Offset[0.7]}, 
        Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {
        Offset[0.2], {
         Offset[0.4]}, 
        Offset[0.2]}, "RowsIndexed" -> {}}], "\[NoBreak]", ")"}],
  Function[BoxForm`e$, 
   MatrixForm[BoxForm`e$]]]], "Output",
 CellChangeTimes->{{3.785498936134321*^9, 3.785498976254356*^9}, 
   3.785499051337714*^9, 3.7854991822279167`*^9, {3.7854996179095917`*^9, 
   3.785499626745008*^9}, {3.78549966237598*^9, 3.785499722201173*^9}, 
   3.7854997825578117`*^9, {3.7854998361104393`*^9, 3.7854998488134537`*^9}, {
   3.78549988628354*^9, 3.7854999014787903`*^9}, {3.785499979060129*^9, 
   3.785500124572399*^9}, {3.7855001735767508`*^9, 3.7855001852589607`*^9}, {
   3.785500399811369*^9, 3.785500420519813*^9}},
 CellLabel->
  "Out[373]//MatrixForm=",ExpressionUUID->"91cda47a-ecbb-4740-906a-\
995965f7cef1"]
}, Open  ]]
},
WindowSize->{1853, 1025},
WindowMargins->{{0, Automatic}, {0, Automatic}},
FrontEndVersion->"11.3 for Linux x86 (64-bit) (March 6, 2018)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[580, 22, 1459, 43, 254, "Input",ExpressionUUID->"401613f9-87f2-40a0-a3de-efcefb1c0883"],
Cell[2042, 67, 316, 8, 97, "Output",ExpressionUUID->"12363197-b3bc-4b3d-8e3c-fd191d9b2afe"]
}, Open  ]],
Cell[CellGroupData[{
Cell[2395, 80, 3654, 109, 971, "Input",ExpressionUUID->"33f36e8d-51bb-4e88-9cd8-f7120cc485bc"],
Cell[6052, 191, 849, 20, 152, "Output",ExpressionUUID->"98314074-f992-4e6d-837e-dfbf46cb848f"],
Cell[6904, 213, 849, 20, 152, "Output",ExpressionUUID->"1dfdc43e-16c4-4017-bec5-82577b3ef31e"],
Cell[7756, 235, 870, 21, 152, "Output",ExpressionUUID->"ca5a3041-afce-4252-843c-cc31dbcbc2d6"],
Cell[8629, 258, 870, 21, 152, "Output",ExpressionUUID->"5d5c94de-7dcd-43ea-a8ff-7cd8f23943b1"],
Cell[9502, 281, 1216, 29, 240, "Output",ExpressionUUID->"91cda47a-ecbb-4740-906a-995965f7cef1"]
}, Open  ]]
}
]
*)

