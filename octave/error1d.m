order = 6;
rkOrder = 6;
ref = 1024;
%filename = "/home/holger/git/ndg2d/trunk/data1d/sol_6-6_512_x.bin";
%fid = fopen (filename, "r");
%ref_x = fread (fid, 100, "double");

filename = sprintf("/home/holger/git/ndg2d/trunk/data1d/sol_%i-%i_%i-0.bin",order,rkOrder,ref);
fid = fopen (filename, "r");
ref_0 = fread (fid, 100, "double");

%filename = "/home/holger/git/ndg2d/trunk/data1d/sol_6-6_512-1.bin";
%fid = fopen (filename, "r");
%ef_1 = fread (fid, 100, "double");

resolutions = [4, 8, 16, 32, 64, 128, 256, 512, 1024];

index = 1;
errorVec=zeros(size(resolutions)(2),2);

for res = resolutions

filename = sprintf("/home/holger/git/ndg2d/trunk/data1d/sol_%i-%i_%i-0.bin",order,rkOrder,res);
fid = fopen (filename, "r");
array_0 = fread (fid, 100, "double");
 
error = 0;
for i=1:100
  error = error + (array_0(i) - ref_0(i))^2;
endfor

error = (error/100)^0.5
errorVec(index,1) = res
errorVec(index,2) = error
index = index + 1
endfor 

filename = sprintf("/home/holger/git/ndg2d/trunk/data1d/sol_%i-%i_%i-0.bin_error.txt",order,rkOrder,ref);

save("-ascii", filename, "errorVec");